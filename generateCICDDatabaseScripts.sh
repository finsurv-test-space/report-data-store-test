#!/bin/bash

cur_dir=$PWD

if [ $# -gt 0 ]
then
  databaseName=$1
  echo "Database name updated to $databaseName"
else
  databaseName="ReportServices"
  echo "Database name defaulted to $databaseName"
fi

cd "$cur_dir/database/Scripts/Installation" || exit

for file in *; do
  if [ -f "$file" ]; then
    sed 's/\[ReportServices\]/'\[$databaseName\]'/gi' "$file" > "$cur_dir/database/Scripts/cicd/$file"
  fi
done

cd "$cur_dir/database/Scripts/Update" || exit

for file in *; do
  if [ -f "$file" ]; then
    sed 's/\[ReportServices\]/'\[$databaseName\]'/gi' "$file" > "$cur_dir/database/Scripts/cicd/$file"
  fi
done

cd "$cur_dir/database/Scripts/DataSetupScripts" || exit

sed 's/\[ReportServices\]/'\[$databaseName\]'/gi' "RS_DataSetup.GENERAL.sql" > "$cur_dir/database/Scripts/cicd/RS_DataSetup.GENERAL.sql"

cd "$cur_dir/database/Scripts/cicd" || exit

sed -i 's/\[ReportServices\]/'\[$databaseName\]'/gi' "000_RS_DropDatabase_MSSQL.sql" 
