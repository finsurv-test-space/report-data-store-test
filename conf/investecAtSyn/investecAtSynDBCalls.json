{
  "datasource": {
    "jdbc.driverClassName" : "com.microsoft.sqlserver.jdbc.SQLServerDriver",
    "jdbc.url" : "jdbc:sqlserver://${txstream.host};databaseName=${txstream.database};sendStringParametersAsUnicode=false",
    "jdbc.username": "${txstream.username}",
    "jdbc.password" : "${txstream.password}"
  },
  "validations": {
    "Validate_LoanRef": {
      "endpoint": "loanRef",
      "parameters": [
        { "name": "loanRef", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT COUNT(*) as count FROM OR_LoanReference WITH (NOLOCK)",
        "WHERE [LoanRefNumber] = '${util.escapeSql(loanRef)}'",
        "AND [Status] IN ('C', 'A')"
        ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row.count == 0>",
        "{status: 'fail', code: 374, message: 'Invalid loan reference number ${util.escapeJavaScript(loanRef)}'}",
        "<#else>",
        "{status: 'pass'}",
        "</#if>"
      ]
    },
    "Validate_ImportUndertakingCCN": {
      "endpoint": "importUndertakingCCN",
      "parameters": [
        { "name": "CCN", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT [LetterOfUndertaking] as LU FROM OR_CustomsClientNumber WITH (NOLOCK)",
        "WHERE [CCN] = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "AND ([ValidFrom] IS NULL OR [ValidFrom] <= '${util.escapeSql(valueDate)}')",
        "AND ([ValidTo] IS NULL OR [ValidTo] >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row??>",
        "  <#if row.LU == 'Y'>",
        "{status: 'pass'}",
        "  <#else>",
        "{status: 'fail', code: 323, message: 'The CCN ${util.escapeJavaScript(CCN)} is not a registered import undertaking entity'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: '323', message: 'The CCN ${util.escapeJavaScript(CCN)} is not a valid entry<#if valueDate??> for the date ${valueDate}</#if>'}",
        "</#if>"
      ]
    },
    "Validate_ReplacementTrnReference": {
      "endpoint": "replacementTrnReference",
      "parameters": [
        { "name": "replacementTrnReference", "path": "ReplacementOriginalReference" }
      ],
      "sql": [
        "WITH cteCancelled AS (",
        "SELECT t.TrnReference, CASE WHEN qeD.EntryStateID IN (4 /*Acknowledged*/, 5 /*AssumedAcknowledged*/) THEN 'Acked' ELSE 'Wait' END DeleteState",
        " FROM OR_Transaction t WITH (NOLOCK)",
        " JOIN (OR_QueueEntry qeD WITH (NOLOCK)",
        "  JOIN OR_TransactionSnapshot tsD WITH (NOLOCK) ON tsD.SnapshotID = qeD.SnapshotID",
        " ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D'",
        " LEFT JOIN (OR_QueueEntry qeA WITH (NOLOCK)",
        "  JOIN OR_TransactionSnapshot tsA WITH (NOLOCK) ON tsA.SnapshotID = qeA.SnapshotID",
        " ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded",
        "WHERE t.TrnReference = '${util.escapeSql(replacementTrnReference)}' AND qeA.QueueEntryID IS NULL)",
        "SELECT MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 /*NotReportable*/ THEN 'Y' WHEN c.TrnReference IS NOT NULL AND c.DeleteState = 'Acked' THEN 'Y' ELSE 'N' END) as Cancelled",
        " FROM OR_Transaction tx WITH (NOLOCK)",
        " LEFT JOIN cteCancelled c WITH (NOLOCK) ON c.TrnReference = tx.TrnReference",
        "WHERE tx.TrnReference = '${util.escapeSql(replacementTrnReference)}'",
        "GROUP BY tx.TrnReference"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row??>",
        "  <#if row.Cancelled == 'Y'>",
        "{status: 'pass'}",
        "  <#else>",
        "{status: 'fail', code: 212, message: '${util.escapeJavaScript(replacementTrnReference)} has not yet been cancelled (Original transaction must first be cancelled)'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: '212', message: '${util.escapeJavaScript(replacementTrnReference)} in not an existing transaction'}",
        "</#if>"
      ]
    },
    "Validate_ReversalTrnRef": {
      "endpoint": "reversalTrnRef",
      "parameters": [
        { "name": "trnReference", "path": "TrnReference" },
        { "name": "flow", "path": "transaction::Flow" },
        { "name": "sequence", "path": "ReversalTrnSeqNumber" },
        { "name": "category", "path": "CategoryCode" }
      ],
      "sql": [
        "SELECT CASE WHEN t.Flow = '${util.escapeSql(flow)}' THEN 'SF' ELSE 'OF' END as Flow,",
        "       CASE WHEN SUBSTRING(ma.CategoryCode, 1, 1) = SUBSTRING('${util.escapeSql(category)}', 1,1) THEN 'SC' ELSE 'OC' END as Category,",
        "       CASE WHEN t.StateID = 7/*Reported*/ THEN 'R' ELSE 'N' END as Reported,",
        "       ma.CategoryCode as OldCategory",
        "  FROM OR_Transaction t WITH (NOLOCK)",
        "  JOIN OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference",
        " WHERE t.TrnReference = '${util.escapeSql(trnReference)}' AND ma.SequenceNumber = ${sequence}"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row??>",
        "  <#if row.Flow == 'SF'>",
        "{status: 'fail', code: 410, message: 'Original transaction and SequenceNumber combination stored on database, but not with an opposite flow'}",
        "  <#elseif row.Category == 'OC'>",
        "{status: 'fail', code: 411, message: 'Incorrect reversal category used with original transaction category. Original category is ${OldCategory}}'}",
        "  <#elseif row.Reported == 'N'>",
        "{status: 'fail', code: 410, message: 'Original transaction stored on database, but has not been reported to the SARB'}",
        "  <#else>",
        "{status: 'pass'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: '410', message: 'Original transaction and SequenceNumber combination not stored on database'}",
        "</#if>"
      ]
    },
    "Validate_ValidCCN": {
      "endpoint": "validCCN",
      "parameters": [
        { "name": "CCN", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT CASE WHEN AuthBy IS NOT NULL THEN 'A' ELSE 'N' END as Auth, CASE WHEN LetterOfUndertaking = 'Y' THEN 'Y' ELSE 'N' END as LU, CCN",
        " FROM OR_CustomsClientNumber WITH (NOLOCK)",
        " WHERE CCN = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "   AND (ValidFrom IS NULL OR ValidFrom <= '${util.escapeSql(valueDate)}')",
        "   AND (ValidTo IS NULL OR ValidTo >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row??>",
        "  <#if row.Auth == 'Y' && row.LU == 'N'>",
        "{status: 'pass'}",
        "  <#elseif row.Auth == 'Y' && row.LU == 'Y'>",
        "{status: 'pass', code: 200, message: 'Is LU Client'}",
        "  <#elseif row.Auth == 'N'>",
        "{status: 'fail', code: 322, message: 'Not authorised as a registered customs client number'}",
        "  </#if>",
        "<#else>",
        "  <#if logic.isValidCCN(CCN)>",
        "{status: 'pass'}",
        "  <#else>",
        "{status: 'fail', code: '322', message: 'Not a valid customs client number'}",
        "  </#if>",
        "</#if>"
      ]
    },
    "Validate_ValidCCNinUCR": {
      "endpoint": "validCCNinUCR",
      "parameters": [
        { "name": "UCR", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT CASE WHEN AuthBy IS NOT NULL THEN 'A' ELSE 'N' END as Auth, CASE WHEN LetterOfUndertaking = 'Y' THEN 'Y' ELSE 'N' END as LU, CCN",
        " FROM OR_CustomsClientNumber WITH (NOLOCK)",
        " WHERE CCN = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "   AND (ValidFrom IS NULL OR ValidFrom <= '${util.escapeSql(valueDate)}')",
        "   AND (ValidTo IS NULL OR ValidTo >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if row??>",
        "  <#if row.Auth == 'Y' && row.LU == 'N'>",
        "{status: 'pass'}",
        "  <#elseif row.Auth == 'Y' && row.LU == 'Y'>",
        "{status: 'pass', code: 200, message: 'Is LU Client'}",
        "  <#elseif row.Auth == 'N'>",
        "{status: 'fail', code: 322, message: 'Not authorised as a registered customs client number'}",
        "  </#if>",
        "<#else>",
        "  <#if logic.isValidCCN(CCN)>",
        "{status: 'pass'}",
        "  <#else>",
        "{status: 'fail', code: '322', message: 'Not a valid customs client number'}",
        "  </#if>",
        "</#if>"
      ]
    },
    "Validate_IndividualSDA": {
      "endpoint": "withinSDALimit",
      "parameters": [
        { "name": "localValue", "path": "value" },
        { "name": "trnRef", "path": "transaction::TrnReference" },
        { "name": "valueDate", "path": "transaction::ValueDate" },
        { "name": "resIDNumber", "path": "transaction::Resident.Individual.IDNumber" },
        { "name": "tpIDNumber", "path": "ThirdParty.Individual.IDNumber" }
      ],
      "sql": [
        "SELECT ISNULL(SUM(ma.DomesticValue), 0) as TotalValueForYear",
        "FROM [OR_Transaction] t",
        "JOIN [OR_MonetaryAmount] ma ON ma.[AdhocSubject] = 'SDA' AND ma.TrnReference = t.TrnReference",
        "LEFT JOIN [OR_ThirdParty] tp ON tp.[TrnReference] = ma.[TrnReference] AND tp.[SequenceNumber] = ma.[SequenceNumber]",
        "WHERE t.[StateID] IN (3 /*Complete*/, 4 /*Authorise*/, 5 /*ReadyToGo*/, 6 /*Submitted*/, 7 /*Reported*/)",
        "AND t.[ValueDate] BETWEEN '${util.escapeSql(valueDate[0..3])}-01-01' AND '${util.escapeSql(valueDate[0..3])}-12-31'",
        "AND ((t.[RI_IDNumber] = '<#if resIDNumber??>${util.escapeSql(resIDNumber)}<#else>${util.escapeSql(tpIDNumber)}</#if>' AND tp.[IndividualIDNumber] IS NULL) OR tp.[IndividualIDNumber] = '<#if tpIDNumber??>${util.escapeSql(tpIDNumber)}<#else>${util.escapeSql(resIDNumber)}</#if>')",
        "AND t.[TrnReference] <> '${util.escapeSql(trnRef)}'"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if (row.TotalValueForYear + localValue?number > 1000000)>",
        "{status: 'fail', code: 'DAL', message: 'You have exceeded the discretionary amount limit of R1,000,000 (for <#if resIDNumber??>${util.escapeSql(resIDNumber)}<#else>${util.escapeSql(tpIDNumber)}</#if> used during ${util.escapeSql(valueDate[0..3])} of ${row.TotalValueForYear?string(',##0.00')})'}",
        "<#else>",
        "{status: 'pass'}",
        "</#if>"
      ]
    },
    "Validate_IndividualFIA": {
      "endpoint": "withinFIALimit",
      "parameters": [
        { "name": "localValue", "path": "value" },
        { "name": "trnRef", "path": "transaction::TrnReference" },
        { "name": "valueDate", "path": "transaction::ValueDate" },
        { "name": "resIDNumber", "path": "transaction::Resident.Individual.IDNumber" },
        { "name": "tpIDNumber", "path": "ThirdParty.Individual.IDNumber" }
      ],
      "sql": [
        "SELECT ISNULL(SUM(ma.DomesticValue), 0) as TotalValueForYear",
        "FROM [OR_Transaction] t",
        "JOIN [OR_MonetaryAmount] ma ON ma.[AdhocSubject] = 'SDA' AND ma.TrnReference = t.TrnReference",
        "LEFT JOIN [OR_ThirdParty] tp ON tp.[TrnReference] = ma.[TrnReference] AND tp.[SequenceNumber] = ma.[SequenceNumber]",
        "WHERE t.[StateID] IN (3 /*Complete*/, 4 /*Authorise*/, 5 /*ReadyToGo*/, 6 /*Submitted*/, 7 /*Reported*/)",
        "AND t.[ValueDate] BETWEEN '${util.escapeSql(valueDate[0..3])}-01-01' AND '${util.escapeSql(valueDate[0..3])}-12-31'",
        "AND ((t.[RI_IDNumber] = '<#if resIDNumber??>${util.escapeSql(resIDNumber)}<#else>${util.escapeSql(tpIDNumber)}</#if>' AND tp.[IndividualIDNumber] IS NULL) OR tp.[IndividualIDNumber] = '<#if tpIDNumber??>${util.escapeSql(tpIDNumber)}<#else>${util.escapeSql(resIDNumber)}</#if>')",
        "AND t.[TrnReference] <> '${util.escapeSql(trnRef)}'",
        "AND t.[Flow] = 'O'",
        "AND (ma.[CategoryCode] LIKE '511%' OR ma.[CategoryCode] LIKE '512%')"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#if (row.TotalValueForYear + localValue?number > 1000000)>",
        "{status: 'fail', code: 'FIAL', message: 'You have exceeded the fia amount limit of R1,000,000 (for <#if resIDNumber??>${util.escapeSql(resIDNumber)}<#else>${util.escapeSql(tpIDNumber)}</#if> used during ${util.escapeSql(valueDate[0..3])} of ${row.TotalValueForYear?string(',##0.00')})'}",
        "<#else>",
        "{status: 'pass'}",
        "</#if>"
      ]
    }
  },
  "lookups": {
    "adhocSubjects": {
      "sql": [
        "SELECT AdhocSubject as Subject",
        "FROM OR_AdhocSubject WITH (NOLOCK)",
        "WHERE AdhocSubject <> 'IHQnnn'",
        "UNION",
        "SELECT Code as Subject",
        "FROM OR_IHQ WITH (NOLOCK)"
        ]
    },
    "branches": {
      "sql": [
        "SELECT BranchCode as code, BranchName as name, HubCode as hubCode, HubName as hubName,",
        "ISNULL(ValidFrom, '2000-01-01') as validFrom, ValidTo as validTo",
        "FROM OR_Branch WITH (NOLOCK)",
        "WHERE BranchName NOT LIKE 'Unknown%'"
      ]
    },
    "categories": {
      "rest": {
        "verb": "GET",
        "url": "http://localhost:${rules_port}/lookups/category"
      }
    },
    "countries": {
      "rest": {
        "verb": "GET",
        "url": "http://localhost:${rules_port}/lookups/country"
      }
    },
    "currencies": {
      "rest": {
        "verb": "GET",
        "url": "http://localhost:${rules_port}/lookups/currency"
      }
    },
    "customsOffices": {
      "sql": [
        "SELECT OfficeCode as code,",
        "OfficeName as name",
        "FROM OR_CustomsOffice WITH (NOLOCK)",
        "WHERE OfficeName NOT LIKE 'Unknown%'"
        ]
    },
    "holdcoCompanies": {
      "sql": [
        "SELECT AccountName as companyName,",
        "AccountNumber as accountNumber,",
        "RegistrationNumber as registrationNumber",
        "FROM OR_HOLDCO WITH (NOLOCK)"
        ]
    },
    "ihqCompanies": {
      "sql": [
        "SELECT CompanyName as companyName,",
        "Code as ihqCode,",
        "RegistrationNumber as registrationNumber",
        "FROM OR_IHQ WITH (NOLOCK)"
        ]
    },
    "moneyTransferAgents": {
      "sql": [
        "SELECT MoneyTransferAgentID as Agent",
        "FROM OR_MoneyTransferAgent WITH (NOLOCK)"
        ]
    },
    "mtaAccounts": {
      "sql": [
        "SELECT AccountNumber as accountNumber, ",
        "  MoneyTransferAgentID as MTA, ",
        "  rs.RulingsSection as rulingSection, ",
        "  ADLALevel as ADLALevel, ",
        "  CASE WHEN ADLAInd = 'Y' THEN 'true' ELSE 'false' END as isADLA ",
        "FROM OR_MTAAccount mta WITH (NOLOCK) ",
        "LEFT JOIN OR_RulingsSection rs on rs.RulingsSectionID =  mta.RulingSectionID"
      ]
    }
  }
}