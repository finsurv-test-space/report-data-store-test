{
  "datasource": {
    "jdbc.driverClassName" : "com.microsoft.sqlserver.jdbc.SQLServerDriver",
    "jdbc.url" : "jdbc:sqlserver://${txstreamNA.host};databaseName=${txstreamNA.database};sendStringParametersAsUnicode=false",
    "jdbc.username": "${txstreamNA.username}",
    "jdbc.password" : "${txstreamNA.password}"
  },
  "validations": {
    "Validate_LoanRef": {
      "endpoint": "loanRef",
      "parameters": [
        { "name": "loanReference", "path": "value" },
        { "name": "loanRef", "path": "money::LoanRefNumber" },
        { "name": "valueDate", "path": "transaction::ValueDate" },
        { "name": "REAccountNumber", "path": "transaction::Resident.Entity.AccountNumber" },
        { "name": "REAccountIdentifier", "path": "transaction::Resident.Entity.AccountIdentifier" },
        { "name": "RIAccountNumber", "path": "transaction::Resident.Individual.AccountNumber" },
        { "name": "RIAccountIdentifier", "path": "transaction::Resident.Individual.AccountIdentifier" }
      ],
      "sql": [
        "SELECT [LoanRefNumber],[Status],[CountryCode],[ValidFrom],[ValidTo],[AccountNumber],[ForeignCurrencyIndicator] ",
        "FROM OR_LoanReference WITH (NOLOCK)",
        "WHERE [LoanRefNumber] = '<#if (loanReference??)>${util.escapeSql(loanReference!'LOAN_REFERENCE_NOT_SUPPLIED')}<#else>${util.escapeSql(loanRef!'LOAN_REF_NOT_SUPPLIED')}</#if>'",
        "AND [Status] IN ('C', 'A')"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign accountNumber = (REAccountNumber??)?then(REAccountNumber!'', RIAccountNumber!'') >",
        "  <#assign accountIdentifier = (REAccountIdentifier??)?then(REAccountIdentifier!'', RIAccountIdentifier!'') >",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if !(row?? && row.LoanRefNumber?? && row.LoanRefNumber?has_content)>",
        "{status: 'fail', code: 374, message: 'Invalid loan reference number <#if (loanReference??)>${util.escapeSql(loanReference!'LOAN_REFERENCE_NOT_SUPPLIED')}<#else>${util.escapeSql(loanRef!'LOAN_REF_NOT_SUPPLIED')}</#if>'}",
        "  <#else>",
        "    <#if !(row.ForeignCurrencyIndicator??) && !(row.AccountNumber??)>",
        "      <#if ('FCA RESIDENT' == accountIdentifier || 'CFC RESIDENT' == accountIdentifier)>",
        "{status: 'fail', code: 'LRN', message: 'Loan Reference is valid, but is not registered as CFC. AccountIdentifier provided as ${accountIdentifier!''}'}",
        "      <#else>",
        "{status: 'pass'}",
        "      </#if>",
        "    <#else>",
        "      <#assign loanFC = (row.ForeignCurrencyIndicator??)?then(row.ForeignCurrencyIndicator,'') >",
        "      <#assign loanAcc = (row.AccountNumber??)?then(row.AccountNumber,'') >",
        "      <#if loanAcc?? && loanAcc?length gt 0 && loanAcc != accountNumber>",
        "{status: 'fail', code: 'LRN', message: 'Invalid loan reference and account number combination: <#if (loanReference??)>${util.escapeSql(loanReference!'LOAN_REFERENCE_NOT_SUPPLIED')}<#else>${util.escapeSql(loanRef!'LOAN_REF_NOT_SUPPLIED')}</#if> and ${accountNumber!''}'}",
        "      <#else>",
        "        <#if loanFC?? && loanFC?length gt 0 && loanFC == 'Y' && !('FCA RESIDENT' == accountIdentifier || 'CFC RESIDENT' == accountIdentifier)>",
        "{status: 'fail', code: 'LRN', message: 'Loan Reference is valid with CFC Indicator ${loanFC!''}, Resident Account Identifier is ${accountIdentifier!''}'}",
        "        <#else>",
        "          <#if loanFC?? && loanFC?length gt 0 && loanFC != 'Y' && ('FCA RESIDENT' == accountIdentifier || 'CFC RESIDENT' == accountIdentifier)>",
        "{status: 'fail', code: 'LRN', message: 'Loan Reference is valid, but is not flagged for CFC. Resident Account Identifier is  ${accountIdentifier!''}'}",
        "          <#else>",
        "{status: 'pass'}",
        "          </#if>",
        "        </#if>",
        "      </#if>",
        "    </#if>",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: 374, message: 'Invalid loan reference number <#if (loanReference??)>${util.escapeSql(loanReference!'LOAN_REFERENCE_NOT_SUPPLIED')}<#else>${util.escapeSql(loanRef!'LOAN_REF_NOT_SUPPLIED')}</#if>'}",
        "</#if>"
      ]
    },
    "Validate_ImportUndertakingCCN": {
      "endpoint": "importUndertakingCCN",
      "parameters": [
        { "name": "CCN", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT [LetterOfUndertaking] as LU FROM OR_CustomsClientNumber WITH (NOLOCK)",
        "WHERE [CCN] = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "AND ([ValidFrom] IS NULL OR [ValidFrom] <= '${util.escapeSql(valueDate)}')",
        "AND ([ValidTo] IS NULL OR [ValidTo] >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if row??>",
        "    <#if row.LU == 'Y'>",
        "{status: 'pass', message: 'The CCN is a registered import undertaking entity'}",
        "    <#else>",
        "{status: 'error', code: 323, message: 'The CCN is not a registered import undertaking entity'}",
        "    </#if>",
        "  <#else>",
        "{status: 'error', code: '323', message: 'The CCN is not a valid entry<#if valueDate??> for the date ${valueDate}</#if>'}",
        "  </#if>",
        "<#else>",
        "{status: 'error', code: 'NOF', message: 'CCN not found or is not valid as per start and end dates'}",
        "</#if>"
      ]
    },
    "Validate_ReplacementTrnReference": {
      "endpoint": "replacementTrnReference",
      "parameters": [
        { "name": "replacementTrnReference", "path": "ReplacementOriginalReference" }
      ],
      "sql": [
        "WITH cteCancelled AS (",
        "SELECT t.TrnReference, CASE WHEN qeD.EntryStateID IN (4 /*Acknowledged*/, 5 /*AssumedAcknowledged*/) THEN 'Acked' ELSE 'Wait' END DeleteState",
        " FROM OR_Transaction t WITH (NOLOCK)",
        " JOIN (OR_QueueEntry qeD WITH (NOLOCK)",
        "  JOIN OR_TransactionSnapshot tsD WITH (NOLOCK) ON tsD.SnapshotID = qeD.SnapshotID",
        " ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D'",
        " LEFT JOIN (OR_QueueEntry qeA WITH (NOLOCK)",
        "  JOIN OR_TransactionSnapshot tsA WITH (NOLOCK) ON tsA.SnapshotID = qeA.SnapshotID",
        " ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded",
        "WHERE t.TrnReference = '${util.escapeSql(replacementTrnReference)}' AND qeA.QueueEntryID IS NULL",
        "UNION ALL",
        " SELECT t.TrnReference, CASE WHEN qeD.EntryStateID IN (4 /*Acknowledged*/, 5 /*AssumedAcknowledged*/) THEN 'Acked' ELSE 'Wait' END DeleteState",
        " FROM archive.OR_Transaction t WITH (NOLOCK)",
        " JOIN (archive.OR_QueueEntry qeD WITH (NOLOCK) JOIN archive.OR_TransactionSnapshot tsD WITH (NOLOCK) ON tsD.SnapshotID = qeD.SnapshotID ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D'",
        " LEFT JOIN (archive.OR_QueueEntry qeA WITH (NOLOCK) JOIN archive.OR_TransactionSnapshot tsA WITH (NOLOCK) ON tsA.SnapshotID = qeA.SnapshotID ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded",
        " WHERE t.TrnReference = '${util.escapeSql(replacementTrnReference)}' AND qeA.QueueEntryID IS NULL)",
        "SELECT MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 /*NotReportable*/ THEN 'Y' WHEN c.TrnReference IS NOT NULL AND c.DeleteState = 'Acked' THEN 'Y' ELSE 'N' END) as Cancelled",
        " FROM OR_Transaction tx WITH (NOLOCK)",
        " LEFT JOIN cteCancelled c WITH (NOLOCK) ON c.TrnReference = tx.TrnReference",
        "WHERE tx.TrnReference = '${util.escapeSql(replacementTrnReference)}'",
        "GROUP BY tx.TrnReference",
        "UNION ALL",
        " SELECT MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 /*NotReportable*/ THEN 'Y' WHEN c.TrnReference IS NOT NULL AND c.DeleteState = 'Acked' THEN 'Y' ELSE 'N' END) as Cancelled",
        " FROM archive.OR_Transaction tx WITH (NOLOCK)",
        " LEFT JOIN cteCancelled c WITH (NOLOCK) ON c.TrnReference = tx.TrnReference WHERE tx.TrnReference = '${util.escapeSql(replacementTrnReference)}'",
        " GROUP BY tx.TrnReference"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if row??>",
        "    <#if row.Cancelled == 'Y'>",
        "{status: 'pass'}",
        "    <#else>",
        "{status: 'error', code: 211, message: '${util.escapeJavaScript(replacementTrnReference)} has not yet been cancelled (Original transaction must first be cancelled)'}",
        "    </#if>",
        "  <#else>",
        "{status: 'error', code: '211', message: '${util.escapeJavaScript(replacementTrnReference)} in not an existing transaction'}",
        "  </#if>",
        "<#else>",
        "{status: 'error', code: 'NOF', message: 'Transaction not found'}",
        "</#if>"
      ]
    },
    "Validate_ReversalTrnRef": {
      "endpoint": "reversalTrnRef",
      "parameters": [
        { "name": "reversalTrnRef", "path": "value" },
        { "name": "trnReference", "path": "transaction::TrnReference" },
        { "name": "flowCurrency", "path": "transaction::FlowCurrency" },
        { "name": "flow", "path": "transaction::Flow" },
        { "name": "reversalTrnSeq", "path": "money::ReversalTrnSeqNumber" },
        { "name": "sequence", "path": "money::ReversalTrnSeqNumber" },
        { "name": "foreignValue", "path": "money::ForeignValue" },
        { "name": "category", "path": "money::CategoryCode" }
      ],
      "sql": [
        "SELECT TOP 1 *",
        "FROM (",
        "SELECT ",
        "       t.CurrencyCode, ",
        "       ma.ForeignValue, ",
        "       ma.CategoryCode,",
        "       <#if flowCurrency??>CASE WHEN t.CurrencyCode = '${util.escapeSql(flowCurrency)}' THEN 'SAME' ELSE 'DIFFER' END<#else>'UNKNOWN'</#if> as CurrencyCheck,",
        "       <#if foreignValue??>CASE WHEN ma.ForeignValue >= ${util.escapeSql(foreignValue)} THEN 'SAMEORLESS' ELSE 'GREATER' END<#else>'UNKNOWN'</#if> as ForeignValueCheck,",
        "       CASE WHEN ma.SequenceNumber <#if reversalTrnSeq??>= ${util.escapeSql(reversalTrnSeq)}<#else><#if sequence??>= ${util.escapeSql(sequence)}<#else>IS NULL</#if></#if> THEN 'SAME' ELSE 'DIFFER' END as SequenceNumberCheck,",
        "       CASE WHEN SUBSTRING(t.Flow,1,1) <#if flow??>= SUBSTRING('${util.escapeSql(flow)}',1,1)<#else>IS NULL</#if> THEN 'SF' ELSE 'OF' END as Flow,",
        "       CASE WHEN SUBSTRING(ma.CategoryCode, 1, 1) = SUBSTRING('${util.escapeSql(category)}', 1,1) THEN 'SC' ELSE 'OC' END as Category,",
        "       CASE WHEN t.StateID = 7/*Reported*/ THEN 'R' ELSE 'N' END as Reported,",
        "       ma.CategoryCode as OldCategory,",
        "       ABS(ma.SequenceNumber - ${util.escapeSql(reversalTrnSeq)}) as SequenceDiff",
        "FROM OR_Transaction t WITH (NOLOCK)",
        "LEFT JOIN OR_MonetaryAmount ma WITH (NOLOCK) ",
        "ON ma.TrnReference = t.TrnReference",
        "WHERE t.TrnReference = <#if reversalTrnRef??>'${util.escapeSql(reversalTrnRef)}'<#else>'${util.escapeSql(trnReference)}'</#if>",
        "UNION ALL",
        "SELECT ",
        "       t.CurrencyCode, ",
        "       ma.ForeignValue, ",
        "       ma.CategoryCode,",
        "       <#if flowCurrency??>CASE WHEN t.CurrencyCode = '${util.escapeSql(flowCurrency)}' THEN 'SAME' ELSE 'DIFFER' END<#else>'UNKNOWN'</#if> as CurrencyCheck,",
        "       <#if foreignValue??>CASE WHEN ma.ForeignValue >= ${util.escapeSql(foreignValue)} THEN 'SAMEORLESS' ELSE 'GREATER' END<#else>'UNKNOWN'</#if> as ForeignValueCheck,",
        "       CASE WHEN ma.SequenceNumber <#if reversalTrnSeq??>= ${util.escapeSql(reversalTrnSeq)}<#else><#if sequence??>= ${util.escapeSql(sequence)}<#else>IS NULL</#if></#if> THEN 'SAME' ELSE 'DIFFER' END as SequenceNumberCheck,",
        "       CASE WHEN SUBSTRING(t.Flow,1,1) <#if flow??>= SUBSTRING('${util.escapeSql(flow)}',1,1)<#else>IS NULL</#if> THEN 'SF' ELSE 'OF' END as Flow,",
        "       CASE WHEN SUBSTRING(ma.CategoryCode, 1, 1) = SUBSTRING('${util.escapeSql(category)}', 1,1) THEN 'SC' ELSE 'OC' END as Category,",
        "       CASE WHEN t.StateID = 7/*Reported*/ THEN 'R' ELSE 'N' END as Reported,",
        "       ma.CategoryCode as OldCategory,",
        "       ABS(ma.SequenceNumber - ${util.escapeSql(reversalTrnSeq)}) as SequenceDiff",
        "FROM archive.OR_Transaction t WITH (NOLOCK)",
        "LEFT JOIN archive.OR_MonetaryAmount ma WITH (NOLOCK) ",
        "ON ma.TrnReference = t.TrnReference",
        "WHERE t.TrnReference = <#if reversalTrnRef??>'${util.escapeSql(reversalTrnRef)}'<#else>'${util.escapeSql(trnReference)}'</#if>",
        ") AS x",
        "ORDER BY ",
        "x.SequenceDiff asc,",
        "x.SequenceNumberCheck desc,",
        "x.ForeignValueCheck desc,",
        "x.CurrencyCheck desc,",
        "x.Flow,",
        "x.Category"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if row??>",
        "    <#if row.CategoryCode??>",
        "      <#assign reversal = row.CategoryCode?ends_with('00') >",
        "      <#if reversal>",
        "        {status: 'fail', code: 'E02', message: 'Original transaction is a reversal. Cannot reverse a reversal'}",
        "      <#else>",
        "        <#if row.CurrencyCheck == 'DIFFER'>",
        "          {status: 'fail', code: 'W01', message: 'Reversal currency is different from the original transaction.'}",
        "        <#else>",
        "          <#if row.SequenceNumberCheck == 'DIFFER'>",
        "            {status: 'fail', code: '410', message: 'Original transaction SequenceNumber not stored on database'}",
        "          <#else>",
        "            <#if row.ForeignValueCheck == 'GREATER'>",
        "              {status: 'fail', code: 'W02', message: 'Reversal amount is greater than the amount on the original transaction sequence'}",
        "            <#else>",
        "              <#if row.Flow == 'SF'>",
        "                {status: 'fail', code: 410, message: 'Original transaction and SequenceNumber combination stored on database, but not with an opposite flow'}",
        "              <#else>",
        "                <#if row.Category == 'OC'>",
        "                  {status: 'fail', code: 411, message: 'Incorrect reversal category used with original transaction category. Original category is ${row.CategoryCode}'}",
        "                <#else>",
        "                  <#if row.Reported == 'N'>",
        "                    {status: 'fail', code: 410, message: 'Original transaction stored on database, but has not been reported to the SARB'}",
        "                  <#else>",
        "                    <#if row.CurrencyCheck == 'UNKNOWN'>",
        "                      {status: 'error', code: 'W04', message: 'Currency not supplied to validation call - may differ between original and reversal transactions'}",
        "                    <#else>",
        "                      <#if row.ForeignValueCheck == 'UNKNOWN'>",
        "                        {status: 'error', code: 'W05', message: 'Value not supplied to validation call - may differ between original and reversal transactions'}",
        "                      <#else>",
        "                        {status: 'pass'}",
        "                      </#if>",
        "                    </#if>",
        "                  </#if>",
        "                </#if>",
        "              </#if>",
        "            </#if>",
        "          </#if>",
        "        </#if>",
        "      </#if>",
        "    <#else>",
        "      {status: 'fail', code: '410', message: 'Original transaction SequenceNumber not stored on database'}",
        "    </#if>",
        "  <#else>",
        "    {status: 'fail', code: '410', message: 'Original transaction not stored on database'}",
        "  </#if>",
        "<#else>",
        "  {status: 'fail', code: '410', message: 'Original transaction not stored on database'}",
        "</#if>"
      ]
    },
    "Validate_ValidCCN": {
      "endpoint": "validCCN",
      "parameters": [
        { "name": "CCN", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT CASE WHEN AuthBy IS NOT NULL THEN 'Y' ELSE 'N' END as Auth, CASE WHEN LetterOfUndertaking = 'Y' THEN 'Y' ELSE 'N' END as LU, CCN",
        " FROM OR_CustomsClientNumber WITH (NOLOCK)",
        " WHERE CCN = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "   AND (ValidFrom IS NULL OR ValidFrom <= '${util.escapeSql(valueDate)}')",
        "   AND (ValidTo IS NULL OR ValidTo >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if row??>",
        "    <#if row.Auth == 'Y' && row.LU == 'N'>",
        "{status: 'pass'}",
        "    <#elseif row.Auth == 'Y' && row.LU == 'Y'>",
        "{status: 'pass', code: 200, message: 'Is LU Client'}",
        "    <#elseif row.Auth == 'N'>",
        "{status: 'error', code: 322, message: 'Not authorised as a registered customs client number'}",
        "    </#if>",
        "<#else>",
        "    <#if logic.isValidCCN(CCN)>",
        "{status: 'pass'}",
        "    <#else>",
        "{status: 'error', code: '322', message: 'Not a valid customs client number'}",
        "    </#if>",
        "  </#if>",
        "<#else>",
        "{status: 'error', code: 'NOF', message: 'Customs client number not found'}",
        "</#if>"
      ]
    },
    "Validate_ValidCCNinUCR": {
      "endpoint": "validCCNinUCR",
      "parameters": [
        { "name": "UCR", "path": "value" },
        { "name": "valueDate", "path": "transaction::ValueDate" }
      ],
      "sql": [
        "SELECT CASE WHEN AuthBy IS NOT NULL THEN 'A' ELSE 'N' END as Auth, CASE WHEN LetterOfUndertaking = 'Y' THEN 'Y' ELSE 'N' END as LU, CCN",
        " FROM OR_CustomsClientNumber WITH (NOLOCK)",
        " WHERE CCN = '${util.escapeSql(CCN)}' <#if valueDate??>",
        "   AND (ValidFrom IS NULL OR ValidFrom <= '${util.escapeSql(valueDate)}')",
        "   AND (ValidTo IS NULL OR ValidTo >= '${util.escapeSql(valueDate)}')</#if>"
      ],
      "compose": [
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#if row??>",
        "    <#if (row.Auth == 'Y' || row.Auth == 'A') && row.LU == 'N'>",
        "{status: 'pass'}",
        "    <#elseif (row.Auth == 'Y' || row.Auth == 'A') && row.LU == 'Y'>",
        "{status: 'pass', code: 200, message: 'Is LU Client'}",
        "    <#else>",
        "{status: 'error', code: 322, message: 'Not authorised as a registered customs client number'}",
        "    </#if>",
        "<#else>",
        "    <#if logic.isValidCCN(CCN)>",
        "{status: 'pass'}",
        "    <#else>",
        "{status: 'error', code: '322', message: 'Not a valid customs client number'}",
        "    </#if>",
        "  </#if>",
        "<#else>",
        "    <#if logic.isValidCCN(CCN)>",
        "{status: 'pass'}",
        "    <#else>",
        "{status: 'error', code: '322', message: 'Not a valid customs client number'}",
        "    </#if>",
        "</#if>"
      ]
    },
    "Validate_IndividualSDA": {
      "endpoint": "withinSDALimit",
      "parameters": [
        { "name": "idNumber", "path": "value" },
        { "name": "trnRef", "path": "transaction::TrnReference" },
        { "name": "valueDate", "path": "transaction::ValueDate" },
        { "name": "localAmountForIDNumber", "path": "function::localAmountForIDNumber" }
      ],
      "sql": [
        "SELECT dbo.SDA_CALC('${util.escapeSql(trnRef)}', '${util.escapeSql(valueDate)}', '${util.escapeSql(idNumber)}') AS 'TotalValueForYear'"
      ],
      "compose": [
        "<#setting locale='en_US'>",
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)?? && util.sqlFirstRowDeserialize(response).TotalValueForYear??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#assign sdaTotal = ((row.TotalValueForYear?replace(',', '')?replace(' ', '')?number) + (localAmountForIDNumber?number))?replace(',', '')?replace(' ', '')?number >",
        "  <#if (sdaTotal gt 1000000)>",
        "{status: 'fail', code: 'DAL', message: 'You have exceeded the discretionary amount limit of R1,000,000 (${util.escapeSql(idNumber)} used ${row.TotalValueForYear?string(',##0.00')} during ${util.escapeSql(valueDate[0..3])} - Total: ${sdaTotal?string(',##0.00')})'}",
        "  <#else>",
        "{status: 'pass'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: 'NOF', message: 'Failed to acquire SDA balance'}",
        "</#if>"
      ]
    },
    "Validate_IndividualFIA": {
      "endpoint": "withinFIALimit",
      "parameters": [
        { "name": "idNumber", "path": "value" },
        { "name": "trnRef", "path": "transaction::TrnReference" },
        { "name": "valueDate", "path": "transaction::ValueDate" },
        { "name": "localAmountForIDNumberFIA", "path": "function::localAmountForIDNumberFIA" }
      ],
      "sql": [
        "SELECT",
        "  dbo.FIALIMIT_CALC('${util.escapeSql(idNumber)}', '${util.escapeSql(valueDate)}') AS 'FiaLimit',",
        "  dbo.FIA_CALC('${util.escapeSql(trnRef)}', '${util.escapeSql(valueDate)}', '${util.escapeSql(idNumber)}') AS 'TotalValueForYear'"
      ],
      "compose": [
        "<#setting locale='en_US'>",
        "<#if response?? && response?size gt 0 && response[0]?? && util.sqlFirstRowDeserialize(response)?? && util.sqlFirstRowDeserialize(response).TotalValueForYear??>",
        "  <#assign row = util.sqlFirstRowDeserialize(response) >",
        "  <#assign FIATotal = ((row.TotalValueForYear?replace(',', '')?replace(' ', '')?number) + (localAmountForIDNumberFIA?number))?replace(',', '')?replace(' ', '')?number >",
        "  <#assign FIALimit = (row.FiaLimit?replace(',', '')?replace(' ', '')?number)>",
        "  <#if (FIATotal gt FIALimit)>",
        "{status: 'fail', code: 'FIAL', message: 'You have exceeded the FIA amount limit of R${FIALimit?string(',##0.00')} (for ${idNumber} used during ${valueDate[0..3]} of ${(FIATotal - FIALimit)?string(',##0.00')})'}",
        "  <#else>",
        "{status: 'pass'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: 'NOF', message: 'Failed to acquire FIA balance'}",
        "</#if>"
      ]
    }
  },
  "lookups": {
    "adhocSubjects": {
      "sql": [
        "SELECT AdhocSubject as Subject",
        "FROM OR_AdhocSubject WITH (NOLOCK)",
        "WHERE AdhocSubject <> 'IHQnnn'",
        "UNION",
        "SELECT Code as Subject",
        "FROM OR_IHQ WITH (NOLOCK)"
      ]
    },
    "branches": {
      "sql": [
        "SELECT BranchCode as code, BranchName as name, HubCode as hubCode, HubName as hubName,",
        "ISNULL(ValidFrom, '2000-01-01') as validFrom, ValidTo as validTo",
        "FROM OR_Branch WITH (NOLOCK)",
        "WHERE BranchName NOT LIKE 'Unknown%'"
      ]
    },
    "categories": {
      "sql": [
        "SELECT CASE WHEN Flow = 'I' THEN 'IN' ELSE 'OUT' END as flow,",
        "CategoryCode as code,",
        "Section as section,",
        "SubSection as subsection,",
        "Description as description,",
        "OldCodes as oldcodes",
        "FROM OR_Category WITH (NOLOCK)",
        "WHERE [Description] NOT LIKE 'Unknown%' AND [Version] = 3"
      ]
    },
    "countries": {
      "sql": [
        "SELECT CountryCode, Name",
        "FROM OR_Country WITH (NOLOCK)",
        "WHERE Name NOT LIKE 'Unknown%'"
      ],
      "compose": [
        "<#assign rows = util.sqlDeserialize(response)>",
        "[",
        "<#list rows as row>",
        "  {code: '${row.CountryCode}', name: '${util.escapeJavaScript(row.Name)}'}<#if row_has_next>,</#if>",
        "</#list>",
        "]"
      ]
    },
    "currencies": {
      "sql": [
        "SELECT CurrencyCode as code,",
        "Name as name",
        "FROM OR_Currency WITH (NOLOCK)",
        "WHERE Name NOT LIKE 'Unknown%'"
      ]
    },
    "customsOffices": {
      "sql": [
        "SELECT OfficeCode as code,",
        "OfficeName as name",
        "FROM OR_CustomsOffice WITH (NOLOCK)",
        "WHERE OfficeName NOT LIKE 'Unknown%'"
      ]
    },
    "luClientCCNs": {
      "sql": [
        "SELECT [CCN] AS ccn, [AccountName] AS accountName, [AccountNumber] AS accountNumber",
        "FROM [OR_CustomsClientNumber]",
        "WHERE [LetterOfUndertaking] = 'Y'"
      ]
    },
    "holdcoCompanies": {
      "sql": [
        "SELECT AccountName as companyName,",
        "AccountNumber as accountNumber,",
        "RegistrationNumber as registrationNumber",
        "FROM OR_HOLDCO WITH (NOLOCK)"
      ]
    },
    "ihqCompanies": {
      "sql": [
        "SELECT CompanyName as companyName,",
        "Code as ihqCode,",
        "RegistrationNumber as registrationNumber",
        "FROM OR_IHQ WITH (NOLOCK)"
      ]
    },
    "moneyTransferAgents": {
      "sql": [
        "SELECT MoneyTransferAgentID as Agent",
        "FROM OR_MoneyTransferAgent WITH (NOLOCK)"
      ]
    },
    "mtaAccounts": {
      "sql": [
        "SELECT AccountNumber as accountNumber, ",
        "  MoneyTransferAgentID as MTA, ",
        "  rs.RulingsSection as rulingSection, ",
        "  ADLALevel as ADLALevel, ",
        "  CASE WHEN ADLAInd = 'Y' THEN 'true' ELSE 'false' END as isADLA ",
        "FROM OR_MTAAccount mta WITH (NOLOCK) ",
        "LEFT JOIN OR_RulingsSection rs on rs.RulingsSectionID =  mta.RulingSectionID"
      ]
    }
  }
}