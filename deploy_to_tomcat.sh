#!/bin/bash
ssh -i /app/finsurv-test.pem ec2-user@ec2-34-244-227-49.eu-west-1.compute.amazonaws.com "sudo ${DIR_TOMCAT}/bin/shutdown.sh"
ssh -i /app/finsurv-test.pem ec2-user@ec2-34-244-227-49.eu-west-1.compute.amazonaws.com "sudo rm -rf ${DIR_TOMCAT}/webapps_frontend/report-data-store/* ${DIR_TOMCAT}/webapps_frontend/report-data-store.war ${DIR_TOMCAT}/logs/report-data-store.log"
 
ssh -i /app/finsurv-test.pem ec2-user@ec2-34-244-227-49.eu-west-1.compute.amazonaws.com "sudo cp %teamcity.build.checkoutDir%/build/libs/report-data-store.war ${DIR_TOMCAT}/webapps_frontend"
 
ssh -i /app/finsurv-test.pem ec2-user@ec2-34-244-227-49.eu-west-1.compute.amazonaws.com "sudo ${DIR_TOMCAT}/bin/startup.sh"