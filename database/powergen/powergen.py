import re,os,sys,zipfile,traceback,os.path
import settings,powerschema,clxml,powerdodl


def outTemplate(dir,fname,template, list):        
    from Cheetah.Template import Template
    fn = os.path.join(dir,str(Template(fname,searchList=list)))
    print 'Generating',fn
    tclass = Template.compile(file='../powergen/'+template)
    t = tclass(searchList=list)
    #print dir(t)
    try:
        print >>file(str(fn),'w'), str(t)
    except Exception,v:
        tb = traceback.extract_tb(sys.exc_info()[2])[-1]
        print 'Exception:',v
        print 'Last line of the stack trace pointed to the following generated code lines:'
        print '--->'+'\n--->'.join(t.generatedModuleCode().split('\n')[tb[1]-2:tb[1]+2])
        print >>file(str(fn)+'.genned.py','w'),t.generatedModuleCode()
        raise

def generate():
    tables,references,views = powerschema.loadSchema()
    dos = powerdodl.loadDos(settings.dodl_files)
    for dir,outname,template in settings.global_template_list:
        outTemplate(dir,outname,template,[locals(),{'getOptions':settings.getOptions}])
    for table in tables:
        if not table.options.has_key('SkipGeneration') or table.options['SkipGeneration'] == False:
            for dir,outname,template in settings.table_template_list:
                outTemplate(dir,outname,template,[{'table':table},{'getOptions':settings.getOptions}])
        else:
            print 'Skip Generation: ' + table.name


    
def main(options_file):
    from optparse import OptionParser

    parser = OptionParser(usage="usage: %prog [options] gen|printsettings")
    parser.add_option("-s", "--settings", dest="settings",
                  help="Use FILE as the settings file", default=options_file, metavar="FILE")
    parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="Don't print status messages to stdout")

    options, args = parser.parse_args()
#    print 'Options',options
#    print 'Args',args

    settings.load(options.settings)
    
    try:
        if len(args) == 0 or args[0].lower() == 'gen':
            generate()
        elif args[0].lower() == 'printsettings':
            settings.print_settings(args[1:])
        else:
            print 'Unknown command. Sorry for you.'
    except EnvironmentError,v:
        print v
    except str,v:
        print v

    raw_input('-'*40+'\nDone\n')
    
if __name__=='__main__':
    main()


