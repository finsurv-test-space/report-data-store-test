﻿<%
import settings
def typeMapping(column):
    type = column.type
    if type.find('(') > 0:
        type = type[:type.find('(')]
    type = type.strip().lower()
    r = { 
      'varchar'	:'String'
     ,'int'		:'Int32'
     ,'datetime':'DateTime'
     ,'bit'		:'Boolean'
     ,'text'	:'String'
     ,'char'	:'String'
     ,'integer' :'Int32'
     ,'uniqueidentifier' :'Int32'
     ,'smallint':'Int16'
     ,'float'   :'Double'
     ,'money'   :'Decimal'
     ,'decimal'   :'Decimal'
    }[type]
    if column.nullable and r in ['DateTime','Int16','Int32','Boolean','Double','Decimal']:
        r += '?'
    return r
    
    
def nullable(column):
    return column.nullable and (column.type=='int' or column.type=='decimal' or column.type=='datetime' )
    
def specialFieldName(column,from_where):
    if nullable(column):
        return 'F'+column.pascalName+'Specified ? '+from_where+'.'+column.pascalName+' : new '+typeMapping(column)+'()'
    else:
        return column.pascalName


%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Codegen.pdb;
using System.IO;
using Microsoft.Practices.CompositeUI;

using System.Text.RegularExpressions;

namespace Codegen.DODL
{
  public class InternalDOList<T> : IList<T> where T : DomainObject
  {
    private List<T> innerList = new List<T>();
    private List<T> deadList = new List<T>();
    private DomainObject parent;

    public List<T> DeadList { get { return deadList; } }
    
    public InternalDOList(DomainObject parent)
    {
      this.parent = parent;
    }

    
    public int IndexOf(T item)
    {
      return innerList.IndexOf(item);
    }

    public void Insert(int index, T item)
    {
      innerList.Insert(index, item);
      parent.IsModified = true;
    }

    public void RemoveAt(int index)
    {
      T obj = innerList[index];
      innerList.RemoveAt(index);
      if (!obj.IsNew)
      {
        deadList.Add(obj);
        obj.State = DomainObject.StateEnum.Deleted;
      }
      parent.IsModified = true;
    }

    public T this[int index]
    {
      get
      {
        return innerList[index];
      }
      set
      {
        innerList[index] = value;
        parent.IsModified = true;
      }
    }
    

    
    public void Add(T item)
    {
      innerList.Add(item);
      item.Parent = parent;
      parent.IsModified = true;
    }
    internal void _InternalAdd(T item)
    {
      innerList.Add(item);
      item.Parent = parent;
    }

    public void Clear()
    {
      innerList.ForEach(x => x.State = DomainObject.StateEnum.Deleted);
      deadList.AddRange(innerList);
      innerList.Clear();
      parent.IsModified = true;
    }

    public bool Contains(T item)
    {
      return innerList.Contains(item);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
      innerList.CopyTo(array, arrayIndex);
    }

    public int Count
    {
      get { return innerList.Count; }
    }

    public bool IsReadOnly
    {
      get { return false; }
    }

    public bool Remove(T item)
    {
      if (innerList.Remove(item))
      {
        if (!item.IsNew)
        {
          deadList.Add(item);
          item.State = DomainObject.StateEnum.Deleted;
        }
        parent.IsModified = true;
        return true;
      }
      return false;
    }
    

    
    public IEnumerator<T> GetEnumerator()
    {
      return innerList.GetEnumerator();
    }
    

    
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return innerList.GetEnumerator();
    }
    
  }
  public abstract class DomainObject:IDomainObject
  {
    public CSDODL service;
    public enum StateEnum { New, NotLoaded, Main, Modified, Deleted }
    public abstract StateEnum State
    {
        get;
        set;
    }

    abstract public void ForceLoad();
    abstract public void Save();
    abstract public void Delete();
    abstract public void Restore();
    abstract protected void Validate();

    protected DomainObject parent;
    public DomainObject Parent
    {
        get { return parent; }
        set { parent = value; }
    }
    
    protected List<String> errors = new List<string>();
    public List<String> Errors
    {
      get { Validate(); return errors; }
    }

    public bool IsValid
    {
      get { Validate(); return errors.Count==0; }
    }

    public virtual bool IsNew
    {
      get { return State == StateEnum.New; }
    }

    public virtual bool IsModified
    {
      set
      {
        if (value == false)
            throw new Exception("Use Save or Restore to change the modified state to false");
        if (parent != null)
            parent.IsModified = true;
        //Validate();
        if (!IsNew)
          State = StateEnum.Modified;
        NotifyUpdateDelegates(UpdateTypeEnum.Updated);
      }
      get { return State == StateEnum.Modified; }
    }



    public enum UpdateTypeEnum { Created, Saved, Updated, Deleted }
    public delegate void UpdateHandler(DomainObject sender,UpdateTypeEnum updateType);
    public event UpdateHandler OnUpdate=null;
    
    protected void NotifyUpdateDelegates(UpdateTypeEnum updateType)
    {
      if (OnUpdate != null)
      {
        OnUpdate(this,updateType);
      }
    }

    public virtual String Name
    {
      set { }
      get { return "Name not defined"; }
    }
  }

#for $do in $dos
  #if do.schema.name != 'EmptyTable'
  #set $summaryFields=$do._summary_fields
  public partial class $(do.name)SummaryDef
  {
    public $(do.name)SummaryDef($(do.name)SummaryDef copyFrom, bool newCopy)
    {
    #for $field in $summaryFields 
        $field.pascalName = #slurp
      #if $field == $do.schema.primaryKey.columns[0]        
        newCopy ? -1 : copyFrom.$field.pascalName;
      #else
        copyFrom.$field.pascalName;
      #end if
    #end for
    }
  }
  #end if
  public partial class $(do.name)Def
  {
##  #if do.schema.name != 'EmptyTable'
##//    public $(do.name)Def()
##//    {
##    #for $field in $do.schema.columns
##      #if ($field.outgoingRef and $field.type.lower() == 'int') or $field == $do.schema.primaryKey.columns[0]
##//      $field.name = -1;
##      #end if
##    #end for
##//    }
##  #end if
    public $(do.name)Def($(do.name)Def copyFrom, bool newCopy)
    {
    #for $field in $do.get_all_basic_columns
        $field.pascalName = #if $field == $do.schema.primaryKey.columns[0]#newCopy ? -1 : copyFrom.$field.pascalName#else#copyFrom.$field.pascalName#end if#;
    #end for
        UpdateStatus = newCopy ? Codegen.DODL.DomainObject.StateEnum.New.ToString() : copyFrom.UpdateStatus;
    #for $field in $do.do_internal_list_fields
        $field.name = new List<$(field.target_name)Def>();
        if(copyFrom.$field.name != null)
          foreach (var item in copyFrom.$field.name)
            $(field.name).Add(new $(field.target_name)Def(item,newCopy));
    #end for
    #for $field in $do.do_external_list_fields
        $field.name = new List<$(field.target_name)SummaryDef>();
        if(copyFrom.$field.name != null)
            foreach (var item in copyFrom.$field.name)
            $(field.name).Add(new $(field.target_name)SummaryDef(item,false));
    #end for
    #for $field in $do.do_external_ref_fields
        $field.name = new $(field.target_name)SummaryDef(copyFrom.$field.name,false); // False for newCopy since these are external refs
    #end for
    #for $field in $do.do_internal_ref_fields
        $field.name = new $(field.target_name)Def(copyFrom.$field.name,newCopy); 
    #end for
    #for $field in $do.do_transient_list_fields
        $field.name = new List<$(field.target_name)Def>();
        if(copyFrom.$field.name != null)
          foreach (var item in copyFrom.$field.name)
            $(field.name).Add(new $(field.target_name)Def(item,newCopy));
    #end for
    }
  }
  
  public partial class $do.name : DomainObject
  {
    protected $(do.name)Def oldDefinition;
    protected $(do.name)Def _internalDefinition;
    #if do.schema.name != 'EmptyTable'
    protected $(do.name)SummaryDef internalSummaryDefinition;
    #end if
    
    protected $(do.name)Def internalDefinition
    {
        get { return _internalDefinition; }
        set 
        {
            _internalDefinition = value;
            if (_internalDefinition != null)
            {
                #for $field in $do.do_internal_list_fields
                _$field.name = new InternalDOList<$(field.target_name)>(this);
                if(_internalDefinition.$field.name != null)
                  foreach (var item in _internalDefinition.$field.name)
                    _$(field.name)._InternalAdd(new $(field.target_name)(item,this));
                #end for
                #for $field in $do.do_external_list_fields
                _$field.name = new List<$(field.target_name)>();
                if(_internalDefinition.$field.name != null) 
                  foreach (var item in _internalDefinition.$field.name)
                    _$(field.name).Add(new $(field.target_name)(item,service));
                #end for
                #for $field in $do.do_external_ref_fields
                _$field.name = new $(field.target_name)(_internalDefinition.$field.name,service);
                #end for
                #for $field in $do.do_internal_ref_fields
                _$field.name = new $(field.target_name)(_internalDefinition.$field.name,this);
                #end for
                #for $field in $do.do_transient_list_fields
                _$field.name = new List<$(field.target_name)>();
                if(_internalDefinition.$field.name != null) 
                  foreach (var item in _internalDefinition.$field.name)
                    _$(field.name).Add(new $(field.target_name)(item));
                #end for
                oldDefinition = new $(do.name)Def(internalDefinition,false);
                #if do.schema.name != 'EmptyTable'
                internalSummaryDefinition = null;
                #end if
            } 
            else
                oldDefinition = null;
        }
    }
    
    private StateEnum _state;
    public override StateEnum State
    {
        get { return _state; }
        set 
        {   
            _state = value; 
            if (_internalDefinition != null)
                _internalDefinition.UpdateStatus = value.ToString();
        }
    }
    
##//    protected CSDODL service;
##//    [ServiceDependency]
##//    public CSDODL Service
##//    {
##//      set { service = value; }
##//      get { return service; }
##//    }

    #if $do.schema.name != 'EmptyTable'
    // Create a new DO with a new internalDef with new status and -1 ID's
    //
    public $(do.name)(CSDODL service)
    {
      this.service = service;
      var tempInternalDefinition = new $(do.name)Def()#if $do.schema.primaryKey.columns[0].type=='int'#{ $do.schema.primaryKey.columns[0].pascalName = -1}#end if#;
      #for $field in $do.do_internal_ref_fields
      tempInternalDefinition.$(field.name).UpdateStatus = "New";
      #end for
    ##  #if do.schema.name != 'EmptyTable'
    ##    #for $field in $do.get_all_basic_columns
    ##        #if ($field.outgoingRef and $field.type.lower() == 'int') or $field == $do.schema.primaryKey.columns[0]
    ##  tempInternalDefinition.$field.name = -1;
    ##        #end if
    ##    #end for
    ##  #end if

      internalDefinition = tempInternalDefinition;
      State = StateEnum.New;
      Validate();
    }
    // Create a not loaded yet DO from a Summary, ready to load whenever non summary fields are accessed
    //
    public $(do.name)($(do.name)SummaryDef summary, DomainObject parent)
    {
      this.parent = parent;
      this.service = parent.service;
      internalDefinition = null;
      internalSummaryDefinition = summary;
      State = StateEnum.NotLoaded;
    }
    // Create a not loaded yet DO from a Summary, ready to load whenever non summary fields are accessed
    //
    public $(do.name)($(do.name)SummaryDef summary, CSDODL service)
    {
      this.service = service;
      internalDefinition = null;
      internalSummaryDefinition = summary;
      State = StateEnum.NotLoaded;
    }
    #else
    // Create an unloaded DO without have to talk to the backend since it has no schema table
    //
    public $(do.name)(CSDODL service)
    {
      this.service = service;
      internalDefinition = null;
      State = StateEnum.NotLoaded;
    }
    #end if


    // Create a  loaded  DO from a Full Def
    //
    public $(do.name)($(do.name)Def fullDef, DomainObject parent)
    {
      this.parent = parent;
      this.service = parent.service;
      internalDefinition = fullDef;
      State = (StateEnum)Enum.Parse(typeof(StateEnum), fullDef.UpdateStatus);
    }
    // Create a  loaded  DO from a Full Def
    //
    public $(do.name)($(do.name)Def fullDef, CSDODL service)
    {
      this.service = service;
      internalDefinition = fullDef;
      State = (StateEnum)Enum.Parse(typeof(StateEnum), fullDef.UpdateStatus);
    }

    // Create a new copy of the sourceDO, reset the internally managed item's ID to -1 and set their status to new
    //
    public $(do.name)($do.name sourceDO, CSDODL service)
    {
      this.service = service;
      internalDefinition = new $(do.name)Def(sourceDO.internalDefinition,true);
      State = StateEnum.New;
      Validate();
    }

    // If not loaded yet, use the summary object to fetch from the backend and populate the lists
    //
    private void Load()
    {
      if (State == StateEnum.NotLoaded)
      {
    #if $do.schema.name == 'EmptyTable'
        internalDefinition = service.Get$(do.name)();
    #else
        internalDefinition = service.Get$(do.name)(internalSummaryDefinition.$do.schema.primaryKey.columns[0].name);
    #end if
        State = StateEnum.Main;
      }
    }
    // Force a load from the backend, used for refreshes
    //
    override public void ForceLoad()
    {
    #if $do.schema.name == 'EmptyTable'
      internalDefinition = service.Get$(do.name)();
    #else
      internalDefinition = service.Get$(do.name)(State == StateEnum.NotLoaded ? internalSummaryDefinition.$do.schema.primaryKey.columns[0].name : internalDefinition.$do.schema.primaryKey.columns[0].name);
    #end if
      State = StateEnum.Main;
    }
    
    // The DO's store their data in their backing internalDefinition structs. However the list structure of these defs can go out of sync when one add new DO's to an internally managed list.
    // Before we save the internalDef to the backend we need to make sure the internalDef structure looks the same as the the DO list structure
    //
    internal void SynchroniseDOWithDef(List<$(do.name)Def> parentList)
    {
        if (parentList != null)
            parentList.Add(_internalDefinition);
                
        #for $field in $do.do_internal_list_fields
        _internalDefinition.$field.name = new List<$(field.target_name)Def>();
        foreach(var domainObject in _$field.name)
            domainObject.SynchroniseDOWithDef(_internalDefinition.$(field.name));
        foreach(var domainObject in _$(field.name).DeadList)
            domainObject.SynchroniseDOWithDef(_internalDefinition.$(field.name));
        #end for
        #for $field in $do.do_internal_ref_fields
        _$(field.name).SynchroniseDOWithDef(null);
        #end for
    }
    
    // Immediate save to the back-end
    //
    public override void Save()
    {
    ##if $do.schema.name != 'EmptyTable'
      if (State == StateEnum.NotLoaded)
        throw new Exception("Cannot save an unloaded DomainObject");

      if(!IsValid)
        throw new Exception("Cannot save non-valid object.");
        
      SynchroniseDOWithDef(null);
      internalDefinition = service.Save$(do.name)(internalDefinition);
      State = StateEnum.Main;

      // The backend may update anything inside the defs, thus we must call updated
      //
      NotifyUpdateDelegates(UpdateTypeEnum.Updated);
    ##else
      //throw new Exception("Cannot save $do.name to the backend, it has no database table");    
    ##end if
    }
    
    // Immediate deletion, not to be called on internally managed objects. Use the remove from the internal managed list
    //
    public override void Delete()
    {
    #if $do.schema.name != 'EmptyTable'
      if (State == StateEnum.Modified ||
          State == StateEnum.NotLoaded ||
          State == StateEnum.Main
         )
      {
        Load();
        internalDefinition.UpdateStatus = StateEnum.Deleted.ToString();
        service.Save$(do.name)(internalDefinition);
      }

      State = StateEnum.Deleted;
      NotifyUpdateDelegates(UpdateTypeEnum.Deleted);
    #else
      throw new Exception("Cannot delete at the backend $do.name, it has no database table");    
    #end if
    }

    public override void Restore()
    {
      if (State == StateEnum.Modified)
      {
        internalDefinition = oldDefinition;
        State = StateEnum.Main;
        Validate();

        NotifyUpdateDelegates(UpdateTypeEnum.Updated);
      }
    }

    protected override void Validate()
    {
      errors.Clear();
      // Simple validation such as string length checks, mandatory references
      //
    #for $column in $do.get_all_basic_columns
      #if $column.type=='varchar'
      if ($column.pascalName != null && $(column.pascalName).Length > $column.length)
        errors.Add("$column.pascalName may not be longer than $column.length, currently it is "+$(column.pascalName).Length+" characters long");
      #end if
      #if $column.outgoingRef and not $column.nullable
      if ($column.pascalName == -1)
        errors.Add("$column.pascalName must be linked to a $column.outgoingRef.parent.name entry");
      #end if
    ##  #if $column.pascalName=='Name'
    ##  // Any name fields must conform to the python identifier rules
    ##  //
    ##  if ($column.pascalName == null || !Regex.Match($(column.pascalName), "^[_A-Za-z][_A-Za-z0-9]*$").Success)
    ##    errors.Add("$column.pascalName contains '"+$(column.pascalName)+"' which is not a valid identifier.");
    ##  #end if
    #end for
    #for $field in $do.do_internal_list_fields
      // Validate elements in the internal lists and add their errors to our own
      //
      foreach(var domainObject in _$field.name)
        if(!domainObject.IsValid)
          errors.AddRange(domainObject.Errors);
    #end for
    #for $field in $do.do_internal_ref_fields
      // Validate elements in the internal ref and add its errors to our own
      //
      if(!_$(field.name).IsValid)
        errors.AddRange(_$(field.name).Errors);
    #end for
    }

    #if do.method_fields
    // Additional Methods
    //
        #for $field in do.method_fields
    public void $(field.name)()
    {
      Load();

      SynchroniseDOWithDef(null);
      internalDefinition = service.$(do.name)_$(field.name)(internalDefinition);
      State = (StateEnum)Enum.Parse(typeof(StateEnum), internalDefinition.UpdateStatus);
      NotifyUpdateDelegates(UpdateTypeEnum.Updated);
    }
        #end for
    #end if
    
    #if $do.schema.name != 'EmptyTable'
    public int ID
    {
      get 
      { 
        if (State == StateEnum.NotLoaded)
            return internalSummaryDefinition.$do.schema.primaryKey.columns[0].pascalName;
        else
            return internalDefinition.$do.schema.primaryKey.columns[0].pascalName;
      }
    }
    #end if
    
    #set $summaryFields=$do._summary_fields
    #for $column in $do.get_all_basic_columns
    public #if $column.name.lower()=='name'#override#end if# $typeMapping($column) $column.pascalName
    {
      set 
      { 
        Load(); 
        #if $column.nullable and ($column.type=='int' or $column.type=='decimal' or $column.type=='datetime' )
        if (value.HasValue)
            internalDefinition.$column.pascalName = value.Value#if column.type=='char'#.ToString()#end if#;
        else
            internalDefinition.F$(column.pascalName)Specified = false;
        #else
        internalDefinition.$column.pascalName = value#if column.type=='char'#.ToString()#end if#;
        #end if
        IsModified = true; 
      }
      get 
      { 
        #if $column in $summaryFields
        if (State == StateEnum.NotLoaded)
            return internalSummaryDefinition.$specialFieldName($column,'internalSummaryDefinition');
        else
            return internalDefinition.$specialFieldName($column,'internalDefinition');
        #else
        Load(); 
        return internalDefinition.$specialFieldName($column,'internalDefinition');
        #end if
      }
    }
    #end for

    #if $do.do_internal_list_fields
    // Domain Internal child object fields
    //
      #for $field in $do.do_internal_list_fields
    private InternalDOList<$field.target_name> _$field.name;
    public InternalDOList<$field.target_name> $field.name { get { Load(); return _$field.name; } }
      #end for
    #end if
    #if $do.do_external_list_fields
    // Domain External child object fields
    //
      #for $field in $do.do_external_list_fields
    private List<$(field.target_name)> _$field.name;
    public List<$(field.target_name)> $field.name { get { Load(); return _$field.name; } }
      #end for
    #end if
    #if $do.do_external_ref_fields
    // Domain External parent reference fields
    //
      #for $field in $do.do_external_ref_fields
    private $(field.target_name) _$field.name;
    public $(field.target_name) $field.name { get { Load(); return _$field.name; } }
      #end for
    #end if
    #if $do.do_internal_ref_fields
    // Domain Internal parent reference fields
    //
      #for $field in $do.do_internal_ref_fields
    private $(field.target_name) _$field.name;
    public $(field.target_name) $field.name { get { Load(); return _$field.name; } }
      #end for
    #end if

    #if $do.do_transient_list_fields
    // Transient child object fields
    //
      #for $field in $do.do_transient_list_fields
    private List<$field.target_name> _$field.name;
    public List<$field.target_name> $field.name { get { Load(); return _$field.name; } }
      #end for
    #end if
    
    #for $field in $do.transient_fields
    public $typeMapping($field) $field.name
    {
      set { internalDefinition.$field.name = value; }
      get { return internalDefinition.$field.name;  }
    }
    #end for
  }
#end for
}
