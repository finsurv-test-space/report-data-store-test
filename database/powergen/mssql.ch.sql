<%
import settings
def typeMapping(type):
    type = type.strip().lower()
    r = {
      'varchar'	:'varchar'
     ,'int'		:'int'
     ,'datetime':'datetime'
     ,'datetime_f':'datetime'
     ,'bit'		:'tinyint'
     ,'text'	:'varchar(max)'
     ,'char'	:'varchar'
     ,'integer' :'int'
     ,'varbinary' :'uniqueidentifier'
     ,'smallint':'smallint'
     ,'float'   :'float'
     ,'money'   :'money'
     ,'decimal' :'decimal'
     ,'timestamp':'timestamp'
     ,'bigint'	:'bigint'
     ,'date':'datetime'
     ,'timestamp_f': 'timestamp'
    }[type]

    return r

def genDefault(x):
    if x and x.lower() != 'null':
        return 'default '+ x
    return ''

nullable = { True : 'null', False : 'not null' }
def buildColumnDDL(c):
    if c.length != -1:
        if c.scale: # decimal(20,2)
            return ' [%s] %s(%s,%s) %s %s'%(c.pascalName,typeMapping(c.type),c.length,c.scale,nullable[c.nullable],genDefault(c.default))
        else: # varchar(200)
			if typeMapping(c.type)=='varchar' and c.length==1:
				return ' [%s] %s(%s) %s %s'%(c.pascalName,'varchar',c.length,nullable[c.nullable],genDefault(c.default))
			else:
				if typeMapping(c.type)=='uniqueidentifier':
					return ' [%s] %s %s %s'%(c.pascalName,'uniqueidentifier',nullable[c.nullable],genDefault(c.default))
				else:
					return ' [%s] %s(%s) %s %s'%(c.pascalName,typeMapping(c.type),c.length,nullable[c.nullable],genDefault(c.default))
    else: # int
        if c.autoIncrement:
          return ' [%s] %s %s'%(c.pascalName,typeMapping(c.type),'IDENTITY(1,1)')
        else:
          return ' [%s] %s %s %s'%(c.pascalName,typeMapping(c.type),nullable[c.nullable],genDefault(c.default))

def primaryKeyName(t):
    name = t.pascalName.replace( "Record_", "Rec" )
    name = name.replace( "Installation", "Install" )
    name = name.replace( "Master_Outbound", "MasterOut" )
    name = name.replace( "Master_In", "MasterIn" )
    return name + 'PK'

def foreignKeyName(ref):
    name = ref.column2.parent.name.replace('OR_', '') + '_' + ref.column2.name.replace('OR_', '') + '__' + ref.column1.parent.name.replace('OR_', '')
    return 'FK' + name

def indexName(t, i):
    name = t.name.replace( "Record_", "Rec" )
    name = name.replace( "Installation", "Install" )
    name = name.replace( "Master_Outbound", "MasterOut" )
    name = name.replace( "Master_In", "MasterIn" )
    return name + 'IX' + str(i)

def isPrimaryKey(col,primKeyCols):
        return str(col in primKeyCols).lower()
%>
IF  NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name='ReportServiceFTCat')
BEGIN
  CREATE FULLTEXT CATALOG ReportServiceFTCat;
END

#for $table in $tables
------------------ $table.pascalName -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='$table.pascalName' AND type = 'U')
BEGIN
  DROP TABLE [$table.pascalName]
END
#end for
go

#for $table in $tables
/* $table.pascalName
 #if $table.comment
 $table.comment
 #end if
 #for $i,$c in enumerate([x for x in $table.columns])
    #if c.comment
    $c.pascalName: $c.comment
    #end if
 #end for
*/
CREATE TABLE [$table.pascalName] (
 #for $i,$c in enumerate($table.columns)
    #if $i==0# #else#,#end if#$buildColumnDDL($c)
 #end for
 #if len($table.primaryKey.columns)
    ,CONSTRAINT [$primaryKeyName($table)] PRIMARY KEY ($(','.join('['+c.pascalName+']' for c in table.primaryKey.columns)))
 #end if
)
#end for
go

-- Create Indexes
#for $table in $tables
  #for $idx in [i for i in $table.indexes if i.name != 'PRIMARY']
    #if $idx.unique
CREATE UNIQUE INDEX $idx.pascalName ON $table.name ($(','.join(['['+c.name+']' for c in $idx.columns])))
    #else
      #if $idx.name == 'NDX_FullText'
CREATE FULLTEXT INDEX ON $(table.name)( $(','.join(['['+c.name+']' for c in $idx.columns])) Language 2057) KEY INDEX [$primaryKeyName($table)] ON ReportServiceFTCat
      #else
CREATE INDEX $idx.pascalName ON $table.name ($(','.join(['['+c.name+']' for c in $idx.columns])))
      #end if
    #end if
  #end for
#end for


-- Create Foreign Key constraints
#for $i,$ref in enumerate($references)
  #if $len(ref.column1.parent.primaryKey.columns) == 1
ALTER TABLE [$ref.column2.parent.name] ADD CONSTRAINT $foreignKeyName(ref) FOREIGN KEY ([$ref.column2.name]) REFERENCES $ref.column1.parent.name ($(','.join(['['+c.name+']' for c in $ref.column1.parent.primaryKey.columns])))
  #else
ALTER TABLE [$ref.column2.parent.name] ADD CONSTRAINT $foreignKeyName(ref) FOREIGN KEY ($(','.join(['['+c.name+']' for c in $ref.column1.parent.primaryKey.columns]))) REFERENCES $ref.column1.parent.name ($(','.join(['['+c.name+']' for c in $ref.column1.parent.primaryKey.columns])))
  #end if
#end for
go
