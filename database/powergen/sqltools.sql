 Cur.Exec"\n        IF NOT EXISTS (SELECT 1\n                    FROM  sysobjects\n                   WHERE  id = OBJECT_ID('SS_SystemParameter')\n                    AND   type = 'U')\n        BEGIN\n            CREATE TABLE SS_SystemParameter (\n            SystemParameterID         int                  identity NOT FOR REPLICATION, \n            Name                      varchar(100)         NOT NULL, \n            Description               varchar(255)         NULL, \n            DateValue                 datetime             NULL, \n            IntValue                  int                  NULL, \n            TextValue                 text                 NULL, \n            TmStamp                   datetime             NOT NULL, \n            CONSTRAINT SS_SystemParameter_PK PRIMARY KEY  (SystemParameterID)\n            ) ; \n            CREATE UNIQUE  INDEX SystemParameter_UK ON SS_SystemParameter (Name)\n            insert into SS_SystemParameter (Name,IntValue,TmStamp) values ('DF_DB_Version',0,getdate())\n        END"None
 Cur.Exec"select intValue from SS_SystemParameter where name = 'DF_DB_Version' "None
 Cur.FetchOne
 Cur.Exec"SELECT t.name,f.name FROM  sysobjects f join sysobjects t on (f.parent_obj = t.id and t.type='U') WHERE  f.type = 'F'"None
 Cur.FetchAll
 Cur.Exec'alter table DF_Batch drop constraint DF_Batch_PageID__DF_Page'None
 Cur.Exec'alter table DF_BatchDataLink drop constraint DF_BatchDataLink_BatchID__DF_Batch'None
 Cur.Exec'alter table SS_Category drop constraint SS_Category_ParentID__SS_Category'None
 Cur.Exec'alter table SS_Script drop constraint SS_Script_CategoryID__SS_Category'None
 Cur.Exec'alter table DF_PageScriptLink drop constraint DF_PageScriptLink_PageID__DF_Page'None
 Cur.Exec'alter table DF_PageScriptLink drop constraint DF_PageScriptLink_ScriptID__SS_Script'None
 Cur.Exec'alter table SS_ScriptDependency drop constraint SS_ScriptDependency_ParentID__SS_Script'None
 Cur.Exec'alter table SS_ScriptDependency drop constraint SS_ScriptDependency_ChildID__SS_Script'None
 Cur.Exec'alter table DSPD_PM1 drop constraint DSPD_PM1_BatchID__DF_Batch'None
 Cur.Exec'alter table DSPD_PM1Extra drop constraint DSPD_PM1Extra_BatchID__DF_Batch'None
 Cur.Exec'alter table DSPD_PM1Delinquency drop constraint DSPD_PM1Delinquency_BatchID__DF_Batch'None
 Cur.Exec'alter table DF_DBField drop constraint DF_DBField_ReferenceID__DF_DBReference'None
 Cur.Exec'alter table DF_DBField drop constraint DF_DBField_TableID__DF_DBTable'None
 Cur.Exec'alter table DF_DBReference drop constraint DF_DBReference_TableID__DF_DBTable'None
 Cur.Exec'alter table DF_DBReference drop constraint DF_DBReference_DBFieldIDCode__DF_DBField'None
 Cur.Exec'alter table DF_DBReference drop constraint DF_DBReference_DBFieldIDName__DF_DBField'None
 Cur.Exec'alter table DF_DBTable drop constraint DF_DBTable_TableIDParent__DF_DBTable'None
 Cur.Exec'alter table DF_Field drop constraint DF_Field_PageID__DF_Page'None
 Cur.Exec'alter table DF_Field drop constraint DF_Field_DBFieldID__DF_DBField'None
 Cur.Exec'alter table DF_Field drop constraint DF_Field_PageIDDrilldown__DF_Page'None
 Cur.Exec'alter table DF_FieldAudit drop constraint DF_FieldAudit_DBFieldID__DF_DBField'None
 Cur.Exec'alter table DF_FieldAudit drop constraint DF_FieldAudit_UserID__DF_User'None
 Cur.Exec'alter table DF_IntFieldUpdate drop constraint DF_IntFieldUpdate_DBFieldID__DF_DBField'None
 Cur.Exec'alter table DF_IntFieldUpdate drop constraint DF_IntFieldUpdate_UserID__DF_User'None
 Cur.Exec'alter table DF_Page drop constraint DF_Page_TableID__DF_DBTable'None
 Cur.Exec'alter table DF_ReferenceMap drop constraint DF_ReferenceMap_SourceID__DF_Source'None
 Cur.Exec'alter table DF_ReferenceMap drop constraint DF_ReferenceMap_ReferenceID__DF_DBReference'None
 Cur.Exec'alter table DF_StrFieldUpdate drop constraint DF_StrFieldUpdate_DBFieldID__DF_DBField'None
 Cur.Exec'alter table DF_StrFieldUpdate drop constraint DF_StrFieldUpdate_UserID__DF_User'None
 Cur.Exec'alter table DF_Batch drop constraint DF_Batch_UserID__DF_User'None
 Cur.Exec'  CREATE TABLE AML_EntityType (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_EntityType_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_Product (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_Product_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_Jurisdiction (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_Jurisdiction_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_ActivitySubject (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_ActivitySubject_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_ActivityType (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_ActivityType_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_IdentifierType (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_IdentifierType_PK PRIMARY KEY  (ID) )'None
 Cur.Exec'  CREATE TABLE AML_BusinessSystem (\n  ID int identity not for replication   ,\n  IDString varchar(100) NOT NULL    ,\n  IDType varchar(100) NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT AML_BusinessSystem_PK PRIMARY KEY  (ID) )'None
 Cur.Exec"update SS_SystemParameter set intValue=%s, TmStamp=getdate() where Name ='DF_DB_Version' "(17,)
 Cur.Exec"\n        SELECT o.name\n          from sysobjects o WHERE o.type in ('U')"None
 Cur.FetchAll
 Cur.Exec'  CREATE TABLE DS_Issuer (\n  IssuerID int identity not for replication   ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT DS_Issuer_PK PRIMARY KEY  (IssuerID) )'None
 Cur.Exec'  CREATE TABLE DS_Deal (\n  DealID int identity not for replication   ,\n  Name varchar(100) NOT NULL    ,\n  IssuerID int NOT NULL    ,\n  TXSmartAccountID int NULL    ,\n CONSTRAINT DS_Deal_PK PRIMARY KEY  (DealID) )'None
 Cur.Exec'  CREATE TABLE DS_SmartAccount (\n  SmartAccountID int identity not for replication   ,\n  DealID int NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT DS_SmartAccount_PK PRIMARY KEY  (SmartAccountID) )'None
 Cur.Exec'  CREATE TABLE DS_DealPages (\n  DealPagesID int identity not for replication   ,\n  DealID int NOT NULL    ,\n  PageID int NOT NULL    ,\n CONSTRAINT DS_DealPages_PK PRIMARY KEY  (DealPagesID) )'None
 Cur.Exec'  CREATE TABLE DS_DealPeriod (\n  DealPeriodID int identity not for replication   ,\n  Number int NOT NULL    ,\n  Name varchar(45) NOT NULL    ,\n  DealID int NOT NULL    ,\n  StartDate datetime NOT NULL    ,\n  EndDate datetime NOT NULL    ,\n  Status varchar(45) NOT NULL    ,\n CONSTRAINT DS_DealPeriod_PK PRIMARY KEY  (DealPeriodID) )'None
 Cur.Exec'  CREATE TABLE DS_SmartInput (\n  SmartInputID int identity not for replication   ,\n  SmartAccountID int NOT NULL    ,\n  Description text NOT NULL    ,\n  Date datetime NOT NULL    ,\n  Amount decimal NOT NULL    ,\n  Type varchar(45) NOT NULL    ,\n CONSTRAINT DS_SmartInput_PK PRIMARY KEY  (SmartInputID) )'None
 Cur.Exec'  CREATE TABLE DS_Pop (\n  PopID int identity not for replication   ,\n  DealPeriodID int NOT NULL    ,\n  Status varchar(45) NOT NULL    ,\n  Name varchar(45) NOT NULL    ,\n  Description text NOT NULL    ,\n CONSTRAINT DS_Pop_PK PRIMARY KEY  (PopID) )'None
 Cur.Exec'  CREATE TABLE DS_PopLevel (\n  PopLevelID int identity not for replication   ,\n  PopID int NOT NULL    ,\n  Name varchar(45) NOT NULL    ,\n  Description text NOT NULL    ,\n  OrderInPop int NOT NULL    ,\n  Max decimal NOT NULL    ,\n CONSTRAINT DS_PopLevel_PK PRIMARY KEY  (PopLevelID) )'None
 Cur.Exec'  CREATE TABLE DS_PopSubLevel (\n  PopSubLevelID int identity not for replication   ,\n  PopLevelID int NOT NULL    ,\n  Name varchar(45) NOT NULL    ,\n  Description text NOT NULL    ,\n  Formula text NOT NULL    ,\n  InputValue decimal NOT NULL    ,\n  PopValue decimal NOT NULL    ,\n  CashFlowValue decimal NOT NULL    ,\n CONSTRAINT DS_PopSubLevel_PK PRIMARY KEY  (PopSubLevelID) )'None
 Cur.Exec'  CREATE TABLE DS_PopScriptLink (\n  PopScriptLinkID int identity not for replication   ,\n  PopID int NOT NULL    ,\n  ScriptID int NOT NULL    ,\n CONSTRAINT DS_PopScriptLink_PK PRIMARY KEY  (PopScriptLinkID) )'None
 Cur.Exec'  CREATE TABLE DS_DealPeriodBatchLink (\n  DealPeriodBatchLinkID int identity not for replication   ,\n  DealPeriodID int NOT NULL    ,\n  BatchID int NOT NULL    ,\n CONSTRAINT DS_DealPeriodBatchLink_PK PRIMARY KEY  (DealPeriodBatchLinkID) )'None
 Cur.Exec"update SS_SystemParameter set intValue=%s, TmStamp=getdate() where Name ='DF_DB_Version' "(18,)
 Cur.Exec'SELECT count(*) FROM  DS_Deal'None
 Cur.FetchOne
 Cur.Exec'drop table DS_Deal'None
 Cur.Exec'  CREATE TABLE DS_Deal (\n  DealID int identity not for replication   ,\n  Name varchar(100) NOT NULL    ,\n  IssuerID int NOT NULL    ,\n  TXSmartAccountID int NULL    ,\n CONSTRAINT DS_Deal_PK PRIMARY KEY  (DealID) )'None
 Cur.Exec'SELECT count(*) FROM  DS_SmartAccount'None
 Cur.FetchOne
 Cur.Exec'drop table DS_SmartAccount'None
 Cur.Exec'  CREATE TABLE DS_SmartAccount (\n  SmartAccountID int identity not for replication   ,\n  DealID int NOT NULL    ,\n  Name varchar(100) NOT NULL    ,\n CONSTRAINT DS_SmartAccount_PK PRIMARY KEY  (SmartAccountID) )'None
 Cur.Exec'SELECT count(*) FROM  DS_DealPeriod'None
 Cur.FetchOne
 Cur.Exec'drop table DS_DealPeriod'None
 Cur.Exec'  CREATE TABLE DS_DealPeriod (\n  DealPeriodID int identity not for replication   ,\n  Number int NOT NULL    ,\n  Name varchar(45) NOT NULL    ,\n  DealID int NOT NULL    ,\n  StartDate datetime NOT NULL    ,\n  EndDate datetime NOT NULL    ,\n  Status varchar(45) NOT NULL    ,\n CONSTRAINT DS_DealPeriod_PK PRIMARY KEY  (DealPeriodID) )'None
 Cur.Exec"\n        SELECT o.name\n          from sysobjects o WHERE o.type in ('U')"None
 Cur.FetchAll
 Cur.Exec'drop table ds_smarttransaction'None