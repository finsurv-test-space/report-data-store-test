import settings
import sqltools,powerschema


#con:['__doc__', '__init__', '__module__', '_pymssqlCnx__cnx', 'close', 'commit', 'cursor', 'rollback']
#cur:['__doc__', '__init__', '__module__', '_pymssqlCursor__fetchpos', '_pymssqlCursor__resultpos', '_pymssqlCursor__source', '_result', 'arraysize', 'close', 'description',  'execute', 'executemany', 'fetchall', 'fetchmany', 'fetchone', 'nextset', 'rowcount', 'setinputsizes', 'setoutputsize']

def cur_fetch_scalar(cur):
    r = cur.fetchone()
    return r[0] if r else None
    
def fetch_scalar(cur,statement,params):
    cur.execute(statement,params)
    r = cur.fetchone()
    if r:
      return r[0]
    else:
      raise Exception('No record found for statement %s with parameters %s'%(statement,params))
    
    
def link_page_to_deal(cur,deal_name,page_name):
    cur.execute('select dealid from ds_deal where name = %s',(deal_name,))
    dealid = cur_fetch_scalar(cur)
    if not dealid: 
        cur.execute('select dealid,name from ds_deal where name like %s',('%'+deal_name+'%',))
        dealid_name = cur.fetchone()
        if not dealid_name: 
            raise Exception('Cannot find deal with name '+deal_name)
        dealid = dealid_name[0]
        print 'WARNING: Resolved deal name to %s from %s'%(dealid_name[1],deal_name)
    cur.execute('select pageid from df_page where name = %s',(page_name,))
    pageid = cur_fetch_scalar(cur)
    if not pageid: 
        raise Exception('Cannot find page with name '+page_name)
    cur.execute('if not exists (select * from ds_dealpagelink where dealid=%s and pageid=%s) insert into ds_dealpagelink (dealid,pageid) values (%s,%s)',(dealid,pageid,dealid,pageid))
    
def copy_page_readonly(cur,from_page_name,to_page_name,indent=''):
    print indent+' Copying page=%s to=%s,'%(from_page_name,to_page_name),
    cur.execute('select pageid from df_page where name=%s',(from_page_name,))
    from_page_id = cur_fetch_scalar(cur)
    if not from_page_id: 
        raise 'Page=[%s] not found'%from_page_name

    cur.execute('select pageid from df_page where name=%s',(to_page_name,))
    page_id = cur_fetch_scalar(cur)
    if page_id:
        print 'Found old Page,'
        cur.execute('delete from df_field where pageid = (select pageid from df_page where name=%s)',(to_page_name,))
    else:
        print 'Creating new Page,'
        cur.execute('''
            insert into df_page (tableid,name,description,formulasetup,formulateardown,postrowupdateformula,filterformula) 
            select tableid,%s,description,formulasetup,formulateardown,postrowupdateformula,filterformula from df_page where pageid=%s''',(to_page_name,from_page_id))
        cur.execute('select scope_identity()')
        page_id = int(cur_fetch_scalar(cur))
    '''                        0        1               2       3           4       5     6     7           8       9'''
    cur.execute('''select dbfieldid,pageiddrilldown,ddp.name as ddp_name,caption,f.description,format,'N',defaultvalue,formula,displayorder from df_field f left join df_page ddp on (f.pageiddrilldown=ddp.pageid) where f.pageid=%s''',(from_page_id,))
    fields = cur.fetchall()
    
    for field in fields:
        new_ddpid = None
        if field[2]:
            new_ddpid = copy_page_readonly(cur,field[2],field[2] +' RO',indent+' ')
        cur.execute('''insert into df_field (dbfieldid,pageid,pageiddrilldown,caption,description,format,editable,defaultvalue,formula,displayorder) 
                                      values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',(field[0],page_id,new_ddpid,field[3],field[4],field[5],'N',field[7],field[8],field[9]))
    print indent+' %s:Copied %d fields. New ID=%s'%(from_page_name,len(fields),str(page_id))
    return page_id
    
def delete_page(cur,name):
    print ' Deleting page=%s'%(name,),
    cur.execute('delete from df_field where pageid = (select pageid from df_page where name=%s)',(name,))
    cur.execute('delete from df_page where name=%s',(name,))
    if cur.rowcount != 1:
        raise 'Page delete had rowcount of %s'%str(cur.rowcount)

        
        
def add_field_to_page(cur,page_name,field_caption,db_field_name=None,xFormIn=None,xFormOut=None,editable='Y',defaultvalue=None,displayorder=1000,drilldown_page_name=None,defaultType=None,defaultLength=None,defaultScale=None):
    '''Add or update field by name to a page by name'''
    cur.execute('select p.pageid,t.tableid,t.tableidparent from df_page p left join df_dbtable t on (t.tableid = p.tableid) where p.name=%s',(page_name,))
    page_id,table_id,parent_table_id = cur.fetchone()
    dbfield_id = None
    if db_field_name:
        cur.execute('select dbFieldID from df_dbfield where tableid=%s and name=%s',(table_id,db_field_name))
        dbfield_id = cur_fetch_scalar(cur)
        if parent_table_id and not dbfield_id:
            cur.execute('select dbFieldID from df_dbfield where tableid=%s and name=%s',(parent_table_id,db_field_name))
            dbfield_id = cur_fetch_scalar(cur)
        if not dbfield_id:
            raise 'Unable to find DBField=[%s] within table with parentID=[%s] and tableID=[%s]'%(db_field_name,parent_table_id,table_id)
    #print '  Field:',field[0],'->',field[1],dbfield_id
    if drilldown_page_name:
      cur.execute('select pageid from df_page p where p.name=%s',(drilldown_page_name,))
      drilldown_page_id = cur_fetch_scalar(cur)
      if not drilldown_page_id:
        raise 'Unable to find page_id for drilldown_page_name=[%s]'%drilldown_page_name
    else:
      drilldown_page_id = None
    
    cur.execute('select fieldid from df_field where pageid = %s and caption = %s',(page_id,field_caption))
    field_id = cur_fetch_scalar(cur)
    if field_id:
      #print defaultType,defaultLength,defaultScale
      cur.execute("update DF_Field set DBFieldID=%s,Editable=%s,XFormIn=%s,XFormOut=%s,pageiddrilldown=%s,defaultvalue=%s,displayorder=%s,defaulttype=%s,defaultlength=%s,defaultscale=%s where fieldid = %s",(dbfield_id,editable,xFormIn,xFormOut,drilldown_page_id,defaultvalue,displayorder,defaultType,defaultLength,defaultScale,field_id))
    else:
      cur.execute("insert into DF_Field (Caption,PageID,DBFieldID,Editable,XFormIn,XFormOut,defaultvalue,displayorder,pageiddrilldown,defaulttype,defaultlength,defaultscale) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    (field_caption,page_id,dbfield_id,editable,xFormIn,xFormOut,defaultvalue,displayorder,drilldown_page_id,defaultType,defaultLength,defaultScale))


def create_update_page_from_table(cur,name,table,setup_formula=None):
    'Creates or updates a page based on it dbfields'
    cur.execute('select tableID,tableIDParent from df_dbtable where name=%s',(table,))
    table_id,parent_id = cur.fetchone()
    if parent_id:
        cur.execute('select Name from df_dbfield where tableid=%s or tableid=%s',(table_id,parent_id))
    else:
        cur.execute('select Name from df_dbfield where tableid=%s',(table_id,))
    db_fields = cur.fetchall()
    field_list = [(fname[0],fname[0],'','','y') for fname in db_fields]
    create_page(cur,name,table,setup_formula,None,field_list)
    
def create_page(cur,name,table,setup_formula,prerow_update,field_list):
    print ' Creating page=%s with %d fields'%(name,len(field_list)),
    
    cur.execute('select Caption,DisplayOrder from df_field where pageid = (select pageid from df_page where name=%s)',(name,))
    old_order = dict(cur.fetchall())
    
    cur.execute('delete from df_field where pageid = (select pageid from df_page where name=%s)',(name,))
    cur.execute('select tableID,tableIDParent from df_dbtable where name=%s',(table,))
    table_id,table_parent_id = cur.fetchone()
    
    data = (name,)
    query = 'select pageid from df_page where name=%s'
    #print query%(data)
    cur.execute(query,data)
    page_id = cur_fetch_scalar(cur)
    linked = 0
    if not page_id:
        cur.execute('insert into df_page (name,tableid,formulasetup,PreRowUpdateFormula) values (%s,%s,%s,%s)',(name,table_id,setup_formula,prerow_update))
        cur.execute('select scope_identity()')
        page_id = int(cur_fetch_scalar(cur))
    else:
      data = (setup_formula,prerow_update,table_id,page_id)
      query = 'update df_page set formulasetup=%s,PreRowUpdateFormula=%s,tableid=%d where pageid=%s'
      #print query%(data)
      cur.execute(query,data)
    
    fieldDisplayOrder = 0
    for dirty_field in field_list:
        field = [x.strip() if len(x) else None for x in dirty_field]
        while len(field) < 6:
            field.append(None)
        field_id = None
        if field[1]:
            cur.execute('select dbFieldID from df_dbfield where tableid=%s and name=%s',(table_id,field[1]))
            field_id = cur_fetch_scalar(cur)
            linked += 1
            if not field_id:
                # If the field is not in the table then check the parent table
                #
                cur.execute('select dbFieldID from df_dbfield where tableid=%s and name=%s',(table_parent_id,field[1]))
                field_id = cur_fetch_scalar(cur)
                if not field_id:
                  raise 'Unable to find DBField=[%s] within table=[%s] with tableID=[%s] oe parentTableID=[%s]'%(field[1],table,table_id,table_parent_id)
        #print '  Field:',field[0],'->',field[1],field_id,field
        cur.execute("insert into DF_Field (Caption,PageID,DBFieldID,Editable,XFormIn,XFormOut,DefaultValue,DisplayOrder,defaultType,defaultLength,defaultScale) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                        (field[0],page_id,field_id,field[4],field[2],field[3],field[5],old_order.get(field[0],1000),defaultType,defaultLength,defaultScale))
    print 'Linked field count',linked
    
def create_update_incoming_dbreference(cur,column):
    ref_id = None
    if column.outgoingRef:
        data = (column.outgoingRef.parent.name,)
        query = 'select TableID from DF_DBTable where Name=%s'
        #print query%(data)
        cur.execute(query,data)
        ref_table_id = cur_fetch_scalar(cur)
        if ref_table_id:
            cur.execute('select DBFieldID from DF_DBField where TableID=%s and Name=%s',(ref_table_id,'Code'))
            code_dbfield = cur_fetch_scalar(cur)
            cur.execute('select referenceID from DF_DBReference where Name=%s',(column.outgoingRefNameFromSchema,))
            ref_id = cur_fetch_scalar(cur)
            if ref_id:
                cur.execute('''update DF_DBReference set tableID=%s where referenceID=%s''',(ref_table_id,ref_id))
            else:
                cur.execute('''insert into DF_DBReference (TableID, Name) values (%s,%s)''',(ref_table_id,column.outgoingRefNameFromSchema))
                cur.execute(''' select SCOPE_IDENTITY()''')
                ref_id = int(cur_fetch_scalar(cur))
    return ref_id
def create_update_outgoing_dbreference(cur,column):
    for incoming in column.incomingRef:
        # if the incoming column exists in dbfield, check his reference and update or create it
        #
        cur.execute('select DBFieldID,ReferenceID from DF_DBField dbf join DF_DBTable dbt on dbf.tableId = dbt.tableId where dbf.Name=%s and dbt.Name=%s',(incoming.name,incoming.parent.name))
        incoming_dbf = cur.fetchone()
        if not incoming_dbf:
            continue
        ref_id = create_update_incoming_dbreference(cur,incoming)
        if ref_id != incoming_dbf[1]:
            print '  Updating incoming reference on field %s.%s'%(incoming.parent.name, incoming.name)
            cur.execute('update DF_DBField set ReferenceID=%s where DBFieldID=%s',(ref_id,incoming_dbf[0]))

def create_new_dbfield(cur,table,table_id,column,isPartOfRefList):
    print '  Add new dbfield : %s'%column.name
    
    if not isPartOfRefList:
      isPartOfReference = 1 if column.name.lower() in ('name','code','idstring') else 0
    else:
      isPartOfReference = isPartOfRefList.index(column.name)+1 if column.name in isPartOfRefList else 0
    ref_id = create_update_incoming_dbreference(cur,column)
    data = (
        table_id,
        ref_id,
        column.name,
        column.type, 
        column.length, 
        column.scale, 
        isPartOfReference, 
        1 if column in table.primaryKey.columns else 0
    )
    cur.execute('''INSERT INTO DF_DBField (TableID, ReferenceID, Name, FieldType, Length, Scale, IsPartOfReference, IsPartOfPrimaryKey )  VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ''',data)
    create_update_outgoing_dbreference(cur,column)

def update_dbfield(cur,table,table_id,column,isPartOfRefList):
    #print '  Updating dbfield : %s'%column.name
    ref_id = create_update_incoming_dbreference(cur,column)
    data = (ref_id,column.type, column.length, column.scale, 1 if column in table.primaryKey.columns else 0,table_id,column.name)
    query = 'update DF_DBField set ReferenceID=%s, FieldType=%s, Length=%s, Scale=%s, IsPartOfPrimaryKey=%s where TableID=%s and Name=%s'
    #print query%(data)
    cur.execute(query,data)
    if isPartOfRefList:
      data = (isPartOfRefList.index(column.name)+1 if column.name in isPartOfRefList else 0,table_id,column.name)
      query = 'update DF_DBField set IsPartOfReference=%s where TableID=%s and Name=%s'
      cur.execute(query,data)
    create_update_outgoing_dbreference(cur,column)
            
def create_update_dbtable_for_table(cur,table_name,isPartOfRefList = None):
    # create or find the dbtable
    #
    print ' CreateOrUpdate DBTable',table_name
    table = powerschema.Table.byName[table_name.lower()]
    #print `[x.name for x in table.columns]`
    #print [x for x in powerschema.View.byName.keys() if len(x) < 15]

    if len(table.primaryKey.columns) and table.primaryKey.columns[0].outgoingRef:
      cur.execute('select TableID from DF_DBTable where name = %s',(table.primaryKey.columns[0].outgoingRef.parent.name,))
      parent_table_id = cur_fetch_scalar(cur)
      if not parent_table_id:
        raise 'Unable to find parent db_table with name=[%s]'%table.primaryKey.columns[0].outgoingRef.parent.name
    else:
      parent_table_id = None
    cur.execute('select TableID from DF_DBTable where Name=%s',(table_name,))
    table_id = cur_fetch_scalar(cur)
    update_field_count = 0
    if table_id != None:
        #print table.primaryKey.name,[x.name for x in table.primaryKey.columns]
        cur.execute('update DF_DBTable set PrimaryKey=%s, PKGenerator=%s, TableIDParent=%s where TableID=%s',
            (','.join([x.name for x in table.primaryKey.columns]),'identity' if len(table.primaryKey.columns) and table.primaryKey.columns[0].autoIncrement else None,parent_table_id,table_id))
        cur.execute('select Name,DBFieldID from DF_DBField where TableID=%s',(table_id,))
        dbFieldIDs = cur.fetchall()
        dbFields = [x[0] for x in dbFieldIDs]
        
        # update all the fields that are the same
        #
        for column in [x for x in table.columns if x.name in dbFields]:
            update_field_count += 1
            update_dbfield(cur,table,table_id,column, isPartOfRefList)
        
        # add new dbFields
        #
        for column in [x for x in table.columns if not x.name in dbFields]:
            create_new_dbfield(cur,table,table_id,column, isPartOfRefList)
            
        # remove dbFields that are extra
        #
        for dbfield_name_id in [x for x in dbFieldIDs if not x[0] in [y.name for y in table.columns]]:
            # find fields that reference this dbfield and remove them first
            #
            print '  Removing dbfield: %s'%dbfield_name_id[0]
            cur.execute('select f.FieldID,f.Caption,p.Name from DF_Field f join DF_Page p on p.pageId = f.pageID where DBFieldID=%s',(dbfield_name_id[1],))
            for field in cur.fetchall():
                print '   Nulling default order by on this field on page: %s.%s'%(field[2],field[1])
                cur.execute('update DF_Page set DefaultOrderByFieldID = NULL where DefaultOrderByFieldID=%s',(field[0],))
                print '   Removing dependant pivot aggregation field: %s.%s'%(field[2],field[1])
                cur.execute('delete from DF_PivotAggregation where FieldID=%s',(field[0],))
                print '   Removing dependant pivot group by field: %s.%s'%(field[2],field[1])
                cur.execute('delete from DF_PivotGroupBy where FieldID=%s',(field[0],))
                print '   Removing dependant field: %s.%s'%(field[2],field[1])
                cur.execute('delete from DF_Field where FieldID=%s',(field[0],))

            cur.execute('delete from DF_DBField where DBFieldID=%s',(dbfield_name_id[1],))
    else:
        data = (parent_table_id
          ,table_name
          ,','.join([x.name for x in table.primaryKey.columns])
          ,'IDENTITY' if len(table.primaryKey.columns) and table.primaryKey.columns[0].autoIncrement else 'NULL')
        cur.execute('''INSERT INTO DF_DBTable (TableIDParent, Name, PrimaryKey, PKGenerator, Editable) values (%s,%s,%s,%s,'Y')''',data)
        cur.execute('''select SCOPE_IDENTITY()''')
        table_id = int(cur.fetchone()[0])
        for column in table.columns:
            create_new_dbfield(cur,table,table_id,column,isPartOfRefList)
    print '  Updated %d dbfields'%update_field_count
# The Extension format is [Name,Description,Formula]
#
def create_namedcalculation(cur,name,description,effective_date,formula,deal_name,extensions=[]):            
  cur.execute('select dealid from ds_deal where name = %s',(deal_name,))
  deal_id = cur_fetch_scalar(cur)

  if deal_id == None:
    raise Exception('Unable to find deal with name %s'%(deal_name))
 
  cur.execute('''INSERT INTO DS_NamedCalculation (Name,Description,EffectiveDate,Formula,DealID) values (%s,%s,%s,%s,%s)''',(name,description,effective_date,formula,deal_id))
  cur.execute('select scope_identity()')
  namedcalculation_id = int(cur_fetch_scalar(cur))

  for extension in extensions:
    cur.execute('''INSERT INTO DS_NamedCalculationExtension (Name,Description,Formula,NamedCalculationID) values (%s,%s,%s,%s)''',(extension[0],extension[1],extension[2],namedcalculation_id))

def main():
    pass
    
if __name__=='__main__':
    main()



        

        
        


class Field:
  def __init__(self,caption,dbfield_name,editable='Y',defaultvalue=None,displayorder=1000,xformin=None,xformout=None,defaultType=None,defaultLength=None,defaultScale=None):
    self.caption = caption
    self.dbfield_name = dbfield_name
    self.xformin = xformin
    self.xformout = xformout
    self.editable = editable
    self.defaultvalue = defaultvalue
    self.displayorder = displayorder
    self.defaultType = defaultType
    self.defaultLength = defaultLength
    self.defaultScale = defaultScale
  def save_to_db(self,cur,page_name,drilldown_page_name=None):
    add_field_to_page(cur,page_name,self.caption,self.dbfield_name,self.xformin,self.xformout,self.editable,self.defaultvalue,self.displayorder,drilldown_page_name=drilldown_page_name,defaultType=self.defaultType,defaultLength=self.defaultLength,defaultScale=self.defaultScale)

    
class PivotDetail:
  def __init__(self,data_source_page_name,pivot_source_page_name,data_pivot_field_name,pivot_pivot_field_name,pivot_heading_field_name):
    self.data_source_page_name = data_source_page_name
    self.pivot_source_page_name = pivot_source_page_name
    self.data_pivot_field_name = data_pivot_field_name
    self.pivot_pivot_field_name = pivot_pivot_field_name
    self.pivot_heading_field_name = pivot_heading_field_name
    self.group_by_fields = []
    self.aggregation_fields = []
   
  def to_string(self):
    ret_val = ''
    ret_val += "page.pivot_detail = pagetools.PivotDetail('%s','%s','%s','%s','%s')\n"%(self.data_source_page_name,self.pivot_source_page_name,self.data_pivot_field_name,self.pivot_pivot_field_name,self.pivot_heading_field_name)
    for field in self.group_by_fields:
      ret_val += field.to_string()
    for field in self.aggregation_fields:
      ret_val += field.to_string()
    return ret_val
    
  def save_to_db(self,cur,parent_page_name):
    # Find the page ID's
    #    
    cur.execute('select PageID from DF_Page where Name = %s',(parent_page_name,))
    parent_page_id = cur.fetchone()[0]
    cur.execute('select PageID from DF_Page where Name = %s',(self.data_source_page_name,))
    data_source_page_id = cur.fetchone()[0]
    
    pivot_source_page_id = fetch_scalar(cur,'select PageID from DF_Page where Name = %s',(self.pivot_source_page_name,))
  
    # Find the field ID's
    #
    cur.execute('select FieldID from DF_Field where Caption = %s and PageID = %s',(self.data_pivot_field_name,data_source_page_id,))
    data_pivot_field_id = cur.fetchone()[0]
    cur.execute('select FieldID from DF_Field where Caption = %s and PageID = %s',(self.pivot_pivot_field_name,pivot_source_page_id,))
    pivot_pivot_field_id = cur.fetchone()[0]
    cur.execute('select FieldID from DF_Field where Caption = %s and PageID = %s',(self.pivot_heading_field_name,pivot_source_page_id,))
    pivot_heading_field_id = cur.fetchone()[0]
  
    # Update of insert the PivotDetail Field
    #
    data = (data_source_page_id,pivot_source_page_id,data_pivot_field_id,pivot_pivot_field_id,pivot_heading_field_id,parent_page_id,)
    cur.execute('update DF_PivotDetail set DataSourcePageID=%s,PivotSourcePageID=%s,DataPivotFieldID=%s,PivotPivotFieldID=%s,PivotHeadingFieldID=%s where PageID=%s',data)
    if cur.rowcount==0:
      cur.execute('insert into DF_PivotDetail (DataSourcePageID,PivotSourcePageID,DataPivotFieldID,PivotPivotFieldID,PivotHeadingFieldID,PageID) values (%s,%s,%s,%s, %s,%s)',data)
  
    # Remove the GroupBy Fields from and reinsert
    #
    cur.execute('delete from DF_PivotGroupBy where PageID = %s',(parent_page_id,))
    for field in self.group_by_fields:
      field.insert_into_db(cur,parent_page_id,data_source_page_id)
    
    # Remove the Aggregation Fields from and reinsert
    #
    cur.execute('delete from DF_PivotAggregation where PageID = %s',(parent_page_id,))
    for field in self.aggregation_fields:
      field.insert_into_db(cur,parent_page_id,data_source_page_id)
  
    
class PivotGroupByField:
  def __init__(self,parent_field_name,order):
    self.parent_field_name = parent_field_name
    self.order = order
    
  def to_string(self):
    return "page.pivot_detail.group_by_fields.append(pagetools.PivotGroupByField('%s','%s'))\n"%(self.parent_field_name,self.order)
    
  def insert_into_db(self,cur,parent_page_id,data_source_page_id):
    cur.execute('select FieldID from DF_Field where Caption = %s and PageID = %s',(self.parent_field_name,data_source_page_id,))
    parent_field_id = cur.fetchone()[0]
    cur.execute('insert into DF_PivotGroupBy (FieldID,Position,PageID) values (%s,%s,%s)',(parent_field_id,self.order,parent_page_id))

class PivotAggregationField:
  def __init__(self,parent_field_name,order,function):
    self.parent_field_name = parent_field_name
    self.order = order
    self.function = function
    
  def to_string(self):
    return "page.pivot_detail.aggregation_fields.append(pagetools.PivotAggregationField('%s','%s','%s'))\n"%(self.parent_field_name,self.order,self.function)

  def insert_into_db(self,cur,parent_page_id,data_source_page_id):
    cur.execute('select FieldID from DF_Field where Caption = %s and PageID = %s',(self.parent_field_name,data_source_page_id,))
    parent_field_id = cur.fetchone()[0]
    cur.execute('insert into DF_PivotAggregation (FieldID,Position,PageID,AggregationFunction) values (%s,%s,%s,%s)',(parent_field_id,self.order,parent_page_id,self.function,))
    
class Page:
  def __init__(self,page_name = None):
    self.name = page_name
    self.description = None
    self.formula_setup = None
    self.formula_teardown = None
    self.post_row_update_formula = None
    self.filter_formula = None
    self.pre_row_update_formula = None
    self.context = None
    self.table_name = None
    self.fields = [] # FieldCaption,DBFieldName,XFormIn,XformOut,Editable,defaultvalue
    self.pivot_detail = None
    self.default_order_by_field_name = None
    self.default_order_by_desc_ind = False
    
  def load_from_db(self,cur):
    default_order_by_field_id = None
    cur.execute('select p.description,p.formulasetup,p.formulateardown, p.postrowupdateformula, p.filterformula, p.prerowupdateformula, p.context, dbt.name, p.defaultorderbyfieldid,p.defaultorderbydescind from df_page p left join df_dbtable dbt on p.tableid = dbt.tableid where p.name = %s',(self.name,))
    results = cur.fetchone()
    if not results:
      raise "Unable to find page with name=[%s]"%self.name
    self.description,self.formula_setup,self.formula_teardown, self.post_row_update_formula, self.filter_formula, self.pre_row_update_formula, self.context, self.table_name,default_order_by_field_id,self.default_order_by_desc_ind = results
    cur.execute('select caption,dbf.name,editable,defaultvalue,displayorder,xformin,xformout,defaultType,defaultLength,defaultScale from df_field f join df_page p on p.pageid = f.pageid left join df_dbfield dbf on f.dbfieldid = dbf.dbfieldid where p.name = %s order by displayorder',(self.name,))
    self.fields = [Field(*i) for i in cur.fetchall()]
    
    cur.execute(''' select F.Caption from DF_Field F join DF_Page P on (F.PageID = P.PageID and P.Name = %s) where DBFieldID = %s''',(self.name,default_order_by_field_id,)) 
    results = cur.fetchone()
    if results != None:
      self.default_order_by_field_name = results[0]
    
    
    cur.execute(''' select DSP.Name,PSP.Name,DPF.Caption,PPF.Caption,PHF.Caption 
                    from DF_Page P 
                    join DF_PivotDetail PD on P.PageID = PD.PageID 
                    join DF_Page DSP on PD.DataSourcePageID = DSP.PageID
                    join DF_Page PSP on PD.PivotSourcePageID = PSP.PageID
                    join DF_Field DPF on PD.PivotPivotFieldID = DPF.FieldID
                    join DF_Field PPF on PD.PivotPivotFieldID = PPF.FieldID
                    join DF_Field PHF on PD.PivotHeadingFieldID = PHF.FieldID
                    where P.Name = %s''',(self.name,)) 
    results = cur.fetchone()
    if results == None: return
    self.pivot_detail = PivotDetail(*results)
        
    cur.execute(''' select F.Caption,GB.Position
                    from DF_Page P 
                    join DF_PivotGroupBy GB on P.PageID = GB.PageID
                    join DF_Field F on GB.FieldID = F.FieldID
                    where P.Name = %s''',(self.name,))
    self.pivot_detail.group_by_fields = [PivotGroupByField(*i) for i in cur.fetchall()]
    
    cur.execute(''' select F.Caption,PA.Position,PA.AggregationFunction
                    from DF_Page P 
                    join DF_PivotAggregation PA on P.PageID = PA.PageID
                    join DF_Field F on PA.FieldID = F.FieldID
                    where P.Name = %s''',(self.name,))
    self.pivot_detail.aggregation_fields = [PivotAggregationField(*i) for i in cur.fetchall()]
    
   
    
  def save_to_file(self,filename):
    f = file(filename,'w')
    page_name = 'page_%s'%self.name
    f.write('page = '+page_name.replace(' ','_').replace('-','_')+' = pagetools.Page()\n')
    for i in self.__dict__:
      if type(self.__dict__[i]) in (int,str):
        value = self.__dict__[i]
        if value.count('\n') <= 2:
            f.write('page.%s = %s\n'%(i,`value`))
        elif value.count('\n') <= 1500:
            f.write('page.%s = """%s"""\n'%(i,value))
        else:
            fn = "%s__%s.py"%(self.name,i)
            open(fn,'w').write(value)
            f.write('page.%s = open("%s").read()\n'%(i,fn))
    for field in self.fields:
      print field.defaultType,field.defaultLength,field.defaultScale
      f.write('page.fields.append(pagetools.Field(%-30s,%-30s,%s,%s,%3s,xformin=%s,xformout=%s,defaultType=%s,defaultLength=%s,defaultScale=%s))\n'%(`field.caption`,`field.dbfield_name`,`field.editable`,`field.defaultvalue`,`field.displayorder`,`field.xformin`,`field.xformout`,`field.defaultType`,`field.defaultLength`,`field.defaultScale`  ))
    if self.pivot_detail != None:
      f.write(self.pivot_detail.to_string())
    if self.default_order_by_field_name != None:
      f.write("page.default_order_by_field_name = '%s'\n"%(self.default_order_by_field_name))
      f.write("page.default_order_by_desc_ind = %s\n"%(str(self.default_order_by_desc_ind)))
    else:
      f.write("page.default_order_by_field_name = None\n")
      f.write("page.default_order_by_desc_ind = False\n")
    
    
    
  def save_to_db(self,cur):
    cur.execute('select tableID,tableIDParent from df_dbtable where name=%s',(self.table_name,))
    result = cur.fetchone()
    if result:
      print 'Found table [%s]'%(self.table_name)
      table_id,table_parent_id = result
    else:
      print 'Cant find table [%s]'%(self.table_name)
      table_id,table_parent_id = None,None

    cur.execute('select F.FieldID from DF_Field F join DF_Page P on (F.PageID = P.PageID and P.Name = %s) where F.Caption=%s',(self.name,self.default_order_by_field_name,))
    result = cur.fetchone()
    if result:
      print 'Found field [%s]-[%s]'%(self.default_order_by_field_name,result[0])
      default_order_by_field_id = result[0]
    else:
      print 'Cant find field [%s]'%(self.default_order_by_field_name)
      default_order_by_field_id = None
      self.default_order_by_desc_ind = False
    
    if self.default_order_by_desc_ind == True:
      self.default_order_by_desc_ind = 1
    else:
      self.default_order_by_desc_ind = 0
    
    data = (self.description,self.formula_setup,self.formula_teardown, self.post_row_update_formula, self.filter_formula, self.pre_row_update_formula, self.context, table_id,default_order_by_field_id,self.default_order_by_desc_ind,self.name,)
    #print data
    #print 'update df_page set description=%s,formulasetup=%s,formulateardown=%s, postrowupdateformula=%s, filterformula=%s, prerowupdateformula=%s, context=%s, tableid=%s,DefaultOrderByFieldID=%s,DefaultOrderByDescInd=%s where name=%s'%data
    cur.execute('update df_page set description=%s,formulasetup=%s,formulateardown=%s, postrowupdateformula=%s, filterformula=%s, prerowupdateformula=%s, context=%s, tableid=%s,DefaultOrderByFieldID=%s,DefaultOrderByDescInd=%s where name=%s',data)
    if not cur.rowcount:
      cur.execute('insert into df_page (description,formulasetup,formulateardown,postrowupdateformula, filterformula,prerowupdateformula,context,tableid,DefaultOrderByFieldID,DefaultOrderByDescInd,name) values (%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s)',data)
    
    cur.execute('select caption from df_field f join df_page p on p.pageid = f.pageid where p.name = %s',(self.name,))
    for remove_field in [x[0] for x in cur.fetchall() if not x[0] in [y.caption for y in self.fields]]: # remove only fields not specified (still leaves duplicates)
    #for remove_field in [x[0] for x in cur.fetchall()]:
      #print 'Removing field with caption=[%s]'%remove_field
      cur.execute('delete from df_field where fieldid in (select fieldid from df_field f join df_page p on p.pageid = f.pageid where p.name = %s and f.caption=%s)',(self.name,remove_field))
    for i in self.fields:
      i.save_to_db(cur,self.name,i.caption if table_id == None else None)
    
    if self.pivot_detail == None: return
    
    if len(self.fields)>0:
      raise 'Pivot pages cannot have fields assigned to them directly'
    
    self.pivot_detail.save_to_db(cur,self.name)
    