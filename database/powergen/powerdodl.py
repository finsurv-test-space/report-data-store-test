import powerschema

class InternalListOf:
    def __init__(self,target,target_field_name=None,link_table_name=None,filter=None):
        self.name = None # completed by the DO 
        self.filter = filter
        self.target_name = target.__name__.split('.')[-1]
        self.target = None # completed by the Loader after all DO's have been loaded
        self.target_field_name=target_field_name
        self.link_table_name=link_table_name
        if link_table_name:
            self.link_table = powerschema.Table.byName[link_table_name.lower()]
        
class ExternalListOf:
    def __init__(self,target,target_field_name=None,link_table_name=None,filter=None):
        self.name = None # completed by the DO 
        self.filter = filter
        self.target_name = target.__name__.split('.')[-1]
        self.target = None # completed by the Loader after all DO's have been loaded
        self.target_field_name=target_field_name
        self.link_table_name=link_table_name
        if link_table_name:
            self.link_table = powerschema.Table.byName[link_table_name.lower()]

            
# Primary Foreign key to a child. Much like the -ListOf but only for 1 item rather than a list
#
class ExternalRefOf:
    def __init__(self,target,target_field_name=None,link_table_name=None,filter=None):
        self.name = None # completed by the DO 
        self.filter = filter
        self.target_name = target.__name__.split('.')[-1]
        self.target = None # completed by the Loader after all DO's have been loaded
        self.target_field_name=target_field_name
        self.link_table_name=link_table_name
        if link_table_name:
            self.link_table = powerschema.Table.byName[link_table_name.lower()]

# Primary Foreign key to a child. Much like the -ListOf but only for 1 item rather than a list
#
class InternalRefOf:
    def __init__(self,target,target_field_name=None,link_table_name=None,filter=None):
        self.name = None # completed by the DO 
        self.filter = filter
        self.target_name = target.__name__.split('.')[-1]
        self.target = None # completed by the Loader after all DO's have been loaded
        self.target_field_name=target_field_name
        self.link_table_name=link_table_name
        if link_table_name:
            self.link_table = powerschema.Table.byName[link_table_name.lower()]

            
# Used to add fields which do not live in the database
#
class TransientListOf:
    def __init__(self,target):
        self.name = None # completed by the DO 
        self.filter = None
        self.target_name = target.__name__.split('.')[-1]
        self.target = None # completed by the Loader after all DO's have been loaded

            
class Method:
    pass
    
class Field:
    def __init__(self,sql_type):
        self.type = sql_type
        self.nullable = False
        
            
class DO:
    byName = {}
    def __init__(self, do_class):
        self.name = do_class.__name__
        DO.byName[self.name.lower()] = self
        if do_class.__dict__.has_key('_hooks'):
           self._hooks = do_class._hooks
        else:
           self._hooks = []

        # Populate the schema for this DO, EmptyTable if none
        #
        if type(do_class._schema) in (tuple,list):
          self.other_schemas = [powerschema.Table.byName[x.lower()] for x in do_class._schema[1:]]
          self.schema_name = do_class._schema[0]
        else:
          self.other_schemas = []
          self.schema_name = do_class._schema

        if self.schema_name:
            self.schema = powerschema.Table.byName[self.schema_name.lower()]
        else:
            self.schema = powerschema.EmptyTable()
            
        # Calculate the summary fields used for summary objects, may be specified by _summary_fields=['FieldName',...] in the DO definition
        # It automatically includes primary keys
        #
        summary_field_names = [x.lower() for x in do_class._summary_fields] if '_summary_fields' in do_class.__dict__ else ['name', 'description']
        self._summary_fields = [x for x in self.get_all_basic_columns() if x.name.lower() in summary_field_names or x.name.lower()==self.schema.primaryKey.columns[0].name.lower()]
        if '_summary_fields' in do_class.__dict__ and len([x for x in self._summary_fields if x.name in do_class._summary_fields]) != len(do_class._summary_fields):
          #print [x.name for x in self.get_all_basic_columns()]
          #print 'Summary fields',[x.name for x in self._summary_fields]
          raise Exception('Could not match all Summary Fields=%s for DO=%s'%(do_class._summary_fields,self.name))
        #print self.name,'Summary fields',[x.name for x in self._summary_fields]
        
        # Extact all internal and external fields and lists
        #
        self.method_fields = []
        self.transient_fields = []
        self.do_transient_list_fields = []
        self.do_internal_list_fields = []
        self.do_external_list_fields = []
        self.do_external_ref_fields = []
        self.do_internal_ref_fields = []
        for field_name,field in do_class.__dict__.items():
            if field_name.startswith('_'):
                continue
            field.name = field_name
            if isinstance(field,InternalListOf):
                self.do_internal_list_fields.append(field)
            elif isinstance(field,ExternalListOf):
                self.do_external_list_fields.append(field)
            elif isinstance(field,Method):
                self.method_fields.append(field)
            elif isinstance(field,InternalRefOf):
                self.do_internal_ref_fields.append(field)
            elif isinstance(field,ExternalRefOf):
                self.do_external_ref_fields.append(field)
            elif isinstance(field,Field):
                self.transient_fields.append(field)
            elif isinstance(field,TransientListOf):
                self.do_transient_list_fields.append(field)
            else:
                raise Exception('Unknown field in DO=%s, field name=%s'%(self.name,field.name))

        self.do_list_fields = self.do_internal_list_fields +self.do_external_list_fields +self.do_transient_list_fields

    def get_all_basic_columns(self):
        '''Returns all the basic non dodl defined columns in the schema and other_schemas'''
        columns = self.schema.columns[:]
        for other_schema in self.other_schemas:
          columns += [x for x in other_schema.columns if x.name.lower()!=other_schema.primaryKey.columns[0].name.lower()]
        return columns
        
    def find_basic_db_schema_link(do,column):
      if do.schema.name == 'EmptyTable':
        raise Exception('Find_basic_db_schema_link should not be called on basic columns when the Do uses the EmptyTable schema. EmptyTable should have no columns, how can u ask the path to a column that does not exist?')
      if column.parent == do.schema:
        return ''
      for local_column in do.schema.columns:
        if local_column.outgoingRef and local_column.outgoingRef.parent == column.parent:
          return '.'+local_column.pascalName+'Ref'
      raise Exception("No direct path exists from DO=%s's schema=%s to column=%s's parent schema which=%s"%(do.name,do.schema.name,column.name,column.parent.name))
        
    def find_ref_db_schema_link(do,field):
      if do.schema.name == 'EmptyTable':
        raise Exception('find_ref_db_schema_link should not be called on basic columns when the Do uses the EmptyTable schema. EmptyTable should have no columns, how can u ask the path to a column that does not exist?')
        
      if field.target.schema.primaryKey.columns[0].outgoingRef.parent.name == do.schema.name:
        return '.'+field.target.schema.pascalName+'Ref'
      for local_column in do.schema.columns:
        if local_column.outgoingRef and local_column.outgoingRef.parent == field.target.schema:
          return '.'+local_column.pascalName+'Ref'
      raise Exception("No direct path exists from DO=%s's schema=%s to field=%s's parent schema which=%s"%(do.name,do.schema.name,field.name,field.target.schema.name))
      
        
    def find_db_schema_link(do,field):
        '''This function returns the text used to construct the hibernate field to iterate over. 'dbObj.DF_BatchList' for example
        ex 0 IssuerListDO has no schema but has a list of issuers
                We should return a string to fetch all the issuers, actually we return None, the template fetches the whole list
        ex 1 DF_Batch points to DF_Page. 
                This returns iterator: foreach (var dbItem in hibernateObject.DF_BatchList)
                And fetcher: dbItem
        ex 2 DF_Field point to DF_Page. But twice. Which one do we refer to?
                DF_Field_PageIDList or DF_Field_PageIDDrilldownList?
                We use target_field_name to resolve the issue
        ex 3 DS_Deal has a many to many relationship to DF_Page
                This returns iterator: foreach (var dbItem in hibernateObject.DS_DealPagesList)
                And fetcher: dbItem.PageIDRef
        '''            
        
        if do.schema.name == 'EmptyTable' or field.target.schema.name == 'EmptyTable':
            return None#'foreach( dbItem in session.CreateCriteria(typeof(%sDef).List())'%;
        
        if not len(do.schema.primaryKey.columns):
            raise Exception('Unable to create DO %s using schema %s because the schema has no primary key'%(do.name,do.schema.name))
            
        if field.link_table_name != None:
            # we must check the link table points to this DO'schema as well as the List Field's DO schema
            for idx,incoming in enumerate(do.schema.primaryKey.columns[0].incomingRef):
                if incoming.parent.name == field.link_table.name:
                    break;
            else:
                raise Exception('Unable to resolve Many2Many table %s\'s link back to %s(Schema:%s)'%(field.link_table_name,do.name,do.schema.name))
            iterator = do.schema.primaryKey.columns[0].uniquePascalIncomingRef[idx]+'List'
            for c in field.link_table.columns:
                if c.outgoingRef and c.outgoingRef == field.target.schema.primaryKey.columns[0]:
                    break
            else:
                raise Exception('Unable to resolve Many2Many table %s\'s link towards %s(Schema:%s)'%(field.link_table_name,field.target.name,field.target.schema.Name))
            return iterator,'dbItem.'+c.pascalName+'Ref'
                
        for idx,incoming in enumerate(do.schema.primaryKey.columns[0].incomingRef):
            # is this incoming ref on this DO's schema from the the target DO's schema?
            if incoming.parent.name == field.target.schema.name:
                if not field.target_field_name:
                    # we are NOT currenly checking whether there might be more matches
                    break;
                if field.target_field_name==incoming.name:
                    break;
        else:
            raise Exception('Unable to resolve which DB List to use (%s(Schema:%s) has a list of %s(Schema:%s). Table %s is not uniquely referenced by table %s'%(do.name,do.schema.name,field.target.name,field.target.schema.name,do.schema.name,field.target.schema.name))
        iterator = do.schema.primaryKey.columns[0].uniquePascalIncomingRef[idx]+'List'
        return iterator,'dbItem'
    
def load_dodl_file(filename):
    print 'Loading DODL',filename
    g = globals()
    all = g.copy()
    execfile(filename,all)
    domain_object_list = []
    for k,v in all.items():
        if not k in g.keys():
            domain_object_list.append( DO(v) )
            
    # Resolve the list_of fields to point to the DO instances rather than the DO classes
    #
    for do in domain_object_list:
        for field in do.do_list_fields:
            field.target = DO.byName[field.target_name.lower()]
            field.link_fetcher = do.find_db_schema_link(field)
        for field in do.do_external_ref_fields +do.do_internal_ref_fields:
            field.target = DO.byName[field.target_name.lower()]
    return domain_object_list

def loadDos(files):
    dos = []
    for f in files:
        dos += load_dodl_file(f)

    #print 'All domain objects',[do.name for do in dos]
    return dos
    do = dos[0]
    print 'First do name,schema',do.__name__,do.schema
    print ' And all its fields',
    print do.__dict__.keys()
 