<%
import settings

def nullable(column):
    return column.nullable and (column.type=='int' or column.type=='decimal' or column.type=='datetime' )

def typeMapping(column):
    type = column.type
    if type.find('(') > 0:
        type = type[:type.find('(')]
    type = type.strip().lower()
    r = { 
      'varchar'	:'String'
     ,'int'		:'Int32'
     ,'datetime':'DateTime'
     ,'bit'		:'Boolean'
     ,'text'	:'String'
     ,'char'	:'String'
     ,'integer' :'Int32'
     ,'uniqueidentifier' :'Int32'
     ,'smallint':'Int16'
     ,'float'   :'Double'
     ,'money'   :'Decimal'
     ,'decimal'   :'Decimal'
    }[type]
    if column.nullable and r in ['DateTime','Int16','Int32','Boolean','Double','Decimal']:
        r += '?'
    return r
    
%>
using $settings.db_class_namespace;
using NHibernate;
using System.Collections.Generic;
using System;

namespace Codegen.DODL
{
#for $do in $dos
  #set $summaryFields=$do._summary_fields
  // Domain Object: $do.name, Schema:$do.schema.name
  #if $summaryFields
  // Summary class
  //
  public partial class $(do.name)SummaryDef
  {
    public $(do.name)SummaryDef($settings.db_class_prefix$do.schema.name hibernateDBObject)
    {
    #for $field in $summaryFields 
        #if field.outgoingRef
        if (hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref != null)
        $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref.$field.outgoingRef.pascalName;
        #else
            #if $field.nullable and ($field.type.lower() == 'int' or $field.type.lower() == 'decimal' or $field.type.lower() == 'datetime')
        if (hibernateDBObject.$(field.pascalName).HasValue)
           $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name).Value;
            #else
                #if field.type.lower() == 'char'
        $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name).ToString();
                #else
        $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name);
                #end if
            #end if
        #end if
    #end for
    }
    
  }
  #end if

  // Full class
  //
  public partial class $(do.name)Def
  {
    //public $(do.name)Def()
    //{
    //}
    #if do.schema.name != 'EmptyTable'
    public $(do.name)Def($settings.db_class_prefix$do.schema.name hibernateDBObject)
    {
      #for $field in $do.get_all_basic_columns
        ##silent print 'Constructor',$field.pascalName
        #if field.outgoingRef
        if (hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref != null)
            $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref.$field.outgoingRef.pascalName;
        #else
            #if $field.nullable and ($field.type.lower() == 'int' or $field.type.lower() == 'decimal' or $field.type.lower() == 'datetime')
        if (hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.pascalName).HasValue)
           $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name).Value;
            #else
                #if field.type.lower() == 'char'
        $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name) == null ? null : hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name).ToString();
                #else
        $field.pascalName = hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name);
                #end if
            #end if
        #end if
      #end for

      #if $do.do_internal_list_fields
        // Domain Internal object fields
        //
        #for $field in $do.do_internal_list_fields
        $field.name = new List<$(field.target_name)Def>();
        foreach (var dbItem in hibernateDBObject.$field.link_fetcher[0])
            $(field.name).Add(new $(field.target_name)Def($field.link_fetcher[1]));

        #end for
      #end if
      #if $do.do_external_list_fields
        // Domain External object fields
        //
        #for $field in $do.do_external_list_fields
        $field.name = new List<$(field.target_name)SummaryDef>();
        foreach (var dbItem in hibernateDBObject.$field.link_fetcher[0])
            $(field.name).Add(new $(field.target_name)SummaryDef($field.link_fetcher[1]));

        #end for
      #end if
      
      #if $do.do_external_ref_fields
        // Domain External reference fields
        //
        #for $field in $do.do_external_ref_fields
        $field.name = new $(field.target_name)SummaryDef(hibernateDBObject$do.find_ref_db_schema_link(field));
        #end for
      #end if
      #if $do.do_internal_ref_fields
        // Domain Internal reference fields
        //
        #for $field in $do.do_internal_ref_fields
        $field.name = new $(field.target_name)Def(hibernateDBObject$do.find_ref_db_schema_link(field));
        #end for
      #end if
      UpdateStatus = "Main";
    }
    
    public $settings.db_class_prefix$do.schema.name Save(ISession session)
    {
        #if 'Save' in $do._hooks
        PreSave(session);
        #end if
        $settings.db_class_prefix$do.schema.name hibernateDBObject;
        if (UpdateStatus == "New")
        {
            hibernateDBObject = new $settings.db_class_prefix$(do.schema.name)();
          ## On Primary Foreign Keys we need to populate the PK from the Parent table even tho this is a new object
          ##
          #if $do.schema.primaryKey.columns[0].outgoingRef
            hibernateDBObject$(do.find_basic_db_schema_link($do.schema.primaryKey.columns[0])).$($do.schema.primaryKey.columns[0].name)Ref = session.Load<$settings.db_class_prefix$do.schema.primaryKey.columns[0].outgoingRef.parent.name>($do.schema.primaryKey.columns[0].pascalName);
          #end if
          #for $field in [x for x in $do.get_all_basic_columns]
                #if field.outgoingRef
            hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref = #if nullable($field)#!F$(field.pascalName)Specified ? null : #end if#session.Load<$settings.db_class_prefix$field.outgoingRef.parent.name>($field.pascalName);
                #else
            hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name) = #if nullable($field)#!F$(field.pascalName)Specified ? new $typeMapping($field)() : new $typeMapping($field)($field.pascalName)#else#$field.pascalName#end if#;
            #end if
          #end for
            session.Save(hibernateDBObject);
        } else 
            hibernateDBObject = session.Load<$settings.db_class_prefix$do.schema.name>($do.schema.primaryKey.columns[0].pascalName);

        if (UpdateStatus == "Deleted")
        {
            session.Delete(hibernateDBObject);
            return hibernateDBObject;
        }

        if (UpdateStatus == "Modified")
        {    
          #for $field in [x for x in $do.get_all_basic_columns]
                #if field.outgoingRef
            hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name)Ref = #if nullable($field)#!F$(field.pascalName)Specified ? null : #end if#session.Load<$settings.db_class_prefix$field.outgoingRef.parent.name>($field.pascalName);
                #else
            hibernateDBObject$(do.find_basic_db_schema_link(field)).$(field.name) = #if nullable($field)#!F$(field.pascalName)Specified ? new $typeMapping($field)() : new $typeMapping($field)($field.pascalName)#else#$field.pascalName#end if#;
                #end if
          #end for
        }
        
        #for $field in $do.do_internal_list_fields
        foreach (var defItem in $field.name)
        {
            #if $field.target_field_name:
            #set $reffield=[x for x in $field.target.schema.columns if x.outgoingRef and x.outgoingRef.parent.name==$do.schema.name and $field.target_field_name == x.name]
            #else
            #set $reffield=[x for x in $field.target.schema.columns if x.outgoingRef and x.outgoingRef.parent.name==$do.schema.name]
            #end if
            // Set the ParentID in the Def object of our child. Cannot do it afterwards cause ISession.Save(obj) inserts immediately when saving objects with Identity ID's
            //
            defItem.$reffield[0].pascalName = hibernateDBObject.$do.schema.primaryKey.columns[0].pascalName;
            $settings.db_class_prefix$field.target.schema.name dbChild = defItem.Save(session);
            if (defItem.UpdateStatus == "New")
            {
                ##//dbChild.$(reffield[0].pascalName)Ref = hibernateDBObject;
                hibernateDBObject.$(field.link_fetcher[0]).Add(dbChild);
            } 
            else if (defItem.UpdateStatus == "Deleted")
                hibernateDBObject.$(field.link_fetcher[0]).Remove(dbChild);
        }
        #end for
        #for $field in $do.do_internal_ref_fields
            #if $field.target_field_name:
            #set $reffield=[x for x in $field.target.schema.columns if x.outgoingRef and x.outgoingRef.parent.name==$do.schema.name and $field.target_field_name == x.name]
            #else
            #set $reffield=[x for x in $field.target.schema.columns if x.outgoingRef and x.outgoingRef.parent.name==$do.schema.name]
            #end if
        {
            // Set the ParentID in the Def object of our child. Cannot do it afterwards cause ISession.Save(obj) inserts immediately when saving objects with Identity ID's
            //
            $(field.name).$reffield[0].pascalName = hibernateDBObject.$do.schema.primaryKey.columns[0].pascalName;
            $settings.db_class_prefix$field.target.schema.name dbChild = $(field.name).Save(session);
            if ($(field.name).UpdateStatus == "New")
                hibernateDBObject$do.find_ref_db_schema_link(field) = dbChild;
            else if ($(field.name).UpdateStatus == "Deleted")
                hibernateDBObject$do.find_ref_db_schema_link(field) = null;
        }
        #end for
        return hibernateDBObject;
    }
    #else 
    ## This side takes no hibernate db object since there is no backing schema
    public $(do.name)Def(ISession session)
    {
      #if $do.do_internal_list_fields
        // Domain Internal object fields
        //
        #for $field in $do.do_internal_list_fields
        $field.name = new List<$(field.target_name)Def>();
        foreach ($settings.db_class_prefix$field.target.schema.name dbItem in #slurp
        #if $field.filter
        session.CreateQuery("select x from $settings.db_class_prefix$field.target.schema.name x where $field.filter").List())
        #else
        session.CreateCriteria(typeof($settings.db_class_prefix$field.target.schema.name)).List())
        #end if
            $(field.name).Add(new $(field.target_name)Def(dbItem));

        #end for
      #end if
      #if $do.do_external_list_fields
        // Domain External object fields
        //
        #for $field in $do.do_external_list_fields
        $field.name = new List<$(field.target_name)SummaryDef>();
        foreach ($settings.db_class_prefix$field.target.schema.name dbItem in #slurp
        #if $field.filter
        session.CreateQuery("select x from $settings.db_class_prefix$field.target.schema.name x where $field.filter").List())
        #else
        session.CreateCriteria(typeof($settings.db_class_prefix$field.target.schema.name)).List())
        #end if
            $(field.name).Add(new $(field.target_name)SummaryDef(dbItem));

        #end for
      #end if
        UpdateStatus = "Main";
    }
    public void Save(ISession session)
    {
        #for $field in $do.do_internal_list_fields
        foreach (var defItem in $field.name)
            defItem.Save(session);
        #end for
        #for $field in $do.do_internal_ref_fields
        $(field.name).Save(session);
        #end for
    }
    #end if
  }
  // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#end for
}
