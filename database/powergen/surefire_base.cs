using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SureFire.Domain.Entity.Generated
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        DateTime CreatedOn { get; set; }
        User CreatedBy { get; set; }
        DateTime ModifiedOn { get; set; }
        User ModifiedBy { get; set; }
    }

    public class Base : IBaseEntity
    {
        public Base()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Required]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public virtual User CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public virtual User ModifiedBy { get; set; }
    }

    public class NamedBase : Base
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
	
	public class ExternalSystemReference : NamedBase
    {
        public virtual SourceSystem SourceSystem { get; set; }
        public string SourceSystemId { get; set; }
        public string SourceSystemType { get; set; }
    }
	
	public class TransactionFee : Base
    {
        public decimal Percentage { get; set; }
        public decimal Amount { get; set; }
        public virtual BaseFee Fee { get; set; }
        public virtual FeeType FeeType { get; set; }
        public virtual FeeChargeType FeeChargeType { get; set; }
        public decimal? MinimumAmount { get; set; }
        public bool InActive { get; set; }

        public void UpdatePercentage(decimal TotalAmount, decimal FeePercentage)
        {
            this.Percentage = FeePercentage;
            this.Amount = calculateAmountFromPercentage(TotalAmount, FeePercentage);
        }

        public void UpdateAmount(decimal TotalAmount, decimal FeeAmount)
        {
            this.Amount = FeeAmount;
            this.Percentage = calculatePercentageFromAmount(TotalAmount, FeeAmount);
        }

        private decimal calculateAmountFromPercentage(decimal TotalAmount, decimal FeePercentage)
        {
            return (FeePercentage / 100) * TotalAmount;
        }

        private decimal calculatePercentageFromAmount(decimal TotalAmount, decimal FeeAmount)
        {
            return TotalAmount <= 0
                    ? 0
                    : Math.Round((FeeAmount * 100) / TotalAmount, 3);
        }
    }
}