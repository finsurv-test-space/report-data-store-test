<%
import settings
import surefire

def getTypeLength(column):    
    type = column.type
    left = type.find('(')
    right = type.find(')')
    if left < 0 or right < 0:
        return '0'
    return type[left+1:right]

def getType(column):    
    type = column.type
    left = type.find('(')
    if left < 0:
        return type
    return type[:left]
%>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SureFire.Common.ExtensionMethods;
using SureFire.Common.Exceptions;
#set $sfClass = surefire.getClass($table)

namespace SureFire.Domain.Entity.Generated
{
    [Table("$(table.name)", Schema = "$(table.parent.name)")]
#if $sfClass.hasParentClass()
    public $sfClass.partial class $sfClass.className : $sfClass.parentClassName
#else
    public $sfClass.partial class $sfClass.className
#end if
    {
#for $c in $table.columns
  #if $sfClass.isClassAttribute(c)
    #if $c.nullable == False
        [Required]	 
    #end if				
    #if $c.outgoingRef == None
	  #if $c.length > 0 and $surefire.typeMapping(c) == 'string'
        [MaxLength($(c.length))]
      #elif $c.type == 'timestamp_f'
        [Timestamp]	  
      #end if
        public $surefire.typeMapping(c) $(surefire.getOutgoingColumnName(None, c)) { get; set; }
    #else
        #set $variableName = surefire.getOutgoingColumnName(c.outgoingRef.parent, c)
        #set $variableType = surefire.getClassName(c.outgoingRef.parent, sfClass.className, variableName)	
        public virtual $variableType $variableName { get; set; }	
    #end if
  #end if
#end for
#for $rel in $sfClass.getManyRelationships()
        public virtual List<$rel.getClassName()> $rel.getInstanceName() { get; set; }
#end for
#if $sfClass.hasCodegen()
        $sfClass.codegen
#end if
    }
#for $childClass in $sfClass.getChildClasses

    public $sfClass.partial class $childClass.className : $childClass.parentClassName
    {
#for $c in $table.columns
  #if $childClass.isClassAttribute(c)
    #if $c.outgoingRef == None
        public $surefire.typeMapping(c) $(c.pascalName) { get; set; }
    #else
        public virtual $(surefire.getComplexClassName(c)) $(surefire.getOutgoingColumnName(c.outgoingRef.parent, c)) { get; set; }
    #end if
  #end if
#end for
#if $childClass.hasCodegen()
        $childClass.codegen
#end if
    }
#end for
}