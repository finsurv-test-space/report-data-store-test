import settings
import pagetools,sqltools,powerschema,csv,pymssql

def create_dbtables(cur,tables):
    print 'Recreating DBTables'
    for table in settings.df_dbtables:
        pagetools.create_update_dbtable_for_table(cur,table)

    
def update_fn(fn,host,database,user,password, auto_commit = 'ask'):
    print 'Connecting to',host,':',database
    con = pymssql.connect(host=host,user=user,password=password,database=database)

    print 'Loading Schema'
    tables,references,views = powerschema.loadSchema()

    try:
        cur = con.cursor()

        fn(cur,tables)

        if auto_commit == 'ask':
          auto_commit = raw_input('All done, type "yes" to commit, [Enter] to rollback:')
          
        if (auto_commit == 'yes'):
            print 'Performing COMMIT',
            con.commit()
            print 'OK'
        else:
            print 'Performing ROLLBACK'
            con.rollback()
        
    except pymssql.DatabaseError,v:
        print v
        print 'DB Exception, performing ROLLBACK'
        raise
    finally:
        con.close()
    
def export(cur,tables,page_name):
  print 'Loading [%s] from db'%page_name
  p = pagetools.Page(page_name)
  p.load_from_db(cur)
  page_name = page_name.replace(' ','_').replace('\\','_').replace('/','_')
  print 'Saving to',page_name+'.py'
  p.save_to_file('pages\\'+page_name+'.py')

def exportall(cur,tables):
  print 'Exporting all pages'
  cur.execute('select name from df_page');
  for i in cur.fetchall():
    export(cur,tables,i[0])

  
def import_page(cur,tables,page_name):
  print 'Loading [%s] from file '%page_name
  execfile(page_name)
  locals()["page"].save_to_db(cur)

def main(options_file):
    from optparse import OptionParser

    parser = OptionParser(usage="usage: %prog [options] dbtable|printsettings|export pagename|exportall|import pagename")
    parser.add_option("-g", "--host", dest="host",
                  help="Target db host", default='(local)')
    parser.add_option("-d", "--database", dest="database",
                  help="Target db", default='PowerGen')
    parser.add_option("-u", "--user", dest="user",
                  help="Target db user name", default='sa')
    parser.add_option("-p", "--password", dest="password",
                  help="Target db user password", default='db')
    parser.add_option("-s", "--settings", dest="settings",
                  help="Use FILE as the settings file", default=options_file, metavar="FILE")
    parser.add_option("-a", "--autocommit", dest="auto_commit",  default=False, action='store_true',
                  help="Autocommit if no error")
    parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="Don't print status messages to stdout")

    options, args = parser.parse_args()
#    print 'Options',options
#    print 'Args',args

    settings.load(options.settings)
    if options.auto_commit:
        auto_commit = 'yes'
    else:
        auto_commit = 'ask'
    
    try:
        if len(args) == 0:
            parser.print_help()
        elif args[0].lower() == 'dbtable':
            update_fn(create_dbtables,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'page_pm1':
            update_fn(create_pm1_pages,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'page_pcm1':
            update_fn(create_pcm1_pages,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'link_pm':
            update_fn(link_pm,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'link_pcm':
            update_fn(link_pcm,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'static_data':
            update_fn(static_data,options.host,options.database,options.user,options.password)
        elif args[0].lower() == 'printsettings':
            settings.print_settings(args[1:])
        elif args[0].lower() == 'export':
            update_fn(lambda c,t: export(c,t,args[1]),options.host,options.database,options.user,options.password,'no')
        elif args[0].lower() == 'exportall':
            update_fn(exportall,options.host,options.database,options.user,options.password,'no')
        elif args[0].lower() == 'import':
            update_fn(lambda c,t: import_page(c,t,args[1]),options.host,options.database,options.user,options.password,auto_commit)
        else:
            print 'Unknown command. Sorry for you.'
    except EnvironmentError,v:
        print v
    except str,v:
        print v

    print '-'*40+'\nDone\n'
    
if __name__=='__main__':
    main()

