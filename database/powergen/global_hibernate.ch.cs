<%
import settings
def typeMapping(column):
	type = column.type
	if type.find('(') > 0:
		type = type[:type.find('(')]
	type = type.strip().lower()
	r = {
		'bigint'	:'BigInteger'
	 , 'varchar'	:'String'
	 ,'int'		:'Int32'
	 ,'datetime':'DateTime'
	 ,'date':'Date'
	 ,'bit'		:'Boolean'
	 ,'text'	:'String'
	 ,'char'	:'String'
	 ,'integer' :'Int32'
	 ,'uniqueidentifier' :'Int32'
	 ,'smallint':'Int16'
	 ,'float'   :'Double'
	 ,'money'   :'Decimal'
	 ,'decimal'   :'Decimal'
	 ,'idfield' :'Int32'
	 ,'dateauditfield' :'DateTime'
	 ,'descriptionfield' :'String'
	 ,'idnamefield' :'String'
	 ,'sysname' :'String'
	}[type]
	if column.nullable and r in ['DateTime','Int16','Int32','Boolean','Double','Decimal','bigint']:
		r += '?'
	return r	

def getTypeLength(column):    
    type = column.type
    left = type.find('(')
    right = type.find(')')
    if left < 0 or right < 0:
        return '0'
    return type[left+1:right]

def getType(column):    
    type = column.type
    left = type.find('(')
    if left < 0:
        return type
    return type[:left]
    
def isPrimaryKey(col,primKeyCols):
        return str(col in primKeyCols).lower()
%>
using System;
using System.Collections.Generic;
using System.Collections;
using NHibernate;

namespace $settings.db_class_namespace
{
  // Class that is used to describe individual fields in the database classes
  //
  public class DBProperties
  {
    private string name;
    private string sqlType;
    private int length;
    private bool isPrimaryKey;
    private string tableAlias;

    public string TableAlias
    {
      get { return tableAlias; }
    }
    public string Name
    {
      get { return name; }
    }
    public string SqlType
    {
      get { return sqlType; }
    }
    public int Length
    {
      get { return length; }
    }
    public bool IsPrimaryKey
    {
      get { return isPrimaryKey; }
    }

    public DBProperties(string _name, string _sqlType, int _length, bool _isPrimaryKey)
    {
      name = _name;
      sqlType = _sqlType;
      length = _length;
      isPrimaryKey = _isPrimaryKey;
    }

    public DBProperties setTableAlias(string _tableAlias)
    {
      DBProperties prop = new DBProperties(name, sqlType, length, isPrimaryKey);
      prop.tableAlias = _tableAlias;
      return prop;
    }
  }

  // Database classes start here
  //
#for $table in $tables
  public partial class $settings.db_class_prefix$table.name
  {
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof($settings.db_class_prefix$table.name));

    // Public properties
    //
#for $c in $table.columns    
 ## The next if hides the ID columns for outgoingRefs so that one can only access them via the FieldIDRef fields
 #if not c.outgoingRef
    public #slurp
 #else
    protected #slurp
 #end if 
     virtual $typeMapping($c) $c.pascalName { get {return _$c.camelName;} set { _$c.camelName = value; } }    
#end for
#for $c in $table.columns    
 #if c.outgoingRef
    public virtual $settings.db_class_prefix$c.outgoingRef.parent.pascalName $(c.pascalName)Ref 
    {
        get { return _$(c.camelName)Ref; } 
        set { _$(c.camelName)Ref = value; }  
    }
 #end if   
 #for $idx,$incoming in enumerate($c.incomingRef)
  #if ($incoming != $incoming.parent.primaryKey.columns[0]) or (len($incoming.parent.primaryKey.columns) > 1):
    public virtual IList<$settings.db_class_prefix$incoming.parent.pascalName> $(c.uniquePascalIncomingRef[$idx])List{get {return _$(c.uniqueCamelIncomingRef[$idx])List;} set { _$(c.uniqueCamelIncomingRef[$idx])List = value; }  }
 #else
    public virtual $settings.db_class_prefix$incoming.parent.pascalName $(c.uniquePascalIncomingRef[$idx])Ref{get {return _$(c.uniqueCamelIncomingRef[$idx])Ref;} set { _$(c.uniqueCamelIncomingRef[$idx])Ref = value; }  }
  #end if
 #end for
#end for
    
    // Constructors
    //
    public $settings.db_class_prefix$(table.name)()
    {
    }
    public $settings.db_class_prefix$(table.name)($settings.db_class_prefix$(table.name) source)
    {
#for $c in [x for x in $table.columns if x != $table.primaryKey.columns[0]]
 #if not $c.outgoingRef
      $c.pascalName = source.$c.pascalName;
 #end if
#end for
    }
    // Constructor to shallow copy the source and update the Created- and ModifiedDate
    //
    public $settings.db_class_prefix$(table.name)($settings.db_class_prefix$(table.name) source, DateTime moment) : this(source)
    {
#for $c in [x for x in $table.columns if x != $table.primaryKey.columns[0] and $x.name.lower() in ['createddate','modifieddate']]
      $c.pascalName = moment;
#end for
    }
    
    // Private members including ID fields
#for $c in $table.columns    
    private $typeMapping($c) _$c.camelName;
#end for
    
    // Private members for references and lists
#for $c in $table.columns
 #if c.outgoingRef
    private $settings.db_class_prefix$c.outgoingRef.parent.pascalName _$(c.camelName)Ref;
 #end if   
 #for $idx,$incoming in enumerate($c.incomingRef)
  #if $incoming != $incoming.parent.primaryKey.columns[0] or len(incoming.parent.primaryKey.columns) > 1:
    private IList<$settings.db_class_prefix$incoming.parent.pascalName> _$(c.uniqueCamelIncomingRef[$idx])List = new List<$settings.db_class_prefix$incoming.parent.pascalName>();
  #else
    private $settings.db_class_prefix$incoming.parent.pascalName _$(c.uniqueCamelIncomingRef[$idx])Ref;
  #end if
 #end for
#end for
    
    // Utility functions
    //
    public static IList<$settings.db_class_prefix$(table.name)> LoadAll(ISession session)
    {
        return session.CreateCriteria(typeof($settings.db_class_prefix$(table.name))).List<$settings.db_class_prefix$(table.name)>();
    }
    public static $settings.db_class_prefix$(table.name) Load(ISession session, object id)
    {
        return ($settings.db_class_prefix$(table.name))session.Load(typeof($settings.db_class_prefix$(table.name)), id);
    }
    public static $settings.db_class_prefix$(table.name) TryLoad(ISession session, object id)
    {
        return session.Load(typeof($settings.db_class_prefix$(table.name)), id) as $settings.db_class_prefix$(table.name);
    }
    public static $settings.db_class_prefix$(table.name) Get(ISession session, object id)
    {
        $settings.db_class_prefix$(table.name) retVal = ($settings.db_class_prefix$(table.name))session.Get(typeof($settings.db_class_prefix$(table.name)), id);
        if (retVal == null)
             throw new Aorta.AortaException("Could not find row in table $(table.name) with id="+id);
        return retVal;	
    }
    public static $settings.db_class_prefix$(table.name) TryGet(ISession session, object id)
    {
        return session.Get(typeof($settings.db_class_prefix$(table.name)), id) as $settings.db_class_prefix$(table.name);
    }
    public override string ToString()
    {
#if [x.name.lower() for x in $table.columns].count('name') == 0
        return $(table.primaryKey.columns[0].pascalName).ToString();
#else
        return $(table.primaryKey.columns[0].pascalName).ToString() +':' +Name.ToString();
#end if        
    }

#if len($table.primaryKey.columns) > 1:
    // Equals and GetHashCode is needed for composite keyed objects
    //
    public override bool Equals(object obj)
    {
      bool eq = true; 
      $settings.db_class_prefix$(table.name) other = obj as $settings.db_class_prefix$(table.name);
 #for $c in $table.primaryKey.columns:
      eq &= $c.pascalName == other.$c.pascalName;
 #end for
      return eq;
    }
    public override int GetHashCode()
    {
      int h = base.GetHashCode();
 #for $c in $table.primaryKey.columns:
      h ^= $(c.pascalName).GetHashCode();
 #end for
      return h;
    }
#end if

    // Properties describes the different fields in the table. Giving compile- and run-time names for the sql definitions
    //
    public class Properties
    {
        static public IDictionary<string,DBProperties> AllProperties;
        static Properties()
        {
            AllProperties = new Dictionary<string,DBProperties>();
#for $c in $table.columns    
            AllProperties.Add("$c.pascalName",$c.pascalName);
#end for
        }
#for $c in $table.columns    
        static public DBProperties $c.pascalName = new DBProperties("$c.name","$getType($c)",$getTypeLength($c),$isPrimaryKey($c,$table.primaryKey.columns));
#end for
    }    
    
#if $table.insert_data
    public enum Values
    {
       #echo '      '+'\n     ,'.join(x[0] for x in $table.insert_data)+'\n'
    }
    public class DBValues
    {
  #for $data in [x[0] for x in $table.insert_data]
        static public $settings.db_class_prefix$(table.name) $(data)(ISession session) { return session.Load<$settings.db_class_prefix$(table.name)>("$data"); }
  #end for
    }
#end if   
  }
#end for  

}