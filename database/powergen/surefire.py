class Rel:
  def __init__(self, className, instanceName=None):
    self.className = className
    self.instanceName = instanceName

  def __cmp__ (self, x):
    return self.className == x

  def getClassName(self):
    return self.className

  def getInstanceName(self):
    return (self.className + 's') if (self.instanceName == None) else self.instanceName
  
def typeMapping(column):
  if column.options.has_key('OverridePropertyType'):
      d = column.options.get('OverridePropertyType') 
      if d.has_key(column.name):
        return d.get(column.name)	
		  
  type = column.type
  if type.find('(') > 0:
    type = type[:type.find('(')]
  type = type.strip().lower()
  r = { 
   'bigint' :'Long'
   ,'varchar' :'string'
   ,'longtext' :'string'
   ,'longblob' :'byte[]'
   ,'binary' :'string'
   ,'int'   :'int'
   ,'tinyint':'bool'
   ,'datetime':'DateTime'
   ,'timestamp_f':'byte[]'
   ,'datetime_f':'DateTime'
   ,'date'    :'DateTime'
   ,'boolean' :'bool'
   ,'bit'   :'bool'
   ,'text'  :'string'
   ,'char'  :'string'
   ,'integer' :'int'
   ,'uniqueidentifier' :'Integer'
   ,'varbinary' :'string'
   ,'smallint':'Short'
   ,'float'   :'Double'
   ,'money'   :'Decimal'
   ,'decimal'   :'decimal'
   ,'idfield' :'Integer'
   ,'dateauditfield' :'DateTime'
   ,'descriptionfield' :'string'
   ,'idnamefield' :'string'
   ,'sysname' :'string'
  }[type]
  if column.nullable and r in ('int','bool','decimal','DateTime'):
    r += '?'
  return r 
  
def getOutgoingColumnName(parent, column):
    if parent != None:	
      name = column.pascalName
      length = len(name)
      if name.endswith('_Id') and length > 3:
        length = length - 3
        name = name[:length]
      elif len(parent.primaryKey.columns) == 1:
        suffixPKName = '_' + parent.primaryKey.columns[0].pascalName
        suffixLen = len(suffixPKName)
        if name.endswith(suffixPKName) and length > suffixLen:
          length = length - suffixLen
          name = name[:length]
        else:		
          suffixPKName = '_' + parent.primaryKey.columns[0].pascalName + 'Code'
          suffixLen = len(suffixPKName)
          if name.endswith(suffixPKName) and length > suffixLen:
            length = length - suffixLen
            name = name[:length]
    else:
      name = column.pascalName
		
    if column.options.has_key('OutgoingColumnNameOverride'):
      for namePair in column.options.get('OutgoingColumnNameOverride'):
        if namePair.has_key(name):
		  return namePair.get(name)
	  
    return name

def getClassName(table, className=None, variableName=None):
    variableType = table.options.get('ClassName', table.name)
    if table.options.has_key('OverridePropertyType'):
      d = table.options.get('OverridePropertyType') 
      if d.has_key(className):
        if d[className].has_key(variableName):
          return d[className].get(variableName)	 	  
		  
    return variableType

def determineParentClass(table):
  #If the table options have been specified then these will override
  if table.options.has_key('ParentClass'):
    return table.options.get('ParentClass')

  #Do it the hard way by looking at the fields in the table
  hasId = False
  hasCreatedOn = False
  hasCreatedBy = False
  hasModifiedOn = False
  hasModifiedBy = False 
  hasName = False 
  hasDescription = False 
  hasSourceSystemId = False
  hasSourceSystemType = False
  hasPercentage = False
  hasAmount = False
  hasFee_Id = False
  hasFeeType_Id = False
  hasFeeChargeType_Id = False
  hasMinimumAmount = False
  hasInActive = False
  
  for c in table.columns:
    if c.pascalName == 'Id':
      hasId = True
    if c.pascalName == 'CreatedOn':
      hasCreatedOn = True
    if c.pascalName == 'CreatedBy_Id':
      hasCreatedBy = True
    if c.pascalName == 'ModifiedOn':
      hasModifiedOn = True
    if c.pascalName == 'ModifiedBy_Id':
      hasModifiedBy = True
    if c.pascalName == 'Name':
      hasName = True
    if c.pascalName == 'Description':
      hasDescription = True
    if c.pascalName == 'SourceSystemId':
      hasSourceSystemId = True
    if c.pascalName == 'SourceSystemType':
      hasSourceSystemType = True
    if c.pascalName == 'Percentage':
      hasPercentage = True
    if c.pascalName == 'Amount':
      hasAmount = True
    if c.pascalName == 'Fee_Id':
      hasFee_Id = True
    if c.pascalName == 'FeeType_Id':
      hasFeeType_Id = True
    if c.pascalName == 'FeeChargeType_Id':
      hasFeeChargeType_Id = True
    if c.pascalName == 'MinimumAmount':
      hasMinimumAmount = True
    if c.pascalName == 'InActive':
      hasInActive = True	

  if hasSourceSystemId and hasSourceSystemType:
    return 'ExternalSystemReference'
  if hasPercentage and hasAmount and hasFee_Id and hasFeeType_Id and hasFeeChargeType_Id and hasMinimumAmount and hasInActive:
    return 'TransactionFee'
	
  if hasId and hasCreatedOn and hasCreatedBy and hasModifiedOn and hasModifiedBy:
    if hasName and hasDescription:
      return 'NamedBase'
    else:  
      return 'Base'
  return ''

def getClass(table):
  return SureFireClass(getClassName(table), determineParentClass(table), table.options)
    
def getComplexClassName(column):
  outgoingColumnName = getOutgoingColumnName(column.outgoingRef.parent, column)
  parentClass = getClass(column.outgoingRef.parent)
  for childClass in parentClass.getChildClasses():
    if childClass.className == outgoingColumnName:
	  return childClass.className
  return getClassName(column.outgoingRef.parent)
  
class SureFireChildClass:
  def __init__(self, parent, options):
    self.className = options.get('ClassName', 'NotSpecified')
    self.parentClassName = parent
    self.codegen = options.get('Codegen', '')
    self.partial = 'partial' if options.get('Partial', False) else ''
    self.properties = options.get('Properties', [])

  def hasCodegen(self):
    return self.codegen != ''

  def isClassAttribute(self, column):
    if column.outgoingRef == None:
      propertyname = column.pascalName
    else:  
      propertyname = getOutgoingColumnName(column.outgoingRef.parent, column)
    if propertyname in self.properties:
      return True
    return False

class SureFireClass:
  def __init__(self, name, parent, options):
    self.className = name
    self.parentClassName = parent
    self.codegen = options.get('Codegen', '')
    self.partial = 'partial' if options.get('Partial', False) else ''
    #Insert Many Relationships from the passed in options
    self.manyrelationships = []
    manyrels = options.get('ManyRelationships', [])
    for manyrel in manyrels:
      class_instance = manyrel.split(':')
      if len(class_instance) == 2:
        self.manyrelationships.extend([Rel(class_instance[0], class_instance[1])])
      else:
        self.manyrelationships.extend([Rel(class_instance[0])])
    #Insert Children from the passed in options
    self.childrenclasses = []
    children = options.get('Children', [])
    for child in children:
      self.childrenclasses.extend([SureFireChildClass(self.className, child)])

  def getClassName(self):
    return self.className

  def getParentClassName(self):
    return self.parentClassName

  def hasParentClass(self):
    return self.parentClassName != ''

  def hasCodegen(self):
    return self.codegen != ''

  def isParentColumn(self, column):
    if self.parentClassName in ['NamedBase', 'ExternalSystemReference']:
      if column.pascalName == 'Name':
        return True
      if column.pascalName == 'Description':
        return True

    if self.parentClassName in ['Base', 'NamedBase', 'TransactionFee', 'ExternalSystemReference']:
      if column.pascalName == 'Id':
        return True
      if column.pascalName == 'CreatedOn':
        return True
      if column.pascalName == 'CreatedBy_Id':
        return True
      if column.pascalName == 'ModifiedOn':
        return True
      if column.pascalName == 'ModifiedBy_Id':
        return True
		
    if self.parentClassName == 'TransactionFee':
      if column.pascalName == 'Percentage':
        return True
      if column.pascalName == 'Amount':
        return True
      if column.pascalName == 'Fee_Id':
        return True
      if column.pascalName == 'FeeType_Id':
        return True
      if column.pascalName == 'FeeChargeType_Id':
        return True
      if column.pascalName == 'MinimumAmount':
        return True
      if column.pascalName == 'InActive':
        return True	
		
    if self.parentClassName == 'ExternalSystemReference':
      if column.pascalName == 'SourceSystemId':
        return True
      if column.pascalName == 'SourceSystemType':
        return True
      if column.pascalName == 'SourceSystem_Id':
        return True
		
    return False

  def isChildColumn(self, column):
    if column.outgoingRef == None:
      propertyname = column.pascalName
    else:  
      propertyname = getOutgoingColumnName(column.outgoingRef.parent, column)
    for child in self.childrenclasses:
      if propertyname in child.properties:
        return True
    return False

  def isClassAttribute(self, column):
    if column.outgoingRef == None:
      propertyname = column.pascalName
    else:  
      propertyname = getOutgoingColumnName(column.outgoingRef.parent, column)
    if column.options.has_key('IgnoredColumns'):
      ignoredColumns = column.options.get('IgnoredColumns');
      if propertyname in ignoredColumns:
        return False
		
    return not (self.isParentColumn(column) or self.isChildColumn(column) or column.pascalName in ['Discriminator'])

  def getManyRelationships(self):
    return self.manyrelationships

  def getChildClasses(self):
    return self.childrenclasses

