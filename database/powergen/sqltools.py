import pymssql,re
import powerschema

nullable = { True : 'NULL', False : 'NOT NULL' }
def genDefault(x):
    if x:
        return 'DEFAULT '+x
    return ''

    
def buildColumnDDL(c):
    if c.autoIncrement:
        return ' %s int identity not for replication'%(c.name)
    else:
        if c.length != -1:
            if c.scale:
                return ' %s %s(%s,%s) %s %s'%(c.name,c.type,c.length,c.scale,nullable[c.nullable],genDefault(c.default))
            else:
                return ' %s %s(%s) %s %s'%(c.name,c.type,c.length,nullable[c.nullable],genDefault(c.default))
        else:
            return ' %s %s %s %s'%(c.name,c.type,nullable[c.nullable],genDefault(c.default))
    
def execFn(fn,cur,command_split=r'\[SQLBATCH\]'):
    print ' Executing script',fn
    splitRe = re.compile(r'\s'+command_split+r'\s',re.I)
    lineNoRe = re.compile(r'line (\d*)')
    commentRe = re.compile('/\\*(.*?)\\*/|--(.*?\n)',re.S)

    def uncomment_func(match):
        comment = match.group(1)
        if comment == None:
            comment = match.group(2)
        return '\n'*comment.count('\n')
    file_content = file(fn).read()
    decommented = file_content #commentRe.sub(uncomment_func,file_content)
    #file('decommented.sql','w').write(decommented)
    batches = splitRe.split(decommented)

    line_counter = 0
    error_line = None
    def replace_func(match):
        error_line = int(match.group(1))+line_counter
        return 'File: %s, Batch Line: %s, File line <%d>\nErrorLine=[%s]'%(fn, match.group(1), error_line, file_content.split('\n')[error_line-1])

    for i in batches:
        try:
            if len(i.strip()) != 0:
                cur.execute(i)
        except pymssql.DatabaseError,v:
            v.message = lineNoRe.sub(replace_func,str(v))
            v.args = (v.message,)
            raise v
        line_counter += i.count('\n')

def get_db_version(cur):
    cur.execute('''
        IF NOT EXISTS (SELECT 1
                    FROM  sysobjects
                   WHERE  id = OBJECT_ID('SS_SystemParameter')
                    AND   type = 'U')
        BEGIN
            CREATE TABLE SS_SystemParameter (
            SystemParameterID         int                  identity NOT FOR REPLICATION, 
            Name                      varchar(100)         NOT NULL, 
            Description               varchar(255)         NULL, 
            DateValue                 datetime             NULL, 
            IntValue                  int                  NULL, 
            TextValue                 text                 NULL, 
            TmStamp                   datetime             NOT NULL, 
            CONSTRAINT SS_SystemParameter_PK PRIMARY KEY  (SystemParameterID)
            ) ; 
            CREATE UNIQUE  INDEX SystemParameter_UK ON SS_SystemParameter (Name)
            insert into SS_SystemParameter (Name,IntValue,TmStamp) values ('DF_DB_Version',0,getdate())
        END''')
    cur.execute('''select intValue from SS_SystemParameter where name = 'DF_DB_Version' ''')
    result = cur.fetchone()
    return int(result[0])

def update_db_version(cur,version):
    cur.execute('''update SS_SystemParameter set intValue=%s, TmStamp=getdate() where Name ='DF_DB_Version' ''',(version,))
    if cur.rowcount == 0:
        cur.execute('''insert into SS_SystemParameter (Name,IntValue,TmStamp) values ('DF_DB_Version',%s,getdate())''',(version,))
    
def drop_all_current_constraints(cur):
    cur.execute("SELECT t.name,f.name FROM  sysobjects f join sysobjects t on (f.parent_obj = t.id and t.type='U') WHERE  f.type = 'F'")
    constraints = cur.fetchall()
    for constraint in constraints:
        cur.execute("alter table %s drop constraint %s"%(constraint[0],constraint[1]))


def create_new_constraints(cur,references):
    for ref in references:
        constraint = '''ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY (%s) REFERENCES %s (%s) '''%\
                (ref.column2.parent.name, ref.codeName, ref.column2.name, ref.column1.parent.name, ref.column1.name)
        cur.execute(constraint)
      
def drop_recreate_table(cur,table):
    print ' Recreating table',table.name
    #cur.execute('''SELECT count(*) FROM  sysobjects WHERE  id = OBJECT_ID('%s') AND   type = 'U')'''%(table.name))
    cur.execute('''SELECT count(*) FROM  %s'''%(table.name))
    if cur.fetchone()[0] != 0:
        print 'WARNING: Dropping non empty table',table.name
    cur.execute('drop table '+table.name)
    create_table(cur,table)
    
def safe_recreate_table(cur,table,column_mapping=[]):
    '''Recreates the table without losing its data. Creates a new tmp table, moves all the data accros then drops the old and renames the new
    column_mapping = [(old_column_name,new_column_name),...] 
    Columns with matching names are moved over automatically unless an a mapping entry for them exists
    '''
    print ' Safe recreation of table',table.name
    current_cols = get_current_columns(cur,table.name)
    mapped = column_mapping[:]
    for old_column in current_cols:
        if old_column in [x[0] for x in column_mapping]:
            continue
        if not old_column in [x.name for x in table.columns]:
            continue
        mapped.append((old_column,old_column))
    
    cur.execute('''
        SET QUOTED_IDENTIFIER ON
        SET ARITHABORT ON
        SET NUMERIC_ROUNDABORT OFF
        SET CONCAT_NULL_YIELDS_NULL ON
        SET ANSI_NULLS ON
        SET ANSI_PADDING ON
        SET ANSI_WARNINGS ON''')

    sql = '  CREATE TABLE tmp_%s (\n'%table.name
    sql += '   ,\n'.join(' '+buildColumnDDL(c) for c in table.columns) +')\n'
    cur.execute(sql)
    if table.primaryKey.columns[0].autoIncrement:
       cur.execute('SET IDENTITY_INSERT tmp_%s ON'%table.name)
    cur.execute('''
    IF EXISTS(SELECT * FROM %s)
         INSERT INTO Tmp_%s (%s)
            SELECT %s FROM %s WITH (HOLDLOCK TABLOCKX)'''%(table.name,table.name,','.join(x[1] for x in mapped),','.join(x[0] for x in mapped),table.name))
    if table.primaryKey.columns[0].autoIncrement:
       cur.execute('SET IDENTITY_INSERT tmp_%s OFF'%table.name)
    cur.execute('DROP TABLE '+table.name)
    cur.execute("EXECUTE sp_rename N'tmp_%s', N'%s', 'OBJECT' "%(table.name,table.name))
    cur.execute('ALTER TABLE %s ADD CONSTRAINT %s_PK PRIMARY KEY (%s)'%(table.name,table.name,','.join([x.name for x in table.primaryKey.columns])))
    
def create_table(cur,table):
    print ' Creating table',table.name
    sql = '  CREATE TABLE %s (\n'%table.name
    sql += '   ,\n'.join(' '+buildColumnDDL(c) for c in table.columns)
    sql += '   ,\n CONSTRAINT %s_PK PRIMARY KEY  (%s) )'%(table.name,','.join([x.name for x in table.primaryKey.columns]))
    #print sql
    cur.execute(sql)

    
def get_current_columns(cur,table_name):
    cur.execute('''
        SELECT sc.Name
          FROM syscolumns sc 
          JOIN systypes st on sc.xtype = st.xusertype
          JOIN sysobjects o ON sc.ID=o.ID AND o.type in ('U','V') AND o.Name = %s''',(table_name,))
    return [x[0] for x in cur.fetchall()]

def get_current_tables_with_columns(cur):
    '''Returns { 'tableX' : [['columnID','int',4,0,True],['columnSOmething','varhchar',20,0,False]] }'''
    map = {
        'integer' : 'int'
    }
    cur.execute('''
        SELECT o.name,sc.Name,st.Name, isnull(sc.Prec,sc.Length), sc.Scale, case when ic.name = sc.Name then 1 else 0 end
          FROM syscolumns sc 
          JOIN systypes st on sc.xtype = st.xusertype
          JOIN sysobjects o ON sc.ID=o.ID AND o.type in ('U')
          left join sys.identity_columns ic on (sc.id = ic.object_id)''')
    rows = cur.fetchall()
    tables = {}
    for r in rows:
        columns = tables.setdefault(r[0],[])
        columns.append((r[1],r[2],r[3],r[4],True if r[5] else False))
    return tables

def get_current_tables(cur):
    cur.execute('''
        SELECT o.name
          from sysobjects o WHERE o.type in ('U')''')
    rows = cur.fetchall()
    return [x[0] for x in rows if x[0].lower() != 'ss_systemparameter']

def add_missing_tables(cur,schema_tables):
    current_tables = get_current_tables(cur)
    missing_tables = [x for x in schema_tables if not x.name in current_tables]
    create_tables(cur,missing_tables)
    
def add_missing_columns(cur,table):
    existing = get_current_columns(cur,table.name)
    missing = [x for x in table.columns if not x.name in existing]
    print ' Adding columns to table %s: %s'%(table.name,','.join(x.name for x in missing))
    for col in missing:
        sql = 'alter table %s add %s'%(table.name,buildColumnDDL(col))
        cur.execute(sql)

def drop_old_columns(cur,table):
    existing = get_current_columns(cur,table.name)
    model = [x.name for x in table.columns]
    missing = [x for x in existing if not x in model]
    print ' Removing columns from table %s: %s'%(table.name,','.join(missing))
    for col in missing:
        cur.execute('alter table %s drop column %s'%(table.name,col))
    
def drop_table(cur,table):
    print ' Dropping table',table
    cur.execute('drop table '+table)
    
def drop_current_tables(cur):
    print 'Dropping all current tables'
    for i in get_current_tables(cur):
        drop_table(cur,i)
    
    
def compare_current_with_model_column(schema_columns,current_columns):
    '''Compares a schema_column with its def in the database
        schema_column is from the schema
        current_columns as defined by get_current_tables_with_columns for each table in its return dictionary
    '''
    result_list = []
    for schema_column in schema_columns:
        current_column = [x for x in current_columns if x[0] == schema_column.name]
        if not len(current_column):
            continue
        current_column = current_column[0]
        result = ''
        if schema_column.type.lower() != current_column[1].lower():
            result += 'Column types differ (Current %s,Schema %s). '%(current_column[1],schema_column.type)
        elif schema_column.type == 'decimal' and (schema_column.scale != current_column[3] or schema_column.length != current_column[2]):
            result += 'Column lengths differ (Current decimal(%s,%s),Schema decimal(%s,%s)). '%(current_column[2],current_column[3],schema_column.length,schema_column.scale)
        elif schema_column.type in ('char','varchar') and schema_column.length != current_column[2]:
            result += 'Column lengths differ (Current %s(%s),Schema %s(%s)). '%(current_column[1],current_column[2],schema_column.type,schema_column.length)
        elif (schema_column.autoIncrement and not current_column[4]) or (not schema_column.autoIncrement and current_column[4]):
            result += 'Column autoIncrement differ (Current %s,Schema %s).'%(current_column[4],schema_column.autoIncrement)
        if result != '':
            result_list.append(schema_column.name +':'+result)
    return result_list
    
def compare_current_with_model(cur,model_tables):
    string_model_tables = [x.name for x in model_tables]
    current_tables = get_current_tables_with_columns(cur)
    
    missing_tables = [x for x in string_model_tables if not x in current_tables.keys()]
    extra_tables = [x for x in current_tables if not x in string_model_tables and x != 'SS_SystemParameter']
    union = [x for x in current_tables if x in string_model_tables]
    ok = 1
    if len(extra_tables):
        ok = 0
        print '  Extra   tables:',','.join(extra_tables)
    if len(missing_tables):
        ok = 0
        print '  Missing tables:',','.join(missing_tables)
    
    for table in union:
        current_column_names = [x[0] for x in current_tables[table]]
        model_column_names = [x.name for x in powerschema.Table.byName[table.lower()].columns]
        extra_columns = [x for x in current_column_names if not x in model_column_names]
        missing_columns = [x for x in model_column_names if not x in current_column_names]
        different_columns = compare_current_with_model_column(powerschema.Table.byName[table.lower()].columns,current_tables[table])
        if len(extra_columns) > 0 or len(missing_columns) > 0 or len(different_columns) > 0:
            print '  Table',table,'differences:'
            ok = 0
            if len(extra_columns):
                print '   Extra   columns:',','.join(extra_columns)
            if len(missing_columns):
                print '   Missing columns:',','.join(missing_columns)
            if len(different_columns):
                print '  ','\n   '.join(different_columns)
    if ok:
        print 'Model and current db matches OK'
    return ok

def sync_current_with_model(cur,model_tables):
    string_model_tables = [x.name for x in model_tables]
    current_tables = get_current_tables_with_columns(cur)
    missing_tables = [x for x in string_model_tables if not x in current_tables.keys()]
    extra_tables = [x for x in current_tables if not x in string_model_tables and x != 'SS_SystemParameter']
    union = [x for x in current_tables if x in string_model_tables]
    if len(extra_tables):
        print '  Dropping extra tables:'
        for dead_table in extra_tables:
            drop_table(cur,dead_table)
    if len(missing_tables):
        print '  Creating missing tables:'
        for new_table in missing_tables:
            create_table(cur,powerschema.Table.byName[new_table.lower()])
    for table in union:
        current_column_names = [x[0] for x in current_tables[table]]
        model_column_names = [x.name for x in powerschema.Table.byName[table.lower()].columns]
        extra_columns = [x for x in current_column_names if not x in model_column_names]
        missing_columns = [x for x in model_column_names if not x in current_column_names]
        different_columns = compare_current_with_model_column(powerschema.Table.byName[table.lower()].columns,current_tables[table])
        if len(extra_columns) > 0 or len(missing_columns) > 0 or len(different_columns) > 0:
            print '  Safe recreate table',table,'due to column differences:',
            if extra_columns: print ' Extra:',extra_columns,
            if missing_columns: print ' Missing:',missing_columns,
            if different_columns: print ' Different:',different_columns,
            print
            safe_recreate_table(cur,powerschema.Table.byName[table.lower()])

    
def grant_permissions_on_current(cur):
    raise Exception('Not implemented yet')
    
def create_tables(cur,tables):
    for i in tables:
        create_table(cur,i)

def insert_preset_data(cur,tables):
    for table in tables:
        if table.insert_statements:
            cur.execute(table.insert_statements.replace('`',''))

def recreate_views(cur,views):
    count = 0
    for view in views:
        if len(view.sql.strip())!=0:
            cur.execute('''IF EXISTS (SELECT name FROM sysobjects WHERE name = '%s' AND type = 'V')  DROP VIEW %s'''%(view.name,view.name))
            cur.execute(view.sql)
            count+=1
    print 'Re/Created %d views'%count
    
class WrapCursor:
    def __init__(self,cur):
        self.cur = cur
        self.log = []
    def execute(self,command,parms=None):
        self.log.append( ' Cur.Exec' +`command`+`parms` )
        try:
            if parms:
                r = self.cur.execute(command,parms)
            else:
                r = self.cur.execute(command)
        except pymssql.DatabaseError,v:
            v.log = self.log
            raise v
        self.rowcount = self.cur.rowcount
    def fetchone(self):
        self.log.append(' Cur.FetchOne')
        return self.cur.fetchone()
    def fetchall(self):
        self.log.append(' Cur.FetchAll')
        return self.cur.fetchall()
    #def rowcount(self):
    #    return self.cur.rowcount()
        
        
def upgrade(cur,tables,references,views,**kwargs):
    upgrade_path=kwargs['upgrade_path']
    version = get_db_version(cur)
    drop_all_current_constraints(cur)
    if upgrade_path.has_key(version):
        print 'Killing constraints'
        while upgrade_path.has_key(version):
            print 'Upgrading from version',version,'...'
            version = upgrade_path[version](cur,tables,references,views)
            update_db_version(cur,version)
    else:
        print 'No upgrade to perform for version',version
    print 'Synching database...'
    sync_current_with_model(cur,tables)
    update_db_version(cur,version)
    print 'Recreating constraints according to Schema'
    create_new_constraints(cur,references)
    print 'Recreating views according to schema'
    recreate_views(cur,views)

def create(cur,tables,references,views,**kwargs):
    print 'Killing constraints'
    drop_all_current_constraints(cur)
    print 'Synching database...'
    sync_current_with_model(cur,tables)
    print 'Inserting preset data'
    sqltools.insert_preset_data(cur,tables)

    update_db_version(cur,kwargs['version'])
    print 'Database now on version',kwargs['version']
   
    print 'Recreating constraints according to Schema'
    create_new_constraints(cur,references)
    print 'Recreating views according to schema'
    recreate_views(cur,views)

        
def db_func_wrapper(func,host,database,user,password,**kwargs):
    print 'Connecting to',host,':',database,
    con = pymssql.connect(host=host,user=user,password=password,database=database)
    print 'OK'

    print 'Loading Schema'
    tables,references,views = powerschema.loadSchema()

    try:
        cur = WrapCursor(con.cursor())
        
        func(cur,tables,references,views,**kwargs)
        
        print 'Comparing current database with model...'
        compare_current_with_model(cur,tables)

        do_commit_input = raw_input('All done, type "yes" or "commit" to commit, [Enter] to rollback:')
        if (do_commit_input.lower() in ['yes','commit']):
            print 'Performing COMMIT',
            con.commit()
            print 'OK'
        else:
            print 'Performing ROLLBACK'
            con.rollback()
    except pymssql.DatabaseError,v:
        print '\nSQL Log:'
        print '\n'.join(v.log[-10:])
        print v
        file('sqltools.sql','w').write('\n'.join(v.log))
        print 'DB Exception, performing ROLLBACK'
        try:
            con.rollback()
        except:
            pass
        raise
    finally:
        con.close()
        
        
