import re

def load(filename):
    execfile(filename,globals())
    
def getOptions(name):
    composite = None
    if name == None:
        return {} 
    composite = {}
    for k,v in table_flags:
        if re.match('^'+k+'$',name):
            composite.update(v)
#    print name,'->',composite
    if composite and composite.has_key('exclude'):
        return None
    return composite

def print_settings(args):
        if len(args) != 1:
            print 'Need table name: ex. powergen.py printopt svRespondent'
            return
        print 'Options for:',args[0]
        for k,v in getOptions(args[0]).items():
            print '%20s : %s'%(str(k),str(v))
