<%
import settings
def typeMapping(type):
    type = type.strip().lower()
    r = { 
      'varchar'	:'VARCHAR2'
     ,'int'		:'NUMBER(8)'
     ,'datetime':'TIMESTAMP(6)'
     ,'bit'		:'NUMBER(1)'
     ,'text'	:'CLOB'
     ,'char'	:'VARCHAR2'
     ,'integer' :'NUMBER(8)'
     ,'uniqueidentifier' :'DOG'
     ,'smallint':'NUMBER(5)'
     ,'float'   :'FLOAT'
     ,'money'   :'DECIMAL'
     ,'decimal' :'DECIMAL'
     ,'timestamp':'TIMESTAMP(6)'
     ,'bigint'	:'NUMBER(12)'
     ,'date':'TIMESTAMP(6)'
    }[type]
    return r

def genDefault(x):
    if x and x.lower() != 'null':
        return 'DEFAULT '+x
    return ''

nullable = { True : '', False : 'NOT NULL' }
def buildColumnDDL(c):
    if c.length != -1:
        if c.scale: # decimal(20,2)
            return ' %s %s(%s,%s) %s %s'%(c.pascalName,typeMapping(c.type),c.length,c.scale,nullable[c.nullable],genDefault(c.default))
        else: # varchar(200)
			if typeMapping(c.type)=='VARCHAR2' and c.length==1:
				return ' %s %s(%s) %s %s'%(c.pascalName,'CHAR',c.length,nullable[c.nullable],genDefault(c.default))
			else:
				return ' %s %s(%s) %s %s'%(c.pascalName,typeMapping(c.type),c.length,nullable[c.nullable],genDefault(c.default))
    else: # int
        return ' %s %s %s %s'%(c.pascalName,typeMapping(c.type),nullable[c.nullable],genDefault(c.default))

def primaryKeyName(t):
    name = t.pascalName.replace( "Record_", "Rec" )
    name = name.replace( "Installation", "Install" )
    name = name.replace( "Master_Outbound", "MasterOut" )
    name = name.replace( "Master_In", "MasterIn" )
    return name + 'PK'

def foreignKeyName(t, i):
    name = t.name.replace( "Record_", "Rec" )
    name = name.replace( "Installation", "Install" )
    name = name.replace( "Master_Outbound", "MasterOut" )
    name = name.replace( "Master_In", "MasterIn" )
    return name + 'FK' + str(i)

def indexName(t, i):
    name = t.name.replace( "Record_", "Rec" )
    name = name.replace( "Installation", "Install" )
    name = name.replace( "Master_Outbound", "MasterOut" )
    name = name.replace( "Master_In", "MasterIn" )
    return name + 'IX' + str(i)

def sequenceName(t):
    name = t.pascalName.replace( "Record_", "Rec_" )
    return 'SEQ_' + name
	 

def isPrimaryKey(col,primKeyCols):
        return str(col in primKeyCols).lower()
%>



 
#for $table in $tables

------------------ $table.pascalName -------------------------
Begin  
  execute immediate 'Drop table $table.pascalName cascade constraints';
  Exception when others then null;
End;
/
#for $c in table.columns
  #if $c.autoIncrement
Begin  
  execute immediate 'Drop sequence $sequenceName($table)';
  Exception when others then null;
End;
/
CREATE SEQUENCE $sequenceName($table) start with 1 increment by 1 nocache ;
  #end if
#end for
   
CREATE TABLE $table.pascalName (
 #for $i,$c in enumerate($table.columns)
    #if $i==0# #else#,#end if#$buildColumnDDL($c)
 #end for
 #if len($table.primaryKey.columns)
    ,constraint $primaryKeyName($table) Primary Key ($(','.join(c.pascalName for c in table.primaryKey.columns))) using index tablespace $settings.index_tablespace_name
 #end if
 #for $idx in [i for i in $table.indexes if i.name.startswith('IDX_')]
    ,constraint $idx.pascalName unique ($(','.join([c.name for c in $idx.columns]))) using index ( create unique index $idx.pascalName on (($(','.join([c.name for c in $idx.columns])))) tablespace $settings.index_tablespace_name    
 #end for
) TABLESPACE #if 'TableSpace' in $table.options#$table.options.TableSpace#else#$settings.data_tablespace_name#end if#;

 #if $table.comment
COMMENT ON TABLE $table.pascalName is '$table.comment.replace("'","''")';
 #end if
 #for $i,$c in enumerate([x for x in $table.columns])
    #if c.comment
    COMMENT ON COLUMN $(table.pascalName).$c.pascalName is '$c.comment.replace("'","''")';
    #end if
 #end for


#end for  

#for $i,$ref in enumerate($references)
  #if not ($ref.column2 in $ref.column2.parent.primaryKey.columns)
CREATE INDEX $indexName(ref.column2.parent, i) ON $ref.column2.parent.name ($ref.column2.name) TABLESPACE $settings.index_tablespace_name;
  #end if
#end for

#for $i,$ref in enumerate($references)
ALTER TABLE $ref.column2.parent.name ADD CONSTRAINT $foreignKeyName(ref.column2.parent, i) FOREIGN KEY ($ref.column2.name) REFERENCES $ref.column1.parent.name ($ref.column1.name);
#end for

