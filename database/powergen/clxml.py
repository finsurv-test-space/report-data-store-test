import xml.sax.handler
import xml.sax
import xml.sax.saxutils
import types


def findTagAttHelp(x,tagName,attDict):
    if  x.name != tagName:
        return 0
    for k,v in attDict.items():
        if x.atts.get(k) != v:
            return 0
    return 1
def findTagAtt(tagName,attDict):
    return lambda x:findTagAttHelp(x,tagName,attDict)


class lXmlNode:
  def __init__ (self, name, atts={}, text='', parent = None):
    self.name = name
    self.text = text
    self.children = []
    self.atts = atts
    self.parent = parent

  def addChild(self, name, atts={}, text=''):
    node = lXmlNode(name, atts, text, self)
    self.children.append(node)
    return node

  def _attString (self):
    return ''.join([' %s=%s'%(n,xml.sax.saxutils.quoteattr(v)) for (n,v) in self.atts.items()])

  def toXml(self, indent = ''):
    if len(self.children) or len(self.text.strip()):
      if not len(self.children):
          return indent +'<%s%s>%s</%s>\n'%(self.name,self._attString(),xml.sax.saxutils.escape(self.text),self.name)
      else:
          xmlStr = indent +'<%s%s>\n'%(self.name,self._attString())
          if len(self.text.strip()):
            xmlStr += indent +xml.sax.saxutils.escape(self.text)+'\n'
          for i in self.children:
            xmlStr += i.toXml(indent+'  ')
          return xmlStr + indent+'</%s>\n'%self.name
    else:
      return indent +'<%s%s/>\n'%(self.name,self._attString())
  def toXmlUgly(self):
    if len(self.children) or len(self.text.strip()):
      xmlStr = '<%s%s>'%(self.name,self._attString())
      if len(self.text.strip()):
        xmlStr += xml.sax.saxutils.escape(self.text)
      for i in self.children:
        xmlStr += i.toXmlUgly()
      return xmlStr + '</%s>\n'%self.name
    else:
      return '<%s%s/>\n'%(self.name,self._attString())

  def path (self, *p):
    if len(p) == 0:
      return self
    try:
      if types.FunctionType != type(p[0]):
        compareFunc = lambda x: x.name == p[0]
      else:
        compareFunc = p[0]
      if len(p) == 1:
        return [x for x in self.children if compareFunc(x)]
      elif type(p[1]) in (types.StringType,types.FunctionType) :
        return [x for x in self.children if compareFunc(x)] [0].path(*p[1:])
      else:
        return [x for x in self.children if compareFunc(x)] [p[1]].path(*p[2:])
    except IndexError:
      raise KeyError('Unable to find tag %s' % p[0])

  def testPath (self, *p):
    try:
      result = self.path(*p)
      return result
    except KeyError:
      return None

  def compare(self,other,indent=''):
    if self.name != other.name:
        print 'err'
        return
    if self.text.strip() != text.strip():
        print 'err'
    
    bothAtts = [x for x in self.atts.keys() if other.atts.has_key(x)]
    bothAtts = [x for x in bothAtts if self.atts[x] != other.atts[x]]
    myAtts = [x for x in self.atts.keys() if not other.atts.has_key(x)]
    otherAtts = [x for x in other.atts.keys() if not self.atts.has_key(x)]
    
    myChild = self.children[:]
    otherChild = other.children[:]
    
      
  def __repr__ (self):
    return '<lXmlNode '+`self.name`+'>'
  def __str__ (self):
    return self.name


class lHandler(xml.sax.handler.ContentHandler):
  def __init__(self):
    self.current = None
    self.base = None
  def startElement(self,name,attrs):
#    print 'Start ->',name,attrs.items()
    if not self.current:
      self.current = lXmlNode(str(name),dict( [(str(n),str(v)) for n,v in attrs.items()] ) )
      self.base = self.current
    else:
      self.current = self.current.addChild(str(name),dict( [(str(n),str(v)) for n,v in attrs.items()] ) ) 
  def endElement(self, name):
    self.current = self.current.parent
  def characters(self, content):
    try:
        self.current.text += str(content)
    except UnicodeEncodeError,v:
        print v
        print `content`
    
from HTMLParser import HTMLParser
class MyHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.current = lXmlNode('/',{} )
        self.base = self.current

    def handle_starttag(self, tag, attrs):
        print "open",tag
        self.current = self.current.addChild(str(tag),dict( [(str(n),str(v)) for n,v in attrs] ) ) 

    def handle_endtag(self, tag):
        print 'END',tag
        if tag == self.current.name:
            self.current = self.current.parent
        else:
            print 'Unmatched end tag'

    def handle_data(self, data):
        self.current.text += str(data)

    

def parseString (string):
  handler = lHandler()
  xml.sax.parseString(string,handler)
  return handler.base

def parseHtmlString (string):
  parser = MyHTMLParser()
  parser.feed(string)
  parser.close()
  return parser.base
  

if __name__=='__main__':
  print 'Test 1:'
  xmlString = '''<ot> s<tag1/>ome text <it fn='profile'> <parm value='12344' type='1'/> <parm type='1' value='12332'/> </it> <tag2/> </ot> '''
  base = parseString(xmlString)
  print base.toXml()
  print base.path('it',0).atts['fn']
  print base.path('it','parm',1).atts['value']=='12332'
  print [x.atts['value'] for x in base.path('it','parm')]
  print base.path('it',lambda x: x.name =='parm' and x.atts.get('value')=='12332',0)
  print base.path('it',findTagAtt('parm',{'type':'1','value':'12332'}),0)
  print base.path('it',findTagAtt('parm',{'type':'1','value':'123321111'}),0)

  print 'Test 2:'
  xmlString = '''<?xml version="1.0" encoding="ISO-8859-1"?><VODACOM_MESSAGE version="1.0"><MESSAGE_HEADER direction="RSP"><SRC_SYSTEM>WE2</SRC_SYSTEM><SRC_APPLICATION>WE2</SRC_APPLICATION><SERVICING_SYSTEM>ICP</SERVICING_SYSTEM><ACTION_CODE>QRY</ACTION_CODE><SERVICE_CODE>GEN</SERVICE_CODE><MESSAGE_ID>WE2200510250644117767950</MESSAGE_ID><MESSAGE_EXPIRY><VALIDITY unit="millisecond">84600000</VALIDITY><ACTION type="DISCARD"/><RESPONSE_REQUIRED>N</RESPONSE_REQUIRED></MESSAGE_EXPIRY><TOKEN>D0PJv8DBPt3h9iyHEtcU</TOKEN><USER_NAME>ICAP</USER_NAME><REQUEST_TIME_STAMP date="25102005" time="064355.620"/><MESSAGE_STATUS><STATUS>SUCCESS</STATUS><CODE>000</CODE></MESSAGE_STATUS><RESPONSE_TIME_STAMP date="25102005" time="064355.912"></RESPONSE_TIME_STAMP></MESSAGE_HEADER><MESSAGE_BODY><ICAP_RESPONSE version="1.0"><FUNCTION highspeed="false" name="getTwoYearWarrenty"><RESULT><![CDATA[iCapException: Error in executing the performFunction iCap call. Received SP_ERROR_INVALID_SECURITY error and failed re-authenticating: Error logging in to iCap SP Interface with username [WE2U4SB1], [SPInterfaceErrorCode=Error while trying to log on to security server: User Already Logged On]: Error while trying to log on to security server: User Already Logged On]]></RESULT></FUNCTION></ICAP_RESPONSE></MESSAGE_BODY></VODACOM_MESSAGE>'''
  base = parseString(xmlString)
  print base.toXml()

  print 'Test 3: Must except'
  xmlString = '''<?xml version="1.0" encoding="ISO-8859-1"?><ODACOM_MESSAGE version="1.0"><MESSAGE_HEADER direction="RSP"><SRC_SYSTEM>WE2</SRC_SYSTEM><SRC_APPLICATION>WE2</SRC_APPLICATION><SERVICING_SYSTEM>ICP</SERVICING_SYSTEM><ACTION_CODE>QRY</ACTION_CODE><SERVICE_CODE>GEN</SERVICE_CODE><MESSAGE_ID>WE2200510250644117767950</MESSAGE_ID><MESSAGE_EXPIRY><VALIDITY unit="millisecond">84600000</VALIDITY><ACTION type="DISCARD"/><RESPONSE_REQUIRED>N</RESPONSE_REQUIRED></MESSAGE_EXPIRY><TOKEN>D0PJv8DBPt3h9iyHEtcU</TOKEN><USER_NAME>ICAP</USER_NAME><REQUEST_TIME_STAMP date="25102005" time="064355.620"/><MESSAGE_STATUS><STATUS>SUCCESS</STATUS><CODE>000</CODE></MESSAGE_STATUS><RESPONSE_TIME_STAMP date="25102005" time="064355.912"></RESPONSE_TIME_STAMP></MESSAGE_HEADER><MESSAGE_BODY><ICAP_RESPONSE version="1.0"><FUNCTION highspeed="false" name="getTwoYearWarrenty"><RESULT><![CDATA[iCapException: Error in executing the performFunction iCap call. Received SP_ERROR_INVALID_SECURITY error and failed re-authenticating: Error logging in to iCap SP Interface with username [WE2U4SB1], [SPInterfaceErrorCode=Error while trying to log on to security server: User Already Logged On]: Error while trying to log on to security server: User Already Logged On]]></RESULT></FUNCTION></ICAP_RESPONSE></MESSAGE_BODY></VODACOM_MESSAGE>'''
  base = parseString(xmlString)
  print base
    