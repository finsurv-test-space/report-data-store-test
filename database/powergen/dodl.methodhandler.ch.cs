<%
import settings
def typeMapping(column):
    type = column.type
    if type.find('(') > 0:
        type = type[:type.find('(')]
    type = type.strip().lower()
    r = { 
      'varchar'	:'String'
     ,'int'		:'Int32'
     ,'datetime':'DateTime'
     ,'bit'		:'Boolean'
     ,'text'	:'String'
     ,'char'	:'Char'
     ,'integer' :'Int32'
     ,'uniqueidentifier' :'Int32'
     ,'smallint':'Int16'
     ,'float'   :'Double'
     ,'money'   :'Decimal'
     ,'decimal'   :'Decimal'
    }[type]
    if column.nullable and r in ['DateTime','Int16','Int32','Boolean','Double','Decimal']:
        r += '?'
    return r
%>
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Security.Cryptography;

using Aorta;
using Aorta.Web.Services;
using Codegen.ASE;
using Codegen.DODL;
using Gen.DB;
using Common;

namespace Codegen.DODL
{
  [AortaService]
  public partial class DODLMethodHandler : WSDODLBase
  {
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(DODLMethodHandler));
    private AortaHibernateTH threadHandler = null;

    public override void doInitialiseService()
    {
      base.doInitialiseService();
      threadHandler = (AortaHibernateTH)base.AortaThread;
    }

    #for $do in $dos
      #if do.schema.name != 'EmptyTable'
    protected override $(do.name)Def Get$(do.name)Imp($typeMapping($do.schema.primaryKey.columns[0]) pk)
    {
      $settings.db_class_prefix$do.schema.name dbObject = threadHandler.session.Load<$settings.db_class_prefix$do.schema.name>(pk);
      $(do.name)Def result = new $(do.name)Def(dbObject);
      return result;
    }
    protected override $(do.name)Def Save$(do.name)Imp($(do.name)Def def)
    {
      // Save the object to the DB via hibernate and then reload it to return to the client, we could also just have completed the ID's in the DEF and returned the original
      //
      $settings.db_class_prefix$do.schema.name dbObject = def.Save(threadHandler.session);
      $(do.name)Def result = new $(do.name)Def(dbObject);
      return result;
    }
      #else:
    protected override $(do.name)Def Get$(do.name)Imp()
    {
      $(do.name)Def result = new $(do.name)Def(threadHandler.session);
      return result;
    }
    protected override $(do.name)Def Save$(do.name)Imp($(do.name)Def def)
    {
      def.Save(threadHandler.session);
      $(do.name)Def result = new $(do.name)Def(threadHandler.session);
      return result;
    }
      #end if
      #for field in do.method_fields
    protected override $(do.name)Def $(do.name)_$(field.name)Imp($(do.name)Def def)
    {
      Page_Service.Session session = new Page_Service.Session(threadHandler.session,((AortaHibernateAH)AortaApplication).SqlTimeout, threadHandler.Credentials, threadHandler);
      $(do.name)Def result = def.$(field.name)(session);
      return result;
    }
      #end for
    #end for

  }
}
