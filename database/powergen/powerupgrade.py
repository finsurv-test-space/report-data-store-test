import settings,sqltools

def main(options_file,upgrade_path):
    from optparse import OptionParser

    parser = OptionParser(usage="usage: %prog [options] upgrade|create|printsettings")
    parser.add_option("-g", "--host", dest="host",
                  help="Target db host", default='(local)')
    parser.add_option("-d", "--database", dest="database",
                  help="Target db", default='PowerGen')
    parser.add_option("-u", "--user", dest="user",
                  help="Target db user name", default='sa')
    parser.add_option("-p", "--password", dest="password",
                  help="Target db user password", default='db')
    parser.add_option("-s", "--settings", dest="settings",
                  help="Use FILE as the settings file", default=options_file, metavar="FILE")
    parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="Don't print status messages to stdout")
    parser.add_option("-N", "--new",
                  action="store_true", dest="new", default=False,
                  help="Drop all tables in current database")

    options, args = parser.parse_args()
#    print 'Options',options
#    print 'Args',args

    settings.load(options.settings)
    #max(upgrade_path.keys())+1
    try:
        if len(args) == 0:
            parser.print_help()
        elif args[0].lower() == 'upgrade':
            sqltools.db_func_wrapper(sqltools.upgrade,options.host,options.database,options.user,options.password,upgrade_path=upgrade_path)
        elif args[0].lower() == 'create':
            sqltools.db_func_wrapper(sqltools.create,options.host,options.database,options.user,options.password,options.new,version=max(upgrade_path.keys())+1)
        elif args[0].lower() == 'printsettings':
            settings.print_settings(args[1:])
        else:
            print 'Unknown command. Sorry for you.'
    except EnvironmentError,v:
        print v
    except str,v:
        print v

    raw_input('-'*40+'\nDone\n')
    
if __name__=='__main__':
    main()

