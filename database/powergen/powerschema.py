import clxml,re,os,sys,zipfile
import settings


class NamedXmlObject:
    global_id = 0
    def __init__(self,name):
        self.id = PyObject.global_id
        PyObject.global_id += 1
        self.name = name
    def get_id(self):
        return self.id
    def get_name(self):
        return self.name.strip()

class MwbXmlObject:
    def __init__(self,xml):
        self.xml = xml
    def get_id(self):
        return self.xml.atts['id']
    def get_name(self):
        return self.xml.path(clxml.findTagAtt('value',{'type':"string",'key':"name"}),0).text.strip()
    def get_comment(self):
        return self.xml.path(clxml.findTagAtt('value',{'type':"string",'key':"comment"}),0).text.strip()

class MwbXmlColumn(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_type(self):
        return self.xml.path(clxml.findTagAtt('link',{'key':"simpleType"}),0).text.strip().split('.')[-1]
    def get_length(self):
        if self.get_type() == 'decimal':
            length = int(self.xml.testPath(clxml.findTagAtt('value',{'type':"int" ,'key':"precision"}),0).text)
            return 20 if length == -1 else length
        else:
            return int(self.xml.testPath(clxml.findTagAtt('value',{'type':"int" ,'key':"length"}),0).text)
    def get_scale(self):
        if self.get_type() == 'decimal':
            scale = int(self.xml.testPath(clxml.findTagAtt('value',{'type':"int" ,'key':"scale"}),0).text)
            return 2 if scale == -1 else scale
        return 0
    def get_nullable(self):
        return self.xml.testPath(clxml.findTagAtt('value',{'type':"int" ,'key':"isNotNull"}),0).text.strip() == '0'
    def get_auto_increment(self):
        return self.xml.testPath(clxml.findTagAtt('value',{'type':"int" ,'key':"autoIncrement"}),0).text.strip() == '1'
    def get_default(self):
        default = self.xml.testPath(clxml.findTagAtt('value',{'type':"string" ,'key':"defaultValue"}),0)
        if default:
            default = default.text.strip()
        return default
        
class MwbXmlKey(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_column_ids(self):
        return [x.path(clxml.findTagAtt('link',{'key':"referencedColumn"}),0).text for x in 
                    self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"columns"}),
                                  clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.IndexColumn"}))]

class MwbXmlIndex(MwbXmlKey):
    def __init__(self,xml):
        MwbXmlKey.__init__(self,xml)
    def get_unique(self):
        return self.xml.path(clxml.findTagAtt('value',{'type':"int" ,'key':"unique"}),0).text == '1'
        
class MwbXmlTrigger(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_trigger_text(self):
        return 'No implemented yet'

class MwbXmlTable(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_columns(self):
        return [MwbXmlColumn(i) for i in self.xml.path(clxml.findTagAtt('value',{'key':"columns"}),
                                                       clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Column"}))]
    def get_keys(self):
        return [MwbXmlKey(i) for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"indices"}),
                                                    clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Index"}))]
    def get_primary_key_id(self):
        return self.xml.path(clxml.findTagAtt('link',{'type':"object",'key':"primaryKey"}),0).text
    def get_indexes(self):
        return [MwbXmlIndex(i) for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"indices"}),
                                                      clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Index"}))]
    def get_triggers(self):
        return [MwbXmlTrigger(i) for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"triggers"}),
                                                        clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Trigger"}))]
    def get_insert_statements(self):
        result = self.xml.testPath(clxml.findTagAtt('value',{'type':"string",'key':"inserts"}),0)
        return result.text if result else ''
        #return self.xml.path(clxml.findTagAtt('value',{'type':"string",'key':"inserts"}),0).text
                                                        
class MwbXmlReference(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_referenced_column_id(self):
        return self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"referencedColumns"}),
                                  clxml.findTagAtt('link',{'type':"object"}),0).text
    def get_referencing_column_id(self):
        return self.xml.path(clxml.findTagAtt('value',{'type':'list','key':"columns"}),
                                  clxml.findTagAtt('link',{'type':"object"}),0).text

class MwbXmlView(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)
    def get_view_sql(self):
        return 'Not implemented yet'
    def get_columns(self):
        return []

class MwbXmlSchema(MwbXmlObject):
    def __init__(self,xml):
        MwbXmlObject.__init__(self,xml)

    def get_tables(self):
        return [MwbXmlTable(i) for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':'tables'}),
                                  clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Table"}) )]
    def get_references(self):
        referenceList = []
        for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':'tables'}),
                                  clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Table"}) ):
            referenceList.extend(MwbXmlReference(x) for x in i.path(clxml.findTagAtt('value',{'type':'list','key':'foreignKeys'}),
                                        clxml.findTagAtt('value',{'type':'object','struct-name':'db.mysql.ForeignKey'})) )
        return referenceList
    def get_views(self):
        return [MwbXmlView(i) for i in self.xml.path(clxml.findTagAtt('value',{'type':'list','key':'views'}))]

class MwbXmlDoc(NamedXmlObject):
    def __init__(self,file_name):
        NamedXmlObject.__init__(self,file_name)
        mwb_content = zipfile.ZipFile(file_name).read('document.mwb.xml')
        all_xml = clxml.parseString(mwb_content)
        self.schemata = all_xml.path(
                          clxml.findTagAtt('value',{'type':'object', 'struct-name':'workbench.Document'}),
                          clxml.findTagAtt('value',{'type':'list', 'key':"physicalModels"}),
                          clxml.findTagAtt('value',{'type':"object", 'struct-name':"workbench.physical.Model"}),
                          clxml.findTagAtt('value',{'type':"object", 'key':"catalog"}),
                          clxml.findTagAtt('value',{'type':"list", 'key':"schemata"}),0)
    def get_schemas(self):
        return [MwbXmlSchema(i) for i in self.schemata.path(clxml.findTagAtt('value',{'type':"object", 'struct-name':"db.mysql.Schema"}) )]

class PdmXmlObject:
    def __init__(self,xml):
        self.xml = xml
    def get_id(self):
        return self.xml.atts['Id']
    def get_name(self):
        return self.xml.path('a:Name',0).text.strip()
    def get_comment(self):
        return self.xml.path('a:Comment',0).text.strip() if self.xml.testPath('a:Comment',0) else ''

class PdmXmlColumn(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_type(self):
        type = self.xml.path('a:DataType',0).text.strip()
        left = type.find('(')
        if left >= 0:
            return type[:left]
        return type
    def get_lengthscale(self):
        type = self.xml.path('a:DataType',0).text.strip()
        left = type.find('(')
        right = type.find(')')
        if left < 0 or right < 0:
            return '-1'
        return type[left+1:right]
    def get_length(self):
        return int(self.get_lengthscale().split(',')[0])
    def get_scale(self):
        ls = self.get_lengthscale().split(',')
        if len(ls) == 2:
            return int(ls[1])
        return None

    def get_nullable(self):
        return not self.xml.testPath('a:Mandatory',0) 
    def get_auto_increment(self):
        return self.xml.testPath('a:Identity',0) 

    def get_default(self):
        default = self.xml.testPath('a:DefaultValue',0)
        if default:
            default = default.text.strip()
        return default
        
class PdmXmlKey(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_column_ids(self):
        return [x.atts['Ref'] for x in self.xml.path('c:Key.Columns','o:Column')]

class PdmXmlIndex(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_column_ids(self):
        columnIds = []
        for outerCol in xml.path('c:IndexColumns','o:IndexColumn'):
            for innerCol in outerCol.path('c:Column','o:Column'):
                columnIds.append(innerCol.atts['Ref'])
        return columnIds
    def get_unique(self):
        return self.xml.testPath('a:Unique',0) != None
        
class PdmXmlTrigger(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_trigger_text(self):
        return self.xml.testPath('a:Text',0).text.replace('%TRIGGER%',self.name).replace('%TABLE%',parent.name)

class PdmXmlTable(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_columns(self):
        return [PdmXmlColumn(i) for i in self.xml.path('c:Columns','o:Column')]
    def get_keys(self):
        return [PdmXmlKey(i) for i in self.xml.path('c:Keys','o:Key')]
    def get_primary_key_id(self):
        return self.xml.path('c:PrimaryKey','o:Key',0).atts['Ref']
    def get_indexes(self):
        return [PdmXmlIndex(i) for i in self.xml.path('c:Indexes','o:Index')]
    def get_triggers(self):
        return [PdmXmlTrigger(i) for i in self.xml.path('c:Triggers','o:Trigger')]
    def get_insert_statements(self):
        return ''

class PdmXmlReference(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_referenced_column_id(self):
        return self.xml.path('c:Joins','o:ReferenceJoin','c:Object1','o:Column',0).atts['Ref']
    def get_referencing_column_id(self):
        return self.xml.path('c:Joins','o:ReferenceJoin','c:Object2','o:Column',0).atts['Ref']

class PdmXmlView(PdmXmlObject):
    def __init__(self,xml):
        PdmXmlObject.__init__(self,xml)
    def get_view_sql(self):
        return self.xml.path('a:SQLQuery',0)
    def get_columns(self):
        return []

class PdmXmlSchema(NamedXmlObject):
    def __init__(self,file_name):
        NamedXmlObject.__init__(self,file_name)
        pdm_content = file(file_name).read()
        self.xml = clxml.parseString(pdm_content)
        self.file_name = file_name
    def get_tables(self):
        return [PdmXmlTable(i) for i in self.xml.path('o:RootObject','c:Children','o:Model','c:Tables','o:Table') ]
    def get_references(self):
        return [PdmXmlReference(x) for x in self.xml.path('o:RootObject','c:Children','o:Model','c:References','o:Reference') ]
    def get_views(self):
        return [PdmXmlView(i) for i in self.xml.path('o:RootObject','c:Children','o:Model','c:Views','o:View')]
    def get_comment(self):
        return ''
        
class PyObject(NamedXmlObject):
    def __init__(self,name):
        NamedXmlObject.__init__(self,name)
    def get_comment(self):
        return ''
        
class PyColumn(PyObject):
    def __init__(self,column_def):
        self.column_def = column_def.split()
        if len(self.column_def) < 2:
            raise 'Untyped column def=[%s]'%column_def
        PyObject.__init__(self,self.column_def[0])
    def get_type(self):
        type = self.column_def[1].strip()
        left = type.find('(')
        if left >= 0:
            return type[:left]
        return type
    def get_lengthscale(self):
        type = self.column_def[1].strip()
        left = type.find('(')
        right = type.find(')')
        if left < 0 or right < 0:
            return '-1'
        return type[left+1:right]
    def get_length(self):
        return int(self.get_lengthscale().split(',')[0])
    def get_scale(self):
        ls = self.get_lengthscale().split(',')
        if len(ls) == 2:
            return int(ls[1])
        return None

    def get_nullable(self):
        return 'null' in [x.lower() for x in self.column_def[1:]]
    def get_auto_increment(self):
        return 'identity' in [x.lower() for x in self.column_def[1:]]
    def get_default(self):
        lowered = [x.lower() for x in self.column_def[1:]]
        if not 'default' in lowered:
            return None
        return self.column_def[lowered.index('default') +2].strip()


class PyKey(PyObject):
    def __init__(self,table,columns):
        PyObject.__init__(self,'PK_'+table.name+'_'.join(columns))
        self.columns = columns
        self.table = table
        if len(self.get_column_ids()) < len(columns):
            raise Exception('Table:%s defines columns [%s] as primary key but they do not map to the table columns'%(table.name,columns))
        #print 'New PyKey',table.name,columns
    def get_column_ids(self):
        return [x.id for x in self.table.columns if x.name in self.columns]

class PyIndex(PyObject):
    def __init__(self,table,columns,unique):
        PyObject.__init__(self,'IDX_'+table.name+'_'.join(columns))
        self.columns = columns
        self.unique = unique
    def get_column_ids(self):
        return [x.id for x in table.columns if x.name in self.columns]
    def get_unique(self):
        return self.unique
        
class PyTrigger(PyObject):
    def __init__(self,xml):
        PyObject.__init__(self,xml)
    def get_trigger_text(self):
        return self.xml.testPath('a:Text',0).text.replace('%TRIGGER%',self.name).replace('%TABLE%',parent.name)

class PyTable(PyObject):
    def __init__(self,name,columns,pk,refs=[]):
        PyObject.__init__(self,name)
        self.columns = [PyColumn(i) for i in columns if len(i.strip())]
        col_names = [x.name for x in self.columns]
        for i in col_names:
            if col_names.count(i) > 1:
                raise 'Duplicated column names=%s in table=%s'%(i,self.name)
        self.keys = [PyKey(self,pk)] 
        self.refs = [PyReference(self,x[0],x[1]) for x in refs]
    def get_columns(self):
        return self.columns
    def get_keys(self):
        return self.keys
    def get_primary_key_id(self):
        return self.keys[0].id
    def get_indexes(self):
        return []
    def get_triggers(self):
        return []
    def get_insert_statements(self):
        return ''

class PyReference(PyObject):
    def __init__(self,table,from_column_name,to_table_name):
        PyObject.__init__(self,table.name+'_'+from_column_name+'__'+to_table_name)
        self.table = table
        self.from_column_name = from_column_name
        self.to_table_name = to_table_name
    def get_referenced_column_id(self):
        return Table.byName[self.to_table_name.lower()].primaryKey.columns[0].id
    def get_referencing_column_id(self):
        return [x.id for x in self.table.columns if x.name==self.from_column_name][0]

class PyView(PyObject):
    def __init__(self,name,columns,sql):
        PyObject.__init__(self,name)
        self.columns = [PyColumn(i) for i in columns if len(i.strip())]
        self.sql = sql
    def get_view_sql(self):
        return self.sql
    def get_columns(self):
        return self.columns

class PySchema(PyObject):
    def __init__(self,file_name):
        PyObject.__init__(self,file_name)
        context = {}
        exec file(file_name) in context
        self.tables = context['tables']
        self.views = context['views']
    def get_tables(self):
        return self.tables
    def get_references(self):
        refs = []
        for t in self.tables:
            refs += t.refs
        return refs
    def get_views(self):
        return self.views
    def get_comment(self):
        return ''
        
        
class NamedIDObject:
    byId = {}
    byName = {}
    def __init__(self,xml,parent=None):
        self.id = xml.get_id()
        NamedIDObject.byId[self.id] = self
        self.name = xml.get_name()
        self.camelName = self.name[0].lower()+self.name[1:]
        self.pascalName = self.name[0].upper()+self.name[1:]
        self.parent = parent
        NamedIDObject.byName[self.getQualifiedName()] = self
        try:
            #optionLines = [x.split('=') for x in xml.path('a:Comment',0).text.split('\n')]
            self.comment = xml.get_comment()
            # Options are spread through the comments in the following fashion
            # @Option1=5
            # This is more comments
            # @Option2 = 6
            optionLines = [x[1:].split('=') for x in self.comment.split('\n') if x.startswith('@')]
            self.options = dict(x for x in optionLines if len(x)==2) 
            self.comment = '\n'.join([x for x in self.comment.split('\n') if not x.startswith('@')])
        except (KeyError,TypeError):
            self.options = {}
    def getQualifiedName(self):
        if self.parent:
            return self.parent.getQualifiedName()+'.'+self.name.lower()
        return self.name.lower()
    def syntaxWarning(self,warning):
        if not self.options.has_key('IgnoreSyntax'):
            print warning
    def oracleWarning(self,warning):
        if self.options.has_key('OracleSyntax'):
            print warning


class Column(NamedIDObject):
    def __init__(self,xml,parent):
        NamedIDObject.__init__(self,xml,parent)
        self.options.update(parent.options)
        if len(self.name) >= 30:
            self.oracleWarning('Column "%s"  in table %s is longer than 30 characters, which Sucky Oracle(TM) dislikes'%(self.name,parent.name))
        self.type = xml.get_type().lower()
        self.length = xml.get_length()
        self.scale = xml.get_scale()
        self.nullable = xml.get_nullable()
        self.autoIncrement = xml.get_auto_increment()
        self.default = xml.get_default()
        self.outgoingRef = None
        self.incomingRef = []
    def addReference(self, column, schemaRefName):
        if not self.incomingRef.count(column):
            self.incomingRef.append( column )
            column.outgoingRef = self
            column.outgoingRefNameFromSchema = schemaRefName
    def addOutgoingReference(self, column):
        self.outgoingRef = column

    def createUniqueRefNames(self):
        names = [x.parent.camelName for x in self.incomingRef]
        uniqueNames = names[:]
        for i in range(len(self.incomingRef)):
            if names.count(names[i]) > 1:
                uniqueNames[i] = self.incomingRef[i].parent.camelName +'_' +self.incomingRef[i].pascalName
        self.uniqueIncomingRef = uniqueNames
        self.uniquePascalIncomingRef = [i[0].upper() +i[1:] for i in uniqueNames]
        self.uniqueCamelIncomingRef = [i[0].lower() +i[1:] for i in uniqueNames]

class Key(NamedIDObject):
    def __init__(self,xml,parent):
        NamedIDObject.__init__(self,xml,parent)
        self.columnIds = xml.get_column_ids()
        self.columns = [Column.byId[x] for x in self.columnIds]
class FakeKey(NamedIDObject):
    def __init__(self,columns):
        self.columns = columns

class Index(NamedIDObject):
    def __init__(self,xml,parent):
        NamedIDObject.__init__(self,xml,parent)
        self.columnIds = xml.get_column_ids()
        self.columns = [Column.byId[x] for x in self.columnIds]
        self.unique = xml.get_unique()
        #print 'EndIndex:',self.name


class Trigger(NamedIDObject):
    def __init__(self,xml,parent):
        NamedIDObject.__init__(self,xml,parent)
        self.text = xml.testPath('a:Text',0).text.replace('%TRIGGER%',self.name).replace('%TABLE%',parent.name)
        #print 'EndIndex:',self.name

        
class EmptyTable:
    def __init__(self):
        self.name = 'EmptyTable'
        self.options = {}
        self.columns = []
        self.keys = []
        self.keys = [ FakeKey( [] ) ]
        self.primaryKey = self.keys[0]
        self.indexes = []
        self.triggers = []
        self.insert_statements = ''
        self.insert_data = []
        
class Table(NamedIDObject):
    def __init__(self,xml, options, parent):
        NamedIDObject.__init__(self,xml,parent=parent)
        if len(self.name) >= 30:
            self.oracleWarning('Table %s in schema %s is longer than 30 characters, which Sucky Oracle(TM) dislikes'%(self.name,parent.name))
        self.options.update(options)
        self.columns = [Column(i,self) for i in xml.get_columns()]
        try:
            self.keys = [Key(i,self) for i in xml.get_keys()]
            self.primaryKeyID = xml.get_primary_key_id()
            self.primaryKey = Key.byId[self.primaryKeyID]
        except KeyError,v:
            print 'SCHEMA STRUCTURE WARNING: key trouble for table %s (Dict KeyError=%s)'%(self.name,v)
            #self.keys = [ FakeKey( [self.columns[0]] ) ]
            #self.primaryKey = self.keys[0]
            self.primaryKey = FakeKey( [] )
        try:
            self.indexes = [Index(i,self) for i in xml.get_indexes()]
        except KeyError:
            self.indexes = []
        try:
            self.triggers = [Trigger(i,self) for i in xml.get_triggers()]
        except KeyError:
            self.triggers = []
        self.insert_statements = xml.get_insert_statements()
        self.insert_data = parse_inserts(self.insert_statements,len(self.columns))

        test_table_name = self.name if not self.name.startswith('#') else self.name[1:]
        #print test_name
        #if len(test_table_name) < 4 or (test_table_name[2] != '_' and len(test_table_name) > 3 and test_table_name[3] != '_' and len(test_table_name) > 4 and test_table_name[4] != '_'):
        #    self.syntaxWarning('SYNTAX WARNING: Table %s does not conform to the XX_Name or XXYY_Name convention '%(self.name))
        if test_table_name.find('#') >= 0:
            self.syntaxWarning('SYNTAX WARNING: Table %s name contains an "#"'%(self.name))
        if len(self.columns) == 0:
            self.syntaxWarning('SYNTAX WARNING: Table %s has no columns'%(self.name))
        #elif len(self.primaryKey.columns) == 1 and self.primaryKey.columns[0].type == 'int':
        #    pk_name = self.primaryKey.columns[0].name
        #    if pk_name != test_table_name[test_table_name.find('_')+1:] +'ID':
        #        self.syntaxWarning('SYNTAX WARNING: Table %s has a single int primary key (%s) that does not conform to XX_Name.NameID convention'%(self.name,pk_name))
            
        
    def processOptions(self):
        #parse reference options looking like 'Ref':'thisColumn,targetTable'
        for i in [self.options[x] for x in self.options if x.lower() == 'ref']:
            #print 'Adding option reference on table %s:%s'%(self.name,i)
            thisColumnName,targetTableName = i.lower().split(',')
            thisColumn = [x for x in self.columns if x.name.lower() == thisColumnName]
            if len(thisColumn) != 1:
                raise 'Unable to find column=[%s] on table=[%s] for ref option=[%s]'%(thisColumnName,self.name,i)
            targetTable = Table.byName[targetTableName]
            #print 'adding ref to',thisColumn[0].getQualifiedName(),'to',targetTable.primaryKey.columns[0].getQualifiedName()
            targetTable.primaryKey.columns[0].addReference(thisColumn[0], None)

    def getQualifiedName(self):
        if self.parent and self.name.startswith('#'):
            return self.parent.getQualifiedName()+'.'+self.name.lower()
        return self.name.lower()
                

class Reference(NamedIDObject):
    def __init__(self,xml,parent):
      try:
        NamedIDObject.__init__(self,xml,parent)
        try:
            self.column1Id = xml.get_referenced_column_id()
            self.column2Id = xml.get_referencing_column_id()
            self.codeName = 'ERROR'
            self.complete = False
        except IndexError:
            print 'ERROR: Cannot get column id''s on ReferenceName=[%s]'%(self.name)
            raise
        
        try:
            self.column1 = Column.byId[self.column1Id]
            self.resolve_referenced_column()
            self.column2 = Column.byId[self.column2Id]

            self.codeName = '%s_%s__%s'%(self.column2.parent.name,self.column2.name,self.column1.parent.name)
            self.column1.addReference(self.column2,self.codeName)
            self.column2.addOutgoingReference(self.column1)
            self.complete = True
        except KeyError,v:
            print 'ERROR: ReferenceName=[%s] Column1ID=%s, Column2ID=%s  (KeyError=%s)'%(self.name,str(self.column1Id),str(self.column2Id),str(v))
            
      except KeyError,v:
        print 'Exception parsing ref with id=%s, name=%s'%(self.id,self.name)
        raise

    def resolve_referenced_column(self):
        try:
            # Check if we are pointing to a reference table, if so try to find the real one]
            #
            if self.column1.parent.name.startswith('#'):
                new_target = Table.byName[self.column1.parent.name[1:].lower()]
                self.column1 = new_target.primaryKey.columns[0]
                self.column1Id = self.column1.id
        except KeyError,v:
            print 'Error trying to resolve referenced column, error trying to do table lookup with [%s]'%(str(v))
            raise v
        
class View(NamedIDObject):
    def __init__(self,xml,parent):
        NamedIDObject.__init__(self,xml,parent)
        self.sql = xml.get_view_sql()
        self.columns = [Column(i,self) for i in xml.get_columns()]
        #print 'View',self.name,self.getQualifiedName()
        #self.keys = [Key(i,self) for i in xml.get_keys()]
         
        self.keys = [ FakeKey( [] ) ]
        self.primaryKey = self.keys[0]
    def getQualifiedName(self):
        if self.parent and self.name.startswith('#'):
            return self.parent.getQualifiedName()+'.'+self.name.lower()
        return self.name.lower()

class Schema(NamedIDObject):
    def __init__(self,xml):
        NamedIDObject.__init__(self,xml)
        
        
        
def loadSchema():
    tables,references,views = [],[],[]
    xml_schemas = []
    for schema_file in settings.import_schema_file:
        print 'Loading schema file:',schema_file
        if schema_file.endswith('.pdm'):
            xml_schema = PdmXmlSchema(schema_file)
            schema = Schema(xml_schema)
            xml_schemas.append((xml_schema,schema))
        elif schema_file.endswith('.mwb'):
            xml_doc = MwbXmlDoc(schema_file)
            for xml_schema in xml_doc.get_schemas():
                xml_schemas.append((xml_schema,Schema(xml_schema)))
        elif schema_file.endswith('.py'):
            xml_schema = PySchema(schema_file)
            schema = Schema(xml_schema)
            xml_schemas.append((xml_schema,schema))
        else:
            raise Exception('Only support for py, pdm and mwb file formats for database schema files')
            
    for xml_schema,schema in xml_schemas:
        tables.extend( [Table(i,settings.getOptions(i.get_name()),schema) for i in xml_schema.get_tables() ] )
        try:
            views.extend( [View(i,schema) for i in xml_schema.get_views()] )
        except KeyError,v:
            views = []
            #print 'No views'
    
    # Do references only after all columns have been created
    for xml_schema,schema in xml_schemas:
        references.extend( [Reference(i,schema) for i in xml_schema.get_references()] )

    tables = [x for x in tables if not x.name.startswith('#')]
        
    for i in tables:
        i.processOptions()
    for i in tables:
        for j in i.columns:
            j.createUniqueRefNames()
    return tables,references,views

 
def parse_inserts(inserts,column_count):
    if not inserts.strip():
        return []
    lines = inserts.strip().split('\n')
    rows = []
    r = r"VALUES \("+", *".join("'(.*?)'" for i in range(column_count)) +"\)"
    pattern = re.compile(r)
    for line in lines:
        m = pattern.search(line)
        if m:
            rows.append(m.groups())
    return rows
         
def main():
    inserts = '''INSERT INTO `DF_QErrorType` (`ErrorCode`, `Name`) VALUES ('VALIDATION', 'Validation');
INSERT INTO `DF_QErrorType` (`ErrorCode`, `Name`) VALUES ('IMPORT_EXCEPTION', 'Exception while inserting the data into the database');'''
    print 'Parse search test succeeded:',`parse_inserts(inserts,2)` == '''[('VALIDATION', 'Validation'), ('IMPORT_EXCEPTION', 'Exception while inserting the data into the database')]'''
    
if __name__=='__main__':
    main()


