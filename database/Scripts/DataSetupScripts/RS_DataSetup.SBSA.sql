USE [ReportServices]
GO

DECLARE @password varchar(100)
--  ** NOTE **
--  To hash a password you can use the utility end point (no auth required): http://localhost:8083/report-service/utils/hash?plaintext=abc
--.... or ...See the utilities end-point on the report-data-store API Swagger interface to make use of the hash end-point
--IE: abc -> $2a$15$HxessqLSiOCLqFuGhKGVeeWrHzL/b1vtJyoxs.rqsYyOXjbGMaKf.
--Therefore, the hashed "abc" password can be stored in the DB...
SET @password = '$2a$15$HxessqLSiOCLqFuGhKGVeeWrHzL/b1vtJyoxs.rqsYyOXjbGMaKf.';
--"dev" password hash:
--SET @password = '$2a$15$oG3scaAWCmA6eohYUgxpQ.IF8PaEXePki9P88z6DvGMZJnHs8fnbq';
--"test" password hash:
--SET @password = '$2a$15$XyMcylzyQCBYYxzTRY4/8OiUuv0RoxiaR6L2d4c2OPeFdPALcYyKq';
--"uat" password hash:
--SET @password = '$2a$15$ZLOuwD1BAslZn.YgeytTFujASAzhtgga2i/kyxO/OqHno3ev5a5k.';


--Create the access sets
DELETE FROM [AccessSet]
INSERT INTO [AccessSet]([GUID], [Key], [AuthToken], [Version], [Content])
VALUES (NEWID(), 'unrestrictedZA', NULL, 1, '{ "name":"unrestrictedZA", "reportSpace": "SARB", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedMW', NULL, 1, '{ "name":"unrestrictedMW", "reportSpace": "RBM", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedLS', NULL, 1, '{ "name":"unrestrictedLS", "reportSpace": "CBL", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedNA', NULL, 1, '{ "name":"unrestrictedNA", "reportSpace": "BON", "read": [], "write": [] }')


DECLARE @channelList varchar(max)
SET @channelList = '"stdSARB","stdBON","stdRBM","stdBankLibra","sbZA","sbNA","sbMW","sbLS","sbLSEBank","sbTradeSuite","sbRAVN"';

DECLARE @accessList varchar(max)
SET @accessList = '"unrestrictedZA","unrestrictedNA","unrestrictedMW","unrestrictedLS"';

--DELETE FROM [User]
--Create the users
INSERT INTO [User]([GUID], [Key], [AuthToken], [Version], [Content])
VALUES (NEWID(), 'consumer', NULL, 1, '{ "username":"consumer", "password":"'+@password+'", "rights":"USER, CONSUMER_API", "access": ['+@accessList+'], "channels":['+@channelList+'] }')
  ,(NEWID(), 'gateway', NULL, 1, '{ "username":"gateway", "password":"'+@password+'", "rights":"USER, PRODUCER_API", "access": ['+@accessList+'], "channels":['+@channelList+'] }')
  ,(NEWID(), 'jwtuser',    NULL, 1, '{ "username":"jwtuser", "password":"'+@password+'", "rights":"USER, PRODUCER_API", "access": ['+@accessList+'], "channels":['+@channelList+'] }')

--  ,(NEWID(), 'producer', NULL, 1, '{ "username":"producer", "password":"'+@password+'", "rights":"USER, PRODUCER_API", "access": ['+@accessList+'], "channels":['+@channelList+'] }')
--  ,(NEWID(), 'jwtuser',    NULL, 1, '{ "username":"jwtuser", "password":"'+@password+'", "rights":"USER, PRODUCER_API, CONSUMER_API, INTERNAL_API, VIEW_USER_DETAILS, SET_USER_DETAILS, SET_ACCESS_SETS, VIEW_ACCESS_SETS, VIEW_ACCOUNTHOLDERS", "access": ['+@accessList+'], "channels":['+@channelList+'] }')

