USE [ReportServices]
GO
/*
THIS SCRIPT IS USED TO ADD A NEW CHANNEL TO THE SPECIFIED USER
----------------------------------------------------------------------
*/

BEGIN TRANSACTION AddChannelToUser
    DECLARE @userToEdit VARCHAR(MAX)
    DECLARE @channelToAdd VARCHAR(MAX)
    DECLARE @commitTransaction INT

    /*
      SPECIFY THE USER TO EDIT AND THE CHANNEL TO BE ADDED THERETO...
    */
    SET @userToEdit = 'superuser';
    SET @channelToAdd = 'sbNAMoby';
    /*
      COMMIT TRANSACTION (1)  /  ROLLBACK TRANSACTION (0)
    */
    SET @commitTransaction = 0;

    ----------------------------------------------------------------------

    IF EXISTS(
			SELECT [Key] AS 'USER', JSON_QUERY([Content], '$.channels') AS 'CHANNELS'
			FROM [ReportServices].[dbo].[UserView]
			WHERE [Key] = @userToEdit
				AND NOT (''+ISNULL(JSON_QUERY([Content], '$.channels'),'')+'' LIKE ('%'+@channelToAdd+'%'))
		)
	BEGIN
		GOTO UPDATE_USER
	END
	ELSE
	BEGIN
		GOTO SKIP_USER
	END


    ----------------------------------------------------------------------
	SKIP_USER:
    PRINT 'USER ('+@userToEdit+') ALREADY CONTAINS CHANNEL ('+@channelToAdd+') OR DOES NOT EXIST...'

	PRINT ''
	PRINT '...LISTING DETAILS FOR USER ('+@userToEdit+')...'
    SELECT [Key] AS 'USER', JSON_QUERY([Content], '$.channels') AS 'CHANNELS'
    FROM [ReportServices].[dbo].[UserView]
    WHERE [Key] = @userToEdit

	PRINT ''
	PRINT '...ABORTING UPDATE.'
	GOTO UNDO_TRANSACTION

    ----------------------------------------------------------------------
	UPDATE_USER:
    PRINT 'ADDING CHANNEL ('+@channelToAdd+') TO USER ('+@userToEdit+')...'
    PRINT ''


    PRINT ''
    PRINT 'BEFORE UPDATE: CHANNELS FOR USER ('+@userToEdit+'):'
    SELECT [Key] AS 'USER', JSON_QUERY([Content], '$.channels') AS 'CHANNELS'
    FROM [ReportServices].[dbo].[UserView]
    WHERE [Key] = @userToEdit

    ----------------------------

    PRINT ''
    PRINT 'INSERTING UPDATED USER RECORD...'
    INSERT INTO [User]([GUID], [Key], [AuthToken], [Version], [Content])
    SELECT
		NEWID() as 'guid',
		[Key] as 'user',
		NULL as 'auth',
		([Version]) as 'version',
		'{ "username":"'+[Key]+'", "password":"'+JSON_VALUE([Content], '$.password')+'", "rights":"'+ISNULL(JSON_VALUE([Content], '$.rights'),'')+'", "access": '+ISNULL(JSON_QUERY([Content], '$.access'),'')+', "channels":'+JSON_MODIFY(ISNULL(JSON_QUERY([Content], '$.channels'), '[]'), 'append $', @channelToAdd)+' }' as 'NewContent'
	FROM
		[ReportServices].[dbo].[UserView]
	WHERE
		[Key] = @userToEdit
	ORDER BY
		[Key],
		CreatedDateTime DESC
	PRINT '...UPDATED USER RECORD INSERTED.'
	PRINT ''

    ----------------------------

    PRINT ''
    PRINT 'AFTER UPDATE: CHANNELS FOR USER ('+@userToEdit+'):'
    SELECT [Key] AS 'USER', JSON_QUERY([Content], '$.channels') AS 'CHANNELS'
    FROM [ReportServices].[dbo].[UserView]
    WHERE [Key] = @userToEdit
    PRINT ''

    PRINT ''
    PRINT 'CURRENT USERS AND ASSOCIATED CHANNELS:'
    SELECT [Key] AS 'USER', JSON_QUERY([Content], '$.channels') AS 'CHANNELS'
    FROM [ReportServices].[dbo].[UserView]
	--WHERE [Key] = @userToEdit
    PRINT ''

	GOTO DO_COMMIT

----------------------------------------------------------------------
/*
  TO COMMIT, OR NOT TO COMMIT...

  ...THAT IS THE QUESTION.
*/
DO_COMMIT:
IF (@commitTransaction = 1)
BEGIN
    GOTO COMMIT_TRANSACTION
END
ELSE
BEGIN
    GOTO UNDO_TRANSACTION
END

COMMIT_TRANSACTION:
COMMIT TRANSACTION AddChannelToUser
PRINT ''
PRINT '...TRANSACTION COMMITTED.'
PRINT ''
GOTO END_TRANSACTION

UNDO_TRANSACTION:
ROLLBACK TRANSACTION AddChannelToUser
PRINT ''
PRINT '...TRANSACTION ROLLED BACK.'
PRINT ''
GOTO END_TRANSACTION


END_TRANSACTION:
print '/************************ SCRIPT COMPLETE **************************/'
