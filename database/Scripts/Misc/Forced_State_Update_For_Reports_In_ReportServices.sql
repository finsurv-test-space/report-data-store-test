/*

  This SQL script can be used to explicitly set the state for a list of given transactions.

  To use this script, you will need to:
    1) update the value for the @NewState variable - to the target State that you would like
    2) update the value for the @ReportSpace variables to the target report space on which you would like to action the reports
    3) Stipulate a list of ReportKeys to action in the statement below (look for the comment)

  NOTE:  This script is used at your own risk!
  It may result in lifecycle events being triggered (depending on the configuration of your system).
  It may also cause unforseen issues if invalid reports are forced into a download queue (or some such state) where
  they may be consumed by another application - such as the tXstream Submission System.

*/

USE [ReportServices]

DECLARE @NewState NVARCHAR(10);
SET @NewState = 'End'

DECLARE @ReportSpace NVARCHAR(10);
SET @ReportSpace = 'SARB'

BEGIN TRANSACTION STATE_UPDATE

       --CREATE THE TEMP TABLE SNAPSHOT OF CURRENT REPORT DATA ETC...
       SELECT TOP 20
              [GUID] AS 'OLD_GUID',
              NEWID() AS 'GUID',
              [ReportSpace],
              [ReportKey],
              '{ "Username": "datamod", "DateTime": "'+CONVERT(VARCHAR(10), GETDATE(), 120)+'"}' AS 'AuthToken',
              [Version],
              [Schema],
              [Content],
              --JSON_MODIFY(JSON_MODIFY([Content], '$.Meta.SnapShot', NULL), '$.State', @NewState) AS 'Content',
              GETDATE() AS [CreatedDateTime]
       INTO #TempStateUpdateReport
       FROM [ReportView]
       WHERE
       (
              (ISJSON([Content]) > 0 AND JSON_VALUE([Content], '$.State') != @NewState) OR
              (ISJSON([Content]) <= 0)
       ) AND
       /*
              ***  PLEASE INSERT RELEVANT LIST OR SELECT STATEMENT TO DETERMINE THE SET OF REPORT KEYS (TRN REFERENCES) BELOW  ***
       */
       ReportKey in (
              '',
              '',
              ''
       ) AND
       ReportSpace = @ReportSpace


       PRINT 'INVALID JSON FOUND IN THE FOLLOWING RECORDS:'
       SELECT [GUID], [ReportSpace], [ReportKey]
       FROM #TempStateUpdateReport WHERE ISJSON([Content]) <= 0

       PRINT 'REMOVING RECORDS CONTAINING INVALID JSON...'
       DELETE FROM #TempStateUpdateReport WHERE ISJSON([Content]) <= 0

       PRINT 'UPDATING STATE IN JSON RECORDS (IN MEMORY):'
       SELECT [GUID], [ReportSpace], [ReportKey], CONVERT(VARCHAR(20),JSON_VALUE([Content], '$.State')) AS 'OldState', @NewState AS 'NewState'
       FROM #TempStateUpdateReport

       UPDATE #TempStateUpdateReport
       SET [Content] = JSON_MODIFY([Content], '$.State', @NewState)

       PRINT 'INSERTING UPDATED RECORDS INTO DB...'
       INSERT INTO Report ([GUID], [ReportSpace], [ReportKey], [AuthToken], [Version], [Schema], [Content], [CreatedDateTime])
       SELECT [GUID], [ReportSpace], [ReportKey], [AuthToken], [Version], [Schema], [Content], [CreatedDateTime]
       FROM #TempStateUpdateReport

       --UPDATE THE REPORT INDICES...
       PRINT 'UPDATING RECORD INDICES IN DB -> NEWLY INSERTED RECORDS...'
       UPDATE ReportIndex
       SET ReportGuid = t.GUID
       FROM ReportIndex AS ri
       JOIN
       #TempStateUpdateReport AS t
       ON ri.ReportGuid = t.OLD_GUID

       UPDATE ReportIndex
       SET IndexValue = @NewState
       FROM ReportIndex AS ri
       JOIN
       #TempStateUpdateReport AS t
       ON ri.ReportGuid = t.GUID
       WHERE ri.IndexField = 'State'

       UPDATE ReportIndex
       SET IndexValue = t.GUID
       FROM ReportIndex AS ri
       JOIN
       #TempStateUpdateReport AS t
       ON ri.ReportGuid = t.GUID
       WHERE ri.IndexField = 'GUID'


       UPDATE ReportFullText
       SET ReportGuid = t.GUID
       FROM ReportFullText AS ri
       JOIN
       #TempStateUpdateReport AS t
       ON ri.ReportGuid = t.OLD_GUID


       -- ...ADD ENTRIES TO DOWNLOAD QUEUE...
       /*
       NOTE: This should be commented out if the reports are not to be downloaded to the submission system!
       */
       PRINT 'Adding entries to the download queue...'
       INSERT INTO ReportDownload(GUID, ReportSpace, ReportKey, ReportGuid, ReportDownloadType, CreatedDateTime)
       SELECT NEWID(), [ReportSpace], [ReportKey], [GUID], 'Update', GETDATE()
       FROM #TempStateUpdateReport



       DROP TABLE #TempStateUpdateReport

COMMIT TRANSACTION STATE_UPDATE

