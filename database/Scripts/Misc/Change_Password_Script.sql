SELECT *
  FROM [ReportServices].[dbo].[User]
  WHERE [GUID] in (Select [GUID] FROM UserView) AND ISJSON([CONTENT]) >0



UPDATE [User]
SET [Content] = JSON_MODIFY([Content], '$.password', '$2a$05$gL/x2rzpotNur1pq0K5/q.I.suotesVnfsbxBKeGq3fhAKIu4A8yG')
WHERE [GUID] in (Select [GUID] FROM UserView) AND ISJSON([CONTENT]) >0
