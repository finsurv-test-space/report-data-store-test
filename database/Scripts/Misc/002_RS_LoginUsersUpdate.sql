USE [ReportServices]
GO
/*
THIS SCRIPT IS USED TO UPDATE THE RELEVANT USERS (AS PER THE "IN" STATEMENT BELOW) WITH THE NEWLY ADDED CBL REPORT SPACE AND CHANNELS
*/
----------------------------------------------------------------------

PRINT ''
PRINT 'Current access sets configured:'
SELECT * FROM [ReportServices].[dbo].[AccessSetView] 
PRINT ''

PRINT ''
PRINT 'Current users configured:'
SELECT * FROM [ReportServices].[dbo].[UserView] 
PRINT ''

----------------------------------------------------------------------
--Create the access sets
PRINT ''
PRINT 'Updating access sets...'
DELETE FROM [AccessSet] --comment this line out if you don't want to clear out the existing entries first.

INSERT INTO [AccessSet]([GUID], [Key], [AuthToken], [Version], [Content])
VALUES (NEWID(), 'unrestrictedZA', NULL, 1, '{ "name":"unrestrictedZA", "reportSpace": "SARB", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedMW', NULL, 1, '{ "name":"unrestrictedMW", "reportSpace": "RBM", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedLS', NULL, 1, '{ "name":"unrestrictedLS", "reportSpace": "CBL", "read": [], "write": [] }'),
  (NEWID(), 'unrestrictedNA', NULL, 1, '{ "name":"unrestrictedNA", "reportSpace": "BON", "read": [], "write": [] }')

PRINT ''

----------------------------------------------------------------------

PRINT ''
PRINT 'Setting Channel List Variable: '
DECLARE @channelList varchar(max)
SET @channelList = '"stdSARB","stdBON","stdRBM","stdBankLibra","sbZA","sbNA","sbMW","sbLS","sbLSEBank","sbTradeSuite","sbRAVN"';
PRINT 'CHANNELS: '+@channelList
PRINT ''

PRINT ''
PRINT 'Setting Access List Variable: '
DECLARE @accessList varchar(max)
SET @accessList = '"unrestrictedZA","unrestrictedNA","unrestrictedMW","unrestrictedLS"';
PRINT 'ACCESS LISTS: '+@channelList
PRINT ''

----------------------------------------------------------------------

PRINT ''
PRINT 'Inserting Updated User Records...'
INSERT INTO [User]([GUID], [Key], [AuthToken], [Version], [Content])
select 
	NEWID() as 'guid', 
	[Key] as 'user', 
	NULL as 'auth', 
	[Version] as 'version', /*THIS VERSION IS SPECIFIC TO THE CLASS THAT PERFORMS THE AUTHENTICATION WITHIN RDS*/
	'{ "username":"'+[Key]+'", "password":"'+ISNULL(JSON_VALUE([Content], '$.password'),'')+'", "rights":"'+ISNULL(JSON_VALUE([Content], '$.rights'),'')+'", "access": ['+@accessList+'], "channels":['+@channelList+'] }' as 'NewContent'
FROM 
	[ReportServices].[dbo].[UserView] 
/*  -- NB: REMOVE THE COMMENT DELIMMITERS TO LIMIT THE QUERY TO SPECIFIC USERS...
WHERE
	----------------------------------------------------------------------------------------------------
	------  *****  CONFIGURE THE "IN" STATEMENT BELOW AS PER THE USERS YOU WISH TO UPDATE  *****  ------
	[Key] in ('consumer', 'producer', 'jwtuser', 'gateway', 'superuser', 'dev', 'test')
	----------------------------------------------------------------------------------------------------
*/
ORDER BY 
	[Key], 
	CreatedDateTime DESC
PRINT '...User Records Inserted.'
PRINT ''

----------------------------------------------------------------------

PRINT ''
PRINT 'Updated/Current access sets configured:'
SELECT * FROM [ReportServices].[dbo].[AccessSetView] 
PRINT ''

PRINT ''
PRINT 'Updated/Current users configured:'
SELECT * FROM [ReportServices].[dbo].[UserView] 
PRINT ''

----------------------------------------------------------------------
--DONE