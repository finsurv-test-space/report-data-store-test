SELECT TOP (1000) [Name]
      --,[Area]
      --,[Content]
      ,[CreatedDateTime]
	  ,JSON_VALUE([Content], '$.validations.Verify_ARM.failOnBusy') as 'ARM_Fail_On_Busy'
	  ,JSON_VALUE([Content], '$.validations.Verify_ARM.failOnError') as 'ARM_Fail_On_Error'
	  ,JSON_VALUE([Content], '$.validations.Validate_IVS.failOnBusy') as 'IVS_Fail_On_Busy'
	  ,JSON_VALUE([Content], '$.validations.Validate_IVS.failOnError') as 'IVS_Fail_On_Error'
	  ,JSON_VALUE([Content], '$.validations.Validate_LoanRef.failOnBusy') as 'LoanRef_Fail_On_Busy'
	  ,JSON_VALUE([Content], '$.validations.Validate_LoanRef.failOnError') as 'LoanRef_Fail_On_Error'
  FROM [ReportServices].[dbo].[SystemConfiguration]
  WHERE [Area] IN ('Repo')