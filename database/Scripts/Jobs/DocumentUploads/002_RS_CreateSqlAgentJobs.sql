USE [msdb]
GO

DECLARE @job nvarchar(128) --Job name
SET @job = 'RDS_DeleteOldDocuments'

DECLARE @jobFrequency int  -- Job Frequency
SET @jobFrequency = 4  /* frequency: 1=Once, 4=Daily, 8=Weekly, 16=Monthly */

DECLARE @startTime nvarchar(8) -- Time To Run The Job
SET @startTime = '200000'  /* 20:00:00 AS [HHMMSS] */

DECLARE @jobCat nvarchar(max) -- Job Category
SET @jobCat=N'Database Maintenance'

DECLARE @jobDescription nvarchar(max) -- Job Description
SET @jobDescription=N'Job to clear out old Document uploads from the RDS database. To minimize the amount of Storage space required for the DB, the documents older than (5) days should be deleted.  The document storage is provided as a temporary/transition storage to facilitate the excon document requirements as part of the BOP/Finsurv reporting process.'

DECLARE @jobOwner nvarchar(max) -- Owner/User
SET @jobOwner=N'sa'

DECLARE @jobCmd nvarchar(max)  -- Job command to execute...
SET @jobCmd = 'exec [ReportServices].[dbo].[DeleteOldDocuments]'

DECLARE @serverName nvarchar(28)
SET @serverName=@@SERVERNAME

DECLARE @startDate nvarchar(8)
SET @startDate = REPLACE( REPLACE( REPLACE( CONVERT(VARCHAR(10), GETDATE(), 120), '-', ''), '/', ''), '\', '') --CURRENT DATE [YYYYMMDD]


-----------------------------------------------------------------------------------------

/****** Object:  Job [RDS_DeleteOldDocuments]    Script Date: 2019/07/30 13:28:42 ******/
IF NOT EXISTS (SELECT job_id FROM msdb.dbo.sysjobs WHERE name = @job)
BEGIN
	BEGIN TRANSACTION
		DECLARE @ReturnCode INT
		SELECT @ReturnCode = 0
		/****** Object:  JobCategory [Database Maintenance]    Script Date: 2019/07/30 13:28:42 ******/
		IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@jobCat AND category_class=1)
		BEGIN
			EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@jobCat
			IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
			PRINT '...CREATED JOB CATEGORY ('+@jobCat+')'
		END

		DECLARE @jobId BINARY(16)
		EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@job,
				@enabled=1,
				@notify_level_eventlog=2,
				@notify_level_email=0,
				@notify_level_netsend=0,
				@notify_level_page=0,
				@delete_level=0,
				@description=@jobDescription,
				@category_name=@jobCat,
				@owner_login_name=@jobOwner,
				@job_id = @jobId OUTPUT

		IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
		PRINT '...CREATED JOB ('+@job+') WITH OWNER ('+@jobOwner+')'

		/****** Object:  Step [Delete Docs]    Script Date: 2019/07/30 13:28:42 ******/
		EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete Docs',
				@step_id=1,
				@cmdexec_success_code=0,
				@on_success_action=1,
				@on_success_step_id=0,
				@on_fail_action=2,
				@on_fail_step_id=0,
				@retry_attempts=0,
				@retry_interval=0,
				@os_run_priority=0, @subsystem=N'TSQL',
				@command=@jobCmd,
				@database_name=N'master',
				@flags=0

		IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
		PRINT '...ADDED JOB STEP WITH COMMAND ('+@jobCmd+')'

		EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1

		IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

		EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'EveningSchedule',
				@enabled=1,
				@freq_type=@jobFrequency,
				@freq_interval=1,
				@freq_subday_type=1,
				@freq_subday_interval=0,
				@freq_relative_interval=0,
				@freq_recurrence_factor=0,
				@active_start_date=@startDate,
				@active_end_date=99991231,
				@active_start_time=@startTime,
				@active_end_time=235959,
				@schedule_uid=N'ba8cbd57-e010-4fa7-8635-f17a3c8e44bb'

		IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
		PRINT '...EVENING SCHEDULE WITH FREQ ('+CONVERT(VARCHAR(5),@jobFrequency)+') AND START TIME ('+@startTime+')'

		EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'

		IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	COMMIT TRANSACTION
	GOTO EndSave

	QuitWithRollback:
		PRINT 'FAILED TO CREATE THE SCHEDULED TASK - ROLLING BACK.'
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
		GOTO EndScript

	EndSave:
		PRINT 'SCHEDULED TASK ADDED SUCCESSFULLY.'
END
ELSE
BEGIN
	PRINT 'JOB ALREADY EXISTS WITH THE GIVEN NAME ('+@job+').'
END

EndScript:
GO


