/* ------------------------------------------------------------------------------------------------------------------ */

IF OBJECT_ID (N'ExtractSARBXmlFromRDS', N'P') IS NULL
    EXEC ('CREATE PROCEDURE ExtractSARBXmlFromRDS AS BEGIN SELECT 1 as Dummy END')
GO

ALTER PROCEDURE [dbo].[ExtractSARBXmlFromRDS]
	@ValueDate DATE = NULL,
	@ApprovalDate  DATE = NULL,
	@CreatedDate DATE = NULL,
	@ReportKey VARCHAR(200) = NULL,
	@ReportState VARCHAR(20) = NULL,
	@ReportSpace VARCHAR(20) = NULL,
	@ShowMessages INT = 0,
	@ExportAll INT = 0,
	@ExportXml XML OUT
AS
BEGIN
	/*
		Author:		James Christopher Eckhardt
		Company:	Synthesis
		Edit Date: 	2020-10-14

		Description:
		Stored procedure to convert the current (SARB/GENV3) JSON schema to the SARBDEX Xml upload file schema.

		Notes:
		- Presently clients are using this to upload transactions into Haywood or to facilitate their AML processes without tXstream Submission application.
		- The mechanisms used to generate the XML in this manner are not optimal and should the volumes reach a level
			at which it becomes unreasonably long-running or broken, the query should either be optimized, rewritten or
			replaced by an external transform mechanism. IE: XSLT, JAVASCRIPT, JAVA, C#...
			...perhaps only make use of the XQuery functionality to query and build the XML?
		- NB: This query is NOT optimised for sqlcmd.  There is a specific limitation on the size of the XML data that can be exported per query (~2MB MAX).
		    At present, sqlcmd is being used to run this query and because it performs a single select to return ALL the records as a single data result,
		    the limit is applied to the full set of data.  IE: combined limit for ALL records returned is 2MB.
		    To further this, it would make sense to have a similar query which performs a single select for each record to be returned (perhaps output as comment)
		    which would extend the 2MB limit to each individual record and not the full dataset.
	*/

	SET NOCOUNT ON

	/*
	Consider Reports only for a particular ReportSpace (any moved out of the ReportSpace can be ignored)
	*/
	IF (@ReportSpace IS NULL)
	BEGIN
		PRINT 'Defaulting to SARB ReportSpace...'
		SET @ReportSpace = 'SARB'
	END

	/*
	Index reports that are missing a ValueDate index...
	*/
	INSERT INTO ReportIndex (ReportSpace, ReportKey, ReportGuid, IndexType, IndexField, IndexValue, CreatedDateTime)
	SELECT r.ReportSpace, r.ReportKey, r.GUID, 'Report', 'ValueDate', JSON_VALUE(r.Content, '$.Report.ValueDate'), r.CreatedDateTime
	FROM
		(SELECT * FROM ReportView WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS r
	LEFT JOIN
		(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri
	ON r.GUID = ri.ReportGuid and ri.IndexField = 'ValueDate'
	WHERE ri.IndexField IS NULL AND r.ReportSpace = @ReportSpace


	/*
	Index reports that are missing a State index...
	*/
	INSERT INTO ReportIndex (ReportSpace, ReportKey, ReportGuid, IndexType, IndexField, IndexValue, CreatedDateTime)
	SELECT r.ReportSpace, r.ReportKey, r.GUID, 'System', 'State', JSON_VALUE(r.Content, '$.State'), r.CreatedDateTime
	FROM
		(SELECT * FROM ReportView WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS r
	LEFT JOIN
		(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri
	ON r.GUID = ri.ReportGuid and ri.IndexField = 'State'
	WHERE ri.IndexField IS NULL AND r.ReportSpace = @ReportSpace


	/*
	DECLARE @ValueDate DATE
	SET @ValueDate = CONVERT(DATE,'2018-11-13',120)
	DECLARE @ReportKey VARCHAR(200)
	SET @ReportKey = NULL -- '1811130155TT9429'
	DECLARE @ExportXml XML
	*/

	/* Prime the file-specific variables as needed */
	DECLARE @SenderName varchar(200)
	SET @SenderName = 'SYNTHESIS'
	DECLARE @CurrentDate varchar(200)
	SET @CurrentDate = (SELECT CONVERT(VARCHAR(MAX),GETDATE(),120))
	DECLARE @FileReference varchar(200)
	SET @FileReference = (SELECT REPLACE(REPLACE(REPLACE(REPLACE(@CurrentDate, '-', ''), ' ', ''), ':', ''), '/', ''))
	DECLARE @Identity varchar(200)
	SET @Identity = 'SARBFileIdentity'
	DECLARE @IdentityType varchar(200)
	SET @IdentityType = '3'

	/* Create an initial container Element */
	SET @ExportXml = (
		SELECT
			@FileReference AS "FINSURV/@Reference",
			'T' AS "FINSURV/@Environment",
			'1' AS "FINSURV/@Version"
		FOR XML PATH(''), TYPE
	)

	/* Check if the temp table exists - if so, purge. */
	IF OBJECT_ID('tempdb..#ReportsToExport') IS NOT NULL DROP TABLE #ReportsToExport

	/* Fetch the list of Reports to include into the XML Export File... */
	SELECT DISTINCT
		r.ReportKey
	INTO #ReportsToExport
	FROM
		(SELECT * FROM ReportView WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS r
	JOIN
		(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri
	ON
		r.GUID = ri.ReportGuid
	LEFT JOIN
		(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri2
	ON
		r.GUID = ri2.ReportGuid AND ri2.IndexField = 'State'
	WHERE
	(
		(
			@ReportKey IS NULL
			AND @ValueDate IS NOT NULL
			AND ri.IndexValue = CONVERT(varchar(10), @ValueDate, 121)
		)
		OR
		(
			@ReportKey IS NULL
			AND @CreatedDate IS NOT NULL
			AND CONVERT(DATE, r.CreatedDateTime, 120) = CONVERT(varchar(10), @CreatedDate, 121)
		)
		OR
		(
			@ValueDate IS NULL
			AND @ReportKey IS NOT NULL
			AND r.ReportKey = @ReportKey
		)
		OR
		(
			@ApprovalDate IS NOT NULL
			AND @ReportKey IS NULL
			AND r.ReportKey IN (
                SELECT ReportKey
                FROM
                (
                    SELECT
                        ReportKey,
                        JSON_VALUE([Content], '$.State') AS 'State',
                        MIN(CreatedDateTime) AS 'ApprovalDate',
                        MAX(CreatedDateTime) AS 'LastEditDate'
                    FROM (SELECT * FROM Report WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS a2
                    WHERE
                        /*
                            THIS LOOKUP WILL ONLY WORK IF THE REPORT INDEX TABLE REMAINS IMMUTABLE AS WELL.
                            WE MAY NEED TO CREATE AN ADDITIONAL INDEX TABLE IF THIS IS REQUIRED OR PERFORMANCE DEGRADES SIGNIFICANTLY.
                        */
                        /*
                        CONVERT(DATE, a2.CreatedDateTime, 120) >= CONVERT(varchar(10), @ApprovalDate, 120)
                        AND
                        a2.reportKey IN (
                            SELECT ReportKey
                            FROM (SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS a1
                            WHERE
                                --CreatedDateTime >= CONVERT(DATE, GETDATE(), 120)
                                CONVERT(DATE, a1.CreatedDateTime, 120) >= CONVERT(varchar(10), @ApprovalDate, 120)
                                AND
                                a1.IndexField = 'State'
                                AND a1.IndexValue = 'End'
                        )
                        */
                        /*
                            THIS IS JUST TO LIMIT THE SELECTION WINDOW SO WE DON'T SCAN THE ENTIRE TABLE ON UN-INDEXED JSON DATA
                            CAN BE REMOVED IF WE RESOLVE THE INDEXING ISSUE ABOVE
                        */
                        CONVERT(DATE, a2.CreatedDateTime, 120) BETWEEN CONVERT(varchar(10), DATEADD(DAY, -5, @ApprovalDate), 120) AND CONVERT(varchar(10), DATEADD(DAY, 5, @ApprovalDate), 120)
                        AND
                        JSON_VALUE(a2.[Content], '$.State') = 'End'
                    GROUP BY
                        ReportKey, JSON_VALUE([Content], '$.State')
                ) AS app
                WHERE
                    CONVERT(DATE, r.CreatedDateTime, 120) >= CONVERT(varchar(10), app.ApprovalDate, 120)
            )
		)
		OR
		(
			@ReportKey IS NOT NULL
			AND @ValueDate IS NOT NULL
			AND r.ReportKey = @ReportKey
			AND ri.IndexValue = CONVERT(varchar(10), @ValueDate, 121)
		)
		OR
		(
			@ReportKey IS NOT NULL
			AND @CreatedDate IS NOT NULL
			AND r.ReportKey = @ReportKey
			AND CONVERT(DATE, r.CreatedDateTime, 120) = CONVERT(varchar(10), @CreatedDate, 121)
		)
		OR
		(
		  @ExportAll = 1
		)
	)
	AND
	(
		(
			@ReportState IS NULL
			AND ri2.IndexValue IN ('End')
		)
		OR
		(
			ri2.IndexValue = @ReportState
		)
	)
	AND
	(
		r.ReportSpace = @ReportSpace
	)


		/* Fetch the list of Reports to include into the XML Export File... */
	IF (@ShowMessages <> 0)
	BEGIN
		PRINT ' ';
		PRINT ' ';
		PRINT ' ';
		PRINT '******  ******  ******  ******  ******  ******  ******  ******  ******  ******';
		PRINT 'Reports currently not elligable for Xml extract due to State being incorrect..'
		PRINT '******  ******  ******  ******  ******  ******  ******  ******  ******  ******';
		PRINT ' ';

		SELECT DISTINCT
			r.ReportKey
		FROM
			(SELECT * FROM ReportView WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS r
		JOIN
			(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri
		ON
			r.GUID = ri.ReportGuid
		LEFT JOIN
			(SELECT * FROM ReportIndex WHERE CreatedDateTime > DATEADD(DAY, -10, GETDATE())) AS ri2
		ON
			r.GUID = ri2.ReportGuid AND ri2.IndexField = 'State'
		WHERE
		(
			(
				@ReportKey IS NULL
				AND @ValueDate IS NOT NULL
				AND ri.IndexValue = CONVERT(varchar(10), @ValueDate, 121)
			)
			OR
			(
				@ReportKey IS NULL
				AND @CreatedDate IS NOT NULL
				AND CONVERT(DATE, r.CreatedDateTime, 120) = CONVERT(varchar(10), @CreatedDate, 121)
			)
			OR
			(
				@ValueDate IS NULL
				AND @ReportKey IS NOT NULL
				AND r.ReportKey = @ReportKey
			)
			OR
			(
				@ReportKey IS NOT NULL
				AND @ValueDate IS NOT NULL
				AND r.ReportKey = @ReportKey
				AND ri.IndexValue = CONVERT(varchar(10), @ValueDate, 121)
			)
			OR
			(
				@ReportKey IS NOT NULL
				AND @CreatedDate IS NOT NULL
				AND r.ReportKey = @ReportKey
				AND CONVERT(DATE, r.CreatedDateTime, 120) = CONVERT(varchar(10), @CreatedDate, 121)
			)
		)
		AND
		(
			(
				@ReportState IS NULL
				AND ri2.IndexValue NOT IN ('End')
			)
			AND
			(
				ri2.IndexValue <> @ReportState
			)
		)
		AND
		(
			r.ReportSpace = @ReportSpace
		)
		PRINT ' ';
		PRINT '******  ******  ******  ******  ******  ******  ******  ******  ******  ******';
		PRINT ' ';
		PRINT ' ';
		PRINT ' ';
	END


	/* Check if there are any reports to export for the given params... */
	IF EXISTS( SELECT TOP 1 * FROM #ReportsToExport )
	BEGIN
		DECLARE @StartDateTime DateTime, @CurrentReportKey VarChar(200), @message NVarChar(200);
		SET @StartDateTime = GETDATE();

		IF (@ShowMessages <> 0)
		BEGIN
			PRINT '******  BATCH PROCESSING OF SARB XML EXTRACTION  ******';
			PRINT 'Started: ' + CONVERT(VARCHAR(40), @StartDateTime, 120);
		END

		/* Create a cursor to iterate through each report reference... */
		DECLARE ReportSource_Cursor CURSOR
			FOR SELECT ReportKey FROM #ReportsToExport;

		OPEN ReportSource_Cursor
		FETCH NEXT FROM ReportSource_Cursor	INTO @CurrentReportKey

		BEGIN TRY
			IF @@FETCH_STATUS <> 0
				PRINT '         <<None>>'

			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF (@ShowMessages <> 0)
				BEGIN
					SELECT @message = '*** CURRENT TRN: ' + @CurrentReportKey + ' ***'
					PRINT @message
					PRINT ' '
					SELECT @message = 'XML(SARB) EXTRACTION: ' + @CurrentReportKey
					PRINT @message
				END

				/* Check that the report contains completely valid JSON (as per MSSQL)... */
				DECLARE @IsValidJSON NVARCHAR(MAX)
				SELECT TOP 1 @IsValidJSON = JSON_QUERY(r.Content, '$.Report') FROM ReportView AS r WHERE r.ReportKey = @CurrentReportKey
				IF (@ShowMessages <> 0)
					PRINT ' '

				/* Only process the report further if it appears to have valid, parsable JSON... */
				IF (ISJSON(@IsValidJSON) > 0)
				BEGIN

					---------------------------------------------------------------------------------
					---------------------------------------------------------------------------------

					BEGIN TRY

						/* Extract some common, functional values from the current report for later use... */
						DECLARE @FlowCurrency NVARCHAR(200)
						SET @FlowCurrency = (
							SELECT TOP 1
								JSON_VALUE(r.Content, '$.Report.FlowCurrency') as 'FlowCurrency'
							FROM
								ReportView AS r
							WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = 'SARB'
						)

						/*
							**********************************************************************************************************
							Composing of each component of the Report (Resident, NonResident, MonetaryAmount, ImportExport  etc)...
							**********************************************************************************************************
						*/

						----------  RESIDENT INDIVIDUAL  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Resident <Individual> details ('+@CurrentReportKey+')...'
						END
						DECLARE @ResIndXml XML
						SET @ResIndXml = (
							SELECT
								ri.Name AS "IndividualCustomer/@Name"
								,ri.Surname AS "IndividualCustomer/@Surname"
								,ri.Gender AS "IndividualCustomer/@Gender"
								,ri.DateOfBirth AS "IndividualCustomer/@DateOfBirth"
								,ri.IDNumber AS "IndividualCustomer/@IDNumber"
								,ri.TempResPermitNumber AS "IndividualCustomer/@TempResPermitNumber"
								,ri.ForeignIDNumber AS "IndividualCustomer/@ForeignIDNumber"
								,ri.ForeignIDCountry AS "IndividualCustomer/@ForeignIDCountry"
								,ri.PassportNumber  AS "IndividualCustomer/@PassportNumber"
								,ri.PassportCountry  AS "IndividualCustomer/@PassportCountry"

								,ri.AccountName AS "IndividualCustomer/AdditionalCustomerData/@AccountName"
								,ri.AccountIdentifier AS "IndividualCustomer/AdditionalCustomerData/@AccountIdentifier"
								,ri.AccountNumber AS "IndividualCustomer/AdditionalCustomerData/@AccountNumber"
								,ri.CustomsClientNumber AS "IndividualCustomer/AdditionalCustomerData/@CustomsClientNumber"
								,ri.TaxNumber AS "IndividualCustomer/AdditionalCustomerData/@TaxNumber"
								,ri.VATNumber AS "IndividualCustomer/AdditionalCustomerData/@VATNumber"
								,ri.TaxClearanceCertificateIndicator AS "IndividualCustomer/AdditionalCustomerData/@TaxClearanceCertificateIndicator"
								,ri.TaxClearanceCertificateReference AS "IndividualCustomer/AdditionalCustomerData/@TaxClearanceCertificateReference"

								,ri.StreetAddressLine1 AS "IndividualCustomer/AdditionalCustomerData/@StreetAddressLine1"
								,ri.StreetAddressLine2 AS "IndividualCustomer/AdditionalCustomerData/@StreetAddressLine2"
								,ri.StreetSuburb AS "IndividualCustomer/AdditionalCustomerData/@StreetSuburb"
								,ri.StreetCity AS "IndividualCustomer/AdditionalCustomerData/@StreetCity"
								,ri.StreetProvince AS "IndividualCustomer/AdditionalCustomerData/@StreetProvince"
								,ri.StreetPostalCode AS "IndividualCustomer/AdditionalCustomerData/@StreetPostalCode"

								,ri.PostalAddressLine1 AS "IndividualCustomer/AdditionalCustomerData/@PostalAddressLine1"
								,ri.PostalAddressLine2 AS "IndividualCustomer/AdditionalCustomerData/@PostalAddressLine2"
								,ri.PostalSuburb AS "IndividualCustomer/AdditionalCustomerData/@PostalSuburb"
								,ri.PostalCity AS "IndividualCustomer/AdditionalCustomerData/@PostalCity"
								,ri.PostalProvince AS "IndividualCustomer/AdditionalCustomerData/@PostalProvince"
								,ri.PostalCode AS "IndividualCustomer/AdditionalCustomerData/@PostalCode"

								,ri.ContactSurname AS "IndividualCustomer/AdditionalCustomerData/@ContactSurname"
								,ri.ContactName AS "IndividualCustomer/AdditionalCustomerData/@ContactName"
								,ri.Email AS "IndividualCustomer/AdditionalCustomerData/@Email"
								,ri.Fax AS "IndividualCustomer/AdditionalCustomerData/@Fax"
								,ri.Telephone AS "IndividualCustomer/AdditionalCustomerData/@Telephone"

								,ri.CardNumber AS "IndividualCustomer/AdditionalCustomerData/@CardNumber"
								,ri.SupplementaryCardIndicator AS "IndividualCustomer/AdditionalCustomerData/@SupplementaryCardIndicator"
							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.Resident.Individual') as 'ResidentIndividual'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.ResidentIndividual,'{}'))
								WITH (
									Name NVARCHAR(200) N'$.Name'
									,Surname NVARCHAR(200) N'$.Surname'
									,Gender NVARCHAR(200) N'$.Gender'
									,DateOfBirth NVARCHAR(200) N'$.DateOfBirth'
									,IDNumber NVARCHAR(200) N'$.IDNumber'
									,TempResPermitNumber NVARCHAR(200) N'$.TempResPermitNumber'
									,ForeignIDNumber NVARCHAR(200) N'$.ForeignIDNumber'
									,ForeignIDCountry NVARCHAR(200) N'$.ForeignIDCountry'
									,PassportNumber NVARCHAR(200) N'$.PassportNumber'
									,PassportCountry NVARCHAR(200) N'$.PassportCountry'

									,AccountName NVARCHAR(200) N'$.AccountName'
									,AccountIdentifier NVARCHAR(200) N'$.AccountIdentifier'
									,AccountNumber NVARCHAR(200) N'$.AccountNumber'
									,CustomsClientNumber NVARCHAR(200) N'$.CustomsClientNumber'
									,TaxNumber NVARCHAR(200) N'$.TaxNumber'
									,VATNumber NVARCHAR(200) N'$.VATNumber'
									,TaxClearanceCertificateIndicator NVARCHAR(200) N'$.TaxClearanceCertificateIndicator'
									,TaxClearanceCertificateReference NVARCHAR(200) N'$.TaxClearanceCertificateReference'

									,StreetAddressLine1 NVARCHAR(200) N'$.StreetAddress.AddressLine1'
									,StreetAddressLine2 NVARCHAR(200) N'$.StreetAddress.AddressLine2'
									,StreetSuburb NVARCHAR(200) N'$.StreetAddress.Suburb'
									,StreetCity NVARCHAR(200) N'$.StreetAddress.City'
									,StreetProvince NVARCHAR(200) N'$.StreetAddress.Province'
									,StreetPostalCode NVARCHAR(200) N'$.StreetAddress.PostalCode'

									,PostalAddressLine1 NVARCHAR(200) N'$.PostalAddress.AddressLine1'
									,PostalAddressLine2 NVARCHAR(200) N'$.PostalAddress.AddressLine2'
									,PostalSuburb NVARCHAR(200) N'$.PostalAddress.Suburb'
									,PostalCity NVARCHAR(200) N'$.PostalAddress.City'
									,PostalProvince NVARCHAR(200) N'$.PostalAddress.Province'
									,PostalCode NVARCHAR(200) N'$.PostalAddress.PostalCode'

									,ContactSurname NVARCHAR(200) N'$.ContactDetails.ContactSurname'
									,ContactName NVARCHAR(200) N'$.ContactDetails.ContactName'
									,Email NVARCHAR(200) N'$.ContactDetails.Email'
									,Fax NVARCHAR(200) N'$.ContactDetails.Fax'
									,Telephone NVARCHAR(200) N'$.ContactDetails.Telephone'

									,CardNumber NVARCHAR(200) N'$.CardNumber'
									,SupplementaryCardIndicator NVARCHAR(200) N'$.SupplementaryCardIndicator'

								) AS ri
							FOR XML PATH(''), TYPE
						)

						----------  RESIDENT ENTITY  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Resident <Entity> details ('+@CurrentReportKey+')...'
						END
						DECLARE @ResEntXml XML
						SET @ResEntXml = (
							SELECT
								re.LegalEntityName AS "EntityCustomer/@LegalEntityName"
								,re.TradingName AS "EntityCustomer/@TradingName"
								,re.RegistrationNumber AS "EntityCustomer/@RegistrationNumber"
								,re.InstitutionalSector AS "EntityCustomer/@InstitutionalSector"
								,re.IndustrialClassification AS "EntityCustomer/@IndustrialClassification"


								,re.AccountName AS "EntityCustomer/AdditionalCustomerData/@AccountName"
								,re.AccountIdentifier AS "EntityCustomer/AdditionalCustomerData/@AccountIdentifier"
								,re.AccountNumber AS "EntityCustomer/AdditionalCustomerData/@AccountNumber"

								,re.CustomsClientNumber AS "EntityCustomer/AdditionalCustomerData/@CustomsClientNumber"

								,re.TaxNumber AS "EntityCustomer/AdditionalCustomerData/@TaxNumber"
								,re.VATNumber AS "EntityCustomer/AdditionalCustomerData/@VATNumber"
								,re.TaxClearanceCertificateIndicator AS "EntityCustomer/AdditionalCustomerData/@TaxClearanceCertificateIndicator"
								,re.TaxClearanceCertificateReference AS "EntityCustomer/AdditionalCustomerData/@TaxClearanceCertificateReference"

								,re.StreetAddressLine1 AS "EntityCustomer/AdditionalCustomerData/@StreetAddressLine1"
								,re.StreetAddressLine2 AS "EntityCustomer/AdditionalCustomerData/@StreetAddressLine2"
								,re.StreetSuburb AS "EntityCustomer/AdditionalCustomerData/@StreetSuburb"
								,re.StreetCity AS "EntityCustomer/AdditionalCustomerData/@StreetCity"
								,re.StreetProvince AS "EntityCustomer/AdditionalCustomerData/@StreetProvince"
								,re.StreetPostalCode AS "EntityCustomer/AdditionalCustomerData/@StreetPostalCode"

								,re.PostalAddressLine1 AS "EntityCustomer/AdditionalCustomerData/@PostalAddressLine1"
								,re.PostalAddressLine2 AS "EntityCustomer/AdditionalCustomerData/@PostalAddressLine2"
								,re.PostalSuburb AS "EntityCustomer/AdditionalCustomerData/@PostalSuburb"
								,re.PostalCity AS "EntityCustomer/AdditionalCustomerData/@PostalCity"
								,re.PostalProvince AS "EntityCustomer/AdditionalCustomerData/@PostalProvince"
								,re.PostalCode AS "EntityCustomer/AdditionalCustomerData/@PostalCode"

								,re.ContactSurname AS "EntityCustomer/AdditionalCustomerData/@ContactSurname"
								,re.ContactName AS "EntityCustomer/AdditionalCustomerData/@ContactName"
								,re.Email AS "EntityCustomer/AdditionalCustomerData/@Email"
								,re.Fax AS "EntityCustomer/AdditionalCustomerData/@Fax"
								,re.Telephone AS "EntityCustomer/AdditionalCustomerData/@Telephone"

								,re.CardNumber AS "EntityCustomer/AdditionalCustomerData/@CardNumber"
								,re.SupplementaryCardIndicator AS "EntityCustomer/AdditionalCustomerData/@SupplementaryCardIndicator"
							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.Resident.Entity') as 'ResidentEntity'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.ResidentEntity,'{}'))
								WITH (
									LegalEntityName NVARCHAR(200) N'$.TradingName'
									,TradingName NVARCHAR(200) N'$.TradingName'
									,RegistrationNumber NVARCHAR(200) N'$.RegistrationNumber'
									,InstitutionalSector NVARCHAR(200) N'$.InstitutionalSector'
									,IndustrialClassification NVARCHAR(200) N'$.IndustrialClassification'

									,AccountName NVARCHAR(200) N'$.AccountName'
									,AccountIdentifier NVARCHAR(200) N'$.AccountIdentifier'
									,AccountNumber NVARCHAR(200) N'$.AccountNumber'

									,CustomsClientNumber NVARCHAR(200) N'$.CustomsClientNumber'

									,TaxNumber NVARCHAR(200) N'$.TaxNumber'
									,VATNumber NVARCHAR(200) N'$.VATNumber'
									,TaxClearanceCertificateIndicator NVARCHAR(200) N'$.TaxClearanceCertificateIndicator'
									,TaxClearanceCertificateReference NVARCHAR(200) N'$.TaxClearanceCertificateReference'

									,StreetAddressLine1 NVARCHAR(200) N'$.StreetAddress.AddressLine1'
									,StreetAddressLine2 NVARCHAR(200) N'$.StreetAddress.AddressLine2'
									,StreetSuburb NVARCHAR(200) N'$.StreetAddress.Suburb'
									,StreetCity NVARCHAR(200) N'$.StreetAddress.City'
									,StreetProvince NVARCHAR(200) N'$.StreetAddress.Province'
									,StreetPostalCode NVARCHAR(200) N'$.StreetAddress.PostalCode'

									,PostalAddressLine1 NVARCHAR(200) N'$.PostalAddress.AddressLine1'
									,PostalAddressLine2 NVARCHAR(200) N'$.PostalAddress.AddressLine2'
									,PostalSuburb NVARCHAR(200) N'$.PostalAddress.Suburb'
									,PostalCity NVARCHAR(200) N'$.PostalAddress.City'
									,PostalProvince NVARCHAR(200) N'$.PostalAddress.Province'
									,PostalCode NVARCHAR(200) N'$.PostalAddress.PostalCode'

									,ContactSurname NVARCHAR(200) N'$.ContactDetails.ContactSurname'
									,ContactName NVARCHAR(200) N'$.ContactDetails.ContactName'
									,Email NVARCHAR(200) N'$.ContactDetails.Email'
									,Fax NVARCHAR(200) N'$.ContactDetails.Fax'
									,Telephone NVARCHAR(200) N'$.ContactDetails.Telephone'

									,CardNumber NVARCHAR(200) N'$.CardNumber'
									,SupplementaryCardIndicator NVARCHAR(200) N'$.SupplementaryCardIndicator'

								) AS re
							FOR XML PATH(''), TYPE
						)


						----------  RESIDENT EXCEPTION  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Resident <Exception> details ('+@CurrentReportKey+')...'
						END
						DECLARE @ResExXml XML
						SET @ResExXml = (
							SELECT
								rx.ExceptionName AS "Exception/@ExceptionName"
								,rx.Country AS "Exception/@Country"
							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.Resident.Exception') as 'ResidentException'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.ResidentException,'{}'))
								WITH (
									ExceptionName NVARCHAR(200) N'$.ExceptionName'
									,Country NVARCHAR(200) N'$.Country'
								) AS rx
							FOR XML PATH(''), TYPE
						)



						----------  NON RESIDENT INDIVIDUAL  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...NonResident <Individual> details ('+@CurrentReportKey+')...'
						END
						DECLARE @NResIndXml XML
						SET @NResIndXml = (
							SELECT
								ri.Name AS "Individual/@Name"
								,ri.Surname AS "Individual/@Surname"
								,ri.AccountName AS "Individual/AdditionalCustomerData/@AccountName"

								,ri.AccountIdentifier AS "Individual/AdditionalNonResidentData/@AccountIdentifier"
								,ri.AccountNumber AS "Individual/AdditionalNonResidentData/@AccountNumber"
								--,ri.AccountNumber AS "Individual/AdditionalNonResidentData/@NRAccountNumber"

								,ri.StreetAddressLine1 AS "Individual/AdditionalNonResidentData/@AddressLine1"
								,ri.StreetAddressLine2 AS "Individual/AdditionalNonResidentData/@AddressLine2"
								,ri.StreetSuburb AS "Individual/AdditionalNonResidentData/@Suburb"
								,ri.StreetCity AS "Individual/AdditionalNonResidentData/@City"
								,ri.StreetProvince AS "Individual/AdditionalNonResidentData/@State"
								,ri.StreetPostalCode AS "Individual/AdditionalNonResidentData/@ZipCode"
								,ri.StreetCountry AS "Individual/AdditionalNonResidentData/@Country"
							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.NonResident.Individual') as 'NResidentIndividual'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.NResidentIndividual,'{}'))
								WITH (
									Name NVARCHAR(200) N'$.Name'
									,Surname NVARCHAR(200) N'$.Surname'

									,AccountName NVARCHAR(200) N'$.AccountName'
									,AccountIdentifier NVARCHAR(200) N'$.AccountIdentifier'
									,AccountNumber NVARCHAR(200) N'$.AccountNumber'

									,StreetAddressLine1 NVARCHAR(200) N'$.Address.AddressLine1'
									,StreetAddressLine2 NVARCHAR(200) N'$.Address.AddressLine2'
									,StreetSuburb NVARCHAR(200) N'$.Address.Suburb'
									,StreetCity NVARCHAR(200) N'$.Address.City'
									,StreetProvince NVARCHAR(200) N'$.Address.Province'
									,StreetPostalCode NVARCHAR(200) N'$.Address.PostalCode'
									,StreetCountry NVARCHAR(200) N'$.Address.Country'
								) AS ri
							FOR XML PATH(''), TYPE
						)

						----------  NON RESIDENT ENTITY  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...NonResident <Entity> details ('+@CurrentReportKey+')...'
						END
						DECLARE @NResEntXml XML
						SET @NResEntXml = (
							SELECT
								re.LegalEntityName AS "Entity/@LegalEntityName"
								,re.CardMerchantName AS "Entity/@CardMerchantName"
								,re.CardMerchantCode AS "Entity/@CardMerchantCode"

								,re.AccountIdentifier AS "Entity/AdditionalNonResidentData/@AccountIdentifier"
								,re.AccountNumber AS "Entity/AdditionalNonResidentData/@AccountNumber"
								--,ri.AccountNumber AS "Entity/AdditionalNonResidentData/@NRAccountNumber"

								,re.StreetAddressLine1 AS "Entity/AdditionalNonResidentData/@AddressLine1"
								,re.StreetAddressLine2 AS "Entity/AdditionalNonResidentData/@AddressLine2"
								,re.StreetSuburb AS "Entity/AdditionalNonResidentData/@AddressLine3"
								,re.StreetCity AS "Entity/AdditionalNonResidentData/@City"
								,re.StreetProvince AS "Entity/AdditionalNonResidentData/@State"
								,re.StreetPostalCode AS "Entity/AdditionalNonResidentData/@ZipCode"
								,re.StreetCountry AS "Entity/AdditionalNonResidentData/@Country"

							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.NonResident.Entity') as 'NResidentEntity'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.NResidentEntity,'{}'))
								WITH (
									LegalEntityName NVARCHAR(200) N'$.EntityName'
									,CardMerchantName NVARCHAR(200) N'$.CardMerchantName'
									,CardMerchantCode NVARCHAR(200) N'$.CardMerchantCode'

									,AccountName NVARCHAR(200) N'$.AccountName'
									,AccountIdentifier NVARCHAR(200) N'$.AccountIdentifier'
									,AccountNumber NVARCHAR(200) N'$.AccountNumber'

									,StreetAddressLine1 NVARCHAR(200) N'$.Address.AddressLine1'
									,StreetAddressLine2 NVARCHAR(200) N'$.Address.AddressLine2'
									,StreetSuburb NVARCHAR(200) N'$.Address.Suburb'
									,StreetCity NVARCHAR(200) N'$.Address.City'
									,StreetProvince NVARCHAR(200) N'$.Address.Province'
									,StreetPostalCode NVARCHAR(200) N'$.Address.PostalCode'
									,StreetCountry NVARCHAR(200) N'$.Address.Country'
								) AS re
							FOR XML PATH(''), TYPE
						)


						----------  NON RESIDENT EXCEPTION  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...NonResident <Exception> details ('+@CurrentReportKey+')...'
						END
						DECLARE @NResExXml XML
						SET @NResExXml = (
							SELECT
								rx.ExceptionName AS "Exception/@ExceptionName"
								,rx.StreetCountry AS "Exception/@Country"

								,rx.CardMerchantName AS "Exception/@CardMerchantName"
								,rx.CardMerchantCode AS "Exception/@CardMerchantCode"

								--,rx.AccountIdentifier AS "Exception/AdditionalNonResidentData/@AccountIdentifier"
								--,rx.AccountNumber AS "Exception/AdditionalNonResidentData/@AccountNumber"

								,rx.StreetAddressLine1 AS "Exception/AdditionalNonResidentData/@AddressLine1"
								,rx.StreetAddressLine2 AS "Exception/AdditionalNonResidentData/@AddressLine2"
								,rx.StreetSuburb AS "Exception/AdditionalNonResidentData/@AddressLine3"
								,rx.StreetCity AS "Exception/AdditionalNonResidentData/@City"
								,rx.StreetProvince AS "Exception/AdditionalNonResidentData/@State"
								,rx.StreetPostalCode AS "Exception/AdditionalNonResidentData/@ZipCode"
								,rx.StreetCountry AS "Exception/AdditionalNonResidentData/@Country"
							from
							(
								SELECT
									r.ReportKey
									,r.GUID as ReportGuid
									,JSON_QUERY(r.Content, '$.Report.NonResident.Exception') as 'NResidentException'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(ISNULL(x.NResidentException,'{}'))
								WITH (
									ExceptionName NVARCHAR(200) N'$.ExceptionName'
									,CardMerchantName NVARCHAR(200) N'$.CardMerchantName'
									,CardMerchantCode NVARCHAR(200) N'$.CardMerchantCode'

									,AccountName NVARCHAR(200) N'$.AccountName'
									,AccountIdentifier NVARCHAR(200) N'$.AccountIdentifier'
									,AccountNumber NVARCHAR(200) N'$.AccountNumber'

									,StreetAddressLine1 NVARCHAR(200) N'$.Address.AddressLine1'
									,StreetAddressLine2 NVARCHAR(200) N'$.Address.AddressLine2'
									,StreetSuburb NVARCHAR(200) N'$.Address.Suburb'
									,StreetCity NVARCHAR(200) N'$.Address.City'
									,StreetProvince NVARCHAR(200) N'$.Address.Province'
									,StreetPostalCode NVARCHAR(200) N'$.Address.PostalCode'
									,StreetCountry NVARCHAR(200) N'$.Address.Country'
								) AS rx
							FOR XML PATH(''), TYPE
						)



						----------  IMPORT EXPORT DETAILS  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Import Export details ('+@CurrentReportKey+')...'
						END
						DECLARE @ImportExport XML
						SET @ImportExport = (
							SELECT
								ISNULL(rm.SequenceNumber, x1.[key] + 1) AS "ImportExportData/@SequenceNumber"
								,ISNULL(ie2.SubSequence, ie1.[key]+1) AS "ImportExportData/@SubSequenceNumber"
								,ie2.ImportControlNumber AS "ImportExportData/@ImportControlNumber"
								,ie2.TransportDocumentNumber AS "ImportExportData/@TransportDocumentNumber"
								,ie2.NoMRNonIVS AS "ImportExportData/@NoMRNonIVS"
								,ie2.UCR AS "ImportExportData/@UCR"
								,REPLACE(REPLACE(ie2.PaymentValue, ',', ''), ' ', '') AS "ImportExportData/@PaymentValue"
								,ie2.PaymentCurrencyCode AS "ImportExportData/@PaymentCurrencyCode"
								--,rm.ImportExport
							from
							(
								SELECT
									r.ReportKey,
									JSON_QUERY(r.Content, '$.Report.ReportingQualifier') as 'ReportingQualifier',
									JSON_QUERY(r.Content, '$.Report.MonetaryAmount') as 'MonetaryAmount'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(x.MonetaryAmount) AS x1
							CROSS APPLY
								OPENJSON(x1.[value])
								WITH (
									SequenceNumber INT N'$.SequenceNumber'
									,ImportExport NVARCHAR(MAX) AS JSON --N'$.ImportExport'
								) AS rm
							CROSS APPLY
								OPENJSON(rm.ImportExport) AS ie1
							CROSS APPLY
								OPENJSON(ie1.[value])
								WITH (
									SubSequence NVARCHAR(70) N'$.SubSequence'
									,ImportControlNumber NVARCHAR(70) N'$.ImportControlNumber'
									,TransportDocumentNumber NVARCHAR(70) N'$.TransportDocumentNumber'
									,NoMRNonIVS NVARCHAR(70) N'$.NoMRNonIVS'
									,UCR NVARCHAR(70) N'$.UCR'
									,PaymentValue NVARCHAR(70) N'$.PaymentValue'
									,PaymentCurrencyCode NVARCHAR(70) N'$.PaymentCurrencyCode'
								) AS ie2
							ORDER BY rm.SequenceNumber, ie1.[key]
							FOR XML PATH(''), TYPE
						)


						----------  MONETARY AMOUNT DETAILS  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Monetary details ('+@CurrentReportKey+')...'
						END
						DECLARE @MoneyXml XML
						SET @MoneyXml = (
							SELECT
								ISNULL(rm.SequenceNumber, 1) AS "MonetaryDetails/@SequenceNumber"
								,rm.MoneyTransferAgentIndicator AS "MonetaryDetails/@MoneyTransferAgentIndicator"
								,REPLACE(REPLACE(ISNULL(rm.RandValue, rm.DomesticValue), ',', ''), ' ', '') AS "MonetaryDetails/@RandValue"
								/*,REPLACE(REPLACE(rm.ForeignValue, ',', ''), ' ', '') AS "MonetaryDetails/@ForeignValue"*/
								,CASE
									WHEN ISNULL(rm.ForeignCurrencyCode, @FlowCurrency) != 'ZAR' --OR x.Flow = 'OUT'
										THEN REPLACE(REPLACE(rm.ForeignValue, ',', ''), ' ', '')
									ELSE NULL
								END AS "MonetaryDetails/@ForeignValue"
								,CASE
									WHEN ISNULL(rm.ForeignCurrencyCode, @FlowCurrency) != 'ZAR' --OR x.Flow = 'OUT'
										THEN ISNULL(rm.ForeignCurrencyCode, @FlowCurrency)
									ELSE NULL
								END AS "MonetaryDetails/@ForeignCurrencyCode"
								,CASE
									WHEN (x.ReportingQualifier != 'NON REPORTABLE' AND x.ReportingQualifier != 'NOT REPORTABLE')
                    THEN LEFT(rm.BoPCategory,3)
									ELSE NULL
								END AS "MonetaryDetails/@BoPCategory"
								,CASE
									WHEN (x.ReportingQualifier != 'NON REPORTABLE' AND x.ReportingQualifier != 'NOT REPORTABLE') AND rm.SubBoPCategory IS NOT NULL
										THEN rm.SubBoPCategory
									WHEN (x.ReportingQualifier != 'NON REPORTABLE' AND x.ReportingQualifier != 'NOT REPORTABLE') AND rm.SubBoPCategory IS NULL AND (LEN(rm.BoPCategory) > 3)
										THEN SUBSTRING(rm.BoPCategory, 4, 2)
									ELSE NULL
								END AS "MonetaryDetails/@SubBoPCategory"
								,rm.StrateRefNumber AS "MonetaryDetails/@StrateRefNumber"
								,rm.LoanRefNumber AS "MonetaryDetails/@LoanRefNumber"
								,rm.LoanTenor AS "MonetaryDetails/@LoanTenor"
								,rm.LoanInterestRate AS "MonetaryDetails/@LoanInterestRate"
								,ISNULL(rm.RulingsSection, rm.RARulingsSection) AS "MonetaryDetails/@RulingsSection"
								,rm.SWIFTDetails AS "MonetaryDetails/@SWIFTDetails"
								,ISNULL(rm.ADInternalAuthNumber,RAREInternalAuthNumber) AS "MonetaryDetails/@ADInternalAuthNumber"
								,ISNULL(rm.ADInternalAuthNumberDate,RAREInternalAuthNumberDate) AS "MonetaryDetails/@ADInternalAuthNumberDate"
								,ISNULL(rm.SARBAuthApplicNumber,RACBAuthApplicNumber) AS "MonetaryDetails/@SARBAuthApplicNumber"
								,ISNULL(rm.SARBAuthReferenceNumber,RACBAuthReferenceNumber) AS "MonetaryDetails/@SARBAuthReferenceNumber"
								,rm.CannotCategorise AS "MonetaryDetails/@CannotCategorise"
								,rm.Subject AS "MonetaryDetails/@Subject"
								,rm.Description AS "MonetaryDetails/@Description"
								,rm.LocationCountry AS "MonetaryDetails/@LocationCountry"
								,rm.ReversalTrnRefNumber AS "MonetaryDetails/@ReversalTrnRefNumber"
								,rm.ReversalSequence AS "MonetaryDetails/@ReversalSequence"
								,rm.BOPDIRTrnReference AS "MonetaryDetails/@BOPDIRTrnReference"
								,rm.BOPDIRADCode AS "MonetaryDetails/@BOPDIRADCode"
								,rm.IndividualThirdPartySurname AS "MonetaryDetails/@IndividualThirdPartySurname"
								,rm.IndividualThirdPartyName AS "MonetaryDetails/@IndividualThirdPartyName"
								,rm.IndividualThirdPartyGender AS "MonetaryDetails/@IndividualThirdPartyGender"
								,rm.IndividualThirdPartyIDNumber AS "MonetaryDetails/@IndividualThirdPartyIDNumber"
								,rm.IndividualThirdPartyDateOfBirth AS "MonetaryDetails/@IndividualThirdPartyDateOfBirth"
								,rm.IndividualThirdPartyTempResPermitNumber AS "MonetaryDetails/@IndividualThirdPartyTempResPermitNumber"
								,rm.IndividualThirdPartyPassportNumber AS "MonetaryDetails/@IndividualThirdPartyPassportNumber"
								,rm.IndividualThirdPartyPassportCountry AS "MonetaryDetails/@IndividualThirdPartyPassportCountry"
								,rm.LegalEntityThirdPartyName AS "MonetaryDetails/@LegalEntityThirdPartyName"
								,rm.LegalEntityThirdPartyRegistrationNumber AS "MonetaryDetails/@LegalEntityThirdPartyRegistrationNumber"
								,rm.ThirdPartyCustomsClientNumber AS "MonetaryDetails/@ThirdPartyCustomsClientNumber"
								,rm.ThirdPartyTaxNumber AS "MonetaryDetails/@ThirdPartyTaxNumber"
								,rm.ThirdPartyVatNumber AS "MonetaryDetails/@ThirdPartyVatNumber"
								,rm.ThirdPartyStreetAddressLine1 AS "MonetaryDetails/@ThirdPartyStreetAddressLine1"
								,rm.ThirdPartyStreetAddressLine2 AS "MonetaryDetails/@ThirdPartyStreetAddressLine2"
								,rm.ThirdPartyStreetSuburb AS "MonetaryDetails/@ThirdPartyStreetSuburb"
								,rm.ThirdPartyStreetCity AS "MonetaryDetails/@ThirdPartyStreetCity"
								,rm.ThirdPartyStreetProvince AS "MonetaryDetails/@ThirdPartyStreetProvince"
								,rm.ThirdPartyStreetPostalCode AS "MonetaryDetails/@ThirdPartyStreetPostalCode"
								,rm.ThirdPartyPostalAddressLine1 AS "MonetaryDetails/@ThirdPartyPostalAddressLine1"
								,rm.ThirdPartyPostalAddressLine2 AS "MonetaryDetails/@ThirdPartyPostalAddressLine2"
								,rm.ThirdPartyPostalSuburb AS "MonetaryDetails/@ThirdPartyPostalSuburb"
								,rm.ThirdPartyPostalCity AS "MonetaryDetails/@ThirdPartyPostalCity"
								,rm.ThirdPartyPostalProvince AS "MonetaryDetails/@ThirdPartyPostalProvince"
								,rm.ThirdPartyPostalCode AS "MonetaryDetails/@ThirdPartyPostalCode"
								,rm.ThirdPartyContactSurname AS "MonetaryDetails/@ThirdPartyContactSurname"
								,rm.ThirdPartyContactName AS "MonetaryDetails/@ThirdPartyContactName"
								,rm.ThirdPartyEmail AS "MonetaryDetails/@ThirdPartyEmail"
								,rm.ThirdPartyFax AS "MonetaryDetails/@ThirdPartyFax"
								,rm.ThirdPartyTelephone AS "MonetaryDetails/@ThirdPartyTelephone"
								,rm.CardChargeBack AS "MonetaryDetails/@CardChargeBack"
								,rm.CardIndicator AS "MonetaryDetails/@CardIndicator"
								,rm.ElectronicCommerceIndicator AS "MonetaryDetails/@ElectronicCommerceIndicator"
								,rm.POSEntryMode AS "MonetaryDetails/@POSEntryMode"
								,rm.CardFraudulentTransactionIndicator AS "MonetaryDetails/@CardFraudulentTransactionIndicator"
								,rm.ForeignCardHoldersPurchasesRandValue AS "MonetaryDetails/@ForeignCardHoldersPurchasesRandValue"
								,rm.ForeignCardHoldersCashWithdrawalsRandValue AS "MonetaryDetails/@ForeignCardHoldersCashWithdrawalsRandValue"

								--,@ImportExport.query('/ImportExportData[@SequenceNumber=sql:column("rm.SequenceNumber")]') AS "MonetaryDetails/ImportExport"
								,@ImportExport.query('/ImportExportData[@SequenceNumber=sql:column("rm.SequenceNumber")]') AS "MonetaryDetails"
							from
							(
								SELECT
									r.ReportKey,
									r.GUID as ReportGuid,
									JSON_VALUE(r.Content, '$.Report.Flow') as 'Flow',
									JSON_VALUE(r.Content, '$.Report.ReportingQualifier') as 'ReportingQualifier',
									JSON_QUERY(r.Content, '$.Report.MonetaryAmount') as 'MonetaryAmount'
								FROM
									ReportView AS r
								WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							) as x
							CROSS APPLY
								OPENJSON(x.MonetaryAmount)
								WITH (
									SequenceNumber INT N'$.SequenceNumber'
									,MoneyTransferAgentIndicator NVARCHAR(70) N'$.MoneyTransferAgentIndicator'
									,RandValue NVARCHAR(200) N'$.RandValue'
									,DomesticValue NVARCHAR(200) N'$.DomesticValue'
									,ForeignValue NVARCHAR(70) N'$.ForeignValue'
									,ForeignCurrencyCode NVARCHAR(70) N'$.ForeignCurrencyCode'
									,BoPCategory NVARCHAR(70) N'$.CategoryCode'
									,SubBoPCategory NVARCHAR(70) N'$.CategorySubCode'
									,SWIFTDetails NVARCHAR(70) N'$.SWIFTDetails'

									,StrateRefNumber NVARCHAR(70) N'$.StrateRefNumber'
									,LoanRefNumber NVARCHAR(70) N'$.LoanRefNumber'
									,LoanTenor NVARCHAR(70) N'$.LoanTenor'
									,LoanInterestRate NVARCHAR(70) N'$.LoanInterestRate'

									,RulingsSection NVARCHAR(70) N'$.SARBAuth.RulingsSection'
									,ADInternalAuthNumber NVARCHAR(70) N'$.SARBAuth.ADInternalAuthNumber'
									,ADInternalAuthNumberDate NVARCHAR(70) N'$.SARBAuth.ADInternalAuthNumberDate'
									,SARBAuthApplicNumber NVARCHAR(70) N'$.SARBAuth.SARBAuthAppNumber'
									,SARBAuthReferenceNumber NVARCHAR(70) N'$.SARBAuth.SARBAuthRefNumber'

									,RARulingsSection NVARCHAR(70) N'$.RegulatorAuth.RulingsSection'
									,RAREInternalAuthNumber NVARCHAR(70) N'$.RegulatorAuth.REInternalAuthNumber'
									,RAREInternalAuthNumberDate NVARCHAR(70) N'$.RegulatorAuth.REInternalAuthNumberDate'
									,RACBAuthApplicNumber NVARCHAR(70) N'$.RegulatorAuth.CBAuthAppNumber'
									,RACBAuthReferenceNumber NVARCHAR(70) N'$.RegulatorAuth.CBAuthRefNumber'

									,CannotCategorise NVARCHAR(70) N'$.CannotCategorize'
									,Subject NVARCHAR(70) N'$.AdHocRequirement.Subject'
									,Description NVARCHAR(70) N'$.AdHocRequirement.Description'
									,LocationCountry NVARCHAR(70) N'$.LocationCountry'
									,ReversalTrnRefNumber NVARCHAR(70) N'$.ReversalTrnRefNumber'
									,ReversalSequence NVARCHAR(70) N'$.ReversalTrnSeqNumber'
									,BOPDIRTrnReference NVARCHAR(70) N'$.BOPDIRTrnReference'
									,BOPDIRADCode NVARCHAR(70) N'$.BOPDIRADCode'

									,IndividualThirdPartySurname NVARCHAR(70) N'$.ThirdParty.Individual.Surname'
									,IndividualThirdPartyName NVARCHAR(70) N'$.ThirdParty.Individual.Name'
									,IndividualThirdPartyGender NVARCHAR(70) N'$.ThirdParty.Individual.Gender'
									,IndividualThirdPartyIDNumber NVARCHAR(70) N'$.ThirdParty.Individual.IDNumber'
									,IndividualThirdPartyDateOfBirth NVARCHAR(70) N'$.ThirdParty.Individual.DateOfBirth'
									,IndividualThirdPartyTempResPermitNumber NVARCHAR(70) N'$.ThirdParty.Individual.TempResPermitNumber'
									,IndividualThirdPartyPassportNumber NVARCHAR(70) N'$.ThirdParty.Individual.PassportNumber'
									,IndividualThirdPartyPassportCountry NVARCHAR(70) N'$.ThirdParty.Individual.PassportCountry'
									,LegalEntityThirdPartyName NVARCHAR(70) N'$.ThirdParty.Entity.Name'
									,LegalEntityThirdPartyRegistrationNumber NVARCHAR(70) N'$.ThirdParty.Entity.RegistrationNumber'

									,ThirdPartyCustomsClientNumber NVARCHAR(70) N'$.ThirdParty.CustomsClientNumber'
									,ThirdPartyTaxNumber NVARCHAR(70) N'$.ThirdParty.TaxNumber'
									,ThirdPartyVatNumber NVARCHAR(70) N'$.ThirdParty.VatNumber'

									,ThirdPartyStreetAddressLine1 NVARCHAR(70) N'$.ThirdParty.StreetAddress.AddressLine1'
									,ThirdPartyStreetAddressLine2 NVARCHAR(70) N'$.ThirdParty.StreetAddress.AddressLine2'
									,ThirdPartyStreetSuburb NVARCHAR(70) N'$.ThirdParty.StreetAddress.Suburb'
									,ThirdPartyStreetCity NVARCHAR(70) N'$.ThirdParty.StreetAddress.City'
									,ThirdPartyStreetProvince NVARCHAR(70) N'$.ThirdParty.StreetAddress.Province'
									,ThirdPartyStreetPostalCode NVARCHAR(70) N'$.ThirdParty.StreetAddress.PostalCode'

									,ThirdPartyPostalAddressLine1 NVARCHAR(70) N'$.ThirdParty.PostalAddress.AddressLine1'
									,ThirdPartyPostalAddressLine2 NVARCHAR(70) N'$.ThirdParty.PostalAddress.AddressLine2'
									,ThirdPartyPostalSuburb NVARCHAR(70) N'$.ThirdParty.PostalAddress.Suburb'
									,ThirdPartyPostalCity NVARCHAR(70) N'$.ThirdParty.PostalAddress.City'
									,ThirdPartyPostalProvince NVARCHAR(70) N'$.ThirdParty.PostalAddress.Province'
									,ThirdPartyPostalCode NVARCHAR(70) N'$.ThirdParty.PostalAddress.PostalCode'

									,ThirdPartyContactSurname NVARCHAR(70) N'$.ThirdParty.ContactDetails.ContactSurname'
									,ThirdPartyContactName NVARCHAR(70) N'$.ThirdParty.ContactDetails.ContactName'
									,ThirdPartyEmail NVARCHAR(70) N'$.ThirdParty.ContactDetails.Email'
									,ThirdPartyFax NVARCHAR(70) N'$.ThirdParty.ContactDetails.Fax'
									,ThirdPartyTelephone NVARCHAR(70) N'$.ThirdParty.ContactDetails.Telephone'

									,CardChargeBack NVARCHAR(70) N'$.CardChargeBack'
									,CardIndicator NVARCHAR(70) N'$.CardIndicator'
									,ElectronicCommerceIndicator NVARCHAR(70) N'$.ElectronicCommerceIndicator'
									,POSEntryMode NVARCHAR(70) N'$.POSEntryMode'
									,CardFraudulentTransactionIndicator NVARCHAR(70) N'$.CardFraudulentTransactionIndicator'
									,ForeignCardHoldersPurchasesRandValue NVARCHAR(70) N'$.ForeignCardHoldersPurchasesRandValue'
									,ForeignCardHoldersCashWithdrawalsRandValue NVARCHAR(70) N'$.ForeignCardHoldersCashWithdrawalsRandValue'
								) AS rm
							ORDER BY rm.SequenceNumber
							FOR XML PATH(''), TYPE
						)



						----------  TRANSACTION DETAILS  ----------
						IF (@ShowMessages <> 0)
						BEGIN
							PRINT '...Transaction Details ('+@CurrentReportKey+')...'
						END

						DECLARE @TranXml XML
						SET @TranXml = (
							SELECT --TOP (1)
							--Transaction Root Level
								JSON_VALUE(r.Content, '$.Report.ReportingQualifier') AS "OriginalTransaction/@ReportingQualifier",
								JSON_VALUE(r.Content, '$.Report.Flow') AS "OriginalTransaction/@Flow",
								ISNULL(JSON_VALUE(r.Content, '$.Report.ReplacementTransaction'), 'N') AS "OriginalTransaction/@ReplacementTransaction",
								JSON_VALUE(r.Content, '$.Report.ReplacementOriginalReference') AS "OriginalTransaction/@ReplacementOriginalReference",
								ISNULL(JSON_VALUE(r.Content, '$.Report.ValueDate'), CONVERT(VARCHAR(10), GETDATE(), 120)) AS "OriginalTransaction/@Date",
								ISNULL(JSON_VALUE(r.Content, '$.Report.TrnReference'), @CurrentReportKey) AS "OriginalTransaction/@TrnReference",
								ISNULL(JSON_VALUE(r.Content, '$.Report.BranchCode'), '99500800') AS "OriginalTransaction/@BranchCode",
								ISNULL(JSON_VALUE(r.Content, '$.Report.BranchName'), 'Forex Head Office') AS "OriginalTransaction/@BranchName",
								JSON_VALUE(r.Content, '$.Report.OriginatingBank') AS "OriginalTransaction/@OriginatingBank",
								JSON_VALUE(r.Content, '$.Report.OriginatingCountry') AS "OriginalTransaction/@OriginatingCountry",
								JSON_VALUE(r.Content, '$.Report.ReceivingBank') AS "OriginalTransaction/@ReceivingBank",
								JSON_VALUE(r.Content, '$.Report.ReceivingCountry') AS "OriginalTransaction/@ReceivingCountry",
								REPLACE(REPLACE(JSON_VALUE(r.Content, '$.Report.TotalValue'), ',', ''), ' ', '') AS "OriginalTransaction/@TotalValue",
								@ResIndXml.query('/') as "OriginalTransaction/ResidentCustomerAccountHolder",
								@ResEntXml.query('/') as "OriginalTransaction/ResidentCustomerAccountHolder",
								@ResExXml.query('/') as "OriginalTransaction/ResidentCustomerAccountHolder",
								@NResIndXml.query('/') as "OriginalTransaction/NonResident",
								@NResEntXml.query('/') as "OriginalTransaction/NonResident",
								@NResExXml.query('/') as "OriginalTransaction/NonResident",
								@MoneyXml.query('/MonetaryDetails') as "OriginalTransaction"
								--JSON_VALUE(r.Content, '$.Report.TotalForeignValue') AS 'TotalForeignValue',
								--JSON_VALUE(r.Content, '$.Report.FlowCurrency') AS 'FlowCurrency'
							FROM
								ReportView as r
							WHERE r.ReportKey = @CurrentReportKey AND r.ReportSpace = @ReportSpace
							--FOR XML PATH('FINSURV'),  ROOT('FINSURV')
							FOR XML PATH(''), TYPE
						)


						SET @ExportXml.modify('
						insert sql:variable("@TranXml")
						as last
						into (/FINSURV)[1]
						')


					END TRY
					BEGIN CATCH
						PRINT '*****  ERROR  *****'
						SELECT
							ERROR_NUMBER() AS ErrorNumber
							,ERROR_SEVERITY() AS ErrorSeverity
							,ERROR_STATE() AS ErrorState
							,ERROR_PROCEDURE() AS ErrorProcedure
							,ERROR_LINE() AS ErrorLine
							,ERROR_MESSAGE() AS ErrorMessage
							,(SELECT TOP 1 [Content] FROM ReportView WHERE ReportSpace='SARB' AND ReportKey = @CurrentReportKey);
						PRINT 'THERE WAS A PROBLEM GENERATING THE XML FROM THE JSON OBJECTS IN THE DB FOR THE PROVIDED PARAMS.'
						PRINT ''
						PRINT '*******************************************************************************************************************'
						PRINT ''
						PRINT ''
						PRINT ''
					END CATCH

				END
				ELSE  /*  If the JSON was not deemed valid and parsable by MSSQL for whatever reason, print a message...  */
				BEGIN
					SELECT @message = 'INVALID JSON FOR TRANSACTION: ' + CONVERT(VARCHAR(40), @CurrentReportKey )
					PRINT @message
				END

				---------------------------------------------------------------------------------
				---------------------------------------------------------------------------------

				FETCH NEXT FROM ReportSource_Cursor
				INTO @CurrentReportKey
			END

			/*
				----------  ADD NAMESPACES, HEADERS AND ROOT LEVEL PACKAGING  ----------
			*/
			SET @ExportXml = (
				SELECT
					@ExportXml.query('
						declare namespace sarbdex="http://sarbdex.resbank.co.za/schemas/sarbdex_schema.xml";
						<sarbdex:SARBDEXEnvelope>
							<sarbdex:SARBDEXHeader Sender="{ sql:variable("@SenderName") }" Recipient="SARB" SentAt="{ sql:variable("@CurrentDate") }" Type="FINSURV" Version="1" Reference="{ sql:variable("@FileReference") }" Test="1" Identity="{ sql:variable("@Identity") }" IdentityType="{ sql:variable("@IdentityType") }"/>
							<sarbdex:SARBDEXBody>
								{/FINSURV}
							</sarbdex:SARBDEXBody>
						</sarbdex:SARBDEXEnvelope>
					')
				FOR XML PATH(''), TYPE
			)

		END TRY
		BEGIN CATCH
			PRINT '*****  ERROR  *****'
			SELECT
				ERROR_NUMBER() AS ErrorNumber
				,ERROR_SEVERITY() AS ErrorSeverity
				,ERROR_STATE() AS ErrorState
				,ERROR_PROCEDURE() AS ErrorProcedure
				,ERROR_LINE() AS ErrorLine
				,ERROR_MESSAGE() AS ErrorMessage;
			PRINT 'THERE WAS A PROBLEM GENERATING THE XML FROM THE JSON OBJECTS IN THE DB FOR THE PROVIDED PARAMS.'
			PRINT ''
			PRINT '*******************************************************************************************************************'
			PRINT ''
			PRINT ''
			PRINT ''
		END CATCH

		/*  CLEAN-UP CURSOR  */
		CLOSE ReportSource_Cursor
		DEALLOCATE ReportSource_Cursor

		PRINT ' ';
		PRINT '******  COMPLETED : GENERATION OF XML EXTRACT  ******';
		PRINT 'Completed: ' + CONVERT(VARCHAR(40), GETDATE(), 120);
		PRINT 'Total Time: ' + CONVERT(VARCHAR(40), DateDiff(Minute, @StartDateTime, GETDATE())) + ' mins';
	END;


	/*  CLEAN-UP TEMP TABLE  */
	DROP TABLE #ReportsToExport

	--SELECT @ExportXml;
	SET NOCOUNT OFF
END
GO



/*
-----  EXAMPLE SQL EXECUTION CODE TO EXTRACT SARB XML FILE USING STORED PROC ABOVE  -----

USE [ReportServices]
GO


SELECT TOP 100
  rv.*,
  JSON_VALUE(rv.[Content], '$.Report.ValueDate') AS 'ValueDate', JSON_VALUE(rv.[Content], '$.Report.MonetaryAmount[0].ImportExport[0].SubsequenceNumber') as 'Imp_Exp_No', ri.IndexValue
FROM ReportView AS rv
LEFT JOIN ReportIndex AS ri
ON rv.GUID = ri.ReportGuid AND ri.IndexField = 'ValueDate'
ORDER BY
	rv.CreatedDateTime desc


DECLARE @DayToRun DATE
SET @DayToRun = '2019-07-22'--(SELECT DATEADD(day, -1, GETDATE()))

DECLARE @TrnReference VARCHAR(200)
SET @TrnReference = NULL

DECLARE	@ExportXml xml

EXEC [dbo].[ExtractSARBXmlFromRDS]
		--@ValueDate = @DayToRun,
		@CreatedDate = @DayToRun,
		@ReportKey = @TrnReference,
		--@ReportState='Complete',
		@ExportXml = @ExportXml OUTPUT

SELECT @ExportXml AS N'SARB_Xml'

GO
*/





/* ------------------------------------------------------------------------------------------------------------------ */


IF OBJECT_ID (N'DeleteOldDocuments', N'P') IS NULL
    EXEC ('CREATE PROCEDURE [dbo].[DeleteOldDocuments] AS BEGIN SELECT 1 as Dummy END')
GO

ALTER PROCEDURE [dbo].[DeleteOldDocuments]
	@DeleteDays INT = 5
AS
BEGIN
	/*
		Author:		James Christopher Eckhardt
		Company:	Synthesis
		Edit Date: 	2019-07-30

		Description:
		Stored procedure is used to remove old document items in the database, to keep ReportServices DB in performant and minimal state

		Notes:

	*/

  DECLARE @DeleteDate DATE
  SET @DeleteDate = (SELECT DATEADD(DAY,(0-ABS(@DeleteDays)),GETDATE()))

	SET NOCOUNT ON
	/*
		...TO OVERRIDE THE DEFAULT DAY COUNT, A SELECT STATEMENT COULD BE ADDED HERE TO DO A LOOKUP AGAINST (PERHAPS) A STORED VALUE IN
		THE "SystemConfiguration" TABLE INSTEAD.
	*/
	PRINT 'DELETING DOCUMENT ENTRIES OLDER THAN '+CONVERT(VARCHAR(5), @DeleteDays)+' DAYS OLD / ADDED PRIOR TO '+CONVERT(VARCHAR(10),@DeleteDate,120)+'.'

	SELECT
		Updated.ReportSpace,
		Updated.ReportKey,
		MAX(Updated.LastUpdated) AS 'LastUpdated'
	INTO #DocumentRemoval
	FROM
	(
		SELECT
			ReportSpace,
			ReportKey,
			MAX(CreatedDateTime) AS 'LastUpdated'
		FROM Document
		GROUP BY
			ReportSpace,
			ReportKey
		UNION
		SELECT
			ReportSpace,
			ReportKey,
			MAX(CreatedDateTime) AS 'LastUpdated'
		FROM DocumentAck
		GROUP BY
			ReportSpace,
			ReportKey
		UNION
		SELECT
			ReportSpace,
			ReportKey,
			MAX(CreatedDateTime) AS 'LastUpdated'
		FROM DocumentDownload
		GROUP BY
			ReportSpace,
			ReportKey
	) AS Updated
	GROUP BY
		Updated.ReportSpace,
		Updated.ReportKey
	HAVING
		MAX(Updated.LastUpdated) < @DeleteDate

	--SELECT * FROM #DocumentRemoval

	PRINT 'REMOVING STALE DOCUMENT ENTRIES FROM DOCUMENT DOWNLOAD TABLE...'
	DELETE
	FROM DocumentDownload
	WHERE [GUID] IN (
		SELECT dd.GUID --dd.ReportSpace, dd.ReportKey, dd.DocumentHandle, dd.DocumentGuid
		FROM
			#DocumentRemoval AS dr
		JOIN
			DocumentDownload AS dd
		ON dd.ReportSpace = dr.ReportSpace AND dd.ReportKey = dr.ReportKey
	)


	PRINT 'REMOVING STALE DOCUMENT ENTRIES FROM DOCUMENT ACK TABLE...'
	DELETE
	FROM DocumentAck
	WHERE [GUID] IN (
		SELECT da.GUID --da.ReportSpace, da.ReportKey, da.DocumentHandle
		FROM
			#DocumentRemoval AS dr
		JOIN
			DocumentAck AS da
		ON da.ReportSpace = dr.ReportSpace AND da.ReportKey = dr.ReportKey
	)


	PRINT 'REMOVING STALE DOCUMENT ENTRIES FROM DOCUMENT TABLE...'
	--SELECT ReportSpace, ReportKey, DocumentHandle
	DELETE
	FROM Document
	WHERE [GUID] IN (
		SELECT d.GUID --d.ReportSpace, d.ReportKey, d.DocumentHandle
		FROM
			#DocumentRemoval AS dr
		JOIN
			Document AS d
		ON d.ReportSpace = dr.ReportSpace AND d.ReportKey = dr.ReportKey
	)
	DROP TABLE #DocumentRemoval

	SET NOCOUNT OFF
END
GO


/*
-----  EXAMPLE SQL EXECUTION CODE TO PURGE OLD DOCUMENTS USING STORED PROC ABOVE  -----

EXEC [ReportServices].[dbo].[DeleteOldDocuments]
*/


/* ------------------------------------------------------------------------------------------------------------------ */



