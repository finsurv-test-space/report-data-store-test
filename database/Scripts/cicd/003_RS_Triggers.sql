-- Only Used by Investec --
USE [Nightly_ReportServices]

IF OBJECT_ID (N'dbo.TRI_Notification', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRI_Notification
GO

CREATE TRIGGER dbo.TRI_Notification ON [dbo].[Notification] AFTER INSERT
AS
  INSERT INTO NotificationSel( GUID )
  SELECT GUID FROM inserted WHERE Status IS NULL
go

IF OBJECT_ID (N'dbo.TRU_Notification', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRU_Notification
GO

CREATE TRIGGER dbo.TRU_Notification ON [dbo].[Notification] AFTER UPDATE
AS
IF ( UPDATE (Status) )
BEGIN
  DELETE dbo.NotificationSel
   WHERE GUID IN
    (SELECT inserted.GUID
       FROM inserted
       JOIN deleted ON deleted.GUID = inserted.GUID
      WHERE inserted.Status IS NOT NULL AND deleted.Status IS NULL)

  INSERT INTO dbo.NotificationSel( GUID )
  SELECT inserted.GUID
    FROM inserted
    JOIN deleted ON deleted.GUID = inserted.GUID
   WHERE inserted.Status IS NULL AND deleted.Status IS NOT NULL
END
go

IF OBJECT_ID (N'dbo.TRD_Notification', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRD_Notification
GO

CREATE TRIGGER dbo.TRD_Notification ON [dbo].[Notification] INSTEAD OF DELETE
AS
  DELETE dbo.NotificationSel
    WHERE GUID IN (SELECT GUID FROM deleted)
  DELETE [dbo].[Notification]
   WHERE GUID IN (SELECT GUID FROM deleted)
go

-------------------------------------------------------------------------------------

IF OBJECT_ID (N'dbo.TRI_SysConfAudit', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRI_SysConfAudit
GO

CREATE TRIGGER dbo.TRI_SysConfAudit ON [dbo].[SystemConfiguration] AFTER INSERT
AS
  INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
  SELECT [Name], [Area], [Content], 'INSERT' FROM inserted
GO

IF OBJECT_ID (N'dbo.TRU_SysConfAudit', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRU_SysConfAudit
GO

CREATE TRIGGER dbo.TRU_SysConfAudit ON [dbo].[SystemConfiguration] AFTER UPDATE
AS
IF ( UPDATE ([Content]) )
BEGIN

  INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
  SELECT inserted.[Name], inserted.[Area], inserted.[Content], 'UPDATE'
	FROM inserted
	JOIN deleted ON deleted.[Name] = inserted.[Name]
END
GO

IF OBJECT_ID (N'dbo.TRD_SysConfAudit', N'TR') IS NOT NULL
  DROP TRIGGER dbo.TRD_SysConfAudit
GO

CREATE TRIGGER dbo.TRD_SysConfAudit ON [dbo].[SystemConfiguration] AFTER DELETE
AS
  INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
  SELECT deleted.[Name], deleted.[Area], deleted.[Content], 'DELETE'
  FROM deleted
GO
