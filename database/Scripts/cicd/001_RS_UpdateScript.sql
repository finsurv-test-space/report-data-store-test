USE [Nightly_ReportServices]

-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2021-03-05'  -----
-------------------------------------------------------------------------------


/*
------------------ SystemConfig Update -------------------------
*/
IF  EXISTS (SELECT * FROM dbo.[SystemConfiguration] WHERE [Area] IN ('Repo','ReportSpace','Type', 'List'))
BEGIN
    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''
    PRINT 'REMOVING STALE CONFIGURATIONS - WILL BE RELOADED FROM THE "Tomcat/Conf/..properties.json" FILE...'
    DELETE FROM dbo.[SystemConfiguration] WHERE [Area] IN ('Repo','ReportSpace','Type')
    PRINT '...STALE CONFIGS REMOVED.'
    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''
END




/*  -----------------------------------------------------------------------  */
/*  ----------  THE VERSION OF THE DATABASE SCRIPTS AND SCHEMAS  ----------  */
/*  -----------------------------------------------------------------------  */
DECLARE @DBVersion VARCHAR(50)
SET @DBVersion = '2021-04-09'
/*  -----------------------------------------------------------------------  */


/*
------------------ IF WE ARE UP TO DATE, THEN SKIP TO THE END ------------------
*/
IF EXISTS (SELECT * FROM dbo.[SystemConfiguration] WHERE [Name] = 'DB_Version' AND [Content] >= @DBVersion)
BEGIN
    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''
    PRINT 'DB Version appears to be up to date - Nothing to do...'
    GOTO COMPLETE_STATE
    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''
END

DECLARE @CurrentDBVersion VARCHAR(50)
SET @CurrentDBVersion = (SELECT TOP 1 [Content] FROM dbo.[SystemConfiguration] WHERE [Name] = 'DB_Version' ORDER BY [CreatedDateTime] DESC)

IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2018-02-21')
BEGIN
	GOTO RDS_20180221
END
ELSE IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2020-11-09')
BEGIN
	GOTO PRE_2020_11_09
END
ELSE IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2020-11-16')
BEGIN
	GOTO PRE_2020_11_16
END
ELSE IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2020-11-25')
BEGIN
	GOTO PRE_2020_11_26
END
ELSE IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2021-03-05')
BEGIN
	GOTO PRE_2021_03_05
END
ELSE IF (@CurrentDBVersion IS NULL OR @CurrentDBVersion < '2021-04-09')
BEGIN
	GOTO PRE_2021_04_09
END




------------------
PRE_2021_04_09:
------------------
-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2020-11-09'  -----
-------------------------------------------------------------------------------

---------------  ADDING VERSION COLUMN TO ACCOUNTENTRY  ------------------------
BEGIN TRANSACTION RDS_20210409_UPDATE

IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntry' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.AccountEntry','DataContent') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'AccountEntry.[DataContent] APPEARS TO BE UP TO DATE'
        END
        ELSE
        BEGIN
            PRINT '...AccountEntry UPDATE 20210409 REQUIRED...'
            PRINT 'ALTERING AccountEntry TABLE -> ADDING DataContent COLUMN...'
            ALTER TABLE dbo.AccountEntry ADD
                DataContent varchar(max) NULL
            ALTER TABLE dbo.AccountEntry SET (LOCK_ESCALATION = TABLE)
            PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

---------------  ADDING VERSION COLUMN TO ACCOUNTENTRYVIEW  ------------------------
 PRINT '...ALTER VIEW AccountEntryView TO ACCOMMODATE FOR [ContentType] COLUMN UPDATE.'
	EXEC ('
    ALTER VIEW dbo.AccountEntryView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Content], curr.[DataContent], curr.[TStamp], curr.[Version], curr.[CreatedDateTime]
            FROM dbo.[AccountEntry] curr
            LEFT JOIN [AccountEntry] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
			')
	PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
	PRINT ''


---------------  FINISH AND COMMIT/ROLLBACK  ------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20210409_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20210409_UPDATE
	GOTO COMPLETE_STATE
END




------------------
PRE_2021_03_05:
------------------
-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2020-11-25'  -----
-------------------------------------------------------------------------------
BEGIN TRANSACTION RDS_20210305_UPDATE

    IF  EXISTS (SELECT * FROM sys.objects WHERE name='SystemConfigurationAudit' AND type = 'U')
    BEGIN
		PRINT 'DROPPING EXISTING TABLE: SystemConfigurationAudit'
        DROP TABLE dbo.[SystemConfigurationAudit] --REMOVE.
    END

    BEGIN
        PRINT ''
        PRINT '------------------------------------------------------------------------------------------'
        PRINT ''
        PRINT 'CREATING TABLE: SystemConfigurationAudit'
            CREATE TABLE [dbo].[SystemConfigurationAudit](
                [GUID] [varchar](50) NOT NULL,
                [Name] [varchar](100) NOT NULL,
                [Area] [varchar](100) NULL,
                [Content] [varchar](max) NULL,
                [CreatedDateTime] [datetime] NOT NULL,
                [Action] [varchar](200) NOT NULL,
             CONSTRAINT [SystemConfigurationAuditPK] PRIMARY KEY CLUSTERED
            (
               [GUID] ASC, [Name] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

        ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
        ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT (newID()) FOR [GUID]
        ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT ('UPDATE') FOR [Action]
        PRINT 'SET DEFAULT VALUE FOR CreatedDateTime, GUID and Action'
    END

    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''


    IF OBJECT_ID (N'dbo.TRI_SysConfAudit', N'TR') IS NOT NULL
    BEGIN
      PRINT '...DROPPING TRIGGER TRI_SysConfAudit.'
      DROP TRIGGER dbo.TRI_SysConfAudit
    END

    PRINT '...CREATING TRIGGER TRI_SysConfAudit ON INSERT.'
    EXEC('
        CREATE TRIGGER dbo.TRI_SysConfAudit ON [dbo].[SystemConfiguration] AFTER INSERT
        AS
          INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
          SELECT [Name], [Area], [Content], ''INSERT'' FROM inserted
    ')


    IF OBJECT_ID (N'dbo.TRU_SysConfAudit', N'TR') IS NOT NULL
    BEGIN
      PRINT '...DROPPING TRIGGER TRU_SysConfAudit.'
      DROP TRIGGER dbo.TRU_SysConfAudit
    END

    PRINT '...CREATING TRIGGER TRU_SysConfAudit ON UPDATE.'
    EXEC('
        CREATE TRIGGER dbo.TRU_SysConfAudit ON [dbo].[SystemConfiguration] AFTER UPDATE
        AS
        IF ( UPDATE ([Content]) )
        BEGIN

          INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
          SELECT inserted.[Name], inserted.[Area], inserted.[Content], ''UPDATE''
            FROM inserted
            JOIN deleted ON deleted.[Name] = inserted.[Name]
        END
    ')


    IF OBJECT_ID (N'dbo.TRD_SysConfAudit', N'TR') IS NOT NULL
    BEGIN
      PRINT '...DROPPING TRIGGER TRD_SysConfAudit.'
      DROP TRIGGER dbo.TRD_SysConfAudit
    END

    PRINT '...CREATING TRIGGER TRD_SysConfAudit ON DELETE.'
    EXEC('
        CREATE TRIGGER dbo.TRD_SysConfAudit ON [dbo].[SystemConfiguration] AFTER DELETE
        AS
          INSERT INTO SystemConfigurationAudit( [Name], [Area], [Content], [Action] )
          SELECT deleted.[Name], deleted.[Area], deleted.[Content], ''DELETE''
          FROM deleted
    ')

    PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
    PRINT ''


---------------  FINISH AND COMMIT/ROLLBACK  ------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20210305_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20210305_UPDATE
	GOTO PRE_2021_04_09
END


------------------
PRE_2020_11_26:
------------------
-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2020-11-25'  -----
-------------------------------------------------------------------------------
BEGIN TRANSACTION RDS_20201126_UPDATE

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE name='Checksum' AND type = 'U')
	BEGIN
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
		PRINT 'CREATING TABLE: Checksum'
			CREATE TABLE [dbo].[Checksum](
			   [GUID] varchar(50) not null
                , [ReportSpace] varchar(50) null
                , [ValueDate] varchar(100) null
                , [AuthToken] varchar(max) null
                , [Version] int null
                , [Content] varchar(max) null
                , [DataContent] varchar(max) null
                , [TStamp] timestamp not null
                , [CreatedDateTime] datetime not null
                ,CONSTRAINT [ChecksumPK] PRIMARY KEY ([GUID])
            )

		ALTER TABLE [dbo].[Checksum] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
        PRINT 'SET DEFAULT VALUE FOR CreatedDateTime'

        CREATE INDEX Idx_Checksum_ReportSpace ON dbo.Checksum ([ReportSpace])
        PRINT 'CREATED INDEX Idx_Checksum_ReportSpace ON dbo.Checksum ([ReportSpace])'
        CREATE INDEX Idx_Checksum_ValueDate ON dbo.Checksum ([ValueDate])
        PRINT 'CREATED INDEX Idx_Checksum_ValueDate ON dbo.Checksum ([ValueDate])'
        CREATE INDEX Idx_Checksum_TStamp ON dbo.Checksum ([TStamp])
        PRINT 'CREATED INDEX Idx_Checksum_TStamp ON dbo.Checksum ([TStamp])'
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
	END

	IF  NOT EXISTS (SELECT * FROM sys.objects WHERE name='ChecksumDownload' AND type = 'U')
    	BEGIN
        	PRINT ''
    		PRINT '------------------------------------------------------------------------------------------'
        	PRINT ''
    		PRINT 'CREATING TABLE: ChecksumDownload'
    			CREATE TABLE [ChecksumDownload] (
                      [GUID] varchar(50) not null
                    , [TStamp] timestamp not null
                    , [ReportSpace] varchar(50) not null
                    , [ValueDate] varchar(100) null
                    , [CreatedDateTime] datetime not null
                    ,CONSTRAINT [ChecksumDownloadPK] PRIMARY KEY ([GUID])
                )

    		ALTER TABLE [dbo].[ChecksumDownload] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
            PRINT 'SET DEFAULT VALUE FOR CreatedDateTime'

            CREATE INDEX Idx_ChecksumDownload_ReportSpace ON dbo.ChecksumDownload ([ReportSpace])
            PRINT 'CREATED INDEX Idx_ChecksumDownload_ReportSpace ON dbo.ChecksumDownload ([ReportSpace])'
            CREATE INDEX Idx_ChecksumDownload_ValueDate ON dbo.ChecksumDownload ([ValueDate])
            PRINT 'CREATED INDEX Idx_ChecksumDownload_ValueDate ON dbo.ChecksumDownload ([ValueDate])'
            CREATE INDEX Idx_ChecksumDownload_TStamp ON dbo.ChecksumDownload ([TStamp])
            PRINT 'CREATED INDEX Idx_ChecksumDownload_TStamp ON dbo.ChecksumDownload ([TStamp])'
        	PRINT ''
    		PRINT '------------------------------------------------------------------------------------------'
        	PRINT ''
    	END

    	  ------------------ AccountEntryView -------------------------
        	PRINT ''
        	PRINT '------------------------------------------------------------------------------------------'
        	PRINT ''
            IF OBJECT_ID (N'dbo.ChecksumView', N'V') IS NULL
            BEGIN
                PRINT '...CREATE VIEW ChecksumView.'
                EXEC ('CREATE VIEW dbo.ChecksumView AS SELECT 1 as Dummy')
            END


            PRINT '...ALTER VIEW ChecksumView.'
        	EXEC ('
            ALTER VIEW dbo.ChecksumView AS
                SELECT curr.[GUID], curr.[ReportSpace], curr.[ValueDate], curr.[AuthToken], curr.[Content],curr.[DataContent], curr.[TStamp], curr.[Version], curr.[CreatedDateTime]
                    FROM dbo.[Checksum] curr
                    LEFT JOIN [Checksum] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ValueDate] = curr.[ValueDate] AND newer.[TStamp] > curr.[TStamp]
                    WHERE newer.[TStamp] IS NULL
        			')
        	PRINT ''
            PRINT '------------------------------------------------------------------------------------------'
        	PRINT ''



---------------  FINISH AND COMMIT/ROLLBACK  ------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20201126_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20201126_UPDATE
	GOTO PRE_2021_03_05
END

------------------
PRE_2020_11_16:
------------------
-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2020-11-09'  -----
-------------------------------------------------------------------------------

---------------  ADDING VERSION COLUMN TO ACCOUNTENTRY  ------------------------
BEGIN TRANSACTION RDS_20201116_UPDATE

IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntry' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.AccountEntry','Version') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'AccountEntry.[Version] APPEARS TO BE UP TO DATE'
        END
        ELSE
        BEGIN
            PRINT '...AccountEntry UPDATE 20201116 REQUIRED...'
            PRINT 'ALTERING AccountEntry TABLE -> ADDING Version COLUMN...'
            ALTER TABLE dbo.AccountEntry ADD
                Version int NULL
            ALTER TABLE dbo.AccountEntry SET (LOCK_ESCALATION = TABLE)
            PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

---------------  ADDING VERSION COLUMN TO ACCOUNTENTRYVIEW  ------------------------
 PRINT '...ALTER VIEW AccountEntryView TO ACCOMMODATE FOR [Version] COLUMN UPDATE.'
	EXEC ('
    ALTER VIEW dbo.AccountEntryView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Content], curr.[TStamp], curr.[Version], curr.[CreatedDateTime]
            FROM dbo.[AccountEntry] curr
            LEFT JOIN [AccountEntry] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
			')
	PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
	PRINT ''


---------------  FINISH AND COMMIT/ROLLBACK  ------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20201116_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20201116_UPDATE
	GOTO PRE_2020_11_26
END



------------------
PRE_2020_11_09:
------------------
-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2020-11-09'  -----
-------------------------------------------------------------------------------
BEGIN TRANSACTION RDS_20201109_UPDATE

    ------------------ AccountEntryDownload -------------------------
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntry' AND type = 'U')
	BEGIN
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
		PRINT 'AccountEntry: Creating AccountEntry table...'
        CREATE TABLE [dbo].[AccountEntry](
			[GUID] [varchar](50) NOT NULL,
			[ReportSpace] [varchar](50) NULL,
			[ReportKey] [varchar](100) NULL,
			[AuthToken] [varchar](max) NULL,
			[Version] [int] NULL,
			[Content] [varchar](max) NULL,
			[TStamp] [timestamp] NOT NULL,
			[CreatedDateTime] [datetime] NOT NULL DEFAULT GETDATE()
		 CONSTRAINT [AccountEntryPK] PRIMARY KEY CLUSTERED
		(
			[GUID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


		ALTER TABLE [dbo].[AccountEntry] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]

        CREATE INDEX Idx_AccountEntry_ReportSpace ON dbo.AccountEntry ([ReportSpace])
        PRINT 'CREATED INDEX Idx_AccountEntry_ReportSpace ON dbo.AccountEntry ([ReportSpace])'
        CREATE INDEX Idx_AccountEntry_ReportKey ON dbo.AccountEntry ([ReportKey])
        PRINT 'CREATED INDEX Idx_AccountEntry_ReportKey ON dbo.AccountEntry ([ReportKey])'
        CREATE INDEX Idx_AccountEntry_CreatedDT ON dbo.AccountEntry ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_AccountEntry_CreatedDT ON dbo.AccountEntry ([CreatedDateTime])'
        CREATE INDEX Idx_AccountEntry_TStamp ON dbo.AccountEntry ([TStamp])
        PRINT 'CREATED INDEX Idx_AccountEntry_TStamp ON dbo.AccountEntry ([TStamp])'
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
	END

	IF  NOT EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntryDownload' AND type = 'U')
	BEGIN
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
		PRINT 'CREATING TABLE: AccountEntryDownload'
			CREATE TABLE [dbo].[AccountEntryDownload](
			[GUID] [varchar](50) NOT NULL,
			[TStamp] [timestamp] NOT NULL,
			[ReportSpace] [varchar](50) NOT NULL,
			[ReportKey] [varchar](100) NULL,
			[AccountEntryGuid] [varchar](50) NOT NULL,
			[CreatedDateTime] [datetime] NOT NULL DEFAULT GETDATE(),
		 CONSTRAINT [AccountEntryDownloadPK] PRIMARY KEY CLUSTERED
		(
			[GUID] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		--ALTER TABLE [dbo].[AccountEntryDownload] ADD  DEFAULT ('Update') FOR [ReportDownloadType]

		ALTER TABLE [dbo].[AccountEntryDownload] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
        PRINT 'SET DEFAULT VALUE FOR CreatedDateTime'

        CREATE INDEX Idx_AccountEntryDownload_ReportSpace ON dbo.AccountEntryDownload ([ReportSpace])
        PRINT 'CREATED INDEX Idx_AccountEntryDownload_ReportSpace ON dbo.AccountEntryDownload ([ReportSpace])'
        CREATE INDEX Idx_AccountEntryDownload_ReportKey ON dbo.AccountEntryDownload ([ReportKey])
        PRINT 'CREATED INDEX Idx_AccountEntryDownload_ReportKey ON dbo.AccountEntryDownload ([ReportKey])'
        CREATE INDEX Idx_AccountEntryDownload_TStamp ON dbo.AccountEntryDownload ([TStamp])
        PRINT 'CREATED INDEX Idx_AccountEntryDownload_TStamp ON dbo.AccountEntryDownload ([TStamp])'
    	PRINT ''
		PRINT '------------------------------------------------------------------------------------------'
    	PRINT ''
	END

    ------------------ AccountEntryView -------------------------
	PRINT ''
	PRINT '------------------------------------------------------------------------------------------'
	PRINT ''
    IF OBJECT_ID (N'dbo.AccountEntryView', N'V') IS NULL
    BEGIN
        PRINT '...CREATE VIEW AccountEntryView.'
        EXEC ('CREATE VIEW dbo.AccountEntryView AS SELECT 1 as Dummy')
    END


    PRINT '...ALTER VIEW AccountEntryView.'
	EXEC ('
    ALTER VIEW dbo.AccountEntryView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
            FROM dbo.[AccountEntry] curr
            LEFT JOIN [AccountEntry] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
			')
	PRINT ''
    PRINT '------------------------------------------------------------------------------------------'
	PRINT ''



    ------------------ CHECK FOR MISSING CreatedDateTime INDICES -------------------------
	PRINT ''
	PRINT '------------------------------------------------------------------------------------------'
	PRINT ''
	PRINT 'CHECKING FOR MISSING INDICES...'
    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_Reprt_CreatedDT')
    BEGIN
        CREATE INDEX Idx_Reprt_CreatedDT ON dbo.Report ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_Reprt_CreatedDT ON dbo.Report ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_Decision_CreatedDT')
    BEGIN
        CREATE INDEX Idx_Decision_CreatedDT ON dbo.Decision ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_Decision_CreatedDT ON dbo.Decision ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_AccountEntry_CreatedDT')
    BEGIN
        CREATE INDEX Idx_AccountEntry_CreatedDT ON dbo.AccountEntry ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_AccountEntry_CreatedDT ON dbo.AccountEntry ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_Document_CreatedDT')
    BEGIN
        CREATE INDEX Idx_Document_CreatedDT ON dbo.Document ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_Document_CreatedDT ON dbo.Document ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_DocumentAck_CreatedDT')
    BEGIN
        CREATE INDEX Idx_DocumentAck_CreatedDT ON dbo.DocumentAck ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_DocumentAck_CreatedDT ON dbo.DocumentAck ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_ReportIndex_CreatedDT')
    BEGIN
        CREATE INDEX Idx_ReportIndex_CreatedDT ON dbo.ReportIndex ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_ReportIndex_CreatedDT ON dbo.ReportIndex ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='Idx_Notif_CreatedDT')
    BEGIN
        CREATE INDEX Idx_Notif_CreatedDT ON dbo.Notification ([CreatedDateTime])
        PRINT 'CREATED INDEX Idx_Notif_CreatedDT ON dbo.Notification ([CreatedDateTime])'
    END

    IF NOT EXISTS (SELECT 'foo' FROM sys.indexes WHERE name='NDX_FullTextCreatedDT')
    BEGIN
        CREATE INDEX NDX_FullTextCreatedDT ON dbo.ReportFullText ([CreatedDateTime])
        PRINT 'CREATED INDEX NDX_FullTextCreatedDT ON dbo.ReportFullText ([CreatedDateTime])'
    END
	PRINT ''
	PRINT '------------------------------------------------------------------------------------------'
	PRINT ''

---------------  FINISH AND COMMIT/ROLLBACK  ------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20201109_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20201109_UPDATE
	GOTO PRE_2020_11_16
END



-------------------------------------------------------------------------------
-----  THIS SECTION WILL UPDATE THE DB SCHEMA TO DB VERSION '2019-11-01'  -----
-------------------------------------------------------------------------------
------------------
RDS_20180221:
------------------
    /*
        Summary of updates
        ------------------

        1) RDS_20180221_UPDATE
          - Add Download Type to the ReportDownload Table.
          - Assign default ReportDownloadType value ("Update") to existing ReportDownload records.
          - Deprecate and remove ReportDelQueue table from DB schema.
          - Add a DecisionLog table to the DB schema.
        2) RDS_20180901_UPDATE
          - Add [CreatedDateTime] to all db tables
          - Add [CreatedDateTime] to ReportView, DecisionView, DocumentView, DocumentAckView definitions.
    */

------------------ ZeroAuthFilter User -------------------------
-- *** THIS USER IS USED TO CREATE AN UNAUTHENTICATED CONNECTION, IT'S PASSWORD SHOULD BE STATIC FOR ALL ENVIRONMENTS AS THE CONFIG IS BUILT INTO THE WARS  *** --
/*
    IF  NOT EXISTS (SELECT * FROM dbo.[User] WHERE [Key] ='developer' and [Content] LIKE '%"password":"$2a$15$oG3scaAWCmA6eohYUgxpQ.IF8PaEXePki9P88z6DvGMZJnHs8fnbq"%')
    BEGIN
        PRINT 'INSERTING THE DEVELOPER (ZERO-AUTH) USER WITH CORRECT PASSWORD...'
        INSERT INTO dbo.[User]([GUID], [Key], [AuthToken], [Version], [Content])
        VALUES (NEWID(), 'developer',    NULL, 1, '{ "username":"developer",    "password":"$2a$15$oG3scaAWCmA6eohYUgxpQ.IF8PaEXePki9P88z6DvGMZJnHs8fnbq", "rights":"USER, PRODUCER_API", "access": ["unrestricted"], "channels":["coreSARB", "coreSADC", "coreBON", "sbZA", "sbNA"] }')
        PRINT '...ZERO-AUTH USER INSERTED AS REQUIRED.'
    END
*/
----------------------------------------------------------------


------------------ ReportDownload -------------------------
BEGIN TRANSACTION --'RDS_20180221_UPDATE'
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDownload' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.ReportDownload', 'DownloadType') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT '**NOTE: THIS DB IS OUT OF DATE (20180221 - 20180401) - IT IS RECOMMENDED THAT YOU RUN THE *NEW* INSTALLATION SQL SCRIPTS (IF THIS IS A NON PROD/TEST SYSTEM) - OR RESOLVE THE ISSUE MANUALLY BY RENAMING THE ReportDownload.DownloadType field to ReportDownload.ReportDownloadType FROM THE DB MANAGEMENT APPLICATION.'
        END
        ELSE
        BEGIN
			IF COL_LENGTH('dbo.ReportDownload', 'ReportDownloadType') IS NOT NULL
			BEGIN
				-- Column Exists
				PRINT '...ReportDownload UP TO DATE (20180901).'
			END
			ELSE
			BEGIN
				PRINT '...ReportDownload UPDATE 20180221 REQUIRED...'
				PRINT 'ALTERING ReportDownload TABLE -> ADDING ReportDownloadType COLUMN...'
				ALTER TABLE dbo.ReportDownload ADD
					ReportDownloadType varchar(50) NULL
				ALTER TABLE dbo.ReportDownload SET (LOCK_ESCALATION = TABLE)
				PRINT '...ALTERATION COMPLETE.'
			END
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END




    IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDownload' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_DownloadTypeDefault' AND type = 'D')
    BEGIN
        PRINT 'ReportDownload: ASSIGNING DEFAULT ReportDownloadType VALUES ("Update")...'
        IF NOT EXISTS (
            SELECT *
            FROM sys.all_columns c
              JOIN sys.tables t ON t.object_id = c.object_id
              JOIN sys.schemas s ON s.schema_id = t.schema_id
              JOIN sys.default_constraints d ON c.default_object_id = d.object_id
            WHERE t.name = 'ReportDownload'
              AND c.name = 'ReportDownloadType'
              AND s.name = 'dbo'
            )
        BEGIN
            UPDATE dbo.[ReportDownload] SET [ReportDownloadType] = 'Update'
            print 'ADD DEFAULT VALUE ''Update''...'
            ALTER TABLE dbo.ReportDownload ADD CONSTRAINT DF_DownloadTypeDefault DEFAULT N'Update' FOR ReportDownloadType
            PRINT 'SETTING ReportDownloadType NOT NULL, DEFAULT ''Update''...'
            ALTER TABLE dbo.ReportDownload ALTER COLUMN ReportDownloadType varchar(50) not null
            PRINT '...Column altered'
        END
        ELSE
        BEGIN
            PRINT '...already has a default value.'
        END
    END

    ------------------ Remove ReportDelQueue -------------------------
    --**  THIS TABLE HAS BEEN DEPRECATED AND REMOVED FROM SCHEMA  **--
    ------------------------------------------------------------------
    IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDelQueue' AND type = 'U')
    BEGIN
        PRINT '...DEPRECATE/REMOVE ReportDelQueue TABLE.'
        DROP TABLE dbo.[ReportDelQueue] --REMOVE AND DON'T RECREATE.
    END
    ELSE
        PRINT '...ReportDelQueue TABLE ALREADY DEPRECATED AND REMOVED - OR EXCLUDED.'




    ------------------ Adding ReportDelQueue -----------------------
    --**  THIS TABLE was added to the schema around 2018-05-08  **--
    ----------------------------------------------------------------
    IF  NOT EXISTS (SELECT * FROM sys.objects WHERE name='DecisionLog' AND type = 'U')
    BEGIN
        PRINT 'CREATING DECISION LOG TABLE...'
        CREATE TABLE dbo.[DecisionLog] (
              [GUID] varchar(50) not null
            , [ReportSpace] varchar(50) null
            , [ReportKey] varchar(100) null
            , [AuthToken] varchar(max) null
            , [Version] int null
            , [Content] varchar(max) null
            , [TStamp] timestamp not null
            ,CONSTRAINT [DecisionLogPK] PRIMARY KEY ([GUID])
        )
        PRINT 'DECISION LOG TABLE CREATED.'
    END
COMMIT --'RDS_20180221_UPDATE'




--IF  NOT EXISTS (SELECT * FROM sys.objects WHERE name='ReportDelQueue' AND type = 'U')
--BEGIN
    ------------------ Views -------------------------
    IF OBJECT_ID (N'dbo.ReportView', N'V') IS NULL
    BEGIN
        PRINT '...CREATE VIEW ReportView.'
        EXEC ('CREATE VIEW dbo.ReportView AS SELECT 1 as Dummy')
    END


    PRINT '...ALTER VIEW ReportView.'
	EXEC ('
	ALTER VIEW dbo.ReportView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Version], curr.[Schema], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
            FROM dbo.[Report] curr
            LEFT JOIN [Report] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
	')




    IF OBJECT_ID (N'dbo.DecisionView', N'V') IS NULL
    BEGIN
        PRINT '...CREATE VIEW DecisionView.'
        EXEC ('CREATE VIEW dbo.DecisionView AS SELECT 1 as Dummy')
    END


    PRINT '...ALTER VIEW DecisionView.'
    EXEC ('
    ALTER VIEW dbo.DecisionView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Version], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
            FROM dbo.[Decision] curr
            LEFT JOIN [Decision] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
    ')




    IF OBJECT_ID (N'dbo.DocumentView', N'V') IS NULL
    BEGIN
        PRINT '...CREATE VIEW DocumentView.'
        EXEC ('CREATE VIEW dbo.DocumentView AS SELECT 1 as Dummy')
    END


    PRINT '...ALTER VIEW DocumentView.'
    EXEC ('
    ALTER VIEW dbo.DocumentView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[DocumentHandle], curr.[AuthToken], curr.[Version], curr.[Content], curr.[DataContent], curr.[TStamp], curr.[CreatedDateTime]
            FROM dbo.[Document] curr
            LEFT JOIN [Document] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[DocumentHandle] = curr.[DocumentHandle] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
    ')




    IF OBJECT_ID (N'dbo.DocumentAckView', N'V') IS NULL
    BEGIN
        PRINT '...CREATE VIEW DocumentAckView.'
        EXEC ('CREATE VIEW dbo.DocumentAckView AS SELECT 1 as Dummy')
    END


    PRINT '...ALTER VIEW DocumentAckView.'
    EXEC ('
    ALTER VIEW dbo.DocumentAckView AS
        SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[DocumentHandle], curr.[Acknowledged], curr.[AcknowledgedComment], curr.[AuthToken], curr.[TStamp], curr.[CreatedDateTime]
            FROM dbo.[DocumentAck] curr
            LEFT JOIN [DocumentAck] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[DocumentHandle] = curr.[DocumentHandle] AND newer.[TStamp] > curr.[TStamp]
            WHERE newer.[TStamp] IS NULL
    ')
--END





-------------------------------------------------------------------
--  BEGIN: ADDITION OF TIME STAMP FIELDS TO ALL RELEVANT TABLES  --
-------------------------------------------------------------------

------------------ Report -------------------------
BEGIN TRANSACTION RDS_20180901_UPDATE
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Report' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.Report', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for Report'
        END
        ELSE
        BEGIN
			PRINT '...Report (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING Report TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.Report ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.Report SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ Decision -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Decision' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.Decision', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for Decision'
        END
        ELSE
        BEGIN
			PRINT '...Decision (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING Decision TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.Decision ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.Decision SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ Document -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Document' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.Document', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for Document'
        END
        ELSE
        BEGIN
			PRINT '...Document (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING Document TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.Document ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.Document SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ ReportSpace -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportSpace' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.ReportSpace', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for ReportSpace'
        END
        ELSE
        BEGIN
			PRINT '...ReportSpace (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING ReportSpace TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.ReportSpace ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.ReportSpace SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ User -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='User' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.User', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for User'
        END
        ELSE
        BEGIN
			PRINT '...User (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING User TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.[User] ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ ReportDownload -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDownload' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.ReportDownload', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for ReportDownload'
        END
        ELSE
        BEGIN
			PRINT '...ReportDownload (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING ReportDownload TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.ReportDownload ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.ReportDownload SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ DecisionDownload -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionDownload' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.DecisionDownload', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for DecisionDownload'
        END
        ELSE
        BEGIN
			PRINT '...DecisionDownload (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING DecisionDownload TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.DecisionDownload ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.DecisionDownload SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ DocumentAck -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentAck' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.DocumentAck', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for DocumentAck'
        END
        ELSE
        BEGIN
			PRINT '...DocumentAck (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING DocumentAck TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.DocumentAck ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.DocumentAck SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ DocumentDownload -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentDownload' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.DocumentDownload', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for DocumentDownload'
        END
        ELSE
        BEGIN
			PRINT '...DocumentDownload (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING DocumentDownload TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.DocumentDownload ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.DocumentDownload SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ AccessSet -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccessSet' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.AccessSet', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for AccessSet'
        END
        ELSE
        BEGIN
			PRINT '...AccessSet (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING AccessSet TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.AccessSet ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.AccessSet SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ List -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='List' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.List', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for List'
        END
        ELSE
        BEGIN
			PRINT '...List (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING List TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.List ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.List SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ ReportIndex -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportIndex' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.ReportIndex', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for ReportIndex'
        END
        ELSE
        BEGIN
			PRINT '...ReportIndex (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING ReportIndex TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.ReportIndex ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.ReportIndex SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ Notification -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Notification' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.Notification', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for Notification'
        END
        ELSE
        BEGIN
			PRINT '...Notification (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING Notification TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.Notification ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.Notification SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ SystemConfiguration -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='SystemConfiguration' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.SystemConfiguration', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for SystemConfiguration'
        END
        ELSE
        BEGIN
			PRINT '...SystemConfiguration (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING SystemConfiguration TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.SystemConfiguration ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.SystemConfiguration SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ ReportFullText -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportFullText' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.ReportFullText', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for ReportFullText'
        END
        ELSE
        BEGIN
			PRINT '...ReportFullText (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING ReportFullText TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.ReportFullText ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.ReportFullText SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END

------------------ DecisionLog -------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionLog' AND type = 'U')
	BEGIN
        IF COL_LENGTH('dbo.DecisionLog', 'CreatedDateTime') IS NOT NULL
        BEGIN
            -- Column Exists
            PRINT 'CreatedDateTime column exists for DecisionLog'
        END
        ELSE
        BEGIN
			PRINT '...DecisionLog (DateTime record stamping) UPDATE 20180901 REQUIRED...'
			PRINT 'ALTERING DecisionLog TABLE -> ADDING CreatedDateTime COLUMN...'
			ALTER TABLE dbo.DecisionLog ADD
				CreatedDateTime datetime NULL
			ALTER TABLE dbo.DecisionLog SET (LOCK_ESCALATION = TABLE)
			PRINT '...ALTERATION COMPLETE.'
        END
	END
	ELSE
	BEGIN
		PRINT 'IT SEEMS YOU ARE MISSING THE REQUIRED TABLES IN THIS DB - PLEASE RUN THE CREATE SCRIPTS INSTEAD OF THE UPDATE.'
	END



	--------------------------------------------------  ADD THE CONSTRAINTS ---------------------------------------------------
	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Report' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_Report_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'Report: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
		IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'Report'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[Report] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.Report ADD CONSTRAINT DF_Report_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.Report ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...already has a default value.'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Decision' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_Decision_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'Decision: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'Decision'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[Decision] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.Decision ADD CONSTRAINT DF_Decision_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.Decision ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Document' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_Document_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'Document: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'Document'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[Document] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.Document ADD CONSTRAINT DF_Document_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.Document ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportSpace' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_ReportSpace_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'ReportSpace: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'ReportSpace'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[ReportSpace] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.ReportSpace ADD CONSTRAINT DF_ReportSpace_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.ReportSpace ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='User' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_User_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'User: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'User'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[User] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.[User] ADD CONSTRAINT DF_User_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.[User] ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDownload' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_ReportDownload_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'ReportDownload: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'ReportDownload'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[ReportDownload] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.ReportDownload ADD CONSTRAINT DF_ReportDownload_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.ReportDownload ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionDownload' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_DecisionDownload_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'DecisionDownload: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'DecisionDownload'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[DecisionDownload] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.DecisionDownload ADD CONSTRAINT DF_DecisionDownload_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.DecisionDownload ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentAck' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_DocumentAck_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'DocumentAck: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'DocumentAck'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[DocumentAck] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.DocumentAck ADD CONSTRAINT DF_DocumentAck_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.DocumentAck ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentDownload' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_DocumentDownload_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'DocumentDownload: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'DocumentDownload'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[DocumentDownload] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.DocumentDownload ADD CONSTRAINT DF_DocumentDownload_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.DocumentDownload ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccessSet' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_AccessSet_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'AccessSet: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'AccessSet'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[AccessSet] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.AccessSet ADD CONSTRAINT DF_AccessSet_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.AccessSet ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='List' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_List_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'List: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'List'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[List] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.List ADD CONSTRAINT DF_List_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.List ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportIndex' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_ReportIndex_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'ReportIndex: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'ReportIndex'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[ReportIndex] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.ReportIndex ADD CONSTRAINT DF_ReportIndex_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.ReportIndex ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='Notification' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_Notification_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'Notification: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'Notification'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[Notification] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.Notification ADD CONSTRAINT DF_Notification_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.Notification ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END




	IF  EXISTS (SELECT * FROM sys.objects WHERE name='SystemConfiguration' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_SystemConfiguration_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'SystemConfiguration: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'SystemConfiguration'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[SystemConfiguration] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.SystemConfiguration ADD CONSTRAINT DF_SystemConfiguration_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.SystemConfiguration ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END



	IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportFullText' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_ReportFullText_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'ReportFullText: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'ReportFullText'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[ReportFullText] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.ReportFullText ADD CONSTRAINT DF_ReportFullText_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.ReportFullText ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END



	IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionLog' AND type = 'U') AND NOT EXISTS (SELECT * FROM sys.objects WHERE name='DF_DecisionLog_CreatedDateTimeDefault' AND type = 'D')
	BEGIN
		PRINT 'DecisionLog: ASSIGNING DEFAULT CreatedDateTime VALUES GETDATE()...'
			IF NOT EXISTS (
			SELECT *
			FROM sys.all_columns c
			  JOIN sys.tables t ON t.object_id = c.object_id
			  JOIN sys.schemas s ON s.schema_id = t.schema_id
			  JOIN sys.default_constraints d ON c.default_object_id = d.object_id
			WHERE t.name = 'DecisionLog'
			  AND c.name = 'CreatedDateTime'
			  AND s.name = 'dbo'
			)
		BEGIN
			UPDATE dbo.[DecisionLog] SET [CreatedDateTime] = GETDATE()
			print 'ADD DEFAULT VALUE GETDATE()...'
			ALTER TABLE dbo.DecisionLog ADD CONSTRAINT DF_DecisionLog_CreatedDateTimeDefault DEFAULT GETDATE() FOR CreatedDateTime
			PRINT 'SETTING CreatedDateTime NOT NULL, DEFAULT GETDATE()...'
			ALTER TABLE dbo.DecisionLog ALTER COLUMN CreatedDateTime datetime not null
			PRINT '...Column altered'
		END
		ELSE
		BEGIN
			PRINT '...Default already assigned'
		END
	END

	--  END: ADDITION OF TIME STAMP FIELDS TO ALL RELEVANT TABLES  --
	-----------------------------------------------------------------
IF (@@ERROR <> 0)
BEGIN
	PRINT ''
	PRINT ''
	PRINT '*****  ERROR - ABORTING UPDATE, ROLLBACK INITIATED.  *****'
	PRINT ''
	PRINT ''
	ROLLBACK TRANSACTION RDS_20180901_UPDATE
	GOTO ERROR_STATE
END
ELSE
BEGIN
	COMMIT TRANSACTION RDS_20180901_UPDATE
	GOTO PRE_2020_11_09
END


-------------------------------------------------------------------
-------------------------------------------------------------------
COMPLETE_STATE:
	PRINT '...UPDATING DB VERSION: '+@DBVersion
	IF EXISTS (SELECT * FROM dbo.[SystemConfiguration] WHERE [Name] = 'DB_Version')
	BEGIN
		UPDATE dbo.[SystemConfiguration]
		SET [Content] = @DBVersion
		WHERE [Name] = 'DB_Version'
	END
	ELSE
	BEGIN
		INSERT INTO dbo.[SystemConfiguration] ([Name], [Area], [Content], [CreatedDateTime])
		VALUES ('DB_Version', 'Version', @DBVersion, GETDATE())
	END
	PRINT '...MSSQL Schema updates complete.'
	--RETURN 0;
	GOTO END_STATE

-------------------------------------------------------------------
ERROR_STATE:
------------
	PRINT '...ERROR ENCOUNTERED WHILE UPDATING TO DB VERSION: '+@DBVersion
	--RETURN 99;
	GOTO END_STATE
-------------------------------------------------------------------

-------------------------------------------------------------------
END_STATE:
	PRINT 'SCRIPT COMPLETE.'
-------------------------------------------------------------------