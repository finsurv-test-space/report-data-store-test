USE [Nightly_ReportServices]

IF (NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name='ReportServiceFTCat')) AND (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
BEGIN
  CREATE FULLTEXT CATALOG ReportServiceFTCat;
END

------------------ Report -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='Report' AND type = 'U')
BEGIN
  DROP TABLE [Report]
END
------------------ Decision -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='Decision' AND type = 'U')
BEGIN
  DROP TABLE [Decision]
END
------------------ AccountEntry -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntry' AND type = 'U')
BEGIN
  DROP TABLE [AccountEntry]
END
------------------ Document -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='Document' AND type = 'U')
BEGIN
  DROP TABLE [Document]
END
------------------ ReportSpace -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportSpace' AND type = 'U')
BEGIN
  DROP TABLE [ReportSpace]
END
------------------ ConfigDelQueue -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='ConfigDelQueue' AND type = 'U')
BEGIN
  DROP TABLE [ConfigDelQueue]
END
------------------ User -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='User' AND type = 'U')
BEGIN
  DROP TABLE [User]
END
------------------ ReportDownload -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportDownload' AND type = 'U')
BEGIN
  DROP TABLE [ReportDownload]
END
------------------ DecisionDownload -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionDownload' AND type = 'U')
BEGIN
  DROP TABLE [DecisionDownload]
END
------------------ AccountEntryDownload -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccountEntryDownload' AND type = 'U')
BEGIN
  DROP TABLE [AccountEntryDownload]
END
------------------ DocumentAck -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentAck' AND type = 'U')
BEGIN
  DROP TABLE [DocumentAck]
END
------------------ DocumentDownload -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='DocumentDownload' AND type = 'U')
BEGIN
  DROP TABLE [DocumentDownload]
END
------------------ AccessSet -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='AccessSet' AND type = 'U')
BEGIN
  DROP TABLE [AccessSet]
END
------------------ List -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='List' AND type = 'U')
BEGIN
  DROP TABLE [List]
END
------------------ ReportIndex -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportIndex' AND type = 'U')
BEGIN
  DROP TABLE [ReportIndex]
END
------------------ Notification -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='Notification' AND type = 'U')
BEGIN
  DROP TABLE [Notification]
END
------------------ NotificationSel -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='NotificationSel' AND type = 'U')
BEGIN
  DROP TABLE [NotificationSel]
END
------------------ SystemConfiguration -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='SystemConfiguration' AND type = 'U')
BEGIN
  DROP TABLE [SystemConfiguration]
END
------------------ ReportFullText -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='ReportFullText' AND type = 'U')
BEGIN
  DROP TABLE [ReportFullText]
END
------------------ DecisionLog -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='DecisionLog' AND type = 'U')
BEGIN
  DROP TABLE [DecisionLog]
END
go

/* Report
*/
CREATE TABLE [dbo].[Report] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Schema] varchar(50) null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ReportPK] PRIMARY KEY ([GUID])
)
/* Decision
*/
CREATE TABLE [dbo].[Decision] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DecisionPK] PRIMARY KEY ([GUID])
)
/* AccountEntry
*/
CREATE TABLE [dbo].[AccountEntry] (
      [GUID] varchar(50) not null
    , [ReportSpace] varchar(50) null
    , [ReportKey] varchar(100) null
    , [AuthToken] varchar(max) null
    , [Version] int null
    , [Content] varchar(max) null
    , [DataContent] varchar(max) null
    , [TStamp] timestamp not null
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [AccountEntryPK] PRIMARY KEY ([GUID])
)
/* Document
*/
CREATE TABLE [dbo].[Document] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [DocumentHandle] varchar(100) null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [DataContent] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DocumentPK] PRIMARY KEY ([GUID])
)
/* Checksum
*/
CREATE TABLE [dbo].[Checksum] (
      [GUID] varchar(50) not null
    , [ReportSpace] varchar(50) null
    , [ValueDate] varchar(100) null
    , [AuthToken] varchar(max) null
    , [Version] int null
    , [Content] varchar(max) null
    , [DataContent] varchar(max) null
    , [TStamp] timestamp not null
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ChecksumPK] PRIMARY KEY ([GUID])
)
/* ReportSpace
*/
CREATE TABLE [dbo].[ReportSpace] (
      [GUID] varchar(50) not null 
    , [Key] varchar(100) null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ReportSpacePK] PRIMARY KEY ([GUID])
)
/* ConfigDelQueue
*/
CREATE TABLE [dbo].[ConfigDelQueue] (
      [TStamp] timestamp not null 
    , [Class] varchar(50) not null 
    , [Key] varchar(100) not null 
    ,CONSTRAINT [ConfigDelQueuePK] PRIMARY KEY ([TStamp])
)
/* User
*/
CREATE TABLE [dbo].[User] (
      [GUID] varchar(50) not null 
    , [Key] varchar(100) not null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [UserPK] PRIMARY KEY ([GUID])
)
/* ReportDownload
*/
CREATE TABLE [dbo].[ReportDownload] (
      [GUID] varchar(50) not null 
    , [TStamp] timestamp not null 
    , [ReportSpace] varchar(50) not null 
    , [ReportKey] varchar(100) null 
    , [ReportGuid] varchar(50) not null 
    , [ReportDownloadType] varchar(50) not null default 'Update'
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ReportDownloadPK] PRIMARY KEY ([GUID])
)
/* DecisionDownload
*/
CREATE TABLE [dbo].[DecisionDownload] (
      [GUID] varchar(50) not null 
    , [TStamp] timestamp not null 
    , [ReportSpace] varchar(50) not null 
    , [ReportKey] varchar(100) null 
    , [DecisionGuid] varchar(50) not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DecisionDownloadPK] PRIMARY KEY ([GUID])
)
/* AccountEntryDownload
*/
CREATE TABLE [dbo].[AccountEntryDownload](
        [GUID] [varchar](50) NOT NULL,
        [TStamp] [timestamp] NOT NULL,
        [ReportSpace] [varchar](50) NOT NULL,
        [ReportKey] [varchar](100) NULL,
        [AccountEntryGuid] [varchar](50) NOT NULL,
        [CreatedDateTime] datetime not null default GETDATE(),
     CONSTRAINT [AccountEntryDownloadPK] PRIMARY KEY ([GUID])
)
/* DocumentAck
*/
CREATE TABLE [dbo].[DocumentAck] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [DocumentHandle] varchar(100) null 
    , [Acknowledged] varchar(5) not null default 'False'
    , [AcknowledgedComment] varchar(200) null 
    , [AuthToken] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DocumentAckPK] PRIMARY KEY ([GUID])
)
/* DocumentDownload
*/
CREATE TABLE [dbo].[DocumentDownload] (
      [GUID] varchar(50) not null 
    , [TStamp] timestamp not null 
    , [ReportSpace] varchar(50) not null 
    , [ReportKey] varchar(100) null 
    , [DocumentHandle] varchar(100) null 
    , [DocumentGuid] varchar(50) not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DocumentDownloadPK] PRIMARY KEY ([GUID])
)
/* ChecksumDownload
*/
CREATE TABLE [dbo].[ChecksumDownload] (
                  [GUID] varchar(50) not null
                , [TStamp] timestamp not null
                , [ReportSpace] varchar(50) not null
                , [ValueDate] varchar(100) null
                , [CreatedDateTime] datetime not null default GETDATE()
                ,CONSTRAINT [ChecksumDownloadPK] PRIMARY KEY ([GUID])
            )
/* AccessSet
*/
CREATE TABLE [dbo].[AccessSet] (
      [GUID] varchar(50) not null 
    , [Key] varchar(100) not null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [AccessSetPK] PRIMARY KEY ([GUID])
)
/* List
*/
CREATE TABLE [dbo].[List] (
      [GUID] varchar(50) not null 
    , [Key] varchar(100) not null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ListPK] PRIMARY KEY ([GUID])
)
/* ReportIndex
*/
CREATE TABLE [dbo].[ReportIndex] (
      [ReportSpace] varchar(50) not null 
    , [ReportKey] varchar(100) null 
    , [ReportGuid] varchar(50) not null 
    , [IndexType] varchar(45) null 
    , [IndexField] varchar(100) null 
    , [IndexValue] varchar(200) null 
    , [CreatedDateTime] datetime not null default GETDATE()
)
/* Notification
*/
CREATE TABLE [dbo].[Notification] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [QueueName] varchar(50) null 
    , [AuthToken] varchar(max) null 
    , [Content] varchar(max) null 
    , [Status] varchar(1) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [NotificationPK] PRIMARY KEY ([GUID])
)
/* NotificationSel
*/
CREATE TABLE [dbo].[NotificationSel] (
      [GUID] varchar(50) not null 
    ,CONSTRAINT [NotificationSelPK] PRIMARY KEY ([GUID])
)
/* SystemConfiguration
*/
CREATE TABLE [dbo].[SystemConfiguration] (
      [Name] varchar(100) not null 
    , [Area] varchar(100) null 
    , [Content] varchar(max) null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [SystemConfigurationPK] PRIMARY KEY ([Name])
)
/* ReportFullText
*/
CREATE TABLE [dbo].[ReportFullText] (
      [ReportGuid] varchar(50) not null 
    , [ReportSpace] varchar(50) not null 
    , [ReportKey] varchar(100) not null 
    , [FullText] varchar(max) null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [ReportFullTextPK] PRIMARY KEY ([ReportGuid])
)
/* DecisionLog
*/
CREATE TABLE [dbo].[DecisionLog] (
      [GUID] varchar(50) not null 
    , [ReportSpace] varchar(50) null 
    , [ReportKey] varchar(100) null 
    , [AuthToken] varchar(max) null 
    , [Version] int null 
    , [Content] varchar(max) null 
    , [TStamp] timestamp not null 
    , [CreatedDateTime] datetime not null default GETDATE()
    ,CONSTRAINT [DecisionLogPK] PRIMARY KEY ([GUID])
)

CREATE TABLE [dbo].[SystemConfigurationAudit](
	[GUID] [varchar](50) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Area] [varchar](100) NULL,
	[Content] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[Action] [varchar](200) NOT NULL,
 CONSTRAINT [SystemConfigurationAuditPK] PRIMARY KEY CLUSTERED
(
	[GUID] ASC, [Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT (newID()) FOR [GUID]
ALTER TABLE [dbo].[SystemConfigurationAudit] ADD  DEFAULT ('UPDATE') FOR [Action]
GO

-- Create Indexes
CREATE INDEX Idx_Reprt_ReportSpace ON Report ([ReportSpace])
CREATE NONCLUSTERED INDEX Idx_Report_ReportSpace_Key_TStamp ON Report ([ReportSpace]) INCLUDE ([ReportKey], [TStamp])
CREATE INDEX Idx_Reprt_ReportKey ON Report ([ReportKey])
CREATE INDEX Idx_Reprt_Schema ON Report ([Schema])
CREATE INDEX Idx_Reprt_TStamp ON Report ([TStamp])
CREATE INDEX Idx_Reprt_CreatedDT ON Report ([CreatedDateTime])
CREATE INDEX Idx_Decision_ReportSpace ON Decision ([ReportSpace])
CREATE INDEX Idx_Decision_ReportKey ON Decision ([ReportKey])
CREATE INDEX Idx_Decision_TStamp ON Decision ([TStamp])
CREATE INDEX Idx_Decision_CreatedDT ON Decision ([CreatedDateTime])

CREATE INDEX Idx_AccountEntry_ReportSpace ON AccountEntry ([ReportSpace])
CREATE INDEX Idx_AccountEntry_ReportKey ON AccountEntry ([ReportKey])
CREATE INDEX Idx_AccountEntry_TStamp ON AccountEntry ([TStamp])
CREATE INDEX Idx_AccountEntry_CreatedDT ON AccountEntry ([CreatedDateTime])

CREATE INDEX Idx_Checksum_ReportSpace ON dbo.Checksum ([ReportSpace])
CREATE INDEX Idx_Checksum_ValueDate ON dbo.Checksum ([ValueDate])
CREATE INDEX Idx_Checksum_TStamp ON dbo.Checksum ([TStamp])

CREATE INDEX Idx_Document_ReportSpace ON Document ([ReportSpace])
CREATE INDEX Idx_Document_ReportKey ON Document ([ReportKey])
CREATE INDEX Idx_Document_DocumentHandle ON Document ([DocumentHandle])
CREATE INDEX Idx_Document_TStamp ON Document ([TStamp])
CREATE INDEX Idx_Document_CreatedDT ON Document ([CreatedDateTime])
CREATE INDEX Idx_ReportDownload_TStamp ON ReportDownload ([TStamp])
CREATE INDEX Idx_ReportDownload_ReportSpace ON ReportDownload ([ReportSpace])
CREATE INDEX Idx_ReportDownload_ReportKey ON ReportDownload ([ReportKey])
CREATE INDEX Idx_DecisionDownload_TStamp ON DecisionDownload ([TStamp])
CREATE INDEX Idx_DecisionDownload_ReportSpace ON DecisionDownload ([ReportSpace])
CREATE INDEX Idx_DecisionDownload_ReportKey ON DecisionDownload ([ReportKey])

CREATE INDEX Idx_ChecksumDownload_ReportSpace ON dbo.ChecksumDownload ([ReportSpace])
CREATE INDEX Idx_ChecksumDownload_ValueDate ON dbo.ChecksumDownload ([ValueDate])
CREATE INDEX Idx_ChecksumDownload_TStamp ON dbo.ChecksumDownload ([TStamp])

CREATE INDEX Idx_AccountEntryDownload_TStamp ON AccountEntryDownload ([TStamp])
CREATE INDEX Idx_AccountEntryDownload_ReportSpace ON AccountEntryDownload ([ReportSpace])
CREATE INDEX Idx_AccountEntryDownload_ReportKey ON AccountEntryDownload ([ReportKey])

CREATE INDEX Idx_DocumentAck_ReportSpace ON DocumentAck ([ReportSpace])
CREATE INDEX Idx_DocumentAck_ReportKey ON DocumentAck ([ReportKey])
CREATE INDEX Idx_DocumentAck_DocumentHandle ON DocumentAck ([DocumentHandle])
CREATE INDEX Idx_DocumentAck_Acknowledged ON DocumentAck ([Acknowledged])
CREATE INDEX Idx_DocumentAck_TStamp ON DocumentAck ([TStamp])
CREATE INDEX Idx_DocumentAck_CreatedDT ON DocumentAck ([CreatedDateTime])
CREATE INDEX Idx_DocumentDownload_TStamp ON DocumentDownload ([TStamp])
CREATE INDEX Idx_DocumentDownload_ReportSpace ON DocumentDownload ([ReportSpace])
CREATE INDEX Idx_DocumentDownload_ReportKey ON DocumentDownload ([ReportKey])
CREATE INDEX Idx_DocumentDownload_DocumentHandle ON DocumentDownload ([DocumentHandle])
CREATE INDEX Idx_ReportIndex_ReportSpace ON ReportIndex ([ReportSpace])
CREATE INDEX Idx_ReportIndex_ReportKey ON ReportIndex ([ReportKey])
CREATE INDEX Idx_ReportIndex_ReportGuid ON ReportIndex ([ReportGuid])
CREATE INDEX Idx_ReportIndex_IndexPrimary ON ReportIndex ([IndexType],[IndexField],[IndexValue])
CREATE INDEX Idx_ReportIndex_IndexFields ON ReportIndex ([IndexType],[IndexField])
CREATE INDEX Idx_ReportIndex_IndexField ON ReportIndex ([IndexField])
CREATE INDEX Idx_ReportIndex_IndexValue ON ReportIndex ([IndexValue])
CREATE INDEX Idx_ReportIndex_IndexFieldValue ON ReportIndex ([IndexField],[IndexValue])
CREATE INDEX Idx_ReportIndex_CreatedDT ON ReportIndex ([CreatedDateTime])
CREATE INDEX Idx_Notif_ReportSpace ON Notification ([ReportSpace])
CREATE INDEX Idx_Notif_ReportKey ON Notification ([ReportKey])
CREATE INDEX Idx_Notif_TStamp ON Notification ([TStamp])
CREATE INDEX Idx_Notif_CreatedDT ON Notification ([CreatedDateTime])
CREATE UNIQUE INDEX NDX_FullTextTrnRef ON ReportFullText ([ReportSpace],[ReportKey])

IF (FULLTEXTSERVICEPROPERTY('IsFullTextInstalled') = 1)
BEGIN
    PRINT('CREATING FULL-TEXT INDEX')
    EXEC ('CREATE FULLTEXT INDEX ON ReportFullText( [FullText] Language 2057) KEY INDEX [ReportFullTextPK] ON ReportServiceFTCat')
END

CREATE INDEX NDX_FullTextCreatedDT ON ReportFullText ([CreatedDateTime])
CREATE INDEX Idx_Decision_ReportSpace ON DecisionLog ([ReportSpace])
CREATE INDEX Idx_Decision_ReportKey ON DecisionLog ([ReportKey])
CREATE INDEX Idx_Decision_TStamp ON DecisionLog ([TStamp])


-- Create Foreign Key constraints
go