USE [master]
GO
PRINT 'CREATE DB [ReportServices]'
CREATE DATABASE [ReportServices]
CONTAINMENT = NONE
GO

/* MSSQL Versions : https://support.microsoft.com/en-za/help/321185/how-to-determine-the-version-edition-and-update-level-of-sql-server-an */
PRINT 'SETTING COMPATIBILITY LEVEL TO 130  ::  SQL Server 2008 (100), SQL Server 2012 (110), SQL Server 2014 (120), SQL Server 2016 (130), and SQL Server 2017 (140).'
ALTER DATABASE [ReportServices] SET COMPATIBILITY_LEVEL = 140 /* MSSQL  */
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
  PRINT 'ENABLING FULL TEXT SEARCH'
  EXEC [ReportServices].[dbo].[sp_fulltext_database] @action = 'enable'
end
else
begin
  PRINT '***  FULLTEXT SEARCH IS NOT INSTALLED OR ENABLED!  ***'
end
GO



