------------------ OR_Branch -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_Branch' AND type = 'U')
BEGIN
DROP TABLE OR_Branch
END
GO
------------------ OR_LoanReference -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_LoanReference' AND type = 'U')
BEGIN
DROP TABLE OR_LoanReference
END
GO
------------------ OR_CustomsClientNumber -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_CustomsClientNumber' AND type = 'U')
BEGIN
DROP TABLE OR_CustomsClientNumber
END
GO
------------------ OR_IHQ -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_IHQ' AND type = 'U')
  BEGIN
    DROP TABLE OR_IHQ
  END
GO
------------------ OR_HOLDCO -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_HOLDCO' AND type = 'U')
  BEGIN
    DROP TABLE OR_HOLDCO
  END
GO
------------------ OR_MTAAccount -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_MTAAccount' AND type = 'U')
  BEGIN
    DROP TABLE OR_MTAAccount
  END
GO
------------------ OR_CategoryRulingSection -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_CategoryRulingSection' AND type = 'U')
  BEGIN
    DROP TABLE OR_CategoryRulingSection
  END
GO
------------------ OR_RulingsSection -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE name='OR_RulingsSection' AND type = 'U')
  BEGIN
    DROP TABLE OR_RulingsSection
  END
GO

/* OR_Branch
*/
CREATE TABLE OR_Branch (
  [BranchId] int IDENTITY(1,1)
  , [BranchCode] varchar(10) not null
  , [BranchName] varchar(50) null
  , [ValidFrom] datetime null
  , [ValidTo] datetime null
  , [HubCode] varchar(10) null
  , [HubName] varchar(50) null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [AuthDate] datetime null
  , [AuthBy] int null
  ,CONSTRAINT OR_BranchPK PRIMARY KEY ([BranchId])
)
GO

/* OR_LoanReference
    Status: P - Pending Authorisation
C - Confirmed by SARB
A - Authorised
*/
CREATE TABLE OR_LoanReference (
  [LoanRefID] int IDENTITY(1,1)
  , [LoanRefNumber] varchar(70) not null
  , [DateReceived] datetime null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [AuthDate] datetime null
  , [AuthBy] int null
  , [Status] varchar(1) not null
  , [CountryCode] varchar(2) not null default 'ZA'
  , [ValidFrom] datetime null
  , [ValidTo] datetime null
  ,CONSTRAINT OR_LoanReferencePK PRIMARY KEY ([LoanRefID])
)
GO

/* OR_CustomsClientNumber
    Status: P - Pending Authorisation
C - Confirmed by SARB
A - Authorised
*/
CREATE TABLE OR_CustomsClientNumber (
  [CCNId] int IDENTITY(1,1)
  , [CCN] varchar(15) not null
  , [ParentCCN] varchar(15) null
  , [AccountName] varchar(70) null
  , [AccountNumber] varchar(40) null
  , [LetterOfUndertaking] varchar(1) null
  , [ValidFrom] datetime null
  , [ValidTo] datetime null
  , [AuthDate] datetime null
  , [AuthBy] int null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [Status] varchar(1) not null
  ,CONSTRAINT OR_CustomsClientNumberPK PRIMARY KEY ([CCNId])
)
GO

/* OR_IHQ
*/
CREATE TABLE OR_IHQ (
  [IHQId] int IDENTITY(1,1)
  , [Code] varchar(10) not null
  , [CompanyName] varchar(70) not null
  , [RegistrationNumber] varchar(30) not null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [AuthDate] datetime null
  , [AuthBy] int null
  ,CONSTRAINT OR_IHQPK PRIMARY KEY ([IHQId])
)
GO

/* OR_HOLDCO
*/
CREATE TABLE OR_HOLDCO (
    [HoldcoId] int IDENTITY(1,1)
  , [AccountNumber] varchar(40) not null
  , [AccountName] varchar(70) not null
  , [AccountTypeID] smallint null
  , [RegistrationNumber] varchar(30) null
  , [REInternalAuthNumber] varchar(30) null
  , [RegulatoryAuthReference] varchar(15) null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [AuthDate] datetime null
  , [AuthBy] int null
  ,CONSTRAINT OR_HOLDCOPK PRIMARY KEY ([HoldcoId])
)
GO

/* OR_MTAAccount
*/
CREATE TABLE OR_MTAAccount (
    [MTAAccountId] int IDENTITY(1,1)
  , [AccountNumber] varchar(40) not null
  , [MoneyTransferAgentID] varchar(35) not null
  , [RulingSection] int not null
  , [ADLAInd] varchar(1) not null
  , [ADLALevel] varchar(1) null
  , [DateAdded] datetime null
  , [AddedBy] int null
  , [AuthDate] datetime null
  , [AuthBy] int null
  ,CONSTRAINT OR_MTAAccountPK PRIMARY KEY ([MTAAccountId],[RulingSection])
)
GO

/* OR_RulingsSection
*/
CREATE TABLE OR_RulingsSection (
    [RulingsSectionID] int IDENTITY(1,1)
  , [RulingsSection] varchar(200) not null
  , [Description] varchar(200) null
  , [DateAdded] datetime null
  , [AddedBy] int null
  ,CONSTRAINT OR_RulingsSectionPK PRIMARY KEY ([RulingsSectionID])
)
GO

/* OR_CategoryRulingSection
*/
CREATE TABLE OR_CategoryRulingSection (
    [CategoryID] int not null
  , [RulingsSectionID] int not null
  , [DateAdded] datetime null
  , [AddedBy] int null
  ,CONSTRAINT OR_CategoryRulingSectionPK PRIMARY KEY ([CategoryID],[RulingsSectionID])
)
GO

CREATE INDEX Fk_OR_LoanReference_OR_User1_idx ON OR_LoanReference ([AuthBy])
CREATE INDEX Fk_OR_LoanReference_OR_User2_idx ON OR_LoanReference ([AddedBy])
CREATE INDEX Fk_OR_LoanReference_OR_Country1_idx ON OR_LoanReference ([CountryCode])
CREATE UNIQUE INDEX LoanRefNumber_UNIQUE ON OR_LoanReference ([LoanRefNumber])
CREATE INDEX IDX_CustomsClientNumber_CCN ON OR_CustomsClientNumber ([CCN])
CREATE INDEX FK_OR_HOLDCO_OR_AccountType1_idx ON OR_HOLDCO ([AccountTypeID])
CREATE INDEX Fk_OR_MTAAccount_OR_MoneyTransferAgent1_idx ON OR_MTAAccount ([MoneyTransferAgentID])
CREATE INDEX Fk_OR_MTAAccount_OR_RulingsSection1_idx ON OR_MTAAccount ([RulingSection])
CREATE INDEX Fk_OR_CategoryRulingSection_OR_RulingsSection1_idx ON OR_CategoryRulingSection ([RulingsSectionID])

ALTER TABLE OR_HOLDCO ADD CONSTRAINT FKHOLDCO_AccountTypeID__AccountType FOREIGN KEY ([AccountTypeID]) REFERENCES OR_AccountType ([AccountTypeID])
ALTER TABLE OR_MTAAccount ADD CONSTRAINT FKMTAAccount_MoneyTransferAgentID__MoneyTransferAgent FOREIGN KEY ([MoneyTransferAgentID]) REFERENCES OR_MoneyTransferAgent ([MoneyTransferAgentID])
ALTER TABLE OR_MTAAccount ADD CONSTRAINT FKMTAAccount_RulingSection__RulingsSection FOREIGN KEY ([RulingSection]) REFERENCES OR_RulingsSection ([RulingsSectionID])
ALTER TABLE OR_CategoryRulingSection ADD CONSTRAINT FKCategoryRulingSection_CategoryID__Category FOREIGN KEY ([CategoryID]) REFERENCES OR_Category ([CategoryID])
ALTER TABLE OR_CategoryRulingSection ADD CONSTRAINT FKCategoryRulingSection_RulingsSectionID__RulingsSection FOREIGN KEY ([RulingsSectionID]) REFERENCES OR_RulingsSection ([RulingsSectionID])
GO


INSERT INTO OR_Branch ([BranchCode],[BranchName],[ValidFrom],[ValidTo])
VALUES ('99030100','SANDTON','1980-01-01','2999-01-01')
GO


grant select on [OR_MTAAccount] to SSRSUser
go
grant select on [OR_HOLDCO] to SSRSUser
go
grant select on [OR_IHQ] to SSRSUser
go
grant select on [OR_CustomsClientNumber] to SSRSUser
go
grant select on [OR_LoanReference] to SSRSUser
go
grant select on [OR_Category] to SSRSUser
go
grant select on [OR_Country] to SSRSUser
go
grant select on [OR_Currency] to SSRSUser
go
grant select on [OR_AdhocSubject] to SSRSUser
go
grant select on [OR_CategoryCode] to SSRSUser
go
grant select on [OR_CustomsOffice] to SSRSUser
go
grant select on [OR_Branch] to SSRSUser
go
grant select on [OR_MoneyTransferAgent] to SSRSUser
go
grant select on [OR_RulingsSection] to SSRSUser
go
grant select on [OR_CategoryRulingSection] to SSRSUser
go