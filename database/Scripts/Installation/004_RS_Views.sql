USE [ReportServices]

IF OBJECT_ID (N'dbo.ReportView', N'V') IS NULL
BEGIN
    PRINT '...CREATE VIEW ReportView.'
    EXEC ('CREATE VIEW dbo.ReportView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW ReportView.'
GO
ALTER VIEW dbo.ReportView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Version], curr.[Schema], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [Report] curr
	  LEFT JOIN [Report] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go




IF OBJECT_ID (N'dbo.DecisionView', N'V') IS NULL
BEGIN
  PRINT '...CREATE VIEW DecisionView.'
  EXEC ('CREATE VIEW dbo.DecisionView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW DecisionView.'
GO
ALTER VIEW dbo.DecisionView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Version], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [Decision] curr
	  LEFT JOIN [Decision] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go




IF OBJECT_ID (N'dbo.AccountEntryView', N'V') IS NULL
BEGIN
  PRINT '...CREATE VIEW AccountEntryView.'
  EXEC ('CREATE VIEW dbo.AccountEntryView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW AccountEntryView.'
GO
ALTER VIEW dbo.AccountEntryView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[AuthToken], curr.[Version], curr.[Content], curr.[DataContent], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [AccountEntry] curr
	  LEFT JOIN [AccountEntry] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go



IF OBJECT_ID (N'dbo.ChecksumView', N'V') IS NULL
BEGIN
  PRINT '...CREATE VIEW ChecksumView.'
  EXEC ('CREATE VIEW dbo.ChecksumView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW ChecksumView.'
GO
ALTER VIEW dbo.ChecksumView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ValueDate], curr.[AuthToken], curr.[Version], curr.[Content], curr.[DataContent], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [Checksum] curr
	  LEFT JOIN [Checksum] newer
	  ON curr.[ReportSpace] = newer.[ReportSpace] AND curr.[ValueDate] = newer.[ValueDate] AND curr.[TStamp] < newer.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go



IF OBJECT_ID (N'dbo.DocumentView', N'V') IS NULL
BEGIN
  PRINT '...CREATE VIEW DocumentView.'
  EXEC ('CREATE VIEW dbo.DocumentView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW DocumentView.'
GO
ALTER VIEW dbo.DocumentView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[DocumentHandle], curr.[AuthToken], curr.[Version], curr.[Content], curr.[DataContent], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [Document] curr
	  LEFT JOIN [Document] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[DocumentHandle] = curr.[DocumentHandle] AND newer.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go




IF OBJECT_ID (N'dbo.DocumentAckView', N'V') IS NULL
BEGIN
  PRINT '...CREATE VIEW DocumentAckView.'
  EXEC ('CREATE VIEW dbo.DocumentAckView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW DocumentAckView.'
GO
ALTER VIEW dbo.DocumentAckView AS
	SELECT curr.[GUID], curr.[ReportSpace], curr.[ReportKey], curr.[DocumentHandle], curr.[Acknowledged], curr.[AcknowledgedComment], curr.[AuthToken], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [DocumentAck] curr
	  LEFT JOIN [DocumentAck] newer ON newer.[ReportSpace] = curr.[ReportSpace] AND newer.[ReportKey] = curr.[ReportKey] AND newer.[DocumentHandle] = curr.[DocumentHandle] AND newer.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL
go





IF OBJECT_ID (N'dbo.UserView', N'V') IS NULL
BEGIN
    PRINT '...CREATE VIEW UserView.'
    EXEC ('CREATE VIEW dbo.UserView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW UserView.'
GO
ALTER VIEW dbo.UserView AS
	SELECT curr.[GUID], curr.[Key], curr.[AuthToken], curr.[Version], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [User] curr
	  LEFT JOIN [User] newer ON newer.[Key] = curr.[Key] AND newer.[TStamp] > curr.[TStamp]
	  LEFT JOIN [ConfigDelQueue] del ON del.[Class] = 'User' AND del.[Key] = curr.[Key] AND del.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL AND del.[TStamp] IS NULL
go





IF OBJECT_ID (N'dbo.AccessSetView', N'V') IS NULL
BEGIN
    PRINT '...CREATE VIEW AccessSetView.'
    EXEC ('CREATE VIEW dbo.AccessSetView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW AccessSetView.'
GO
ALTER VIEW dbo.AccessSetView AS
	SELECT curr.[GUID], curr.[Key], curr.[AuthToken], curr.[Version], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [AccessSet] curr
	  LEFT JOIN [AccessSet] newer ON newer.[Key] = curr.[Key] AND newer.[TStamp] > curr.[TStamp]
	  LEFT JOIN [ConfigDelQueue] del ON del.[Class] = 'AccessSet' AND del.[Key] = curr.[Key] AND del.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL AND del.[TStamp] IS NULL
go





IF OBJECT_ID (N'dbo.ListView', N'V') IS NULL
BEGIN
    PRINT '...CREATE VIEW ListView.'
    EXEC ('CREATE VIEW dbo.ListView AS SELECT 1 as Dummy')
END
GO

PRINT '...ALTER VIEW ListView.'
GO
ALTER VIEW dbo.ListView AS
	SELECT curr.[GUID], curr.[Key], curr.[AuthToken], curr.[Version], curr.[Content], curr.[TStamp], curr.[CreatedDateTime]
	  FROM [List] curr
	  LEFT JOIN [List] newer ON newer.[Key] = curr.[Key] AND newer.[TStamp] > curr.[TStamp]
	  LEFT JOIN [ConfigDelQueue] del ON del.[Class] = 'List' AND del.[Key] = curr.[Key] AND del.[TStamp] > curr.[TStamp]
	 WHERE newer.[TStamp] IS NULL AND del.[TStamp] IS NULL
go
