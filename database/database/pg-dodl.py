
class DBFieldsDO:
    _schema = 'DF_DBField'

class DBTableDO:
    _schema = 'DF_DBTable'
    DBFields = InternalListOf(DBFieldsDO)
    
class DBTableListDO:
    _schema = None
    DBTables = InternalListOf(DBTableDO)

class PageFieldDO:
    _schema = 'DF_Field'
    
class PageDO:
    _schema = 'DF_Page'
    Fields = InternalListOf(PageFieldDO,'PageID')

class SmartInputDO:
    _schema = 'DS_SmartInput'

class SmartAccountDO:
    _schema = 'DS_SmartAccount'
    Inputs = InternalListOf(SmartInputDO)

class PopSubLevelDO:
    _schema = 'DS_PopSubLevel'

class PopLevelDO:
    _schema = 'DS_PopLevel'
    SubLevels = InternalListOf(PopSubLevelDO)
    
class PopDO:
    _schema = 'DS_Pop'
    Levels = InternalListOf(PopLevelDO)
    #RunPop = MethodCall()
    
class TriggerDO:    
    _schema = 'DS_Trigger'    
    
class DealPeriodDO:
    _hooks = ['Save']
    _schema = 'DS_DealPeriod'
    PopList = ExternalListOf(PopDO)
    Triggers = ExternalListOf(TriggerDO)
    
class TriggerListByPeriodDO:
    _schema = 'DS_DealPeriod'
    Triggers = InternalListOf(TriggerDO)
    Recalculate = Method()
    
#class DealPageLinkDO:
#    _schema = 'DS_DealPages'
#    Pops = ExternalListOf(PageDO)
    
class DealDO:
    _schema = 'DS_Deal'
    #_hooks = ['Save']
    #Scripts = InternalListOf(ScriptDO,link_table_name='DS_DealScriptLink')
    Pages = ExternalListOf(PageDO,link_table_name='DS_DealPageLink')
    Periods = ExternalListOf(DealPeriodDO)
    SmartAccounts = ExternalListOf(SmartAccountDO)

class IssuerDO:
    _schema = 'DS_Issuer'
    Deals = ExternalListOf(DealDO)

class IssuerListDO:
    _schema = None
    Issuers = ExternalListOf(IssuerDO)

class DatedValueDO:
    _schema = None
    Date = Field('datetime')
    Value = Field('text')
    InError = Field('bit')
    ErrorMessage = Field('text')
        
class NamedCalculationExtensionDO:
    _schema = 'DS_NamedCalculationExtension'
    Values = TransientListOf(DatedValueDO)
    
class NamedCalculationDO:
    _schema = 'DS_NamedCalculation'
    NamedCalculationExtensions = InternalListOf(NamedCalculationExtensionDO)
    Values = TransientListOf(DatedValueDO)
    
class NamedCalculationListDO:
    _schema = None
    NamedCalculations = InternalListOf(NamedCalculationDO,filter='DealID is null')
    Recalculate = Method()
    
class NamedCalculationListByDealDO:
    _schema = 'DS_Deal'
    NamedCalculations = InternalListOf(NamedCalculationDO)
    Recalculate = Method()

   
    
