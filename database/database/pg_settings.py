table_flags = [
 #('#.*',{'IgnoreSyntax':True}),
 #('ds.*', {'OracleSyntax':True} ),
]

dodl_files = []#r'pg-dodl.py']
# The assembly name and namespace which will eventually contain the the db classes. Used by the hibernate xbm file to find the db classes
#
db_class_assembly = 'PGData'
db_class_namespace = 'Gen.DB'

# The prefix which will automatically be applied to any generated db classes.
#
db_class_prefix = '' #'DB'

# DBTables for DataFacet
#
df_dbtables = [
 'dspd_pm1',
 'dspd_pm1extra',
 'dspd_pm1delinquency',
 'dspd_pm1delinquencyold',
 'dspd_pm1jumboloanpivot',
 'dspd_pm1delinquencypivot',
]


# Schema files which define how the DB looks
#
import_schema_file = [
# r'example_db.pdm',
# r'example_db.mwb',
# r'example1_db.mwb',
# r'example2_db.mwb',
 r'../ReportServiceCentral.mwb',
 ]

 # List of output files and 
 
entities_output_dir = '../generated/'
db_output_dir = './'

global_template_list = [
#    (entities_output_dir ,'Base.cs','surefire_base.cs'),
#    (output_dir ,'dodl.clientside.cs','dodl.clientside.ch.cs'),
#    (output_dir ,'dodl.def.cs','dodl.def.ch.cs'),
#    (output_dir ,'dodl.methodhandler.cs','dodl.methodhandler.ch.cs'),
#    (output_dir ,'data_dictionary.html','data_dictionary.ch.html'),
#    (output_dir ,'hibernate.hbm.xml','hibernate.ch.xml'),
#    (output_dir ,'hibernate.cs','global_hibernate.ch.cs'),
#    (output_dir ,'test.out','test.ch'),
    (db_output_dir ,'RS_CreateTables_MSSQL.sql','mssql.ch.sql'),
]
table_template_list = [
#    (entities_output_dir, '$($table.options.get("ClassName", $table.name)).cs','surefire.ch.cs'),
]
index_tablespace_name = 'IPH_BANKSERV_INDEX_4M'
data_tablespace_name = 'IPH_BANKSERV_DATA_4M'
