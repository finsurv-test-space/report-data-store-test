#!/bin/bash

echo "============================================"
echo "GENERAL RDS / RSUI / RULES MANAGER BUILD..."
echo "============================================"

curDate=$(date +"%Y%m%d")



##--------------------------------------------------------------------------------------------------------------------##
##----  This function will checkout the specified GIT repo.                                                       ----##
function checkoutRepo(){
  sourceDir=$1
  repoName=$2
  repoUrl=$3
  if [ ! -d "$sourceDir/$repoName" ]
  then
    echo "Clone $repoName repo..."
    git clone "$repoUrl"
    if [ $? -eq 0 ]; then
      echo "Git Clone successful."
    else
      exit 1;
    fi
  fi
}


##--------------------------------------------------------------------------------------------------------------------##
##----  This function will make use of the bump_version script to increment the build version tag.                ----##
function incrementBuildVersion(){
	echo "...Incrementing build version..."
	previousVersionStr=$(grep -Po '(?<=\"[Vv][Ee][Rr][Ss][Ii][Oo][Nn]\":\")([^"]*)' ../src/main/resources/config/version.json)
	./bump_version.sh build
	currentVersionStr=$(grep -Po '(?<=\"[Vv][Ee][Rr][Ss][Ii][Oo][Nn]\":\")([^"]*)' ../src/main/resources/config/version.json)
	echo "latest tag to use: $currentVersionStr"
}


##--------------------------------------------------------------------------------------------------------------------##
##----  This function will attempt to tag the current repo with the current build version.                        ----##
function tagVersion(){
  repoTag="$versionPrefixStr$currentVersionStr"
  #repoTag="$versionPrefixStr$curDate_v$currentVersionStr"
	echo "Adding new version tag: $repoTag..."
	git tag -a "$repoTag" -m "[BUILD][RDS] $repoTag"
	current_branch=$(git symbolic-ref --short HEAD)
	echo "Pushing branch $current_branch and tag $repoTag upstream..."
	git push origin "$repoTag" --tags
}

##--------------------------------------------------------------------------------------------------------------------##
##----  This function will attempt to pull the commit logs for all commits since the previous build version tag.  ----##
##----  if no previous build version tags are found, it will pull the commits logs for the last month.            ----##
function getDocumentation(){
	repoName=$1
	docsFile="$destinationFolder/CHANGELOG.TXT"
	#NB: for details around formatting the log lines see the "PRETTY FORMATS" section in the git-log documentation (file:///C:/Program%20Files/Git/mingw64/share/doc/git-doc/git-log.html).
	#gitTags=$(git describe --tags --abbrev=0)
	#gitTags="$versionPrefixStr$previousVersionStr"
	gitTags=$(git tag --list -i --sort=taggerdate | grep "^$versionPrefixStr.*" | tail -n 1)
	logOutput=""
	fileOutput=""
	# --- IF THE COMMIT MESSAGES ARE PREFIXED WITH SPECIFIC TAGS, THEN YOU CAN REMOVE/ALLOW ONLY CERTAIN TAGS TO BE OUTPUT TO THE CHANGELOG --- #
	#git log $(git describe --tags --abbrev=0)..HEAD --pretty=format:"%s" -i -E --grep="^(\[INTERNAL\]|\[FEATURE\]|\[FIX\]|\[DOC\])*\[FEATURE\]"
	echo "  *****  GIT TAG :: $gitTags  *****  "
	echo "$repoName:" >> $docsFile
	echo "------------------------------" >> $docsFile
	if [[ "$gitTags" != "" ]]; then
		echo "< PreviousTag: $gitTags >" >> $docsFile
		echo "------------------------------"
		echo " $repoName / Delta ($gitTags) -> HEAD "
		echo "------------------------------"
		logOutput=$(git log $gitTags..HEAD --pretty=format:"%ai %an <%ae>: %s")
		echo "Changelog:"
		echo "$logOutput"
		echo ""
		fileOutput=$(git diff --name-status $gitTags HEAD)
		echo "Affected Files:"
		echo "$fileOutput"
		echo ""
		echo "------------------------------"
	else 
		echo "< No previous tag detected >" >> $docsFile
		logOutput=$(git log --pretty=format:"%ai %an <%ae>: %s" --since=1.months | head -n 15)
	fi
	if [[ $logOutput ]]; then
		echo "$logOutput" >> $docsFile
	else 
		echo "...no changes to document." >> $docsFile
	fi
	if [[ $fileOutput ]]; then
		echo "Affected Files:" >> $docsFile
		echo "$fileOutput" >> $docsFile
	fi
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
}


##--------------------------------------------------------------------------------------------------------------------##
##----  This function will add some header information to the release notes/build documentation.                  ----##
function primeDocumentation(){
	buildInfo=$1
	docsFile="$destinationFolder/CHANGELOG.TXT"
	echo " " >> $docsFile
	echo "========================================" >> $docsFile
	echo " $buildInfo" >> $docsFile
	echo "========================================" >> $docsFile
	echo "----------------------------------------" >> $docsFile
	echo " < Change log file: $docsFile >" >> $docsFile
	echo "----------------------------------------" >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
}


##--------------------------------------------------------------------------------------------------------------------##
##----  This function will close-off the release notes/build documentation.                                       ----##
function closeDocumentation(){
	docsFile="$destinationFolder/CHANGELOG.TXT"
	consolodatedDocsFile="$rootFolder/dist/CONSOLODATED_CHANGELOG.TXT"
	curDate=$(date +"%Y-%m-%d %T")
	#git log $(git describe --tags --abbrev=0)..HEAD --pretty=format:"%s" -i -E --grep="^(\[INTERNAL\]|\[FEATURE\]|\[FIX\]|\[DOC\])*\[FEATURE\]"
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo "########################################################################################################################" >> $docsFile
	echo " " >> $docsFile
	echo " BUILD ($versionPrefixStr$currentVersionStr  @  $curDate) COMPLETE. " >> $docsFile
	echo " < Change log file: $docsFile >" >> $docsFile
	echo " " >> $docsFile
	echo "########################################################################################################################" >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
	cat "$docsFile" >> "$consolodatedDocsFile"
}

	
awsHostName="ec2-34-243-246-75.eu-west-1.compute.amazonaws.com"
awsPort="81"
awsTestApi="/report-data-store/test/evaluation/correctness"
testApiUrl="http://$awsHostName:$awsPort$awsTestApi"
deploymentArtefactBaseUrl="http://$awsHostName:$awsPort/static/"
swaggerBaseUrl="http://$awsHostName:$awsPort/report-data-store/swagger-ui.html"
rsuiBaseUrl="http://$awsHostName:$awsPort/rsui/login"

workingDir=$(pwd)
awsDir="$workingDir/AWS/RDS_eu-central-1"
versionPrefixStr="ReportServices_v"
currentVersionStr=""
previousVersionStr=""
incrementBuildVersion
#echo "...Working dir (return point): $workingDir"



#-----  SCRIPT USAGE  -----#
[ $# -eq 0 ] && { 
	echo "Params:"; 
	echo "-------"; 
	echo ""; 
	echo "--All                    \tBuild ALL components (rules, rules manager, tXstream, RDS etc)"; 
	echo "--tXstream:[Y/N]         \tBuild tXstream (Yes / No)."; 
	echo "                         \tWill force a build of rules, java rules engine and rules manager."; 
	echo "--Dashboard:[Y/N]        \tBuild Finsurv Dashboard (Yes / No)"; 
	echo "--RulesManager:[Y/N]     \tBuild RulesManager (Yes / No). Will force a build of rules and java rules engine."; 
	echo "--JavaRulesEngine:[Y/N]  \tBuild Java Rules Engine (Yes / No)"; 
	echo "--ReportDataStore:[Y/N]  \tBuild Report Data Store (Yes / No). Will force a build of rules, java rules engine and rules manager."; 
	echo "--DeployToAWS:[Y/N]      \tDeploy build artefacts to AWS ec2 instance (Yes / No). Will force a build of RDS."; 
	echo "--TestAWS:[Y/N]          \tTest the deployed AWS ec2 instance (Yes / No). Will force a build of RDS and deployment to AWS."; 
	echo "--UpdatePOC:[Y/N]        \tUpdate POC code with the latest deployed artefacts from RDS (Yes / No). Will force a build of RDS and deployment to AWS."; 
	echo ""; 
	echo ""; 
	echo ""; 
	echo "Usage: $0 [root-build-dir] [optional:dest-deployment-dir]"; 
	echo "Example: $0 /d/RDS_Build/"; 
	exit 1; 
}


rootFolder="$1"
echo "...Root build directory: $rootFolder"
#remove trailing "/" from root path...
re="^(.+)\/$"
if [[ $rootFolder =~ $re ]]; then
  echo "...remove trailing slash '/'..."
  rootFolder="${BASH_REMATCH[1]}";
fi
rootFolder="$rootFolder"
sourceFolder="$rootFolder/src"
destinationFolder="$2"
destinationBaseFolder="$destinationFolder"
destinationFolderName=""




#-----  SOURCE FOLDER  -----#
if [ -d "$sourceFolder" ]
then
  echo ""
  echo ""
  echo "*********"
  echo "* NOTE: *"
  echo "*********"
  echo -n "Source Folder already exists - perform *CLEAN* build (SSD Recommended) [Y/N]: "
  read cleanSrc
  if [[ $cleanSrc == "Y" ]]; then
	echo "Cleaning out source folder..."
    rm -rf "$sourceFolder"
	echo "...Clean complete."
	mkdir -p "$sourceFolder"
  else 
	echo "...'$cleanSrc' selected - *DIRTY* build commencing..."
  fi
  [ ! -d "$sourceFolder" ] && { exit 1; }
else 
  echo "Error: Source folder ($sourceFolder) not found"
  echo -n "Enter the path to create the projects source folder (current: '$sourceFolder'): "
  read srcFolder
  if [[ $srcFolder != "" ]]; then
    sourceFolder="$srcFolder"
  fi
  mkdir -p "$sourceFolder"
  [ ! -d "$sourceFolder" ] && { exit 1; }
fi
echo "...Project source folder: $sourceFolder"
cd "$sourceFolder"




#-----  DESTINATION FOLDER  -----#
if [[ $destinationFolder =~ $re ]]; then
  echo "...remove trailing slash '/' from destination folder..."
  destinationFolder="${BASH_REMATCH[1]}";
fi
if [ -d "$destinationFolder" ]
then
  echo "...Destination ($destinationFolder) exists - clearing..."
  rm -f "$destinationFolder/*"
  echo "...$destinationFolder cleared."
else
  #echo "Error: Destination ($destinationFolder) not found"
	echo "current version: $currentVersionStr"
  if [[ "$destinationFolder" == "" ]]
  then
    #dest="$rootFolder/dist/RDS_"
    #versionStr="_v$currentVersionStr"
    #destinationFolder="$dest$curDate$versionStr"
    destinationBaseFolder="$rootFolder/dist"
    versionStr="v$currentVersionStr"
    destinationFolderName="$curDate$versionStr"
    destinationFolder="$destinationBaseFolder/$destinationFolderName"
    echo "...defaulting destination folder to: $destinationFolder"
  fi
	
  echo ""
  echo ""
  echo "*********"
  echo "* NOTE: *"
  echo "*********"
  echo -n "Enter the destination path for the deployment artifacts (currently: '$destinationFolder'): "
  read destFolder
  if [[ $destFolder != "" ]]; then
    destinationFolder="$destFolder"
  fi

  mkdir -p "$destinationFolder"
  if [ ! -d "$destinationFolder" ]
  then
    exit 1;
  fi
fi
echo "...WAR files destination folder: $destinationFolder"





primeDocumentation "RDS Build ($currentVersionStr) -- $(date +"%Y%m%d")"

echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#***************************#
#****  Finsurv tXstream ****#
#checkoutRepo $sourceFolder "tXstream" "git@bitbucket.org:synthesis_admin/txstream.git"
#if [ -d "$sourceFolder/tXstream" ]
#then
#    echo "Building : tXstream..."
#    cd tXstream
#    git pull
#    gradle clean build war
#fi
#cd "$workingDir"
#cd "$sourceFolder"

#****************************#
#****  Finsurv Dashboard ****#
#checkoutRepo $sourceFolder "finsurv-dashboard" "git@bitbucket.org:synthesis_admin/finsurv-dashboard.git"
#if [ -d "$sourceFolder/finsurv-dashboard" ]
#then
#    echo "Building : finsurv-dashboard..."
#    cd finsurv-dashboard
#    git pull
#    # THIS NEEDS TO BE A GRADLE 3.X VERSION - JETTY PLUGIN IS DEPRECATED IN GRADLE 4 AND UP
#    # /d/Gradle/gradle-3.5/bin/gradle clean build
#    gradle clean build
#fi
#cd "$workingDir"
#cd "$sourceFolder"

#************************#
#****  Finsurv Rules ****#
if [ -d "$sourceFolder/finsurv-rules" ]
then
	echo "Deleting : finsurv-rules..."
	rm -rf "$sourceFolder/finsurv-rules"
fi
checkoutRepo $sourceFolder "finsurv-rules" "git@bitbucket.org:synthesis_admin/finsurv-rules.git"
if [ -d "$sourceFolder/finsurv-rules" ]
then
  echo "Building : finsurv-rules..."
  cd finsurv-rules
  #git checkout cleanup
  git checkout master
  git pull
  #npm install
  #npm run build
	getDocumentation "finsurv-rules"
	tagVersion
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#********************#
#****  BopServer ****#
if [ -d "$sourceFolder/bopserver" ]
then
	echo "Deleting : bopserver..."
	rm -rf "$sourceFolder/bopserver"
fi
checkoutRepo $sourceFolder "bopserver" "git@bitbucket.org:synthesis_admin/bopserver.git"
if [ -d "$sourceFolder/bopserver" ]
then
  echo "Building : bopserver..."
  cd bopserver
  git submodule update --init --recursive
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
	getDocumentation "bopserver"
	#tagVersion
  cd electronic-bop-form
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
	getDocumentation "bopserver/electronic-bop-form"
	#tagVersion
  cd rules
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
  npm install
	getDocumentation "bopserver/electronic-bop-form/rules"
	tagVersion
  cd ../
	tagVersion
  cd ../
	tagVersion
  npm install
  npm run build
  #./build.sh
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#******************************#
#****  Preprocessor Plugin ****#
checkoutRepo $sourceFolder "preprocessorplugin" "git@bitbucket.org:synthesis_admin/preprocessorplugin.git"
if [ -d "$sourceFolder/preprocessorplugin" ]
then
  echo "Building : preprocessorplugin..."
  cd preprocessorplugin
  git pull
	getDocumentation "preprocessorplugin"
	tagVersion
  gradle clean
  gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#****************************#
#****  Rules Java Engine ****#
checkoutRepo $sourceFolder "finsurv-rules-java-engine" "git@bitbucket.org:synthesis_admin/finsurv-rules-java-engine.git"
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
  echo "Building : finsurv-rules-java-engine..."
  cd finsurv-rules-java-engine
  git fetch
  git checkout "master" #Version 2.x rules engine.
  #git checkout "Version1" #Version 1.x rules engine
  git pull
	getDocumentation "finsurv-rules-java-engine"
	tagVersion
  gradle clean
  rm -rf "$sourceFolder/finsurv-rules-java-engine/build/**"
  gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#***************************#
#****  Rules Management ****#
checkoutRepo $sourceFolder "rules-management" "git@bitbucket.org:synthesis_admin/rules-management.git"
if [ -d "$sourceFolder/rules-management" ]
then
  echo "Building : rules-management..."
  cd rules-management
  git pull

  #*** Copy all the resources to the intelliJ "out" compilation folder ***#
  rm -rf out/production/resources/*
  cp -r build/resources/main/* out/production/resources/
	#*** Remove all stale, build artefacts (clean doesn't always seem to work)...
	rm -rf build/resources

	getDocumentation "rules-management"
	tagVersion
  gradle processResources
  #gradle build
  gradle --refresh-dependencies
  gradle clean
  gradle war
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#*****************************#
#****  Report Services UI ****#
checkoutRepo $sourceFolder "report-services-ui" "git@bitbucket.org:synthesis_admin/report-services-ui.git"
if [ -d "$sourceFolder/report-services-ui" ]
then
  echo "Building : report-services-ui (Master)..."
  echo "...RSUI App..."
  cd report-services-ui
  #nuisance files...
  git checkout -- package-lock.json
  git checkout -- package.json

  git pull
	getDocumentation "report-services-ui (Master)"
	tagVersion

  gradle clean

  npm install
	npm audit fix
  npm run build
  gradle wars

  #gradle war

  echo "***** WAR files => $destinationFolder *****"
  echo "	RSUI WAR... -> $destinationFolder"
  cp "$sourceFolder/report-services-ui/build/libs/*.war" "$destinationFolder"
  echo "***** WAR files copied. *****"
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#****************************#
#****  Report Data Store ****#
checkoutRepo $sourceFolder "report-data-store" "git@bitbucket.org:synthesis_admin/report-data-store.git"
if [ -d "$sourceFolder/report-data-store" ]
then
  echo "Building : report-data-store..."
  cd report-data-store
  git pull
	getDocumentation "report-data-store"
	tagVersion
  gradle clean
  gradle --refresh-dependencies
  #gradle build -x test
  gradle war -x test
fi
cd "$workingDir"
echo "...build complete."
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


#--------------------------------------------------------------#
#----  COPY THE BUILD FILES TO SOME FOLDER FOR DEPLOYMENT  ----#
#clear
#echo "...copying war files to destination folder: $destinationFolder"
echo "***** Remaining WAR files => $destinationFolder *****"

ls "$destinationFolder"
currentWar="$sourceFolder/rules-management/build/libs"
echo "	Rules Management WAR... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
currentWar="$sourceFolder/report-data-store/build/libs"
echo "	Report Data Store WAR... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
currentWar="$sourceFolder/report-services-ui/build/libs"
echo "	Report Services UI WARs... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
echo "***** WAR files copied. *****"
echo ""
echo ""
echo ""
echo "...WARs Complete."
echo ""
echo ""
echo ""

cd "$workingDir"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "






#-------------------------------------------------------------#
#----  BUILD AND COPY JAR FILES TO FOLDER FOR DEPLOYMENT  ----#
#clear
echo "***** Building and copying JAR files => $destinationFolder/jars/... *****"
echo ""
echo ""
echo ""

#***************************************#
#****  Rules Java Engine -- Java 6  ****#
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
  cd "$sourceFolder/finsurv-rules-java-engine"
  rm -rf "$sourceFolder/finsurv-rules-java-engine/build/preprocessed"
  echo "building : finsurv-rules-java-engine - Java 6..."
  gradle -b build16.gradle build
  echo "...build instruction complete."
fi
cd "$workingDir"
cd "$sourceFolder"

mkdir "$destinationFolder/OFFLINE_RULES_ENGINE_JARS"
currentJar="$sourceFolder/finsurv-rules-java-engine/build/libs"
echo "Copying finsurv-rules-java-engine JAR files to deployment folder..."
ls "$currentJar"
cp -f "$currentJar"/*.jar "$destinationFolder/OFFLINE_RULES_ENGINE_JARS/"
echo "***** JAR files copied. *****"

#** remove any source JARS that were copied over  **#
cd "$destinationFolder/OFFLINE_RULES_ENGINE_JARS"
rm -f *source*.jar
echo "***** Source JARs removed. *****"

echo ""
echo ""
echo ""
echo "...JAR files build and copy - complete."
echo ""
echo ""
echo ""

cd "$workingDir"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "



#----------------------------------------------------------#
#----  COPY SQL SCRIPT FILES TO FOLDER FOR DEPLOYMENT  ----#
#clear
echo "***** Copying SQL files => $destinationFolder/SQL/... *****"
echo ""
echo ""
echo ""

sqlDestFolder="$destinationFolder/SQL"
mkdir "$sqlDestFolder"
# SQL SCRIPTS SUB DIR STRUCTURE:
# /SQL
#   \_ /DataSetupScripts
#   \_ /Installation
#   \_ /Jobs
#        \_DocumentUploads
#   \_ /Update

# -- For NEW installation:
currentSql="$sourceFolder/report-data-store/database/Scripts"
#ls "$currentSql"

#-----------------------------------#
currentSqlFile="$currentSql/Installation";
sqlCurDestDir="Installation";
mkdir "$sqlDestFolder/$sqlCurDestDir"
echo "	Report Data Store SQL... cp -f -r -u \"$currentSqlFile\"/**  \"$sqlDestFolder/$sqlCurDestDir\""
cp -f -r -u "$currentSqlFile"  "$sqlDestFolder/$sqlCurDestDir"

currentSqlFile="$currentSql/Jobs";
sqlCurDestDir="Jobs";
mkdir "$sqlDestFolder/$sqlCurDestDir"
echo "	Report Data Store SQL... cp -f -r -u \"$currentSqlFile\"/**  \"$sqlDestFolder/$sqlCurDestDir\""
cp -f -r -u "$currentSqlFile"  "$sqlDestFolder/$sqlCurDestDir"

currentSqlFile="$currentSql/Update";
sqlCurDestDir="Update";
mkdir "$sqlDestFolder/$sqlCurDestDir"
echo "	Report Data Store SQL... cp -f -r -u \"$currentSqlFile\"/**  \"$sqlDestFolder/$sqlCurDestDir\""
cp -f -r -u "$currentSqlFile"  "$sqlDestFolder/$sqlCurDestDir"

currentSqlFile="$currentSql/Misc";
sqlCurDestDir="Misc";
mkdir "$sqlDestFolder/$sqlCurDestDir"
echo "	Report Data Store SQL... cp -f -r -u \"$currentSqlFile\"/**  \"$sqlDestFolder/$sqlCurDestDir\""
cp -f -r -u "$currentSqlFile"  "$sqlDestFolder/$sqlCurDestDir"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


#-----------------------------------#
# -- DATA SETUP SCRIPT FOR SBSA  -- #
#currentSqlFile="$currentSql/DataSetupScripts/RS_DataSetup.SBSA.sql";
#destinationSqlFile="05__RS_DataSetup.SBSA.sql";
#echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/New/$destinationSqlFile"
#cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/New/$destinationSqlFile"

# -- DATA SETUP SCRIPT FOR GENERIC INSTALL  -- #
currentSqlFile="$currentSql/DataSetupScripts/RS_DataSetup.GENERAL.sql";
sqlCurDestDir="DataSetupScripts/RS_DataSetup.GENERAL.sql";
mkdir "$sqlDestFolder/$sqlCurDestDir"
echo "	Report Data Store SQL... $currentSqlFile -> $sqlDestFolder/$sqlCurDestDir"
cp -f -u "$currentSqlFile" "$sqlDestFolder/$sqlCurDestDir"
#-----------------------------------#



##-----------------------------------#
## -- For EXISTING installation:
#mkdir "$destinationFolder/SQL/Existing"
#currentSql="$sourceFolder/report-data-store/database/database"
#
#currentSqlFile="RS_UpdateScript.sql";
#destinationSqlFile="01__$currentSqlFile";
#echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/Existing/$destinationSqlFile"
#cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/Existing/$destinationSqlFile"
#
#ls "$destinationFolder/SQL/Existing"
#-----------------------------------#
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

echo ""
echo ""
echo ""
echo "...Build Complete."

closeDocumentation




echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


cd "$destinationBaseFolder"
tarFileName="$destinationFolderName.tar.gz"
tar -zcv --file="$tarFileName" "$destinationFolderName"




#awsDir="$workingDir/AWS/RDS_eu-central-1"
awsDir="$workingDir/AWS/RDS_eu-west-1"
if [ -d $awsDir ]
then
	echo -n "Deploy to AWS? [Y/N]: "
	read deployToAWS
	echo " ** Deploy to AWS: $deployToAWS ** "
	if [[ $deployToAWS == "Y" ]]; then
		echo " ** AWS instance folder: $awsDir ** "
		cd "$awsDir"
		echo "...uploading WAR files and deployment (tar.gz) artefact..."
		$(cp -f -u $destinationFolder/*.war .)
		$(cp -f -u $destinationBaseFolder/$tarFileName .)
		./upload.sh
		#rm -f *.war

		#echo "Upload completed - deploying WAR files..."
		#ssh -i "finsurv-report-services.pem" "ubuntu@$awsHostName" './deploy.sh'

		echo " "
		echo "     **********     **********     **********     **********     **********"
		echo " "

		echo "...Awaiting EC2 Instance startup ( 4 min )..."
		sleep 240;
		echo " "
		echo " "


		#echo -n "Run evaluation correctness check against AWS? [Y/N]: "
		#read evalCorrectness
		evalCorrectness="Y";
		if [[ $evalCorrectness == "Y" ]]; then
			#curl -v --styled-output -X GET --header 'Accept: application/json' "http://ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:8080/report-data-store/test/evaluation/correctness"
			echo "curl -v --styled-output -X GET --header 'Accept: application/json' \"$testApiUrl\""
			curl -v --styled-output -X GET --header 'Accept: application/json' "$testApiUrl"
			echo " "
			echo "     **********     **********     **********     **********     **********"
			echo " "
		fi

		echo -n "Update integration examples: rules and artefacts? [Y/N]: "
		read updatePOC
		if [[ $updatePOC == "Y" ]]; then
			cd "$workingDir/ScriptsAndUtils"
			./FetchOfflineFormEvalAndValidationArtefacts.sh .
			echo " "
			echo "     **********     **********     **********     **********     **********"
			echo " "
		fi

		echo ""
		echo "     **********     **********     **********     **********     **********"
		echo ""
		echo "     DEPLOYMENT ARTEFACT AVAILABLE ON THE BELOW URL:"
		echo ""
		echo "     $deploymentArtefactBaseUrl$tarFileName"
		echo ""
		echo ""
		echo ""
		echo "     REPORT DATA STORE SWAGGER AND UI URLS:"
		echo ""
		echo "     $swaggerBaseUrl"
		echo "     $rsuiBaseUrl"
		echo ""
		echo "     **********     **********     **********     **********     **********"
		echo ""


		echo ""
		echo ""
		echo ""
		echo "...Deployment Complete."
	fi

	echo ""
	echo ""
	echo ""
	echo "...Script Complete."
fi
