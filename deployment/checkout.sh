#!/bin/bash

echo "==============================================="
echo "STD BANK RDS / RSUI / RULES MANAGER CHECKOUT..."
echo "==============================================="



workingDir=$(pwd)
#echo "...Working dir (return point): $workingDir"

#-----  SCRIPT USAGE  -----#
[ $# -eq 0 ] && { echo "Usage: $0 [project-source-parent-dir] "; echo "Example: $0 /d/projects"; exit 1; }
sourceFolder="$1"


#-----  SOURCE FOLDER  -----#
if [ ! -d "$sourceFolder" ]
then
    echo "Error: $sourceFolder not found"
    echo -n "Enter the path to create the projects source folder (current: '$sourceFolder'): "
    read srcFolder
    if [[ $srcFolder != "" ]]; then
        sourceFolder="$srcFolder"
    fi
    mkdir -p "$sourceFolder"
    [ ! -d "$sourceFolder" ] && { exit 1; }
fi
echo "...Project source folder: $sourceFolder"
#remove trailing "/" from source path...
re="(.+)\/"
if [[ $sourceFolder =~ $re ]]; then
    echo "...remove trailing slash '/'..."
    sourceFolder="${BASH_REMATCH[1]}";
    echo "...Project source folder: $sourceFolder"
fi
cd "$sourceFolder"


function checkoutRepo(){
    sourceDir=$1
    repoName=$2
    repoUrl=$3
    if [ ! -d "$sourceDir/$repoName" ]
    then
        echo "Clone $repoName repo..."
        git clone "$repoUrl"
        if [ $? -eq 0 ]; then
            echo "Git Clone successful."
        else
            exit 1;
        fi
    fi
}



#***************************#
#****  Finsurv tXstream ****#
checkoutRepo $sourceFolder "tXstream" "git@bitbucket.org:synthesis_admin/txstream.git"
if [ -d "$sourceFolder/tXstream" ]
then
    echo "Building : tXstream..."
    cd tXstream
    git pull
#    gradle clean build war
fi
cd "$workingDir"
cd "$sourceFolder"

#****************************#
#****  Finsurv Dashboard ****#
checkoutRepo $sourceFolder "finsurv-dashboard" "git@bitbucket.org:synthesis_admin/finsurv-dashboard.git"
if [ -d "$sourceFolder/finsurv-dashboard" ]
then
    echo "Building : finsurv-dashboard..."
    cd finsurv-dashboard
    git pull
#    gradle clean build
fi
cd "$workingDir"
cd "$sourceFolder"

#************************#
#****  Finsurv Rules ****#
#if [ -d "$sourceFolder/finsurv-rules" ] 
#then
#	echo "Deleting : finsurv-rules..."
#	rm -rf "$sourceFolder/finsurv-rules"
#fi
checkoutRepo $sourceFolder "finsurv-rules" "git@bitbucket.org:synthesis_admin/finsurv-rules.git"
if [ -d "$sourceFolder/finsurv-rules" ]
then
    echo "Building : finsurv-rules..."
    cd finsurv-rules
    #git checkout cleanup
    git checkout master
    git pull
    #npm install
    #npm run build
fi
cd "$workingDir"
cd "$sourceFolder"

#********************#
#****  BopServer ****#
#if [ -d "$sourceFolder/bopserver" ] 
#then
#	echo "Deleting : bopserver..."
#	rm -rf "$sourceFolder/bopserver/dist"
#	rm -rf "$sourceFolder/bopserver/build"
#	#rm -rf "$sourceFolder/bopserver/out"
#	#rm -rf "$sourceFolder/bopserver"
#fi
checkoutRepo $sourceFolder "bopserver" "git@bitbucket.org:synthesis_admin/bopserver.git"
if [ -d "$sourceFolder/bopserver" ]
then
  echo "Building : bopserver..."
  cd bopserver
  git submodule update --init --recursive
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
  cd electronic-bop-form
  git checkout -- package-lock.json
  git checkout -- package.json
  #git checkout cleanup
  git checkout master
  git pull
  cd rules
  git checkout -- package-lock.json
  git checkout -- package.json
  #git checkout cleanup
  git checkout master
  git pull

  cd ../
  cd ../
  npm install
  npm run build
  #./build.sh
fi
cd "$workingDir"
cd "$sourceFolder"

#******************************#
#****  Preprocessor Plugin ****#
checkoutRepo $sourceFolder "preprocessorplugin" "git@bitbucket.org:synthesis_admin/preprocessorplugin.git"
if [ -d "$sourceFolder/preprocessorplugin" ]
then
    echo "Building : preprocessorplugin..."
    cd preprocessorplugin
    git pull
    gradle clean
    gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"

#****************************#
#****  Rules Java Engine ****#
checkoutRepo $sourceFolder "finsurv-rules-java-engine" "git@bitbucket.org:synthesis_admin/finsurv-rules-java-engine.git"
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
    echo "Building : finsurv-rules-java-engine..."
    cd finsurv-rules-java-engine
    git pull
    gradle clean
    gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"

#***************************#
#****  Rules Management ****#
checkoutRepo $sourceFolder "rules-management" "git@bitbucket.org:synthesis_admin/rules-management.git"
if [ -d "$sourceFolder/rules-management" ]
then

    echo "Building : rules-management..."
    cd rules-management
    git pull

    #*** Copy all the resources to the intelliJ "out" compilation folder ***#
    rm -rf out/production/resources/*
    cp -r build/resources/main/* out/production/resources/
	#*** Remove all stale, build artefacts (clean doesn't always seem to work)...
	rm -rf build/resources

  gradle processResources
  #gradle build
  gradle --refresh-dependencies
  gradle clean
  gradle war

fi
cd "$workingDir"
cd "$sourceFolder"

#*****************************#
#****  Report Services UI ****#
#checkoutRepo $sourceFolder "report-services-ui" "git@bitbucket.org:synthesis_admin/report-services-ui.git"
#if [ -d "$sourceFolder/report-services-ui" ]
#then
#    echo "Building : report-services-ui (Master)..."
#    echo "...RSUI App..."
#    cd report-services-ui
#    #nuisance files...
#    git checkout -- package-lock.json
#    git checkout -- package.json
#
#    git pull
#
#    gradle clean
#    gradle --refresh-dependenices
#
#fi
#cd "$workingDir"
#cd "$sourceFolder"

#****************************#
#****  Report Data Store ****#
checkoutRepo $sourceFolder "report-data-store" "git@bitbucket.org:synthesis_admin/report-data-store.git"
if [ -d "$sourceFolder/report-data-store" ]
then
    echo "Building : report-data-store..."
    cd report-data-store
    git pull
    gradle clean
    gradle build
    gradle --refresh-dependencies
fi
cd "$workingDir"
echo "...build complete."



