echo "...You may need to sudo to get this to work..."
echo "Uploading war files..."
scp -i "finsurv-report-services.pem" *.war ubuntu@ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:/home/ubuntu
echo "Updating deployment shell script..."
scp -i "finsurv-report-services.pem" deploywars.sh ubuntu@ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:/home/ubuntu
echo "Uploading deployment (tar.gz) artefacts..."
scp -i "finsurv-report-services.pem" *.tar.gz ubuntu@ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:/home/ubuntu
echo "Uploading ZIP artefacts..."
scp -i "finsurv-report-services.pem" *.zip ubuntu@ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:/home/ubuntu
echo "Executing deployment shell script..."
ssh -i "finsurv-report-services.pem" "ubuntu@ec2-34-242-236-95.eu-west-1.compute.amazonaws.com" './deploywars.sh'
echo "...Complete."
echo "removing local copy of deployment (tar.gz) artefacts..."
rm -f *.tar.gz
echo "removing local copy of ZIP artefacts..."
rm -f *.zip

echo " "
echo " "
echo "please run the following command to test the correctness of the server:"
echo "curl -v --styled-output -X GET --header 'Accept: application/json' \"http://ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:8080/report-data-store/test/evaluation/correctness\""
echo " "
echo " "
echo "uploaded zip and tar.gz artefacts can be found in the following path on the ec2 instance:"
echo "\thttp://ec2-34-242-236-95.eu-west-1.compute.amazonaws.com:8080/builds/ ... "

