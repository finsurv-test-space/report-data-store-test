PRINT 'STATE OF CONFIGURATION TABLE BEFORE DELETION:'
SELECT [Name], [Area], CreatedDateTime FROM [SystemConfiguration] ORDER BY 1, 2, 3;

PRINT 'DELETING CONFIGS AND TYPES...'
DELETE FROM [SystemConfiguration] WHERE [Name] LIKE 'config/%' or [Name] LIKE 'type/%';

PRINT 'STATE OF CONFIGURATION TABLE AFTER DELETION:'
SELECT [Name], [Area], CreatedDateTime FROM [SystemConfiguration] ORDER BY 1, 2, 3;
