tomcatInstance="tomcat9"

#----------#
#  stop tomcat...
echo "Stopping Tomcat service..."
sudo service "$tomcatInstance" stop

#----------#
echo "Stashing old war files..."
sudo cp -f -u "/var/lib/$tomcatInstance/webapps/*.war" ~/


echo "Removing exploded war directories and obsolete wars..."
#  check and clear out the OLD wars from tomcat...
sudo rm -rf "/var/lib/$tomcatInstance/webapps/**"

##  check and clear out the OLD wars from tomcat...
#sudo rm -rf /var/lib/tomcat8/webapps_frontend/**

#----------#

#  check and clear out temp files for tomcat...
echo "Clearing tomcat cached files..."
sudo rm -rf /tmp/**

#----------#

#  check and clear out logs for tomcat...
echo "Clearing tomcat logs..."
sudo rm -rf "/var/lib/$tomcatInstance/logs/**"

#----------#

#  create backup copy of wars in user dir (change dates as necessary)...
#mkdir RDS_20180911
#cp *.war RDS_20180911

#----------#

echo "NOTE: We do not deploy to separate folders for the EC2 instance - it doesn't work with the current configurations."
echo "...As such, all wars are deployed as normal to the webapps directory."


#  move the war files to tomcat for deployment...
#echo "Deploying Frontend WARs..."
#sudo mv tXstreamClient.war "/var/lib/$tomcatInstance/webapps_frontend/"
#sudo mv finsurv-dashboard.war "/var/lib/$tomcatInstance/webapps_frontend/"

#  move the remaining war files to tomcat for deployment...
#echo "Deploying Backend WARs..."
echo "Deploying WARs to Tomcat..."
sudo mv *.war "/var/lib/$tomcatInstance/webapps/"

#----------#

echo "Moving deployment (tar.gz) artefacts to Tomcat..."
sudo mv *.tar.gz "/var/lib/$tomcatInstance/static/"

#----------#

echo "Moving ZIP artefacts to Tomcat..."
sudo mv *.zip "/var/lib/$tomcatInstance/static/"

#----------#

#  clear out stale DB configs...
echo "Attempting to clear stale DB configs using sqlcmd..."
sqlcmd -u -p1 -e -S "reportservicesdemodb-dev.cpbsecysczsq.eu-west-1.rds.amazonaws.com" -d ReportServices -U rs_user -P 'lI%559U$OMt7KC6' -i /home/ubuntu/clear_db_configs.sql >> sql_output.log

#----------#

#  start tomcat...
echo "Starting Tomcat service..."
sudo service "$tomcatInstance" start

#----------#

#  check that the wars are being exploded (repeat as necessary)...
#ls -la "/var/lib/$tomcatInstance/webapps/"

#----------#

#  tail the logs as necessary...
#tail -f -n 50 "/var/lib/$tomcatInstance/logs/report-data-store.log"

date +"%Y-%m-%d %T" >> deploy.log

#----------#
#  exit the remote terminal by entering the following command:
exit
