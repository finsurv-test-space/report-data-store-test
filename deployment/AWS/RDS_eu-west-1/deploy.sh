#!/bin/bash
clear 

tomcatInstance="tomcat"
#tomcatDir="/home/ubuntu/tomcat"
tomcatDir="/var/lib/apache-tomcat/Tomcat-SARB"
deployDate=$(date +"%Y%m%d")
#homeDir="/home/ubuntu"
homeDir=$(pwd)
relativeArtefactDir="uploads/$deployDate"
artefactsDir="uploads/$deployDate"

#  stop tomcat...
echo "Stopping Tomcat service..."
sudo service "$tomcatInstance" stop

#----------#
echo "----------"
echo ""
echo "Creating archive directories..."
mkdir -p "$relativeArtefactDir/"
mkdir -p "$relativeArtefactDir/webapps/bk/"
mkdir -p "$relativeArtefactDir/webapps_frontend/bk/"
#chmod -R 660 "$relativeArtefactDir/"

#sudo mkdir -p "$artefactsDir/"
#sudo mkdir -p "$artefactsDir/webapps/bk/"
#sudo mkdir -p "$artefactsDir/webapps_frontend/bk/"
#sudo chmod -R 660 "$artefactsDir/"

echo ""
echo "Stashing old war files..."
sudo cp -f "$tomcatDir/webapps"/*.war "$artefactsDir/webapps/bk/"
sudo cp -f "$tomcatDir/webapps_frontend"/*.war "$artefactsDir/webapps_frontend/bk/"

echo ""
echo "Cleaning home directory - moving artefacts..."
sudo mv -f "$homeDir"/tXstreamServer.war "$artefactsDir/webapps/"
sudo mv -f "$homeDir"/rules-management.war "$artefactsDir/webapps/"
sudo mv -f "$homeDir"/*.war "$artefactsDir/webapps_frontend/"
sudo mv -f "$homeDir"/*.zip "$artefactsDir/"
sudo mv -f "$homeDir"/*.tar.gz "$artefactsDir/"

echo ""
echo "Removing exploded war directories and obsolete wars..."
#  check and clear out the OLD wars from tomcat...
sudo rm -rf "$tomcatDir/webapps"/**
sudo rm -rf "$tomcatDir/webapps_frontend"/**

#----------#

#  check and clear out temp files for tomcat...
echo "----------"
echo ""
echo "Clearing tomcat cached files..."
sudo rm -rf "$tomcatDir/tmp"/**

#----------#

#  check and clear out logs for tomcat...
echo "----------"
echo ""
echo "Clearing tomcat logs..."
sudo rm -rf "$tomcatDir/logs"/**

#----------#

#  move the remaining war files to tomcat for deployment...
echo "----------"
echo ""
echo "Deploying WARs to Tomcat..."
#backups first...	
sudo mv -f "$artefactsDir/webapps/bk"/*.war "$tomcatDir/webapps/"
sudo mv -f "$artefactsDir/webapps_frontend/bk"/*.war "$tomcatDir/webapps_frontend/"
#then the new artefacts (overwrite backups)...
sudo cp -f "$artefactsDir/webapps"/*.war "$tomcatDir/webapps/"
sudo cp -f "$artefactsDir/webapps_frontend"/*.war "$tomcatDir/webapps_frontend/"
#kill bk dir...
#sudo rm -rf "$artefactsDir/webapps/bk"
#sudo rm -rf "$artefactsDir/webapps_frontend/bk"

#----------#
echo "----------"
echo ""
echo "Moving deployment (tar.gz) artefacts to Tomcat..."
sudo cp -f "$artefactsDir"/*.tar.gz "$tomcatDir/static/"
echo ""
echo "Moving ZIP artefacts to Tomcat..."
sudo cp -f "$artefactsDir"/*.zip "$tomcatDir/static/"

#----------#

#****************************************************************************************
#  TODO: FIX THIS!!!  
#****************************************************************************************
#  clear out stale DB configs...
echo "----------"
echo ""
echo "Attempting to clear stale DB configs using sqlcmd..."
sqlcmd -u -p1 -e -S "finsurvdb.co9oayzbdiwo.eu-west-1.rds.amazonaws.com" -d ReportServices -U tXstreamUser -P 'Password123' -i /home/ubuntu/clear_db_configs.sql
#****************************************************************************************

#----------#

#  start tomcat...
echo "----------"
echo ""
echo "Starting Tomcat service..."
sudo service "$tomcatInstance" start

#----------#

#  check that the wars are being exploded (repeat as necessary)...
#ls -la "/var/lib/$tomcatInstance/webapps/"

#----------#

#  tail the logs as necessary...
#tail -f -n 50 "/var/lib/$tomcatInstance/logs/report-data-store.log"

date +"%Y-%m-%d %T" >> deploy.log

#----------#
#  exit the remote terminal by entering the following command:
echo ""
echo "...Remote Deployment Complete."
echo "------------------------------"
exit
