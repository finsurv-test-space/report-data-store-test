#!/bin/bash
echo "...You may need to sudo to get this to work..."

deployDate=$(date +"%Y%m%d")
awsInstanceHost="ec2-34-244-160-180.eu-west-1.compute.amazonaws.com"
awsInstance="ubuntu@$awsInstanceHost"
awsInstanceCert="finsurv-report-services-dev.pem"
homeDir="/home/ubuntu"
relativeArtefactDir="uploads/$deployDate"
artefactDir="$homeDir/$relativeArtefactDir"

echo "REMOTE HOST:   \"$awsInstance\""
echo "CERT FILE:     \"$awsInstanceCert\""
echo "ARTEFACT DIR:  \"$artefactDir\""

#----  THIS IS NOT WORKING, PERMISSIONS OR PATHS OR SOMETHING ARE BORKED  ----#
#echo "Creating remote deployment artefact repo..."
#ssh -i "$awsInstanceCert" "$awsInstance" 'mkdir -p $artefactDir/'
#ssh -i "$awsInstanceCert" "$awsInstance" 'mkdir -p $artefactDir/webapps/'
#ssh -i "$awsInstanceCert" "$awsInstance" 'mkdir -p $artefactDir/webapps_frontend/'


#Systemd Unit file ==>  Service configuration -> https://www.linode.com/docs/quick-answers/linux/start-service-at-boot/
#echo "Uploading systemd unit file..."
#scp -i "$awsInstanceCert" "tomcat_systemd_unit_file" "$awsInstance":~/

echo "Uploading war files..."
#scp -i "$awsInstanceCert" tXstreamServer.war "$awsInstance":~/
scp -i "$awsInstanceCert" *.war "$awsInstance":~/

echo "Updating deployment scripts..."
scp -i "$awsInstanceCert" deploy.sh "$awsInstance":~/
scp -i "$awsInstanceCert" *.sql "$awsInstance":~/

echo "Uploading deployment (tar.gz) artefacts..."
scp -i "$awsInstanceCert" *.tar.gz "$awsInstance":~/

echo "Uploading ZIP artefacts..."
scp -i "$awsInstanceCert" *.zip "$awsInstance":~/

echo "Executing deployment shell script..."
ssh -i "$awsInstanceCert" "$awsInstance" './deploy.sh'

echo "...Upload Complete."
echo ""
echo "...Clean-up..."

echo "removing local copy of deployment (war) artefacts..."
rm -f *.war

echo "removing local copy of deployment (tar.gz) artefacts..."
rm -f *.tar.gz

echo "removing local copy of ZIP artefacts..."
rm -f *.zip

echo "...Clean-up Complete."
echo ""
echo "please run the following command to test the correctness of the server:"
echo "curl -v --styled-output -X GET --header 'Accept: application/json' \"http://$awsInstanceHost/report-data-store/test/evaluation/correctness\""
