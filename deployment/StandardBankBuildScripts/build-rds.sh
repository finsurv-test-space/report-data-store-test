#!/bin/bash

echo "============================================"
echo "STD BANK RDS / RSUI / RULES MANAGER BUILD..."
echo "============================================"


workingDir=$(pwd)
#echo "...Working dir (return point): $workingDir"

#-----  SCRIPT USAGE  -----#
[ $# -eq 0 ] && { echo "Usage: $0 [root-build-dir] [optional:dest-deployment-dir]"; echo "Example: $0 /d/build"; exit 1; }
rootFolder="$1"
echo "...Root build directory: $rootFolder"
#remove trailing "/" from root path...
re="^(.+)\/$"
if [[ $rootFolder =~ $re ]]; then
    echo "...remove trailing slash '/'..."
    rootFolder="${BASH_REMATCH[1]}";
fi
$rootFolder="$rootFolder"
sourceFolder="$rootFolder/src"
destinationFolder="$2"


#-----  SOURCE FOLDER  -----#
if [ ! -d "$sourceFolder" ]
then
    echo "Error: $sourceFolder not found"
    echo -n "Enter the path to create the projects source folder (current: '$sourceFolder'): "
    read srcFolder
    if [[ $srcFolder != "" ]]; then
        sourceFolder="$srcFolder"
    fi
    mkdir -p "$sourceFolder"
    [ ! -d "$sourceFolder" ] && { exit 1; }
fi
echo "...Project source folder: $sourceFolder"
cd "$sourceFolder"




#-----  DESTINATION FOLDER  -----#
if [[ $destinationFolder =~ $re ]]; then
    echo "...remove trailing slash '/'..."
    $destinationFolder="${BASH_REMATCH[1]}";
fi
if [ -d "$destinationFolder" ]
then
    echo "...Destination $destinationFolder exists - clearing..."
    rm -f "$destinationFolder/*"
    echo "...$destinationFolder cleared."
else
    echo "Error: destination $destinationFolder not found"
    if [[ "$destinationFolder" == "" ]]
    then
        dest="$rootFolder/dist/RDS_"
        curDate=$(date +"%Y%m%d")
        destinationFolder="$dest$curDate"
    fi

    echo -n "Enter the destination path for the deployment artifacts (currently: '$destinationFolder'): "
    read destFolder
    if [[ $destFolder != "" ]]; then
        destinationFolder="$destFolder"
    fi

    mkdir -p "$destinationFolder"
    if [ ! -d "$destinationFolder" ]
    then
        exit 1;
    fi
fi
echo "...WAR files destination folder: $destinationFolder"



function checkoutRepo(){
    sourceDir=$1
    repoName=$2
    repoUrl=$3
    if [ ! -d "$sourceDir/$repoName" ]
    then
        echo "Clone $repoName repo..."
        git clone "$repoUrl"
        if [ $? -eq 0 ]; then
            echo "Git Clone successful."
        else
            exit 1;
        fi
    fi
}



#***************************#
#****  Finsurv tXstream ****#
#checkoutRepo $sourceFolder "tXstream" "git@bitbucket.org:synthesis_admin/txstream.git"
#if [ -d "$sourceFolder/tXstream" ]
#then
#    echo "Building : tXstream..."
#    cd tXstream
#    git pull
#    gradle clean build war
#fi
#cd "$workingDir"
#cd "$sourceFolder"

#****************************#
#****  Finsurv Dashboard ****#
#checkoutRepo $sourceFolder "finsurv-dashboard" "git@bitbucket.org:synthesis_admin/finsurv-dashboard.git"
#if [ -d "$sourceFolder/finsurv-dashboard" ]
#then
#    echo "Building : finsurv-dashboard..."
#    cd finsurv-dashboard
#    git pull
#    gradle clean build
#fi
#cd "$workingDir"
#cd "$sourceFolder"

#************************#
#****  Finsurv Rules ****#
checkoutRepo $sourceFolder "finsurv-rules" "git@bitbucket.org:synthesis_admin/finsurv-rules.git"
if [ -d "$sourceFolder/finsurv-rules" ]
then
    echo "Building : finsurv-rules..."
    cd finsurv-rules
    git checkout cleanup
    git pull
    #npm install
    #npm run build
fi
cd "$workingDir"
cd "$sourceFolder"

#********************#
#****  BopServer ****#
checkoutRepo $sourceFolder "bopserver" "git@bitbucket.org:synthesis_admin/bopserver.git"
if [ -d "$sourceFolder/bopserver" ]
then
    echo "Building : bopserver..."
    cd bopserver
    git submodule update --init --recursive
    git checkout -- package-lock.json
    git checkout -- package.json
    git checkout master
    git pull
    cd electronic-bop-form
    git checkout -- package-lock.json
    git checkout -- package.json
    git checkout cleanup
    git pull
    cd rules
    git checkout -- package-lock.json
    git checkout -- package.json
    git checkout cleanup
    git pull
    cd ../../
    npm install
    npm run build
fi
cd "$workingDir"
cd "$sourceFolder"

#******************************#
#****  Preprocessor Plugin ****#
checkoutRepo $sourceFolder "preprocessorplugin" "git@bitbucket.org:synthesis_admin/preprocessorplugin.git"
if [ -d "$sourceFolder/preprocessorplugin" ]
then
    echo "Building : preprocessorplugin..."
    cd preprocessorplugin
    git pull
    gradle clean
    gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"

#****************************#
#****  Rules Java Engine ****#
checkoutRepo $sourceFolder "finsurv-rules-java-engine" "git@bitbucket.org:synthesis_admin/finsurv-rules-java-engine.git"
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
    echo "Building : finsurv-rules-java-engine..."
    cd finsurv-rules-java-engine
    git pull
    gradle clean
    gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"

#***************************#
#****  Rules Management ****#
checkoutRepo $sourceFolder "rules-management" "git@bitbucket.org:synthesis_admin/rules-management.git"
if [ -d "$sourceFolder/rules-management" ]
then


    #*** Copy all the resources to the intelliJ "out" compilation folder ***#
    rm -rf out/production/resources/*
    #cp -r build/resources/main/* out/production/resources/

    echo "Building : rules-management..."
    cd rules-management
    git pull
    gradle processResources
    gradle build
    gradle --refresh-dependencies
    gradle clean
    gradle war
fi
cd "$workingDir"
cd "$sourceFolder"

#*****************************#
#****  Report Services UI ****#
checkoutRepo $sourceFolder "report-services-ui" "git@bitbucket.org:synthesis_admin/report-services-ui.git"
if [ -d "$sourceFolder/report-services-ui" ]
then
    echo "Building : report-services-ui (Master)..."
    echo "...RSUI App..."
    cd report-services-ui
    #nuisance files...
    git checkout -- package-lock.json
    git checkout -- package.json

    git pull

    gradle clean

    npm install
	  npm audit fix
    npm run build
    gradle wars

    #gradle war

    echo "***** WAR files => $destinationFolder *****"
    echo "	RSUI WAR... -> $destinationFolder"
    cp "$sourceFolder/report-services-ui/build/libs/*.war" "$destinationFolder"
    echo "***** WAR files copied. *****"
fi
cd "$workingDir"
cd "$sourceFolder"

#****************************#
#****  Report Data Store ****#
checkoutRepo $sourceFolder "report-data-store" "git@bitbucket.org:synthesis_admin/report-data-store.git"
if [ -d "$sourceFolder/report-data-store" ]
then
    echo "Building : report-data-store..."
    cd report-data-store
    git pull
    gradle build
    gradle --refresh-dependencies
    gradle clean
    gradle war
fi
cd "$workingDir"
echo "...build complete."


#--------------------------------------------------------------#
#----  COPY THE BUILD FILES TO SOME FOLDER FOR DEPLOYMENT  ----#
#clear
#echo "...copying war files to destination folder: $destinationFolder"
echo "***** Remaining WAR files => $destinationFolder *****"

ls "$destinationFolder"
currentWar="$sourceFolder/rules-management/build/libs"
echo "	Rules Management WAR... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
currentWar="$sourceFolder/report-data-store/build/libs"
echo "	Report Data Store WAR... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
currentWar="$sourceFolder/report-services-ui/build/libs"
echo "	Report Services UI WARs... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
ls "$destinationFolder"
echo "***** WAR files copied. *****"
echo ""
echo ""
echo ""
echo "...WARs Complete."
cd "$workingDir"






#-------------------------------------------------------------#
#----  BUILD AND COPY JAR FILES TO FOLDER FOR DEPLOYMENT  ----#
#clear
echo "***** Building and copying JAR files => $destinationFolder/jars/... *****"


#***************************************#
#****  Rules Java Engine -- Java 6  ****#
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
    echo "building : finsurv-rules-java-engine - Java 6..."
    gradle -b build16.gradle build
    echo "...build instruction complete."
fi
cd "$workingDir"
cd "$sourceFolder"

mkdir "$destinationFolder/OFFLINE_RULES_ENGINE_JARS"
currentJar="$sourceFolder/finsurv-rules-java-engine/build/libs"
echo "Copying finsurv-rules-java-engine JAR files to deployment folder..."
ls "$currentJar"
cp -f "$currentJar"/*.jar "$destinationFolder/OFFLINE_RULES_ENGINE_JARS/"
rm -rf "$destinationFolder/OFFLINE_RULES_ENGINE_JARS/*source*.jar"
ls "$destinationFolder/OFFLINE_RULES_ENGINE_JARS/"
echo "***** JAR files copied. *****"
echo ""
echo ""
echo ""
echo "...Script Complete."
cd "$workingDir"



#----------------------------------------------------------#
#----  COPY SQL SCRIPT FILES TO FOLDER FOR DEPLOYMENT  ----#
#clear
echo "***** Copying SQL files => $destinationFolder/SQL/... *****"

mkdir "$destinationFolder/SQL"
# DIR STRUCTURE:
# /SQL
#   \_ /New
#   \_ /Existing

# -- For NEW installation:
mkdir "$destinationFolder/SQL/New"
currentSql="$sourceFolder/report-data-store/database/database"
#ls "$currentSql"

currentSqlFile="RS_CreateTables_MSSQL.sql";
destinationSqlFile="01__$currentSqlFile";
echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/New/$destinationSqlFile"
cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/New/$destinationSqlFile"

currentSqlFile="RS_Triggers.sql";
destinationSqlFile="02__$currentSqlFile";
echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/New/$destinationSqlFile"
cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/New/$destinationSqlFile"

currentSqlFile="RS_Views.sql";
destinationSqlFile="03__$currentSqlFile";
echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/New/$destinationSqlFile"
cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/New/$destinationSqlFile"

# -- DATA SETUP SCRIPT FOR SBSA  -- #
currentSqlFile="DataSetupScripts/RS_DataSetup.SBSA.sql";
destinationSqlFile="04__RS_DataSetup.SBSA.sql";
echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/New/$destinationSqlFile"
cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/New/$destinationSqlFile"

ls "$destinationFolder/SQL/New"


# -- For EXISTING installation:
mkdir "$destinationFolder/SQL/Existing"
currentSql="$sourceFolder/report-data-store/database/database"

currentSqlFile="RS_UpdateScript.sql";
destinationSqlFile="01__$currentSqlFile";
echo "	Report Data Store SQL... $currentSql -> $destinationFolder/SQL/Existing/$destinationSqlFile"
cp -f "$currentSql/$currentSqlFile" "$destinationFolder/SQL/Existing/$destinationSqlFile"

ls "$destinationFolder/SQL/Existing"
