﻿# If you are having trouble running this powershell script, you may need to change your execution policy with the below command... 
#
# Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted
#

$sqlServer = "ITDS110"
$sqlDBName = "ReportServices"

$curDate = Get-Date -format "yyyyMMdd"
$rootDir = Get-Location
$destinationDir = "$rootDir\..\HaywoodFiles"
$currentHaywoodFile = "$destinationDir\HaywoodExport_$curDate.xml"

IF (-NOT ( Test-Path $destinationDir ) ) {
	New-Item -Path $destinationDir -ItemType Directory
}

sqlcmd -S "$sqlServer" -d "$sqlDBName" -E -U sa -P Albaraka786 -Q "DECLARE @DayToRun DATE, @TrnReference VARCHAR(200), @ExportXml xml; SET @DayToRun = (SELECT GETDATE()); SET @TrnReference = NULL; EXEC [dbo].[ExtractSARBXmlFromRDS] @ValueDate = @DayToRun, @ReportKey = @TrnReference, @ExportXml = @ExportXml OUTPUT; SELECT @ExportXml AS N'SARB_Xml';" -o "$currentHaywoodFile" -h-1
    
