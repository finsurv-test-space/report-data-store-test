﻿###############################################################################################################################################################
#
# TXstream Offline Artefact Download Script
# =========================================
#
# Author: James Eckhardt
# ----------------------
#
# This script is intended as a useable example for downloading the necessary BOP artefacts from TXstream Finsurv Reporting Services.
# The offline artefacts allow systems to be integrated using centrally managed and updated rules and artefacts for BOP reporting - without the usual dependencies
# on highly available services and infrastructure, or the overheads usually associated with API-based integrations for similar purposes.1
# The offline artefacts are capable of running entirely offline, but there may be varying dependencies on external validation calls to systems such as:
# - ARM
# - IVS
# - TXstream Server: for validations such as SDA & FIA limits
#
# NOTE:
# - This script requires PowerShell v3+
# - (TODO) The PowerShell Script does not currently check the file hash of the downloaded artefacts with the corresponding file hash on the server 
# - If you are having trouble running this powershell script, you may need to change your execution policy with the below command:
#   "Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted"
#
###############################################################################################################################################################


$channelNames = "sbNA", "sbMW", "sbZA"      # The list of channel packages to download for offline caching (Specific to Regulator, Client, Division and/or System)
$curDate = Get-Date -format "yyyyMMdd"
$rootDir = Get-Location
$ReportServicesServerBaseRDSUrl = "http://ec2-3-250-203-40.eu-west-1.compute.amazonaws.com:81/report-data-store/" # Report Services Instance (Current Synthesis EC2 Dev instance, by default)


foreach ($channelName in $channelNames){
    $formJsPath="producer/api/rules/js?channelName=$channelName"        # JS Rules engine + Self-contained BOP Form partials and Encoded artefacts (CSS etc)
    $evalRulesPath="producer/api/rules/$channelName/evaluation"         # Evaluation Rules (Determine whether the transaction needs to be reported or not)
    $lookupsRulesPath="producer/api/rules/$channelName/lookups"         # Static + Psuedo-static Lookup Data (Regulator-specific values, Client-specific data + additionally configured values)
    $documentRulesPath="producer/api/rules/$channelName/document"       # Excon Document Validation Rules (Accompanying Docs Required for BOP)
    $validationRulesPath="producer/api/rules/$channelName/validation"   # BOP Validation Rules (Determine the correctness and completeness of the supplied BOP data)
    $Artefacts = @{"form.js"="$ReportServicesServerBaseRDSUrl$formJsPath"; "evaluation.js"="$ReportServicesServerBaseRDSUrl$evalRulesPath"; "lookups.js"="$ReportServicesServerBaseRDSUrl$lookupsRulesPath"; "document.js"="$ReportServicesServerBaseRDSUrl$documentRulesPath"; "validation.js"="$ReportServicesServerBaseRDSUrl$validationRulesPath"}

    $currentCopyDir ="$rootDir\producer\api\rules\$channelName"             # Base artefact path
    $tempDir        ="$rootDir\producer\api\rules\$channelName\temp"        # Temp artefact download & integrity checking path
    $previousCopyDir="$rootDir\producer\api\rules\$channelName\previous"    # Snapshot of previous version of the artefact set
    $destinationDir ="$rootDir\producer\api\rules\$channelName\$curDate"    # Artefact snapshot dir for current artefact set

    $currentCopyDir

    IF (-NOT ( Test-Path $currentCopyDir ) ) {
        New-Item -Path $currentCopyDir -ItemType Directory
    }
    IF (-NOT ( Test-Path $tempDir ) ) {
        New-Item -Path $tempDir -ItemType Directory
    }


    $Artefacts.Keys | % { 
        "key = $_ , value = " + $Artefacts.Item($_) 
        $ArtefactContent = Invoke-WebRequest $Artefacts.Item($_) -SessionVariable rds
        IF (Test-Path "$tempDir\$_"){
            Remove-Item "$tempDir\$_"
        }
        New-Item -Path "$tempDir\$_" -ItemType File
        Set-Content "$tempDir\$_" $ArtefactContent

        IF (Test-Path "$currentCopyDir\$_"){
            IF ((Get-FileHash "$currentCopyDir\$_").Hash -ne (Get-FileHash "$tempDir\$_").Hash) {
                Write-Warning "Artefact $_ has changed, backing up current version and deploying new..."

                IF (-NOT ( Test-Path $previousCopyDir ) ) {
                    New-Item -Path $previousCopyDir -ItemType Directory
                }
                IF (-NOT ( Test-Path $destinationDir ) ) {
                    New-Item -Path $destinationDir -ItemType Directory
                }

                Copy-Item "$tempDir\$_" -Destination "$destinationDir\$_"
                Copy-Item "$currentCopyDir\$_" -Destination "$previousCopyDir\$_"
                Copy-Item "$tempDir\$_" -Destination "$currentCopyDir\$_"
            } ELSE {
                "Artefact $_ is unchanged - ignoring."
            }
        } ELSE {
            "Artefact $_ doesn't exist - deploying current version."
            Copy-Item "$tempDir\$_" -Destination "$currentCopyDir\$_"
        }
    }

}