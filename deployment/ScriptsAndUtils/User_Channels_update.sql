/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [GUID]
      ,[Key]
      ,[AuthToken]
      ,[Version]
      ,[Content]
      ,[TStamp]
      ,[CreatedDateTime]
  FROM [tXstream_RDS].[dbo].[User]


SELECT JSON_QUERY([Content], '$.channels') as channels,  JSON_QUERY(JSON_MODIFY(JSON_MODIFY([Content], '$.channels', JSON_VALUE('[]','$')), 'append $.channels', 'sasfin'), '$.channels') as updatedChannels
FROM [tXstream_RDS].[dbo].[User]

UPDATE [tXstream_RDS].[dbo].[User]
SET [Content] = JSON_MODIFY(JSON_MODIFY([Content], '$.channels', JSON_VALUE('[]','$')), 'append $.channels', 'sasfin')