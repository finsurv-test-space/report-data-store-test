#!/bin/bash

###############################################################################################################################################################
#
# TXstream Offline Artefact Download Script
# =========================================
#
# Author: James Eckhardt
# ----------------------
#
# This script is intended as a useable example for downloading the necessary BOP artefacts from TXstream Finsurv Reporting Services.
# The offline artefacts allow systems to be integrated using centrally managed and updated rules and artefacts for BOP reporting - without the usual dependencies
# on highly available services and infrastructure, or the overheads usually associated with API-based integrations for similar purposes.
# The offline artefacts are capable of running entirely offline, but there may be varying dependencies on external validation calls to systems such as:
# - ARM
# - IVS
# - TXstream Server: for validations such as SDA & FIA limits
#
# This script will download a copy of the relevant artefacts for the given channel, check their integrity by calculating a local checksum and comparing
# with the server checksum APIS, and then replace only the artefacts where a difference has been detected.
# Backups are of artefacts are kept in date-stamped directories, within the channel directory.
#
###############################################################################################################################################################


workingDir=$(pwd)
#echo "...Working dir (return point): $workingDir"
curDate=$(date +"%Y%m%d")
#echo "...Current date: $curDate"

#####################################################################################################################################################
####                                                                                                                                             ####
####    CONFIGURATION SECTION                                                                                                                    ####
####                                                                                                                                             ####
#####################################################################################################################################################


#NB: This is the channel to use for the artefacts - be sure to use the one relevant to your system!   ...OR READ IT IN AS A PARAMETER!
channelName="sbZA"

#####  PLEASE SELECT THE SOURCE OF THE ARTEFACTS TO FETCH FROM  #####

# ----  Local deployment/cache of the artefacts  ---- #
ReportServicesServerBaseRDSUrl="http://localhost:8084/report-data-store"

# ----  LIBRA ReportServices UAT environment  ----#
#ReportServicesServerBaseRDSUrl="http://10.147.149.36:8080/report-data-store/"

# ----  Synthesis Dev EC2 Instance  ----#
#ReportServicesServerBaseRDSUrl="http://ec2-3-250-203-40.eu-west-1.compute.amazonaws.com:81/report-data-store/"

#####################################################################################################################################################
#####################################################################################################################################################


#-----  SCRIPT USAGE  -----#
[ $# -eq 0 ] && { echo "Usage: $0 [RootDownloadDIRECTORY]"; echo "Example: $0 /d/ReportServicesDownloads\n OR    $0 ."; exit 1; }

#-----  ROOT DOWNLOAD DIRECTORY  -----#
rootDir="$1"
echo "...Root download directory: $rootDir"
#remove trailing "/" from root path...
re="^(.+)\/$"
if [[ $rootDir =~ $re ]]; then
    echo "...remove trailing slash '/'..."
    rootDir="${BASH_REMATCH[1]}";
fi
rootDir="$rootDir"
mkdir -p "$rootDir"
if [ ! -d "$rootDir" ]
then
	echo "Unable to create root-download directory for deployment artefacts: '$rootDir'."
	exit 1;
fi

#-----  TEMP DOWNLOAD DIRECTORY  -----#
tempDir="$rootDir/producer/api/rules/$channelName/temp"
mkdir -p "$tempDir"
if [ ! -d "$tempDir" ]
then
	echo "Unable to create temp directory for deployment artefacts: '$tempDir'."
	exit 1;
fi


#-----  CURRENT-COPY ARTEFACTS DIRECTORY  -----#
currentCopyDir="$rootDir/producer/api/rules/$channelName"
mkdir -p "$currentCopyDir"
if [ ! -d "$currentCopyDir" ]
then
	echo "Unable to create current-copy directory for deployment artefacts: '$currentCopyDir'."
	exit 1;
fi


#-----  PREVIOUS-COPY ARTEFACTS DIRECTORY  -----#
previousCopyDir="$rootDir/producer/api/rules/$channelName/previous"
mkdir -p "$previousCopyDir"
if [ ! -d "$previousCopyDir" ]
then
	echo "Unable to create previous-copy directory for deployment artefacts: '$previousCopyDir'."
	exit 1;
fi


#-----  DESTINATION DIRECTORY  -----#
destinationDir="$rootDir/producer/api/rules/$channelName/$curDate"
if [[ $destinationDir =~ $re ]]; then
    echo "...remove trailing slash '/'..."
    $destinationDir="${BASH_REMATCH[1]}";
fi
if [ -d "$destinationDir" ]
then
    echo "...Destination (DELTA DIR) $destinationDir exists - clearing..."
    rm -f "$destinationDir/*"
    echo "...$destinationDir cleared (DELTA DIR)."
else
    echo "Warning: destination (DELTA DIR) $destinationDir not found"
    if [[ "$destinationDir" == "" ]]
    then
        dest="$rootDir/ReportServices_"
        destinationDir="$dest$curDate"
    fi

    echo -n "Enter the destination (DELTA DIR) path for the revised deployment artifacts (currently: '$destinationDir'): "
    read destDir
    if [[ $destDir != "" ]]; then
        destinationDir="$destDir"
    fi
fi
echo "...Artefact files destination (DELTA DIR) directory: $destinationDir"



#==========================================================================================================================================
# CONFIGURATION AND VARIABLES
#==========================================================================================================================================


echo "==========================================="
echo "Params:"
echo "[curDate] => $curDate"
echo "[ReportServicesServerBaseRDSUrl] => $ReportServicesServerBaseRDSUrl"
echo "[channelName] => $channelName"

#Artefact APIs:
formJsPath="/producer/api/rules/$channelName/js?packedRules=true"
#formJsPath="/producer/api/rules/$channelName/js"
echo "[formJsPath] => $formJsPath"
evalRulesPath="/producer/api/rules/$channelName/evaluation"
echo "[evalRulesPath] => $evalRulesPath"
lookupsRulesPath="/producer/api/rules/$channelName/lookups"
echo "[lookupsRulesPath] => $lookupsRulesPath"
validationRulesPath="/producer/api/rules/$channelName/validation"
echo "[validationRulesPath] => $validationRulesPath"
documentRulesPath="/producer/api/rules/$channelName/document"
echo "[documentRulesPath] => $documentRulesPath"

#Checksum API(s):
baseChecksumUrl="$ReportServicesServerBaseRDSUrl/producer/api/rules/$channelName/check/"



#==========================================================================================================================================
# Functions
#==========================================================================================================================================


#----  DOWNLOAD THE ARTEFACT FROM THE API  ----#

function FetchArtefact(){
    artefactName=$1
    artefactUrl=$2
	#curl --basic --user "$RESTUsername":"$RESTPassword" -X GET "$artefactUrl" > "$tempDir/$artefactName"	
	curl -X GET "$artefactUrl" > "$tempDir/$artefactName"	
}

#----  CHECK ARTEFACT FILE AND MOVE IF SUCCESSFUL  ----#

function CompareArtefacts(){
    artefactName=$1
	artefactId=$2
    if [ -f "$tempDir/$artefactName" ]
    then
		if [ -f "$currentCopyDir/$artefactName" ]
		then
			#diff in linux : https://superuser.com/questions/234810/how-do-i-compare-two-files-with-a-shell-script
			#further file comparators: https://www.networkworld.com/article/3190407/data-center/nine-ways-to-compare-files-on-unix.html
			if cmp -s "$tempDir/$artefactName" "$currentCopyDir/$artefactName"
			then
				echo "Artefact ($artefactName) is identical to previous version - ignoring."
			else
				echo "Artefact ($artefactName) has changed from previous version..."
				
				#--  FORCE CHECKSUM VERIFICATION OF DOWNLOADED ARTEFACT TO FAIL  --#
				#res=$(echo " /* blah */ " >> "$tempDir/$artefactName")
				
				#--  (TODO) CHECK THAT THE ARTEFACT IS WELL-FORMED  --#
				#CheckJSON "$artefacName" "$artefactId"

				#--  (TODO) CHECK ARTEFACT VERSION COMPATIBILITY  --#
				#CheckArtefactCompatibility "$artefacName" "$artefactId" "$RulesEngineVersion"

				#--  VERIFY DOWNLOADED ARTEFACT INTEGRITY WITH SHA-256 CHECKSUM  --#
				artefactCheck=$(CheckArtefactSHA256Hash "$artefactName" "$artefactId")


				if [ "$artefactCheck" = "PASSED" ]; 
				then
					echo "...$artefactName download verification: $artefactCheck"
					#--  ONLY CREATE IF ALL TESTS PASS FOR ATLEAST ONE ARTEFEACT  --#
					mkdir -p "$destinationDir"
					if [ ! -d "$destinationDir" ]
					then
						echo "Unable to create directory for deployment artefacts: '$destinationDir'."
						exit 1;
					else
						mv -f "$currentCopyDir/$artefactName" "$previousCopyDir/$artefactName"
						cp -f "$tempDir/$artefactName" "$destinationDir/$artefactName"
						cp -f "$tempDir/$artefactName" "$currentCopyDir/$artefactName"
						echo "NOTE: $artefactName copied to current."
					fi
				else 
					echo "...ERROR - Skipping deployment of artefact ($artefactName): verification failed."
				fi
			fi
		else 
			echo "No current version of the artefact: $artefactName"
			cp -f "$tempDir/$artefactName" "$currentCopyDir/$artefactName"
			echo "NOTE: $artefactName copied to current."
		fi
    fi
}

function CheckJSON(){
    artefactName=$1
	artefactId=$2
	#NOTE:
	#-----
	#  The implementation of this function may require some mechanism to strip of the encapsulating JS (Require) syntax
	#  before the contained JSON can be checked for well-formedness.
	#
	#Well Formedness:
	#----------------
	# recommendation is to use either JAVA-based JS/JSON parser OR  NODEJS-based JS/JSON parser - most notably JSLint.
	#  https://xmodulo.com/validate-json-command-line-linux.html
	#
	# JSLint:
	# Douglas Crockfords JS Lint stuff:  https://code.google.com/archive/p/js-ria-tools/wikis/JSLint.wiki
	# http://www.jslint.com/
      #for JSLint, you could run the jar impl. and create and alias thereto: https://stackoverflow.com/questions/30138769/how-to-run-jslint-using-bash
      #further reading for JSLint on ubuntu command line: http://infoheap.com/jslint-command-line-on-ubuntu-linux/
      #https://stackoverflow.com/questions/8863888/how-do-i-install-jslint-on-ubuntu
      #Java Implementation Usage:
      #java -jar /path/to/js.jar jslint <file1.js> <file2.js> ... <fileN.js>
	#jq:
      #jq usage for JSON well-formedness testing:
      #if jq -e . >/dev/null 2>&1 <<<"$json_string"; then
      #	echo "Parsed JSON successfully and got something other than false/null"
      #else
      #	echo "Failed to parse JSON, or got false/null"
      #fi
	#python (json.tool):
      #Validate JSON using python:
      #echo '{"foo":"bar"}' | python -m json.tool  >> /dev/null && exit 0 || echo "NOT valid JSON"; exit 1
}



function CheckArtefactSHA256Hash (){
    artefactName=$1
	artefactId=$2
    artefactChecksumUrl="$baseChecksumUrl$artefactId"
	checksumToUse="SHA-256"
	checksumForArtefact=$(cat "$tempDir/$artefactName" | sha256sum)
	checksumForArtefact=$( echo "$checksumForArtefact" | sed -e 's/[^a-zA-Z0-9]//g')
	response=$(curl -sb -H "Accept: application/json" "$artefactChecksumUrl?checksumAlgorithm=$checksumToUse&checksumValue=$checksumForArtefact")
	re="\"Matched\"\:true"
	if [[ $response =~ $re ]]; then
		echo "PASSED"
	else 
		echo "FAILED"
	fi
}



#==========================================================================================================================================
# SCRIPT LOGIC
#==========================================================================================================================================


FetchArtefact "form.js" "$ReportServicesServerBaseRDSUrl$formJsPath"
FetchArtefact "evaluation.js" "$ReportServicesServerBaseRDSUrl$evalRulesPath"
FetchArtefact "lookups.js" "$ReportServicesServerBaseRDSUrl$lookupsRulesPath"
FetchArtefact "validation.js" "$ReportServicesServerBaseRDSUrl$validationRulesPath"
FetchArtefact "document.js" "$ReportServicesServerBaseRDSUrl$documentRulesPath"

CompareArtefacts "form.js" "js"
CompareArtefacts "evaluation.js" "evaluation"
CompareArtefacts "lookups.js" "lookups"
CompareArtefacts "validation.js" "validation"
CompareArtefacts "document.js" "document"

