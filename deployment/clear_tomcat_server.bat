@ECHO OFF
cls
cd C:\Program Files\Apache Software Foundation\Tomcat 8.5

echo RUN_DATE_TIME >> tomcat_clear.log
date /T >> tomcat_clear.log
time /T >> tomcat_clear.log
color 4F


@ECHO ...DELETING LOGS...
del /F /Q logs\*

@ECHO ...DELETING CACHE...
del /F /Q temp\*

@ECHO ...DELETING WARS...
del /F /Q webapps\*.war

@ECHO ...DELETING EXPANDED WARS...
rmdir /Q /S webapps\admin
rmdir /Q /S webapps\bmp
rmdir /Q /S webapps\rsui
rmdir /Q /S webapps\report-data-store
rmdir /Q /S webapps\rules-management

color 07

@ECHO ** THIS WILL ERASE THE DATABASE CONTENTS (SYSTEM CONFIGURATION DATA)! **


@ECHO Deleting existing logs...
del *.log
@ECHO ...Logs purged, beginning deployment of sql scripts...


@ECHO Running 0000_sql_deploy_schema_counts.sql... 
echo "***  0000_sql_deploy_schema_counts.sql  ***" >> tomcat_clear.log
echo START_DATE_TIME 
date /T 
time /T 
echo START_DATE_TIME  >> tomcat_clear.log
date /T >> tomcat_clear.log
time /T >> tomcat_clear.log
sqlcmd -u -p1 -e -r1 -S localhost -d ReportServices -Q "DELETE FROM [ReportServices].[dbo].[SystemConfiguration]" >> tomcat_clear.log 
echo END_DATE_TIME  >> tomcat_clear.log
date /T >> tomcat_clear.log
time /T >> tomcat_clear.log
echo END_DATE_TIME 
date /T 
time /T 
