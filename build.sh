#!/bin/bash

ROOT=$PWD

if [ $# -gt 0 ]
then
  sed -i "s|^\s*\"version\":.*|\"version\":\"$1\"|g" ./src/main/resources/config/version.json
  sed -i "s|^\s*version =.*|version = \"$1\"|g" ./build.gradle
  echo "Version is set to $1"
else
  echo "Version could not be set to $1"
fi

# bump_version.sh & commit + push

write_metadata(){
   echo "{
        	\"artefact\":
                [{
                	\"name\":\"${ROOT}/build/libs/*.war\",
                	\"destination\":\"TOMCAT_BACK\"
                }]
        }" >> ${ROOT}/build/metadata.json
}

rm -rf ${ROOT}/build
./gradlew clean
./gradlew --refresh-dependencies
./gradlew war -x test

ret_code=$?
if [ $ret_code == 1 ]
then
	echo "Exiting Script"
	exit 1
fi

write_metadata