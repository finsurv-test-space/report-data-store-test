{
  "datasource": {
    "jdbc.driverClassName" : "com.microsoft.sqlserver.jdbc.SQLServerDriver",
    "jdbc.url" : "jdbc:sqlserver://${txstreamZA.host};databaseName=${txstreamZA.database};sendStringParametersAsUnicode=false",
    "jdbc.username": "${txstreamZA.username}",
    "jdbc.password" : "${txstreamZA.password}"
  },
  "validations": {
    "SBValidate_ReversalTrnRef": {
      "endpoint": "sbvalidReversalTrnRef",
      "parameters": [
        { "name": "trnRef", "path": "value" },
        { "name": "currency", "path": "transaction::FlowCurrency" },
        { "name": "reversalTrnSeq", "path": "ReversalTrnSeqNumber" },
        { "name": "foreignValue", "path": "ForeignValue" }
      ],
      "sql": [
        "SELECT t.CurrencyCode, ma.ForeignValue, ma.CategoryCode ",
        " FROM OR_Transaction t WITH (NOLOCK)",
        " JOIN OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference",
        " WHERE t.TrnReference = '${util.escapeSql(trnRef)}' AND ma.SequenceNumber = ${util.escapeSql(reversalTrnSeq)}",
        " UNION ALL ",
        " SELECT t.CurrencyCode, ma.ForeignValue, ma.CategoryCode ",
        " FROM archive.OR_Transaction t WITH (NOLOCK)",
        " JOIN archive.OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference",
        " WHERE t.TrnReference = '${util.escapeSql(trnRef)}' AND ma.SequenceNumber = ${util.escapeSql(reversalTrnSeq)}"
      ],
      "compose": [
        "<#assign row = util.sqlFirstRowDeserialize(response) >",
        "<#assign reversal = row.CategoryCode?ends_with('00') >",
        "<#if row??>",
        "  <#if reversal>",
        "{status: 'fail', code: 'E02', message: 'Original transaction is a reversal. Cannot reverse a reversal'}",
        "  <#elseif row.CurrencyCode == currency>",
        "  </#else>",
        "{status: 'error', code: 'W01', message: 'Reversal currency is different from the original transaction. Ensure reversal amount is reasonable'}",
        "  </#if>",
        "{status: 'pass'}",
        "</#if>"
      ]
    },
    "SBValidate_IVS": {
      "endpoint": "validateIVS",
      "parameters": [
        { "name": "FieldValue", "path": "value" },
        { "name": "TrnRef", "path": "transaction::TrnReference" },
        { "name": "Seq", "path": "money::SequenceNumber" },
        { "name": "ThirdPartyCCN", "path": "money::ThirdParty.CustomsClientNumber" },
        { "name": "ResidentInvCCN", "path": "transaction::Resident.Individual.CustomsClientNumber" },
        { "name": "ResidentEntityCCN", "path": "transaction::Resident.Entity.CustomsClientNumber" },
        { "name": "TDN", "path": "TransportDocumentNumber" },
        { "name": "MRN", "path": "ImportControlNumber" }
      ],
      "rest": {
        "verb": "POST",
        "headers": [
          {
            "name": "Content-Type",
            "value": "application/xml"
          }
        ],
        "url": "http://fmodab1jbslz.standardbank.co.za:10080/services/compliance/ivs/request",
        "body": ["<#assign nowTime = .now>",
          "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>",
          "<IVS>",
          " <Version>1</Version>",
          " <Test>1</Test>",
          " <Import>",
          "  <ImportVerification>",
          "   <TransportDocumentNumber>${TDN}</TransportDocumentNumber>",
          "   <CustomsClientNumber><#if ThirdPartyCCN??>${ThirdPartyCCN}<#elseif ResidentInvCCN??>${ResidentInvCCN}<#else>${ResidentEntityCCN}</#if></CustomsClientNumber>",
          "   <MRN>${MRN}</MRN>",
          "   <BOPTrnReference>${TrnRef}</BOPTrnReference>",
          "   <BOPSequenceNumber>${Seq}</BOPSequenceNumber>",
          "   <Description></Description>",
          "   <DateTime>${nowTime?iso_local_m_nz}</DateTime>",
          "  </ImportVerification>",
          " </Import>",
          "</IVS>"
        ]
      },
      "compose": [
        "<#assign softError=['203', '204', '210', '400', '401', '408'] >",
        "<#if response?? && util.xmlDeserialize(response)??>",
        "  <#assign doc = util.xmlDeserialize(response) >",
        "  <#if doc.IVS??>",
        "    <#if doc.IVS.Import?? && doc.IVS.Import.VerificationResponse??>",
        "      <#if !(doc.IVS.Import.VerificationResponse.@ErrorCode[0]??)>",
        "{status: 'pass'}",
        "      <#else>",
        "{",
        "  status: '${softError?seq_contains(doc.IVS.Import.VerificationResponse.@ErrorCode)?string('error', 'fail')}',",
        "  code: '${doc.IVS.Import.VerificationResponse.@ErrorCode}',",
        "  message: '${util.escapeJavaScript(doc.IVS.Import.VerificationResponse.@ErrorMessage)}'",
        "}",
        "      </#if>",
        "    <#else>",
        "      <#list doc.reason as data>",
        "{status: 'fail', code: '${util.escapeJavaScript(data.number)}', message: '${util.escapeJavaScript(data.description)}'}",
        "      </#list>",
        "    </#if>",
        "  <#else>",
        "{status: 'fail', code: 412, message: 'The call to ARM within SBSA returned an unknown or unexpected response.'}",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: 'NOF', message: 'External IVS service response is empty'}",
        "</#if>"
      ]
    },
    "SBVerify_ARM": {
      "endpoint": "verifyARM",
      "parameters": [
        { "name": "FieldValue", "path": "value" },
        { "name": "TrnRef", "path": "transaction::TrnReference" },
        { "name": "Seq", "path": "money::SequenceNumber" },
        { "name": "Flow", "path": "transaction::Flow" },
        { "name": "FlowCurrency", "path": "transaction::FlowCurrency" },
        { "name": "TotalFlowAmount", "path": "transaction::TotalForeignValue" },
        { "name": "FlowAmount", "path": "money::ForeignValue" },
        { "name": "SarbAuth", "path": "money::SARBAuth.SARBAuthAppNumber" },
        { "name": "InternalAuth", "path": "money::SARBAuth.ADInternalAuthNumber" }
      ],
      "rest": {
        "verb": "POST",
        "headers": [
          {
            "name": "Content-Type",
            "value": "application/xml"
          }
        ],
        "url": "http://fmodab1jbslz.standardbank.co.za/cibws/services/compliance/exchange-control/${SarbAuth}${InternalAuth}/verify",
        "body": ["<#assign nowTime = .now>",
          "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>",
          "<verifyExcon>",
          "  <flow><#if Flow=='OUT'>Buy<#else>Sell</#if></flow>",
          "  <transaction>",
          "    <currency>${FlowCurrency}</currency>",
          "    <amount>${FlowAmount}</amount>",
          "  </transaction>",
          "</verifyExcon>"
        ]
      },
      "compose": [
        "<#assign doc = util.xmlDeserialize(response) >",
        "<#if doc.verificationSuccessful??>",
        "  <#if doc.verificationSuccessful == 'YES'>",
        "{status: 'pass'}",
        "  <#else>",
        "    <#list doc.reason as data>",
        "{status: 'fail', code: '${util.escapeJavaScript(data.number)}', message: '${util.escapeJavaScript(data.description)}'}",
        "    </#list>",
        "  </#if>",
        "<#else>",
        "{status: 'fail', code: 'W01', message: 'The call to ARM within SBSA returned an unknown or unexpected response.'}",
        "</#if>"
      ]
    }
	},
	"lookups": {
	}
}
