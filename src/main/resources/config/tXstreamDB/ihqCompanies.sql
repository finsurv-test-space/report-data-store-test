SELECT CompanyName as companyName,
       Code as ihqCode,
       RegistrationNumber as registrationNumber
  FROM OR_IHQ WITH (NOLOCK)
