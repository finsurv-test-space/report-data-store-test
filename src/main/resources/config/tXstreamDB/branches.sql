SELECT BranchCode as code, BranchName as name, HubCode as hubCode, HubName as hubName,
       ISNULL(ValidFrom, '2000-01-01') as validFrom, ValidTo as validTo
  FROM OR_Branch WITH (NOLOCK)
 WHERE BranchName NOT LIKE 'Unknown%'
