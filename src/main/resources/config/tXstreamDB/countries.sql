SELECT CountryCode as code,
       Name as name
  FROM OR_Country WITH (NOLOCK)
 WHERE Name NOT LIKE 'Unknown%'
