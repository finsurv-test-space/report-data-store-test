var mapping = {
  src: {
/*    cd3_ack: {
      description: "SARB Response",
      select: "SELECT r.* FROM Result r",
      transaction_field: "r.TRN_REFERENCE",
      order_by: "r.TRN_REFERENCE",
      master_src: "cd3_tran"
    },*/
    cd3_tran: {
      description: "Transaction",
      select: "SELECT rq.Name as ReportingQualifier, CASE WHEN t.Flow = 'I' THEN 'IN' ELSE 'OUT' END as Flow, ae.Amount as TotalAmount, CASE WHEN ReplacementTrnReference IS NULL THEN 'N' ELSE 'Y' END as ReplacementInd, nr_at.Name as NR_AccountType, re_at.Name as R_AccountType, t.* FROM OR_Transaction t WITH (NOLOCK) LEFT JOIN OR_AccountEntry ae WITH (NOLOCK) ON ae.TrnReference = t.TrnReference LEFT JOIN OR_AccountType nr_at WITH (NOLOCK) ON nr_at.AccountTypeID = t.NR_AccountTypeID LEFT JOIN OR_AccountType re_at WITH (NOLOCK) ON re_at.AccountTypeID = t.R_AccountTypeID LEFT JOIN OR_ReportingQualifier rq WITH (NOLOCK) ON rq.ReportingQualifierID = t.ReportingQualifierID WHERE t.TrnReference IN ({'References'})",
      transaction_field: "t.TrnReference",
      order_by: "t.TrnReference"
    },
    cd3_money: {
      description: "Monetary Amount",
      select: "SELECT CASE WHEN ma.CategoryCode LIKE '___/__' THEN SUBSTRING(ma.CategoryCode, 1, 3) ELSE ma.CategoryCode END as CategoryMainCode, CASE WHEN ma.CategoryCode LIKE '___/__' THEN SUBSTRING(ma.CategoryCode, 5, 2) ELSE NULL END as CategorySubCode, ma.*, tp.* FROM OR_MonetaryAmount ma WITH (NOLOCK) LEFT JOIN OR_ThirdParty tp WITH (NOLOCK) ON tp.TrnReference = ma.TrnReference WHERE ma.TrnReference IN ({'References'})",
      master_transaction_field: "TrnReference",
      transaction_field: "ma.TrnReference",
      money_field: "ma.SequenceNumber",
      order_by: "ma.TrnReference, ma.SequenceNumber",
      master_src: "cd3_tran"
    },
    cd3_importexport: {
      description: "Import Export entries",
      select: "SELECT ie.* FROM OR_ImportExport ie WITH (NOLOCK) WHERE ie.TrnReference IN ({'References'})",
      master_transaction_field: "TrnReference",
      master_money_field: "SequenceNumber",
      transaction_field: "ie.TrnReference",
      money_field: "ie.SequenceNumber",
      order_by: "ie.TrnReference, ie.SequenceNumber, ie.SubSequenceNumber",
      master_src: "cd3_money"
    }
  },
  dest: [
/*    { scope: "result",
      src: "cd3_ack",
      fields: [
        {col: "ERROR_CODE",          field: "ErrorCode"},
        {col: "ERROR_DESCRIPTION",   field: "ErrorDescription"},
        {col: "ATTRIBUTE",           field: "ErrorAttribute"},
        {col: "TRN_REFERENCE",       field: "TrnReference"},
        {col: "SEQUENCE_NUMBER",     field: "Level1SeqNo"},
        {col: "SEQUENCE_NUMBER",     field: "Level2SeqNo"},
        {col: "TRN_STATUS",          field: "TrnStatus"}
      ]
    },*/
    { scope: "transaction",
      src: "cd3_tran",
      fields: [
        {col: "TrnReference",         field: "TrnReference"},
        {col: "ReportingQualifier",   field: "ReportingQualifier"}, // BOPCUS
        {col: "Flow",                 field: "Flow"}, // OUT
        {col: "ReplacementInd",       field: "ReplacementTransaction"}, // N
        {col: "ReplacementTrnReference", field: "ReplacementOriginalReference"}, //
        {col: "ValueDate",             field: "ValueDate", type: "date"}, // 22/DEC/14
        {col: "BranchCode",            field: "BranchCode", default: ""}, // 02015500
        {col: "BranchName",            field: "BranchName", default: ""}, // TPS TRADE PROCESSING
        {col: "HubCode",               field: "HubCode"}, //
        {col: "HubName",               field: "HubName"}, //
        {col: "OriginatingBank",       field: "OriginatingBank"}, // SBZAZAJJ
        {col: "OriginatingCountryCode", field: "OriginatingCountry"}, // ZA
        {col: "CorrespondentBank",     field: "CorrespondentBank"}, // SCBLUS33
        {col: "CorrespondentCountryCode", field: "CorrespondentCountry"}, // US
        {col: "ReceivingBank",         field: "ReceivingBank"}, // BOTKJPJT
        {col: "ReceivingCountryCode",  field: "ReceivingCountry"}, // JP
        {col: "CurrencyCode",          field: "FlowCurrency"},
        {col: "DomesticCurrency",      field: "DomesticCurrency", default: "ZAR"},
        {col: "TotalAmount",           field: "TotalForeignValue"}, // 454659.56
        {col: "NR_Type",               meta : "NR_Type"}, // I, E, X
        {col: "NRI_Surname",           field: "NonResident.Individual.Surname", condition: {NR_Type: "I"}}, //
        {col: "NRI_Name",              field: "NonResident.Individual.Name", condition: {NR_Type: "I"}}, //
        {col: "NRI_Gender",            field: "NonResident.Individual.Gender", condition: {NR_Type: "I"}}, //
        {col: "NRI_PassportNumber",    field: "NonResident.Individual.PassportNumber", condition: {NR_Type: "I"}}, //
        {col: "NRI_PassportCountryCode", field: "NonResident.Individual.PassportCountry", condition: {NR_Type: "I"}}, //
        {col: "NR_AddressLine1",       field: "NonResident.Individual.Address.AddressLine1", condition: {NR_Type: "I"}}, //
        {col: "NR_AddressLine2",       field: "NonResident.Individual.Address.AddressLine2", condition: {NR_Type: "I"}}, //
        {col: "NR_Suburb",             field: "NonResident.Individual.Address.Suburb", condition: {NR_Type: "I"}}, //
        {col: "NR_City",               field: "NonResident.Individual.Address.City", condition: {NR_Type: "I"}}, //
        {col: "NR_State",              field: "NonResident.Individual.Address.State", condition: {NR_Type: "I"}}, //
        {col: "NR_ZIPCode",            field: "NonResident.Individual.Address.PostalCode", condition: {NR_Type: "I"}}, //
        {col: "NR_CountryCode",        field: "NonResident.Individual.Address.Country", condition: {NR_Type: "I"}}, //
        {col: "NR_AccountType",        field: "NonResident.Individual.AccountIdentifier", condition: {NR_Type: "I"}}, // NON RESIDENT OTHER
        {col: "NR_AccountNumber",      field: "NonResident.Individual.AccountNumber", condition: {NR_Type: "I"}},
        {col: "NRE_EntityName",        field: "NonResident.Entity.EntityName", condition: {NR_Type: "E"}}, //
        {col: "NR_AccountType",        field: "NonResident.Entity.AccountIdentifier", condition: {NR_Type: "E"}}, //
        {col: "NR_AccountNumber",      field: "NonResident.Entity.AccountNumber", condition: {NR_Type: "E"}}, //
        {col: "NR_AddressLine1",       field: "NonResident.Entity.Address.AddressLine1", condition: {NR_Type: "E"}}, //
        {col: "NR_AddressLine2",       field: "NonResident.Entity.Address.AddressLine2", condition: {NR_Type: "E"}}, //
        {col: "NR_Suburb",             field: "NonResident.Entity.Address.Suburb", condition: {NR_Type: "E"}}, //
        {col: "NR_City",               field: "NonResident.Entity.Address.City", condition: {NR_Type: "E"}}, //
        {col: "NR_State",              field: "NonResident.Entity.Address.State", condition: {NR_Type: "E"}}, //
        {col: "NR_ZIPCode",            field: "NonResident.Entity.Address.PostalCode", condition: {NR_Type: "E"}}, //
        {col: "NR_CountryCode",        field: "NonResident.Entity.Address.Country", condition: {NR_Type: "E"}}, //
        {col: "NRX_ExceptionName",     field: "NonResident.Exception.ExceptionName", condition: {NR_Type: "X"}, default: ""}, //
        {col: "R_Type",                meta : "R_Type"}, // I, E, X
        {col: "RI_Surname",            field: "Resident.Individual.Surname", condition: {R_Type: "I"}}, //
        {col: "RI_Name",               field: "Resident.Individual.Name", condition: {R_Type: "I"}}, //
        {col: "RI_Gender",             field: "Resident.Individual.Gender", condition: {R_Type: "I"}}, //
        {col: "RI_DoB",                field: "Resident.Individual.DateOfBirth", type: "date", condition: {R_Type: "I"}}, //
        {col: "RI_IDNumber",           field: "Resident.Individual.IDNumber", condition: {R_Type: "I"}}, //
        {col: "RI_TempResPermitNumber", field: "Resident.Individual.TempResPermitNumber", condition: {R_Type: "I"}}, //
        {col: "RI_ForeignIDNumber",    field: "Resident.Individual.ForeignIDNumber", condition: {R_Type: "I"}}, //
        {col: "RI_ForeignIDCountry",   field: "Resident.Individual.ForeignIDCountry", condition: {R_Type: "I"}}, //
        {col: "RI_PassportNumber",     field: "Resident.Individual.PassportNumber", condition: {R_Type: "I"}}, //
        {col: "RI_PassportCountryCode", field: "Resident.Individual.PassportCountry", condition: {R_Type: "I"}}, //
        {col: "R_AccountName",         field: "Resident.Individual.AccountName", condition: {R_Type: "I"}},
        {col: "R_AccountType",         field: "Resident.Individual.AccountIdentifier", condition: {R_Type: "I"}}, // RESIDENT OTHER
        {col: "R_AccountNumber",       field: "Resident.Individual.AccountNumber", condition: {R_Type: "I"}},
        {col: "R_CustomsClientNumber", field: "Resident.Individual.CustomsClientNumber", condition: {R_Type: "I"}},
        {col: "R_TaxNumber",           field: "Resident.Individual.TaxNumber", condition: {R_Type: "I"}},
        {col: "R_VATNumber",           field: "Resident.Individual.VATNumber", condition: {R_Type: "I"}},
        {col: "R_TAXClrCertInd",       field: "Resident.Individual.TaxClearanceCertificateIndicator", condition: {R_Type: "I"}}, // N
        {col: "R_TAXClrCertRef",       field: "Resident.Individual.TaxClearanceCertificateReference", condition: {R_Type: "I"}}, //
        {col: "R_StreetAddressLine1",  field: "Resident.Individual.StreetAddress.AddressLine1", condition: {R_Type: "I"}},
        {col: "R_StreetAddressLine2",  field: "Resident.Individual.StreetAddress.AddressLine2", condition: {R_Type: "I"}}, //
        {col: "R_StreetSuburb",        field: "Resident.Individual.StreetAddress.Suburb", condition: {R_Type: "I"}},
        {col: "R_StreetCity",          field: "Resident.Individual.StreetAddress.City", condition: {R_Type: "I"}}, // GAUTENG
        {col: "R_StreetProvince",      field: "Resident.Individual.StreetAddress.Province", transform: "toUpper", condition: {R_Type: "I"}}, // GAUTENG
        {col: "R_StreetPostalCode",    field: "Resident.Individual.StreetAddress.PostalCode", condition: {R_Type: "I"}}, // 1501
        {col: "R_PostalAddressLine1",  field: "Resident.Individual.PostalAddress.AddressLine1", condition: {R_Type: "I"}},
        {col: "R_PostalAddressLine2",  field: "Resident.Individual.PostalAddress.AddressLine2", condition: {R_Type: "I"}}, //
        {col: "R_PostalSuburb",        field: "Resident.Individual.PostalAddress.Suburb", condition: {R_Type: "I"}}, // BENONI SOUTH
        {col: "R_PostalCity",          field: "Resident.Individual.PostalAddress.City", condition: {R_Type: "I"}}, // GAUTENG
        {col: "R_PostalProvince",      field: "Resident.Individual.PostalAddress.Province", transform: "toUpper", condition: {R_Type: "I"}}, // GAUTENG
        {col: "R_PostalCode",          field: "Resident.Individual.PostalAddress.PostalCode", condition: {R_Type: "I"}}, // 1502
        {col: "R_ContactSurname",      field: "Resident.Individual.ContactDetails.ContactSurname", condition: {R_Type: "I"}},
        {col: "R_ContactName",         field: "Resident.Individual.ContactDetails.ContactName", condition: {R_Type: "I"}},
        {col: "R_ContactEmail",        field: "Resident.Individual.ContactDetails.Email", condition: {R_Type: "I"}}, //
        {col: "R_ContactFax",          field: "Resident.Individual.ContactDetails.Fax", condition: {R_Type: "I"}}, //
        {col: "R_ContactTelephone",    field: "Resident.Individual.ContactDetails.Telephone", condition: {R_Type: "I"}},
        {col: "RE_LegalEntityName",    field: "Resident.Entity.EntityName", condition: {R_Type: "E"}},
        {col: "RE_TradingName",        field: "Resident.Entity.TradingName", condition: {R_Type: "E"}},
        {col: "RE_RegistrationNumber", field: "Resident.Entity.RegistrationNumber", condition: {R_Type: "E"}},
        {col: "RE_InstitutionalSector", field: "Resident.Entity.InstitutionalSector", condition: {R_Type: "E"}}, // 01
        {col: "RE_IndustrialClass",    field: "Resident.Entity.IndustrialClassification", condition: {R_Type: "E"}}, // 08
        {col: "R_AccountName",         field: "Resident.Entity.AccountName", condition: {R_Type: "E"}},
        {col: "R_AccountType",         field: "Resident.Entity.AccountIdentifier", condition: {R_Type: "E"}},
        {col: "R_AccountNumber",       field: "Resident.Entity.AccountNumber", condition: {R_Type: "E"}},
        {col: "R_CustomsClientNumber", field: "Resident.Entity.CustomsClientNumber", condition: {R_Type: "E"}},
        {col: "R_TaxNumber",           field: "Resident.Entity.TaxNumber", condition: {R_Type: "E"}},
        {col: "R_VATNumber",           field: "Resident.Entity.VATNumber", condition: {R_Type: "E"}},
        {col: "R_TAXClrCertInd",       field: "Resident.Entity.TaxClearanceCertificateIndicator", condition: {R_Type: "E"}}, // N
        {col: "R_TAXClrCertRef",       field: "Resident.Entity.TaxClearanceCertificateReference", condition: {R_Type: "E"}}, //
        {col: "R_StreetAddressLine1",  field: "Resident.Entity.StreetAddress.AddressLine1", condition: {R_Type: "E"}},
        {col: "R_StreetAddressLine2",  field: "Resident.Entity.StreetAddress.AddressLine2", condition: {R_Type: "E"}}, //
        {col: "R_StreetSuburb",        field: "Resident.Entity.StreetAddress.Suburb", condition: {R_Type: "E"}}, // BENONI SOUTH
        {col: "R_StreetCity",          field: "Resident.Entity.StreetAddress.City", condition: {R_Type: "E"}}, // GAUTENG
        {col: "R_StreetProvince",      field: "Resident.Entity.StreetAddress.Province", transform: "toUpper", condition: {R_Type: "E"}}, // GAUTENG
        {col: "R_StreetPostalCode",    field: "Resident.Entity.StreetAddress.PostalCode", condition: {R_Type: "E"}}, // 1501
        {col: "R_PostalAddressLine1",  field: "Resident.Entity.PostalAddress.AddressLine1", condition: {R_Type: "E"}},
        {col: "R_PostalAddressLine2",  field: "Resident.Entity.PostalAddress.AddressLine2", condition: {R_Type: "E"}}, //
        {col: "R_PostalSuburb",        field: "Resident.Entity.PostalAddress.Suburb", condition: {R_Type: "E"}}, // BENONI SOUTH
        {col: "R_PostalCity",          field: "Resident.Entity.PostalAddress.City", condition: {R_Type: "E"}}, // GAUTENG
        {col: "R_PostalProvince",      field: "Resident.Entity.PostalAddress.Province", transform: "toUpper", condition: {R_Type: "E"}}, // GAUTENG
        {col: "R_PostalCode",          field: "Resident.Entity.PostalAddress.PostalCode", condition: {R_Type: "E"}}, // 1502
        {col: "R_ContactSurname",      field: "Resident.Entity.ContactDetails.ContactSurname", condition: {R_Type: "E"}},
        {col: "R_ContactName",         field: "Resident.Entity.ContactDetails.ContactName", condition: {R_Type: "E"}},
        {col: "R_ContactEmail",        field: "Resident.Entity.ContactDetails.Email", condition: {R_Type: "E"}}, //
        {col: "R_ContactFax",          field: "Resident.Entity.ContactDetails.Fax", condition: {R_Type: "E"}}, //
        {col: "R_ContactTelephone",    field: "Resident.Entity.ContactDetails.Telephone", condition: {R_Type: "E"}},
        {col: "RX_ExceptionName",      field: "Resident.Exception.ExceptionName", condition: {R_Type: "X"}, default: ""}, //
        {col: "RX_CountryCode",        field: "Resident.Exception.Country", condition: {R_Type: "X"}} //
      ]
    },
    { scope: "money",
      src: "cd3_money",
      fields: [
        {col: "SequenceNumber",        field: "SequenceNumber"}, // 1
        {col: "MoneyTransferAgentID",  field: "MoneyTransferAgentIndicator"}, // AD
        {col: "RandValue",             field: "DomesticValue"}, // 454659.56
        {col: "ForeignValue",          field: "ForeignValue"}, // 38700.03
        {col: "CategoryMainCode",      field: "CategoryCode"}, // 104
        {col: "CategorySubCode",       field: "CategorySubCode"}, // 01
        {col: "SwiftDetails",          field: "SWIFTDetails"},
        {col: "StrateRefNumber",       field: "StrateRefNumber"},
        {col: "LoanRefNumber",         field: "LoanRefNumber"},
        {col: "LoanTenor",             field: "LoanTenor"},
        {col: "LoanInterestRate",      field: "LoanInterestRate"},
        {col: "RulingSection",         field: "RegulatorAuth.RulingsSection"}, // B.1(H)
        {col: "InternalAuthNumber",    field: "RegulatorAuth.REInternalAuthNumber"}, //
        {col: "InternalAuthDate",      field: "RegulatorAuth.REInternalAuthNumberDate", type: "date"}, //
        {col: "CBAuthAppNumber",      field: "RegulatorAuth.CBAuthAppNumber"}, //
        {col: "CBAuthReference",      field: "RegulatorAuth.CBAuthRefNumber"}, //
        {col: "CannotCategorize",      field: "CannotCategorize"}, //
        {col: "AdhocSubject",          field: "AdHocRequirement.Subject"}, //
        {col: "AdhocDescription",      field: "AdHocRequirement.Description"}, //
        {col: "LocationCountryCode",   field: "LocationCountry"}, // JO
        {col: "ReversalTrnRefNumber",  field: "ReversalTrnRefNumber"}, //
        {col: "ReversalSequence",      field: "ReversalTrnSeqNumber"}, //
        {col: "IndividualSurname",     field: "ThirdParty.Individual.Surname"}, //
        {col: "IndividualName",        field: "ThirdParty.Individual.Name"}, //
        {col: "IndividualGender",      field: "ThirdParty.Individual.Gender"}, //
        {col: "IndividualIDNumber",    field: "ThirdParty.Individual.IDNumber"}, //
        {col: "IndividualDoB",         field: "ThirdParty.Individual.DateOfBirth", type: "date"}, //
        {col: "IndividualTempResPermitNum", field: "ThirdParty.Individual.TempResPermitNumber"}, //
        {col: "IndividualPassportNumber", field: "ThirdParty.Individual.PassportNumber"}, //
        {col: "IndividualPassportCountry", field: "ThirdParty.Individual.PassportCountry"}, //
        {col: "EntityName",            field: "ThirdParty.Entity.Name"}, //
        {col: "EntityRegistrationNumber", field: "ThirdParty.Entity.RegistrationNumber"}, //
        {col: "CustomsClientNumber",   field: "ThirdParty.CustomsClientNumber"}, //
        {col: "TaxNumber",             field: "ThirdParty.TaxNumber"}, //
        {col: "VatNumber",             field: "ThirdParty.VATNumber"}, //
        {col: "StreetAddressLine1",    field: "ThirdParty.StreetAddress.AddressLine1"}, //
        {col: "StreetAddressLine2",    field: "ThirdParty.StreetAddress.AddressLine2"}, //
        {col: "StreetSuburb",          field: "ThirdParty.StreetAddress.Suburb"}, //
        {col: "StreetCity",            field: "ThirdParty.StreetAddress.City"}, //
        {col: "StreetProvince",        field: "ThirdParty.StreetAddress.Province", transform: "toUpper"}, //
        {col: "StreetPostalCode",      field: "ThirdParty.StreetAddress.PostalCode"}, //
        {col: "PostalAddressLine1",    field: "ThirdParty.PostalAddress.AddressLine1"}, //
        {col: "PostalAddressLine2",    field: "ThirdParty.PostalAddress.AddressLine2"}, //
        {col: "PostalSuburb",          field: "ThirdParty.PostalAddress.Suburb"}, //
        {col: "PostalCity",            field: "ThirdParty.PostalAddress.City"}, //
        {col: "PostalProvince",        field: "ThirdParty.PostalAddress.Province", transform: "toUpper"}, //
        {col: "PostalCode",            field: "ThirdParty.PostalAddress.PostalCode"}, //
        {col: "ContactSurname",        field: "ThirdParty.ContactDetails.ContactSurname"}, //
        {col: "ContactName",           field: "ThirdParty.ContactDetails.ContactName"}, //
        {col: "Email",                 field: "ThirdParty.ContactDetails.Email"}, //
        {col: "Fax",                   field: "ThirdParty.ContactDetails.Fax"}, //
        {col: "Telephone",             field: "ThirdParty.ContactDetails.Telephone"} //
      ]
    },
    { scope: "importexport",
      src: "cd3_importexport",
      fields: [
        {col: "ImportControlNumber",   field: "ImportControlNumber"}, //
        {col: "TransportDocumentNumber", field: "TransportDocumentNumber"}, //
        {col: "MRNNotOnIVS",           field: "MRNNotOnIVS"}, //
        {col: "UCR",                   field: "UCR"}, //
        {col: "PaymentValue",          field: "PaymentValue"}, //
        {col: "PaymentCurrencyCode",   field: "PaymentCurrencyCode"} //
      ]
    }
  ]
}
