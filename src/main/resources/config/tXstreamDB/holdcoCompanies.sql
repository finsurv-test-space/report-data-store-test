SELECT AccountName as companyName,
       AccountNumber as accountNumber,
       RegistrationNumber as registrationNumber
  FROM OR_HOLDCO WITH (NOLOCK)
