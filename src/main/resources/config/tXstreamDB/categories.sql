SELECT CASE WHEN Flow = 'I' THEN 'IN' ELSE 'OUT' END as flow,
       CategoryCode as code,
       Section as section,
       SubSection as subsection,
       Description as description,
       OldCodes as oldcodes
  FROM OR_Category WITH (NOLOCK)
 WHERE [Description] NOT LIKE 'Unknown%' AND [Version] = 3
