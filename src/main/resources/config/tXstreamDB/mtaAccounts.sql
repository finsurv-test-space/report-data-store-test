SELECT AccountNumber as accountNumber,
       MoneyTransferAgentID as MTA,
       RulingSection as rulingSection,
       ADLALevel as ADLALevel,
       CASE WHEN ADLAInd = 'Y' THEN 'true' ELSE 'false' END as isADLA
  FROM OR_MTAAccount WITH (NOLOCK)

