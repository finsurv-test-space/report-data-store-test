SELECT CurrencyCode as code,
       Name as name
  FROM OR_Currency WITH (NOLOCK)
 WHERE Name NOT LIKE 'Unknown%'
