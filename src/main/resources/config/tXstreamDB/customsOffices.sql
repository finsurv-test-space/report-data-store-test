SELECT OfficeCode as code,
       OfficeName as name
  FROM OR_CustomsOffice WITH (NOLOCK)
 WHERE OfficeName NOT LIKE 'Unknown%'
