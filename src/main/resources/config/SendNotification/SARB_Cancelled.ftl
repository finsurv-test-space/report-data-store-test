{
  "ReportSpace": "${util.escapeJavaScript(ReportSpace)}",
  "TrnReference": "${util.escapeJavaScript(Report.TrnReference)}",
  "Documents" : [
<#list Documents as doc>
    "${util.escapeJavaScript(doc.getDocumentHandle())}"<#if doc_has_next>,</#if>
</#list>
  ],
  "DocumentNames" : [
<#list Documents as doc>
    "${util.escapeJavaScript(doc.getFileName())}"<#if doc_has_next>,</#if>
</#list>
  ]
}
