package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by james on 2017/09/05.
 */

public class ReportInfo {
  private final String state;
  private final String reportSpace;
  private final String channelName;
  private final String schema;
  private final ReportData reportData;
  private final List<ValidationResponse> validations;
  private ReportEventContext eventContext;
  private ReportDownloadType downloadType;

  public ReportInfo(String state, ReportInfo report, ReportEventContext eventContext) {
    this.state = state;
    this.schema = report.schema;
    this.reportSpace = report.reportSpace;
    this.channelName = report.channelName;
    this.reportData = report.reportData;
    this.validations = report.validations;
    setEventContext(eventContext);
    this.downloadType = ReportDownloadType.Update;
  }

  public ReportInfo(String state, String schema, String reportSpace, String channelName,
                    ReportData reportData, List<ValidationResponse> validations, ReportEventContext eventContext) {
    this.state = state;
    this.schema = schema;
    this.reportSpace = reportSpace;
    this.channelName = channelName;
    this.reportData = reportData;
    this.validations = validations;
    setEventContext(eventContext);
    this.downloadType = ReportDownloadType.Update;
  }

  public ReportInfo(String state, String schema, String reportSpace, String channelName,
                    ReportData reportData, List<ValidationResponse> validations, ReportEventContext eventContext,
                    ReportDownloadType downloadType) {
    this.state = state;
    this.schema = schema;
    this.reportSpace = reportSpace;
    this.channelName = channelName;
    this.reportData = reportData;
    this.validations = validations;
    setEventContext(eventContext);
    this.downloadType = downloadType;
  }

  public List<ValidationResponse> getValidations() {
    return validations;
  }

  public String getState() {
    return state;
  }

  public String getSchema() {
    return schema;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public String getChannelName() {
    return channelName;
  }

  public ReportData getReportData() {
    return reportData;
  }

  public ReportDataWithDownloadType getReportDataWithDownloadType() {
    return new ReportDataWithDownloadType(reportData, downloadType);
  }

  public ReportEventContext getEventContext() {
    return eventContext;
  }

  @Deprecated
  public ReportDownloadType getDownloadType() {
    return downloadType;
  }

  @Deprecated
  public void setDownloadType(ReportDownloadType downloadType) {
    this.downloadType = downloadType;
  }
  
  public ReportDownloadType getReportDownloadType() {
    return downloadType;
  }

  public void setReportDownloadType(ReportDownloadType downloadType) {
    this.downloadType = downloadType;
  }

  public String getSystemValue(String name) {
    if (FieldConstant.State.equals(name))
      return state;
    if (FieldConstant.ReportSpace.equals(name))
      return reportSpace;
    if (FieldConstant.Schema.equals(name))
      return schema;
    if (FieldConstant.Channel.equals(name))
      return channelName;
    return null;
  }


  private void setEventContext(ReportEventContext eventContext) {
    if (eventContext != null) {
      this.eventContext = eventContext;
    } else {
      ReportServicesAuthentication user = null;
      try{
        user = SecurityHelper.getAuthMeta();
      } catch (Exception e){
      }
      if (user==null) {
//        this.eventContext = new ReportEventContext((String)null, LocalDateTime.now());
      } else {
        this.eventContext = new ReportEventContext(user, LocalDateTime.now());
      }
    }
  }
}