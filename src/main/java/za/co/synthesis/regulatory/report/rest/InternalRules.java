package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.PackageCache;
import za.co.synthesis.regulatory.report.support.RulesCache;

/**
 * This REST service provides access to all the evaluation, validation and lookup rules that are each consolidated into
 * a single file based on the configuration system configuration
 */
@Controller
@RequestMapping("/internal/producer/api/rules")
public class InternalRules {
  Logger logger = LoggerFactory.getLogger(InternalRules.class);
  @Autowired
  private RulesCache rulesCache;


  @RequestMapping(value = "/{packageName}/validation", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody
  String getInternalValidations(@PathVariable String packageName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalValidations()");
    return rulesCache.getValidationRules(packageName, PackageCache.CacheStrategy.CacheOrSource).getData();
  }

  @RequestMapping(value = "/{packageName}/document", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody
  String getInternalDocuments(@PathVariable String packageName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDocuments()");
    return rulesCache.getDocumentRules(packageName, PackageCache.CacheStrategy.CacheOrSource).getData();
  }

  @RequestMapping(value = "/{packageName}/evaluation", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getInternalEvaluations(@PathVariable String packageName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalEvaluations()");
    return rulesCache.getEvaluationRules(packageName, PackageCache.CacheStrategy.CacheOrSource).getData();
  }

  @RequestMapping(value = "/{packageName}/lookups", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getInternalLookups(@PathVariable String packageName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalLookups()");
    return rulesCache.getLookups(packageName, PackageCache.CacheStrategy.CacheOrSource).getData();
  }
}
