package za.co.synthesis.regulatory.report.security.jwt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthenticationCache {

  public enum CachedLogin {
    NoUserInCache,
    InvalidPassword,
    CacheCanBeUsed,
    DoLogin
  }

  public static class UserDetails
  {
    private String name;
    private String password;
    private long correctAt;
    private long checkedAt;
    private List<String> groups;

    public UserDetails(String name, String password, List<String> groups) {
      this.name = name;
      this.password = password;
      this.groups = groups;
    }

    public String getName() {
      return name;
    }

    public List<String> getGroups() {
      return groups;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public void setGroups(List<String> groups) {
      this.groups = groups;
    }

    public long getCheckedAt() {
      return checkedAt;
    }

    public void setCheckedAt(long checkedAt) {
      this.checkedAt = checkedAt;
    }

    public long getCorrectAt() {
      return correctAt;
    }

    public void setCorrectAt(long correctAt) {
      this.correctAt = correctAt;
    }
  }

  private final Map<String, UserDetails> userMap = new HashMap<String, UserDetails>();
  private int maxCacheSize = 1000;
  private long nocheckMillis = 30000; // 30 seconds
  private long expiryMillis = 7200000; // 2 hours

  public AuthenticationCache() {
  }

  public AuthenticationCache(int maxCacheSize) {
    this.maxCacheSize = maxCacheSize;
  }

  public AuthenticationCache(int maxCacheSize, long nocheckMillis, long expiryMillis) {
    this.maxCacheSize = maxCacheSize;
    this.nocheckMillis = nocheckMillis;
    this.expiryMillis = expiryMillis;
  }

  public void setMaxCacheSize(int maxCacheSize) {
    this.maxCacheSize = maxCacheSize;
  }

  public void setNocheckMillis(long nocheckMillis) {
    this.nocheckMillis = nocheckMillis;
  }

  public void setExpiryMillis(long expiryMillis) {
    this.expiryMillis = expiryMillis;
  }

  private void removeOldest() {
    long oldestMillis = -1;
    String oldestName = null;

    for (Map.Entry<String, UserDetails> entry : userMap.entrySet()) {
      long correctAt = entry.getValue().getCorrectAt();
      if (oldestMillis == -1 || correctAt <  oldestMillis) {
        oldestMillis = correctAt;
        oldestName = entry.getKey();
      }
    }
    userMap.remove(oldestName);
  }

  public synchronized void onAuthenticate(long millis, String name, String password, List<String> groups) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      user.setPassword(password);
      user.setGroups(groups);
      user.setCheckedAt(millis);
      user.setCorrectAt(millis);
    }
    else {
      while (userMap.size() > maxCacheSize) {
        removeOldest();
      }
      UserDetails user = new UserDetails(name, password, groups);
      user.setCheckedAt(millis);
      user.setCorrectAt(millis);
      userMap.put(name, user);
    }
  }

  public synchronized void onRefreshGroups(long millis, String name, List<String> groups) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      user.setGroups(groups);
      user.setCheckedAt(millis);
      user.setCorrectAt(millis);
    }
    else {
      while (userMap.size() > maxCacheSize) {
        removeOldest();
      }
      UserDetails user = new UserDetails(name, null, groups);
      user.setCheckedAt(millis);
      user.setCorrectAt(millis);
      userMap.put(name, user);
    }
  }

  public synchronized CachedLogin checkCacheWithPassword(long millis, String name, String password) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      if (user.getPassword() == null)
        return CachedLogin.NoUserInCache;

      if (password.equals(user.getPassword())) {
        if (millis - user.getCheckedAt() < nocheckMillis) {
          return CachedLogin.CacheCanBeUsed;
        }
        if (millis - user.getCorrectAt() < expiryMillis) {
          return CachedLogin.DoLogin;
        }
        else {
          userMap.remove(name);
          return CachedLogin.NoUserInCache;
        }
      }
      else {
        return CachedLogin.InvalidPassword;
      }
    }
    return CachedLogin.NoUserInCache;
  }

  public synchronized CachedLogin checkCacheNoPassword(long millis, String name) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      if (millis - user.getCheckedAt() < nocheckMillis) {
        return CachedLogin.CacheCanBeUsed;
      }
      if (millis - user.getCorrectAt() < expiryMillis) {
        return CachedLogin.DoLogin;
      }
      else {
        userMap.remove(name);
        return CachedLogin.NoUserInCache;
      }
    }
    return CachedLogin.NoUserInCache;
  }

  /**
   * Use this method when the checkCacheWithPassword method returns CachedLogin.CacheCanBeUsed
   * @param name
   * @return
   */
  public synchronized List<String> getUserGroups(String name) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      return user.getGroups();
    }
    return null;
  }

  /**
   * Use this method when the checkCacheWithPassword method returns CachedLogin.DoLDAPLogin and the LDAP service is not available
   * @param name
   * @return
   */
  public synchronized List<String> getUserGroupsAndResetCheckTime(long millis, String name) {
    if (userMap.containsKey(name)) {
      UserDetails user = userMap.get(name);
      user.setCheckedAt(millis);
      return user.getGroups();
    }
    return null;
  }
}
