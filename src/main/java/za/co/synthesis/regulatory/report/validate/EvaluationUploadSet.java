package za.co.synthesis.regulatory.report.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.rule.core.Evaluator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Created by jake on 6/13/17.
 */
public class EvaluationUploadSet implements Runnable {
  public static class EvaluationSet {
    private String trnReference;
    private Future<EvaluationDecisions> result;

    public EvaluationSet(String trnReference, Future<EvaluationDecisions> result) {
      this.trnReference = trnReference;
      this.result = result;
    }
  }
  private static final Logger logger = LoggerFactory.getLogger(EvaluationUploadSet.class);

  private final ExecutorService executorService;
  private final Channel channel;
  private final DataLogic dataLogic;

  private final String uploadReference;
  private final List<EvaluationSet> evaluationList = new ArrayList<EvaluationSet>();
  private boolean hasCompleted = false;

  private Authentication authentication;

  public EvaluationUploadSet(ExecutorService executorService, Channel channel, DataLogic dataLogic, Authentication authentication) {
    this.executorService = executorService;
    this.channel = channel;
    this.dataLogic = dataLogic;
    this.uploadReference = UUID.randomUUID().toString();
    this.authentication = authentication;
  }

  public String getUploadReference() {
    return uploadReference;
  }

  public List<EvaluationSet> getEvaluationList() {
    return evaluationList;
  }

  public void addDecision(Evaluator evaluator, String trnReference, EvaluationRequest evaluationRequest, Boolean relevantResultsOnly) {
    Future<EvaluationDecisions> evaluationResult =
        executorService.submit(
            new ReferencedEvaluationTask(evaluator, evaluationRequest, relevantResultsOnly)
        );
    EvaluationSet evaluationSet = new EvaluationSet(trnReference, evaluationResult);
    evaluationList.add(evaluationSet);
  }



  public void closeSet() {
    closeSet(false);
  }

  public void closeSet(boolean blocking) {
    executorService.submit(this);
    while (blocking && !hasCompleted) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        logger.error("Error while waiting for upload set to finish", e);
      }
    }
  }

  public UploadStatus getUploadStatus() {
    for (EvaluationSet evaluationSet : evaluationList) {
      if (!evaluationSet.result.isDone())
        return new UploadStatus(UploadStatus.Status.InProgess);
    }
    return new UploadStatus(UploadStatus.Status.Complete);
  }

  public List<EvaluationDecisions> getUploadResultList() throws ExecutionException, InterruptedException {
    List<EvaluationDecisions> result = new ArrayList<EvaluationDecisions>();
    for (EvaluationSet evaluationSet : evaluationList) {
      if (!evaluationSet.result.isDone())
        return null;
      result.add(evaluationSet.result.get());
    }
    return result;
  }

  @Override
  public void run() {
    hasCompleted = false;
    boolean isDone = false;
    while (!hasCompleted) {
      try {
        Thread.sleep(isDone?0:10);
      } catch (InterruptedException e) {
        logger.error("Error while waiting for upload set to finish", e);
      }
      if (!isDone) {
        for (EvaluationSet evaluationSet : evaluationList) {
          if (!evaluationSet.result.isDone()) {
            break;
          }
          isDone = true;
        }
      }
      if (isDone) {
        /*
        for (EvaluationSet evaluationSet : evaluationList) {
          FullyReferencedEvaluationDecisions evalResult = null;
          try {
            evalResult = evaluationSet.result.get();

            if (evalResult != null) {
              dataLogic.setEvaluationDecision(evalResult.getEvaluations().get(0).getEvaluation());
            }
          } catch (Exception e) {
            logger.error("Error while processing upload set", e);
          }
        }
        */
        // Persist stuff
        hasCompleted = true;
      }
    }
  }
}
