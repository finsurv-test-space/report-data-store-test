package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ChannelData;
import za.co.synthesis.regulatory.report.persist.ListData;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.ReportSpace;
import za.co.synthesis.regulatory.report.schema.State;
import za.co.synthesis.regulatory.report.schema.Transition;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("producer/api/system")
public class Configuration extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(Configuration.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    ApplicationContext ctx;
    
    /**
     * Return the version of this Report Data Store build.
     * @throws Exception
     */
    @RequestMapping(value = "/version", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    String getRDSVersion() throws Exception {
        return "{\n\t\"Version\":\""+systemInformation.getVersion(ctx)+"\"\n}";
    }

    /**
     * Return all the report spaces configured for this instance of Report Data Store
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getProducerListOfReportSpaces(HttpServletResponse response) throws Exception {
        return systemInformation.getReportSpaces();
    }

    /**
     * Return the configuration details for the specified report space
     * @param reportSpace the report space configuration to return (any of the report spaces configured for this instance)
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/details", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ReportSpace getProducerReportSpace(
            @PathVariable String reportSpace) throws Exception {
        return systemInformation.getReportSpace(reportSpace);
    }
    
    /**
     * The list of all channels available for the specified report space
     * @param reportSpace The reporting space for which the allowed channels should be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/channels", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getProducerChannelsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        return systemInformation.getReportSpaceChannels(reportSpace);
    }
    
    
    /**
     * The list of all possible report states, for a given reporting space configuration
     * @param reportSpace The reporting space configuration for which the possible (or allowed) states must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/states", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<State> getProducerStatesForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        return systemInformation.getReportSpaceStates(reportSpace);
    }
    
    
    /**
     * The list of all valid state transitions (including user actions) configured for the provided reporting space.
     * @param reportSpace The reporting space for which the full set of configured state transitions must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/transitions", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getProducerTransitionsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        return systemInformation.getReportSpaceTransitions(reportSpace);
    }

    
    //-------------------------------//
    //----- Channel Interfaces  -----//
    
    /**
     * The complete list of configured channel names for the given reporting space(s).
     * Reporting spaces can be comma separated if you intend to list channels for multiple spaces.
     * If no reporting space is provided, all configured channels, across all reporting spaces are returned.
     * @param reportSpace The reporting space (or spaces - comma separated) for which all configured channels must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getProducerChannels(
        @RequestParam(required = false) String reportSpace,
        HttpServletResponse response) throws Exception {
        
        if (reportSpace instanceof String) {
            if (reportSpace.matches("^.*[,].*$")) {
                List<String> channels = new ArrayList<String>();
                String[] reportSpaces = reportSpace.split("\\s*[,;]\\s*");
                for (int i=0; i<reportSpaces.length; i++) {
                    if (reportSpaces[i] instanceof String && reportSpaces[i].length() > 0) {
                        channels.addAll(systemInformation.getReportSpaceChannels(reportSpaces[i]));
                    }
                }
                return channels;
            } else {
                return systemInformation.getReportSpaceChannels(reportSpace);
            }
        }
        return systemInformation.getChannelNames();
    }

    @RequestMapping(value = "/allChannels", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<Channel> getChannels(
            @RequestParam(required = false) String reportSpace,
            HttpServletResponse response) throws Exception {
        List<Channel> channelList = new ArrayList<>();

        if (reportSpace instanceof String) {
            String[] reportSpaces = reportSpace.split("\\s*[,;]\\s*");
            for (int i = 0; i < reportSpaces.length; i++) {
                if (reportSpaces[i] instanceof String && reportSpaces[i].length() > 0) {

                    List<ChannelData> channels = systemInformation.getReportSpaceChannelData(reportSpaces[i]);
                    for (int j = 0; j < channels.size(); j++) {
                        channelList.add(new Channel(channels.get(j)));
                    }
                }
            }
        }
        return channelList;
    }
    
    /**
     * The list of all configured states that are applicable to the report space relating to the provided channel name.
     * @param channelName The channel name for which all possible, configured report states may be applied or expected.
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/states", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<State> getProducerStatesForChannel(
        @PathVariable String channelName,
        HttpServletResponse response) throws Exception {
        return systemInformation.getChannelStates(channelName);
    }
    
    
    /**
     * The list of all transitions that can be applied for a given channel name (and if provided, specified report state).
     * The transitions that will be returned are all those configured transitions (including user actions/events) belonging to the
     * related reporting space, where the channel name exists in the allowed channels list or where the allowed channels list is empty or null,
     * and if the state is specified - where the current state list for the respective transitions are empty or null or contain the state specified.
     * @param channelName The channel to/in which the relevant transitions may be applied
     * @param state (optional) The current state for which the transitions may be applied
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/transitions", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getProducerTransitionsForChannel(
        @PathVariable String channelName,
        @RequestParam (required=false) String state,
        HttpServletResponse response) throws Exception {
        return systemInformation.getChannelTransitionsForState(channelName, state);
    }
    
    /**
     * The list of all user events which can be applied for the given channel, and if provided - for the current report state stipulated.
     * The transitions that will be returned are all those configured transitions which are user actions/events, belonging to the
     * reporting space related to the specified channel name, where the channel name exists in the allowed channels list or where the allowed channels list is empty or null,
     * ...and where if the state is specified - the current state list for the respective transitions are empty or null or contain the state specified.
     * @param channelName The channel to/in which the relevant user actions/events may be applied
     * @param state (optional) The current state for which the user actions/events may be applied
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/user_events", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getProducerUserActionsForChannel(
        @PathVariable String channelName,
        @RequestParam (required=false) String state,
        HttpServletResponse response) throws Exception {
        return systemInformation.getChannelUserActionsForState(channelName, state);
    }
    
    /**
     * Returns the default schema for the channel specified - if one is configured.
     * @param channelName The channel for which the default schema is required
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/default_schema", method = RequestMethod.GET, produces = "application/text")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String getProducerDefaultSchemaForChannel(
        @PathVariable String channelName,
        HttpServletResponse response) throws Exception {
        Channel channel = systemInformation.getChannel(channelName);
        if (channel == null) {
            throw new PreconditionFailedException("A valid channel name must be provided");
        }
        String defaultSchema = channel.getDefaultSchema();
        return defaultSchema;
    }
    
    //-------------------------------//
    
    
    /**
     * The list of all configured report schemas for the current instance
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/schemas", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getProducerReportSchemas(
        HttpServletResponse response) throws Exception {
        return systemInformation.getConfigStore().getTypeSchemas("report_core");
    }
    
    
    /**
     * The list of all configured user actions/events for the current instance
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/user_events", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getProducerUserEvents(
//            @RequestParam(required = true) String reportSpace,
        HttpServletResponse response) throws Exception {
        return systemInformation.getUserEventNames();
    }
    
    
    /**
     * An inventory of all configured report lists for the current instance
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/lists", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getProducerReportLists() throws Exception {
        return systemInformation.getListNames();
    }
    
    
    /**
     * An inventory of all configured report lists for the current instance, where:
     * 1) "listingExclusionFilter" does not contain the supplied filter string (Black listing)
     * 2) "listingFilter" does contain the supplied filter string (White listing)
     * 3) exclusiveFiltering param is false or null and the "listingFilter" is empty (Inclusion)
     * ...OR...
     * 4) exclusiveFiltering param is true and the "listingFilter" is empty and the supplied filter string is empty or null (Exclusively where filter is empty)
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/filtered_lists", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getProducerReportLists(
            @RequestParam(required = true) String filter,
            @RequestParam(required = false) Boolean exclusiveFiltering
    ) throws Exception {
        exclusiveFiltering = exclusiveFiltering!=null?exclusiveFiltering:false;
        return systemInformation.getListNames(filter!=null?filter:(exclusiveFiltering?"":filter), exclusiveFiltering);
    }
    
    
    /**
     * the full list definition/configuration for the specified list name
     * @param listName the name of the list for which the configured list definition should be returned
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/list/{listName}", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    ListData getProducerReportList(
            @PathVariable String listName
        ) throws Exception {
        return systemInformation.getListDataByName(listName);
    }

    @RequestMapping(value = "/report/list/complete", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    Map<String, ListData> getProducerReportList(
    ) throws Exception {
        return systemInformation.getAllListDetails();
    }
}
