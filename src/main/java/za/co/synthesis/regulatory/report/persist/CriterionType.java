package za.co.synthesis.regulatory.report.persist;

/**
 * Created by jake on 6/27/17.
 */
public enum CriterionType {
  Meta("Meta"),
  Report("Report"),
  System("System");

  private final String text;

  /**
   * @param text
   */
  private CriterionType(final String text) {
    this.text = text;
  }

  /* (non-Javadoc)
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
    return text;
  }

}
