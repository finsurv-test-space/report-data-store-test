package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.ValidateResponse;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.validate.SBValidate_ReversalTrnRef;
import za.co.synthesis.regulatory.report.validate.ValidationRunner;
import za.co.synthesis.rule.core.StatusType;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * User: jake
 * Date: 5/3/16
 * Time: 9:14 AM
 * A collection of externalised Validate_xxx calls that will be used by a client application to determine validity
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/validation")
public class ExternalValidate extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(ExternalValidate.class);

  @Autowired
  private ValidationRunner validationRunner;

  //TODO: Create dynamicList API
//  @RequestMapping(value = "/dynamicList", method = RequestMethod.GET, produces = "application/json")
//  public @ResponseBody
//  JSObject getDynamicList(HttpServletRequest request) throws Exception {
//    //return dynamicValidationRequestCall(validationName, request);
//
//    //  THIS SHOULD RETRIEVE THE LIST OF DYNAMIC VALIDATIONS AVAILABLE AS WELL AS THEIR MANDATORY AND OPTIONAL PARAMS
//    new JSObject
//  }


  @RequestMapping(value = "/dynamic/{validationName}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_dynamic(
          @PathVariable String validationName,
          HttpServletRequest request) throws Exception {
    return dynamicValidationRequestCall(validationName, request);
  }


  /**
   * This is a generic / dynamic implementation of the external validation calls.
   * It will automatically import all headers and request params from the HTTP Request and populate the validation params Map therewith.
   *
   * @param validationName
   * @return
   * @throws Exception
   */
  public ValidateResponse dynamicValidationRequestCall(String validationName, HttpServletRequest request) throws Exception{
    if (validationName != null) {
      if (request == null) {
        ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        request = servletRequestAttributes != null ? servletRequestAttributes.getRequest() : null;
      }

      if (request != null) {
        Map<String, Object> parameters = new HashMap<String, Object>();

        //HTTP Headers
        for (String header : Collections.list(request.getHeaderNames())) {
          parameters.put(header, request.getHeader(header));
        }
        //HTTP Params
        for (String param : Collections.list(request.getParameterNames())) {
          parameters.put(param, request.getParameter(param));
        }


        if (parameters.get("channelName") instanceof String) {
          String channelName = (String) parameters.get("channelName");

          //TODO: we should check for CONFIGURED, mandatory parameters and if not supplied, then throw PreconditionFailedException.

          return new ValidateResponse(validationRunner.runValidation(channelName, validationName, parameters));
        } else {
          throw new PreconditionFailedException("Parameter 'channelName' not supplied");
        }
      }
      throw new Exception("Unable to obtain HTTP request object for parsing");
    }
    throw new PreconditionFailedException("No validation name specified.");
  }


  @Deprecated
  @RequestMapping(value = "/importUndertakingCCN", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_importUndertakingCCN(
          @RequestParam String channelName,
          @RequestParam String CCN,
          @RequestParam(required = false) String valueDate) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("CCN", CCN);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "importUndertakingCCN", parameters));
  }

  @Deprecated
  @RequestMapping(value = "/loanRef", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_loanRef(
          @RequestParam String channelName,
          @RequestParam String loanRef,
          @RequestParam(required = false) String valueDate) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("loanRef", loanRef);
    parameters.put("valueDate", valueDate);

    return new ValidateResponse(validationRunner.runValidation(channelName, "loanRef", parameters));
  }
  @Deprecated
  @RequestMapping(value = "/replacementTrnReference", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_replacementTrnReference(
          @RequestParam String channelName,
          @RequestParam(required = false) String replacementTrnReference) throws Exception {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("replacementTrnReference", replacementTrnReference);

    return new ValidateResponse(validationRunner.runValidation(channelName, "replacementTrnReference", parameters));
  }
  @Deprecated
  @RequestMapping(value = "/reversalTrnRef", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_reversalTrnRef(
          @RequestParam String channelName,
          @RequestParam String trnReference,
          @RequestParam String flow,
          @RequestParam String sequence,
          @RequestParam(required=false) String flowCurrency,
          @RequestParam(required=false) String foreignValue,
          @RequestParam String category) throws Exception {
    Map<String, Object> parameters = new HashMap<String, Object>();
    //parameters.put("trnReference", trnReference);  //DEPRECATED/OPTIONAL param
    parameters.put("trnRef", trnReference);
    parameters.put("flow", flow);
    //parameters.put("sequence", sequence);  //DEPRECATED/OPTIONAL param
    parameters.put("reversalTrnSeq", sequence);
    parameters.put("category", category);
    parameters.put("flowCurrency", flowCurrency); //OPTIONAL param
    parameters.put("foreignValue", foreignValue); //OPTIONAL param
    return new ValidateResponse(validationRunner.runValidation(channelName, "reversalTrnRef", parameters));
  }
  @Deprecated
  @RequestMapping(value = "/validCCN", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_validCCN(
          @RequestParam String channelName,
          @RequestParam String CCN,
          @RequestParam(required = false) String valueDate) throws Exception {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("CCN", CCN);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "validCCN", parameters));
  }

  @Deprecated
  @RequestMapping(value = "/validCCNinUCR", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_validCCNinUCR(
          @RequestParam String channelName,
          @RequestParam String UCR,
          @RequestParam(required = false) String valueDate) throws Exception {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("UCR", UCR);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "validCCNinUCR", parameters));
  }

  @Deprecated
  @RequestMapping(value = "/withinSDALimit", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_individualSDA(
          @RequestParam String channelName,
          @RequestParam String localValue,
          @RequestParam String trnReference,
          @RequestParam String valueDate,
          @RequestParam(required = false) String residentIDNumber,
          @RequestParam(required = false) String thirdPartyIDNumber) throws Exception {

    if (residentIDNumber == null && thirdPartyIDNumber == null)
      throw new PreconditionFailedException("A Resident IDNumber or a ThirdParty IDNumber must be specified");

    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("localValue", localValue);
    parameters.put("trnRef", trnReference);
    parameters.put("valueDate", valueDate);
    parameters.put("resIDNumber", residentIDNumber);
    parameters.put("tpIDNumber", thirdPartyIDNumber);
    return new ValidateResponse(validationRunner.runValidation(channelName, "withinSDALimit", parameters));
  }

  @Deprecated
  @RequestMapping(value = "/withinFIALimit", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_individualFIA(
          @RequestParam String channelName,
          @RequestParam String localValue,
          @RequestParam String trnReference,
          @RequestParam String valueDate,
          @RequestParam(required = false) String residentIDNumber,
          @RequestParam(required = false) String thirdPartyIDNumber) throws Exception {

    if (residentIDNumber == null && thirdPartyIDNumber == null)
      throw new PreconditionFailedException("A Resident IDNumber or a ThirdParty IDNumber must be specified");

    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("localValue", localValue);
    parameters.put("trnRef", trnReference);
    parameters.put("valueDate", valueDate);
    parameters.put("resIDNumber", residentIDNumber);
    parameters.put("tpIDNumber", thirdPartyIDNumber);
    return new ValidateResponse(validationRunner.runValidation(channelName, "withinFIALimit", parameters));
  }
  @Deprecated
  @RequestMapping(value = "/validMRNonIVS", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse validate_validMRNonIVS(
          @RequestParam String channelName,
          @RequestParam String customsClientNumber,
          @RequestParam String transportDocumentNumber,
          @RequestParam String importControlNumber) throws Exception {
    return new ValidateResponse(StatusType.Pass);
  }

  //TODO: Factor out the JDBC dependency on this level and reinstate this.
//  @RequestMapping(value = "/sbvalidReversalTrnRef", method = RequestMethod.GET, produces = "application/json")
//  public @ResponseBody
//  ValidateResponse sbvalidate_ReversalTrnRef(
//          @RequestParam String channelName,
//          @RequestParam String reversalTrn,
//          @RequestParam String reversalTrnSeqNumber,
//          @RequestParam String flowCurrency,
//          @RequestParam String foreignValue) throws Exception {
//    SBValidate_ReversalTrnRef validate = new SBValidate_ReversalTrnRef(jdbcTemplate);
//
//    return new ValidateResponse(validate.call(reversalTrn, flowCurrency, reversalTrnSeqNumber, foreignValue));
//  }
}
