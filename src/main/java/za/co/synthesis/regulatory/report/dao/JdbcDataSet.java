package za.co.synthesis.regulatory.report.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import java.sql.Date;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 3/22/17.
 */
public class JdbcDataSet {
  private static final Logger logger = LoggerFactory.getLogger(JdbcDataSet.class);

  private final String code;
  private final String masterSrcCode;
  private final String select;
  private final String transaction_link;
  private final String money_link;
  private final String master_transaction_link;
  private final String master_money_link;
  private final String transaction_field;
  private final String money_field;
  private final String master_transaction_field;
  private final String master_money_field;
  private final String order_by;
  private final String unique_key;
  private final boolean enforce_unique_key;
  private final List<Map<String, String>> dataBuffer = new ArrayList<Map<String, String>>();
  private final DateTimeFormatter jsTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
  private final DateTimeFormatter jsDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private boolean isDataBufferLoaded = false;
  private String last_unique_key = null;

  private static String stripPrefix(String name) {
    if (name != null) {
      int posDot = name.indexOf('.');
      if (posDot > -1) {
        return name.substring(posDot+1);
      }
    }
    return name;
  }

  JdbcDataSet(String code, String masterSrcCode, String select, String transaction_link, String money_link,
              String master_transaction_link, String master_money_link, String order_by,
              String unique_key, boolean enforce_unique_key) {
    this.code = code;
    this.masterSrcCode = masterSrcCode;
    this.select = select;

    if (transaction_link != null && transaction_link.startsWith("Match:")) {
      int posArrow = transaction_link.indexOf("=>");
      if (posArrow != -1) {
        String matchingPath = transaction_link.substring(6, posArrow).trim();
        try {
          transaction_link = transaction_link.substring(posArrow+2).trim();
        } catch (Exception e) {
          logger.error("Cannot load matching engine for: " + transaction_link, e);
        }
      }
    }
    this.transaction_link = transaction_link;
    this.money_link = money_link;
    this.master_transaction_link = master_transaction_link;
    this.master_money_link = master_money_link;
    this.transaction_field = stripPrefix(transaction_link);
    this.money_field = stripPrefix(money_link);
    this.master_transaction_field = stripPrefix(master_transaction_link);
    this.master_money_field = stripPrefix(master_money_link);
    this.order_by = order_by;
    this.unique_key = unique_key;
    this.enforce_unique_key = enforce_unique_key;
  }

  public void bufferResults(SqlRowSet rs) {
    isDataBufferLoaded = true;
    try {
      SqlRowSetMetaData srsmd = rs.getMetaData();
      int columnCount = srsmd.getColumnCount();

      while(rs.next()){
        Map<String, String> rowMap = new HashMap<String, String>();

        for (int i=1; i<=columnCount; i++) {
          String colName = srsmd.getColumnName(i);
          int colType = srsmd.getColumnType(i);
          String value;
          if (colType == Types.TIMESTAMP) {
            Object objVal = rs.getObject(i);
            if (objVal != null) {
              Timestamp datetimeValue = rs.getTimestamp(i);
              LocalDateTime dt = datetimeValue.toLocalDateTime();
              value = dt.format(jsTimeFormatter);
            }
            else
              value = null;
          }
          else
          if (colType == Types.DATE) {
            Object objVal = rs.getObject(i);
            if (objVal != null) {
              Date dateValue = rs.getDate(i);
              LocalDate dt = dateValue.toLocalDate();
              value = dt.atStartOfDay().format(jsDateFormatter);
            }
            else
              value = null;
          }
          else {
            value = rs.getString(i);
          }
          if (!rowMap.containsKey(colName))
            rowMap.put(colName, value);
        }
        if (enforce_unique_key) {
          String this_unique_key = rowMap.get(unique_key);
          if (last_unique_key == null || !last_unique_key.equals(this_unique_key)) {
            last_unique_key = this_unique_key;
            dataBuffer.add(rowMap);
          }
          else {
            logger.info("Enforce Unique Key " + unique_key + ": " + last_unique_key + " removed as a duplicate");
          }
        }
        else
          dataBuffer.add(rowMap);
      }
    } catch (Exception e) {
      logger.error("Exception loading data queue buffer",e);
    }
  }

  public boolean isDataBufferLoaded() {
    return isDataBufferLoaded;
  }

  public void clear() {
    isDataBufferLoaded = false;
    dataBuffer.clear();
  }

  String getCode() {
    return code;
  }

  String getMasterSrcCode() {
    return masterSrcCode;
  }

  String getSelect() {
    return select;
  }

  String getTransaction_link() {
    return transaction_link;
  }

  String getMoney_link() {
    return money_link;
  }

  String getTransaction_field() {
    return transaction_field;
  }

  String getMoney_field() {
    return money_field;
  }

  public String getMaster_transaction_field() {
    return master_transaction_field;
  }

  public String getMaster_money_field() {
    return master_money_field;
  }

  String getOrder_by() {
    return order_by;
  }

  public int getResultCount() {
    return dataBuffer.size();
  }

  public List<Map<String, String>> getDataBuffer() {
    return dataBuffer;
  }

  public void filterOutRowsByMissingField(String fieldName) {
    List<Map<String, String>> removeRows = new ArrayList<Map<String, String>>();
    for (Map<String, String> row : dataBuffer) {
      if (row.get(fieldName) == null) {
        removeRows.add(row);
      }
    }
    dataBuffer.removeAll(removeRows);
  }

  public Map<String, String> getResult(int index) {
    return dataBuffer.get(index);
  }

  public List<Map<String, String>> getFocusedLevel1Results(String transactionValue) {
    List<Map<String, String>> focusBuffer = new ArrayList<Map<String, String>>();
    for (Map<String, String> entry : dataBuffer) {
      String tranField = entry.get(transaction_field);
      if (tranField != null && tranField.equals(transactionValue)) {
        focusBuffer.add(entry);
      }
    }
    return focusBuffer;
  }

  public List<List<Map<String, String>>> getFocusedLevel2Results(JdbcDataSet level1Dataset, String transactionValue) {
    List<String> moneyOrder = new ArrayList<String>();
    List<String> moneyRefs = new ArrayList<String>(); // A list of money references to match against

    for (Map<String, String> level1Entry : level1Dataset.dataBuffer) {
      String tranValue = level1Entry.get(level1Dataset.getTransaction_field());
      if (tranValue != null && tranValue.equals(transactionValue)) {
        String moneyRef = level1Entry.get(master_transaction_field);
        if (!moneyRefs.contains(moneyRef)) {
          moneyRefs.add(moneyRef);
        }
        String moneyValue = level1Entry.get(master_money_field);
        if (!moneyOrder.contains(moneyValue))
          moneyOrder.add(moneyValue);
      }
    }

    Map<String, List<Map<String, String>>> moneyBuffer = new HashMap<String, List<Map<String, String>>>();
    for (Map<String, String> entry : dataBuffer) {
      if (moneyRefs.contains(entry.get(transaction_field))) {
        String moneyValue = entry.get(money_field);
        List<Map<String, String>> focusBuffer;
        if (moneyBuffer.containsKey(moneyValue)) {
          focusBuffer = moneyBuffer.get(moneyValue);
        }
        else {
          focusBuffer = new ArrayList<Map<String, String>>();
          moneyBuffer.put(moneyValue, focusBuffer);
        }
        focusBuffer.add(entry);
      }
    }
    List<List<Map<String, String>>> result = new ArrayList<List<Map<String, String>>>();
    for (String money : moneyOrder) {
      result.add(moneyBuffer.get(money));
    }
    return result;
  }
}
