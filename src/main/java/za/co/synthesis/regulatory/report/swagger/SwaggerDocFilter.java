package za.co.synthesis.regulatory.report.swagger;

import org.springframework.security.core.Authentication;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.TypeLogic;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 6/4/17.
 */
public class SwaggerDocFilter implements Filter {
  private static class ByteArrayServletStream extends ServletOutputStream {
    ByteArrayOutputStream baos;
    WriteListener writeListener;

    ByteArrayServletStream(ByteArrayOutputStream baos) {
      this.baos = baos;
    }

    public void write(int param) throws IOException {
      baos.write(param);
    }

    @Override
    public boolean isReady() {
      return true;
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {
      this.writeListener = writeListener;
    }
  }

  private static class ByteArrayPrintWriter {
    private ByteArrayOutputStream baos = new ByteArrayOutputStream();
    private PrintWriter pw = new PrintWriter(baos);
    private ServletOutputStream sos = new ByteArrayServletStream(baos);

    public PrintWriter getWriter() {
      return pw;
    }

    public ServletOutputStream getStream() {
      return sos;
    }

    byte[] toByteArray() {
      return baos.toByteArray();
    }
  }

  public class CharResponseWrapper extends HttpServletResponseWrapper {
    private ByteArrayPrintWriter output;
    private boolean usingWriter;

    public CharResponseWrapper(HttpServletResponse response) {
      super(response);
      usingWriter = false;
      output = new ByteArrayPrintWriter();
    }

    public byte[] getByteArray() {
      return output.toByteArray();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
      // will error out, if in use
      if (usingWriter) {
        super.getOutputStream();
      }
      usingWriter = true;
      return output.getStream();
    }

    @Override
    public PrintWriter getWriter() throws IOException {
      // will error out, if in use
      if (usingWriter) {
        super.getWriter();
      }
      usingWriter = true;
      return output.getWriter();
    }

    public String toString() {
      return output.toString();
    }
  }


  private UserInformation userInformation;
  private Map<String, JavaDocPath> javaDocPathMap = null;

  public TypeLogic typeLogic;


  @Override
  public void destroy() {
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    WebApplicationContext springContext =
        WebApplicationContextUtils.getWebApplicationContext(filterConfig.getServletContext());
    userInformation = springContext.getBean(UserInformation.class);
    typeLogic = springContext.getBean(TypeLogic.class);

    try {
      JSArray doc = JSReaderUtil.loadResourceAsJSArray(springContext, "classpath:/docs/javadocs.json");
      javaDocPathMap = JavaDocPath.parseJavaDocs(doc);
    } catch (Exception e) {
      e.printStackTrace();
    }
    ;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    CharResponseWrapper wrappedResponse = new CharResponseWrapper((HttpServletResponse) response);

    chain.doFilter(request, wrappedResponse);

    Reader swaggerDoc = new InputStreamReader(new ByteArrayInputStream(wrappedResponse.getByteArray()));

    AugmentSwagger merger = new AugmentSwagger();

    boolean bProducerApi = false;
    boolean bConsumerApi = false;
    boolean bInternalApi = false;

    List<String> rights = null;

    Object authToken = request.getAttribute(SecurityHelper.REQUEST_AUTH_TOKEN);
    if (authToken instanceof Authentication) {
      rights = SecurityHelper.convertGrantedAuthoritiesToRights(((Authentication) authToken).getAuthorities());
    }

    if (rights != null) {
      if (rights.contains("PRODUCER_API"))
        bProducerApi = true;
      if (rights.contains("CONSUMER_API"))
        bConsumerApi = true;
      if (rights.contains("INTERNAL_API"))
        bInternalApi = true;
    }

    try {
      merger.load(swaggerDoc);

      // Set the Swagger
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      String referer = httpRequest.getHeader("Referer");
      if (referer != null) {
        URL swaggerUrl = new URL(referer);
        if(swaggerUrl.getPort() > 0) {
          merger.setSwaggerHost(swaggerUrl.getHost() + ":" + swaggerUrl.getPort());
        } else {
          merger.setSwaggerHost(swaggerUrl.getHost());
        }
        if (swaggerUrl.getPath().endsWith("swagger-ui.html")) {
          String basePath = swaggerUrl.getPath().substring(0, swaggerUrl.getPath().length() - 16);
          merger.setSwaggerBasePath(basePath);
        }
      }

      Object subsSchemas = httpRequest.getParameter("substitute-schemas");
      boolean substituteSchemaNamesForSwagger = subsSchemas==null?false:true;//(subsSchemas instanceof String?subsSchemas.toString().equalsIgnoreCase("true"):false);

      if (!bProducerApi) {
        //merger.stripPaths("/producer/api");
        //--- WE WANT TO RETAIN "/producer/api/js [GET]" AND "/producer/api/rules/... [GET]"
        merger.stripPaths("/producer/api/validation");
        merger.stripPaths("/producer/api/system");
        merger.stripPaths("/producer/api/decision");
        merger.stripPaths("/producer/api/document");
        merger.stripPaths("/producer/api/form");
        merger.stripPaths("/producer/api/js");
        merger.stripPaths("/producer/api/submit/");
        merger.stripPaths("/producer/api/lookup/");
        merger.stripPaths("/producer/api/report/");
        merger.stripPaths("/producer/api/upload/");
        merger.stripPaths("/producer/api/accountEntry");
        merger.stripPaths("/producer/api/checkSum");
        //remove the POST options for the /producer/api paths...
        merger.stripPostPaths("/producer/api");
      }
      if (!bConsumerApi) {
        merger.stripPaths("/consumer/api");
      }
      if (!bInternalApi) {
        merger.stripPaths("/internal/api");
        merger.stripPaths("/internal/producer");
        merger.stripPaths("/internal/consumer");
        merger.stripPaths("/internal/form");
      }

      merger.enrichSwaggerDocs(javaDocPathMap);

      Map<String, Map<String, String>> schemaTypeMap = new HashMap<String, Map<String, String>>();

      JSObject sarb = typeLogic.getType("report", "sarb");
      Map<String, String> sarbTypes = SwaggerUtils.namespaceTypes(sarb, "Sarb_");
      schemaTypeMap.put("sarb", sarbTypes);
      merger.mergeDefinitions(sarb);

      /*
      JSObject sadc = typeLogic.getType("report", "sadc");
      Map<String, String> sadcTypes = SwaggerUtils.namespaceTypes(sadc, "Sadc_");
      schemaTypeMap.put("sadc", sadcTypes);
      merger.mergeDefinitions(sadc);

      JSObject bon = typeLogic.getType("report", "bon");
      Map<String, String> bonTypes = SwaggerUtils.namespaceTypes(bon, "Bon_");
      schemaTypeMap.put("bon", bonTypes);
      merger.mergeDefinitions(bon);

      JSObject rbm = typeLogic.getType("report", "rbm");
      Map<String, String> rbmTypes = SwaggerUtils.namespaceTypes(rbm, "Rbm_");
      schemaTypeMap.put("rbm", rbmTypes);
      merger.mergeDefinitions(rbm);
      */

      JSObject genv3 = typeLogic.getType("report", "genv3");
      Map<String, String> genv3Types = SwaggerUtils.namespaceTypes(genv3, "Genv3_");
      schemaTypeMap.put("genv3", genv3Types);
      merger.mergeDefinitions(genv3);

      JSObject error = typeLogic.getType("error", null);
      merger.mergeDefinitions(error);

//      JSObject user = configStore.getType("user", null);
//      merger.mergeDefinitions(user);

      //TODO: drive this from configs / parameters
      if (substituteSchemaNamesForSwagger) {
        merger.extendCallBySchema("/producer/api/report/validate/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/report/{schema}", "get", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/report/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/report/data/{schema}", "get", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/report/data/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/report/decision/data/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/async/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/async/data/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/async/data/obj/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/sync/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/sync/data/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/producer/api/upload/reports/sync/data/obj/{schema}", "post", schemaTypeMap);
        merger.extendCallBySchema("/consumer/api/download/reports/{schema}", "get", schemaTypeMap);
      }
    } catch (Exception e) {
      throw new ServletException("Cannot merge Swagger docs", e);
    }

    response.getOutputStream().write(merger.toString().getBytes());
  }
}