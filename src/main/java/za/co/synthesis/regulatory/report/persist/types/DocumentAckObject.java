package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.DocumentData;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;

import javax.persistence.Entity;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */
@Entity
public class DocumentAckObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(DocumentAckObject.class);

  private String documentHandle;
  private String reportSpace;
  private String reportKey;
  private boolean acknowledged;
  private String acknowledgedComment;
  private Map<String, Object> eventContext;


  public DocumentAckObject(String uuid) {
    super(uuid);
  }

  public DocumentAckObject(String uuid, DocumentData doc, ReportEventContext eventContext) {
    super(uuid);
    setDocumentData(doc);
  }

  public String getDocumentHandle() {
    return documentHandle;
  }

  public void setDocumentHandle(String documentHandle){
    this.documentHandle = documentHandle;
  }

  public String getReportSpace() {
    return this.reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getReportKey(){
    return reportKey;
  }

  public void setReportKey(String reportKey){
    this.reportKey = reportKey;
  }

  public String getAcknowledged(){
    return this.acknowledged?"true":"false";
  }

  public void setAcknowledged(String ack){
    this.acknowledged = "true".equalsIgnoreCase(ack);
  }

  public String getAcknowledgedComment(){
    return this.acknowledgedComment;
  }

  public void setAcknowledgedComment(String comment){
    this.acknowledgedComment = comment;
  }

  public String getContent(){
    return null;
  }

  public void setContent(String contentJson){
  }

  public String getAuthToken() {
    if (eventContext == null)
      eventContext = composeEventContext();
    if (eventContext != null)
      return mapToJsonStr(eventContext);
    else
      return null;
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public DocumentData getDocumentData() { //This is a bit out of place - we're bleeding logic layer into persistence layer.
    return DataLogic.getDocumentDataObjFromHandle(reportSpace, documentHandle);
  }


  public void setDocumentData(DocumentData doc) {
    if (doc != null) {
      this.documentHandle = doc.getDocumentHandle();
      this.reportSpace = doc.getReportSpace();
      this.reportKey = doc.getTrnReference();
      this.acknowledged = doc.getAcknowledged();
      this.acknowledgedComment = doc.getAcknowledgedComment();
    }
  }

  public boolean isAcknowledged() {

    return acknowledged;
  }
}
