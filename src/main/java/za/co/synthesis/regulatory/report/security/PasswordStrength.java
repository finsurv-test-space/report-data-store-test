package za.co.synthesis.regulatory.report.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordStrength implements IPasswordStrength {
    private int minimumPasswordLength;
    private List<String> regexValidations;
    private int regexValidationCount;
    private String passwordStrengthRequirements;
    static final Logger logger = LoggerFactory.getLogger(DataLogic.class);


    @Override
    public int getMinimumPasswordLength(){
        return this.minimumPasswordLength;
    }

    @Override
    public boolean getPasswordStrength(String password){
        boolean passwordStrength = false;
        int tokenCount = 0;

        for(String regex:regexValidations){
            Pattern pattern;
            Matcher matcher = null;
            try{
            pattern = Pattern.compile(regex);
            matcher = pattern.matcher(password);
            }catch (Exception e){
                logger.error("Regex pattern does not compile " + e.toString());
            }
            if(matcher != null && matcher.find()){
                tokenCount++;
            }
        }
        if(tokenCount>=this.regexValidationCount && password.length()>=this.minimumPasswordLength){
            passwordStrength = true;
        }
        return passwordStrength;
    }

    @Override
    public List<String> getRegexValidations(){return this.regexValidations;}
    @Override
    public int getRegexValidationCount(){return this.regexValidationCount;}
    @Override
    public void setRegexValidations(List<String> validations){this.regexValidations = validations;}
    @Override
    public void setRegexValidationCount(int regexValidationCount){this.regexValidationCount= regexValidationCount;}

    @Override
    public void setMinimumPasswordLength(int passwordLength){
        this.minimumPasswordLength = passwordLength;
    }

    @Override
    public String getPasswordStrengthRequirements() {
        return this.passwordStrengthRequirements;
    }

    @Override
    public void setPasswordStrengthRequirements(String newRequirements) {
        this.passwordStrengthRequirements = newRequirements;
    }
}
