package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 5/30/17.
 */
@JsonPropertyOrder({
        "Code"
        , "Name"
})
public class Currency {
  private String code;
  private String name;

  public Currency(Object code, Object name) {
    this.code = (String)code;
    this.name = (String)name;
  }

  @JsonProperty("Code")
  public String getCode() {
    return code;
  }

  @JsonProperty("Name")
  public String getName() {
    return name;
  }
}
