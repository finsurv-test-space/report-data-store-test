package za.co.synthesis.regulatory.report.security.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static za.co.synthesis.regulatory.report.security.SecurityHelper.logoutActiveSessions;

public class JWTAuthFilter extends OncePerRequestFilter {
//  private RememberMeServices rememberMeServices = new NullRememberMeServices();

  private SystemInformation systemInformation;
  private UserInformation userInformation;
  private String allowedExternalSubjects = "";
  private String defaultTemplateUser = "producer";
  public static boolean useFileAndUrlFriendlyBase64 = true;

  public static boolean getUseFileAndUrlFriendlyBase64() {
    return useFileAndUrlFriendlyBase64;
  }

  public static void setUseFileAndUrlFriendlyBase64(boolean useFileAndUrlFriendlyBase64) {
    JWTProvider.useFileAndUrlFriendlyBase64 = useFileAndUrlFriendlyBase64;
  }

  public UserInformation getUserInformation() {
    return userInformation;
  }

  public void setUserInformation(UserInformation userInformation) {
    this.userInformation = userInformation;
    if (userInformation == null && systemInformation != null && systemInformation.getUserInformation() != null){
      this.userInformation = systemInformation.getUserInformation();
    }
  }

  public JWTAuthFilter() {
  }

  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  private String extractJWTAuthorization(HttpServletRequest request) {
    String header = request.getHeader("Authorization");

    if (header != null && header.startsWith("Bearer ")) {
      return header.substring(7);
    }
    return null;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request,
                                  HttpServletResponse response, FilterChain chain)
          throws IOException, ServletException {
    final boolean debug = this.logger.isDebugEnabled();
    //request.getReader().lines().toArray()
    Authentication currentAuthentication = SecurityContextHolder.getContext().getAuthentication();
    if (currentAuthentication == null) {
      String jwtTokenAuth = extractJWTAuthorization(request);
      String jwtTokenUrl = request.getParameter("access_token");
      String jwtTokenCookie = null;
      Cookie[] cookies = request.getCookies();
      if (cookies != null) {
        for (Cookie cookie : cookies) {
          if (cookie.getName().equals("access_token")) {
            jwtTokenCookie = cookie.getValue();
            break;
          }
        }
      }

      
      String jwtToken = (jwtTokenAuth != null) ? jwtTokenAuth : (jwtTokenCookie != null ? jwtTokenCookie : jwtTokenUrl);
      if (jwtToken != null) {
        logoutActiveSessions(request, response);

        try {
          //resolve potential race condition when creating the singletons...
          if (userInformation == null && systemInformation != null && systemInformation.getUserInformation() != null){
            this.userInformation = systemInformation.getUserInformation();
          }

          Authentication authentication = JWTConsumer.consumeJWT(systemInformation, userInformation, jwtToken, allowedExternalSubjects, defaultTemplateUser, useFileAndUrlFriendlyBase64);
          if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            request.setAttribute(SecurityHelper.REQUEST_AUTH_TOKEN, authentication);
            onSuccessfulAuthentication(request, response, authentication);
          }
        }
        catch (ExpiredJwtException|MalformedJwtException|SignatureException|JWTConsumer.JwtTokenException e) {
          //delete any cookie if it exists...
          JWTAuthToken jwtAuthToken = new JWTAuthToken(null, null,null, null, JWTAuthToken.TokenUse.ACCESS);
          jwtAuthToken.setAuthenticated(false);
          jwtAuthToken.setDetails(e.getMessage());
          SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);
        }
        catch (Exception e) {
          clearJwt(jwtTokenCookie, response);
          throw new AuthenticationServiceException("Cannot parse JWT token", e); //ServletException("Cannot parse JWT token", e);
        }
      }
    }
    chain.doFilter(request, response);
  }
  
  private void clearJwt(String jwtTokenCookie, HttpServletResponse response) {
    if (jwtTokenCookie != null) {
      logger.info("Removing invalid JWT cookie.");
      Cookie cookie = new Cookie("access_token", null); // Not necessary, but saves bandwidth.
      cookie.setPath("/");
      cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
      response.addCookie(cookie);
    }
  }
  
  protected void onSuccessfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response, Authentication authResult) throws IOException {
  }

  protected void onUnsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response, AuthenticationException failed)
          throws IOException {
    Cookie cookie = new Cookie("access_token", null); // Not necessary, but saves bandwidth.
    cookie.setPath("/");
    cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
    response.addCookie(cookie);
  }

//  public void setRememberMeServices(RememberMeServices rememberMeServices) {
//    Assert.notNull(rememberMeServices, "rememberMeServices cannot be null");
//    this.rememberMeServices = rememberMeServices;
//  }


  public String getAllowedExternalSubjects() {
    return allowedExternalSubjects;
  }

  public void setAllowedExternalSubjects(String allowedExternalSubjects) {
    this.allowedExternalSubjects = allowedExternalSubjects;
  }

  public String getDefaultTemplateUser() {
    return defaultTemplateUser;
  }

  public void setDefaultTemplateUser(String defaultTemplateUser) {
    this.defaultTemplateUser = defaultTemplateUser;
  }
}
