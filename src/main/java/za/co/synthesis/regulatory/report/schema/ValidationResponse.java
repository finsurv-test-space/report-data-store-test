package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.core.Scope;

import javax.persistence.Embeddable;


/**
 * User: jake
 * Date: 4/21/16
 * Time: 11:04 AM
 * A class to wrap the validation response to make it look good
 */
@JsonPropertyOrder({
        "Type"
        , "ReportingQualifier"
        , "Name"
        , "Code"
        , "Message"
        , "Scope"
        , "MoneyInstance"
        , "ImportExportInstance"
})
@Embeddable
public class ValidationResponse {
  private final ResultEntry resultEntry;

  public ValidationResponse(ResultEntry resultEntry) {
    this.resultEntry = resultEntry;
  }

  @JsonProperty("Type")
  public ResultType getType() {
    return resultEntry.getType();
  }

  @JsonProperty("Name")
  public String getName() {
    return resultEntry.getName();
  }

  @JsonProperty("Code")
  public String getCode() {
    return resultEntry.getCode();
  }

  @JsonProperty("Message")
  public String getMessage() {
    return resultEntry.getMessage();
  }

  @JsonProperty("Scope")
  public Scope getScope() {
    return resultEntry.getScope();
  }

  @JsonProperty("MoneyInstance")
  public int getMoneyInstance() {
    return resultEntry.getMoneyInstance() + 1;
  }

  @JsonProperty("ImportExportInstance")
  public int getImportExportInstance() {
    return resultEntry.getImportExportInstance() + 1;
  }

}
