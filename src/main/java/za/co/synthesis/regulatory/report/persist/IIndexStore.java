package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;

import javax.persistence.Entity;
import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/09/07.
 */
public interface IIndexStore {


  @Entity
  public static class IndexEntity{
    private String compoundIndexKey;
    private String referenceKey;

    public IndexEntity(String path, String value, String referenceKey) {
      compoundIndexKey = generateIndexKey(path, value);
      this.referenceKey = referenceKey;
    }

    public IndexEntity(String indexKey, String referenceKey) {
      this.compoundIndexKey = indexKey;
      this.referenceKey = referenceKey;
    }

    public IndexEntity(CriterionData criterion, String referenceKey) {
      this.compoundIndexKey = generateIndexKey(criterion);
      this.referenceKey = referenceKey;
    }

    public String  getIndexKey(){
      return compoundIndexKey;
    }

    public String getReferenceKey(){
      return this.referenceKey;
    }

    public static String generateIndexKey(String path, String value) {
      return path + "::" + value;
    }


    public static String generateIndexKey(CriterionData criterion){
      if (criterion != null) {
        String path = "";
        //This is likely NOT going to work for nested map object paths (ie: report.money.importexport.x)
        if (criterion.getType().equals(CriterionType.Meta)) {
          path = "meta." + criterion.getName();
        } else if (criterion.getType().equals(CriterionType.Report)) {
          path = "report." + criterion.getName();
        } else if (criterion.getType().equals(CriterionType.System)) {
          path = "state";
        }
        return generateIndexKey(path, criterion.getValue());
      }
      return null;
    }

    public String toString(){
      return this.compoundIndexKey+" => "+this.referenceKey;
    }
  }


  public List<String> getReferenceKeys(Map<String, List<CriterionData>> mappedCriteria);

  public List<IndexEntity> getIndices(Map<String, List<CriterionData>> mappedCriteria);

  public void setIndices(List<IndexEntity> haIndexEntities);

  public void clearIndex();
}
