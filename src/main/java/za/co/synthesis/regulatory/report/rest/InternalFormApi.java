package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.schema.ValidationResponse;
import za.co.synthesis.regulatory.report.schema.ValidationResult;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.IChannelCache;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static za.co.synthesis.regulatory.report.rest.FormApi.getFormAPI;

@Controller
@RequestMapping("internal")
public class InternalFormApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalFormApi.class);
  
  @Autowired
  private SystemInformation systemInformation;
  
  @Autowired
  private JsonSchemaTransform jsonSchemaTransform;
  
  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private SystemInformation channelInformation;

  @Autowired
  private IChannelCache channelCache;

  @Autowired
  private RulesCache rulesCache;

//  private final Report reportService;

  public InternalFormApi() {
//    this.reportService = new Report(systemInformation, userInformation, dataLogic, validationEngineFactory);
  }

  private static String authorisationHeader(Authentication authentication) {
//        Authentication authentication = SecurityHelper.getAuthMeta();
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      String currentUserName = authentication.getName();
      Object credentials = authentication.getCredentials();
      if (credentials != null) {
        return "Basic " + Base64.encode((currentUserName + ":" + credentials.toString()).getBytes());
      }
    }
    return "";
  }


  @RequestMapping(value = "/producer/api/js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getFormAPIInternal(@RequestParam String channelName,
                          @RequestParam(required = false) String trnReference,
                          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getFormAPIInternal()");
    getFormAPI(channelName, trnReference, false, response, channelInformation, dataLogic, channelCache, rulesCache, false);

  }


//-----------------------------------------------------------------------------------------------
//  Externally Registered Form API
  
  @RequestMapping(value = "/producer/api/js", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  public
  @ResponseBody
  ValidationResult setFormInternal(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference, //TODO: should this be encoded into the report body?
      @RequestBody Map<String, Object> report) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setFormInternal()");
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null)
      throw new PreconditionFailedException("A valid channelName must be specified");
    
    if (report == null)
      throw new PreconditionFailedException("Valid json report data must be submitted");
    
    ReportData reportData = new ReportData((Map)report.get("Meta"), (Map)report.get("Report"));
    String schema = "sarb";
    try {
      schema = (jsonSchemaTransform != null ? jsonSchemaTransform.getReportSchema(reportData.getReport(), "sarb") : "sarb");
    } catch (Exception e) {
      logger.error("Unable to determine report schema automatically, defaulting to 'sarb' schema : "+e.getMessage(), e);
    }
    
    //TODO: REMOVE THIS DEBUG OUTPUT...
    logger.trace("Data Received("+channelName+ (trnReference!=null?"->"+trnReference:"") +") : \n\n"+ JsonSchemaTransform.mapToJsonStr(report)+"\n\n");
    
    ValidationResult validationResult = dataLogic.setReportData(schema, channelName, reportData, SecurityHelper.getAuthMeta());
    
    //TODO: REMOVE THIS DEBUG OUTPUT...
    String text = "";
    int cnt = 1;
    for (ValidationResponse response : validationResult.getValidations()){
      text += "###  "+cnt+"  ###\n";
      text += "scope: "+response.getScope()+"\n";
      text += "money: "+response.getMoneyInstance()+"\n";
      text += "import-export: "+response.getImportExportInstance()+"\n";
      text += "type: "+response.getType()+"\n";
      text += "code: "+response.getCode()+"\n";
      text += "name: "+response.getName()+"\n";
      text += "message: "+response.getMessage()+"\n\n";
      cnt++;
    }
    logger.trace("Validation Responses ("+trnReference+"):\n"+text);
    
    return validationResult;
  }
  
  
  
  @RequestMapping(value = "/form/api/submit/{schema}", method = RequestMethod.POST, produces = "application/json")
  @Deprecated
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ValidationResult submitFormDataInternal(@PathVariable String schema,
                      @RequestParam (required = true) String channelName,
                       @RequestBody Map data,
                       HttpServletResponse response) throws Exception{

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "submitFormDataInternal()");
    if (data != null) {
      /*TODO: we will need to flesh out a datatype with some sort of minimum mandatory information
          IE: Version, Report, Meta...
       */
      String branch = "Version";
      if (data.containsKey(branch)){
        logger.debug("Form data version: {}", data.getOrDefault(branch, "UNKNOWN"));
      }
      //JSObject formData = JsonSchemaTransform.mapToJson(data);
      Map <String, Object> metaData = null;
      Map <String, Object> reportData = null;
      //fetch Report data
      branch = "Report";
      if (data.containsKey(branch)){
        Object branchObj = data.get(branch);
        if (branchObj instanceof Map) {
          reportData = (Map) branchObj;
        }
      }
      //fetch Meta data
      branch = "Meta";
      if (data.containsKey(branch)){
        Object branchObj = data.get(branch);
        if (branchObj instanceof Map) {
          metaData = (Map) branchObj;
        }
      }
      if (reportData != null) {
        ReportData report = new ReportData(metaData, reportData);
        return dataLogic.setReportData(schema, channelName, report, SecurityContextHolder.getContext().getAuthentication());
      }
      //TODO: perform configured actions with remaining data (if any)
    }
    throw new PreconditionFailedException("One or more of the minimum mandatory fields were absent from the data provided: Version, Meta & Report");
  }


}
