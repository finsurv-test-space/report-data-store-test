package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.ChannelData;
import za.co.synthesis.regulatory.report.persist.UserData;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("internal/api/user")
public class  InternalUserApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalUserApi.class);

  @Autowired
  private UserInformation userInformation;

  @Autowired
  ApplicationContext ctx;

//-----------------------------------------------------------------------------------------------
//  User Management APIs

  @RequestMapping(value = "/whois", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  UserInfo getUserLoginDetails(
          @RequestParam String username,
          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserLoginDetails()");

    UserInfo user = null;
    if (username != null) {
      user = userInformation.getUserInfo(username);
    }
    if (user == null) {
      throw new ResourceNotFoundException("Valid user not found or incorrectly specified: \"" + username + "\"");
    }
    return user;
  }

  @RequestMapping(value = "/validChannels", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
  public
  @ResponseBody
  List<Channel> getChannels(
          @RequestParam(required = true) String reportSpace,
          @RequestParam(required = true) String username,
          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getChannels()");
    List<String> allReportSpaces = new ArrayList<>();

    if (reportSpace instanceof String) {
      String[] reportSpaces = reportSpace.split("\\s*[,;]\\s*");
      for (int i = 0; i < reportSpaces.length; i++) {
        if (reportSpaces[i] instanceof String && reportSpaces[i].length() > 0) {
          allReportSpaces.add(reportSpaces[i]);
        }
      }
    }

    return userInformation.getUserChannels(username, allReportSpaces);
  }


  @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  List<UserInfo> getUserLoginDetailList(
          @RequestParam(required = false) String[] usernamesFilter,
          HttpServletResponse response) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserLoginDetailList()");
    if (usernamesFilter != null && usernamesFilter.length > 0) {
      List<UserInfo> userList = new ArrayList<UserInfo>();
      for (String username : usernamesFilter) {
        userList.add(userInformation.getUserInfo(username));
      }
      return userList;
    } else {
      List<UserInfo> userList = new ArrayList<UserInfo>();
      for (UserData authUser : userInformation.getUserList()) {
        String username = authUser.getUsername();
        userList.add(userInformation.getUserInfo(username));
      }
      return userList;
    }
  }


  @RequestMapping(value = "/map", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  Map<String, UserInfo> getUserLoginDetailMap(
          @RequestParam String[] usernamesFilter,
          HttpServletResponse response) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserLoginDetailMap()");
    if (usernamesFilter != null && usernamesFilter.length > 0) {
      Map<String, UserInfo> userList = new HashMap<String, UserInfo>();
      for (String username : usernamesFilter) {
        userList.put(username, userInformation.getUserInfo(username));
      }
      return userList;
    } else {
      Map<String, UserInfo> userList = new HashMap<String, UserInfo>();
      for (UserData authUser : userInformation.getUserList()) {
        String username = authUser.getUsername();
        userList.put(username, userInformation.getUserInfo(username));
      }
      return userList;
    }
  }


  @RequestMapping(value = "/usernames", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  List<String> getUserLoginList(HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserLoginList()");
    List<String> userList = new ArrayList<String>();
    for (UserData authUser : userInformation.getUserList()) {
      String username = authUser.getUsername();
      userList.add(username);
    }
    return userList;
  }


  /**
   * if setting data for a user which does not yet exist, the provided password will be used - unless it is blank, in which case a psuedo-random password will be generated and returned as part of the response - in plaintext.
   * if setting data for an existing user, any password specified in the data will be ignored and the existing password will remain in use (visible as a BCrypt hash returned in the payload).
   * @param user
   * @return
   * @throws Exception
   */
    @RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    //@PreAuthorize("hasRole('SET_USER_DETAILS')")
    public
    @ResponseBody
    UserInfo setUser(
            @RequestBody (required = true) UserInfo user) throws Exception {

      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setUser()");
      Authentication authentication = SecurityHelper.getAuthMeta();
      return userInformation.setUserInfo(user);
    }


  //-----  Access Sets  -----//
  @RequestMapping(value = "/access_sets", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_ACCESS_SETS') OR hasRole('SET_ACCESS_SETS')")
  public
  @ResponseBody
  List<AccessSet> getAccessSets(
          @RequestParam(required = false) String reportSpace) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getAccessSets()");

    if (reportSpace instanceof String) {
      return userInformation.getAccessSets(reportSpace);
    }

    return userInformation.getAccessSets();
  }

  //-----  Access Sets  -----//
  @RequestMapping(value = "/access_set", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_ACCESS_SETS') OR hasRole('SET_ACCESS_SETS')")
  public
  @ResponseBody
  AccessSet getAccessSet(
          @RequestParam String name) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getAccessSet()");

    return userInformation.getAccessSet(name) ;
  }

  @RequestMapping(value = "/access_set", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('SET_ACCESS_SETS')")
  public
  @ResponseBody
  void setAccessSet(
          @RequestBody AccessSet accessSet) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setAccessSet()");
    userInformation.setAccessSet(accessSet);
  }

  //-----  User Access  -----//
  @RequestMapping(value = "/access", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
 // @PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  UserAccess getUserAccess(
          @RequestParam String username,
          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserAccess()");

    return userInformation.getUserAccess(username);
  }


  //-----  User Channels  -----//
  @RequestMapping(value = "/channels", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  List<Channel> getUserChannel(
          @RequestParam String username,
          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getUserChannel()");

    return userInformation.getUserChannels(username);
  }


  @RequestMapping(value = "/channels", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  void setUserChannels(
          @RequestParam String username,
          @RequestParam List<Channel> channels,
          HttpServletResponse response) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setUserChannels()");
    //TODO: implement set user channels
  }



  //*************************************************************************
  //***  TODO: Disable & Enable Users (already configured in the system)  ***
  //*************************************************************************
  @RequestMapping(value = "/disable", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  UserData disableUser(
          @RequestParam String username,
          HttpServletRequest request,
          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "disableUser()");

    Authentication authentication = SecurityHelper.getAuthMeta();
    String currentUserName = authentication.getName();

    //Check if current user has access/authorization to view/set userInfo for other users
    SecurityHelper.checkUserAuthority(userInformation.getUserInfo(currentUserName), "SET_USER_DETAILS", "disableUser()");

    logger.info("Removing user '{}' - initiated by: {}", username, currentUserName);

    //TODO: check user store and disable the respective user.
    return null;
  }



  @RequestMapping(value = "/enable", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  //@PreAuthorize("hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  UserData enableUser(
          @RequestParam String username,
          HttpServletRequest request,
          HttpServletResponse response) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "enableUser()");
    Authentication authentication = SecurityHelper.getAuthMeta();
    String currentUserName = authentication.getName();

    //Check if current user has access/authorization to view/set userInfo for other users
    SecurityHelper.checkUserAuthority(userInformation.getUserInfo(currentUserName), "SET_USER_DETAILS", "enableUser()");

    logger.info("Enabling user '{}' - initiated by: {}", username, currentUserName);

    //TODO: check user store and enable the respective user.
    return null;
  }


}
