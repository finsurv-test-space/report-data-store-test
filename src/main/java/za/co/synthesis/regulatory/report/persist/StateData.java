package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.support.ActionType;
import za.co.synthesis.regulatory.report.support.BehaviourType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/26/17.
 */
public class StateData {
  private String name;
  private final List<BehaviourType> behaviours;
  private final List<ActionType> actions;
  private final List<String> userEvents;
  private boolean defaultState;

  public StateData() {
    this.behaviours = new ArrayList<BehaviourType>();
    this.actions = new ArrayList<ActionType>();
    this.userEvents = new ArrayList<String>();
  }

  public StateData(String name, List<BehaviourType> behaviours, List<ActionType> actions, List<String> userEvents, boolean defaultState) {
    this.name = name;
    this.behaviours = behaviours;
    this.actions = actions;
    this.userEvents = userEvents;
    this.defaultState = defaultState;
  }

  public String getName() {
    return name;
  }

  public boolean isDefaultState() {
    return defaultState;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDefaultState(boolean defaultState) {
    this.defaultState = defaultState;
  }

  public List<BehaviourType> getBehaviours() {
    return behaviours;
  }

  public List<ActionType> getActions() {
    return actions;
  }

  public List<String> getUserEvents() {
    return userEvents;
  }
}