package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.AccessSetData;
import za.co.synthesis.regulatory.report.persist.UserAccessData;
import za.co.synthesis.regulatory.report.support.AccessType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/27/17.
 */
@JsonPropertyOrder({
        "AccessList"
})
public class UserAccess {
  private final UserAccessData userAccessData;
  private final List<AccessSet> accessList;

  public UserAccess() {
    this.userAccessData = new UserAccessData();
    this.accessList = new ArrayList<AccessSet>();
  }

  public UserAccess(UserAccessData userAccessData) {
    this.userAccessData = userAccessData;
    this.accessList = AccessSet.wrapList(userAccessData.getAccessList());
  }

  @JsonProperty("AccessList")
  public List<AccessSet> getAccessList() {
    return accessList;
  }

  @JsonIgnore
  public void syncData() {
    userAccessData.getAccessList().clear();
    for (AccessSet accessSet : accessList) {
      accessSet.syncData();
      userAccessData.getAccessList().add(accessSet.getAccessSetData());
    }
  }
}
