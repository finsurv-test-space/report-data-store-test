package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.schema.ColumnDisplay;
import za.co.synthesis.regulatory.report.schema.StoredKey;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;
import za.co.synthesis.regulatory.report.support.FileUploadUtil;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jake on 5/29/17.
 */
public interface IConfigStore {
  //-----  Type JSON Interface  -----//
  JSObject getTypeJSObject(String path) throws Exception;

  //-----  Type JSON Interface  -----//
  String getFreemarkerContent(String path) throws Exception;

  //----- Temporary Cache Store (used) -----//
  void setCacheByName(String name, String data);
  String getCacheByName(String name);

  //-----  Channel Interfaces  -----//
  Map<String, ChannelData> getChannelData();
  //CHANNEL CONFIG
  void setChannelData(ChannelData channelData);

  //----- Type Interfaces  -----//
  List<String> getTypeSchemas(String typeName);

  //----- Other Interfaces  -----//
  Integer getDefaultMaxDownloadRecords();
  //SYSTEM CONFIG
  void setDefaultMaxDownloadRecords(Integer max);


  //-----  User Management Interfaces  -----//
  Map<String, UserData> getUserMap();
  void setUser(UserData user);
  void removeUser(String username);

  //-----  Access Set -----//
  Map<String, AccessSetData> getAccessSetMap();
  void setAccessSet(AccessSetData accessSet);
  void removeAccessSet(String setname);

  //-----  Rights  -----//
  List<String> getRightsList();
  void addRight(String right);
  void removeRight(String right);

  //-----  User Events  -----//
//  List<String> getUserEventsList(String reportSpace);
//  void addUserEvent(String reportSpace, String right);
//  void removeUserEvent(String reportSpace, String right);

  //-----  List Definitions  -----//
  Map<String, ListData> getListData();
  void setListDefinition(ListData definition);

  //----- Lookups and Validations  -----//
  Map<String, LookupData> getLookupsForRepo(String repo);
  Map<String, ValidationData> getValidationsForRepo(String repo);

  //----- Report Space configuration -----//
  Map<String, ReportSpaceData> getReportSpaces();
  void setReportSpace(ReportSpaceData data);

  UserData getUser(String username);

  //----- Proxy Call configuration -----//
  Map<String, ProxyCallData> getProxyCalls();

  List<RequestParamData> getAuthTokenPopulateMetaParamList();
  List<RequestParamData> getAuditParamList();

  //----- Paramemeters and Properties
  Properties getEnvironmentParameters();
  Properties getEnvironmentProperties();


  //----- File Upload properties -----//
  List<String> getExtensionWhitelist();
  List<String> getMimeTypeWhitelist();
  FileUploadUtil.MimeTypeWhiteListPass getMimeTypeWhitelistMechanism();
  
  //----- Public and Private keypair store -----//
  String setKeyPair(String kid, StoredKey storedPrivateKey, StoredKey storedPublicKey);
  String getCurrentKeyId();
  StoredKey getPrivateKey(String kid);
  StoredKey getPublicKey(String kid);
  Map<String, StoredKey> getPublicKeys();
  List<String> getPKSourcesList();

  //----- RSUI Column Displays Config ------//
  JSObject getExtConfig(String name);
  void setExtConfig(String name, String value);
  void setExtConfig(String name, JSObject value);

  long getTokenExpiryMinutes(JWTAuthToken.TokenUse tokenUse);

  String getDefaultTypeLocation();
  String getDefaultMiscLocation();
  String getDefaultConfigLocation();
}
