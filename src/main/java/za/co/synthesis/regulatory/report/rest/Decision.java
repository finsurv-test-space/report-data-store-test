package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.EvaluationEngineFactory;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceAccessAuthorizationException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.EvaluationScenarioDecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 3/31/16.
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/decision")
public class Decision extends ControllerBase {
  static final Logger logger = LoggerFactory.getLogger(Decision.class);

  @Autowired
  private EvaluationEngineFactory engineFactory;

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private DataLogic dataLogic;


  private Evaluator evaluator;

  public void setEngineFactory(EvaluationEngineFactory engineFactory) {
    this.engineFactory = engineFactory;
  }

  public static synchronized Evaluator getEvaluator(SystemInformation systemInformation, String packageName, EvaluationEngineFactory engineFactory,
                                                    Logger logger) throws Exception {
    Evaluator evaluator = null;
//        if (evaluator == null) {
    try {
      EvaluationEngine engine = engineFactory.getEngine(packageName);
      if (engine == null) {
        if (systemInformation.getChannel(packageName) == null) {
          throw new PreconditionFailedException("The ChannelName '" + packageName + "' is not valid. A valid channelName must be specified");
        }
        throw new EvaluationException("The rules for ChannelName '" + packageName + "' are not available. Cannot perform evaluation.");
      }
      evaluator = engine.issueEvaluator();
    } catch (Exception e) {
      logger.error("Cannot get Evaluator", e);
      throw e;
    }
//        }
    return evaluator;
  }

  /**
   * This will retrieve a stored decision related to the specific TrnReference provided, located within the given reporting space.
   * You need to provide either a valid channelName or reportSpace.
   *
   * @param channelName  (Optional / mutually exclusive)  - the channel (and hence underlying reporting space) associated with the decision
   * @param reportSpace (Optional / mutually exclusive) - the reporting space associated with the decision
   * @param trnReference   - the transaction reference for which the decision will apply
   * @return the stored decision for the given TrnReference <br><b>PLEASE NOTE:</b> The return type for this API has been updated to be in-line with internal naming conventions.  All instances of "<b>reportingSpace</b>" are being superceded with "<b>reportSpace</b>" and systems making use of this return type should plan on deprecating the existing "reportingSpace" field altogether.
   */
  @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  FullyReferencedEvaluationDecision getDecision(
      @RequestParam(required = false) String channelName,
      @RequestParam(required = false) String reportSpace,
      @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getDecision()");
    return getDecision(channelName, reportSpace, trnReference, systemInformation, dataLogic);
  }

  public static FullyReferencedEvaluationDecision getDecision(
      String channelName,
      String reportSpace,
      String trnReference,
      SystemInformation systemInformation,
      DataLogic dataLogic) {
    if (reportSpace == null && channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null)
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");

    FullyReferencedEvaluationDecision decision = dataLogic.getEvaluationDecision(reportSpace, trnReference);
    if (decision == null)
      throw new ResourceNotFoundException("Decision for '" + trnReference + "@" + reportSpace + "' not found");

    return decision;
  }


  /**
   * This will store the provided decision object in the system, against the specified trnReference, in the reporting space related to the channel provided
   *
   * @param channelName  The channel configuration from which to extract the related reporting space to store the object in - should typically be the same as the channel used to perform the evaluation
   * @param trnReference The transaction reference to store the object against
   * @param decision     The complete decision object
   */
  @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  void setDecision(
      @RequestParam String channelName,
      @RequestParam String trnReference,
      @RequestBody EvaluationDecision decision) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setDecision()");
    setDecision(channelName, trnReference, decision, systemInformation, dataLogic);
  }

  public static void setDecision(String channelName, String trnReference, EvaluationDecision decision,
                                 SystemInformation systemInformation, DataLogic dataLogic) {
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null)
      throw new PreconditionFailedException("A valid channelName must be specified");

    dataLogic.setEvaluationDecision(new FullyReferencedEvaluationDecision(channel.getReportSpace(), trnReference, decision));
  }


  /**
   * Omnibus evaluation function that covers all evaluation scenarios with a single JSON request. This function can
   * only evaluate one scenario per call. The documentation for each of the scenarios can be found under the respective
   * API call.
   *
   * @param evaluationRequest The scenario to be evaluated
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateAny", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateAny(@RequestBody EvaluationRequest evaluationRequest) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateAny()");
    return evaluateAnyStatic(evaluationRequest, systemInformation, engineFactory, logger, dataLogic);
  }


  /**
   * Customer payment, typically within the bank where both remitter and beneficiary details (account type and resident status) are known.
   * For example, a CFC to CFC transfer from one customer to another.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   *
   * @param channelName             The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference            - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param drResStatus             The remitter Resident Status
   * @param drAccountHolderStatus   - (Optional) The remitter Status - if not provided, "Individual" will be assumed.
   * @param drAccType               The remitter Account Type
   * @param crResStatus             The beneficiary Resident Status
   * @param crAccountHolderStatus   - (Optional) The beneficiary Status - if not provided, "Individual" will be assumed.
   * @param crAccType               The beneficiary Account Type
   * @param mostRelevantResultsOnly When true, returns only the most relevant results for Dr and CR - where reportable.  If no reportable results, the next most onerous decision is returned (if any).
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateCustomerOnUs", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateCustomerOnUs(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam ResidenceStatus drResStatus,
      @RequestParam(required = false) AccountHolderStatus drAccountHolderStatus,
      @RequestParam BankAccountType drAccType,
      @RequestParam ResidenceStatus crResStatus,
      @RequestParam(required = false) AccountHolderStatus crAccountHolderStatus,
      @RequestParam BankAccountType crAccType,
      @RequestParam(required = false) Boolean mostRelevantResultsOnly) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateCustomerOnUs()");
    return evaluateCustomerOnUsStatic(channelName, trnReference, drResStatus, drAccountHolderStatus, drAccType, crResStatus, crAccountHolderStatus, crAccType, mostRelevantResultsOnly, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * This is typically a payment from a customer within the bank, to an external account.
   * The remitter details (Resident status and account type) are generally known in full.
   * For the beneficiary however, only the BIC and transaction currency might be known.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   *
   * @param channelName               The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference              - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param drResStatus               The remitter Resident Status
   * @param drAccountHolderStatus     - (Optional) The remitter Status - if not provided, "Individual" will be assumed.
   * @param drAccType                 The remitter Account Type
   * @param beneficiaryInstitutionBIC The BIC code of the institution (bank or other) to which the payment was made
   * @param beneficiaryCurrency       The currency in which the payment was made
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateCustomerPayment", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateCustomerPayment(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam ResidenceStatus drResStatus,
      @RequestParam(required = false) AccountHolderStatus drAccountHolderStatus,
      @RequestParam BankAccountType drAccType,
      @RequestParam String beneficiaryInstitutionBIC,
      @RequestParam String beneficiaryCurrency) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateCustomerPayment()");
    return evaluateCustomerPaymentStatic(channelName, trnReference, drResStatus, drAccountHolderStatus, drAccType, beneficiaryInstitutionBIC, beneficiaryCurrency, systemInformation, engineFactory, logger, dataLogic);
  }


  /**
   * This is typically a payment to a customer (beneficiary) within the bank, from an external account (remitter).
   * The beneficiary details (Resident status and account type) are known in full.
   * For the remitter however, only the BIC and transaction currency might be known.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   *
   * @param channelName             The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference            - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param crResStatus             The beneficiary Resident Status
   * @param crAccountHolderStatus   - (Optional) The beneficiary Status - if not provided, "Individual" will be assumed.
   * @param crAccType               The beneficiary Account Type
   * @param orderingInstituitionBIC The BIC code of the institution (bank or other) from which the payment was made
   * @param orderingCurrency        The currency in which the payment was made
   * @param field72                 Value provided in field72 of the SWIFT message detail
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateCustomerReceipt", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateCustomerReceipt(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam ResidenceStatus crResStatus,
      @RequestParam(required = false) AccountHolderStatus crAccountHolderStatus,
      @RequestParam BankAccountType crAccType,
      @RequestParam String orderingInstituitionBIC,
      @RequestParam String orderingCurrency,
      @RequestParam(required = false) String field72) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateCustomerReceipt()");
    return evaluateCustomerReceiptStatic(channelName, trnReference, crResStatus, crAccountHolderStatus, crAccType, orderingInstituitionBIC, orderingCurrency, field72, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment made to another (banking) institute.
   *
   * @param channelName            The channel package containing the rules to apply to the evaluation
   * @param trnReference           - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param orderingInstitutionBIC (Optional - Deprecated) The BIC code of the ordering (remitter) Institution - i.e.: Our BIC code.
   * @param beneficiaryBIC         The BIC code of the beneficiary bank - not a correspondent bank
   * @param beneficiaryCurrency    The currency in which the payment is made
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateBankPayment", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateBankPayment(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @Deprecated @RequestParam(required = false) String orderingInstitutionBIC,
      @RequestParam String beneficiaryBIC,
      @RequestParam String beneficiaryCurrency) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateBankPayment()");
    return evaluateBankPaymentStatic(channelName, trnReference, orderingInstitutionBIC, beneficiaryBIC, beneficiaryCurrency, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment received from another (banking) institute.
   *
   * @param channelName      The channel package containing the rules to apply to the evaluation
   * @param trnReference     - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param beneficiaryBIC   (Optional - Deprecated) The BIC code of the beneficiary bank  - i.e.: Our BIC code.
   * @param orderingBIC      The BIC code of the ordering (remitter) bank  - not a correspondent bank
   * @param orderingCurrency The currency in which the payment is made
   */
  @RequestMapping(value = "/evaluateBankReceipt", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateBankReceipt(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @Deprecated @RequestParam(required = false) String beneficiaryBIC,
      @RequestParam String orderingBIC,
      @RequestParam String orderingCurrency) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateBankReceipt()");
    return evaluateBankReceiptStatic(channelName, trnReference, beneficiaryBIC, orderingBIC, orderingCurrency, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment received on behalf of another (banking) institute - where we are acting as a correspondent bank in the payment.
   *
   * @param channelName               The channel package containing the rules to apply to the evaluation
   * @param trnReference              - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param beneficiaryInstitutionBIC The BIC code of the beneficiary bank  - not a correspondent bank (not us)
   * @param orderingInstitutionBIC    The BIC code of the ordering (remitter) bank  - not a correspondent bank (not us)
   * @param orderingCustomer          The ordering customer type - Bank or Non-Bank.  This will determine the value populated for the suggested field72 value (NTNRC vs NTNRB).
   */
  @RequestMapping(value = "/evaluateCorrespondentBank", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateCorrespondentBank(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam String beneficiaryInstitutionBIC,
      @RequestParam String orderingInstitutionBIC,
      @RequestParam OrderingCustomerType orderingCustomer) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateCorrespondentBank()");
    return evaluateCorrespondantBankStatic(channelName, trnReference, beneficiaryInstitutionBIC, orderingInstitutionBIC, orderingCustomer, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Card transaction evaluation
   *
   * @param channelName    The channel package containing the rules to apply to the evaluation
   * @param trnReference   - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param issuingCountry The country of the bank that issued the card to the customer
   * @param useCountry     The country where the card was used
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluateCard", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions evaluateCard(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam String issuingCountry,
      @RequestParam String useCountry) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateCard()");
    return evaluateCardStatic(channelName, trnReference, issuingCountry, useCountry, systemInformation, engineFactory, logger, dataLogic);
  }


  /**
   * The consolidated evaluation API will select the appropriate evaluation scenario based on the parameters passed and the assumption-logic for the specified channel.
   *
   * @param channelName                    The channel package containing the rules to apply to the evaluation
   * @param trnReference                   - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param domesticAmount                 - (Optional) The value of the transaction in the domestic currency - this is to cater for allowances such as those for threshold values between CMA countries
   * @param orderingBIC                    - BIC code for the ordering institution
   * @param orderingCurrency               - Ordering currency
   * @param orderingResStatus              - Remitter resident status
   * @param orderingAccType                - The resident status of the remitter
   * @param orderingAccountHolderStatus    - the account holder status of the remitter
   * @param orderingOptionalParams         - A delimited string containing a list of optional params. <br/><i>Expected format: <b>"[key1]=[value1];[key2]=[value2];..."</b></i>
   * @param field72                        - Field 72 of the SWIFT message (if available)
   * @param beneficiaryBIC                 - BIC code for the beneficiary institution
   * @param beneficiaryCurrency            - currency that the beneficiary will receive
   * @param beneficiaryResStatus           - The resident status of the beneficiary
   * @param beneficiaryAccType             - The account type for the beneficiary
   * @param beneficiaryAccountHolderStatus - the account holder status of the beneficiary
   * @param beneficiaryOptionalParams      - A delimited string containing a list of optional params. <br/><i>Expected format: <b>"[key1]=[value1];[key2]=[value2];..."</b></i><br/>
   * @param sideFiltered                   - Filter the results to the relevant side (Dr/Cr) only
   * @param mostOnerous                    - Filter the results to the most onerous results only
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/consolidatedEvaluation", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  ConsolidatedEvaluationDecision consolidatedEvaluation(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference,
      @RequestParam(required = false) String domesticAmount,
      @RequestParam String orderingBIC,
      @RequestParam(required = false) String orderingCurrency,
      @RequestParam(required = false) ResidenceStatus orderingResStatus,
      @RequestParam(required = false) BankAccountType orderingAccType,
      @RequestParam(required = false) String field72,
      @RequestParam(required = false) AccountHolderStatus orderingAccountHolderStatus,
      @RequestParam(required=false) String orderingOptionalParams,
      @RequestParam String beneficiaryBIC,
      @RequestParam(required = false) String beneficiaryCurrency,
      @RequestParam(required = false) ResidenceStatus beneficiaryResStatus,
      @RequestParam(required = false) BankAccountType beneficiaryAccType,
      @RequestParam(required = false) AccountHolderStatus beneficiaryAccountHolderStatus,
      @RequestParam(required=false) String beneficiaryOptionalParams,
      @RequestParam(required = false) Boolean sideFiltered,
      @RequestParam(required = false) Boolean mostOnerous) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "consolidatedEvaluation()");
    sideFiltered = (sideFiltered == null ? false : sideFiltered);
    mostOnerous = (mostOnerous == null ? false : mostOnerous);
    Map<String, String> orderingParamMap = new HashMap<String,String>();
    if (orderingOptionalParams != null && !orderingOptionalParams.isEmpty() && orderingOptionalParams.indexOf("=") > 0){
      try{
        String[] optionalParams = orderingOptionalParams.split("\\s*;\\s*");
        for (String paramStr : optionalParams){
          if (paramStr != null && !paramStr.isEmpty() && paramStr.indexOf("=")>0){
            String[] paramParts = paramStr.split("\\s*=\\s*");
            if (paramParts.length == 2 && !paramParts[0].isEmpty() && !paramParts[1].isEmpty()){
              orderingParamMap.put(paramParts[0], paramParts[1]);
            }
          }
        }
      } catch(Exception error){
        logger.warn("Unable to process provided optional ordering parameter string: "+orderingOptionalParams);
      }
    }
    Map<String, String> beneficiaryParamMap = new HashMap<String,String>();
    if (beneficiaryOptionalParams != null && !beneficiaryOptionalParams.isEmpty() && beneficiaryOptionalParams.indexOf("=") > 0){
      try{
        String[] optionalParams = beneficiaryOptionalParams.split("\\s*;\\s*");
        for (String paramStr : optionalParams){
          if (paramStr != null && !paramStr.isEmpty() && paramStr.indexOf("=")>0){
            String[] paramParts = paramStr.split("\\s*=\\s*");
            if (paramParts.length == 2 && !paramParts[0].isEmpty() && !paramParts[1].isEmpty()){
              beneficiaryParamMap.put(paramParts[0], paramParts[1]);
            }
          }
        }
      } catch(Exception error){
        logger.warn("Unable to process provided optional beneficiary parameter string: "+beneficiaryOptionalParams);
      }
    }
    ConsolidatedOptionalParams orderingOptionalParamsObj = new ConsolidatedOptionalParams(orderingAccountHolderStatus, domesticAmount, orderingParamMap);
    ConsolidatedOptionalParams beneficiaryOptionalParamsObj = new ConsolidatedOptionalParams(beneficiaryAccountHolderStatus, domesticAmount, beneficiaryParamMap);
    return evaluateConsolidatedStatic(
        channelName, trnReference, orderingBIC, orderingCurrency, orderingResStatus, orderingAccType, field72, orderingOptionalParamsObj.getMap(),
        beneficiaryBIC, beneficiaryCurrency, beneficiaryResStatus, beneficiaryAccType, beneficiaryOptionalParamsObj.getMap(), sideFiltered, mostOnerous, systemInformation, engineFactory, logger, dataLogic);
  }


  //--------------------------------------------------------------------------------------------------------------------//
  //STATIC EVALUATION METHODS
  //--------------------------------------------------------------------------------------------------------------------//


  public static EvaluationDecisions evaluateAnyStatic(EvaluationRequest evaluationRequest, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    int scenarioCount = (evaluationRequest.getEvaluationCard() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationBankPayment() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationBankReceipt() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerOnUs() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerPayment() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerReceipt() != null ? 1 : 0);
    if (evaluationRequest.getChannelName() != null) {
      Channel channel = systemInformation.getChannel(evaluationRequest.getChannelName());
      if (channel == null)
        throw new PreconditionFailedException("The ChannelName '" + evaluationRequest.getChannelName() + "' is not valid. A valid channelName must be specified");
    } else {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }

    if (scenarioCount > 1) {
      throw new PreconditionFailedException("Too many evaluation scenarios have been specified");
    }
    if (scenarioCount == 0) {
      throw new PreconditionFailedException("No evaluation scenarios have been specified");
    }

    EvaluationDecisions decisions = null;
    if (evaluationRequest.getEvaluationCustomerOnUs() != null) {
      EvaluationRequest.EvaluationCustomerOnUs eval = evaluationRequest.getEvaluationCustomerOnUs();
      decisions = evaluateCustomerOnUsStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getDrResStatus(), eval.getDrAccountHolderStatus(), eval.getDrAccType(), eval.getCrResStatus(), eval.getCrAccountHolderStatus(), eval.getDrAccType(),
          true, systemInformation, engineFactory, logger, dataLogic);
    }
    if (decisions == null && evaluationRequest.getEvaluationCustomerPayment() != null) {
      EvaluationRequest.EvaluationCustomerPayment eval = evaluationRequest.getEvaluationCustomerPayment();
      decisions = evaluateCustomerPaymentStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getDrResStatus(), eval.getDrAccountHolderStatus(), eval.getDrAccType(), eval.getBeneficiaryBIC(), eval.getBeneficiaryCurrency(),
          systemInformation, engineFactory, logger, dataLogic);
    }
    if (decisions == null && evaluationRequest.getEvaluationCustomerReceipt() != null) {
      EvaluationRequest.EvaluationCustomerReceipt eval = evaluationRequest.getEvaluationCustomerReceipt();
      decisions = evaluateCustomerReceiptStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getCrResStatus(), eval.getCrAccountHolderStatus(), eval.getCrAccType(), eval.getOrderingBIC(), eval.getOrderingCurrency(), eval.getField72(),
          systemInformation, engineFactory, logger, dataLogic);
    }
    if (decisions == null && evaluationRequest.getEvaluationBankPayment() != null) {
      EvaluationRequest.EvaluationBankPayment eval = evaluationRequest.getEvaluationBankPayment();
      decisions = evaluateBankPaymentStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getOrderingBIC(), eval.getBeneficiaryBIC(), eval.getBeneficiaryCurrency(),
          systemInformation, engineFactory, logger, dataLogic);
    }
    if (decisions == null && evaluationRequest.getEvaluationBankReceipt() != null) {
      EvaluationRequest.EvaluationBankReceipt eval = evaluationRequest.getEvaluationBankReceipt();
      decisions = evaluateBankReceiptStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getBeneficiaryBIC(), eval.getOrderingBIC(), eval.getOrderingCurrency(),
          systemInformation, engineFactory, logger, dataLogic);
    }
    if (decisions == null && evaluationRequest.getEvaluationCard() != null) {
      EvaluationRequest.EvaluationCard eval = evaluationRequest.getEvaluationCard();
      decisions = evaluateCardStatic(evaluationRequest.getChannelName(), evaluationRequest.getTrnReference(),
          eval.getIssuingCountry(), eval.getUseCountry(),
          systemInformation, engineFactory, logger, dataLogic);
    }


    if (evaluationRequest.getTrnReference() != null && decisions != null) {
      String channelName = evaluationRequest.getChannelName();
      String trnReference = evaluationRequest.getTrnReference();
      try {
        String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
        if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
          FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, decisions);
          dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
        }
      } catch (Exception e) {
        logger.error("unable to log evaluation decisions (" + channelName + " : " + trnReference + ").", e);
      }
    }

    return decisions;
  }


  public static EvaluationDecisions evaluateCustomerOnUsStatic(String channelName, String trnReference, ResidenceStatus drResStatus, AccountHolderStatus drAccountHolderStatus, BankAccountType drAccType, ResidenceStatus crResStatus, AccountHolderStatus crAccountHolderStatus, BankAccountType crAccType, Boolean mostRelevantResultsOnly, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    mostRelevantResultsOnly = mostRelevantResultsOnly == null ? false : mostRelevantResultsOnly;
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    if (drAccountHolderStatus != null)
      additionalParams.put("drAccountHolderStatus", drAccountHolderStatus.name());
    if (crAccountHolderStatus != null)
      additionalParams.put("crAccountHolderStatus", crAccountHolderStatus.name());
    if (mostRelevantResultsOnly != null)
      additionalParams.put("functionCall", mostRelevantResultsOnly ? "evaluateCustomerOnUsFiltered" : "evaluateCustomerOnUs");

    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setEvaluationCustomerOnUs(new EvaluationRequest.EvaluationCustomerOnUs(drResStatus, drAccType, crResStatus, crAccType, additionalParams));


    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    List<IEvaluationDecision> decisions;
    if (mostRelevantResultsOnly) {
      decisions = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    } else {
      decisions = evaluator.evaluateCustomerOnUs(drResStatus, drAccType, drAccountHolderStatus, crResStatus, crAccType, crAccountHolderStatus);
    }
    List<EvaluationResponse> responses = new ArrayList<EvaluationResponse>();
    for (IEvaluationDecision decision : decisions) {
      responses.add(new EvaluationResponse(decision));
    }
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, responses);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, request, responses);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log customer on us evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }


  public static EvaluationDecisions evaluateCustomerPaymentStatic(String channelName, String trnReference, ResidenceStatus drResStatus, AccountHolderStatus drAccountHolderStatus, BankAccountType drAccType, String beneficiaryInstitutionBIC, String beneficiaryCurrency, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    if (drAccountHolderStatus != null)
      additionalParams.put("drAccountHolderStatus", drAccountHolderStatus.name());
    additionalParams.put("functionCall", "evaluateCustomerPayment");


    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateCustomerPayment(
        drResStatus, drAccType, beneficiaryInstitutionBIC, beneficiaryCurrency, drAccountHolderStatus);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setEvaluationCustomerPayment(new EvaluationRequest.EvaluationCustomerPayment(
        drResStatus, drAccType,
        beneficiaryInstitutionBIC, evaluator.getCounterpartyInstituitionByBIC(beneficiaryInstitutionBIC), beneficiaryCurrency));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log customer payment evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }

  public static EvaluationDecisions evaluateCustomerReceiptStatic(String channelName, String trnReference, ResidenceStatus crResStatus, AccountHolderStatus crAccountHolderStatus, BankAccountType crAccType, String orderingInstituitionBIC, String orderingCurrency, String field72, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    if (crAccountHolderStatus != null)
      additionalParams.put("crAccountHolderStatus", crAccountHolderStatus.name());
    additionalParams.put("functionCall", "evaluateCustomerReceipt");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateCustomerReceipt(
        crResStatus, crAccType, orderingInstituitionBIC, orderingCurrency, crAccountHolderStatus, field72);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setEvaluationCustomerReceipt(new EvaluationRequest.EvaluationCustomerReceipt(
        crResStatus, crAccType,
        orderingInstituitionBIC, evaluator.getCounterpartyInstituitionByBIC(orderingInstituitionBIC), orderingCurrency, field72));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log customer receipt evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }


  public static EvaluationDecisions evaluateBankPaymentStatic(String channelName, String trnReference, String orderingInstitutionBIC, String beneficiaryBIC, String beneficiaryCurrency, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (orderingInstitutionBIC != null)
      additionalParams.put("orderingInstitutionBIC", orderingInstitutionBIC);
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    additionalParams.put("functionCall", "evaluateBankPayment");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateBankPayment(beneficiaryBIC, beneficiaryCurrency);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    orderingInstitutionBIC = orderingInstitutionBIC != null ? orderingInstitutionBIC : ""; //we should be populating this with the bank's (our) BIC code.
    request.setEvaluationBankPayment(new EvaluationRequest.EvaluationBankPayment(orderingInstitutionBIC, beneficiaryBIC, beneficiaryCurrency));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log bank payment evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }

  public static EvaluationDecisions evaluateBankReceiptStatic(String channelName, String trnReference, String beneficiaryBIC, String orderingBIC, String orderingCurrency, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (beneficiaryBIC != null)
      additionalParams.put("beneficiaryBIC", beneficiaryBIC);
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    additionalParams.put("functionCall", "evaluateBankReceipt");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateBankReceipt(orderingBIC, orderingCurrency);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    beneficiaryBIC = beneficiaryBIC != null ? beneficiaryBIC : ""; //we should be populating this with the bank's (our) BIC code.
    request.setEvaluationBankReceipt(new EvaluationRequest.EvaluationBankReceipt(beneficiaryBIC, orderingBIC, orderingCurrency));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log bank receipt evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }


  public static EvaluationDecisions evaluateCorrespondantBankStatic(String channelName, String trnReference, String beneficiaryInstitutionBIC, String orderingInstitutionBIC, OrderingCustomerType orderingCustomer, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    additionalParams.put("functionCall", "evaluateCorrespondentBank");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateCorrespondentBank(beneficiaryInstitutionBIC, orderingInstitutionBIC, orderingCustomer);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setEvaluationCorrespondentBank(new EvaluationRequest.EvaluationCorrespondentBank(beneficiaryInstitutionBIC, orderingInstitutionBIC, orderingCustomer));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log correspondent bank evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }


  public static EvaluationDecisions evaluateCardStatic(String channelName, String trnReference, String issuingCountry, String useCountry, SystemInformation systemInformation, EvaluationEngineFactory engineFactory, Logger logger, DataLogic dataLogic) throws Exception {
    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    additionalParams.put("functionCall", "evaluateCARD");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationDecision decision = evaluator.evaluateCARD(issuingCountry, useCountry);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setEvaluationCard(new EvaluationRequest.EvaluationCard(issuingCountry, useCountry, additionalParams));
    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();

    if (decision != null)
      evaluations.add(new EvaluationResponse(decision));
    EvaluationDecisions evalDecisions = new EvaluationDecisions(request, evaluations);

    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e) {
      logger.error("unable to log card evaluation decisions (" + channelName + " : " + trnReference + ").", e);
    }
    return evalDecisions;
  }


  public static ConsolidatedEvaluationDecision evaluateConsolidatedStatic(
                                                  String channelName,
                                                  String trnReference,
                                                  String drBIC,
                                                  String drCurrency,
                                                  ResidenceStatus drResStatus,
                                                  BankAccountType drAccType,
                                                  String field72,
                                                  Map<String, Object> optionalDrParams,
                                                  String crBIC,
                                                  String crCurrency,
                                                  ResidenceStatus crResStatus,
                                                  BankAccountType crAccType,
                                                  Map<String, Object> optionalCrParams,
                                                  boolean sideFiltered,
                                                  boolean mostOnerous,
                                                  SystemInformation systemInformation,
                                                  EvaluationEngineFactory engineFactory,
                                                  Logger logger,
                                                  DataLogic dataLogic) throws Exception {

    Map<String, String> additionalParams = new HashMap<String, String>();
    if (trnReference != null)
      additionalParams.put("trnReference", trnReference);
    additionalParams.put("functionCall", "evaluateConsolidated");

    Evaluator evaluator = getEvaluator(systemInformation, channelName, engineFactory, logger);
    IEvaluationScenarioDecision result = evaluator.consolidatedEvaluation(drBIC, drResStatus, drAccType, drCurrency, field72, optionalDrParams, crBIC, crResStatus, crAccType, crCurrency, optionalCrParams, sideFiltered, mostOnerous);
    EvaluationRequest request = new EvaluationRequest();
    request.setChannelName(channelName);
    request.setConsolidatedEvaluation(new EvaluationRequest.ConsolidatedEvaluation(drBIC, drCurrency, drResStatus, drAccType, field72, optionalDrParams, crBIC, crCurrency, crResStatus, crAccType, optionalCrParams, additionalParams));

    ConsolidatedEvaluationDecision decision = new ConsolidatedEvaluationDecision(request, result);
    /*
    try {
      String reportSpace = systemInformation.getReportSpaceForChannel(channelName);
      if (trnReference != null && !trnReference.isEmpty() && reportSpace != null && !reportSpace.isEmpty()) {
        FullyReferencedEvaluationDecisions evalDecisionLogs = new FullyReferencedEvaluationDecisions(reportSpace, trnReference, evalDecisions);
        dataLogic.setEvaluationDecisionLog(evalDecisionLogs);
      }
    } catch (Exception e){
      logger.error("unable to log consolidated evaluation decisions ("+channelName+" : "+trnReference+").", e);
    }
    */

    return decision;
  }

}
