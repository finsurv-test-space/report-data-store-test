package za.co.synthesis.regulatory.report.businesslogic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.multipart.MultipartFile;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.index.IndexField;
import za.co.synthesis.regulatory.report.persist.index.IndexFieldValue;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecord;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecordDefinition;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.persist.types.ReportObject;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;
import za.co.synthesis.regulatory.report.validate.ValidationUtils;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Rule;
import za.co.synthesis.rule.support.StringList;
import za.co.synthesis.rule.support.Util;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static za.co.synthesis.regulatory.report.security.SecurityHelper.getAuthMeta;

/**
 * Created by jake on 7/27/17.
 */
public class DataLogic {
  static final Logger logger = LoggerFactory.getLogger(DataLogic.class);

  private IDataStore dataStore;
  private SystemInformation systemInformation;
  private JsonSchemaTransform jsonSchemaTransform;
  private ValidationEngineFactory validationEngineFactory;
  
  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  @Required
  public void setDataStore(IDataStore dataStore) {
    this.dataStore = dataStore;
  }

  @Required
  public void setValidationEngineFactory(ValidationEngineFactory validationEngineFactory) {
    this.validationEngineFactory = validationEngineFactory;
  }

  public IDataStore getDataStore() {
    return dataStore;
  }

  @Required
  public void setJsonSchemaTransform(JsonSchemaTransform jsonSchemaTransform) {
    this.jsonSchemaTransform = jsonSchemaTransform;
  }

  public ReportEventContext getReportEventContext(SecurityContext securityContext) {
    Authentication authentication = securityContext.getAuthentication();
    return getReportEventContext(authentication);
  }

  public ReportEventContext getReportEventContext(Authentication authentication) {
    /**
     * "If you want to make use of the contents of the SecurityContext contents during a request,
     * then it must have passed through the security filter chain. Otherwise the SecurityContextHolder
     * will not have been populated and the contents will be null."
     *
     * https://docs.spring.io/spring-security/site/docs/3.0.x/reference/security-filter-chain.html#d0e2938 */
//    Authentication authentication = SecurityHelper.getAuthMeta();
    if (authentication == null){
      logger.warn("Authentication null, no user information obtainable - Defaulting as 'SYSTEM' user context.");
      return new ReportEventContext("SYSTEM", LocalDateTime.now());
    }
    return new ReportEventContext(getAuthMeta(authentication, systemInformation.getConfigStore()), LocalDateTime.now());
  }

  //----------------------------------------------------------------------------------------------------
  //  System Logic
  //----------------------------------------------------------------------------------------------------
  private String getNewState(String reportSpace, EventType eventType, String channelName, String currentState) {
    String result = null;
    List<TransitionData> transitions = systemInformation.getReportSpaceTransitionData(reportSpace);

    if (transitions != null) {
      for (TransitionData transition : transitions) {
        if (transition.getEvent().equals(eventType)) {
          if (channelName != null && transition.getChannels() != null) {
            if (transition.getChannels().size() > 0 && !transition.getChannels().contains(channelName))
              continue;
          }
          if (currentState != null && transition.getCurrentStates() != null) {
            if (transition.getCurrentStates().size() > 0 && !transition.getCurrentStates().contains(currentState))
              continue;
          }
          result = transition.getNewState();
          break;
        }
      }
    }
    return result;
  }

  public String getStateForAddOrUpdateReport(String reportSpace, ReportData reportData, String currentState, String channelName) throws Exception {
    String newState;
    if (currentState == null) {
      // New Reports
      EventType eventType = EventType.NewReportOnChannel;
      newState = getNewState(reportSpace, eventType, channelName, null);
      if (newState == null) {
        eventType = EventType.NewReportForReportSpace;
        newState = getNewState(reportSpace, eventType, channelName, null);
      }
      if (newState == null) {
        StateData stateData = systemInformation.getReportSpaceDefaultStateData(reportSpace);
        if (stateData != null)
          newState = stateData.getName();
      }
    }
    else {
      // Existing Reports
      EventType eventType = EventType.UpdatedReportOnChannel;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
      if (newState == null) {
        eventType = EventType.UpdatedReportForReportSpace;
        newState = getNewState(reportSpace, eventType, channelName, currentState);
      }
      if (newState == null) {
        newState = currentState;
      }
    }
    newState = composeFromReportData(reportData, newState);
    return newState;
  }

  public String getStateForValidationResult(String reportSpace, ReportData reportData, String channelName, String currentState, boolean valid) throws Exception {
    String newState;
    if (valid) {
      EventType eventType = EventType.ReportValidationSuccess;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    else {
      EventType eventType = EventType.ReportValidationFailure;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    if (newState == null) {
      newState = currentState;
    }
    return composeFromReportData(reportData, newState);
  }
  
  //do we need the "valid" param?  I think not.
  public String getStateForDocSightingUploadsComplete(String reportSpace, ReportData reportData, String channelName, String currentState) throws Exception {
    String newState;
    boolean valid = true;
    //    List<DocumentInfo> documentList = dataLogic.getDocumentsForReport(channel, trnReference);
    List<DocumentInfo> documentList = getDocumentInfoForReportOrTrnReference(channelName, reportData.getTrnReference(), reportData);
    if (documentList != null) {
      //Cycle through required docs and check that each has been uploaded
      for (DocumentInfo doc : documentList) {
        valid = valid ? doc.getUploaded() : valid;
      }
    } else if (reportData != null){
      throw new Exception("No configured documents required for this report");
    }
    if (valid) {
      EventType eventType = EventType.DocumentsUploadValidationSuccess;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    else {
      EventType eventType = EventType.DocumentsUploadValidationFailure;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    if (newState == null) {
      newState = currentState;
    }
    return composeFromReportData(reportData, newState);
  }
  
  public String getStateForDocSightingAcknowledgedComplete(String reportSpace, ReportData reportData, String channelName, String currentState) throws Exception {
    String newState;
    boolean valid = true;
    //    List<DocumentInfo> documentList = dataLogic.getDocumentsForReport(channel, trnReference);
    List<DocumentInfo> documentList = getDocumentInfoForReportOrTrnReference(channelName, reportData.getTrnReference(), reportData);
    if (documentList != null) {
      //Cycle through required docs and check that each has been acknowledged
      for (DocumentInfo doc : documentList) {
        valid = valid ? doc.getAcknowledged() : valid;
      }
    } else if (reportData != null){
      throw new Exception("No configured documents required for this report");
    }
    if (valid) {
      EventType eventType = EventType.DocumentsAcknowledgedValidationSuccess;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    else {
      EventType eventType = EventType.DocumentsAcknowledgedValidationFailure;
      newState = getNewState(reportSpace, eventType, channelName, currentState);
    }
    if (newState == null) {
      newState = currentState;
    }
    return composeFromReportData(reportData, newState);
  }


  public List<ReportDataWithState> getReportHistory(String reportSpace, String trnReference, Authentication authentication) {
    List<ReportDataWithState> reportHistory = new ArrayList<>();
    List<ReportInfo> reportList= getReportInfoHistory(reportSpace, trnReference, authentication);
    if (reportList == null || reportList.size()==0)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    for (ReportInfo report : reportList){
      reportHistory.add(new ReportDataWithState(report.getReportData(), report.getState()));
    }
    return reportHistory;
  }


  public ReportDataWithState getReportData(String schema, String channelName, String reportSpace, String trnReference, Authentication authentication) {
    ReportInfo report = getReportInfo(schema, channelName, reportSpace, trnReference, authentication);
    if (report == null)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    return new ReportDataWithState(report.getReportData(), report.getState());
  }


  public List<ReportInfo> getReportInfoHistory(String reportSpace, String trnReference, Authentication authentication) {
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      String currentUserName = authentication.getName();
      logger.debug("User '{}' accessing 'getReportHistory' end-point. Authorities Granted: {}",currentUserName,authentication.getAuthorities());
      authentication.setAuthenticated(false);
    }

    List<ReportInfo> reportHistory = getReportHistory(reportSpace, trnReference);
    if (reportHistory == null)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    return reportHistory;
  }


  public ReportInfo getReportInfo(String schema, String channelName, String reportSpace, String trnReference, Authentication authentication) {
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      String currentUserName = authentication.getName();
      logger.debug("User '{}' accessing 'getReport' end-point. Authorities Granted: {}",currentUserName,authentication.getAuthorities());
      authentication.setAuthenticated(false);
    }

    Channel channel = null;
    if (reportSpace == null && channelName != null) {
      channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null) {
      logger.warn("Invalid channelName and/or reportSpace provided (\""+channelName+"\" / \""+reportSpace+"\").");
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
    if ((schema == null || schema.equalsIgnoreCase("default")) && (channel != null && channel.getDefaultSchema() != null)){
      schema = channel.getDefaultSchema();

      //otherwise ... default to a hard "genv3" schema.
      if (schema == null || schema.equalsIgnoreCase("default") || schema.trim().isEmpty()) {
        schema = "genv3";
      }
    }

    if (schema.isEmpty() || !schemaExists(schema)){
      logger.warn("Invalid schema provided (\""+schema+"\")");
      if (caseInsensitiveSchemaExists(schema)){
        logger.debug("Schema provided (\""+schema+"\") matches (case-insensitive) to existing schema");
        schema = getSchemaCasing(schema);
      }
      if (schema == null || schema.isEmpty()){
        schema = channel.getDefaultSchema();
        logger.debug("Applying default channel ("+channelName+") schema : "+(schema!=null?schema:"[NONE CONFIGURED]"));
      }
      if (schema == null || schema.isEmpty() || !schemaExists(schema)){
        logger.error("No valid schema provided or assignable - channel ("+channelName+") has no valid, default schema.");
        throw new PreconditionFailedException("A valid schema must be specified as a parameter and/or applied to the channel \""+channelName+"\" configuration.");
      } else {
        logger.debug("Schema updated to \""+schema+"\"");
      }
    }

    ReportInfo report = getReport(reportSpace, trnReference, schema);
    if (report == null)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    return report;
  }


  public boolean schemaExists(String schema){
    for (String s : systemInformation.getConfigStore().getTypeSchemas("report_core")){
      if (schema.equals(s)){
        return true;
      }
    }
    return false;
  }

  public boolean caseInsensitiveSchemaExists(String schema){
    for (String s : systemInformation.getConfigStore().getTypeSchemas("report_core")){
      if (schema.equalsIgnoreCase(s)){
        return true;
      }
    }
    return false;
  }

  public String getSchemaCasing(String schema){
    for (String s : systemInformation.getConfigStore().getTypeSchemas("report_core")){
      if (schema.equalsIgnoreCase(s)){
        return s;
      }
    }
    return "";
  }

  //----------------------------------------------------------------------------------------------------
  //  Untransformed Setters
  //----------------------------------------------------------------------------------------------------
  public ValidationResult setReportData(String sourceSchema, String channelName, ReportData report, Authentication authentication){
    Channel channel = systemInformation.getChannel(channelName);
    boolean isSchemaDefaulted = false;
    if (channel == null)
      throw new PreconditionFailedException("A valid channelName must be specified");
    String targetSchema = channel.getDefaultSchema();
    if (targetSchema == null || targetSchema.equalsIgnoreCase("default") || targetSchema.trim().isEmpty()) {
      targetSchema = "genv3";
    }

    if ((sourceSchema == null || sourceSchema.equalsIgnoreCase("default") || sourceSchema.trim().isEmpty()) && (channel.getDefaultSchema() != null)){
      sourceSchema = channel.getDefaultSchema();
      //otherwise ... default to a hard "genv3" schema.
      if (sourceSchema == null || sourceSchema.equalsIgnoreCase("default") || sourceSchema.trim().isEmpty()) {
        sourceSchema = "genv3";
      }
      isSchemaDefaulted = true;
    }

    if ((report == null) || (report.getTrnReference() == null))
      throw new PreconditionFailedException("Report data must contain a trnReference");

    //Determine the source data schema provided (Best effort):
    String dataSourceSchema = jsonSchemaTransform.getReportSchema(report.getReport(), sourceSchema);
    if (!dataSourceSchema.equalsIgnoreCase(sourceSchema) && !isSchemaDefaulted){
      logger.info("...Determined schema ("+dataSourceSchema+") differs from the schema stipulated ("+sourceSchema+")");
    }

    JSObject reportMap = JsonSchemaTransform.mapToJson(report.getReport());

    boolean autoConvertSchema = false;
    if ((!isSchemaDefaulted && !sourceSchema.equalsIgnoreCase(targetSchema)) || (isSchemaDefaulted && !dataSourceSchema.equalsIgnoreCase(targetSchema))) {
      try {
        String obj = systemInformation.getConfigStore().getEnvironmentParameters().getProperty("autoConvertSchema");
        if (obj != null) {
          autoConvertSchema = "True".equalsIgnoreCase(obj);//"Persistence".equalsIgnoreCase(obj) || "Validation".equalsIgnoreCase(obj);
        }
      } catch (Exception e) {
      }
      if (autoConvertSchema) {
        reportMap = jsonSchemaTransform.convert(reportMap, (isSchemaDefaulted?dataSourceSchema:sourceSchema), targetSchema);
        logger.info("...Converting source schema (" + (isSchemaDefaulted ? dataSourceSchema + "<defaulted>" : sourceSchema + "<explicit>") + ") to target schema (" + targetSchema + ").");
      } else {
        logger.trace("AutoConversion recommended for cases where the source schema("+(isSchemaDefaulted?dataSourceSchema:sourceSchema)+") differs from the target schema ("+targetSchema+").");
        targetSchema = (isSchemaDefaulted?dataSourceSchema:sourceSchema);
      }
    }
    report = new ReportDataWithDecision(report.getMeta(), reportMap);


    String trnReference = report.getTrnReference();

    String reportSpace = channel.getReportSpace();

    ValidationStats stats = new ValidationStats();

    List<ResultEntry> validationResults = null;
    Validator validator = null;
    try {
      validator = getValidator(channelName);
      validationResults = validator.validate(new MapFinsurvContext(report.getReport()),getCustomValues(report.getMeta()),stats);
    }
    catch (Exception e) {
      logger.error("Cannot validate transaction", e);
    }

    ValidationResultType resultType = ValidationResultType.Failure;
    if (stats.getErrorCount() == 0) {
      resultType = validator==null?ValidationResultType.Unspecified:ValidationResultType.Success;
    }
    ValidationResult result = new ValidationResult(reportSpace, trnReference, resultType, filterValidationResults(validationResults));

    ReportInfo reportInfo = getReport(reportSpace, trnReference);
    String currentState = reportInfo != null ? reportInfo.getState(): null;

    String newState = null;
    try {
      newState = getStateForAddOrUpdateReport(reportSpace, report, currentState, channelName);
    } catch (Exception e) {
      logger.error("Cannot determine report state ("+reportSpace+":"+trnReference+")", e);
    }
    try {
      newState = getStateForValidationResult(reportSpace, report, channelName, newState, resultType.equals(ValidationResultType.Success));
    }
    catch (Exception e) {
      logger.error("Cannot determine report state ("+reportSpace+":"+trnReference+")", e);
    }
    
    //*******************************************//
    //*****  DOCSIGHTING VALIDATION CHECKS  *****//
    //*******************************************//
    //Check that all required documents have been uploaded for the relevant trnRefrence
    try {
      newState = getStateForDocSightingUploadsComplete(reportSpace, report, channelName, newState);
    }
    catch (Exception e) {
      logger.error("Cannot determine report state ("+reportSpace+":"+trnReference+")", e);
    }
    //Check that all the required documents have been acknowledged for the relevant trnReference
    try {
      newState = getStateForDocSightingAcknowledgedComplete(reportSpace, report, channelName, newState);
    }
    catch (Exception e) {
      logger.error("Cannot determine report state ("+reportSpace+":"+trnReference+")", e);
    }
    //*******************************************//

    if (newState != null && !newState.equals(currentState)){
      logger.trace("Report ("+reportSpace+":"+trnReference+") state changed : "+(currentState!= null?currentState:"NULL")+" => "+newState);
    } else if (newState == null) {
      logger.trace("Report (" + reportSpace + ":" + trnReference + ") state indeterminate after validation : " + (currentState != null ? currentState : "NULL") + " => " + (newState != null ? newState : "NULL"));
    } else if (newState.equals(currentState)) {
      logger.trace("Report (" + reportSpace + ":" + trnReference + ") state unchanged after validation : " + (currentState != null ? currentState : "NULL") + " => " + (newState != null ? newState : "NULL"));
    }
  
  
    setReport(reportSpace, channelName, trnReference,
            new ReportInfo(newState, targetSchema, reportSpace, channelName,
                    report, result.getValidations(), getReportEventContext(getAuthMeta(systemInformation.getConfigStore()))));

    return result;
  }


  public static List<ValidationResponse> filterValidationResults(List<ResultEntry> validationResults) {
    List<ValidationResponse> result = new ArrayList<ValidationResponse>();

    if (validationResults != null) {
      for (ResultEntry resultEntry : validationResults) {
        if (resultEntry.getType() == ResultType.Error || resultEntry.getType() == ResultType.Warning)
          result.add(new ValidationResponse(resultEntry));
      }
    }
    return result;
  }

  private void processNotifications(String reportSpace, String channelName, String trnReference, StateData state, ReportInfo report) throws Exception {
    if (state != null && state.getActions().contains(ActionType.SendNotification)) {
      //TODO: THIS SHOULD BE EXTERNALIZED
      String path = "config/SendNotification/" + reportSpace + "_" + state.getName();
      String content = systemInformation.getConfigStore().getFreemarkerContent(path);

      if (content != null) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("ReportSpace", report.getReportSpace());
        params.put("Meta", report.getReportData().getMeta());
        params.put("Report", report.getReportData().getReport());
        params.put("State", report.getState());

        String defaultChannelName = getDefaultChannel(reportSpace).getChannelName();
        Channel channel = null;
        if (channelName != null && !channelName.isEmpty()){ //if a channel was specified... THEN USE IT!
          channel = systemInformation.getChannel(channelName);
        }
        if (channel == null && defaultChannelName != null) {
          channel = systemInformation.getChannel(defaultChannelName);
        }
        List<DocumentInfo> docs = getDocumentInfoListForReport(channel, report.getReportData());
        params.put("Documents", docs);

        String composed = ValidationUtils.compose(path, content, params, systemInformation.getConfigStore(), logger);

        dataStore.addNotification(new NotificationData(reportSpace, trnReference,reportSpace + "_" + state.getName(), composed));
      }
    }
  }


  /**
   *...Find/Create unique IDs for each sequence and sub-sequence in the report data.
   * This will help with correlating documents to specific monetary amounts and import exports.
   * It will also help in the diffing of versions - specifically in the case where there are
   * multiple monetary amounts and import export sequences which have been affected by deletes and
   * re-arrangements.
   */
  public void tagReportSequences(ReportInfo reportInfo){
    if (reportInfo != null && reportInfo.getReportData()!= null){
      ReportData reportData = reportInfo.getReportData();
      Map<String, Object> report = reportData.getReport();
      if (report != null && report.size() > 0 && report.containsKey(JsonConstant.MonetaryAmount)){
        //try getting the current report in the data store...
        // ...then apply some logic to determine if there is a matching sequence?
        ReportInfo prevReport = getReport(reportInfo.getReportSpace(), reportInfo.getReportData().getTrnReference());

        Object sequences = report.get(JsonConstant.MonetaryAmount);
        if (sequences instanceof List){
          for (Object sequence : (List)sequences){
            if (sequence instanceof Map){
              Map money = (Map)sequence;
              if (!money.containsKey(JsonConstant.UniqueObjectInstanceID)){
                //...Attempt to determine the UniqueID if one should already exist?
                if (prevReport != null){
                  //scan for ... items that are the same?  yeesh.
                }

                //if no previous report or unable to determine unique sequence id, then assign one.
                money.put(JsonConstant.UniqueObjectInstanceID, PersistenceObject.generateUUID());
              }
            }
          }
        }
      }
    }
  }


  public void setReport(String reportSpace, String channelName, String trnReference, ReportInfo report) {
    StateData state = systemInformation.getReportSpaceStateData(reportSpace, report.getState());

    boolean bAddToDownload = false;
    if (state != null) {
      if (state.getActions().contains(ActionType.QueueForDownload)) {
        bAddToDownload = true;
      } else if (state.getActions().contains(ActionType.CancelWithReplace)) {
        bAddToDownload = true;
        report.setDownloadType(ReportDownloadType.CancelWithReplace);
      } else if (state.getActions().contains(ActionType.Cancel)) {
        bAddToDownload = true;
        report.setDownloadType(ReportDownloadType.Cancel);
      }
    }
    //tagReportSequences(report);
    dataStore.setReport(reportSpace, trnReference, report, bAddToDownload);
    try {
      processNotifications(reportSpace, channelName, trnReference, state, report);
    } catch (Exception e) {
      logger.error("Could not compose notification for " + trnReference + " in state " + state.getName(), e);
    }
  }

  public void setReports(String reportSpace, String channelName, List<ReportInfo> reports) {
    for (ReportInfo report : reports) {
      StateData state = systemInformation.getReportSpaceStateData(reportSpace, report.getState());
      boolean bAddToDownload = false;
      if (state != null) {
        if (state.getActions().contains(ActionType.QueueForDownload)) {
          bAddToDownload = true;
        } else if (state.getActions().contains(ActionType.CancelWithReplace)) {
          bAddToDownload = true;
          report.setDownloadType(ReportDownloadType.CancelWithReplace);
        } else if (state.getActions().contains(ActionType.Cancel)) {
          bAddToDownload = true;
          report.setDownloadType(ReportDownloadType.Cancel);
        }
      }
      //tagReportSequences(report);
      dataStore.setReport(reportSpace, report.getReportData().getTrnReference(), report, bAddToDownload);

      try {
        processNotifications(reportSpace, channelName, report.getReportData().getTrnReference(), state, report);
      } catch (Exception e) {
        logger.error("Could not compose notification for " + report.getReportData().getTrnReference() + " in state " + state.getName(), e);
      }
    }
  }

  //----------------------------------------------------------------------------------------------------
  //  Untransformed Getters
  //----------------------------------------------------------------------------------------------------
  public ReportInfo getReport(String reportSpace, String trnReference) {
    return this.getReport(reportSpace, trnReference, null);
  }

  //----------------------------------------------------------------------------------------------------
  //  Transforming Getters
  //----------------------------------------------------------------------------------------------------
  public List<ReportInfo> getReportHistory(String reportSpace, String trnReference) {
    return this.dataStore.getReportHistory(reportSpace, trnReference);
  }

  public ReportInfo getReport(String reportSpace, String trnReference, String targetSchema) {
    return jsonSchemaTransform.convert(this.dataStore.getReport(reportSpace, trnReference), targetSchema);
  }

  public ReportInfoDownload getReports(String reportSpace, String fromSyncPoint, Integer maxRecords, String targetSchema) {
    ReportInfoDownload result = dataStore.getReports(reportSpace, fromSyncPoint, maxRecords);
    if (result!= null) {
      jsonSchemaTransform.convert(result.getReportInfoList(), targetSchema);
    }
    return result;
  }

  private String composeFromReportData(ReportData reportData, String stateTemplate) throws Exception {
    if (stateTemplate != null && stateTemplate.contains("<")) {
      Map<String, Object> params = new HashMap<String, Object>();
      params.put("Meta", reportData.getMeta());
      params.put("Report", reportData.getReport());
      return ValidationUtils.compose(stateTemplate, stateTemplate, params, systemInformation.getConfigStore(), logger);
    }
    return stateTemplate;
  }

  public ReportInfo setReportState(ReportInfo report, String channelName, String state, Authentication authentication) {
    try {
      String newStateName = composeFromReportData(report.getReportData(), state);
      String reportSpace = report.getReportSpace();
      channelName = channelName != null ? channelName : report.getChannelName();
      String trnReference = report.getReportData().getTrnReference();
      if (report != null) {
        StateData stateData = systemInformation.getReportSpaceStateData(reportSpace, newStateName);
        if (stateData != null) {
          ReportInfo newReport = new ReportInfo(stateData.getName(), report, getReportEventContext(authentication));
          boolean bAddToDownload = false;
          if (stateData.getActions().contains(ActionType.QueueForDownload)) {
            bAddToDownload = true;
          } else if (stateData.getActions().contains(ActionType.CancelWithReplace)) {
            bAddToDownload = true;
            newReport.setDownloadType(ReportDownloadType.CancelWithReplace);
          } else if (stateData.getActions().contains(ActionType.Cancel)) {
            bAddToDownload = true;
            newReport.setDownloadType(ReportDownloadType.Cancel);
          }
          //tagReportSequences(report);
          dataStore.setReport(reportSpace, trnReference, newReport, bAddToDownload);
          try {
            processNotifications(reportSpace, channelName, trnReference, stateData, newReport);
          } catch (Exception e) {
            logger.error("Could not compose notification for " + trnReference + " in state " + stateData.getName(), e);
          }
          return newReport;
        }
      }
    }
    catch (Exception e) {
      logger.error("Cannot set the report State", e);
    }
    return null;
  }
  

  /*
   * We ought to perhaps check if the NEW trnReference already exists and maybe whether or not it's newer than the reports with the old trnReference or not.
   * ...if it's newer, then we don't want to insert a copy of the current one with the trnReference changed - it may override "current" report with new trnReference.
   * otherwise, always create a copy of the old report and insert with NEW trnReference to create a history where the reference number is migrated.
   */
  /**
   * NOTE: The lifecycle should NEVER be defined such that a report can be renamed after a download has been created.
   * ...in such cases, a CancelWithReplace should be initiated instead and this should be handled and initiated
   * outside of the Report-data-store.
   */
  public ReportInfo renameReportReference(ReportInfo report, String newReference, Authentication authentication) {
    try {
      String reportSpace = report.getReportSpace();
      String trnReference = report.getReportData().getTrnReference();
      String channelName = report.getChannelName();
      
      //*** Check if new reference exists...   if so, throw exception!  ***//
      ReportInfo newReportExists = null;
      try {
        newReportExists = dataStore.getReport(reportSpace, newReference);
      } catch(Exception e){
        //ignore failures to retrieve the report for now and assume this
      }
      if (newReportExists != null)
        throw new PreconditionFailedException("If renaming a report, the new report reference ("+newReference+") must be a completely new, unique reference.");
      
      if (report != null) {
        StateData stateData = systemInformation.getReportSpaceStateData(reportSpace, report.getState());
        if (stateData != null) {

          //*** Check if state is valid for a rename/reference change  ***//
          if (stateData.getActions().contains(ActionType.QueueForDownload) ||
              stateData.getActions().contains(ActionType.CancelWithReplace) ||
              stateData.getActions().contains(ActionType.Cancel)) {
            throw new PreconditionFailedException("If renaming a report, the existing report ("+reportSpace+":"+trnReference+") should not be in a state ("+stateData.getName()+") where it has or will be queued for download (or cancellation).");
          }

          
          ReportData reportData = report.getReportData();
          Map<String, Object> reportReportData = reportData.getReport();
          if (reportReportData.containsKey(FieldConstant.TrnReference)) {
            reportReportData.put(FieldConstant.TrnReference, newReference);
          }
          Map<String, Object> reportMeta = reportData.getMeta();
          String associatedReferencesKey = "RDS_AssociatedReferences";
          List<String> associatedRefs = new ArrayList<String>();
          if (reportMeta.containsKey(associatedReferencesKey)) {
            Object refs = reportMeta.get(associatedReferencesKey);
            if (refs instanceof List) {
              associatedRefs = (List) refs;
            } else {
              associatedRefs.add(refs.toString());
            }
          }
          if (associatedRefs != null) {
            associatedRefs.add(trnReference);
            reportMeta.put(associatedReferencesKey, associatedRefs);
          }
          
          reportReportData.put(FieldConstant.TrnReference, newReference);
          boolean bAddToDownload = false;
          dataStore.changeReportKey(reportSpace, trnReference, newReference); //do this first - otherwise we may well run into issues with indices (Especially with ReportFullText table).
          dataStore.setReport(reportSpace, newReference, report, bAddToDownload);
          try {
            processNotifications(reportSpace, channelName, newReference, stateData, report);
          } catch (Exception e) {
            logger.error("Could not compose notification for " + newReference + " in state " + stateData.getName(), e);
          }
          return report;
        }
      }
    }
    catch (Exception e) {
      logger.error("Cannot set the report State", e);
    }
    return null;
  }

  public void setEvaluationDecision(FullyReferencedEvaluationDecision evaluation) {
    dataStore.setEvaluationDecision(evaluation);
  }
  
  
  public void setEvaluationDecisionLog(FullyReferencedEvaluationDecisions evalDecisionLogs) {
    dataStore.setEvaluationDecisionLog(evalDecisionLogs);
  }
  
  public FullyReferencedEvaluationDecision getEvaluationDecision(String reportSpace, String trnReference) {
    return dataStore.getEvaluationDecision(reportSpace, trnReference);
  }

  public DecisionDownload getDecisions(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return dataStore.getDecisions(reportSpace, fromSyncPoint, maxRecords);
  }

  public DocumentDataDownload getDocuments(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return dataStore.getDocuments(reportSpace, fromSyncPoint, maxRecords);
  }
  public ChecksumDataDownload getChecksums(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return dataStore.getChecksums(reportSpace, fromSyncPoint, maxRecords);
  }

  public AccountEntryDownload getAccountEntries(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return dataStore.getAccountEntries(reportSpace, fromSyncPoint, maxRecords);
  }

  public AccountEntryData getAccountEntry(String reportSpace, String reportKey) {
    return dataStore.getAccountEntry(reportSpace,reportKey);
  }


  public void setAccountEntry(String reportSpace, String reportKey, AccountEntryData data, ReportEventContext eventContext) throws Exception{
    String failedConditions = "";
    //Basic checks for mandatory values ...
    failedConditions += (reportSpace == null || reportSpace.trim().isEmpty() ? "ReportSpace" : "");
    failedConditions += (data.getTrnReference() == null || data.getTrnReference().trim().isEmpty() ? (!failedConditions.isEmpty()?", ":"")+"TrnReference" : "");
    failedConditions += (data.getContent() == null || data.getContent().getContent().length == 0 ? (!failedConditions.isEmpty()?", ":"")+"Content" : "");
    dataStore.setAccountEntry(reportSpace,reportKey,data);
  }


//  public DocumentType getDocumentType(String typeName) {
//    for (DocumentType docType : systemInformation.getDocumentTypes()) {
//      if (docType.getType().equals(typeName))
//        return docType;
//    }
//    return null;
//  }

  public static String getDocumentHandle(DocumentData doc) {
    return getDocumentScopeKey(doc.getTrnReference(), doc.getScope().getName(), doc.getSequence(), doc.getSubSequence(), doc.getType());
  }

  public static String getAccountEntryReportKey(AccountEntryData accountEntryData){
    return getReportKey(accountEntryData.getReportSpace(),accountEntryData.getTrnReference());
  }

/*
  public HashMap<String, DocumentInfo> getUploadedDocumentInfoMapForReport(Channel channel, ReportData report) {
    MapFinsurvContext context = new MapFinsurvContext(report.getReport());
    HashMap<String, DocumentInfo> uploaded = new HashMap<String, DocumentInfo>();
    String trnReference = (String) context.getTransactionField("TrnReference");
    if (trnReference != null) {
      List<DocumentInfo> docList = getCombinedDocumentInfoListForReport(channel, report);
      if (docList != null && docList.size() > 0) {
        for (DocumentInfo doc : docList) {
          //DocumentType docType = getDocumentType(doc.getType());
          DocumentType docType = new DocumentType();
          docType.setType(doc.getType());
          docType.setScope(doc.getScope());
          docType.setDescription(doc.getDescription());

          boolean docExists = dataStore.hasDocument(
                                channel.getReportSpace(),
                                doc.getDocumentHandle()
                              );
          String scopeKey = getDocumentScopeKey(
                  channel.getReportSpace(),
                  trnReference,
                  doc.getScope().getName(),
                  doc.getSequence(),
                  doc.getSubSequence(),
                  docType.getType());
          if (docExists) {
            doc = dataStore.getDocument(channel.getReportSpace(), scopeKey);
          }
          DocumentInfo docInfo = new DocumentInfo(doc,
                  docType.getDescription(),
                  docExists
          );
          docInfo.setDocumentHandle(scopeKey);
          uploaded.put(scopeKey,
                  docInfo
          );
        }
      }
    }
    return uploaded;
  }
   */



  public static Object getFieldValue(ReportInfo report, CriterionType fieldType, String fieldName){
    Object val = null;
    if (report != null) {
      if (fieldType.equals(CriterionType.Meta)) {
        val = report.getReportData().getMeta().get(fieldName);
      } else if (fieldType.equals(CriterionType.Report)) {
        val = report.getReportData().getReport().get(fieldName);
      } else if (fieldType.equals(CriterionType.System)) {
        val = report.getSystemValue(fieldName);
      }
    }
    return val;
  }

  public static Map<String, List<CriterionData>> mapByCriterionName(List<CriterionData> criteria) {
    Map<String, List<CriterionData>> result = new HashMap<String, List<CriterionData>>();
    for (CriterionData criterion : criteria) {
      String key = criterion.getType().name() + criterion.getName();
      if (!result.containsKey(key)) {
        result.put(key, new ArrayList<CriterionData>());
      }
      result.get(key).add(criterion);
    }
    return result;
  }



  public static ListResult convertToListResult(ListData listDefinition, Collection<ReportInfo> reportInfoList) {
    ListResult result = new ListResult();
    List<Map<String, String>> rows = result.getRows();
    List<ListData.Field> fields = listDefinition.getFields();
    List<String> headers = result.getHeaders();
    //populate headers
    for (ListData.Field field : listDefinition.getFields()) {
      headers.add(field.getLabel());
    }
    //populate data
    for (ReportInfo report : reportInfoList) {
      Map<String, String> row = new HashMap<String, String>();
      for (ListData.Field field : fields) {
        Object val = getFieldValue(report, field.getCriterionType(), field.getName());
        row.put(field.getLabel(), (val != null ? val.toString() : ""));
      }
      if (row.size() > 0) {
        row.put("ReportSpace", report.getReportSpace());
        row.put("ReportingSpace", report.getReportSpace()); /*DEPRECATED NAMING CONVENTION*/
        row.put("TrnReference", report.getReportData().getTrnReference());
        rows.add(row);
      }
    }

    return result;
  }


/*
  public List<DocumentInfo> getCombinedDocumentInfoListForReportOld(Channel channel, ReportData report, HashMap<String, DocumentInfo> uploaded) {
    HashMap<String, DocumentInfo> scopedDocInfo = new HashMap<>();
    MapFinsurvContext context = new MapFinsurvContext(report.getReport());
    String trnReference = (String) context.getTransactionField("TrnReference");

    FlowType flow;
    String flowValue = (String)context.getTransactionField("Flow");
    if (flowValue.equalsIgnoreCase("IN") || flowValue.equalsIgnoreCase("I"))
      flow = FlowType.Inflow;
    else
      flow = FlowType.Outflow;
    for (int i=0; i<context.getMoneySize(); i++){
      String cat = (String)context.getMoneyField(i, "CategoryCode");
      String subCat = (String)context.getMoneyField(i, "SubCategoryCode");
      String category = (subCat!=null && !subCat.isEmpty())?cat+"/"+subCat : cat;
      String sequenceNo = (String)context.getMoneyField(i, "SequenceNumber");

      List<DocumentType> docTypes = new ArrayList<DocumentType>();
      List<DocumentType> tmpDocTypes = systemInformation.getDocumentTypesForCategory(channel.getChannelName(), flow, category);
      if (tmpDocTypes != null)
        docTypes.addAll(tmpDocTypes);
      if (!category.equals(cat)) {
        tmpDocTypes = systemInformation.getDocumentTypesForCategory(channel.getChannelName(), flow, cat);
        if (tmpDocTypes != null)
          docTypes.addAll(tmpDocTypes);
      }

      for (DocumentType docType : docTypes){
        String docScope = docType.getScope().getName();
        String scopeKey = getDocumentScopeKey(channel.getReportSpace(), trnReference, docScope, i, null, docType.getType());
        if (docScope.equals("importexport")){
          for (int j=0; j<context.getImportExportSize(i); j++) {
            scopeKey = getDocumentScopeKey(channel.getReportSpace(), trnReference, docScope, i, j, docType.getType());
            addDocInfoToMap(channel, trnReference, i, j, scopeKey, scopedDocInfo, docType, uploaded);
          }
        } else if (docScope.equals("money")){
          addDocInfoToMap(channel, trnReference, i, null, scopeKey, scopedDocInfo, docType, uploaded);
        } else if (docScope.equals("transaction")) {
          addDocInfoToMap(channel, trnReference, null, null, scopeKey, scopedDocInfo, docType, uploaded);
        }
      }
    }
    List<DocumentInfo> results = new ArrayList<>();
    results.addAll(scopedDocInfo.values());
    return results;
  }
*/

  private final Map<String, Validator> validationValidators = new HashMap<String, Validator>();
  private DefaultSupporting supporting = new DefaultSupporting();

  public synchronized Validator getValidator(String packageName) throws Exception {
    Validator validator = null;
    if (validator == null && validationEngineFactory != null && supporting != null) {
      try {
        ValidationEngine engine = validationEngineFactory.getEngineForCurrentDate(packageName);

        supporting.setCurrentDate(LocalDate.now());
        supporting.setGoLiveDate(Util.date("2013-08-18"));
        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);

        validator = engine.issueValidator();
        validationValidators.put(packageName, validator);
      }
      catch (Exception e) {
        if (validationValidators.containsKey(packageName)){
          validator = validationValidators.get(packageName);
        }
        if (validator == null) {
          logger.error("Cannot get Validation Validator", e);
          throw e;
        }
      }
    }
    if (validator == null){
      throw new ValidationException("Cannot get Validation Validator because factory not provided.");
    }
    return validator;
  }

  private final Map<String, Validator> documentValidators = new HashMap<String, Validator>();
  public synchronized Validator getDocumentValidator(String packageName) throws Exception {
    Validator validator = null;
    if (validator == null && validationEngineFactory != null && supporting != null) {
      try {
        ValidationEngine engine = validationEngineFactory.getDocumentEngineForCurrentDate(packageName);

        supporting.setCurrentDate(LocalDate.now());
        supporting.setGoLiveDate(Util.date("2013-08-18"));
        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);

        validator = engine.issueValidator();
        documentValidators.put(packageName, validator);
      }
      catch (Exception e) {
        if (documentValidators.containsKey(packageName)){
          validator = documentValidators.get(packageName);
        }
        if (validator == null) {
          logger.error("Cannot get Document Validator", e);
          throw e;
        }
      }
    }
    if (validator == null) {
      throw new ValidationException("Cannot get Document Validator");
    }
    return validator;
  }

  private MapCustomValues getCustomValues(Map<String, Object> meta) {
    if (meta == null) {
      meta = new JSObject();
    }
    if (!meta.containsKey("DealerType"))
      meta.put("DealerType", "AD");

    return new MapCustomValues(meta);
  }

  public List<DocumentInfo> getCombinedDocumentInfoListForReport(Channel channel, ReportData report) {
    ValidationStats stats = new ValidationStats();

    List<ResultEntry> documentResults = null;
    Validator validator = null;
    try {
      validator = getDocumentValidator(channel.getChannelName());
      if (validator != null) {
        documentResults = validator.validate(new MapFinsurvContext(report.getReport()), getCustomValues(report.getMeta()), stats);
      }
    }
    catch (Exception e) {
      logger.error("Cannot determine documents for transaction", e);
    }

    HashMap<String, DocumentInfo> scopedDocInfo = new HashMap<>();
    if (documentResults != null) {
      for (ResultEntry entry : documentResults) {
        if (entry.getType() == ResultType.Document) {
          Integer moneyIndex = entry.getMoneyInstance() >= 0 ? entry.getMoneyInstance()+1 : null;
          Integer ieIndex = entry.getImportExportInstance() >= 0 ? entry.getImportExportInstance()+1 : null;
          String scopeKey = getDocumentScopeKey(report.getTrnReference(),
                  entry.getScope().getName(), moneyIndex, ieIndex, entry.getCode());

          DocumentType documentType = new DocumentType();
          documentType.setType(entry.getCode());
          documentType.setScope(entry.getScope());
          documentType.setDescription(entry.getMessage());

          //TODO: check if document is acknowledged?

          addDocInfoToMap(channel, report.getTrnReference(), moneyIndex, ieIndex, scopeKey, scopedDocInfo, documentType);
        }
      }
    }
    List<DocumentInfo> results = new ArrayList<>();
    results.addAll(scopedDocInfo.values());
    return results;
  }


  private void addDocInfoToMap(Channel channel,
                               String trnReference,
                               Integer sequence,
                               Integer subSequence,
                               String scopeKey,
                               Map<String, DocumentInfo> scopedDocInfo,
                               DocumentType docType) {
    FileData fileData = null;

    DocumentData documentData = dataStore.getDocument(channel.getReportSpace(), scopeKey);
    DocumentInfo docInfo = new DocumentInfo(documentData);
    docInfo.setDescription(docType.getDescription());
    scopedDocInfo.put(scopeKey, docInfo);
  }


  public List<DocumentInfo> getDocumentInfoListForTrnReference(Channel channel, String trnReference) {
    return getDocumentInfoListForReport(channel, dataStore.getReport(channel.getReportSpace(), trnReference).getReportData());
  }

  /**
   * return the docsighting for the report provided - both required and completed.
   * @param channel
   * @param report
   * @return
   */
  public List<DocumentInfo> getDocumentInfoListForReport(Channel channel, ReportData report) {
    HashMap<String, DocumentInfo> scopedDocInfo = new HashMap<>();

    return getCombinedDocumentInfoListForReport(channel, report);
  }
  /**
   * return the docsighting for the report provided - both required and completed.
   * @param channel
   * @param trnReference
   * @return
   */
  public List<DocumentInfo> getAdhocDocumentInfoListForReport(Channel channel, String trnReference) {
    List<DocumentData> adhocDocList;
    adhocDocList = dataStore.getDocumentsByReference(channel.getReportSpace(), trnReference);
    adhocDocList = adhocDocList==null?new ArrayList<>():adhocDocList;
    List<DocumentInfo> adhocDocInfoList = new ArrayList<>();
    for (DocumentData doc : adhocDocList){
      adhocDocInfoList.add(new DocumentInfo(doc));
    }
    return adhocDocInfoList;
  }
/*
  private void addDocInfoToMap(Channel channel,
                                     String trnReference,
                                     Integer sequence,
                                     Integer subSequence,
                                     String scopeKey,
                                     Map<String, DocumentInfo> scopedDocInfo,
                                     DocumentType docType,
                                     Map<String, DocumentInfo> uploaded){
    DocumentInfo uploadedDoc = (uploaded!=null?uploaded.get(scopeKey):null);
    String documentHandle = null;
    FileData fileData = null;
    boolean isUploaded = false;
    //create a revised document info object
    DocumentInfo docInfo = new DocumentInfo(documentHandle, channel.getReportSpace(), trnReference,
            docType.getScope(), docType.getType(), docType.getDescription(), sequence, subSequence, isUploaded);
    if (uploadedDoc != null) {
      documentHandle = uploadedDoc.getDocumentHandle();
      fileData = dataStore.getDocumentContent(channel.getReportSpace(), documentHandle);
      isUploaded = fileData != null && fileData.getContent() != null && fileData.getContentType() != null;
      docInfo = uploadedDoc;
    }
    if (documentHandle == null || documentHandle.isEmpty()){
      documentHandle = getDocumentHandle(docInfo);
    }
    //check again for uploaded content based on revised doc handle...
    if (!isUploaded){
      fileData = dataStore.getDocumentContent(channel.getReportSpace(), documentHandle);
      isUploaded = fileData != null && fileData.getContent() != null && fileData.getContentType() != null;
      docInfo.setUploaded(isUploaded);
    }
    docInfo.setDocumentHandle(documentHandle);
    scopedDocInfo.put(scopeKey, docInfo);
  }
*/


  public List<DocumentInfo> getDocumentsForReport(Channel channel, String trnReference) {
    List<DocumentInfo> result = new ArrayList<DocumentInfo>();
    ReportInfo report = dataStore.getReport(channel.getReportSpace(), trnReference);
    List<DocumentInfo> docList = getDocumentInfoListForReport(
            channel,
            report.getReportData());
    if (docList != null && docList.size() > 0){
      for (DocumentInfo doc : docList) {
          result.add(getDocumentInfoFromDocumentData(systemInformation.getConfigStore(), dataStore, doc));
      }
    }
    return result;
  }


  public List<DocumentInfo> getUploadedDocumentsForTrnReference(Channel channel, String trnReference) {
    List<DocumentInfo> result = new ArrayList<DocumentInfo>();
    List<DocumentInfo> docList = getAdhocDocumentInfoListForReport(channel, trnReference);
    if (docList != null && docList.size() > 0){
      for (DocumentInfo doc : docList) {
          result.add(getDocumentInfoFromDocumentData(systemInformation.getConfigStore(), dataStore, doc));
      }
    }
    return result;
  }


  public DocumentInfo setDocumentContent(String reportSpace, String documentHandle, FileData data) {
    return setDocumentContent(reportSpace, documentHandle, data, null);
  }

  public DocumentInfo setDocumentContent(String reportSpace, Channel channel, String trnReference, String documentHandle, FileData data, String description) {
    if (documentHandle.contains(trnReference)) {
      return setDocumentContent(reportSpace, channel, documentHandle, data, description);
    } else {
      return setDocumentContent(reportSpace, channel, getDocumentScopeKey(trnReference, null, null, null, documentHandle), data, description);
    }
  }

  public DocumentInfo setDocumentContent(String reportSpace, String documentHandle, FileData data, String description) {
    Channel channel = getDefaultChannel(reportSpace);
    if (channel == null){
      logger.warn("Unable to get default channel for given reportSpace: "+reportSpace);
    }
    return setDocumentContent(reportSpace, channel, documentHandle, data, description);
  }

  public DocumentInfo setDocumentContent(String reportSpace, Channel channel, String documentHandle, FileData data, String description) {
    if (data!=null) {
      DocumentData document = null;//dataStore.getDocument(reportSpace, documentHandle);
      if (document == null){
        if (documentHandle != null){
          document = getDocumentDataObjFromHandle(channel, documentHandle);
          if (document != null) {
            document = new DocumentData(documentHandle, reportSpace, document.getTrnReference(), document.getScope(), document.getType(), description, document.getSequence(), document.getSubSequence(), false);
          }
        }
        if (document == null){
          document = new DocumentData(reportSpace, null, null, null, description, null, null);
        }
        document.setDocumentHandle(documentHandle);
      }
      if (document != null) {
        if (document.getReportSpace() == null || document.getTrnReference() == null || data == null) {
          throw new PreconditionFailedException("Error while attempting to store the document - ensure valid file handle is provided (as Provided by: getReportDocuments, getPotentialDocs, getDocumentInfoListForReportRef, getDocumentInfoListForReport or getUploadedReportDocuments APIs) - alternatively, please make use of the setAdhocDocument API.");
        } else {
          dataStore.setDocumentContent(document, data);
          return getDocumentInfoFromDocumentData(systemInformation.getConfigStore(), dataStore, document);
        }
      }
    }
    return null;
  }

  public ChecksumInfo setChecksumContent(String reportSpace, String valueDate, FileData data) {
    if (data!=null) {
      ChecksumData checksumData = null;//dataStore.getDocument(reportSpace, documentHandle);
      if (checksumData == null){
        if (valueDate != null){
            checksumData = new ChecksumData(data.getFilename(), data.getContentType(), valueDate, reportSpace, data.getContent());
        }
      }
      if (checksumData != null) {
        if (checksumData.getReportSpace() == null || checksumData.getValueDate() == null || data == null) {
          throw new PreconditionFailedException("Error while attempting to store the checksum.");
        } else {
          dataStore.setChecksumContent(checksumData);
          return getChecksumInfoFromChecksumData(systemInformation.getConfigStore(), dataStore, checksumData);
        }
      }
    }
    return null;
  }

  public DocumentInfo setDocument(DocumentInfo info, FileData data) {
    if (info != null && data != null) {
      info.setFilename(data.getFilename());
      info.setFileType(data.getContentType());
    }
    DocumentData doc = dataStore.setDocumentContent(info, data);
    return getDocumentInfoFromDocumentData(systemInformation.getConfigStore(), dataStore, doc);
//    //get the rest of the document type info...
//    DocumentInfo docInfo = new DocumentInfo(doc, info.getDescription(), dataStore.hasDocument(info.getReportSpace(), info.getDocumentHandle()));
//    return docInfo;
  }
  public ChecksumInfo setChecksum(ChecksumInfo info) {
    ChecksumData checksumData = dataStore.setChecksumContent(info);
    return getChecksumInfoFromChecksumData(systemInformation.getConfigStore(), dataStore, checksumData);
  }

  public DocumentInfo setDocumentAcknowledgement(DocumentInfo info, Boolean acknowledged, String acknowledgementComment) {
    FileData data = dataStore.getDocumentContent(info.getReportSpace(), info.getDocumentHandle());
    DocumentData doc = dataStore.setDocumentContent(info, data);
    //get the rest of the document type info...
    DocumentInfo docInfo = new DocumentInfo(doc, dataStore.hasDocument(info.getReportSpace(), info.getDocumentHandle()));
    return docInfo;
  }

  public DocumentInfo setDocumentAcknowledgement(String reportSpace, String documentHandle, Boolean acknowledged, String acknowledgementComment) {
    FileData fileData = null;
    if (dataStore.hasDocument(reportSpace, documentHandle)) {
      fileData = dataStore.getDocumentContent(reportSpace, documentHandle);
    }
    return null;
  }

  public boolean hasDocument(String reportSpace, String documentHandle) {
    return dataStore.hasDocument(reportSpace, documentHandle);
  }
  public boolean hasChecksum(String reportSpace, String valueDate) {
    return dataStore.hasChecksum(reportSpace, valueDate);
  }

  public FileData getDocumentContent(String reportSpace, String documentHandle) {
    return dataStore.getDocumentContent(reportSpace, documentHandle);
  }
  public FileData getChecksumContent(String reportSpace, String valueDate) {
    return dataStore.getChecksumContent(reportSpace, valueDate);
  }

  public DocumentInfo getDocument(String reportSpace, String documentHandle) {
    DocumentData doc = dataStore.getDocument(reportSpace, documentHandle);
    if (doc != null) {
      return getDocumentInfoFromDocumentData(systemInformation.getConfigStore(), dataStore, doc);
    }
    return null;
  }

  public ListResult getReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    return dataStore.getReportList(reportSpace, listDefinition, searchMap, userAccessData, maxRecords, page);
  }

  public ListResult getOrderedReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, SortDefinition sortDefinition, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    return dataStore.getOrderedReportList(reportSpace, listDefinition, searchMap, sortDefinition, userAccessData, maxRecords, page);
  }



  //----------------------------------------------------------------------------------------------------
  //  Actions
  //----------------------------------------------------------------------------------------------------
  public ReportState doAction(String reportSpace, String channelName, String trnReference, String action, Authentication authentication){
    boolean actionIsValid = false;
    boolean stateValid = false;
    boolean channelValid = false;
    boolean reportSpaceValid = false;

    ReportInfo report = dataStore.getReport(reportSpace, trnReference);
    if (report != null) {
      ReportSpace reportSpaceInstance = systemInformation.getReportSpace(reportSpace);
      if (reportSpaceInstance != null) {
        //get the relevant Transition based on action and apply to the current report (and state)
        String reportState = report.getState();
        List<Transition> transitions = reportSpaceInstance.getTransitions();
        for (Transition transition : transitions){
          actionIsValid = !actionIsValid && action != null ?
                  (
                          (
                                  transition.getEvent() != null && transition.getEvent().toString().equals(action)
                          ) || (
                                  transition.getAction() != null && transition.getAction().equals(action)
                          )
                  ) : actionIsValid;
          if (transition.getEvent().toString().equals(action) ||
                  (transition.getEvent().equals(EventType.UserAction) &&
                          transition.getAction() != null &&
                          transition.getAction().equals(action))){
            //check if the current report state is valid for this transition...
            List<String> states = transition.getCurrentStates();
            if (states != null && states.size() > 0){
              stateValid = states.contains(report.getState());
            } else {
              stateValid = true;
            }
//            for (String validState : transition.getCurrentStates()) {
//              if (report.getState().equals(validState)) {
//                stateValid = true;
//                break;
//              }
//            }
//            if (!stateValid && transition.getCurrentStates().size() == 0){
//              stateValid = true;
//            }

            List<String> channels = transition.getChannels();
            if (channels != null && channels.size() > 0){
              channelValid = channels.contains(channelName);
            } else {
              channelValid = true;
            }

            List<String> spaces = transition.getReportSpaces();
            if (spaces != null && spaces.size() > 0){
              reportSpaceValid = spaces.contains(reportSpace);
            } else {
              reportSpaceValid = true;
            }

            if (stateValid && channelValid && reportSpaceValid){
              // Resolve the new state based on potential Freemarker script defining the new state

              report = setReportState(report, channelName, transition.getNewState(), authentication);
              return new ReportState( reportSpace, trnReference, report.getState());
            }
          }
        }
      }
    }

    if (report == null) {
      throw new ResourceNotFoundException("Report not found for provided reference and report space - '" + trnReference + "' @ '"+reportSpace+"'");
    } else if (!actionIsValid){
      throw new PreconditionFailedException("A valid action must be specified - '"+action+"' not configured");
    } else if (!stateValid){
      throw new PreconditionFailedException("Action '"+action+"' cannot be applied to the specified report ("+trnReference+") - current report state not allowed: "+report.getState());
    } else if (!channelValid){
      throw new PreconditionFailedException("Action '"+action+"' cannot be applied to the specified report ("+trnReference+") - channel  ("+channelName+") not allowed");
    } else if (!reportSpaceValid){
      throw new PreconditionFailedException("Action '"+action+"' cannot be applied to the specified report ("+trnReference+") - report space ("+reportSpace+") not allowed");
    }

    return null;
  }

  public static String getDocumentScopeKey(String trnReference, String scope, Integer sequence, Integer subSequence, String type){
    scope = scope==null?"adhoc":scope;
    type = type==null?"document":type;
    String scopeKey = trnReference+"."+scope;
    scopeKey += ((scope.equals("money") || scope.equals("importexport"))
            ? ((sequence != null) ? "." + sequence : "")
            : "");
    scopeKey += ((scope.equals("importexport") && subSequence != null)
            ? "." + subSequence
            : "");
    scopeKey += "." + type;
    return scopeKey;
  }

  public static DocumentData getDocumentDataObjFromHandle(Channel channel, String documentHandle){
    return getDocumentDataObjFromHandle(channel.getReportSpace(), documentHandle);
  }

  public static DocumentData getDocumentDataObjFromHandle(String reportSpaceName, String documentHandle){
    if (documentHandle != null) {
      String trnReference = null;
      String scope = null;
      String sequence = null;
      String subSequence = null;
      String type = null;
      String[] parts = documentHandle.split("\\.");
      if (parts.length == 5){  //importExport level
        trnReference = parts[0];
        scope = parts[1];
        sequence = parts[2];
        subSequence = parts[3];
        type = parts[4];
      } else if (parts.length == 4){  //monetaryAmount level
        trnReference = parts[0];
        scope = parts[1];
        sequence = parts[2];
        type = parts[3];
      } else if (parts.length == 3){  //transaction level
        trnReference = parts[0];
        scope = parts[1];
        type = parts[2];
      } else if (parts.length >= 1){  //...bare minimum?
        trnReference = parts[0];
      }
      if ((trnReference != null && !trnReference.isEmpty()) &&
            (scope !=null && !scope.isEmpty()) &&
            (type != null && !type.isEmpty())){
        DocumentData document = new DocumentData(documentHandle, reportSpaceName, trnReference, getScope(scope), type, null , parseIntDef(sequence, null), parseIntDef(subSequence, null));
//        document.setDocumentHandle(documentHandle);
        //TODO: check if the document exists or if the content has been uploaded etc?  Just don't get stuck in a loop.
        return document;
      }
    }
    return null;
  }


  public static Long parseLongDef(String str, Long def){
    try{
      return Long.parseLong(str);
    } catch(Exception e){}
    return def;
  }

  public static Integer parseIntDef(String str, Integer def){
    try{
      return Integer.parseInt(str);
    } catch(Exception e){}
    return def;
  }

  public static Scope getScope(String scopeName){
    if(Scope.Transaction.getName().equals(scopeName)){
      return Scope.Transaction;
    } else if(Scope.Money.getName().equals(scopeName)){
      return Scope.Money;
    } if(Scope.ImportExport.getName().equals(scopeName)){
      return Scope.ImportExport;
    }
    return null;
  }

  public static DocumentInfo getDocumentInfoFromDocumentData(IConfigStore configStore, IDataStore dataStore, DocumentData document){
    boolean uploaded = isDocumentUploaded(dataStore, document);
    DocumentInfo result = new DocumentInfo(document, uploaded);
    if (uploaded){
      FileData file = dataStore.getDocumentContent(document.getReportSpace(), document.getDocumentHandle());
      if (file != null) {
        result.setFilename(file.getFilename());
        result.setFileType(file.getContentType());
      }
    }
    return result;
  }

  public static ChecksumInfo getChecksumInfoFromChecksumData(IConfigStore configStore, IDataStore dataStore, ChecksumData checksum){
    ChecksumInfo result = new ChecksumInfo(checksum);
    if (checksum!=null){
      FileData file = dataStore.getChecksumContent(checksum.getReportSpace(), checksum.getValueDate());
      if (file != null) {
        result.setFilename(file.getFilename());
        result.setFileType(file.getContentType());
      }
    }
    return result;
  }


  public static boolean isDocumentUploaded(IDataStore dataStore, DocumentData document) {
    if (document != null && dataStore !=null) {
      FileData fileData = dataStore.getDocumentContent(document.getReportSpace(), document.getDocumentHandle());
      return fileData != null && fileData.getContent() != null && fileData.getContentType() != null;
    }
    return false;
  }



  public List<DocumentInfo> getDocumentInfoForReportOrTrnReference(
          String channelName,
          String trnReference,
          ReportData report) {
    if (channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      if (channel != null) {
        if (report != null) {
          return getDocumentInfoListForReport(channel, report);
        } else if (trnReference != null && !trnReference.isEmpty()) {
          ReportInfo reportInfo = getReport(channel.getReportSpace(), trnReference);
          if (reportInfo != null) {
            return getDocumentInfoListForReport(channel, reportInfo.getReportData());
          } else {
            throw new PreconditionFailedException("A valid trnReference (for a previously uploaded report) must be specified");
          }
//          return dataLogic.getDocumentInfoListForTrnReference(channel, trnReference);
        } else {
          throw new PreconditionFailedException("A valid report json object or valid, existing trnReference must be specified");
        }
      }
    }
    //if we got here, then there's a problem with the channelName supplied.
    throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
  }


  public List<String> getDocumentHandlesForReport(String reportSpace, String reportKey) {
    ArrayList<String> handles = new ArrayList<String>();
    String defaultChannelName = getDefaultChannel(reportSpace).getChannelName();
    List<DocumentInfo> docs = getDocumentInfoForReportOrTrnReference(
            defaultChannelName,
            reportKey,
            null);
    if (docs != null) {
      for (DocumentInfo doc : docs) {
        handles.add(doc.getDocumentHandle());
      }
    }
    return handles;
  }



  public Channel getDefaultChannel(String reportSpace) {
    ReportSpace rp = systemInformation.getReportSpace(reportSpace);
    if (rp != null){
      String def = rp.getDefaultChannel();
      if (def != null)
        return systemInformation.getChannel(def);
    }
    return null;
  }


  public static Channel getDefaultChannel(SystemInformation systemInformation, String reportSpace) {
    ReportSpace rp = systemInformation.getReportSpace(reportSpace);
    if (rp != null){
      String def = rp.getDefaultChannel();
      if (def != null)
        return systemInformation.getChannel(def);
    }
    return null;
  }


  public static String getReportKey(String reportSpace, String trnReference) {
    String reportSpacePrefix = (reportSpace != null ? reportSpace : "") + "@";
    if (trnReference != null && trnReference.startsWith(reportSpacePrefix))
      return trnReference;
    return reportSpacePrefix + trnReference;
  }

  public static byte[] Base64Encode(byte[] content) {
    if (content != null && content.length > 0) {
      byte[] encoded = Base64.getEncoder().encode(content);
      return encoded;
    }
    return new byte[0];
  }

  public static byte[] Base64Decode(byte[] b64Content) {
    if (b64Content != null && b64Content.length > 0) {
      byte[] decoded = Base64.getDecoder().decode(b64Content);
      return decoded;
    }
    return new byte[0];
  }



  public static String getSystemValue(String name, ReportInfo report) {
    if (FieldConstant.State.equals(name))
      return report.getState();
    if (FieldConstant.ReportSpace.equals(name))
      return report.getReportSpace();
    if (FieldConstant.Schema.equals(name))
      return report.getSchema();
    if (FieldConstant.Channel.equals(name))
      return report.getChannelName();
    return null;
  }

  public IndexedRecord getIndexRecordForReport(ReportInfo report) {
    IndexedRecordDefinition ird = systemInformation.getIndexDefinitionForReports();
    IndexedRecord indexedRecord = new IndexedRecord();
    List<IndexFieldValue> indexFields = indexedRecord.getFields();
    for (IndexField field : ird.getFields()){
      Object value = null;
      if (field.getType().equals(CriterionType.Report)){
        try {
          value = report.getReportData().getReport().get(field.getName());
        } catch(Exception e){
          //TODO: ignore for now.
        }
      } else if (field.getType().equals(CriterionType.Meta)){
        try {
          value = report.getReportData().getMeta().get(field.getName());
        } catch(Exception e){
          //TODO: ignore for now.
        }
      } else if (field.getType().equals(CriterionType.System)){
        try {
          value = getSystemValue(field.getName(), report); //NB: GUID DOESN'T EXIST, WILL BE NULL ON REQUEST - USE ReportObject INSTEAD.
        } catch (Exception e) {
          //TODO: ignore for now.
        }
      }
      IndexFieldValue fieldValue = new IndexFieldValue(field);
      fieldValue.setValue(value != null ? value.toString() : null);
      indexFields.add(fieldValue);
    }
    return indexedRecord;
  }

  public IndexedRecord getIndexRecordForReportObject(ReportObject report) {
    ReportInfo reportInfo = null;
    try {
      reportInfo = report.getReportData();
    } catch (Exception e) {
      e.printStackTrace();
    }
//    return getIndexRecordForReport(reportInfo);
    IndexedRecordDefinition ird = systemInformation.getIndexDefinitionForReports();
    IndexedRecord indexedRecord = new IndexedRecord();
    List<IndexFieldValue> indexFields = indexedRecord.getFields();
    for (IndexField field : ird.getFields()){
      Object value = null;
      if (field.getType().equals(CriterionType.Report)){
        try {
          value = report.getReportData().getReportData().getReport().get(field.getName());
        } catch(Exception e){
          //TODO: ignore for now.
          logger.warn("Unable to create report index field ("+field.getName()+"): "+e.getMessage());
        }
      } else if (field.getType().equals(CriterionType.Meta)){
        try {
          value = report.getReportData().getReportData().getMeta().get(field.getName());
        } catch(Exception e){
          //TODO: ignore for now.
          logger.warn("Unable to create meta index field ("+field.getName()+"): "+e.getMessage());
        }
      } else if (field.getType().equals(CriterionType.System)){
        try {
          value = report.getSystemValue(field.getName());
        } catch (Exception e) {
          //TODO: ignore for now.
          logger.warn("Unable to create system index field ("+field.getName()+"): "+e.getMessage());
        }
      }
      IndexFieldValue fieldValue = new IndexFieldValue(field);
      fieldValue.setValue(value != null ? value.toString() : null);
      indexFields.add(fieldValue);
    }
    return indexedRecord;
  }
  
  public boolean isMimeTypeWhiteListed(MultipartFile file) {
    if ((FileUploadUtil.mimeTypeWhiteList == null || FileUploadUtil.mimeTypeWhiteList.isEmpty()) && (FileUploadUtil.extensionWhiteList == null || FileUploadUtil.extensionWhiteList.isEmpty())) {
      FileUploadUtil.passWhiteListMechanism = systemInformation.getConfigStore().getMimeTypeWhitelistMechanism();
    }
    if (FileUploadUtil.mimeTypeWhiteList == null || FileUploadUtil.mimeTypeWhiteList.isEmpty()){
      FileUploadUtil.setMimeTypeWhiteList(systemInformation.getConfigStore().getMimeTypeWhitelist());
    }
    if (FileUploadUtil.extensionWhiteList == null || FileUploadUtil.extensionWhiteList.isEmpty()){
      FileUploadUtil.setSafeExtensionWhiteList(systemInformation.getConfigStore().getExtensionWhitelist());
    }
    return FileUploadUtil.isMimeTypeWhiteListed(file);
  }
  
  public String makeNameSafe(String originalFilename) throws Exception{
    return FileUploadUtil.makeNameSafe(originalFilename);
  }
  
  public boolean isNameSafe(String originalFilename) {
    return FileUploadUtil.isNameSafe(originalFilename);
  }
  
  public boolean isWhiteListed(MultipartFile file) {
    if ((FileUploadUtil.mimeTypeWhiteList == null || FileUploadUtil.mimeTypeWhiteList.isEmpty()) && (FileUploadUtil.extensionWhiteList == null || FileUploadUtil.extensionWhiteList.isEmpty())) {
      FileUploadUtil.passWhiteListMechanism = systemInformation.getConfigStore().getMimeTypeWhitelistMechanism();
    }
    if (FileUploadUtil.mimeTypeWhiteList == null || FileUploadUtil.mimeTypeWhiteList.isEmpty()){
      FileUploadUtil.setMimeTypeWhiteList(systemInformation.getConfigStore().getMimeTypeWhitelist());
    }
    if (FileUploadUtil.extensionWhiteList == null || FileUploadUtil.extensionWhiteList.isEmpty()){
      FileUploadUtil.setSafeExtensionWhiteList(systemInformation.getConfigStore().getExtensionWhitelist());
    }
    return FileUploadUtil.isWhiteListed(file);
  }

  public List<String> getPotentialDocs(String channelName,String category,
                                       String flow,
                                       String section) throws Exception {
    Validator validator = null;
    StringList out = new StringList();
    if (validationValidators.containsKey(channelName)){
      validator = validationValidators.get(channelName);
    }
    if (validator == null && validationEngineFactory != null && supporting != null) {
      try {
        ValidationEngine engine = validationEngineFactory.getDocumentEngineForCurrentDate(channelName);


        List<Rule> rules = engine.getPotentialDocuments(category, FlowType.fromString(flow));
        for(Rule rule :rules){
          //only add "unique" items...
          String item = rule.getCode()+", "+rule.getMessage();
          if (!out.contains(item)) {
            out.add(item);
          }
        }
      }
      catch (Exception e) {
        logger.error("Cannot get Validation Validator", e);
        throw e;
      }
    }
    return out;
  }






  public static byte[] createChecksum(InputStream inputStream, String hashType) throws Exception {
    //poached from : https://www.rgagnon.com/javadetails/java-0416.html
    byte[] buffer = new byte[1024];
    MessageDigest complete = MessageDigest.getInstance(hashType);
    int numRead;
    do {
      numRead = inputStream.read(buffer);
      if (numRead > 0) {
        complete.update(buffer, 0, numRead);
      }
    } while (numRead != -1);
    inputStream.close();
    return complete.digest();
  }


  public static String getChecksumString(InputStream inputStream, String hashType) throws Exception {
    byte[] b = createChecksum(inputStream, hashType);
    String result = "";
    for (int i=0; i < b.length; i++) {
      result +=
          Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
    }
    return result;
  }


  public static String getChecksumForDataString(String data, String hashType) throws Exception {
    return getChecksumString(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)), hashType);
  }


  public void addChecksumHeaders(String dataStr, HttpServletResponse response) {
    for (Map.Entry<String, String> checksum : getChecksumValues(dataStr, null).entrySet()) {
      try {
        response.addHeader(checksum.getKey() + "_CHECKSUM", checksum.getValue());
      } catch (Exception e) {
        logger.error("Unable to create or add checksum ("+checksum.getKey()+" : "+(checksum.getValue()!=null?checksum.getValue():"null")+") to header: " + e.getMessage(), e);
      }
    }
  }


  public List<String> getDefaultChecksumAlgorithms(List<String> algorithms){
    if (algorithms == null){
      algorithms = new ArrayList<>();
    }
    if (algorithms.size() == 0){
      //TODO: these should be pulled or configured externally somewhere.
      algorithms.add("MD5");
      algorithms.add("SHA1");
      algorithms.add("SHA-256");
      algorithms.add("SHA-384");
      algorithms.add("SHA-512");
    }
    return algorithms;
  }

  public Map<String, String> getChecksumValues(String dataStr, List<String> algorithms) {
    HashMap<String, String> checksumMap = new HashMap<String, String>();
    if (algorithms == null || algorithms.size() == 0){
      algorithms = getDefaultChecksumAlgorithms(algorithms);
    }
    for (String algorithm : algorithms) {
      try {
        checksumMap.put(algorithm, getChecksumForDataString(dataStr, algorithm));
      } catch (Exception e) {
        logger.info("Unable to create or add checksum ("+algorithm+"): " + e.getMessage());
      }
    }
    return checksumMap;
  }

  public Map<String, String> checkForChecksumMatch(String dataStr, String checksumAlgorithm, String checksumValue) {
    ArrayList<String> checksumAlgorithms = new ArrayList<String>();
    if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
      checksumAlgorithms.add(checksumAlgorithm);
    }
    Map<String, String> checksums = getChecksumValues(dataStr, checksumAlgorithms);
    if (checksumValue != null && !checksumValue.isEmpty()){
      boolean checksumMatched = false;
      for (Map.Entry<String, String> checksum : checksums.entrySet()) {
        if (checksum.getValue().equals(checksumValue)){
          if (checksumAlgorithm == null || checksumAlgorithm.isEmpty() || checksumAlgorithm.equalsIgnoreCase(checksum.getKey())) {
            checksums.put("MatchedAlgorithm", checksum.getKey());
            if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
              checksums.put("MatchedChecksum", checksum.getValue());
            }
            checksumMatched = true;
            checksums.put("MATCH", "YES");
            break;
          }
        }
      }
      checksums.put("ProvidedChecksum", checksumValue);
      if (checksumAlgorithm != null && !checksumAlgorithm.isEmpty()) {
        checksums.put("ProvidedAlgorithm", checksumAlgorithm);
      }
      if (!checksumMatched){
        checksums.put("MATCH", "NO");
      }
    }
    return checksums;
  }
}
