package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.EvaluationEngineFactory;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ReportServiceExecutorService;
import za.co.synthesis.regulatory.report.support.ResourceAccessAuthorizationException;
import za.co.synthesis.rule.core.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("internal/producer/api")
public class InternalProducerDecisionApi extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalProducerDecisionApi.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    private DataLogic dataLogic;

    @Autowired
    ApplicationContext ctx;


  @Autowired
  ReportServiceExecutorService reportServiceExecutorService;

  @Autowired
  private EvaluationEngineFactory engineFactory;


  //-----------------------------------------------------------------------------------------------
  //  Evaluation APIs
  //-----------------------------------------------------------------------------------------------

  /**
   * expose request for "producer/api/evaluation"
   * Exposed as "internal/producer/api/evaluation"
   */
  /**
   * This will retrieve a decision related to the specific TrnReference located within a given reporting space.
   * @param channelName - the channel (and hence underlying reporting space) associated with the decision
   * @param reportSpace - the reporting space associated with the decision
   * @param trnReference - the transaction reference for which the decision will apply
   * @return the stored decision for the given TrnReference
   */
  @RequestMapping(value = "/evaluation", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  FullyReferencedEvaluationDecision getInternalDecision(
          @RequestParam(required = false) String channelName,
          @RequestParam(required = false) String reportSpace,
          @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDecision()");
    return Decision.getDecision(channelName, reportSpace, trnReference, systemInformation, dataLogic);
  }

  /**
   * expose request for "producer/api/evaluation"
   * Exposed as "internal/producer/api/evaluation"
   */
  @RequestMapping(value = "/evaluation", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  void setInternalDecision(
          @RequestParam String channelName,
          @RequestParam String trnReference,
          @RequestBody EvaluationDecision decision) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setInternalDecision()");
    Decision.setDecision(channelName, trnReference, decision, systemInformation, dataLogic);
  }


  /**
   * Omnibus evaluation function that covers all evaluation scenarios with a single JSON request. This function can
   * only evaluate one scenario per call. The documentation for each of the scenarios can be found under the respective
   * API call.
   * @param evaluationRequest The scenario to be evaluated
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateAny", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateAny(@RequestBody EvaluationRequest evaluationRequest) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateAny()");
    return Decision.evaluateAnyStatic(evaluationRequest, systemInformation, engineFactory, logger, dataLogic);
  }



  /**
   * Customer payment, typically within the bank where both remitter and beneficiary details (account type and resident status) are known.
   * For example, a CFC to CFC transfer from one customer to another.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   * @param channelName The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param drResStatus The remitter Resident Status
   * @param drAccountHolderStatus - (Optional) The remitter Status - if not provided, "Individual" will be assumed.
   * @param drAccType The remitter Account Type
   * @param crResStatus The beneficiary Resident Status
   * @param crAccountHolderStatus - (Optional) The beneficiary Status - if not provided, "Individual" will be assumed.
   * @param crAccType The beneficiary Account Type
   * @param mostRelevantResultsOnly When true, returns only the most relevant results for Dr and CR - where reportable.  If no reportable results, the next most onerous decision is returned (if any).
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateCustomerOnUs", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateCustomerOnUs(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @RequestParam ResidenceStatus drResStatus,
      @RequestParam(required=false) AccountHolderStatus drAccountHolderStatus,
      @RequestParam BankAccountType drAccType,
      @RequestParam ResidenceStatus crResStatus,
      @RequestParam(required=false) AccountHolderStatus crAccountHolderStatus,
      @RequestParam BankAccountType crAccType,
      @RequestParam (required=false) Boolean mostRelevantResultsOnly) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateCustomerOnUs()");
    return Decision.evaluateCustomerOnUsStatic(channelName, trnReference, drResStatus, drAccountHolderStatus, drAccType, crResStatus, crAccountHolderStatus, crAccType, mostRelevantResultsOnly, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * This is typically a payment from a customer within the bank, to an external account.
   * The remitter details (Resident status and account type) are generally known in full.
   * For the beneficiary however, only the BIC and transaction currency might be known.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   * @param channelName The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param drResStatus The remitter Resident Status
   * @param drAccountHolderStatus - (Optional) The remitter Status - if not provided, "Individual" will be assumed.
   * @param drAccType The remitter Account Type
   * @param beneficiaryInstitutionBIC The BIC code of the institution (bank or other) to which the payment was made
   * @param beneficiaryCurrency The currency in which the payment was made
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateCustomerPayment", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateCustomerPayment(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @RequestParam ResidenceStatus drResStatus,
      @RequestParam(required=false) AccountHolderStatus drAccountHolderStatus,
      @RequestParam BankAccountType drAccType,
      @RequestParam String beneficiaryInstitutionBIC,
      @RequestParam String beneficiaryCurrency) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateCustomerPayment()");
    return Decision.evaluateCustomerPaymentStatic(channelName, trnReference, drResStatus, drAccountHolderStatus, drAccType, beneficiaryInstitutionBIC, beneficiaryCurrency, systemInformation, engineFactory, logger, dataLogic);
  }


  /**
   * This is typically a payment to a customer (beneficiary) within the bank, from an external account (remitter).
   * The beneficiary details (Resident status and account type) are known in full.
   * For the remitter however, only the BIC and transaction currency might be known.
   * Should any of the parameters not be known, the system will make the necessary assumptions and permutations, and return all potential variations of decisions that arise therefrom.
   * @param channelName The channel package containing the evaluation rules that should be applied to the information provided
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param crResStatus The beneficiary Resident Status
   * @param crAccountHolderStatus - (Optional) The beneficiary Status - if not provided, "Individual" will be assumed.
   * @param crAccType The beneficiary Account Type
   * @param orderingInstituitionBIC The BIC code of the institution (bank or other) from which the payment was made
   * @param orderingCurrency The currency in which the payment was made
   * @param field72 Value provided in field72 of the SWIFT message detail
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateCustomerReceipt", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateCustomerReceipt(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @RequestParam ResidenceStatus crResStatus,
      @RequestParam(required=false) AccountHolderStatus crAccountHolderStatus,
      @RequestParam BankAccountType crAccType,
      @RequestParam String orderingInstituitionBIC,
      @RequestParam String orderingCurrency,
      @RequestParam(required = false) String field72) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateCustomerReceipt()");
    return Decision.evaluateCustomerReceiptStatic(channelName, trnReference, crResStatus, crAccountHolderStatus, crAccType, orderingInstituitionBIC, orderingCurrency, field72, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment made to another (banking) institute.
   * @param channelName The channel package containing the rules to apply to the evaluation
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param orderingInstitutionBIC (Optional - Deprecated) The BIC code of the ordering (remitter) Institution - i.e.: Our BIC code.
   * @param beneficiaryBIC The BIC code of the beneficiary bank - not a correspondent bank
   * @param beneficiaryCurrency The currency in which the payment is made
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateBankPayment", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateBankPayment(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @Deprecated @RequestParam(required=false) String orderingInstitutionBIC,
      @RequestParam String beneficiaryBIC,
      @RequestParam String beneficiaryCurrency) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateBankPayment()");
    return Decision.evaluateBankPaymentStatic(channelName, trnReference, orderingInstitutionBIC, beneficiaryBIC, beneficiaryCurrency, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment received from another (banking) institute.
   * @param channelName The channel package containing the rules to apply to the evaluation
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param beneficiaryBIC (Optional - Deprecated) The BIC code of the beneficiary bank  - i.e.: Our BIC code.
   * @param orderingBIC The BIC code of the ordering (remitter) bank  - not a correspondent bank
   * @param orderingCurrency The currency in which the payment is made
   */
  @RequestMapping(value = "/evaluation/evaluateBankReceipt", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateBankReceipt(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @Deprecated @RequestParam(required=false) String beneficiaryBIC,
      @RequestParam String orderingBIC,
      @RequestParam String orderingCurrency) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateBankReceipt()");
    return Decision.evaluateBankReceiptStatic(channelName, trnReference, beneficiaryBIC, orderingBIC, orderingCurrency, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Inter-bank payment received on behalf of another (banking) institute - where we are acting as a correspondent bank in the payment.
   * @param channelName The channel package containing the rules to apply to the evaluation
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param beneficiaryInstitutionBIC The BIC code of the beneficiary bank  - not a correspondent bank (not us)
   * @param orderingInstitutionBIC The BIC code of the ordering (remitter) bank  - not a correspondent bank (not us)
   * @param orderingCustomer The ordering customer type - Bank or Non-Bank.  This will determine the value populated for the suggested field72 value (NTNRC vs NTNRB).
   */
  @RequestMapping(value = "/evaluation/evaluateCorrespondentBank", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateCorrespondentBank(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @RequestParam String beneficiaryInstitutionBIC,
      @RequestParam String orderingInstitutionBIC,
      @RequestParam OrderingCustomerType orderingCustomer) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateCorrespondentBank()");
    return Decision.evaluateCorrespondantBankStatic(channelName, trnReference, beneficiaryInstitutionBIC, orderingInstitutionBIC, orderingCustomer, systemInformation, engineFactory, logger, dataLogic);
  }

  /**
   * Card transaction evaluation
   * @param channelName The channel package containing the rules to apply to the evaluation
   * @param trnReference - (Optional) Transaction reference for which the decisions apply. <b>If provided, the evaluation decisions will automatically be stored in a decision log, against the trnReference provided for auditing purposes.  However, it is important to note that the ultimate decision selected or used to determine reportability of the transaction should be POSTed back to the POST end-point for decisions, with the relevant trnReference to keep a proper audit trail showing why a specific transaction was deemed reportable or not - based on which decision and accompanying inputs that derived it.</b>.
   * @param issuingCountry The country of the bank that issued the card to the customer
   * @param useCountry The country where the card was used
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/evaluation/evaluateCard", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  EvaluationDecisions internalEvaluateCard(
      @RequestParam String channelName,
      @RequestParam(required=false) String trnReference,
      @RequestParam String issuingCountry,
      @RequestParam String useCountry) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalEvaluateCard()");
    return Decision.evaluateCardStatic(channelName, trnReference, issuingCountry, useCountry, systemInformation, engineFactory, logger, dataLogic);
  }


//-----------------------------------------------------------------------------------------------
  //TODO: expose Lookup APIs
  //TODO: expose Rules APIs
  //TODO: expose Enquiry APIs
//-----------------------------------------------------------------------------------------------
}
