package za.co.synthesis.regulatory.report.businesslogic;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.mchange.v2.collection.MapEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.index.IndexField;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecordDefinition;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.security.jwt.IPublicKeySource;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;
import za.co.synthesis.regulatory.report.support.EventType;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by jake on 7/26/17.
 */
public class SystemInformation {
  private static final Logger logger = LoggerFactory.getLogger(SystemInformation.class);
  private IConfigStore configStore;
  private IPublicKeySource publicKeySource;
  private UserInformation userInformation;


  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  public IConfigStore getConfigStore() {
    return configStore;
  }

  public UserInformation getUserInformation() {
    return userInformation;
  }

  public void setUserInformation(UserInformation userInformation) {
    this.userInformation = userInformation;
  }

  public SystemInformation() {
  }

  //----- Temporary Cache Store (used) -----//
  public void setCacheByName(String name, String data) {
    configStore.setCacheByName(name, data);
  }

  public String getCacheByName(String name) {
    return configStore.getCacheByName(name);
  }

  public void setReportSpace(ReportSpace reportSpace) {
    reportSpace.syncData();
    configStore.setReportSpace(reportSpace.getReportSpaceData());
  }

  public List<String> getReportSpaceChannels(String reportSpaceName) {
    List<String> result = new ArrayList<String>();
    Map<String, ChannelData> channels = configStore.getChannelData();

    for (ChannelData channelData : channels.values()) {
      if (channelData.getReportSpace().equals(reportSpaceName)) {
        result.add(channelData.getChannelName());
      }
    }
    return result;
  }

  public List<ChannelData> getReportSpaceChannelData(String reportSpaceName) {
    List<ChannelData> result = new ArrayList<>();
    Map<String, ChannelData> channels = configStore.getChannelData();

    for (ChannelData channelData : channels.values()) {
      if (channelData.getReportSpace().equals(reportSpaceName)) {
        result.add(channelData);
      }
    }
    return result;
  }

  private ReportSpaceData getReportSpaceData(String reportSpaceName) {
    return configStore.getReportSpaces().get(reportSpaceName);
  }

  public ReportSpace getReportSpace(String reportSpaceName) {
    ReportSpaceData info = getReportSpaceData(reportSpaceName);
    if (info != null) {
      return new ReportSpace(info, getReportSpaceChannels(reportSpaceName));
    }
    return null;
  }

  public boolean isValidReportSpace(String reportSpaceName) {
    if (reportSpaceName != null && !reportSpaceName.isEmpty()) {
      ReportSpace reportSpace = getReportSpace(reportSpaceName);
      return reportSpace != null &&
              reportSpace.getChannels() != null &&
              reportSpace.getChannels().size() > 0 &&
              reportSpace.getDefaultChannel() != null &&
              !reportSpace.getDefaultChannel().isEmpty();
    }
    return false;
  }

  public List<String> getChannelNames() {
    List<String> channelNames = new ArrayList<String>();
    channelNames.addAll(configStore.getChannelData().keySet());
    return channelNames;
  }

  public Channel getChannel(String channelName) {
    ChannelData info = configStore.getChannelData().get(channelName);
    return info != null ? new Channel(info) : null;
  }

  public String getReportSpaceForChannel(String channelName) {
    ChannelData info = configStore.getChannelData().get(channelName);
    return info != null ? info.getReportSpace() : null;
  }

  /**
   * Returns all states that may apply to a given channel (essentially all states in the related reporting space)
   * @param channelName
   * @return
   */
  public List<State> getChannelStates(String channelName) {
    ChannelData info = configStore.getChannelData().get(channelName);
    String reportSpaceName =  info != null ? info.getReportSpace() : null;
    if (reportSpaceName != null) {
      ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
      if (reportSpaceDef != null && reportSpaceDef.getStates() != null) {
        return State.wrapList(reportSpaceDef.getStates());
      }
    }
    return null;
  }
  
  /**
   * Returns all allowed transitions for a given channel
   * @param channelName
   * @return
   */
  public List<Transition> getChannelTransitions(String channelName) {
    ChannelData info = configStore.getChannelData().get(channelName);
    String reportSpaceName =  info != null ? info.getReportSpace() : null;
    List<Transition> newTransitions = new ArrayList<>();
    if (reportSpaceName != null) {
      ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
      if (reportSpaceDef != null) {
        List<Transition> transitions = Transition.wrapList(reportSpaceDef.getTransitions());
        for (Transition transition : transitions) {
          if (transition.getChannels() == null || transition.getChannels().isEmpty() || transition.getChannels().contains(channelName)) {
            newTransitions.add(transition);
          }
        }
      }
    }
    return newTransitions;
  }
  
  /**
   * Returns all allowed transitions for a given state and channel
   * @param channelName
   * @param currentState
   * @return
   */
  public List<Transition> getChannelTransitionsForState(String channelName, String currentState) {
    List<Transition> transitions = getChannelTransitions(channelName);
    List<Transition> newTransitions = new ArrayList<>();
    if (transitions != null) {
      if (currentState != null) {
        for (Transition transition : transitions) {
          if (transition.getCurrentStates() == null || transition.getCurrentStates().isEmpty() || transition.getCurrentStates().contains(currentState)) {
            newTransitions.add(transition);
          }
        }
      } else {
        newTransitions.addAll(transitions);
      }
    }
    return newTransitions;
  }
  
  
  /**
   * Returns all allowed user actions for a given channel
   * @param channelName
   * @return
   */
  public List<Transition> getChannelUserActions(String channelName) {
      List<Transition> transitions = getChannelTransitions(channelName);
      List<Transition> newTransitions = new ArrayList<>();
      if (transitions != null) {
        for (Transition transition : transitions) {
          if (transition.getEvent() == EventType.UserAction) {
            newTransitions.add(transition);
          }
        }
      }
      return newTransitions;
  }
  
  /**
   * Returns all allowed user actions for a given state and channel
   * @param channelName
   * @param currentState
   * @return
   */
  public List<Transition> getChannelUserActionsForState(String channelName, String currentState) {
    List<Transition> transitions = getChannelUserActions(channelName);
    List<Transition> newTransitions = new ArrayList<>();
    if (transitions != null) {
      if (currentState != null) {
        for (Transition transition : transitions) {
          if (transition.getCurrentStates() == null || transition.getCurrentStates().isEmpty() || transition.getCurrentStates().contains(currentState)) {
            newTransitions.add(transition);
          }
        }
      } else {
        newTransitions.addAll(transitions);
      }
    }
    return newTransitions;
  }
  
  
  public List<String> getReportSpaces() {
    List<String> reportSpaces = new ArrayList<String>();

    for (ReportSpaceData reportSpaceDef : configStore.getReportSpaces().values()) {
      reportSpaces.add(reportSpaceDef.getName());
    }
    return reportSpaces;
  }

  public List<State> getReportSpaceStates(String reportSpaceName) {
    ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
    if (reportSpaceDef!=null && reportSpaceDef.getStates() != null) {
      return State.wrapList(reportSpaceDef.getStates());
    }
    return null;
  }

  public boolean hasReportSpaceStateData(String reportSpaceName, String stateName) {
    ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
    if (reportSpaceDef!=null) {
      for (StateData state : reportSpaceDef.getStates()) {
        if (state.getName().equals(stateName)) {
          return true;
        }
      }
    }
    return false;
  }

  public StateData getReportSpaceStateData(String reportSpaceName, String stateName) {
    ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
    if (reportSpaceDef!=null) {
      for (StateData state : reportSpaceDef.getStates()) {
        if (state.getName().equals(stateName)) {
          return state;
        }
      }
    }
    return null;
  }

  public StateData getReportSpaceDefaultStateData(String reportSpaceName) {
    ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
    if (reportSpaceDef!=null) {
      for (StateData state : reportSpaceDef.getStates()) {
        if (state.isDefaultState()) {
          return state;
        }
      }
    }
    return null;
  }

  public List<Transition> getReportSpaceTransitions(String reportSpaceName) {
    ReportSpaceData reportSpaceDef = getReportSpaceData(reportSpaceName);
    if (reportSpaceDef != null) {
      return Transition.wrapList(reportSpaceDef.getTransitions());
    }
    return null;
  }

  public List<TransitionData> getReportSpaceTransitionData(String reportSpaceName) {
    ReportSpaceData reportSpaceData = getReportSpaceData(reportSpaceName);
    if (reportSpaceData != null) {
      return reportSpaceData.getTransitions();
    }
    return null;
  }

  public ListData getListDataByName(String listname) {
    //use a case-insensitive match to get the list...
    Map<String, ListData> lists = configStore.getListData();
    for (Map.Entry<String, ListData> listEntry : lists.entrySet()){
        if (listEntry.getKey().equalsIgnoreCase(listname)) {
          return listEntry.getValue();
        }
    }
    return configStore.getListData().get(listname);
  }
  public Map<String, ListData> getAllListDetails() {
    return configStore.getListData();
  }
  
  public List<String> getListNames() {
    return getListNames(null, false);
  }
  
  public List<String> getListNames(String filter, boolean exclusiveFiltering) {
    List<String> result = new ArrayList<String>();
    if (filter != null && (!filter.isEmpty() || exclusiveFiltering)){
      for (Map.Entry<String, ListData> listEntry : configStore.getListData().entrySet()){
        //check for unfiltered or whitelist filtering results
        boolean returnListName = listEntry.getValue().getListingFilter().contains(filter) ||
            (!exclusiveFiltering && listEntry.getValue().getListingFilter().isEmpty()) ||
            (exclusiveFiltering && listEntry.getValue().getListingFilter().isEmpty() && filter.isEmpty());
        //check for blacklist filtering results
        returnListName = returnListName && (
            listEntry.getValue().getListingExclusionFilter().isEmpty() &&
                (!listEntry.getValue().getListingFilter().contains(filter))
            );
        if (returnListName) {
          result.add(listEntry.getKey());
        }
      }
    } else {
      result.addAll(configStore.getListData().keySet());
    }
    return result;
  }

  public List<String> getUserEventNames() {
    List<String> result = new ArrayList<String>();
    for (ReportSpaceData reportSpace : configStore.getReportSpaces().values()) {
      List<StateData> states = reportSpace.getStates();
      if (states != null) {
        for (StateData state : states) {
//          result.addAll(state.getUserEvents());
          for (String userEvent : state.getUserEvents()){
            if (!result.contains(userEvent)){
              result.add(userEvent);
            }
          }
        }
      }
    }
    return result;
  }

  public Integer getDefaultMaxDownloadRecords() {
    return configStore.getDefaultMaxDownloadRecords();
  }

  public List<String> getRightsList() {
    return configStore.getRightsList();
  }

  public IndexedRecordDefinition getIndexDefinitionForReports() {
    IndexedRecordDefinition index = new IndexedRecordDefinition("Report");

    try {
      for (AccessSetData accessSet: configStore.getAccessSetMap().values()) {
        for (CriterionData criteria: accessSet.getAccessCriteria()) {
          index.intelligentAddField(new IndexField(criteria.getType(), IndexField.Usage.Search, criteria.getName()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    for (ListData list : configStore.getListData().values()) {
      for (CriterionData criteria: list.getCriteria()) {
        index.intelligentAddField(new IndexField(criteria.getType(), IndexField.Usage.Search, criteria.getName()));
      }
      for (ListData.Field field : list.getFields()) {
        index.intelligentAddField(new IndexField(field.getCriterionType(), IndexField.Usage.Store, field.getName()));
      }
    }

    // Add in the compulsary stuff
    index.intelligentAddField(new IndexField(CriterionType.System, IndexField.Usage.Store, FieldConstant.ReportSpace));
    index.intelligentAddField(new IndexField(CriterionType.Report, IndexField.Usage.Store, FieldConstant.TrnReference));
    index.intelligentAddField(new IndexField(CriterionType.System, IndexField.Usage.Store, FieldConstant.GUID));

    return index;
  }



  //*******************************************************************************************//
  
  
  public void setPublicKeySource(IPublicKeySource publicKeySource) {
    this.publicKeySource = publicKeySource;
    this.publicKeySource.setSystemInformation(this);
  }
  
  public IPublicKeySource getPublicKeySource() {
    return publicKeySource;
  }
  
  public List<String> getPublicKeySources() {
    return configStore.getPKSourcesList();
  }
  
  private LoadingCache<String, StoredKey> publicKeyCache = Caffeine.newBuilder()
          .expireAfterWrite(5, TimeUnit.MINUTES)
          .maximumSize(10_000)
          .build(kid -> getInternalStoredPublicKey(kid));


  private StoredKey getInternalStoredPublicKey(String kid){
    StoredKey storedKey = null;
    if (kid != null) {
      IConfigStore configStore = getConfigStore();
      if (configStore != null) {
        return configStore.getPublicKey(kid);
      }
    }
    return storedKey;
  }

  public StoredKey getStoredPublicKey(String kid) throws Exception {
    try {
      return publicKeyCache.get(kid);
    } catch (Exception e) {
      throw new Exception("Cannot resolve public key for kid:"+kid, e);
    }
  }



  private LoadingCache<String, StoredKey> privateKeyCache = Caffeine.newBuilder()
          .expireAfterWrite(5, TimeUnit.MINUTES)
          .maximumSize(10_000)
          .build(kid -> getInternalStoredPrivateKey(kid));


  private StoredKey getInternalStoredPrivateKey(String kid){
    StoredKey storedKey = null;
    if (kid != null) {
      IConfigStore configStore = getConfigStore();
      if (configStore != null) {
        return configStore.getPrivateKey(kid);
      }
    }
    return storedKey;
  }


  public StoredKey getStoredPrivateKey(String kid) throws Exception {
    try {
      return privateKeyCache.get(kid);
    } catch (Exception e) {
      throw new Exception("Cannot resolve private key for kid:"+kid, e);
    }
  }


  /**
   * @return Returns a complete manifest of all (active) public keys in the system
   */
  public Map<String, StoredKey> getStoredPublicKeys() {
    Map<String, StoredKey> storedKeys = new HashMap<String, StoredKey>();
    IConfigStore configStore = getConfigStore();
    if (configStore != null) {
      storedKeys.putAll(configStore.getPublicKeys());
    }
    return storedKeys;
  }
  
  
  
  //TODO: We may need an Expiry date, Comment, Security context, Update context (ie: Where to fetch a new key when this one expires)
  public String setStoredKey(String kid, String algorithm, String encodedKey, boolean isPrivate) {
    if (kid == null || kid.isEmpty()){
      kid = PersistenceObject.generateUUID();
    }
    
    if (kid != null && encodedKey != null){
      if (configStore != null) {
        if (encodedKey != null) {
          //assume base64 encoding on encoded key...
          byte[] decodedKey = null;
          
          //check if the key is URL-encoded...
          if (encodedKey.indexOf("%2D") >= 0) {
            try {
              encodedKey = URLDecoder.decode(encodedKey, "UTF-8");
            } catch (Exception e){}
          }
          
          //strip off leading and trailing "----...----" lines...
          encodedKey = encodedKey.trim()
                  .replaceAll("-+\\s*(BEGIN|END)\\s(PUBLIC|PRIVATE)\\sKEY\\s*-+","")
                  .replaceAll("\n", "")
                  .replaceAll("\\n", "")
                  .replaceAll("\\\\n", ""); //sometimes the encoded string is escaped so we need to just remove any literal representations of escaped line-feeds
          
          //attempt to base64 decode if necessary...
          try {
            decodedKey = Base64.getDecoder().decode(encodedKey);
          } catch(Exception e){
            //if the decode failed, perhaps the key is not encoded?
            decodedKey = encodedKey.getBytes(Charset.forName("UTF-8"));
          }
          
          Key key = StoredKey.getKeyfromBytes(decodedKey, algorithm, isPrivate);
          if (key != null) {
            StoredKey storedPublicKey = new StoredKey(kid, key, key.getAlgorithm(), isPrivate);
            return configStore.setKeyPair(kid, null, storedPublicKey); //return the assigned keyID
          } else {
            logger.warn("Unable to convert and store the provided key: "+ (isPrivate?"<PRIVATE> ": "<PUBLIC> ") + kid + " (" + algorithm + ") - "+ encodedKey);
          }
          
        }
      }
    }
    return null;
  }

  
  private String setKeyPair(String kid, Key privateKey, Key publicKey){
    if (kid == null || kid.isEmpty()) {
      kid = PersistenceObject.generateUUID();
    }

    IConfigStore configStore = getConfigStore();
    if (configStore != null) {
      StoredKey storedPrivateKey = new StoredKey(kid, privateKey, privateKey.getAlgorithm(), true);
      StoredKey storedPublicKey = new StoredKey(kid, publicKey, publicKey.getAlgorithm(), false);
      return configStore.setKeyPair(kid, storedPrivateKey, storedPublicKey); //return the assigned keyID
    }
    return null;
  }

  private LoadingCache<String, String> currentKeyCache = Caffeine.newBuilder()
          .expireAfterWrite(2, TimeUnit.MINUTES)
          .maximumSize(2)
          .build(dummy -> getInternalCurrentKeyId(dummy));


  private String getCurrentKeyId() {
    return currentKeyCache.get("dummy");
  }

  private String getInternalCurrentKeyId(String dummy) {
    IConfigStore configStore = getConfigStore();
    if (configStore != null) {
      String kid = configStore.getCurrentKeyId();
      if (kid != null) {
        return kid; //return the current, private key, ID
      }
    }
    //otherwise generate, store and return a new private key (ID)
    try {
      KeyPair ppk = SecurityHelper.generatePPK();
      if (ppk != null){
        String baseID = PersistenceObject.generateUUID();
        logger.trace("PPK Generated. "+ppk.toString());
        setKeyPair(baseID, ppk.getPrivate(), ppk.getPublic());
        logger.trace("Key Pair Stored with key ID : " + baseID);

        return baseID;
      }
    } catch(Exception e){
      logger.error("NON-Halting exception encountered while attempting to generate new, current PPK pair for system instance: "+e.getMessage(),e);
    }
    return null;
  }


  public StoredKey getCurrentPrivateKey() throws Exception {
    return getStoredPrivateKey(getCurrentKeyId());
  }
  
  
  //*******************************************************************************************//
  
  
  
  private LoadingCache<JWTAuthToken.TokenUse, Long> tokenExpiryCache = Caffeine.newBuilder()
          .expireAfterWrite(5, TimeUnit.MINUTES)
          .maximumSize(10)
          .build(tokenUse -> getInternalTokenExpiryMinutes(tokenUse));


  private long getInternalTokenExpiryMinutes(JWTAuthToken.TokenUse tokenUse) {
    return getConfigStore().getTokenExpiryMinutes(tokenUse);
  }

  public long getTokenExpiryMinutes(JWTAuthToken.TokenUse tokenUse) {
    return tokenExpiryCache.get(tokenUse);
  }


  public String getVersion(ApplicationContext ctx) {
    String versionStr = "Version not retrievable";
    try{
      JSObject jso = JSReaderUtil.loadResourceAsJSObject(ctx, "classpath:/config/version.json");
      versionStr = (String)jso.get("version");
      logger.info("Version request - "+versionStr);
    } catch(Exception e){

    }
    return  versionStr;
  }

  public JSObject getExtConfig(String name) {
    IConfigStore configStore = getConfigStore();

    if (configStore != null) {
      return configStore.getExtConfig(name);
    }
    return null;
  }

  public void setExtConfig(String name, String value) {
    IConfigStore configStore = getConfigStore();

    if (configStore != null) {
      configStore.setExtConfig(name, value);
    }
  }

  public void setExtConfig(String name, JSObject value) {
    IConfigStore configStore = getConfigStore();

    if (configStore != null) {
      configStore.setExtConfig(name, value);
    }
  }
}
