package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.ValidationData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/12/16
 * Time: 10:39 AM
 * This class implements the functionality to retrieve and cache rules and lookups used by the validation and evaluation
 * engines. The core concept is that a centralised Rules Management system with store and provide access to the rules
 * required by this system and that this object will be responsible for retrieving the rules from this system.
 */
public class RulesCache {
  private static final Logger log = LoggerFactory.getLogger(RulesCache.class);

  private JdbcConnectionManager jdbcConnectionManager;
  // What rules are to be used
  private String rulesManagementUrl;

  // Cache of the rules
  private IConfigStore configStore;
  private long cachePeriod = 60000;
  private final Map<String, PackageCache> cacheMap = new HashMap<String, PackageCache>();

  @Required
  public void setRulesManagementUrl(String rulesManagementUrl) {
    if (rulesManagementUrl != null) {
      char ch = rulesManagementUrl.charAt(rulesManagementUrl.length()-1);
      if (ch == '/') {
        this.rulesManagementUrl = rulesManagementUrl;
      }
      else {
        this.rulesManagementUrl = rulesManagementUrl + '/';
      }
    }
  }

  @Required
  public void setJdbcConnectionManager(JdbcConnectionManager jdbcConnectionManager) {
    this.jdbcConnectionManager = jdbcConnectionManager;
  }

  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  public long getCachePeriod() {
    return cachePeriod;
  }

  public void setCachePeriod(long cachePeriod) {
    this.cachePeriod = cachePeriod;
  }

  private synchronized PackageCache getPackageCache(String packageName) {
    if (cacheMap.containsKey(packageName)) {
      return cacheMap.get(packageName);
    }
    else {
      PackageCache packageCache = new PackageCache(jdbcConnectionManager, configStore, rulesManagementUrl, packageName);

      PackageInfo info = packageCache.getPackageInfo(PackageCache.CacheStrategy.GetFromSource);
      //if (info.getEntry() != null && info.getEntry().getData() != null && info.getEntry().getData().length() > 0) {
        cacheMap.put(packageName, packageCache);
      //}
      return packageCache;
    }
  }

  public boolean isEntryOld(String packageName, PackageCache.Entry entry) {
    return getPackageCache(packageName).isEntryOld(entry);
  }

  public PackageCache.Entry getValidationRules(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getRuleContent(PackageCache.CacheType.Validation, "validation", cacheStrategy);
  }

  public PackageCache.Entry getDocumentRules(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getRuleContent(PackageCache.CacheType.Document, "document", cacheStrategy);
  }

  public PackageCache.Entry getEvaluationRules(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getRuleContent(PackageCache.CacheType.Evaluation, "evaluation", cacheStrategy);
  }

  public PackageCache.Entry getLookups(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getRuleContent(PackageCache.CacheType.Data, "lookups", cacheStrategy);
  }

  public PackageInfo getPackageInfo(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getPackageInfo(cacheStrategy);
  }

  public String getTests(String packageName, String fileName) {
    return getPackageCache(packageName).getTests(fileName);
  }

  public ValidationData getValidationByEndpoint(
          String packageName,
          PackageCache.CacheStrategy cacheStrategy,
          String endpoint) {
    return getPackageCache(packageName).getConnectionValidationByEndpoint(cacheStrategy, endpoint);
  }

  public List<ValidationData> getValidations(
          String packageName,
          PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getConnectionValidations(cacheStrategy);
  }

  public List<String> getParentPackageNames(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getParentPackageNames(cacheStrategy);
  }

  public List<String> getFeaturePackageNames(String packageName, PackageCache.CacheStrategy cacheStrategy) {
    return getPackageCache(packageName).getFeaturePackageNames(cacheStrategy);
  }
}
