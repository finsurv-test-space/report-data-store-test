package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecision;

import java.util.List;

/**
 * Created by james on 2017/09/05.
 */
public class DecisionDownload {
  private final String upToSyncpoint;
  private final List<FullyReferencedEvaluationDecision> evaluationDecisionList;

  public DecisionDownload(String upToSyncpoint, List<FullyReferencedEvaluationDecision> evaluationDecisionList) {
    this.upToSyncpoint = upToSyncpoint;
    this.evaluationDecisionList = evaluationDecisionList;
  }

  public String getUpToSyncpoint() {
    return upToSyncpoint;
  }

  public List<FullyReferencedEvaluationDecision> getEvaluationDecisionList() {
    return evaluationDecisionList;
  }
}

