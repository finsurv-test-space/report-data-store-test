package za.co.synthesis.regulatory.report.persist.types;

import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by james on 2017/10/02.
 */
@Entity
public class CriterionObject {
  @Enumerated
  private CriterionType type;
  private String name;
  @ManyToMany(fetch= FetchType.EAGER)
  private List<String> values;
  private String source;


  public CriterionObject(CriterionType type, String name, List<String> values, String source) {
    this.type = type;
    this.name = name;
    this.values = values;
    this.source = source;
  }

  public CriterionType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public List<String> getValues() {
    return values;
  }

  public CriterionData getCriterionData() {
    return new CriterionData(this.type, this.name, this.values, this.source);
  }

}
