package za.co.synthesis.regulatory.report.support.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;

import java.io.*;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jake on 3/17/17.
 */
public class CompoundProperties {
  static final Logger logger = LoggerFactory.getLogger(CompoundProperties.class);

  private final String propertyFilename;
  private final JSObject environments;
  private final JSObject propertySets;

  private static final String ENVIRONMENT = "environment";
  private static final String PROPERTYSET = "propertySet";
  private static final String DEPENDSON = "dependsOn";
  private static final String PARAMETER = "parameter";

  private CompoundProperties(String propertyFilename, JSObject environments, JSObject propertySets) {
    this.propertyFilename = propertyFilename;
    this.environments = environments;
    this.propertySets = propertySets;
  }

  private static JSObject findJSObject(Object properties, String propertyName) {
    JSObject parent = JSUtil.findObjectWith(properties, propertyName);
    Object objProperty = parent.get(propertyName);
    if (objProperty instanceof JSObject)
      return (JSObject)objProperty;
    return null;
  }

  /**
   * This will load properties from a Reader object
   * @param name
   * @param reader
   * @throws PropertyException
   */
  public static CompoundProperties loadReader(final String name, final Reader reader) throws PropertyException {
    try {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();

      JSObject environments = findJSObject(obj, ENVIRONMENT);
      JSObject propertySets = findJSObject(obj, PROPERTYSET);
      return new CompoundProperties(name, environments, propertySets);
    }
    catch (Exception e) {
      throw new PropertyException("Could not parse '" + name + "' property file", e);
    }
  }

  /**
   * This will load properties from a JavaScript file
   * @param ctx
   * @param path
   * @throws PropertyException
   */
  public static CompoundProperties loadFromPath(ApplicationContext ctx, final String path) throws PropertyException {
    try {
      logger.debug("...Properties file path to load \""+path+"\".");
      Reader reader = loadResourceAsReader(ctx, path);
      if (reader != null) {
        return loadReader(path, reader);
      }
      else {
        throw new PropertyException("Resource '" + path + "' with properties does not exist");
      }
    }
    catch (Exception e) {
      throw new PropertyException("Could not load '" + path + "' properties file", e);
    }
  }

  /**
   * This will load properties from a JavaScript file
   * @param file
   * @throws PropertyException
   */
  public static CompoundProperties loadFromFile(final File file) throws PropertyException {
    try {
      logger.debug("...Properties (File) to load \""+file.getAbsolutePath()+"\".");
      FileReader fileReader = new FileReader(file);
      return loadReader(file.getName(), fileReader);
    }

    catch (Exception e) {
      throw new PropertyException("Could not load '" + file.getName() + "' properties file", e);
    }
  }
  
  private static Reader loadResourceAsReader(ApplicationContext ctx, String path) throws IOException {
    logger.debug("...Properties resource to load \""+path+"\".");
    Resource resource = ctx.getResource(path);
    if (resource != null ) {
      return new BufferedReader(new InputStreamReader(resource.getInputStream()));
    }
    return null;
  }


  private Properties getParameters(JSObject jsEnv) {
    Properties props = new Properties();

    Object objParameter = jsEnv.get(PARAMETER);
    if (objParameter instanceof JSObject) {
      JSObject parameters = (JSObject)objParameter;
      for (Map.Entry<String, Object> entry : parameters.entrySet()) {
        if (entry.getValue() != null && !props.contains(entry.getKey())) {
          String val = entry.getValue().toString();
          try {
            if (val.matches(CompoundResource.cryptoRegex)) {
              val = decryptCryptoParam(val);
            } else if (val.matches(CompoundResource.encryptRegex)) {
              val = unwrapCryptoParam(val);
            }
          } catch(Exception e){
            logger.error("Error decrypting crypto field ({}:{}): {}", entry.getKey(), val, e.getMessage());
          }
          props.put(entry.getKey(), val);
        }
      }
    }

    return props;
  }

  public static String unwrapCryptoParam(String val) {
    return val.substring(CompoundResource.encryptStringPrefix.length(), val.indexOf(CompoundResource.encryptStringSuffix));
    //return CompoundResource.cryptoLib.decrypt(val.substring(CompoundResource.encryptStringPrefix.length(), val.indexOf(CompoundResource.encryptStringSuffix)));
  }

  public static String decryptCryptoParam(String val) {
    return CompoundResource.cryptoLib.decrypt(val.substring(CompoundResource.cryptoStringPrefix.length(),  val.indexOf(CompoundResource.cryptoStringSuffix)));
  }


  private String composeValue(Properties parameters, String key, String valueWithParams) {
    if (valueWithParams.contains("${")) {
      for (Map.Entry<Object, Object> entry : parameters.entrySet()) {
        String paramName = "${" + entry.getKey() + "}";
        String paramValue = entry.getValue().toString();

        while (valueWithParams.contains(paramName) && !paramName.equals(paramValue)) {
          //logger.info("*** Injecting param value: "+paramName+"="+paramValue+" \t=> \t"+key+"="+valueWithParams);
          valueWithParams = valueWithParams.replace(paramName, entry.getValue().toString());
        }
      }
    }
    return valueWithParams;
  }

  private void populatePropertiesInSet(Properties parameters, Object objSet, Properties properties) {
    if (objSet instanceof JSObject) {
      JSObject jsSet = (JSObject)objSet;
      for (Map.Entry<String, Object> entry : jsSet.entrySet()) {
        if (!(entry.getKey().equals(DEPENDSON) || entry.getKey().equals(PARAMETER))) {
          if (entry.getValue() == null) {
            if (!properties.containsKey(entry.getKey())) {
              properties.setProperty(entry.getKey(), "");
            }
          }
          else
          if (entry.getValue() instanceof String) {
            if (!properties.containsKey(entry.getKey())) {
              properties.setProperty(entry.getKey(), composeValue(parameters, entry.getKey(), (String) entry.getValue()));
            }
          }
          else
          if (entry.getValue() instanceof Boolean) {
            if (!properties.containsKey(entry.getKey())) {
              properties.setProperty(entry.getKey(), composeValue(parameters, entry.getKey(), entry.getValue().toString()));
            }
          }
        }
      }
    }
  }

  private void populatePropertiesForSet(Properties parameters, String setName, Properties properties) throws PropertyException {
    if (propertySets.containsKey(setName)) {
      Object objSet = propertySets.get(setName);
      populatePropertiesInSet(parameters, propertySets.get(setName), properties);
    }
    else {
      throw new PropertyException("Cannot find set '" + setName + "' in propertySets");
    }
  }


  public Properties getPropertiesForEnvironment(String env) throws PropertyException {
    try {
      logger.debug("...Find environment \""+env+"\" in properties...");
      Object objEnv = environments.get(env);
      if (objEnv instanceof JSObject) {
        Properties props = new Properties();

        JSObject jsEnv = (JSObject) objEnv;
        Properties parameters = getParameters(jsEnv);

        populatePropertiesInSet(parameters, jsEnv, props);

        Object objDependsOn = jsEnv.get(DEPENDSON);
        if (objDependsOn instanceof JSArray) {
          JSArray dependsOnList = (JSArray) objDependsOn;
          for (Object dependsOn : dependsOnList) {
            populatePropertiesForSet(parameters, dependsOn.toString(), props);
          }
        }
        return props;
      } else {
        throw new PropertyException("Could not find environment '" + env + "' in properties file '" + propertyFilename + "'");
      }
    }
    catch (PropertyException e) {
      throw new PropertyException("Error loading environment '" + env + "'", e);
    }
  }

  public Properties getParametersForEnvironment(String env) throws PropertyException {
    try {
      logger.debug("...Loading properties (" + propertyFilename + ") for environment \""+env+"\"...");
      Object objEnv = environments.get(env);
      if (objEnv instanceof JSObject) {
        JSObject jsEnv = (JSObject) objEnv;
        Properties parameters = getParameters(jsEnv);
        return parameters;
      } else {
        throw new PropertyException("Could not find environment '" + env + "' in properties file '" + propertyFilename + "'");
      }
    }
    catch (PropertyException e) {
      throw new PropertyException("Error loading environment '" + env + "'", e);
    }
  }
}
