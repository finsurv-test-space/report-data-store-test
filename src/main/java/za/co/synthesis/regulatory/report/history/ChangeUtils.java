package za.co.synthesis.regulatory.report.history;

import com.fasterxml.jackson.databind.ObjectMapper;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.regulatory.report.persist.JsonConstant;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.ChangeItem;
import za.co.synthesis.regulatory.report.schema.ChangeType;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.rule.core.Scope;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangeUtils {
  private interface IChange {
    void Added(String field, String to);
    void Removed(String field, String from);
    void Updated(String field, String from, String to);

    IChange getDeeperChange(String field);
  }

  private static class MetaChange implements IChange {
    private final String prefix;
    private final List<ChangeItem> changes;

    public MetaChange(List<ChangeItem> changes) {
      this.prefix = "";
      this.changes = changes;
    }

    public MetaChange(String prefix, List<ChangeItem> changes) {
      this.prefix = prefix;
      this.changes = changes;
    }

    public void Added(String field, String to) {
      changes.add(createAddMetaField(prefix + field, to));
    }

    public void Removed(String field, String from) {
      changes.add(createRemoveMetaField(prefix + field, from));
    }

    public void Updated(String field, String from, String to) {
      changes.add(createUpdatedMetaField(prefix + field, from, to));
    }

    public IChange getDeeperChange(String field) {
      return new MetaChange(prefix.length() > 0 ? prefix + field + "." : field + ".", changes);
    }
  }

  private static class TransactionChange implements IChange {
    private final String prefix;
    private final List<ChangeItem> changes;

    public TransactionChange(List<ChangeItem> changes) {
      this.prefix = "";
      this.changes = changes;
    }

    public TransactionChange(String prefix, List<ChangeItem> changes) {
      this.prefix = prefix;
      this.changes = changes;
    }

    public void Added(String field, String to) {
      changes.add(createAddTransactionField(prefix + field, to));
    }

    public void Removed(String field, String from) {
      changes.add(createRemoveTransactionField(prefix + field, from));
    }

    public void Updated(String field, String from, String to) {
      changes.add(createUpdatedTransactionField(prefix + field, from, to));
    }

    public IChange getDeeperChange(String field) {
      return new TransactionChange(prefix.length() > 0 ? prefix + field + "." : field + ".", changes);
    }
  }

  private static class MoneyChange implements IChange {
    private final String prefix;
    private final Integer index;
    private final List<ChangeItem> changes;

    public MoneyChange(Integer index, List<ChangeItem> changes) {
      this.index = index;
      this.prefix = "";
      this.changes = changes;
    }

    public MoneyChange(Integer index, String prefix, List<ChangeItem> changes) {
      this.index = index;
      this.prefix = prefix;
      this.changes = changes;
    }

    public void Added(String field, String to) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createAddMoneyField(index,prefix + field, to));
    }

    public void Removed(String field, String from) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createRemoveMoneyField(index,prefix + field, from));
    }

    public void Updated(String field, String from, String to) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createUpdatedMoneyField(index,prefix + field, from, to));
    }

    public IChange getDeeperChange(String field) {
      return new MoneyChange(index, prefix.length() > 0 ? prefix + field + "." : field + ".", changes);
    }
  }

  private static class ImportExportChange implements IChange {
    private final String prefix;
    private final Integer moneyIndex;
    private final Integer ieIndex;
    private final List<ChangeItem> changes;

    public ImportExportChange(Integer moneyIndex, Integer ieIndex, List<ChangeItem> changes) {
      this.moneyIndex = moneyIndex;
      this.ieIndex = ieIndex;
      this.prefix = "";
      this.changes = changes;
    }

    public ImportExportChange(Integer moneyIndex, Integer ieIndex, String prefix, List<ChangeItem> changes) {
      this.moneyIndex = moneyIndex;
      this.ieIndex = ieIndex;
      this.prefix = prefix;
      this.changes = changes;
    }

    public void Added(String field, String to) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createAddIEField(moneyIndex, ieIndex,prefix + field, to));
    }

    public void Removed(String field, String from) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createRemoveIEField(moneyIndex, ieIndex,prefix + field, from));
    }

    public void Updated(String field, String from, String to) {
      if (!field.equals(JsonConstant.UniqueObjectInstanceID))
        changes.add(createUpdatedIEField(moneyIndex, ieIndex,prefix + field, from, to));
    }

    public IChange getDeeperChange(String field) {
      return new ImportExportChange(moneyIndex, ieIndex, prefix.length() > 0 ? prefix + field + "." : field + ".", changes);
    }
  }


  public static ChangeItem createAddMetaField(String field, String to) {
    return new ChangeItem(ChangeType.Added, null, field, null, null, null, to);
  }

  public static ChangeItem createRemoveMetaField(String field, String from) {
    return new ChangeItem(ChangeType.Removed, null, field, null, null, from, null);
  }

  public static ChangeItem createUpdatedMetaField(String field, String from, String to) {
    return new ChangeItem(ChangeType.Updated, null, field, null, null, from, to);
  }

  public static ChangeItem createAddTransactionField(String field, String to) {
    return new ChangeItem(ChangeType.Added, Scope.Transaction, field, null, null, null, to);
  }

  public static ChangeItem createRemoveTransactionField(String field, String from) {
    return new ChangeItem(ChangeType.Removed, Scope.Transaction, field, null, null, from, null);
  }

  public static ChangeItem createUpdatedTransactionField(String field, String from, String to) {
    return new ChangeItem(ChangeType.Updated, Scope.Transaction, field, null, null, from, to);
  }

  public static ChangeItem createAddMoneyField(Integer moneyInd, String field, String to) {
    return new ChangeItem(ChangeType.Added, Scope.Money, field, moneyInd, null, null, to);
  }

  public static ChangeItem createRemoveMoneyField(Integer moneyInd, String field, String from) {
    return new ChangeItem(ChangeType.Removed, Scope.Money, field, moneyInd, null, from, null);
  }

  public static ChangeItem createUpdatedMoneyField(Integer moneyInd, String field, String from, String to) {
    return new ChangeItem(ChangeType.Updated, Scope.Money, field, moneyInd, null, from, to);
  }

  public static ChangeItem createAddIEField(Integer moneyInd, Integer importexportInd, String field, String to) {
    return new ChangeItem(ChangeType.Added, Scope.ImportExport, field, moneyInd, importexportInd, null, to);
  }

  public static ChangeItem createRemoveIEField(Integer moneyInd, Integer importexportInd, String field, String from) {
    return new ChangeItem(ChangeType.Removed, Scope.ImportExport, field, moneyInd, importexportInd, from, null);
  }

  public static ChangeItem createUpdatedIEField(Integer moneyInd, Integer importexportInd, String field, String from, String to) {
    return new ChangeItem(ChangeType.Updated, Scope.ImportExport, field, moneyInd, importexportInd, from, to);
  }

  private static boolean isStringArray(Object obj) {
    boolean result = false;
    if (obj instanceof List) {
      result = true;
      List list = (List)obj;
      for (Object objEntry : list) {
        if (!(objEntry instanceof String)) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

  private static String composeStringArray(Object objValue) {
    if (objValue instanceof List) {
      StringBuilder array = new StringBuilder();
      array.append("[");
      for (Object obj : (List)objValue) {
        if (array.length() > 1)
          array.append(", ");
        if (obj instanceof String)
          array.append('"').append(JSUtil.escapeJavaScriptString(obj.toString(), false)).append('"');
        else
          array.append(obj.toString());
      }
      array.append("]");

      return array.toString();
    }
    else
    if (objValue instanceof String) {
      return (String)objValue;
    }
    return null;
  }

  private static void getJsonChanges(IChange changes, Map<String, Object> fromVersion, Map<String, Object> toVersion) {
    // Values in FromVersion: Updated or Added in ToVersion
    for (Map.Entry<String, Object> fromEntry : fromVersion.entrySet()) {
      String key = fromEntry.getKey();
      Object fromValue = fromEntry.getValue();
      if (fromValue instanceof String) {
        if (toVersion.containsKey(key)) {
          Object toValue = toVersion.get(key);
          //check for diff.
          if (!fromEntry.getValue().equals(toValue)) {
            changes.Updated(key, fromValue.toString(), toValue != null ? toValue.toString() : null);
          }
        } else {
          changes.Removed(key, fromValue.toString());
        }
      }
      else
      if (isStringArray(fromValue)) {
        String fromValueStr = composeStringArray(fromValue);
        if (toVersion.containsKey(key)) {
          String toValueStr = composeStringArray(toVersion.get(key));
          //check for diff.
          if (!fromValueStr.equals(toValueStr)) {
            changes.Updated(key, fromValueStr, toValueStr);
          }
        } else {
          changes.Removed(key, fromValueStr);
        }
      }
      else
      if (fromValue instanceof Map) {
        if (toVersion.containsKey(key)) {
          Object toValue = toVersion.get(key);
          if (toValue instanceof Map) {
            getJsonChanges(changes.getDeeperChange(key), (Map)fromValue, (Map)toValue);
          }
          else {
            getJsonChanges(changes.getDeeperChange(key), (Map)fromValue, new HashMap<String, Object>());
          }
        }
        else {
          getJsonChanges(changes.getDeeperChange(key), (Map)fromValue, new HashMap<String, Object>());
        }
      }
    }
    // Values in ToVersion: Added to FromVersion
    for (Map.Entry<String, Object> toEntry : toVersion.entrySet()) {
      String key = toEntry.getKey();
      Object toValue = toEntry.getValue();
      if (toValue instanceof String) {
        if (!fromVersion.containsKey(key)) {
          changes.Added(key, toValue.toString());
        }
      }
      else
      if (toValue instanceof Map && !fromVersion.containsKey(key)) {
        getJsonChanges(changes.getDeeperChange(key), new HashMap<String, Object>(), (Map)toValue);
      }
    }
  }

  public static List<ChangeItem> getMetaChanges(ReportInfo prevData, ReportInfo nextData) {
    List<ChangeItem> changes = new ArrayList<>();
    if (prevData != null) {
      IChange metaChange = new MetaChange(changes);
      getJsonChanges(metaChange, prevData.getReportData().getMeta(), nextData.getReportData().getMeta());
    }
    return changes;
  }

  public static ChangeItem getStateChange(ReportInfo ri_prev, ReportInfo ri) {
    ChangeItem theStateChange = null;
    if (ri_prev == null && ri != null) {
      theStateChange = createAddMetaField("state", ri.getState());
    } else if (!ri_prev.getState().equals(ri.getState())) {
      theStateChange = createUpdatedMetaField("state",ri_prev.getState(), ri.getState());
    }
    return theStateChange;
  }

  private static Map<String, Object> getMonetaryAmount(Map<String, Object> fromMoney, List<Map<String, Object>> toList, List<Integer> selected) {
    String fromUniqueNo = (String)fromMoney.get(JsonConstant.UniqueObjectInstanceID);
    String fromSequenceNo = (String)fromMoney.get(JsonConstant.SequenceNumber);

    if (fromUniqueNo != null) {
      int index = 0;
      for (Map<String, Object> ma : toList) {
        index++;
        String toUniqueNo = (String)ma.get(JsonConstant.UniqueObjectInstanceID);
        if (fromUniqueNo.equals(toUniqueNo)) {
          if (!selected.contains(index)) {
            selected.add(index);
            return ma;
          }
        }
      }
    }

    //TODO: Check for data matches

    if (fromSequenceNo != null) {
      int index = 0;
      for (Map<String, Object> ma : toList) {
        index++;
        String toSequenceNo = (String)ma.get(JsonConstant.SequenceNumber);
        if (fromSequenceNo.equals(toSequenceNo)) {
          if (!selected.contains(index)) {
            selected.add(index);
            return ma;
          }
        }
      }
    }

    return null;
  }

  private static Map<String, Object> getImportExport(int fromSequenceNo, Map<String, Object> fromIE, List<Map<String, Object>> toList, List<Integer> selected) {
    //TODO: Check for data matches

    int index = 0;
    for (Map<String, Object> ie : toList) {
      index++;
      int toSequenceNo = index;
      if (fromSequenceNo == toSequenceNo) {
        if (!selected.contains(index)) {
          selected.add(index);
          return ie;
        }
      }
    }
    return null;
  }

  private static void getImportExportChanges(int moneySequenceNo, Map<String, Object> fromMoneyAmount, Map<String, Object> toMoneyAmount, List<ChangeItem> changes) {
    Object fromIEList = fromMoneyAmount.get(JsonConstant.ImportExport);
    Object toIEList = toMoneyAmount.get(JsonConstant.ImportExport);
    List<Integer> selected = new ArrayList<Integer>();

    if (fromIEList instanceof List && toIEList instanceof List) {
      int ieIndex = 0;
      for (Object fromIE : (List)fromIEList) {
        ieIndex++;
        int fromSequenceNo = ieIndex;
        Map<String, Object> toIE = getImportExport(ieIndex, (Map)fromIE, (List)toIEList, selected);
        if (toIE != null) {
          IChange ieChange = new ImportExportChange(moneySequenceNo, fromSequenceNo, changes);
          getJsonChanges(ieChange, (Map)fromIE, (Map)toIE);
        }
        else {
          changes.add(createRemoveMoneyField(moneySequenceNo, JsonConstant.ImportExport + "[]", Integer.toString(fromSequenceNo)));
          IChange ieChange = new ImportExportChange(moneySequenceNo, fromSequenceNo, changes);
          getJsonChanges(ieChange, (Map)fromIE, new HashMap<String, Object>());
        }
      }
      int index = 0;
      for (Object toIE : (List)toIEList) {
        index++;
        if (!selected.contains(index)) {
          int toSequenceNo = index;
          changes.add(createAddMoneyField( moneySequenceNo,JsonConstant.ImportExport + "[]", Integer.toString(toSequenceNo)));
          IChange ieChange = new ImportExportChange(moneySequenceNo, toSequenceNo, changes);
          getJsonChanges(ieChange, new HashMap<String, Object>(), (Map)toIE);
        }
      }
    }
    else
    if (fromIEList instanceof List && toIEList == null) {
      int index = 0;
      for (Object fromIE : (List)fromIEList) {
        index++;
        int fromSequenceNo = index;
        changes.add(createRemoveMoneyField( moneySequenceNo,JsonConstant.ImportExport + "[]", Integer.toString(fromSequenceNo)));
        IChange ieChange = new ImportExportChange(moneySequenceNo, fromSequenceNo, changes);
        getJsonChanges(ieChange, (Map)fromIE, new HashMap<String, Object>());
      }
    }
    else
    if (fromIEList == null && toIEList instanceof List) {
      int index = 0;
      for (Object toIE : (List)toIEList) {
        index++;
        int toSequenceNo = index;
        changes.add(createAddMoneyField( moneySequenceNo,JsonConstant.ImportExport + "[]", Integer.toString(toSequenceNo)));
        IChange ieChange = new ImportExportChange(moneySequenceNo, toSequenceNo, changes);
        getJsonChanges(ieChange, new HashMap<String, Object>(), (Map)toIE);
      }
    }
  }

  public static List<ChangeItem> getReportChanges(ReportInfo prevData, ReportInfo nextData) {
    List<ChangeItem> changes = new ArrayList<ChangeItem>();
    if (prevData == null) {
      return changes;
    }
    Map<String, Object> emptyMap = new HashMap<String, Object>();

    IChange tranChange = new TransactionChange(changes);
    getJsonChanges(tranChange, prevData.getReportData().getReport(), nextData.getReportData().getReport());

    Object fromMAList = prevData.getReportData().getReport().get(JsonConstant.MonetaryAmount);
    Object toMAList = nextData.getReportData().getReport().get(JsonConstant.MonetaryAmount);
    List<Integer> selected = new ArrayList<Integer>();

    if (fromMAList instanceof List && toMAList instanceof List) {
      for (Object fromMA : (List)fromMAList) {
        String fromSequenceNo = (String)((Map)fromMA).get(JsonConstant.SequenceNumber);
        Map<String, Object> toMA = getMonetaryAmount((Map)fromMA, (List)toMAList, selected);
        if (toMA != null) {
          String toSequenceNo = (String)((Map)toMA).get(JsonConstant.SequenceNumber);
          if (fromSequenceNo != null && !fromSequenceNo.equals(toSequenceNo)) {
            changes.add(createUpdatedTransactionField(JsonConstant.MonetaryAmount + "[]", fromSequenceNo, toSequenceNo));
          }
          IChange moneyChange = new MoneyChange(Integer.parseInt(fromSequenceNo), changes);
          getJsonChanges(moneyChange, (Map)fromMA, (Map)toMA);
          getImportExportChanges(Integer.parseInt(fromSequenceNo), (Map)fromMA, (Map)toMA, changes);
        }
        else {
          changes.add(createRemoveTransactionField(JsonConstant.MonetaryAmount + "[]", fromSequenceNo));
          IChange moneyChange = new MoneyChange(Integer.parseInt(fromSequenceNo), changes);
          getJsonChanges(moneyChange, (Map)fromMA, emptyMap);
          getImportExportChanges(Integer.parseInt(fromSequenceNo), (Map)fromMA, emptyMap, changes);
        }
      }
      int index = 0;
      for (Object toMA : (List)toMAList) {
        index++;
        if (!selected.contains(index)) {
          String toSequenceNo = (String)((Map)toMA).get(JsonConstant.SequenceNumber);
          changes.add(createAddTransactionField(JsonConstant.MonetaryAmount + "[]", toSequenceNo));
          IChange moneyChange = new MoneyChange(Integer.parseInt(toSequenceNo), changes);
          getJsonChanges(moneyChange, emptyMap, (Map)toMA);
          getImportExportChanges(Integer.parseInt(toSequenceNo), emptyMap, (Map)toMA, changes);
        }
      }
    }

    return changes;
  }


  private static void addChangeToMap(String fieldName, ChangeItem change, Map<String, Object> map) {
    if (fieldName.contains(".")) {
      int pos = fieldName.indexOf('.');
      String start = fieldName.substring(0, pos);
      if (map.containsKey(start)) {
        Object obj = map.get(start);
        if (obj instanceof Map) {
          addChangeToMap(fieldName.substring(pos+1), change, (Map) obj);
        }
      }
      else {
        if (change.getType() == ChangeType.Removed) {
          Map<String, Object> subMap = new HashMap<String, Object>();
          map.put(start, subMap);
          addChangeToMap("-" + fieldName.substring(pos + 1), change, subMap);
        }
      }
    }
    else {
      Object objFieldValue;
      if (map.containsKey(fieldName)) {
        if (change.getType() == ChangeType.Added) {
          objFieldValue = map.get(fieldName);
          map.remove(fieldName);
          map.put("+" + fieldName, objFieldValue);
        }
        else
        if (change.getType() == ChangeType.Updated) {
          objFieldValue = map.get(fieldName);
          map.remove(fieldName);
          map.put("-" + fieldName, change.getFrom());
          map.put("+" + fieldName, objFieldValue);
        }
      } else {
        if (change.getType() == ChangeType.Removed) {
          objFieldValue = change.getFrom();
          map.put("-" + fieldName, objFieldValue);
        }
      }
    }
  }

  public static void addChangesToReportData (List<ChangeItem> metaChanges, List<ChangeItem> reportChanges, ReportData reportData) {
    for (ChangeItem change : metaChanges) {
      addChangeToMap(change.getField(), change, reportData.getMeta());
    }
    for (ChangeItem change : reportChanges) {
      if (change.getScope() == Scope.Transaction) {
        addChangeToMap(change.getField(), change, reportData.getReport());
      }
      if (change.getScope() == Scope.Money) {
        Object obj = reportData.getReport().get(JsonConstant.MonetaryAmount);
        if (obj instanceof List) {
          List list = (List)obj;
          if (change.getMoneyInd() <= list.size()) {
            Object objEntry = list.get(change.getMoneyInd()-1);
            if (objEntry instanceof Map) {
              addChangeToMap(change.getField(), change, (Map)objEntry);
            }
          }
        }
      }
      if (change.getScope() == Scope.ImportExport) {
        Object objMA = reportData.getReport().get(JsonConstant.MonetaryAmount);
        if (objMA instanceof List) {
          List listMA = (List)objMA;
          if (change.getMoneyInd() <= listMA.size()) {
            Object objMAEntry = listMA.get(change.getMoneyInd()-1);
            if (objMAEntry instanceof Map) {
              Map mapMAEntry = (Map)objMAEntry;
              Object objIE = mapMAEntry.get(JsonConstant.ImportExport);
              if (objIE instanceof List) {
                List listIE = (List)objIE;
                if (change.getImportexportInd() <= listIE.size()) {
                  Object objIEEntry = listIE.get(change.getImportexportInd()-1);
                  if (objIEEntry instanceof Map) {
                    addChangeToMap(change.getField(), change, (Map)objIEEntry);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  public static ReportData deepCopy (ReportData reportData) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    String json = mapper.writeValueAsString(reportData);
    ReportData newReportData = mapper.readValue(json, ReportData.class);
    return newReportData;
  }
}
