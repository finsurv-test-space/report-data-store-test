package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by james on 2017/05/30.
 */
@JsonPropertyOrder({
        "Code"
        , "Description"
})
public class IndustrialClassification {
    private String code;
    private String description;

    public IndustrialClassification(Object code, Object description) {
        this.code = (String)code;
        this.description = (String)description;
    }

    @JsonProperty("Code")
    public String getCode() {
        return code;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }
}