package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.AccessSetData;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.impl.ListAndUserDataJSONHelper;
import za.co.synthesis.regulatory.report.support.AccessType;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/09/29.
 */
@Entity
public class AccessSetObject extends PersistenceObject{
  private static final Logger logger = LoggerFactory.getLogger(AccessSetObject.class);

  private String name;
  private String reportSpace;
  @Enumerated
  private AccessType accessType;
  @ManyToMany(fetch= FetchType.EAGER)
  private List<CriterionObject> accessCriteria = new ArrayList<CriterionObject>();

  private Map<String, Object> eventContext;
  private Integer version = 1;

  public AccessSetObject(String uuid) {
    super(uuid);
  }

  public AccessSetObject(String uuid, AccessSetData accessSet) {
    super(uuid);
    if (accessSet != null) {
      this.name = accessSet.getName();
      this.reportSpace = accessSet.getReportSpace();
      this.accessType = accessSet.getAccessType();
      if (accessSet.getAccessCriteria() != null) {
        for (CriterionData criterion : accessSet.getAccessCriteria()) {
          this.accessCriteria.add(new CriterionObject(criterion.getType(), criterion.getName(), criterion.getValues(), "ACCESS_SET"));
        }
      }
    } else {
      throw new IllegalArgumentException("Null instance of AccessSetData provided to AccessSetObject constructor.");
    }
  }

  public AccessSetObject(String uuid, String name, String reportSpace, AccessType accessType, List<CriterionData> accessCriteria) {
    super(uuid);
    this.name = name;
    this.reportSpace = reportSpace;
    this.accessType = accessType;
    if (accessCriteria != null && accessCriteria.size() > 0) {
      for (CriterionData criterion : accessCriteria) {
        this.accessCriteria.add(new CriterionObject(criterion.getType(), criterion.getName(), criterion.getValues(), "ACCESS_SET"));
      }
    }
  }

  public AccessSetData getAccessSetData(){
    AccessSetData accessSet = new AccessSetData();
    accessSet.setAccessType(this.accessType);
    accessSet.setName(this.name);
    accessSet.setReportSpace(this.reportSpace);
    accessSet.setAccessCriteria(getAccessCriteria());
    return accessSet;
  }

  public List<CriterionData> getAccessCriteria(){
    List<CriterionData> accessCriteriaSet = new ArrayList<CriterionData>();
    if (this.accessCriteria != null && this.accessCriteria.size() > 0) {
      for (CriterionObject criterion : this.accessCriteria) {
        accessCriteriaSet.add(new CriterionData(criterion.getType(), criterion.getName(), criterion.getValues(), "ACCESS_SET"));
      }
    }
    return accessCriteriaSet;
  }

  public String getAuthToken() {
    if (eventContext == null)
      eventContext = composeEventContext();
    if (eventContext != null)
      return mapToJsonStr(eventContext);
    else
      return null;
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public AccessType getAccessType() {
    return accessType;
  }

  private JSArray composeCriteria(List<CriterionObject> accessCriteria) {
    JSArray array = new JSArray();
    for (CriterionObject criterionObject : accessCriteria) {
      JSObject jsCriterion = new JSObject();
      if (criterionObject.getType() == CriterionType.Meta)
        jsCriterion.put("meta", criterionObject.getName());
      if (criterionObject.getType() == CriterionType.Report)
        jsCriterion.put("report", criterionObject.getName());
      if (criterionObject.getType() == CriterionType.System)
        jsCriterion.put("system", criterionObject.getName());
      jsCriterion.put("values", criterionObject.getValues());
      array.add(jsCriterion);
    }
    return array;
  }

  public String getContent() {
    JSObject jsonMap = new JSObject();
    jsonMap.put("name", name);
    jsonMap.put("reportSpace", reportSpace);
    if (accessType == AccessType.Read) {
      jsonMap.put("read", composeCriteria(accessCriteria));
    }
    else
    if (accessType == AccessType.Write) {
      jsonMap.put("write", composeCriteria(accessCriteria));
    }
    return mapToJsonStr(jsonMap);
  }

  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);
        if (version == 1) {
          AccessSetData accessSetData = ListAndUserDataJSONHelper.parseAccessSet(jsonMap);
          name = accessSetData.getName();
          reportSpace = accessSetData.getReportSpace();
          accessType = accessSetData.getAccessType();
          accessCriteria.clear();
          for (CriterionData data : accessSetData.getAccessCriteria()) {
            accessCriteria.add(new CriterionObject(data.getType(), data.getName(), data.getValues(), "ACCESS_SET"));
          }
        }
      } catch (Exception e){
        logger.error("Unable to deserialize json content for AccessSet object: " + e.getMessage(), e);
      }
    }
  }

  public String getKey() {
    return name;
  }

  public void setKey(String key) {
    this.name = key;
  }
}
