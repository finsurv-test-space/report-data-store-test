package za.co.synthesis.regulatory.report.security.jwt;

import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.PublicKeyRef;

public interface IPublicKeySource {
  
  PublicKeyRef fetchKey(String keySource, String keyId) throws Exception ;

    void setSystemInformation(SystemInformation systemInformation);
}
