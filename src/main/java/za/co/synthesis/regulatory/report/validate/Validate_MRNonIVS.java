package za.co.synthesis.regulatory.report.validate;

import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.Map;

/**
 * User: jake
 * Date: 1/20/16
 * Time: 1:00 PM
 * Blank implementation of the function to validate errors relating to MRNNotOnIVS:
 * 221, "If outflow and the category 103/01 to 103/10 or 105 or 106, and no MRN can be found on the IVS, must contain the value Y"
 * 232, "If the MRN is registered on the IVS, it must not be completed"
 * 500, "If outflow and the category 103/01 to 103/10 or 105 or 106, the number supplied must be a valid MRN stored in the IVS"
 */
public class Validate_MRNonIVS implements ICustomValidate {
  public Validate_MRNonIVS() {}

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    return new CustomValidateResult(StatusType.Pass);
  }
}

