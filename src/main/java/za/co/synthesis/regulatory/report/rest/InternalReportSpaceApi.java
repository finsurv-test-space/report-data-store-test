package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.schema.ReportSpace;
import za.co.synthesis.regulatory.report.schema.State;
import za.co.synthesis.regulatory.report.schema.Transition;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.IChannelCache;
import za.co.synthesis.regulatory.report.support.properties.CompoundResource;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("internal/api/reportspace")
public class InternalReportSpaceApi extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalReportSpaceApi.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    ApplicationContext ctx;

    //TODO: Managing Reporting Spaces
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getListOfReportSpaces(HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getListOfReportSpaces()");
        return systemInformation.getReportSpaces();
    }

    @RequestMapping(value = "/{reportSpace}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ReportSpace getReportSpace(
            @PathVariable String reportSpace) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getReportSpace()");
        return systemInformation.getReportSpace(reportSpace);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @PreAuthorize("hasRole('SET_REPORT_SPACE')")
    public
    @ResponseBody
    void setReportSpace(@RequestBody ReportSpace reportSpace) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setReportSpace()");
        systemInformation.setReportSpace(reportSpace);
    }

    //TODO: Managing Reporting Space - Channels
    @RequestMapping(value = "/{reportSpace}/channels", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getChannelsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getChannelsForReportSpace()");
        return systemInformation.getReportSpaceChannels(reportSpace);
    }


    //TODO: Managing Reporting Space - States
    @RequestMapping(value = "/{reportSpace}/states", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<State> getStatesForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getStatesForReportSpace()");
        return systemInformation.getReportSpaceStates(reportSpace);
    }


    //TODO: Managing Reporting Space - Transitions
    @RequestMapping(value = "/{reportSpace}/transitions", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getTransitionsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getTransitionsForReportSpace()");
        return systemInformation.getReportSpaceTransitions(reportSpace);
    }
//    //----- Channel Interfaces  -----//
//    //CHANNEL CONFIG
//    void setChannel(Channel channelConfig);
//    List<DocumentDefinition> getDocumentTypesForCategory(String channelName, FlowType flow, String category);
//    //CHANNEL CONFIG
//    void setDocumentTypesForCategory(String channelName, FlowType flow, String category, List<DocumentDefinition> documents);

}
