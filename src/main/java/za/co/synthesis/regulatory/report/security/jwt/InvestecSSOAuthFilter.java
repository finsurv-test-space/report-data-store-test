package za.co.synthesis.regulatory.report.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class InvestecSSOAuthFilter extends OncePerRequestFilter {
  private static final Logger logger = LoggerFactory.getLogger(InvestecSSOAuthFilter.class);
  private GCNAuthModule gcnAuthModule;
//  private RememberMeServices rememberMeServices = new NullRememberMeServices();

  @Required
  public void setGcnAuthModule(GCNAuthModule gcnAuthModule) {
    this.gcnAuthModule = gcnAuthModule;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    if (SecurityContextHolder.getContext().getAuthentication() == null && gcnAuthModule.isAvailable()) {
      String GCN = request.getHeader("GCN");
      if (GCN == null) {
        GCN = request.getParameter("GCN");
      }
      if (GCN != null) {
        JWTAuthToken authToken = gcnAuthModule.authAndGenerateAccessToken(GCN, null);

        SecurityContextHolder.getContext().setAuthentication(authToken);
        request.setAttribute(SecurityHelper.REQUEST_AUTH_TOKEN, authToken);
      }
    }
    //If the GCN is not present, we continue through to the next filter.
    filterChain.doFilter(request, response);
  }
}
