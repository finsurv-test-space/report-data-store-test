package za.co.synthesis.regulatory.report.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.ParameterData;
import za.co.synthesis.regulatory.report.persist.ValidationData;
import za.co.synthesis.regulatory.report.support.JdbcConnectionManager;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.StatusType;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * Created by jake on 8/4/17.
 */
public class ValidateViaDatabaseTask implements Callable<CustomValidateResult> {
  private static final Logger log = LoggerFactory.getLogger(ValidateViaDatabaseTask.class);

  private final JdbcConnectionManager jdbcConnectionManager;
  private final IConfigStore configStore;
  private final ValidationData validationData;
  private final Map<String, Object> parameterValues;
  private final static Map<String, JdbcTemplate> jdbcTemplateMap = new ConcurrentHashMap<String, JdbcTemplate>();

  public ValidateViaDatabaseTask(JdbcConnectionManager jdbcConnectionManager, IConfigStore configStore, ValidationData validationData, Map<String, Object> parameterValues) {
    this.jdbcConnectionManager = jdbcConnectionManager;
    this.configStore = configStore;
    this.validationData = validationData;
    this.parameterValues = parameterValues != null ? parameterValues : new HashMap<String, Object>();
    fleshOutParameterValues();
  }

  private void fleshOutParameterValues() {
    for (ParameterData param : validationData.getParameters()) {
      if (!parameterValues.containsKey(param.getName())) {
        parameterValues.put(param.getName(), null);
      }
    }
  }

  @Override
  public CustomValidateResult call() {
    try {
      JdbcTemplate jdbcTemplate = jdbcConnectionManager.getJdbcTemplate(validationData.getDataSource());

      if (jdbcTemplate != null) {
        String sql = ValidationUtils.compose(validationData.getName(), validationData.getSql(), parameterValues, configStore, log);

        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        String responseName = "response";
        if (list != null) {
          parameterValues.put(responseName, list);
        } else {
          parameterValues.put(responseName, null);
        }

        String composedResult = ValidationUtils.compose(validationData.getName() + " " + responseName,
                validationData.getCompose(), parameterValues, configStore, log);

        CustomValidateResult result = ValidationUtils.parseComposedResponse(composedResult);
        if (result == null) {
          result = new CustomValidateResult(StatusType.Error, "NOR", "No valid result for " + validationData.getEndpoint() + getParamString());
        }
        return result;
      }
      return new CustomValidateResult(StatusType.Error, "NOP", "No database connection defined for endpoint " + validationData.getEndpoint());
    } catch (Exception e) {
      log.error("Exception running task for endpoint " + validationData.getEndpoint(), e);
      return new CustomValidateResult(StatusType.Error, "NOP", "Exception running task for endpoint " + validationData.getEndpoint() + getParamString());
    }
  }

  public String getParamString(){
    String params = "";
    if (validationData != null && validationData.getParameters() != null) {
      for (ParameterData param : validationData.getParameters()) {
        String paramName = param.getName() != null ? param.getName() : "[NO_NAME]";
        String paramPath = param.getPath() != null ? param.getPath() : "[NO_PATH]";
        String paramValue = (parameterValues!= null && parameterValues.containsKey(paramName) && parameterValues.get(paramName) != null) ? "" + parameterValues.get(paramName) + "" : "[NULL]";
        params += (params.length() > 0 ? ", " : "") + "" + paramName + "@" + paramPath + "=>" + paramValue;
      }
    }
    params = params.length()>0?params:"[NO_PARAMS_REQUIRED]";
    return " (PARAMS "+ params + ")";
  }
}
