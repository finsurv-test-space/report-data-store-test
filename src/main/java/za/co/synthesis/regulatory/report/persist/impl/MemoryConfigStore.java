package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.schema.ColumnDisplay;
import za.co.synthesis.regulatory.report.schema.StoredKey;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;
import za.co.synthesis.regulatory.report.support.FileUploadUtil;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;
import za.co.synthesis.regulatory.report.support.properties.CompoundResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * Created by jake on 5/29/17.
 */
public abstract class MemoryConfigStore implements IConfigStore, ApplicationContextAware {
  private static final Logger logger = LoggerFactory.getLogger(MemoryConfigStore.class);

  private CompoundResource environmentResources;
  private Properties environmentProperties;
  private ApplicationContext applicationContext;

  private String cacheDir;

  private AuthenticationManager authenticationManager;

  private JSObject package_config = null;
  private JSObject type_config = null;
  private JSObject rights_config = null;
  private JSObject auth_and_audit_config = null;
  private JSObject whitelists_config = null;
  private HashMap<String, UserData> users = null;
  //DataRepoName -> DataRepoConfig {lookups<>, validations<>}
  private Map<String, DataRepositoryDefinition> repo_definitions = null;
  private Map<String, ProxyCallData> proxy_definitions = null;

  private JSObject reportSpace_config = null;
  private Map<String, ReportSpaceData> reportSpace_map = null;
  private Map<String, ChannelData> channeldata_map = null;

  private Map<String, ListData> listdata_map = null;

  private JSObject user_config = null;
  private Map<String, UserData> userdata_map = null;
  private Map<String, AccessSetData> accessdata_map = null;

  private Map<String, StoredKey> key_map = new HashMap<String, StoredKey>();
  private String latest_key_id = null;
  
  private JSObject public_key_sources_config = null;
  private JSObject column_display_config = null;
  @Autowired
  private String defaultConfigLocation;

  @Autowired
  private String defaultTypeLocation;

  @Autowired
  private String defaultMiscLocation;

  public String getDefaultTypeLocation() {
    return defaultTypeLocation;
  }

  @Required
  public void setDefaultTypeLocation(String defaultTypeLocation) {
    this.defaultTypeLocation = makeLastCharacterAFileSeparator(defaultTypeLocation);
  }

  public String getDefaultMiscLocation() {
    return defaultMiscLocation;
  }

  @Required
  public void setDefaultMiscLocation(String defaultMiscLocation) {
    this.defaultMiscLocation = makeLastCharacterAFileSeparator(defaultMiscLocation);
  }


  public String getDefaultConfigLocation() {
      return defaultConfigLocation;
  }

  @Required
  public void setDefaultConfigLocation(String defaultConfigLocation) {
    this.defaultConfigLocation = makeLastCharacterAFileSeparator(defaultConfigLocation);
  }

 public String makeLastCharacterAFileSeparator(String path){
   if (path.endsWith("/") || path.endsWith("\\")) {
     return path;
   } else {
     return path + "/";
   }
 }

  public MemoryConfigStore() {
    cacheDir = System.getProperty("java.io.tmpdir");
    if (cacheDir != null) {
      char ch = cacheDir.charAt(cacheDir.length()-1);
      if (ch == '\\' || ch == '/') {
        this.cacheDir = cacheDir;
      }
      else {
        this.cacheDir = cacheDir + File.separator;
      }
    }
  }

  protected String getResourceName(String path, String defaultExt) throws IOException {
    Properties props = environmentProperties;
    if (props.containsKey(path)) {
      return props.getProperty(path);
    }
    return path + defaultExt;
  }

  protected JSObject getJSObject(String schema, String path) throws Exception {
    //if contains file: then use jsreadutil.loadfileasjsobject
    JSObject result;
    if (path.contains("classpath:/")) {
      result = JSReaderUtil.loadResourceAsJSObject(applicationContext,
              getResourceName(path, ".json"));
      if (result == null) {
        result = JSReaderUtil.loadResourceAsJSObject(applicationContext,
                getResourceName(path, ".js"));
      }
    } else {
      result = JSReaderUtil.loadResourceAsJSObject(Paths.get(path+".json"));
      if (result == null) {
        result = JSReaderUtil.loadResourceAsJSObject(Paths.get(path+".js"));
      }
    }
    return result;
  }

  @Required
  public void setEnvironmentProperties(CompoundResource environmentResources) throws IOException {
    this.environmentResources = environmentResources;
    this.environmentProperties = environmentResources.getEnvironmentProperties();
  }

  //----------------------------------------------------------------------------------------------------//
  //  Generic JSON Interface                                                                            //
  //----------------------------------------------------------------------------------------------------//
  @Override
  public JSObject getTypeJSObject(String path) throws Exception {
    return getJSObject("Type", path);
  }

  @Override
  public String getFreemarkerContent(String path) throws Exception {
    //TODO: THIS SHOULD BE EXTERNALIZED ... OR SHOULD BE EXTERNAL-FRIENDLY.
    Resource resource = applicationContext.getResource("classpath:/" + getResourceName(path, ".ftl"));
    if (resource != null && resource.exists()) {
      final int bufferSize = 1024;
      final char[] buffer = new char[bufferSize];
      final StringBuilder out = new StringBuilder();
      Reader in = new InputStreamReader(resource.getInputStream(), "UTF-8");
      for (; ; ) {
        int rsz = in.read(buffer, 0, buffer.length);
        if (rsz < 0)
          break;
        out.append(buffer, 0, rsz);
      }
      return out.toString();
    }
    return null;
  }

  //----- Temporary Cache Store (used) -----//
  @Override
  public void setCacheByName(String name, String data) {
    String path = cacheDir + name + "_cache.js";
    Charset encoding = StandardCharsets.UTF_8;
    try
    {
      //check if path exist, otherwise attempt to create
      Path cacheFile = Paths.get(path);
      Path directory = cacheFile.getParent();
      Files.createDirectories(directory);
      Files.write(cacheFile, data.getBytes(encoding),
              StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }
    catch (IOException e) {
      logger.warn("Cannot write cached entry to file name '{}'", path, e);
    }
  }

  @Override
  public String getCacheByName(String name) {
    String path = cacheDir + name + "_cache.js";
    Charset encoding = StandardCharsets.UTF_8;
    try
    {
      byte[] encoded = Files.readAllBytes(Paths.get(path));
      return new String(encoded, encoding);
    }
    catch (IOException e) {
      logger.warn("Cannot load cached entry from file name '" + path + "'");
      return null;
    }
  }


  private synchronized void loadReportSpaceConfig() {
    if (reportSpace_config == null) {
      try {
//        reportSpace_config = getJSObject("ReportSpace", "config/" + "reportspace_config");
        reportSpace_config = getJSObject("ReportSpace", defaultConfigLocation + "reportspace_config");

        reportSpace_map = ReportSpaceJSONHelper.parseReportSpaceList(reportSpace_config);
      } catch (Exception e) {
        logger.warn("Cannot load "+ defaultConfigLocation + "reportspace_config.json that is configured in the properties.json with key [defaultConfigLocation]", e);
      }
    }
  }

  private synchronized void loadPackageConfig() {
    if (package_config == null) {
      try {
        package_config = getJSObject("ReportSpace", defaultConfigLocation + "package_config");

        channeldata_map = ReportSpaceJSONHelper.parseChannelDataMap(package_config);

        //Load the repo definitions into memory...
        loadRepoLookups();
      } catch (Exception e) {
        logger.warn("Cannot load "+defaultConfigLocation+"package_config.json that is configured in the properties.json with key [defaultConfigLocation]", e);
      }
    }
  }

  private synchronized void loadUserConfig() {
    if (user_config == null) {
      try {
        user_config = getJSObject("User", "classpath://config/user_config");

        userdata_map = ListAndUserDataJSONHelper.parseUserDataMap(user_config);
        accessdata_map = ListAndUserDataJSONHelper.parseAccessSetMap(user_config);
      } catch (Exception e) {
        logger.warn("Cannot load config/user_config.json", e);
      }
    }
  }

  private synchronized void loadListConfig() {
    if (listdata_map == null) {
      try {
        JSObject jsLists = getJSObject("List", defaultConfigLocation +"list_config");

        listdata_map = ListAndUserDataJSONHelper.parseListDataMap(jsLists);
      } catch (Exception e) {
        logger.warn("Cannot load "+defaultConfigLocation +"list_config.json that is configured in the properties.json with key [defaultConfigLocation]", e);
      }
    }
  }

  private synchronized void loadTypeConfig() {
    if (type_config == null) {
      try {
        type_config = getJSObject("Type", defaultTypeLocation+"type_config");
      } catch (Exception e) {
        logger.warn("Cannot load "+defaultTypeLocation+"type_config.json", e);
      }
    }
  }

  private synchronized void loadProxyCallConfig() {
    if (proxy_definitions == null || proxy_definitions.size() == 0) {
      try {
        JSObject jsProxyCalls = getJSObject("ProxyCalls", defaultConfigLocation +"proxycall_config");

        proxy_definitions = DataRepositoryJSONHelper.parseProxyCallData(environmentResources.getEnvironmentParameters(), jsProxyCalls);

      } catch (Exception e) {
        logger.warn("Cannot load "+defaultConfigLocation +"proxycall_config.json that is configured in the properties.json with key [defaultConfigLocation]", e);
      }
    }
  }

  private synchronized void loadColumnDisplayConfig() {
    if (column_display_config == null || column_display_config.size() == 0) {
      try {
          //TODO: if necessary or appriopriate, move this over to the external configs when RSUI is ready for such a change.
          //NOTE: IT MAY NOT BE NECESSARY TO EXTERNALISE THIS FILE - RSUI CAN MANAGE AND MAINTAIN THIS THROUGH THE APIS AND DB
//        column_display_config = getJSObject("Display", defaultMiscLocation+"config/rsui_config");
        column_display_config = getJSObject("Display", "classpath://config/rsui_config");
      } catch (Exception e) {
        logger.warn("Cannot load config/rsui_config.json", e);
      }
    }
  }


  private JSObject getTypes() {
    loadTypeConfig();
    if (type_config != null)
      return (JSObject) type_config.get("types");
    return null;
  }


  private synchronized void loadRightsConfig() {
    if (rights_config == null) {
      try {
        rights_config = getJSObject("Rights", defaultMiscLocation+"rights_config");
      } catch (Exception e) {
        logger.warn("Cannot load config/rights_config.json", e);
      }
    }
  }

  private JSArray getRights(){
    loadRightsConfig();
    if (rights_config != null)
      return (JSArray) rights_config.get("rights");
    return null;
  }

  private synchronized void loadPKSourcesConfig() {
    if (public_key_sources_config == null) {
      try {
        public_key_sources_config = getJSObject("Sources", defaultMiscLocation+"jwt_ext_key_srcs");
      } catch (Exception e) {
        logger.warn("Cannot load config/jwt_ext_key_srcs.json", e);
      }
    }
  }

  private JSArray getPKSourcesConfig(){
    loadPKSourcesConfig();
    if (public_key_sources_config != null)
      return (JSArray) public_key_sources_config.get("sources");
    return null;
  }

  private synchronized void loadRequestParamsConfig() {
    if (auth_and_audit_config == null) {
      try {
        auth_and_audit_config = getJSObject("Request", defaultMiscLocation+"authentication_and_audit_config");
      } catch (Exception e) {
        logger.warn("Cannot load config/authentication_and_audit_config.json", e);
      }
    }
  }

  private synchronized void loadWhitelistParamsConfig() {
    if (whitelists_config == null) {
      try {
        whitelists_config = getJSObject("whitelist", defaultMiscLocation+"whitelist_config");
      } catch (Exception e) {
        logger.warn("Cannot load config/whitelist_config.json", e);
      }
    }
  }

  private Map getAuthTokenPopulateFromRequestMap(){
    loadRequestParamsConfig();
    if (auth_and_audit_config != null) {
      Object authToken = auth_and_audit_config.get("authToken");
      if (authToken instanceof Map) {
        return (Map) ((Map) authToken).get("request");
      }
    }
    return null;
  }

  private Map getAuditPopulateMap(){
    loadRequestParamsConfig();
    if (auth_and_audit_config != null) {
      Object audit = auth_and_audit_config.get("audit");
      if (audit instanceof Map) {
        return (Map) audit;
      }
    }
    return null;
  }

  private Map getWhitelistsMap(){
    loadWhitelistParamsConfig();
    if (whitelists_config != null) {
      Object whitelists = whitelists_config.get("whitelist");
      if (whitelists instanceof Map) {
        return (Map) whitelists;
      }
    }
    return null;
  }
  
  
  public FileUploadUtil.MimeTypeWhiteListPass getMimeTypeWhitelistMechanism(){
    Map whitelist = getWhitelistsMap();
    String mechanism = null;
    String labelName = "mimeTypeMechanism";
    if (whitelist != null && whitelist.containsKey(labelName) && whitelist.get(labelName) instanceof String) {
      try {
        mechanism = (String) whitelist.get(labelName);
      } catch(Exception e){
        //unable to fetch mimetype validation mechanism - defaults to "Any".
      }
    }
    return FileUploadUtil.MimeTypeWhiteListPass.fromString(mechanism);
  }
  
  public List<String> getExtensionWhitelist() {
    Map whitelist = getWhitelistsMap();
    List<String> extensionWhitelist = new ArrayList<>();
    String labelName = "extensions";
    if (whitelist != null && whitelist.containsKey(labelName) && whitelist.get(labelName) instanceof List) {
      for (Object wObj : (List)whitelist.get(labelName)){
        if (wObj instanceof String) {
          extensionWhitelist.add((String)wObj);
        } else {
          try {
            extensionWhitelist.add(wObj.toString());
          } catch(Exception e){}
        }
      }
    }
    return extensionWhitelist;
  }

  public List<String> getMimeTypeWhitelist() {
    Map whitelist = getWhitelistsMap();
    List<String> mimeTypeWhitelist = new ArrayList<>();
    String labelName = "mimeTypes";
    if (whitelist != null && whitelist.containsKey(labelName) && whitelist.get(labelName) instanceof List) {
      for (Object wObj : (List)whitelist.get(labelName)){
        if (wObj instanceof String) {
          mimeTypeWhitelist.add((String)wObj);
        } else {
          try {
            mimeTypeWhitelist.add(wObj.toString());
          } catch(Exception e){}
        }
      }
    }
    return mimeTypeWhitelist;
  }

  //----------------------------------------------------------------------------------------------------//
  //  Channel Interfaces                                                                                //
  //----------------------------------------------------------------------------------------------------//
  @Override
  public Map<String, ChannelData> getChannelData() {
    loadPackageConfig();
    return channeldata_map;
  }

  @Override
  public void setChannelData(ChannelData channelData) {
    loadPackageConfig();
    channeldata_map.put(channelData.getChannelName(), channelData);
  }

  //----------------------------------------------------------------------------------------------------//
  //  Type Interfaces                                                                                   //
  //----------------------------------------------------------------------------------------------------//
  @Override
  public List<String> getTypeSchemas(String typeName) {
    List<String> result = new ArrayList<String>();
    JSObject types = getTypes();
    if (types != null) {
      if (types.containsKey(typeName)) {
        Object objType = types.get(typeName);
        if (objType instanceof JSArray) {
          for (Object entry : (JSArray)objType) {
            result.add(entry.toString());
          }
        }
        if(objType instanceof String) {
          result.add((String) objType);
        }
      }
    }
    return result;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @Override
  public Integer getDefaultMaxDownloadRecords() {
    return 10;
  }

  @Override
  public void setDefaultMaxDownloadRecords(Integer max) {

  }


  //----------------------------------------------------------------------------------------------------//
  //  User Management Interface                                                                        //
  //----------------------------------------------------------------------------------------------------//
  public Map<String, UserData> getUserMap() {
    loadUserConfig();
    return userdata_map;
  }

  public void setUser(UserData user){
    loadUserConfig();
    userdata_map.put(user.getUsername(), user);
  }

  public void removeUser(String username) {
    userdata_map.remove(username);
  }

  //-----  User Access  -----//
  public Map<String, AccessSetData> getAccessSetMap() {
    return accessdata_map;
  }

  public void setAccessSet(AccessSetData accessSet) {
    accessdata_map.put(accessSet.getName(), accessSet);
  }

  @Override
  public void removeAccessSet(String setname) {
    accessdata_map.remove(setname);
  }

  @Override
  public List<String> getRightsList() {
    ArrayList<String> rightsList = new ArrayList<String>();
    for (Object obj : getRights()) {
      if (!rightsList.contains(obj.toString()))
        rightsList.add(obj.toString());
    }
    return rightsList;
  }

  @Override
  public void addRight(String right) {
    if (rights_config == null){
      getRights();
    }
    if (rights_config != null ) {
      Object rightsArr = rights_config.get("rights");
      if (rightsArr != null && rightsArr instanceof List && ((List)rightsArr).size() > 0) {
        if (!((List)rightsArr).contains(right)) {
          ((List) rightsArr).add(right);
        }
      }
    }
  }

  @Override
  public void removeRight(String right) {
    if (rights_config == null){
      getRights();
    }
    if (rights_config != null ) {
      Object rightsArr = rights_config.get("rights");
      if (rightsArr != null && rightsArr instanceof List && ((List)rightsArr).size() > 0) {
        if (((List)rightsArr).contains(right)) {
          ((List) rightsArr).remove(right);
        }
      }
    }
  }

//  @Override
//  public List<String> getUserEventsList(String reportSpace) {
//    ArrayList<String> userEventsList = new ArrayList<String>();
//
//    if (reportSpace_config!=null){
//      Object reportSpacesMap = reportSpace_config.get("reportspaces");
//      if (reportSpacesMap!=null &&
//              reportSpacesMap instanceof Map &&
//              !((Map)reportSpacesMap).isEmpty() &&
//              ((Map)reportSpacesMap).get(reportSpace) != null &&
//              ((Map)reportSpacesMap).get(reportSpace) instanceof Map){
//        Map<String,Object> reportSpaceObj = (Map<String,Object>) ((Map<String,Object>)reportSpacesMap).get(reportSpace);
//        List<Object> transitions = (List<Object>) reportSpaceObj.get("transitions");
//        if (transitions!=null){
//          for (Object transitionObj: transitions){
//            if (transitionObj != null && transitionObj instanceof Map){
//              String event = (String) ((Map<String,Object>) transitionObj).get("event");
//              if ("UserAction".equals(event)){
//                String userAction = (String) ((Map<String,Object>) transitionObj).get("action");
//                if (userAction != null &&
//                        userAction.length() > 0 &&
//                        !userEventsList.contains(userAction)){
//                  userEventsList.add(userAction);
//                }
//              }
//            }
//          }
//        }
//      }
//    }
//
//    return userEventsList;
//  }


  //-----  List Definitions  -----//
  public Map<String, ListData> getListData() {
    loadListConfig();
    return listdata_map;
  }

  @Override
  public void setListDefinition(ListData definition) {
    loadListConfig();
    listdata_map.put(definition.getName(), definition);
  }

  private synchronized DataRepositoryDefinition loadRepoFromJSFile(String repoName){
    DataRepositoryDefinition repoDefinition = null;
    try {
      JSObject repoJS = null;
      repoJS = getJSObject("Repo", defaultConfigLocation+repoName);

      repoDefinition = DataRepositoryJSONHelper.parseDataRepository(environmentResources.getEnvironmentParameters(), repoName, repoJS);
    } catch (Exception e) {
      logger.warn("Cannot load config "+repoName, e);
    }
    return repoDefinition;
  }

  private synchronized void loadRepoLookups() {
    if (repo_definitions==null){
      loadPackageConfig();
      repo_definitions = new HashMap<>();

      for (ChannelData channelData : channeldata_map.values()) {
        String repoName = channelData.getCallRepo();

        if (!repo_definitions.containsKey(repoName)){
          repo_definitions.put(repoName, loadRepoFromJSFile(repoName));
        }
      }
    }
  }

  @Override
  public Map<String, LookupData> getLookupsForRepo(String repo) {
    if (repo_definitions.containsKey(repo) &&
            repo_definitions.get(repo) != null &&
            repo_definitions.get(repo).getLookupDefinitions() != null){
      return repo_definitions.get(repo).getLookupDefinitions();
    }
    return null;
  }

  @Override
  public Map<String, ValidationData> getValidationsForRepo(String repo) {
    if (repo_definitions.containsKey(repo) &&
            repo_definitions.get(repo) != null &&
            repo_definitions.get(repo).getValidationDefinitions() != null){
      return repo_definitions.get(repo).getValidationDefinitions();
    }
    return null;
  }

  //----------  reportSpaces and Associated Channels  ----------//
  @Override
  public Map<String, ReportSpaceData> getReportSpaces() {
    loadReportSpaceConfig();
    if (reportSpace_map != null) {
      return reportSpace_map;
    }
    return new HashMap<String, ReportSpaceData>();
  }

  @Override
  public void setReportSpace(ReportSpaceData data) {
    loadReportSpaceConfig();
    reportSpace_map.put(data.getName(), data);
  }

  @Override
  public Map<String, ProxyCallData> getProxyCalls() {
    loadProxyCallConfig();
    return proxy_definitions;
  }

  @Override
  public UserData getUser(String username) {
    loadUserConfig();
    UserData user = userdata_map.get(username);
    return user;
  }

  @Override
  public List<RequestParamData> getAuthTokenPopulateMetaParamList() {
    ArrayList<RequestParamData> paramsList = new ArrayList<>();
    Map<String,Object> rp = getAuthTokenPopulateFromRequestMap();
    if (rp != null && rp.size()>0) {
      for (Map.Entry<String, Object> objectEntry : rp.entrySet()){
        if (objectEntry.getValue() instanceof Map) {
          RequestParamData param = new RequestParamData(objectEntry.getKey());
          param.configure((Map<String,Object>) objectEntry.getValue());
          paramsList.add(param);
        }
      }
    }
    return paramsList;
  }


  @Override
  public List<RequestParamData> getAuditParamList() {
    ArrayList<RequestParamData> paramsList = new ArrayList<>();
    Map<String,Object> apm = getAuditPopulateMap();
    if (apm != null && apm.size()>0) {
      for (Map.Entry<String, Object> objectEntry : apm.entrySet()){
        if (objectEntry.getValue() instanceof Map) {
          RequestParamData param = new RequestParamData(objectEntry.getKey());
          param.configure((Map<String,Object>) objectEntry.getValue());
          paramsList.add(param);
        }
      }
    }
    return paramsList;
  }

  @Override
  public Properties getEnvironmentParameters() {
    try {
      return environmentResources.getEnvironmentParameters();
    } catch (IOException e) {
      logger.error("Error getting Environment Parameters", e);
      return new Properties();
    }
  }

  @Override
  public Properties getEnvironmentProperties() {
    try {
    return environmentResources.getEnvironmentProperties();
    } catch (IOException e) {
      logger.error("Error getting Environment Properties", e);
      return new Properties();
    }
  }

  @Override
  public String setKeyPair(String kid, StoredKey storedPrivateKey, StoredKey storedPublicKey) {
    if (kid == null){
      kid = PersistenceObject.generateUUID();
    }
    latest_key_id = kid;
    if (storedPrivateKey != null) {
      key_map.put(kid + "_PVT", storedPrivateKey);
    }
    if (storedPublicKey != null) {
      key_map.put(kid, storedPublicKey);
    }
    return kid;
  }

  @Override
  public String getCurrentKeyId() {
    return latest_key_id;
  }

  @Override
  public StoredKey getPrivateKey(String kid) {
    if (kid != null) {
      return key_map.get(kid + "_PVT");
    }
    return null;
  }

  @Override
  public StoredKey getPublicKey(String kid) {
    if (kid != null) {
      return key_map.get(kid);
    }
    return null;
  }

  @Override
  public Map<String, StoredKey> getPublicKeys() {
    Map<String, StoredKey> manifest = new HashMap<String, StoredKey>();
    for (Map.Entry<String, StoredKey> entry : key_map.entrySet()) {
      if (!entry.getKey().endsWith("_PVT"))
        manifest.put(entry.getKey(), entry.getValue());
    }
    return manifest;
  }

  @Override
  public long getTokenExpiryMinutes(JWTAuthToken.TokenUse tokenUse) {
    if (tokenUse.equals(JWTAuthToken.TokenUse.ID)) {
      return 600;
    }
    if (tokenUse.equals(JWTAuthToken.TokenUse.ACCESS)) {
      return 60;
    }
    return 0;
  }
  
  @Override
  public List<String> getPKSourcesList() {
    ArrayList<String> pkSourcesList = new ArrayList<String>();
    for (Object obj : getPKSourcesConfig()) {
      if (!pkSourcesList.contains(obj.toString()))
        pkSourcesList.add(obj.toString());
    }
    return pkSourcesList;
  }

  @Override
  public JSObject getExtConfig(String name) {
     loadColumnDisplayConfig();
     return column_display_config;
  }

  @Override
  public void setExtConfig(String name, String value) {
    loadColumnDisplayConfig();
  }

  public abstract void setExtConfig(String name, JSObject value);
}
