package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import za.co.synthesis.rule.support.Util;
/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:16 PM
 * A tXstream Finsurv direct DB implementation of the Validate_ValidCCN function.
 * Needs to raise the following SARB error: 322, "Not a registered customs client number"
 *
 * Register: registerCustomValidate("Validate_ValidCCN",
 *   new Validate_ValidCCN(...), "transaction::ValueDate");
 */
public class Validate_ValidCCN implements ICustomValidate {
  private JdbcTemplate jdbcTemplate;
  private DateTimeFormatter formatter;

  public Validate_ValidCCN(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ccn = value.toString();
    Timestamp valueDate = null;

    boolean isValidCCN = Util.isValidCCN(ccn);

    if (otherInputs.length > 0 && otherInputs[0] != null) {
      try {
        LocalDate dt = LocalDate.parse(otherInputs[0].toString(), formatter);
        valueDate = Timestamp.valueOf(dt.atStartOfDay());
      }
      catch (IllegalArgumentException e) {
        valueDate = null;
      }
    }

    Integer resultCount;
    List<Map<String,Object>> mapList;

    StringBuilder sb = new StringBuilder();
    sb.append("SELECT COUNT(*), LetterOfUndertaking, AuthBy FROM OR_CustomsClientNumber WITH (NOLOCK)");
    sb.append(" WHERE CCN = ?");
    if (valueDate != null) {
      sb.append(" AND (ValidFrom IS NULL OR ValidFrom <= ?)");
      sb.append(" AND (ValidTo IS NULL OR ValidTo >= ?)");
      sb.append(" GROUP BY LetterOfUndertaking, AuthBy");
      mapList = jdbcTemplate.queryForList(sb.toString(), ccn, valueDate, valueDate);
    }
    else {
      sb.append(" GROUP BY LetterOfUndertaking, AuthBy");
      mapList = jdbcTemplate.queryForList(sb.toString(), ccn);
    }

    if ( mapList  != null) {
      if (mapList.size() > 0) {
        Object objAuthBy = mapList.get(0).get("AuthBy");
        if (objAuthBy != null) {
          Object objLUClient = mapList.get(0).get("LetterOfUndertaking");
          if (objLUClient != null && objLUClient.equals("Y"))
            return new CustomValidateResult(StatusType.Pass, "200", "Is LU Client");

          return new CustomValidateResult(StatusType.Pass);
        }
        else {
          //LIBRA-1280 - fail only if CCN is invalid AND it is not registered.
          //LIBRA-1346 - fail if not in table... which overrides some of LIBRA-1280
          if(isValidCCN){
            return new CustomValidateResult(StatusType.Fail, "322", "CCN is valid, but is not authorised as a registered customs client number");
          }else{
            return new CustomValidateResult(StatusType.Fail, "322", "Not authorised as a registered customs client number");
          }
        }
      }
      else{
        if(isValidCCN) {
          return new CustomValidateResult(StatusType.Fail, "322", "CCN is valid, but is not a registered customs client number");
        }else{
          return new CustomValidateResult(StatusType.Fail, "322", "Not a registered customs client number");
        }
      }
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
