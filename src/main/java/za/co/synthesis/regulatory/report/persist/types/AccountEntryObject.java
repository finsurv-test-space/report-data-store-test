package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.AccountEntryData;
import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

import static za.co.synthesis.regulatory.report.businesslogic.DataLogic.parseIntDef;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

@Entity
public class AccountEntryObject  extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(AccountEntryObject.class);
  private String reportSpace;
  private String reportKey;
  private FileData content;
  private Map<String, Object> eventContext;
  private Integer version;
  private String fileName;
  private String fileType;


  public AccountEntryObject(String uuid) {
    super(uuid);
  }
  public AccountEntryObject(String uuid, AccountEntryData accountEntryData) {
    super(uuid);
    if (accountEntryData != null){
      this.reportSpace = accountEntryData.getReportSpace();
      this.reportKey = accountEntryData.getTrnReference();
      this.content = accountEntryData.getContent();
      this.version = accountEntryData.getVersion();
      /*
      if (accountEntryData.getEventContext() == null) {
        try {
          //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
          ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
          if (user != null) {
            this.eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
          }
        } catch (Exception e) {
        }
      } else {
        this.eventContext = SerializationTools.serializeEventContext(accountEntryData.getEventContext());
      }
       */
    }
  }
  public Integer getVersion() {
    return version;
  }
  public AccountEntryData getAccountEntryData() {

//    this.channelName, //TODO: Check if channel is relevant?
    //TODO: add params
    AccountEntryData accountEntryData = new AccountEntryData(
            this.reportSpace,
            this.reportKey,
            this.content,
            this.eventContext != null ? SerializationTools.deserializeEventContext(eventContext) : null,
            this.version == null ? 1 : this.version
    );
    return accountEntryData;
  }

  public void setVersion(Integer version) {
    this.version = version == null ? 1 : version;
  }

  public String getContent(){
    JSObject jsonMap = new JSObject();

    jsonMap.put("User", SecurityHelper.getAuthMeta().getName());

    jsonMap.put("FileName", fileName instanceof String ? fileName : (content != null ? content.getFilename() : ""));
    jsonMap.put("ContentType", fileType instanceof String ? fileType : (content != null ? content.getContentType() : ""));
    if (!jsonMap.isEmpty()) {
      return mapToJsonStr(jsonMap);
    }
    return null;
  }

  public void setContent(String contentJson) {
    if (contentJson != null && !contentJson.isEmpty()) {
      try {
        Map jsonMap = jsonStrToMap(contentJson);

        Object obj = jsonMap.get("FileName");
        this.fileName = obj != null ? obj.toString() : (content != null ? content.getFilename() : null);
        obj = jsonMap.get("ContentType");
        this.fileType = obj != null ? obj.toString() : (content != null ? content.getContentType() : null);


      } catch (Exception e) {
        logger.error("Unable to deserialize json content for Document object: " + e.getMessage(), e);
      }
    }
  }

  public String getDataContent() {
    return new String(DataLogic.Base64Encode(this.content.getContent()));
  }

  public void setDataContent(String dataContent) {
    this.content = new FileData(
            this.fileType instanceof String ? this.fileType : (content != null ? content.getContentType() : ""),
            this.fileName instanceof String ? this.fileName : (content != null ? content.getFilename() : ""),
            DataLogic.Base64Decode(dataContent.getBytes())
          );
  }

  public void setEventContext(Map<String, Object> eventContext) {
    this.eventContext = eventContext;
  }

  public String getReportSpace(){
    return reportSpace;
  }

  public void setReportSpace(String reportSpace){
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }


  public String getAuthToken() {

    if (this.eventContext == null) {
      try {
        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases, seems to be a spring thing.
        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
        if (user != null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      } catch (Exception e) {
        //we seem to be running into null pointers where we really shouldn't be.
        logger.warn("Unable to persist the ReportObject eventContext / Audit detail : " + e.getMessage());
        e.printStackTrace();
      }
    }

    return mapToJsonStr(eventContext);
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }


  public ReportEventContext getEventContext() {
    return eventContext != null ? SerializationTools.deserializeEventContext(eventContext) : null;
  }

/*
  public String getSystemValue(String name) {
    if (FieldConstant.State.equals(name))
      return state;
    if (FieldConstant.ReportSpace.equals(name))
      return reportSpace;
//    if (FieldConstant.Schema.equals(name))
//      return schema;
    if (FieldConstant.Channel.equals(name))
      return channelName;
    if (FieldConstant.GUID.equals(name))
      return getGUID();
    return null;
  }
*/

}