package za.co.synthesis.regulatory.report.businesslogic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.IPasswordStrength;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 7/26/17.
 */
public class UserInformation {
  private static final Logger logger = LoggerFactory.getLogger(UserInformation.class);
  private IConfigStore configStore;
  private SystemInformation systemInformation;

  public IPasswordStrength passwordStrength;

  public IPasswordStrength getPasswordStrength() {
    return passwordStrength;
  }

  @Required
  public void setPasswordStrength(IPasswordStrength IPasswordStrength) {
    this.passwordStrength = IPasswordStrength;
  }

  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
    systemInformation.setUserInformation(this);
  }

  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }


  public UserData getUser(String username) {
//    return configStore.getUserMap().get(username);
    return configStore.getUser(username);
  }

  public Collection<UserData> getUserList() {
    return configStore.getUserMap().values();
  }

  public List<AccessSet> getAccessSets() {
    List<AccessSet> result = new ArrayList<AccessSet>();
      for (AccessSetData accessSetData : configStore.getAccessSetMap().values()) {
        result.add(new AccessSet(accessSetData));
      }
    return result;
  }

  public List<AccessSet> getAccessSets(String reportSpace) {
    List<AccessSet> result = new ArrayList<AccessSet>();

    String[] reportSpaces = reportSpace.split("\\s*[,;]\\s*");

    for (String report : reportSpaces) {
      if (report instanceof String && reportSpaces.length > 0) {
        for (AccessSetData accessSetData : configStore.getAccessSetMap().values()) {
          if((accessSetData.getReportSpace()).equals(report)) {
            result.add(new AccessSet(accessSetData));
          }
        }
      }
    }


    return result;
  }

  public AccessSet getAccessSet(String name) {
      return new AccessSet(configStore.getAccessSetMap().get(name));
  }

  public void setAccessSet(AccessSet accessSet) {
    configStore.setAccessSet(accessSet.getAccessSetData());
  }

  public UserAccess getUserAccess(String username) {
    return new UserAccess(getUserAccessData(username));
  }

  public UserAccessData getUserAccessData(String username) {
    UserAccessData userAccessData = new UserAccessData();

    Map<String, AccessSetData> accessSetDataMap = configStore.getAccessSetMap();

    UserData userData = getUser(username);
    if (userData != null && userData.getAccess() != null) {
      for (String accessSetName : userData.getAccess()) {
        AccessSetData accessSet = accessSetDataMap.get(accessSetName);
        if (accessSet != null)
          userAccessData.getAccessList().add(accessSet);
      }
    }
    return userAccessData;
  }

  public UserAccessData getLoggedOnUserAccessData() {
    UserAccessData userAccessData = new UserAccessData();

    Map<String, AccessSetData> accessSetDataMap = configStore.getAccessSetMap();

    ReportServicesAuthentication authMeta = SecurityHelper.getAuthMeta();
    List<String> accessSets = authMeta.getAccessSets();
    if (accessSets != null) {
      for (String accessSetName : accessSets) {
        AccessSetData accessSet = accessSetDataMap.get(accessSetName);
        if (accessSet != null)
          userAccessData.getAccessList().add(accessSet);
      }
    }
    return userAccessData;
  }

  public List<String> getUserReportSpaces(String username) {
    List<String> result = new ArrayList<String>();
    UserAccessData userAccessData = getUserAccessData(username);
    if (userAccessData.getAccessList() != null) {
      for (AccessSetData accessSetData : userAccessData.getAccessList()) {
        if (!result.contains(accessSetData.getReportSpace()))
          result.add(accessSetData.getReportSpace());
      }
    }
    return result;
  }

  public List<Channel> getPrimaryUserChannels(String username) {
    List<Channel> result = new ArrayList<Channel>();
    List<String> reportSpaceNames = getUserReportSpaces(username);
    UserData userData = getUser(username);

    for (String reportSpaceName : reportSpaceNames) {
      ReportSpace reportSpace = systemInformation.getReportSpace(reportSpaceName);
      boolean userHasChannel = false;
      for (String userChannel : userData.getChannels()) {
        if (reportSpace.getChannels().contains(userChannel)) {
          userHasChannel = true;
          result.add(systemInformation.getChannel(userChannel));
          break;
        }
      }
      if (!userHasChannel) {
        result.add(systemInformation.getChannel(reportSpace.getDefaultChannel()));
      }
    }
    return result;
  }

  public List<Channel> getUserChannels(String username) {
    List<Channel> result = new ArrayList<Channel>();
    List<String> reportSpaceNames = getUserReportSpaces(username);
    UserData userData = getUser(username);

    for (String reportSpaceName : reportSpaceNames) {
      try {
        ReportSpace reportSpace = systemInformation.getReportSpace(reportSpaceName);
        if (reportSpace != null) {
          boolean userHasChannel = false;
          for (String userChannel : userData.getChannels()) {
            if (reportSpace.getChannels().contains(userChannel)) {
              userHasChannel = true;
              result.add(systemInformation.getChannel(userChannel));
            }
          }
          if (!userHasChannel) {
            result.add(systemInformation.getChannel(reportSpace.getDefaultChannel()));
          }
        } else {
          logger.warn("Report space configuration does not exist for: " + reportSpaceName + ", but is stipulated in configuration for user: " + username);
        }
      } catch(Exception err){
        logger.warn("Unable to resolve System Configuration details for Report Space: "+ reportSpaceName +", as stipulated for user: "+ username);
      }
    }
    return result;
  }

  public List<Channel> getUserChannels(String username, List<String> reportSpaces) {
    List<Channel> result = new ArrayList<Channel>();
    List<String> reportSpaceNames = reportSpaces;
    UserData userData = getUser(username);

    for (String reportSpaceName : reportSpaceNames) {
      ReportSpace reportSpace = systemInformation.getReportSpace(reportSpaceName);
      for (String userChannel : userData.getChannels()) {
        if (reportSpace.getChannels().contains(userChannel)) {
          result.add(systemInformation.getChannel(userChannel));
        }
      }
    }
    return result;
  }

  public UserInfo getUserInfo(Authentication authentication) {
    UserInfo userInfo = null;
    String currentUserName = null;
    currentUserName = authentication.getName();
    userInfo = getUserInfo(currentUserName);

//    ArrayList<String> defaultLists = new ArrayList<String>();
//    for (Map.Entry<String, ListData> entry: systemInformation.getAllListDetails().entrySet()){
//      defaultLists.add(entry.getKey());
//    }

    if (userInfo != null) {
      userInfo.getAuth().setRights(SecurityHelper.convertGrantedAuthoritiesToRights(authentication.getAuthorities()));
      if (authentication instanceof ReportServicesAuthentication) {
        userInfo.getAuth().setAccess(((ReportServicesAuthentication) authentication).getAccessSets());
      }
    } else {
      userInfo = new UserInfo();
      if (authentication instanceof ReportServicesAuthentication) {
        ReportServicesAuthentication rsa = (ReportServicesAuthentication)authentication;
        if (rsa.getUserTemplateName() != null && !rsa.getUserTemplateName().isEmpty()) {
          userInfo = getUserInfo(rsa.getUserTemplateName());
          userInfo = userInfo==null?new UserInfo():userInfo;
        }
        UserInfo.Auth auth = userInfo.getAuth();
        if (auth == null){
          List<String> rights = new ArrayList<String>();
          for (GrantedAuthority ga : rsa.getAuthorities()){
            rights.add(ga.getAuthority());
          }

          //TODO: we need to ensure that the "defaultLists" instance is only used where the user has no assigned list set.
          UserData user = new UserData(rsa.getPrincipal().toString(), "",  rights, rsa.getAccessSets(), new ArrayList<String>(), true, null,systemInformation.getListNames(),systemInformation.getUserEventNames());
          UserInfo.Auth userInfoAuth = new UserInfo.Auth(user);
          userInfo.setAuth(userInfoAuth);
        }
        userInfo.getAuth().setAccess(rsa.getAccessSets());
//        Map<String, RequestParamData> meta = rsa.getAuthenticationMeta();
//        userInfo.
      }
    }
    return userInfo;
  }

  public UserInfo getUserInfo(String username) {
    ArrayList<String> defaultLists = new ArrayList<String>();
    for (Map.Entry<String, ListData> entry: systemInformation.getAllListDetails().entrySet()){
      defaultLists.add(entry.getKey());
    }

    UserData userData = getUser(username);
    if (userData != null) {
      //TODO: the below defaulting of null Lists and Events should ONLY be removed once a valid update process has been created to set these for each user already in the database
      return new UserInfo(userData, getUserChannels(username),
              (userData.getReportLists() != null && userData.getReportLists().size() > 0 ? userData.getReportLists() : systemInformation.getListNames()),
              (userData.getUserEvents() != null && userData.getUserEvents().size() > 0 ? userData.getUserEvents() : systemInformation.getUserEventNames()),
              userData.getDetails());
    }
    return null;
  }


  /**
   *
   * @param userInfo
   * @return
   * @throws Exception
   */
  public UserInfo setUserInfo(UserInfo userInfo) throws Exception {
    if (!(userInfo instanceof UserInfo)){
      throw new Exception("User information must be an instance of UserInfo.");
    }
    if (!(userInfo.getAuth() instanceof UserInfo.Auth) || !(userInfo.getAuth().getUsername() instanceof String) || userInfo.getAuth().getUsername().trim().isEmpty()){
      throw new Exception("User information must contain an instance of UserInfo.Auth, with a valid username.");
    }

    //TODO: Create global bean reference for a PasswordStrength checker. and use it to check the password for the given user. If fail - throw exception.
    boolean passwordIsStrongEnough = false;
    String username = userInfo.getAuth().getUsername();
    //String password = generateUUID().replaceAll("[^\\dA-Za-z]","").substring(0,minimumPasswordLengthRequired);  //Generate a random user password - to be used if no previous version of the user exists - and return this as part of the user info stored.
    String password = "";
    if (userInfo.getAuth().getPassword() instanceof String &&
            !userInfo.getAuth().getPassword().trim().isEmpty() &&
            !userInfo.getAuth().getPassword().trim().matches("^(\\$[\\da-zA-Z]+){2}.+")) {
      passwordIsStrongEnough = this.passwordStrength.getPasswordStrength(userInfo.getAuth().getPassword());
      if (!passwordIsStrongEnough) {
        throw new Exception(this.passwordStrength.getPasswordStrengthRequirements());
      } else {
        password = SecurityHelper.getPasswordHash(userInfo.getAuth().getPassword().trim());
      }
    }

    UserData userData = null;
    UserData currentUserData = configStore.getUser(username);
    Boolean active = true;
    Map<String, Object> details = null;

    if (currentUserData instanceof UserData) {
      List<String> rights = userInfo.getAuth().getRights();
      List<String> access = userInfo.getAuth().getAccess();  //TODO: do we store this? and how does this differ from the rights?
      List<String> channels = new ArrayList<String>();
      List<String> reportLists = userInfo.getReportLists();
      List<String> userEvents = userInfo.getUserEvents();
//      active = currentUserData.getActive();
      active = userInfo.getAuth().getActive();
      details = userInfo.getDetails();
      for (Channel channel : userInfo.getChannels()) {
        channels.add(channel.getChannelName());
      }
      userData = currentUserData;  //we are not updating the username - so we inherit it from the previous version of the user data.
      if (password instanceof String && !password.trim().isEmpty()) {
        userData.setPassword(password);
      }
      userData.setRights(rights);
      userData.setAccess(access);
      userData.setChannels(channels);
      userData.setActive(active);
      userData.setDetails(details);
      userData.setReportLists(reportLists);
      userData.setUserEvents(userEvents);
    } else {
      //if the password is provided for a new user, then use it instead of the generated one..
      if(!passwordIsStrongEnough)
      {
        throw new Exception(this.passwordStrength.getPasswordStrengthRequirements());
      }

      List<String> rights = userInfo.getAuth().getRights();
      List<String> access = userInfo.getAuth().getAccess();  //TODO: do we store this? and how does this differ from the rights?
      List<String> channels = new ArrayList<String>();
      List<String> reportLists = userInfo.getReportLists();
      List<String> userEvents = userInfo.getUserEvents();
      for (Channel channel : userInfo.getChannels()) {
        channels.add(channel.getChannelName());
      }
      userData = new UserData(
              username,
              password, //if not already hashed, then hash the password when setting!
              rights,
              access,
              channels,
              active,
              details,
              reportLists,
              userEvents
              );
    }
    configStore.setUser(userData);
    //if generated, we should return the password as part of the payload - in plain text... otherwise, simply return whatever the password field is currently set to (hashed or specified etc).
    userData.setPassword(password);
    //TODO: The specific access to select report Lists and UserEvents are not seemingly stored anywhere in the system.  we may need to address this in the near future.
    return new UserInfo(userData, userInfo.getChannels(), userInfo.getReportLists(), userInfo.getUserEvents(), details);
  }
}
