package za.co.synthesis.regulatory.report.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import za.co.synthesis.regulatory.report.support.HttpResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by james on 2017/06/23.
 */
public class ReportServiceAccessDeniedHandler implements AccessDeniedHandler {
    private static final Logger logger = LoggerFactory.getLogger(ReportServiceAccessDeniedHandler.class);

    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException,
            ServletException {

        //*************************************************************************************************
        //***  401 Unauthorized Similar to 403 Forbidden, but specifically for use when authentication  ***
        //***  is required and has failed or has not yet been provided.                                 ***
        //*************************************************************************************************
        //***  SC_TEMPORARY_REDIRECT    => 307
        //***  SC_UNAUTHORIZED          => 401
        //***  SC_FORBIDDEN             => 403
        //*************************************************************************************************

        if (!response.isCommitted()) {
            if (accessDeniedException.getMessage().startsWith("REDIRECT:")) {
                response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                response.setHeader("Location", accessDeniedException.getMessage().substring(9));
            } else if (accessDeniedException.getMessage().startsWith("AUTH:")) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); //Auth required (Authentication failure)
                HttpResponseUtil.writeError(response, accessDeniedException.getClass().getSimpleName(), accessDeniedException.getMessage());
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);//Forbidden resource (Authorization failed)
                HttpResponseUtil.writeError(response, accessDeniedException.getClass().getSimpleName(), accessDeniedException.getMessage());
            }
        }
    }
}