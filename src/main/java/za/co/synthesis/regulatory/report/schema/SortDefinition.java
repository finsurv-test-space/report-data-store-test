package za.co.synthesis.regulatory.report.schema;

import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.ListData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/07/03.
 */
public class SortDefinition {

    public class SortField extends ListData.Field{
        private SortOrder order = SortOrder.Ascending;

        public SortField(String label, CriterionType criterionType, String name, SortOrder order){
            super(label, criterionType, name);
            this.order = order;
        }

        public SortOrder getOrder() {
            return order;
        }

        public void setOrder(SortOrder order) {
            this.order = order;
        }

        public String toString(){
            return (getLabel()!=null? ((order.equals(SortOrder.Descending)?"-":"")+getLabel()) : "");
        }
    }

    private final List<SortField> fields = new ArrayList<SortField>();


    public SortDefinition() {
    }


    /**
     * Attempts to parse String array of values which are ordered in preference of sorting priority and have the
     * following composition: [-/+][[[CriterionType][[:][FieldName]]]/[ListDefinition.FieldLabel]/[ListDefinition.FieldName]]
     * Example Composition Patterns:
     *  [-/+][ListDefinition.FieldLabel]
     *  [-/+][ListDefinition.FieldName]
     *  [-/+][FieldName]
     *  [-/+][CriterionType][[:][FieldName]]
     *
     * Descending fields are prefixed with an optional hyphen/minus "-" operator.
     * Ascending fields are depicted with an optional plus "+" operator.
     * If the sort order operator is ommitted, the sort order for the field is assumed to be ascending.
     * I.E. (Descending order):
     *  -Report:TrnReference
     *  or -Meta:Corporation
     *  or -TrnReference
     *  or -System:ValidationState
     *  or -System
     *
     * I.E. (Ascending order - specified):
     *  +Report:TrnReference
     *  or +Meta:Corporation
     *  or +TrnReference
     *  or +System:ValidationState
     *  or +System
     *
     * I.E. (Ascending order - defaulted):
     *  Report:TrnReference
     *  or Meta:Corporation
     *  or TrnReference
     *  or System:ValidationState
     *  or System
     *
     *
     * Criterion Type is denoted by a case-insensitive CriterionType value ("Report", "Meta", "System"), and is
     * followed by a colon ":" character as a delimiter to the field name (with exception to the System criterion,
     * which has no field name requirement).
     * IE: +Report:ValueDate
     *  or Meta:SubmittingUser
     *  or -System:ReportState
     *  or System
     *
     * Specifying an ascending field
     * @param strFields
     */
    public SortDefinition(String[] strFields, ListData listDefinition) {
        if (strFields != null && strFields.length > 0){
            for (String strField : strFields){
                SortOrder order = SortOrder.Ascending;
                //parse order type
                if (strField.startsWith("-") || strField.startsWith("+")){
                    order = strField.startsWith("-")?SortOrder.Descending:order;
                    strField = strField.substring(1);
                }

                ListData.Field field = null;
                for (ListData.Field definitionField : listDefinition.getFields()){
                    if (definitionField.getLabel().equalsIgnoreCase(strField) ||
                            definitionField.getName().equalsIgnoreCase(strField)) {  //should perhaps include the criterion type in the comparison for field label .. ?
                        field = definitionField;
                        break;
                    }
                }

                //no matching fields were found for the specification... try creating one by parsing the data provided?
                if (field == null) {
                    //parse criterion type
                    CriterionType criterionType = null;
                    int criterionTypeIndex = strField.indexOf(":");
                    if (criterionTypeIndex > 0) {
                        String criterionTypeString = strField.substring(0, criterionTypeIndex);
                        if ("Meta".equalsIgnoreCase(criterionTypeString)) {
                            criterionType = CriterionType.Meta;
                        } else if ("System".equalsIgnoreCase(criterionTypeString)) {
                            criterionType = CriterionType.System;
                        } else if ("Report".equalsIgnoreCase(criterionTypeString)) {
                            criterionType = CriterionType.Report;
                        }
                        if (criterionType != null) {
                            strField = strField.substring(criterionTypeIndex + 1);
                        } else {
                            criterionType = CriterionType.Report;
                        }
                    } else if (strField.equalsIgnoreCase("System")) {
                        criterionType = CriterionType.System;
                    }
                    field = new ListData.Field(strField, criterionType!=null?criterionType:CriterionType.Report, strField);
                }

                fields.add(new SortField(field.getLabel(), field.getCriterionType(), field.getName(), order));
            }
        }
    }

    public SortDefinition(List<SortField> fields) {
        this.fields.addAll(fields);
    }

    public List<SortField> getFields() {
        return fields;
    }

    public void addField(SortField field) {
        if (field != null) {
            this.fields.add(field);
        }
    }

    public String toString(){
        String representation = "";
        for (SortField field : fields){
            representation += (representation.length()>0?", ":"") + field.toString();
        }
        return representation;
    }
}
