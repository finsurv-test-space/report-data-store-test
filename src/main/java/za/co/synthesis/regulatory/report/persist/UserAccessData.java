package za.co.synthesis.regulatory.report.persist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/27/17.
 */
public class UserAccessData {
  private final List<AccessSetData> accessList = new ArrayList<AccessSetData>();

  public List<AccessSetData> getAccessList() {
    return accessList;
  }
}
