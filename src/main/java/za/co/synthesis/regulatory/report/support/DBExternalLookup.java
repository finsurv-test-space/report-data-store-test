package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.dao.IExternalLookup;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.LookupData;
import za.co.synthesis.regulatory.report.validate.ValidationUtils;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.StatusType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 8/2/17.
 */
public class DBExternalLookup implements IExternalLookup {
  static final Logger log = LoggerFactory.getLogger(DBExternalLookup.class);

  private final LookupData lookupData;
  private final JdbcConnectionManager jdbcConnectionManager;

  public DBExternalLookup(JdbcConnectionManager jdbcConnectionManager, LookupData lookupData) {
    this.jdbcConnectionManager = jdbcConnectionManager;
    this.lookupData = lookupData;
  }

  public static JSArray parseComposedResponse(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      Object valRes = parser.parse();

      if (valRes instanceof JSArray) {
        return (JSArray)valRes;
      }
    }
    return null;
  }

  @Override
  public JSArray getFullLookupList(String listName) {
    JdbcTemplate jdbcTemplate = jdbcConnectionManager.getJdbcTemplate(lookupData.getDataSource());

    Map<String, Object> parameterValue = new HashMap<String, Object>();

    List<Map<String, Object>> response = jdbcTemplate.queryForList(lookupData.getSql());

    if (lookupData.getCompose() == null) {
      JSArray result = new JSArray();

      if (response != null) {
        for (Map<String, Object> row : response) {
          if (row.size() > 1) {
            JSObject jsObj = new JSObject();
            result.add(jsObj);
            for (Map.Entry<String, Object> entry : row.entrySet()) {
              if (entry.getValue() != null) {
                jsObj.put(entry.getKey(), entry.getValue().toString());
              }
            }
          }
          else {
            for (Map.Entry<String, Object> entry : row.entrySet()) {
              if (entry.getValue() != null) {
                result.add(entry.getValue().toString());
              }
            }
          }
        }
      }
      return result;
    }
    else {
      String responseName = "response";
      if (response != null) {
        parameterValue.put(responseName, response);
      } else {
        parameterValue.put(responseName, null);
      }

      String composedResult;
      try {
        composedResult = ValidationUtils.compose(lookupData.getName() + " " + responseName,
                lookupData.getCompose(),
                parameterValue,
                null,
                log);
      } catch (Exception e) {
        log.error("Error composing response for lookup " + lookupData.getName(), e);
        return new JSArray();
      }
      try {
        return parseComposedResponse(composedResult);
      } catch (Exception e) {
        log.error("Composed Response: " + composedResult + "\nError parsing composing response for lookup " + lookupData.getName(), e);
        return new JSArray();
      }
    }
  }
}
