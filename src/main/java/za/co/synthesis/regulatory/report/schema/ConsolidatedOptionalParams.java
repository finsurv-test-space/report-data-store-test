package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.AccountHolderStatus;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "AccountHolderStatus",
    "DomesticAmount",
    "Optional"
})
public class ConsolidatedOptionalParams {
  private AccountHolderStatus accountHolderStatus;
  private String domesticAmount;
  private Map optionalParamMap;

  public ConsolidatedOptionalParams(AccountHolderStatus accountHolderStatus, String domesticAmount, Map optionalParamMap) {
    this.accountHolderStatus = accountHolderStatus;
    this.domesticAmount = domesticAmount;
  }

  @JsonProperty("AccountHolderStatus")
  public AccountHolderStatus getAccountHolderStatus() {
    return accountHolderStatus;
  }

  public void setAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
    this.accountHolderStatus = accountHolderStatus;
  }

  @JsonProperty("DomesticAmount")
  public String getDomesticAmount() {
    return domesticAmount;
  }

  public void setDomesticAmount(String domesticAmount) {
    this.domesticAmount = domesticAmount;
  }

  public void setDomesticAmount(Double domesticAmount) {
    this.domesticAmount = domesticAmount.toString();
  }

  @JsonProperty("Optional")
  public Map<String,String> getOptionalParamMap() {
    return optionalParamMap;
  }

  public void setOptionalParamMap(Map<String,String> optionalParamMap) {
    this.optionalParamMap = optionalParamMap;
  }


  public Map<String, Object> getMap() {
    Map<String, Object> res = new HashMap<String, Object>();
    if (getAccountHolderStatus() != null) {
      res.put("AccountHolderStatus", getAccountHolderStatus().name());
    }
    if (getDomesticAmount() != null) {
      res.put("DomesticAmount", getDomesticAmount());
    }
    if (getOptionalParamMap() != null) {
      res.putAll(getOptionalParamMap());
    }
    return res;
  }
}
