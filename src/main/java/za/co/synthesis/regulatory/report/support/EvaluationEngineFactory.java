package za.co.synthesis.regulatory.report.support;

import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.rule.core.EvaluationEngine;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 3/31/16.
 */
public class EvaluationEngineFactory {
  private static class EngineEntry {
    private final EvaluationEngine engine;
    private PackageCache.Entry evalEntry = null;
    public EngineEntry(EvaluationEngine engine) {
      this.engine = engine;
    }

    public EngineEntry(EvaluationEngine engine, PackageCache.Entry evalEntry) {
      this.engine = engine;
      this.evalEntry = evalEntry;
    }

    public EvaluationEngine getEngine() {
      return engine;
    }

    public PackageCache.Entry getEvalEntry() {
      return evalEntry;
    }

    public void setEvalEntry(PackageCache.Entry evalEntry) {
      this.evalEntry = evalEntry;
    }

  }

  private RulesCache rulesCache;
  private boolean raiseExceptionOnDuplicateRules = false;
  private final Map<String, EngineEntry> engineMap = new HashMap<String, EngineEntry>();

  public EvaluationEngineFactory() {
  }

  @Required
  public void setRulesCache(RulesCache rulesCache) {
    this.rulesCache = rulesCache;
  }

  private EvaluationEngine getEvaluationEngine(String packageName) throws Exception {
    EngineEntry engineEntry = engineMap.get(packageName);
    EvaluationEngine engine = engineEntry != null ? engineEntry.getEngine() : null;
    PackageCache.Entry evalutionEntry = engineEntry != null ? engineEntry.getEvalEntry() : null;

    if (evalutionEntry != null) {
      if (rulesCache.isEntryOld(packageName, evalutionEntry)) {
        evalutionEntry = null;
      }
    }

    if (evalutionEntry == null) {
      evalutionEntry = rulesCache.getEvaluationRules(packageName, PackageCache.CacheStrategy.CacheOrSource);

      if (evalutionEntry != null) {
        engine = new EvaluationEngine();
        engine.setRaiseExceptionOnDuplicateRules(raiseExceptionOnDuplicateRules);

        engine.setupLoadRule(evalutionEntry.getName(), new StringReader(evalutionEntry.getData()));
        engine.setupLoadAssumptions(evalutionEntry.getName(), new StringReader(evalutionEntry.getData()));
        engine.setupLoadContext(evalutionEntry.getName(), new StringReader(evalutionEntry.getData()));

        if (engineMap.containsKey(packageName)) {
          engineMap.remove(packageName);
        }
        engineMap.put(packageName, new EngineEntry(engine, evalutionEntry));
      }
    }
    return engine;
  }

  public synchronized EvaluationEngine getEngine(String packageName) throws Exception {
   return getEvaluationEngine(packageName);
  }

  public boolean isRaiseExceptionOnDuplicateRules() {
    return raiseExceptionOnDuplicateRules;
  }

  public void setRaiseExceptionOnDuplicateRules(boolean raiseExceptionOnDuplicateRules) {
    this.raiseExceptionOnDuplicateRules = raiseExceptionOnDuplicateRules;
  }
}
