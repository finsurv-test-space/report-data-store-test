package za.co.synthesis.regulatory.report.persist.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.schema.ValidationResponse;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.core.Scope;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by james on 2017/09/04.
 */
public class SerializationTools {
  public static final Logger logger = LoggerFactory.getLogger(SerializationTools.class);

  public static String localDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

  @Autowired
  UserInformation userInformation;


  public static List<Map<String, Object>> serializeValidations(List<ValidationResponse> validationResponses){
    if (validationResponses != null) {
      List<Map<String, Object>> validations = new ArrayList<>();
      for (ValidationResponse val : validationResponses) {
        Map<String, Object> jsonMap = new HashMap();
        jsonMap.put("Type", val.getType());
        jsonMap.put("Name", val.getName());
        jsonMap.put("Code", val.getCode());
        jsonMap.put("Message", val.getMessage());
        jsonMap.put("Scope", val.getScope());
        jsonMap.put("MoneyInstance", val.getMoneyInstance());
        jsonMap.put("ImportExportInstance", val.getImportExportInstance());
        validations.add(jsonMap);
      }
      return validations;
    }
    return null;
  }


  public static List<ValidationResponse> deserializeValidations(List<Map<String, Object>> jsonValidations) {
    List<ValidationResponse> validationResults = new ArrayList<>();
    if (jsonValidations != null) {
      for (Map<String, Object> jsonMap : jsonValidations) {
        Object getObj = jsonMap.get("Type");
        ResultType type = ResultType.valueOf(getObj!=null?getObj.toString():ResultType.Warning.name());
        getObj = jsonMap.get("Type");
        String name = getObj!=null?getObj.toString():null;
        getObj = jsonMap.get("Code");
        String code = getObj!=null?getObj.toString():null;
        getObj = jsonMap.get("Message");
        String message = getObj!=null?getObj.toString():null;
        getObj = jsonMap.get("Scope");
        Scope scope = Scope.Transaction;
        try{
          scope = getObj!=null?Scope.valueOf(getObj.toString()):Scope.Transaction;
        } catch(Exception e){}
        getObj = jsonMap.get("MoneyInstance");
        Integer moneyInstance = (getObj!=null && getObj instanceof Integer?
                (Integer)getObj:
                (getObj!=null && getObj instanceof String?
                        DataLogic.parseIntDef((String)getObj,0):
                        new Integer(0)
                )
          );
        getObj = jsonMap.get("ImportExportInstance");
        Integer importExportInstance = (getObj!=null && getObj instanceof Integer?
                (Integer)getObj:
                (getObj!=null && getObj instanceof String?
                        DataLogic.parseIntDef((String)getObj,0):
                        new Integer(0)
                )
        );
        ResultEntry res = new ResultEntry(type,
                name,
                code,
                message,
                scope,
                moneyInstance!=null?moneyInstance:0,
                importExportInstance!=null?importExportInstance:0);
        validationResults.add(new ValidationResponse(res));
      }
    }
    return validationResults;
  }

  public static Map<String, Object> serializeEventContext(ReportEventContext eventContext){
      HashMap<String, Object> values = new HashMap<>();
      if (eventContext != null) {
        values.put("Username", eventContext.getUsername());
        values.put("DateTime", serializeLocalDateTime(eventContext.getDateTime()));


        values.putAll(eventContext.getOther());

        Map<String, String> auditFields = eventContext.getAuditFields();
        if (auditFields != null && auditFields.size() > 0) {
          List<String> removeKeys = new ArrayList<String>();
          for (Map.Entry<String,String> entry : auditFields.entrySet()){
            if (entry.getValue() == null || entry.getValue().trim().isEmpty()){
              removeKeys.add(entry.getKey());
            }
          }
          for (String key : removeKeys) {
            auditFields.remove(key);
          }
          if (auditFields.size() > 0)
            values.put("Audit", auditFields);
        }
      }
    return values;
  }


  public static ReportEventContext deserializeEventContext(Map<String, Object> jsonEventContext) {
    String username = (String)jsonEventContext.get("Username");
    String dateTime = (String)jsonEventContext.get("DateTime");
    ReportEventContext context = new ReportEventContext(username, deserializeLocalDateTime(dateTime));
    Map<String, String> audit = context.getAuditFields();
    Object auditMap = jsonEventContext.get("Audit");
    if (auditMap != null && auditMap instanceof Map && ((Map) auditMap).size()>0){
      audit.putAll((Map<String, String>) auditMap);
    }
    return context;
  }

  public static String serializeLocalDateTime(LocalDateTime dateTime){
    return serializeLocalDateTime(dateTime, null);
  }

  public static String serializeLocalDateTime(LocalDateTime dateTime, String pattern) {
    if (dateTime == null) {
      dateTime = LocalDateTime.now();
      //SerializationTools.logger.warn("No dateTime instance supplied - defaulting instance with LocalDateTime.now()");
    }
    if (pattern == null || pattern.isEmpty()) {
      pattern = localDateTimeFormat;
    }
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    return dateTime.format(formatter);
  }

  public static LocalDateTime deserializeLocalDateTime(String dateTimeStr){
    return deserializeLocalDateTime(dateTimeStr, null);
  }

  public static LocalDateTime deserializeLocalDateTime(String dateTimeStr, String pattern){
    if (dateTimeStr != null && !dateTimeStr.isEmpty()) {
      if (pattern == null || pattern.isEmpty()) {
        pattern = localDateTimeFormat;
      }
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
      return LocalDateTime.parse(dateTimeStr, formatter);
    }
    //SerializationTools.logger.warn("No dateTimeStr value supplied - providing default value of LocalDateTime.now()");
    return LocalDateTime.now();
  }

  public static Map<String, String> parseSearchString(String search) {
    String tokenDelim = "\\s*;\\s*";
    String keyValueDelim = "\\s*:\\s*";
    return parseSearchString(search, tokenDelim, keyValueDelim);
  }


  /**
   * This function will create tokens to be used to generate sql queries that perform fulltext searches using the FULLTEXTTABLE and CONTAINS statements
   *
   * FOR MORE INFORMATION:
   *   https://docs.microsoft.com/en-us/sql/t-sql/queries/contains-transact-sql
   */
  public static Map<String, String> parseSearchString(String search, String tokenDelimRegex, String keyValueDelimRegex) {
    Map<String, String> tokenMap = new HashMap<String, String>();
    String fullTextSearchString = "";

    logger.trace("PARSING SEARCH STRING: "+search);
    //Regex to extract quoted, wild-carded or asserted ("+" prefixed) strings for MSSQL ContainsText search criteria...
    String regex = "(?:" +
            //--  GROUP 1:  --//
              "\\+?(\"[^\"]+\")" + //ANYTHING IN QUOTES (POSSIBLY PREFIXED WITH A "+" PLUS) <plus not captured, but quotes are>
            "|" + //...or...

            //--  GROUP 2:  --//
              "(?=" +

                "(?:" +
                  "\\+" +//Something prefixed with a plus(+) <plus not captured>
                "|" + // ...OR...
                  "(?:[\\w\\-\\@\\.\\+\\_\\#\\(\\)]+[\\*\\%])+)" +
                ")" + //WHICH CONTAINS SOME EXPECTED CHARACTERS AS WELL AS SOME WILDCARD CHARACTERS: Asterisk or Percentage ("*"/"%")

                "([\\w\\-\\@\\.\\+\\%\\*\\_\\#\\(\\)]+)" + //CONSISTING OF ALPHANUMERICS AND A FEW OTHER DESIGNATED CHARACTERS
                "(?:\\b|\\s|$)" + //TERMINATED BY A WORD BOUNDARY, WHITE SPACE OR EOL
              ")";
    Pattern pattern = Pattern.compile(regex);
    int tokenCount = 0;
    if (search != null && !search.isEmpty()) {
      Matcher matcher = pattern.matcher(search);
      while (matcher.find()) {
        String matchString = matcher.group(0);
        if (matchString.indexOf("\"") >= 0){
          matchString = matcher.group(1);
        } else {
          matchString = matcher.group(2);
          //-- CHECK FOR WILDCARD SEARCH OPPORTUNITIES --//
          //If the UN-QUOTED matchstring contains "%" symbols, then we need to convert the string to a partial or wild-card match instead...
          if (matchString != null && !matchString.isEmpty() && (matchString.indexOf("*") >= 0 || matchString.indexOf("%") >= 0)){
            matchString = matchString.replaceAll("%", "*");
            if (!matchString.endsWith("*")) {
              matchString += "*";
            }
            matchString = "\"" + matchString + "\"";
          }
        }

        if (matchString != null) {
          //strip off prefixed plus (+) symbols...
          if (matchString.startsWith("+")){
            matchString = matchString.substring(1);
          }
          logger.trace("CONTAINS_TEXT_TOKEN: "+matchString);
          tokenMap.put("CONTAINS_TEXT_SEARCH",
                  (tokenMap.containsKey("CONTAINS_TEXT_SEARCH") ?
                          tokenMap.get("CONTAINS_TEXT_SEARCH") + " |,| " :
                          ""
                  ) +
                  matchString);
          tokenCount++;
        }

        //if there is ONLY ONE search token, automatically convert it to a "Partial, Prefix Search"
        if (tokenCount == 1 && tokenMap.containsKey("CONTAINS_TEXT_SEARCH")){
          String searchToken = tokenMap.get("CONTAINS_TEXT_SEARCH").trim();
          if (searchToken != null &&
                  !searchToken.isEmpty() &&
                  !searchToken.startsWith("\"") &&
                  !searchToken.endsWith("\"") &&
                  searchToken.indexOf("*") < 0){
            tokenMap.put("CONTAINS_TEXT_SEARCH","\""+searchToken+"*\"");
          }
        }

        ///TODO: if necessary, we could remove the assertion strings above from the search string... but I suspect it would be counter-productive to the outcome accuracy and ranking order
      }

      //TODO: use regex similar to the above to pull out index specifiers and remove them from the full text search input.
      //-----  CREATE INDEX SPECIFIER EXTRACTION HERE  -----//

      //non-assertive, FullText search strings ...(and index specifiers extraction - for now)
      String[] tokens = search.split(tokenDelimRegex);
      if (tokens != null && tokens.length > 0){
        for (String token : tokens){
          String[] keyVal = token.trim().split(keyValueDelimRegex);
          if (keyVal != null && keyVal.length == 2 && !keyVal[0].isEmpty()){
            tokenMap.put(keyVal[0], keyVal[1]);
            logger.trace("Search token {}=>{}",keyVal[0], keyVal[1]);
          } else if (!token.isEmpty() && token.indexOf("*") < 0 && token.indexOf("%") < 0){
            logger.trace("...Non-specific search token: {}",token);
            fullTextSearchString += token.trim() + " ";
//            logger.debug("Discarding invalid token: {}",token);
            tokenCount++;
          }
        }
      }
      if (!fullTextSearchString.isEmpty()){
        logger.trace("FULL_TEXT_TOKEN: "+fullTextSearchString);
        tokenMap.put("FULL_TEXT_SEARCH", fullTextSearchString);
      }

      //if there is ONLY ONE search token (without spaces), automatically convert it to a "Partial, Prefix Search"
      if (tokenCount == 1 && tokenMap.containsKey("FULL_TEXT_SEARCH")){
        String searchToken = tokenMap.get("FULL_TEXT_SEARCH").trim();
        if (searchToken != null &&
                !searchToken.isEmpty() &&
                !searchToken.startsWith("\"") &&
                !searchToken.endsWith("\"") &&
                searchToken.indexOf(" ") < 0 &&
                searchToken.indexOf("*") < 0){
          searchToken = "\""+searchToken+"*\"";
          logger.trace("Converting FULL_TEXT_TOKEN to CONTAINS_TEXT_TOKEN : " + searchToken);
          tokenMap.put("CONTAINS_TEXT_SEARCH",searchToken);
          tokenMap.remove("FULL_TEXT_SEARCH");
        }
      }

    }
    logger.debug("\n\nSEARCH TOKENS: "+ JsonSchemaTransform.mapToJsonStr((Map)tokenMap)+"\n\n");
    return tokenMap;
  }

}
