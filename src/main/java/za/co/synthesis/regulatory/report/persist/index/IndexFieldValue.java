package za.co.synthesis.regulatory.report.persist.index;

import za.co.synthesis.regulatory.report.persist.CriterionType;

public class IndexFieldValue extends IndexField {
  private String value;

  public IndexFieldValue(CriterionType type, Usage usage, String name) {
    super(type, usage, name);
  }

  public IndexFieldValue(IndexField indexField) {
    super(indexField.getType(), indexField.getUsage(), indexField.getName());
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
