package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:18 PM
 * A tXstream Finsurv direct DB implementation of the Validate_ValidCCNinUCR function.
 * Needs to raise the following SARB error: 514, "Invalid customs client number completed in UCR"
 * UCR format: nZA12345678a...24a where n = last digit of the year, ZA = Fixed, 12345678 = Valid Customs Client Number, a...24a = consignment number
 *
 */
public class Validate_ValidCCNinUCR implements ICustomValidate {
  private JdbcTemplate jdbcTemplate;
  private DateTimeFormatter formatter;

  public Validate_ValidCCNinUCR(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ucr = value.toString();
    String ccn = ucr.substring(3, 11);

    Timestamp valueDate = null;

    if (otherInputs.length > 0 && otherInputs[0] != null) {
      try {
        LocalDate dt = LocalDate.parse(otherInputs[0].toString(), formatter);
        valueDate = Timestamp.valueOf(dt.atStartOfDay());
      }
      catch (IllegalArgumentException e) {
        valueDate = null;
      }
    }

    List<Map<String,Object>> mapList;

    StringBuilder sb = new StringBuilder();
    sb.append("SELECT COUNT(*), AuthBy FROM OR_CustomsClientNumber WITH (NOLOCK)");
    sb.append(" WHERE CCN = ?");
    if (valueDate != null) {
      sb.append(" AND (ValidFrom IS NULL OR ValidFrom <= ?)");
      sb.append(" AND (ValidTo IS NULL OR ValidTo >= ?)");
      sb.append(" GROUP BY AuthBy");
      mapList = jdbcTemplate.queryForList(sb.toString(), ccn, valueDate, valueDate);
    }
    else {
      sb.append(" GROUP BY AuthBy");
      mapList = jdbcTemplate.queryForList(sb.toString(), ccn);
    }

    if (mapList != null) {
      if (mapList.size() > 0) {
        Object objAuthBy = mapList.get(0).get("AuthBy");
        if (objAuthBy != null) {
          return new CustomValidateResult(StatusType.Pass);
        }
        else {
          return new CustomValidateResult(StatusType.Fail, "514", "Unauthorised customs client number completed in UCR");
        }
      }
      else
        return new CustomValidateResult(StatusType.Fail, "514", "Invalid customs client number completed in UCR");
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
