package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.ReportSpaceData;
import za.co.synthesis.regulatory.report.persist.StateData;
import za.co.synthesis.regulatory.report.persist.TransitionData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/07/26.
 */
@JsonPropertyOrder({
        "Name"
        , "DefaultChannel"
        , "Channels"
        , "States"
        , "Transitions"
})
public class ReportSpace {
  private final ReportSpaceData reportSpaceData;
  private final List<String> channels;
  private final List<State> states;
  private final List<Transition> transitions;

  public ReportSpace() {
    this.reportSpaceData = new ReportSpaceData();
    this.states = new ArrayList<State>();
    this.transitions = new ArrayList<Transition>();
    this.channels = new ArrayList<String>();
  }

  public ReportSpace(ReportSpaceData reportSpaceData, List<String> channels) {
    this.reportSpaceData = reportSpaceData;
    this.states = State.wrapList(reportSpaceData.getStates());
    this.transitions = Transition.wrapList(reportSpaceData.getTransitions());
    this.channels = channels;
  }

  @JsonProperty("Name")
  public String getName() {
    return reportSpaceData.getName();
  }

  @JsonProperty("DefaultChannel")
  public String getDefaultChannel() {
    return reportSpaceData.getDefaultChannel();
  }

  @JsonProperty("Channels")
  public List<String> getChannels() {
    return channels;
  }

  @JsonProperty("States")
  public List<State> getStates() {
    return states;
  }

  @JsonProperty("Transitions")
  public List<Transition> getTransitions() {
    return transitions;
  }

  public void setName(String name) {
    reportSpaceData.setName(name);
  }

  public void setDefaultChannel(String defaultChannel) {
    reportSpaceData.setDefaultChannel(defaultChannel);
  }

  @JsonIgnore
  public ReportSpaceData getReportSpaceData() {
    return reportSpaceData;
  }

  public void syncData() {
    reportSpaceData.getStates().clear();
    reportSpaceData.getTransitions().clear();
    for (State state : states) {
      reportSpaceData.getStates().add(state.getStateData());
    }
    for (Transition transition : transitions) {
      reportSpaceData.getTransitions().add(transition.getTransitionData());
    }
  }
}
