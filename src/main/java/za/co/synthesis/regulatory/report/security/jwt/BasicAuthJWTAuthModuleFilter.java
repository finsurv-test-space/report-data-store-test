package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BasicAuthJWTAuthModuleFilter extends OncePerRequestFilter {

  // ~ Instance fields
  // ================================================================================================

  private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();
  private AuthenticationEntryPoint authenticationEntryPoint;
  private AuthModuleManager authModuleManager;
//  private RememberMeServices rememberMeServices = new NullRememberMeServices();
  private boolean ignoreFailure = false;
  private String credentialsCharset = "UTF-8";

  /**
   * Creates an instance which will authenticate against the supplied
   * {@code AuthenticationManager} and which will ignore failed authentication attempts,
   * allowing the request to proceed down the filter chain.
   */
  public BasicAuthJWTAuthModuleFilter() {
    this.ignoreFailure = true;
  }

  public void setAuthModule(IJWTAuthModule authModule) {
    this.authModuleManager = new AuthModuleManager();
    List<IJWTAuthModule> authModules = new ArrayList<IJWTAuthModule>();
    authModules.add(authModule);
    this.authModuleManager.setAuthModules(authModules);
  }

  public void setAuthModuleManager(AuthModuleManager authModuleManager) {
    this.authModuleManager = authModuleManager;
  }

  /**
   * Creates an instance which will authenticate against the supplied
   * {@code AuthenticationManager} and use the supplied {@code AuthenticationEntryPoint}
   * to handle authentication failures.
   *
   * @param authenticationEntryPoint will be invoked when authentication fails.
   * Typically an instance of {@link BasicAuthenticationEntryPoint}.
   */
  public BasicAuthJWTAuthModuleFilter(AuthenticationEntryPoint authenticationEntryPoint) {
    Assert.notNull(authenticationEntryPoint,
            "authenticationEntryPoint cannot be null");
    this.authenticationEntryPoint = authenticationEntryPoint;
  }

  // ~ Methods
  // ========================================================================================================

  @Override
  public void afterPropertiesSet() {
    Assert.notNull(this.authModuleManager,
            "Either Authentication Module or Authentication Module Manager must be specified");

    if (!isIgnoreFailure()) {
      Assert.notNull(this.authenticationEntryPoint,
              "An AuthenticationEntryPoint is required");
    }
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request,
                                  HttpServletResponse response, FilterChain chain)
          throws IOException, ServletException {
    boolean isAuthAvailable = false;
    for (IJWTAuthModule authModule : authModuleManager.getAuthModules()) {
      isAuthAvailable = isAuthAvailable || authModule.isAvailable();
    }

    if (isAuthAvailable) {
      final boolean debug = this.logger.isDebugEnabled();

      String header = request.getHeader("Authorization");

      if (header == null || !header.toUpperCase().startsWith("BASIC ")) {
        chain.doFilter(request, response);
        return;
      }

      try {
        String[] tokens = extractAndDecodeHeader(header, request);
        assert tokens.length == 2;

        String username = tokens[0];

        if (debug) {
          this.logger
                  .debug("Basic Authentication Authorization header found for user '"
                          + username + "'");
        }

        if (authenticationIsRequired(username)) {
          JWTAuthToken authToken = null;
          for (IJWTAuthModule authModule : authModuleManager.getAuthModules()) {
            if (authModule.isAvailable()) {
              authToken = authModule.authAndGenerateAccessToken(username, tokens[1]);
              if (authToken.isAuthenticated())
                break;
            }
          }

          if (debug) {
            this.logger.debug("Authentication success: " + authToken.getPrincipal());
          }

          SecurityContextHolder.getContext().setAuthentication(authToken);
          request.setAttribute(SecurityHelper.REQUEST_AUTH_TOKEN, authToken);

          onSuccessfulAuthentication(request, response, authToken);
        }

      } catch (AuthenticationException failed) {
        SecurityContextHolder.clearContext();

        if (debug) {
          this.logger.debug("Authentication request for failed: " + failed);
        }

        onUnsuccessfulAuthentication(request, response, failed);

        if (this.ignoreFailure) {
          chain.doFilter(request, response);
        } else {
          this.authenticationEntryPoint.commence(request, response, failed);
        }

        return;
      }
    }
    chain.doFilter(request, response);
  }

  /**
   * Decodes the header into a username and password.
   *
   * @throws BadCredentialsException if the Basic header is not present or is not valid
   * Base64
   */
  private String[] extractAndDecodeHeader(String header, HttpServletRequest request)
          throws IOException {

    byte[] base64Token = header.substring(6).getBytes("UTF-8");
    byte[] decoded;
    try {
      decoded = Base64.decode(base64Token);
    }
    catch (IllegalArgumentException e) {
      throw new BadCredentialsException(
              "Failed to decode basic authentication token");
    }

    String token = new String(decoded, getCredentialsCharset(request));

    int delim = token.indexOf(":");

    if (delim == -1) {
      throw new BadCredentialsException("Invalid basic authentication token");
    }
    return new String[] { token.substring(0, delim), token.substring(delim + 1) };
  }

  private boolean authenticationIsRequired(String username) {
    // Only reauthenticate if username doesn't match SecurityContextHolder and user
    // isn't authenticated
    // (see SEC-53)
    Authentication existingAuth = SecurityContextHolder.getContext()
            .getAuthentication();

    if (existingAuth == null || !existingAuth.isAuthenticated()) {
      return true;
    }

    // Limit username comparison to providers which use usernames (ie
    // UsernamePasswordAuthenticationToken)
    // (see SEC-348)

    if (existingAuth instanceof UsernamePasswordAuthenticationToken
            && !existingAuth.getName().equals(username)) {
      return true;
    }

    // Handle unusual condition where an AnonymousAuthenticationToken is already
    // present
    // This shouldn't happen very often, as BasicProcessingFitler is meant to be
    // earlier in the filter
    // chain than AnonymousAuthenticationFilter. Nevertheless, presence of both an
    // AnonymousAuthenticationToken
    // together with a BASIC authentication request header should indicate
    // reauthentication using the
    // BASIC protocol is desirable. This behaviour is also consistent with that
    // provided by form and digest,
    // both of which force re-authentication if the respective header is detected (and
    // in doing so replace
    // any existing AnonymousAuthenticationToken). See SEC-610.
    if (existingAuth instanceof AnonymousAuthenticationToken) {
      return true;
    }

    return false;
  }

  protected void onSuccessfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response, Authentication authResult) throws IOException {
  }

  protected void onUnsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response, AuthenticationException failed)
          throws IOException {
  }

  protected AuthenticationEntryPoint getAuthenticationEntryPoint() {
    return this.authenticationEntryPoint;
  }

  protected boolean isIgnoreFailure() {
    return this.ignoreFailure;
  }

  public void setAuthenticationDetailsSource(
          AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource) {
    Assert.notNull(authenticationDetailsSource,
            "AuthenticationDetailsSource required");
    this.authenticationDetailsSource = authenticationDetailsSource;
  }

//  public void setRememberMeServices(RememberMeServices rememberMeServices) {
//    Assert.notNull(rememberMeServices, "rememberMeServices cannot be null");
//    this.rememberMeServices = rememberMeServices;
//  }

  public void setCredentialsCharset(String credentialsCharset) {
    Assert.hasText(credentialsCharset, "credentialsCharset cannot be null or empty");
    this.credentialsCharset = credentialsCharset;
  }

  protected String getCredentialsCharset(HttpServletRequest httpRequest) {
    return this.credentialsCharset;
  }
}
