package za.co.synthesis.regulatory.report.persist;

public class HeaderData {
  private String name;
  private String value;

  public HeaderData(String name, String value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }

  public String toString(){
    return this.name +": " + this.value;
  }
}
