package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 6/13/17.
 */
@JsonPropertyOrder({
        "TrnReference"
})
public class TrnReferencedEvaluationDecision extends EvaluationDecision {
  private final String trnReference;

  public TrnReferencedEvaluationDecision() {
    this.trnReference = "";
  }

  public TrnReferencedEvaluationDecision(String trnReference) {
    this.trnReference = trnReference;
  }

  public TrnReferencedEvaluationDecision(String trnReference, EvaluationDecision decision) {
    super(decision.getParameters(), decision.getEvaluation());
    this.trnReference = trnReference;
  }

  public TrnReferencedEvaluationDecision(String trnReference, EvaluationRequest parameters, EvaluationResponse evaluation) {
    super(parameters, evaluation);
    this.trnReference = trnReference;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }
}
