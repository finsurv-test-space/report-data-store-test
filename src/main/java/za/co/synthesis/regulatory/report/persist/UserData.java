package za.co.synthesis.regulatory.report.persist;

import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/06/21.
 */
public class UserData {
  private String username;
  private String password;
  private List<String> rights;
  private List<String> access;
  private List<String> channels;
  private Boolean active;
  private Map<String, Object> details;
  private List<String> reportLists;
  private List<String> userEvents;

  public UserData() {
  }

  public UserData(String username, String password, List<String> rights, List<String> access, List<String> channels, Boolean active, Map<String, Object> details, List<String> reportLists, List<String> userEvents) {
    this.username = username;
    this.password = password;
    this.rights = rights;
    this.access = access;
    this.channels = channels;
    this.active = active;
    this.details = details;
    this.reportLists = reportLists;
    this.userEvents = userEvents;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public List<String> getRights() {
    return rights;
  }

  public List<String> getReportLists() { return reportLists; }
  public List<String> getUserEvents() { return userEvents; }

  public List<String> getChannels() {
    return channels;
  }

  public List<String> getAccess() {
    return access;
  }

  public Boolean getActive() { return  active; }

  public Map<String, Object> getDetails() { return details; }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
//    if (SecurityHelper.isPasswordHashed(password)) {
      this.password = password;
//    } else {
//      this.password = SecurityHelper.getPasswordHash(password);
//    }
  }

  public void setRights(List<String> rights) {
    this.rights = rights;
  }

  public void setAccess(List<String> access) {
    this.access = access;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }
  public void setUserEvents(List<String> userEvents) {
    this.userEvents = userEvents;
  }

  public void setReportLists(List<String> reportLists) {
    this.reportLists = reportLists;
  }

  public void setActive(Boolean active) { this.active = active; }

  public void setDetails(Map<String, Object> details) { this.details = details; }
}