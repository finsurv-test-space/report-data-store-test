package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 6/7/17.
 */
@JsonPropertyOrder({
        "AccountNumber"
        , "MTA"
        , "RulingSection"
        , "ADLALevel"
        , "IsADLA"
})
public class MTAAccount {
  private String accountNumber;
  private String MTA;
  private String rulingSection;
  private String ADLALevel;
  private Boolean isADLA;

  public MTAAccount(Object accountNumber, Object MTA, Object rulingSection, Object ADLALevel, Object isADLA) {
    this.accountNumber = (String)accountNumber;
    this.MTA = (String)MTA;
    this.rulingSection = (String)rulingSection;
    this.ADLALevel = (String)ADLALevel;
    this.isADLA = Boolean.parseBoolean((String)isADLA);
  }


  @JsonProperty("AccountNumber")
  public String getAccountNumber() {
    return accountNumber;
  }

  @JsonProperty("MTA")
  public String getMTA() {
    return MTA;
  }

  @JsonProperty("RulingSection")
  public String getRulingSection() {
    return rulingSection;
  }

  @JsonProperty("ADLALevel")
  public String getADLALevel() {
    return ADLALevel;
  }

  @JsonProperty("IsADLA")
  public Boolean getIsADLA() {
    return isADLA;
  }
}
