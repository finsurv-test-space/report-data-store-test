package za.co.synthesis.regulatory.report.persist;

/**
 * Created by jake on 7/26/17.
 */
public class ChannelData {
  private String channelName;
  private String reportSpace;
  private String defaultSchema;
  private String callRepo;

  public ChannelData() {
  }

  public ChannelData(String channelName, String reportSpace, String callRepo, String defaultSchema) {
    this.channelName = channelName;
    this.reportSpace = reportSpace;
    this.defaultSchema = defaultSchema;
    this.callRepo = callRepo;
  }

  public String getChannelName() {
    return channelName;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public String getCallRepo() {
    return callRepo;
  }
  
  public String getDefaultSchema() {
    return defaultSchema;
  }
  
  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public void setCallRepo(String callRepo) {
    this.callRepo = callRepo;
  }
  
  public void setDefaultSchema(String defaultSchema) {
    this.defaultSchema = defaultSchema;
  }
}
