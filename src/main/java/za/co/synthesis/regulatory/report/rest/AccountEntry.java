package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.AccountEntryData;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;

import static za.co.synthesis.regulatory.report.persist.types.PersistenceObject.generateUUID;
import static za.co.synthesis.regulatory.report.security.SecurityHelper.getAuthMeta;


@CrossOrigin
@Controller
@RequestMapping("producer/api/accountEntry")
public class AccountEntry extends ControllerBase {
    static final Logger logger = LoggerFactory.getLogger(AccountEntry.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    private DataLogic dataLogic;


    /**
     * This API is used to upload valid account entries.
     * You need to provide a valid channelName, uploadReference and file.
     * @param channelName A channelName is required
     * @param uploadReference Used to uniquely identify an accounting entry file upload.- if not provided, a GUID will be assigned and returned
     * @param file A file containing the accounting entry detail
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    String setAccountEntry(
            @RequestParam String channelName,
            @RequestParam (required=false) String uploadReference,
            @RequestBody MultipartFile file
            ) throws Exception
    {
        Channel channel = systemInformation.getChannel(channelName);
        if (channel == null)
            throw new PreconditionFailedException("A valid channelName must be specified");

        if (uploadReference == null || uploadReference.isEmpty()){
            uploadReference = generateUUID();
        }

        if (dataLogic.isWhiteListed(file)) {

            String fileName = file.getOriginalFilename();
            if (!dataLogic.isNameSafe(file.getOriginalFilename())) {
                try {
                    fileName = dataLogic.makeNameSafe(file.getOriginalFilename());
                } catch (Exception e) {
                    throw new PreconditionFailedException(e.getMessage());
                }
            }

            ReportEventContext eventCtx = dataLogic.getReportEventContext(getAuthMeta(systemInformation.getConfigStore()));
            FileData data = new FileData(file.getContentType(), fileName, file.getBytes());
            AccountEntryData accountEntryData = new AccountEntryData(channel.getReportSpace(), uploadReference, data, eventCtx, null);
            dataLogic.setAccountEntry(channel.getReportSpace(), uploadReference, accountEntryData, eventCtx);
        } else {
            throw new PreconditionFailedException("Invalid file type (or derived mime type) provided");
        }
        return "{\"uploadReference\":\""+uploadReference+"\"}";
    };


    /**
     * This API is used to retrieve uploaded account entries.
     * You need to provide either a valid channelName or reportSpace.
     * The uploadReference is required to select the specific accounting entry file.
     * @param channelName (optional / mutually-exclusive) If provided, the ReportSpace is not required as the channelName is directly linked to a report space
     * @param reportSpace (optional / mutually-exclusive) <b>Deprecated</b>
     * @param uploadReference Required to select the specified accounting entry file uploaded.
     * @return a JSON object containing the Trn Reference, filename, file type and base64 encoded file contents.
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    AccountEntryData getAccountEntry(
            @RequestParam(required = false) String channelName,
            @RequestParam(required = false) String reportSpace,
            @RequestParam String uploadReference) {
        if (channelName == null && reportSpace == null)
            throw new PreconditionFailedException("A valid Channel Name or Report Space must be specified");

        if (reportSpace == null) {
            Channel channel = systemInformation.getChannel(channelName);
            if (channel == null) {
                throw new PreconditionFailedException("A valid Channel Name must be specified - " + channelName + " is incorrect");
            } else {
                reportSpace = channel.getReportSpace();
            }
        } else {
            if (systemInformation.getReportSpace(reportSpace) == null)
                throw new PreconditionFailedException("A valid Report Space must be specified - " + reportSpace + " is incorrect");
        }


        AccountEntryData accountEntryData = dataLogic.getAccountEntry(reportSpace, uploadReference);
        if (accountEntryData == null){
            throw new ResourceNotFoundException("Requested Account Entry ("+uploadReference+"@"+reportSpace+") not found.");
        }

        return accountEntryData;
    };

}
