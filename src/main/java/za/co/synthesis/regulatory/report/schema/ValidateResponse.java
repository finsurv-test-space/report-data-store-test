package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.StatusType;

/**
 * User: jake
 * Date: 5/3/16
 * Time: 9:19 AM
 * A class to wrap the CustomValidateResult to make it look good
 */
@JsonPropertyOrder({
        "Status"
        , "Code"
        , "Message"
})
public class ValidateResponse {
  private StatusType status;
  private String code;
  private String message;

  public ValidateResponse(StatusType status, String code, String message) {
    this.status = status;
    this.code = code;
    this.message = message;
  }

  public ValidateResponse(StatusType status) {
    this.status = status;
    if (status != StatusType.Pass) {
      this.code = "ERR";
      this.message = "Message and code needs to be set for validation results that do not pass";
    }
  }

  public ValidateResponse(CustomValidateResult result) {
    this.status = result.getStatus();
    this.code = result.getCode();
    this.message = result.getMessage();
  }

  @JsonProperty("Status")
  public StatusType getStatus() {
    return status;
  }

  @JsonProperty("Code")
  public String getCode() {
    return code;
  }

  @JsonProperty("Message")
  public String getMessage() {
    return message;
  }
}
