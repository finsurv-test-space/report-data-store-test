package za.co.synthesis.regulatory.report.support;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jake on 6/13/17.
 */
public class ReportServiceExecutorService {
  private ExecutorService executorService;
  private int threads = 10;

  public ReportServiceExecutorService() {
  }

  public int getThreads() {
    return threads;
  }

  public void setThreads(int threads) {
    this.threads = threads;
  }

  public synchronized ExecutorService getExecutorService() {
    if (executorService == null) {
      executorService = Executors.newFixedThreadPool(threads);
    }
    return executorService;
  }
}
