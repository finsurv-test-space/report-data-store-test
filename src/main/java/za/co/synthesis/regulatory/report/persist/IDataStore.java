package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.rest.Checksum;
import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecision;
import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecisions;
import za.co.synthesis.regulatory.report.schema.ListResult;
import za.co.synthesis.regulatory.report.schema.SortDefinition;

import java.util.List;
import java.util.Map;

/**
 * Created by jake on 5/24/17.
 */
public interface IDataStore {

  //-----  Evaluation Storage  -----//
  void setEvaluationDecision(FullyReferencedEvaluationDecision evaluation);
  FullyReferencedEvaluationDecision getEvaluationDecision(String reportSpace, String trnReference);
  void setEvaluationDecisionLog(FullyReferencedEvaluationDecisions evaluations);
  FullyReferencedEvaluationDecisions getEvaluationDecisionLog(String reportSpace, String trnReference);

  //-----  Report Storage  -----//
  void setReport(String reportSpace, String trnReference, ReportInfo report, boolean addToDownload);
  ReportInfo getReport(String reportSpace, String trnReference);
  List<ReportInfo> getReportHistory(String reportSpace, String trnReference);

  //-----  Document Storage  -----//
  DocumentData setDocumentContent(DocumentData document, FileData data);
  ChecksumData setChecksumContent(ChecksumData document);
  FileData getDocumentContent(String reportSpace, String documentHandle);
  FileData getChecksumContent(String reportSpace, String valueDate);
  DocumentData getDocument(String reportSpace, String documentHandle);
  ChecksumData getChecksum(String reportSpace, String valueDate);
  DocumentData setDocument(DocumentData document, FileData data);
  ChecksumData setChecksum(ChecksumData document);
  boolean hasDocument(String reportSpace, String documentHandle);
  boolean hasChecksum(String reportSpace, String valueDate);
  List<DocumentData> getDocumentsByReference(String reportSpace, String trnReference);

  //-----  AccountEntry Storage  -----//
  AccountEntryData setAccountEntry(String reportSpace, String reportKey, AccountEntryData data);
  AccountEntryData getAccountEntry(String reportSpace, String trnReference);



  //-----  Download Support -----//
  //return a list with corresponding SyncPoints?
  ReportInfoDownload getReports(String reportSpace, String fromSyncPoint, Integer maxRecords);
  DecisionDownload getDecisions(String reportSpace, String fromSyncPoint, Integer maxRecords);
  DocumentDataDownload getDocuments(String reportSpace, String fromSyncPoint, Integer maxRecords);
  AccountEntryDownload getAccountEntries(String reportSpace, String fromSyncPoint, Integer maxRecords);
  ChecksumDataDownload getChecksums(String reportSpace, String fromSyncPoint, Integer maxRecords);

  //-----  Report List Support -----//
  ListResult getReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, UserAccessData userAccessData, Integer maxRecords, Integer page);
  ListResult getOrderedReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, SortDefinition sortDefinition, UserAccessData userAccessData, Integer maxRecords, Integer page);

  //-----  Send Notification Support -----//
  void addNotification(NotificationData notification);
  List<NotificationData> getNotifications();
  void removeNotification(NotificationData notification);
  
  void changeReportKey(String reportSpace, String trnReference, String newTrnReference);

  void logActivity(LogActivityType activityType, LogReadWrite ioAction, String Url, String httpAction, String reportSpace, String trnReference, String logBody);
}
