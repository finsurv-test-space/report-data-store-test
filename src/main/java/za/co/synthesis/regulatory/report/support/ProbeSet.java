package za.co.synthesis.regulatory.report.support;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/08/11.
 */
public class ProbeSet {
  final String Name;
  final String Description;
  final List<ProbeEntry> items;

  public ProbeSet(String name, String description) {
    Name = name;
    Description = description;
    items = new ArrayList<ProbeEntry>();
  }

  public List<ProbeEntry> getItems(){
    return items;
  }

  public List<String> getPaths() {
    List<String> paths = new ArrayList<>();
    if (items != null) {
      for (ProbeEntry item : items) {
        paths.add(item.getKey());
      }
    }
    return paths;
  }
}
