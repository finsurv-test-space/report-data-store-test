package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.ChecksumData;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.rule.core.Scope;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */
@Entity
public class ChecksumObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(ChecksumObject.class);

  private String valueDate;
  private String reportSpace;
  private String fileType;
  private String fileName;
  private byte[] content;
  private String authToken;
  private Map<String, Object> eventContext;
  private Integer version = 1;


  public ChecksumObject(String uuid) {
    this(uuid, null, null);
  }

  public ChecksumObject(String uuid, ChecksumData checksum) {
    this(uuid, checksum, null);
  }

  public ChecksumObject(String uuid, ChecksumData checksum, ReportEventContext eventContext) {
    super(uuid);
    try {
      setChecksumData(checksum);
    }catch (Exception ex){
      logger.error("Could not set the checksum data");
    }

    try {
      //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
      ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
      if (user != null) {
        eventContext = new ReportEventContext(user, LocalDateTime.now());
      }
    } catch (Exception e) {
    }
    this.eventContext = SerializationTools.serializeEventContext(eventContext);
  }


  public FileData getData() { return new FileData(fileType, fileName, content); }


  public static Scope getScopeEnum(String scope){
    Scope scopeEnum = Scope.Transaction;
    if (scope != null){
      if (scope.equals(Scope.ImportExport.toString())){
        scopeEnum = Scope.ImportExport;
      } else if (scope.equals(Scope.Money.toString())){
        scopeEnum = Scope.Money;
      }
    }
    return scopeEnum;
  }

  public ChecksumData getChecksumData() {
    ChecksumData checksumData = new ChecksumData(valueDate, reportSpace);
    checksumData.setFilename(this.fileName);
    checksumData.setFileType(this.fileType);
//    checksumData.setContent(new String(DataLogic.Base64Decode(this.dataContent)));
    checksumData.setValueDate(this.valueDate);
    checksumData.setContent(this.content);
    return checksumData;
  }

  public void setChecksumData(ChecksumData checksumData){
    if (checksumData != null) {
      this.valueDate = checksumData.getValueDate();
      this.reportSpace = checksumData.getReportSpace();
      this.fileName = checksumData.getFileName();
      this.fileType = checksumData.getFileType();
      this.content = checksumData.getContent();
    }
  }

  public String getValueDate() {
    return valueDate;
  }

  public void setValueDate(String valueDate){
    this.valueDate = valueDate;
  }

  public String getReportSpace() {
    return this.reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getContent(){
    JSObject jsonMap = new JSObject();

    jsonMap.put("User", SecurityHelper.getAuthMeta().getName());

    if (content != null && content.length > 0) {
      jsonMap.put("FileName", fileName);
      jsonMap.put("ContentType", fileType);
//      jsonMap.put("Content", new String(DataLogic.Base64Encode(content)));
    }
    if (!jsonMap.isEmpty()) {
      return mapToJsonStr(jsonMap);
    }
    return null;
  }

  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);

        Object obj = jsonMap.get("Scope");

        obj = jsonMap.get("FileName");
        this.fileName = obj!=null?obj.toString():null;
        obj = jsonMap.get("ContentType");
        this.fileType = obj!=null?obj.toString():null;
//        obj = jsonMap.get("Content");
//        String content = obj!=null?obj.toString():null;
//        if (content != null) {
//          this.content = DataLogic.Base64Decode(content.getBytes());
//        }
      } catch (Exception e){
        logger.error("Unable to deserialize json content for Document object: "+e.getMessage(), e);
      }
    }
  }

//  public byte[] getDataContent() {
//    return dataContent;
//  }

  public String getDataContent() {
    return new String(DataLogic.Base64Encode(content));
  }

//  public void setContent(byte[] content) {
//    this.content = content;
//  }
//

  public void setDataContent(String dataContent) {
    this.content = DataLogic.Base64Decode(dataContent.getBytes());
  }

  public String getAuthToken() {

    if (this.eventContext == null) {
      try {
        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
        if (user != null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      } catch (Exception e) {
        //we seem to be running into null pointers where we really shouldn't be.
        logger.warn("Unable to persist the ReportObject eventContext / Audit detail : " + e.getMessage());
        e.printStackTrace();
      }
    }

    return mapToJsonStr(eventContext);
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }


}
