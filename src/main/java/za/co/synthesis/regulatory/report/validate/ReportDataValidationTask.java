package za.co.synthesis.regulatory.report.validate;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.schema.ValidationResult;
import za.co.synthesis.regulatory.report.schema.ValidationResultType;
import za.co.synthesis.regulatory.report.support.MapFinsurvContext;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ValidationStats;
import za.co.synthesis.rule.core.Validator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by jake on 6/13/17.
 */
public class ReportDataValidationTask implements Callable<ValidationResult> {
  final static Log log = LogFactory.getLog(ReportDataValidationTask.class);

  private final Validator validator;
  private final String reportSpace;
  private final ReportData reportData;

  public ReportDataValidationTask(Validator validator, String reportSpace, ReportData reportData) {
    this.validator = validator;
    this.reportSpace = reportSpace;
    this.reportData = reportData;
  }

  @Override
  public ValidationResult call() throws Exception {
    // NOTE: THIS CODE MUST BE WELL BEHAVED and follow the following rules:
    // 1. All selects from the database should be with NO LOCKING since we will be running at the same time as
    // other processes responsible for updating the database and DB deadlocks could then result.
    // 2. NO UPDATING OF THE DATABASE AT ALL!!!!!!!!

    ValidationResult result = new ValidationResult(reportSpace, reportData.getTrnReference());
    ValidationStats stats = new ValidationStats();

    List<ResultEntry> entries = validator.validate (new MapFinsurvContext(reportData.getReport()), ValidationUtils.getCustomValues(reportData.getMeta()), stats);

    if (stats.getErrorCount() > 0) {
      result.setResultType(ValidationResultType.Failure);
    }
    else {
      result.setResultType(ValidationResultType.Success);
    }

    result.setValidations(ValidationUtils.filterValidationResults(entries));
    result.setValidationTime(LocalDateTime.now());

    return result;
  }
}
