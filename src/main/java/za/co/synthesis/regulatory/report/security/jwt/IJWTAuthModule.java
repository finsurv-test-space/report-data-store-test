package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.security.core.AuthenticationException;

public interface IJWTAuthModule {
  boolean isAvailable();
  String getName();
  String authAndComposeJWTIdToken(JWTProvider provider, String username, String password) throws AuthenticationException;
  String composeJWTAccessTokenForAuthenticatedUser(JWTProvider provider, String username) throws AuthenticationException;
  String authAndComposeJWTAccessToken(JWTProvider provider, String username, String password) throws AuthenticationException;
  JWTAuthToken authAndGenerateAccessToken(String username, String password);
}
