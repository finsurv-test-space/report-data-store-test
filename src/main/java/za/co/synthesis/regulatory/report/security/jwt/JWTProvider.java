package za.co.synthesis.regulatory.report.security.jwt;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.StoredKey;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * OWASP document containing considerations for using JWT.
 *
 * https://www.owasp.org/index.php/JSON_Web_Token_(JWT)_Cheat_Sheet_for_Java#Consideration_about_using_JWT
 *
 * - It is recommended that there be some secured element within the token to both
 *   validate authenticity and integrity of the token (aside from the signature).
 * - Sensitive data should be obscured/encrypted so that it is difficult to gain an
 *   understanding of security roles etc within the system.
 *
 */
public class JWTProvider {
  private static final Logger logger = LoggerFactory.getLogger(JWTProvider.class);

  public SystemInformation getSystemInformation() {
    return systemInformation;
  }

  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  private SystemInformation systemInformation;
  public final static String subject = "report-data-store";
  public final static String applicationName = "report-data-store";

  public final static String USER = "user";
  public final static String TEMPLATE_USER = "template_user";
  public final static String ACCESS = "access";
  public final static String RIGHTS = "rights";
  public final static String TOKENUSE = "token_use";
  public final static String AUTHMODULE = "auth_module";
  public final static String ISSUER = "iss";

  public static boolean useFileAndUrlFriendlyBase64 = true;

  public static boolean getUseFileAndUrlFriendlyBase64() {
    return useFileAndUrlFriendlyBase64;
  }

  public static void setUseFileAndUrlFriendlyBase64(boolean useFileAndUrlFriendlyBase64) {
    JWTProvider.useFileAndUrlFriendlyBase64 = useFileAndUrlFriendlyBase64;
  }

  public String produceJWTID(String username, String authModuleName, Map<String, Object> other) {
    if (other == null) {
      other = new HashMap<String, Object>();
    }
    other.put(AUTHMODULE, authModuleName);
    return produceJWT(username, null, null, other, JWTAuthToken.TokenUse.ID);
  }

  public String produceJWTAccess(String username, List<String> access, List<String> rights, Map<String, Object> other) {
    return produceJWT(username, access, rights, other, JWTAuthToken.TokenUse.ACCESS);
  }

  public static String fixSignature(String nativeJwt) {
    if (!useFileAndUrlFriendlyBase64) {
      int pos = nativeJwt.lastIndexOf('.') + 1;
      String base64UrlEncodedSignature = nativeJwt.substring(pos);
      byte[] signature = TextCodec.BASE64URL.decode(base64UrlEncodedSignature);
      return nativeJwt.substring(0, pos) + Base64.getEncoder().encodeToString(signature);
    }
    return nativeJwt;
  }

  private String produceJWT(String username, List<String> access, List<String> rights, Map<String, Object> other, JWTAuthToken.TokenUse tokenUse) {
    Map<String, Object> claims = new HashMap<String, Object>();
    Map<String, Object> headers = new HashMap<String, Object>();
    claims.put(USER, username);
    if (access != null)
      claims.put(ACCESS, access);
    if (rights != null)
      claims.put(RIGHTS, rights);
    claims.put(TOKENUSE, tokenUse.toString());
    if (other != null) {
      claims.putAll(other);
    }
    StoredKey pvtKey = null;
    try {
      pvtKey = systemInformation.getCurrentPrivateKey();
    } catch (Exception e) {
      logger.debug("Error fetching public key to use for JWT signature creation", e);
    }

    Date issuedDate = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
    LocalDateTime ldt = LocalDateTime.now().plusMinutes(systemInformation.getTokenExpiryMinutes(tokenUse));
    Date expiryDate = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    JwtBuilder jwtBuilder = Jwts.builder()
            .setSubject(subject)
            .setIssuer(applicationName)
            .setIssuedAt(issuedDate)
            .setExpiration(expiryDate)
            .addClaims(claims);
            //.compressWith(CompressionCodecs.DEFLATE);

    if (pvtKey != null) {
      SignatureAlgorithm sigAlg = SignatureAlgorithm.RS512;
      headers.put("kid", pvtKey.getKeyId());
      jwtBuilder = jwtBuilder.signWith(sigAlg, pvtKey.getKey());
    }
    jwtBuilder.setHeader(headers);

    return fixSignature(jwtBuilder.compact());
  }

}
