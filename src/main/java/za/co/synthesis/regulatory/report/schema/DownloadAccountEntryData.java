package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.AccountEntryData;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({
        "Syncpoint"
        , "ÄccountEntries"
})
public class DownloadAccountEntryData {
    private String syncPoint;
    private List<AccountEntryData> accountEntries;

    public DownloadAccountEntryData(String syncPoint, List<AccountEntryData> accountEntries){
        this.syncPoint = syncPoint;
        this.accountEntries = accountEntries;
    }

    @JsonProperty("SyncPoint")
    public String getSyncPoint() {
        return syncPoint;
    }

    @JsonProperty("AccountEntries")
    public List<AccountEntryData> getAccountEntries(){return accountEntries;}

}
