package za.co.synthesis.regulatory.report.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.rule.core.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Created by jake on 6/13/17.
 */
public class ValidationUploadSet implements Runnable {
  public static class ValidationSet {
    private ReportData reportData;
    private Future<ValidationResult> result;

    public ValidationSet(ReportData reportData, Future<ValidationResult> result) {
      this.reportData = reportData;
      this.result = result;
    }
  }
  private static final Logger logger = LoggerFactory.getLogger(ValidationUploadSet.class);

  private final ExecutorService executorService;
  private final String schema;
  private final Channel channel;
  private final DataLogic dataLogic;

  private final String uploadReference;
  private final List<ValidationSet> validationList = new ArrayList<ValidationSet>();
  private boolean hasCompleted = false;

  private Authentication authentication;

  public ValidationUploadSet(ExecutorService executorService, String schema, Channel channel, DataLogic dataLogic, Authentication authentication) {
    this.executorService = executorService;
    this.schema = schema;
    this.channel = channel;
    this.dataLogic = dataLogic;
    this.uploadReference = UUID.randomUUID().toString();
    this.authentication = authentication;
  }

  public String getUploadReference() {
    return uploadReference;
  }

  public List<ValidationSet> getValidationList() {
    return validationList;
  }

  public void addValidation(Validator validator, ReportData reportData) {
    Future<ValidationResult> validationResult =
            executorService.submit(
                    new ReportDataValidationTask(validator, channel.getReportSpace(), reportData)
            );
    ValidationSet validationSet = new ValidationUploadSet.ValidationSet(reportData, validationResult);
    validationList.add(validationSet);
  }

  public void closeSet() {
    closeSet(false);
  }

  public void closeSet(boolean blocking) {
    executorService.submit(this);
    while (blocking && !hasCompleted) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        logger.error("Error while waiting for upload set to finish", e);
      }
    }
  }

  public UploadStatus getUploadStatus() {
    for (ValidationSet validationSet : validationList) {
      if (!validationSet.result.isDone())
        return new UploadStatus(UploadStatus.Status.InProgess);
    }
    return new UploadStatus(UploadStatus.Status.Complete);
  }

  public List<ValidationResult> getUploadResultList() throws ExecutionException, InterruptedException {
    List<ValidationResult> result = new ArrayList<ValidationResult>();
    for (ValidationSet validationSet : validationList) {
      if (!validationSet.result.isDone())
        return null;
      result.add(validationSet.result.get());
    }
    return result;
  }

  @Override
  public void run() {
    hasCompleted = false;
    boolean isDone = false;
    while (!hasCompleted) {
      try {
        Thread.sleep(isDone?0:10);
      } catch (InterruptedException e) {
        logger.error("Error while waiting for upload set to finish", e);
      }
      if (!isDone) {
        for (ValidationSet validationSet : validationList) {
          if (!validationSet.result.isDone()) {
            break;
          }
          isDone = true;
        }
      }
      if (isDone) {
        for (ValidationSet validationSet : validationList) {
          ValidationResult valResult = null;
          try {
            valResult = validationSet.result.get();

            ReportInfo existingInfo = dataLogic.getReport(valResult.getReportSpace(), valResult.getTrnReference());
            String currentState = existingInfo != null ? existingInfo.getState(): null;
            String newState = null;
            try {
              newState = dataLogic.getStateForAddOrUpdateReport(valResult.getReportSpace(), validationSet.reportData, currentState, channel.getChannelName());
            } catch (Exception e) {
              logger.error("Cannot determine report state", e);
            }

            newState = dataLogic.getStateForValidationResult(channel.getReportSpace(), validationSet.reportData, channel.getChannelName(), newState, valResult.getResultType().equals(ValidationResultType.Success));
            ReportInfo reportInfo =
                    new ReportInfo(newState,
                            schema, channel.getReportSpace(), channel.getChannelName(),
                            validationSet.reportData,
                            valResult.getValidations(),
                            dataLogic.getReportEventContext(authentication)
                    );
            dataLogic.setReport(valResult.getReportSpace(), channel.getChannelName(), valResult.getTrnReference(), reportInfo);
          } catch (Exception e) {
            logger.error("Error while processing upload set", e);
          }
        }
        // Persist stuff
        hasCompleted = true;
      }
    }
  }
}
