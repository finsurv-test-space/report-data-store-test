package za.co.synthesis.regulatory.report.support;

/**
 * Created by jake on 8/2/17.
 */
public enum ActionType {
  QueueForDownload,
  Cancel,
  CancelWithReplace,
  SendNotification
}
