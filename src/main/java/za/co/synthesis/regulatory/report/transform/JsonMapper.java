package za.co.synthesis.regulatory.report.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/06/05.
 */
public class JsonMapper {

    /**
     * A very rudimentary conversion mapper for JSONObjectMap-to-JSONObjectMap conversion.
     * Each JSONObjectMap is represented by a path string and it's accompanying value-object
     */

    public static Map<String, Object> mapJsonSrcToTarget(Map<String, Object> src, Map<String, String> mappings){
        Map<String, Object> targetMapping = new HashMap<>();
        Map<String, Object> overflow = new HashMap<>();
        Map<String, String> missing = new HashMap<>();
        List<String> overFlowKeys = new ArrayList<>(src.keySet());
        for (String srcPath : mappings.keySet()){
            String tgtPath = mappings.get(srcPath);
            if (src.containsKey(srcPath)){
                Object obj = src.get(srcPath);
                overFlowKeys.remove(srcPath);
                targetMapping.put(tgtPath, obj);
            } else {
                missing.put(srcPath, tgtPath);
            }
        }
        //create missing items?
        //store overflow items as meta?
        return targetMapping;
    }

    public static void mapJsonObject(Object obj, String path, Map<String, Object> objMap){
        path = (path==null?"":path);
        if (obj instanceof Map){
            Map jso = (Map)obj;
            for (Object key : jso.keySet()){
                mapJsonObject(jso.get(key), path + (path.isEmpty()?"":".") + key, objMap);
            }
        } else if (obj instanceof List){
            List jso = (List)obj;
            int i = 0;
            for (Object child : jso){
                mapJsonObject(child, path + "["+(i++)+"]", objMap);
            }
        } else {
            objMap.put(path,obj);
//            System.out.println("\""+path+"\":\t\t\""+obj+"\"");
        }
    }
}
