package za.co.synthesis.regulatory.report.persist;

import java.util.List;

/**
 * Created by james on 2017/09/05.
 */
public class DocumentDataDownload {
  private final String upToSyncpoint;
  private final List<DocumentData> documentInfoList;

  public DocumentDataDownload(String upToSyncpoint, List<DocumentData> documentInfoList) {
    this.upToSyncpoint = upToSyncpoint;
    this.documentInfoList = documentInfoList;
  }

  public String getUpToSyncpoint() {
    return upToSyncpoint;
  }

  public List<DocumentData> getDocumentDataList() {
    return documentInfoList;
  }

}
