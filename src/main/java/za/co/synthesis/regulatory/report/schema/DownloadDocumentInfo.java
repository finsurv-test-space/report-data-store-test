package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.DocumentData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/8/17.
 */
@JsonPropertyOrder({
        "SyncPoint"
        , "Decisions"
})
public class DownloadDocumentInfo {
  private String syncPoint;
  private List<DocumentInfo> documents;

  public DownloadDocumentInfo(String syncPoint, List<DocumentInfo> documents) {
    this.syncPoint = syncPoint;
    this.documents = documents;
  }

  @JsonProperty("SyncPoint")
  public String getSyncPoint() {
    return syncPoint;
  }

  @JsonProperty("Documents")
  public List<DocumentInfo> getDocuments() {
    return documents;
  }

  public static List<DocumentInfo> wrapDocumentDataList(List<DocumentData> documents, boolean uploaded){
    List<DocumentInfo> docs = new ArrayList<>();
    for (DocumentData document: documents){
      docs.add(new DocumentInfo(document, uploaded));
    }
    return docs;
  }
}
