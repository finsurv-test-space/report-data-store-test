package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.common.TableDefinition;
import za.co.synthesis.regulatory.report.persist.types.AccessSetObject;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.persist.types.UserObject;
import za.co.synthesis.regulatory.report.schema.StoredKey;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;
import za.co.synthesis.regulatory.report.support.properties.SettingsCrypto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJson;

/**
 * Created by jake on 5/29/17.
 */
public class MSSqlConfigStore extends MemoryConfigStore {
  private static final Logger logger = LoggerFactory.getLogger(MSSqlConfigStore.class);

  private final Map<Class, ITablePersist> persistTables = new HashMap<Class, ITablePersist>();

  private JdbcTemplate jdbcTemplate;
  private final static String TOKEN = "Token";
  private final static String TOKENACCESS = "AccessTimeoutMinutes";
  private final static String TOKENID = "IdTimeoutMinutes";
  private final static String KEY = "Key";
  private final static String LATESTKEYID = "LatestKeyId";

  @Required
  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    persistTables.clear();
    loadCommonPersistors(jdbcTemplate, persistTables);
  }

  private static void loadCommonPersistors(JdbcTemplate jdbcTemplate, Map<Class, ITablePersist> persistTables){
    Map<String, TableDefinition> definitions = TableDefinition.getCommonTableDefinitionMap();
    ITablePersist table = new MSSqlTablePersist<UserObject>(definitions.get(TableConstant.User), jdbcTemplate, UserObject.class);
    persistTables.put(table.getPersistClass(), table);
    table = new MSSqlTablePersist<AccessSetObject>(definitions.get(TableConstant.AccessSet), jdbcTemplate, AccessSetObject.class);
    persistTables.put(table.getPersistClass(), table);
  }

  private void deleteConfigRecord(String className, String key) {
    StringBuilder sb = new StringBuilder();
    sb.append("INSERT INTO [").append(TableConstant.ConfigDelQueue).append("] ([Class], [Key]) ");
    sb.append("VALUES (?, ?)");
    jdbcTemplate.update(sb.toString(), className, key);
  }

  private JSObject getPersistedJSObject(String className, String key) {
    String content = getPersistedString(className, key);
    if (content != null) {
      JSStructureParser parser = new JSStructureParser(content);
      try {
        return (JSObject) parser.parse();
      } catch (Exception e) {
        logger.error("Cannot parse " + className + ": " + key + " from DB", e);
        return null;
      }
    }
    return null;
  }

  private String getPersistedString(String className, String key) {
    StringBuilder sb = new StringBuilder();
    sb.append("SELECT [Content] FROM [SystemConfiguration] ");
    sb.append("WHERE [Name] = ?");
    Object[] params = new Object[1];
    params[0] = key;
    List<Map<String, Object>> list = jdbcTemplate.queryForList(sb.toString(), params);
    if (list != null && list.size() > 0) {
      return (String) list.get(0).get("Content");
    }
    return null;
  }

  private List<String> getPersistedNamesForClass(String className) {
    List<String> result = new ArrayList<String>();
    StringBuilder sb = new StringBuilder();
    sb.append("SELECT [Name] FROM [SystemConfiguration] ");
    sb.append("WHERE [Area] = ?");
    Object[] params = new Object[1];
    params[0] = className;
    List<Map<String, Object>> list = jdbcTemplate.queryForList(sb.toString(), params);
    if (list != null) {
      for (Map<String, Object> row : list){
        result.add ((String) row.get("Name"));
      }
    }
    return result;
  }

  private void persistJSObject(String className, String key, JSObject jsObject) {
    String content = mapToJsonStr(jsObject);
    persistString(className, key, content);
  }

  private void persistString(String className, String key, String content) {
    StringBuilder sb = new StringBuilder();
//    sb.append("INSERT INTO [SystemConfiguration] ([Name], [Area], [Content])  ");
//    sb.append("VALUES( ?, ?, ? )");
    sb.append("DECLARE @className VARCHAR(100);\n" +
            "SET @className = ?;\n" +
            "DECLARE @confKey VARCHAR(100);\n" +
            "SET @confKey = ?;\n" +
            "DECLARE @confContent VARCHAR(MAX);\n" +
            "SET @confContent = ?;\n" +
            "IF (EXISTS( SELECT * FROM [SystemConfiguration] WHERE [Area] = @className AND [Name] = @confKey ))\n" +
            "BEGIN\n" +
            "  UPDATE [SystemConfiguration] SET [Content] = @confContent, [CreatedDateTime] = GETDATE()\n" +
            "  WHERE [Area] = @className AND [Name] = @confKey;\n" +
            "END\n" +
            "ELSE\n" +
            "BEGIN\n" +
            "  INSERT INTO [SystemConfiguration] ([Name], [Area], [Content])\n" +
            "  VALUES (@confKey, @className, @confContent);\n" +
            "END" );
    jdbcTemplate.update(sb.toString(), className, key, content);
  }

  //----- persisting configuration
  @Override
  protected JSObject getJSObject(String schema, String path) throws Exception {
    String relativePath = path.replace(getDefaultConfigLocation(),"")
            .replace(getDefaultTypeLocation(),"")
            .replace(getDefaultMiscLocation(),"");
    JSObject result = getPersistedJSObject(schema, relativePath);
    if (result == null) {
      result = super.getJSObject(schema, path);
      if (result != null) {
        System.out.println("Persisting config: "+ path + " to database with key ["+relativePath+"]");
        persistJSObject(schema, relativePath, result);
      }
    }
    return result;
  }

  //-----  User Management Interfaces  -----//
  @Override
  public Map<String, UserData> getUserMap() {
    Map<String, UserData> result = new HashMap<String, UserData>();

    ITablePersist<UserObject> persistor = persistTables.get(UserObject.class);
    if (persistor != null) {
      List<UserObject> userObjects = persistor.readCurrent();

      for (UserObject userObject : userObjects){
        result.put(userObject.getUsername(), userObject.getUserData());
      }
    }
    return result;
  }

  @Override
  public void setUser(UserData user) {
    ITablePersist<UserObject> persistor = persistTables.get(UserObject.class);
    try {
      persistor.write(new UserObject(PersistenceObject.generateUUID(), user));
    } catch (PersistException e) {
      logger.error("Error persisting " + persistor.getPersistClass().getName(), e);
    }
  }

  @Override
  public void removeUser(String username) {
    deleteConfigRecord(TableConstant.User, username);
  }

  //-----  Access Set -----//
  @Override
  public Map<String, AccessSetData> getAccessSetMap() {
    Map<String, AccessSetData> result = new HashMap<String, AccessSetData>();

    ITablePersist<AccessSetObject> persistor = persistTables.get(AccessSetObject.class);
    if (persistor != null) {
      List<AccessSetObject> accessSetObjects = persistor.readCurrent();

      for (AccessSetObject accessSetObject : accessSetObjects){
        result.put(accessSetObject.getName(), accessSetObject.getAccessSetData());
      }
    }
    return result;
  }

  @Override
  public void setAccessSet(AccessSetData accessSet) {
    ITablePersist<AccessSetObject> persistor = persistTables.get(AccessSetObject.class);
    try {
      persistor.write(new AccessSetObject(PersistenceObject.generateUUID(), accessSet));
    } catch (PersistException e) {
      logger.error("Error persisting " + persistor.getPersistClass().getName(), e);
    }
  }

  @Override
  public void removeAccessSet(String setname) {
    deleteConfigRecord(TableConstant.AccessSet, setname);
  }

  private Map<String, List<String>> getSimpleCriteria(String key, String value) {
    Map<String, List<String>> result = new HashMap<String, List<String>>();
    List<String> values = new ArrayList<String>();
    values.add(value);
    result.put(key, values);
    return result;
  }

  @Override
  public UserData getUser(String username) {
    ITablePersist<UserObject> persistor = persistTables.get(UserObject.class);
    List<UserObject> list = persistor.queryByMapList(getSimpleCriteria(FieldConstant.Key, username), 1, 0, true, null);
    if (list.size() > 0) {
      return list.get(0).getUserData();
    }
    return null;
  }

  @Override
  public String setKeyPair(String kid, StoredKey storedPrivateKey, StoredKey storedPublicKey) {
    if (kid == null){
      kid = PersistenceObject.generateUUID();
    }
    try {
      persistString(KEY, LATESTKEYID, kid);
    } catch(Exception e) {
      logger.error("Unable to set latest KID ("+kid+"): "+e.getMessage(), e);
    }

    if (storedPrivateKey != null) {
      JSObject jso = storedPrivateKey.toJSObject();
      //*******************************//
      //*** encrypt the private key ***//
      SettingsCrypto crypto = SettingsCrypto.Standard();
      String keyStr = (String)jso.get("key");
      keyStr = crypto.encrypt(keyStr);
      jso.put("key", keyStr);
      //*******************************//
      persistJSObject(KEY, kid + "_PVT", jso);
    }
    if (storedPublicKey != null) {
      persistJSObject(KEY, kid, storedPublicKey.toJSObject());
    }
    return kid;
  }

  @Override
  public String getCurrentKeyId() {
    return getPersistedString(KEY, LATESTKEYID);
  }

  @Override
  public StoredKey getPrivateKey(String kid) {
    if (kid != null) {
      JSObject jso = getPersistedJSObject(KEY, kid + "_PVT");
      //*******************************//
      //*** decrypt the private key ***//
      SettingsCrypto crypto = SettingsCrypto.Standard();
      String keyStr = (String)jso.get("key");
      keyStr = crypto.decrypt(keyStr);
      jso.put("key", keyStr);
      //*******************************//
      return StoredKey.fromJSObject(jso);
    }
    return null;
  }

  @Override
  public StoredKey getPublicKey(String kid) {
    if (kid != null) {
      JSObject jso = getPersistedJSObject(KEY, kid);
      return StoredKey.fromJSObject(jso);
    }
    return null;
  }

  @Override
  public Map<String, StoredKey> getPublicKeys() {
    Map<String, StoredKey> manifest = new HashMap<String, StoredKey>();
    List<String> names = getPersistedNamesForClass(KEY);
    for (String name : names) {
      if (!name.endsWith("_PVT") && !name.equals(LATESTKEYID))
        manifest.put(name, getPublicKey(name));
    }
    return manifest;
  }

  @Override
  public long getTokenExpiryMinutes(JWTAuthToken.TokenUse tokenUse) {
    if (tokenUse.equals(JWTAuthToken.TokenUse.ID)) {
      String mins = getPersistedString(TOKEN, TOKENID);
      try{
        return Long.parseLong(mins);
      }
      catch (Exception e){
        persistString(TOKEN, TOKENID, "600");
        return 600;
      }
    }
    if (tokenUse.equals(JWTAuthToken.TokenUse.ACCESS)) {
      String mins = getPersistedString(TOKEN, TOKENACCESS);
      try{
        return Long.parseLong(mins);
      }
      catch (Exception e){
        persistString(TOKEN, TOKENACCESS, "60");
        return 60;
      }
    }
    return 0;
  }

  @Override
  public JSObject getExtConfig(String name) {
    try {
      return getPersistedJSObject("extConfig", name);
    } catch (Exception e) {
      logger.warn("Cannot load ExtConfig: " + name, e);
    }
    return null;
  }

  @Override
  public void setExtConfig(String name, String value) {
    try {
      persistString("extConfig" , name, value);
    } catch (Exception e) {
      logger.warn("Cannot persist extConfig: " + name, e);
    }
//    return jsLists;
  }

  @Override
  public void setExtConfig(String name, JSObject value) {
    try {
      persistJSObject("extConfig" , name, value);
    } catch (Exception e) {
      logger.warn("Cannot persist extConfig: " + name, e);
    }
//    return jsLists;
  }

}
