package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.schema.ReportDownloadType;

import javax.persistence.Entity;

/**
 * Created by james on 2017/09/05.
 */
@Entity
public class ReportDownloadObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(ReportDownloadObject.class);

  private String reportSpace;
  private String reportKey;
  private String reportGuid;
  private String reportDownloadType;

  public ReportDownloadObject(String uuid) {
    super(uuid);
    this.reportDownloadType = ReportDownloadType.Update.toString();
  }

  public ReportDownloadObject(String uuid, String reportSpace, String reportKey, String reportGuid) {
    this(uuid, reportSpace, reportKey, reportGuid, ReportDownloadType.Update.toString());
  }
  
  public ReportDownloadObject(String uuid, String reportSpace, String reportKey, String reportGuid, String reportDownloadType) {
    super(uuid);
    this.reportSpace = reportSpace;
    this.reportKey = reportKey;
    this.reportGuid = reportGuid;
    this.reportDownloadType = reportDownloadType;
  }
  
  public ReportDownloadObject(String uuid, ReportObject reportObject) {
    this(uuid, reportObject, reportObject.getReportDownloadType());
  }

  public ReportDownloadObject(String uuid, ReportObject reportObject, ReportDownloadType reportDownloadType) {
    super(uuid);
    this.reportSpace = reportObject.getReportSpace();
    this.reportKey = reportObject.getReportKey();
    this.reportGuid = reportObject.getGUID();
    this.reportDownloadType = reportDownloadType!=null?reportDownloadType.toString():ReportDownloadType.Update.toString();
  }


  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public String getReportGuid() {
    return reportGuid;
  }

  public void setReportGuid(String reportGuid) {
    this.reportGuid = reportGuid;
  }
  
  public String getDownloadType() {
    return reportDownloadType;
  }
  
  public void setDownloadType(String reportDownloadType) {
    this.reportDownloadType = reportDownloadType;
  }

  public void setReportDownloadType(String reportDownloadType) {
    this.reportDownloadType = reportDownloadType;
  }

  public void setReportDownloadType(ReportDownloadType reportDownloadType) {
    this.reportDownloadType = reportDownloadType.toString();
  }
  public ReportDownloadType getReportDownloadType() {
    return ReportDownloadType.fromString(reportDownloadType);
  }
}
