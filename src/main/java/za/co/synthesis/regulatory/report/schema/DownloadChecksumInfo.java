package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.ChecksumData;
import za.co.synthesis.regulatory.report.persist.DocumentData;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({
        "SyncPoint"
        , "Checksums"
})
public class DownloadChecksumInfo {
  private String syncPoint;
  private List<ChecksumInfo> checksums;

  public DownloadChecksumInfo(String syncPoint, List<ChecksumInfo> checksums) {
    this.syncPoint = syncPoint;
    this.checksums = checksums;
  }

  @JsonProperty("SyncPoint")
  public String getSyncPoint() {
    return syncPoint;
  }

  @JsonProperty("Checksums")
  public List<ChecksumInfo> getChecksums() {
    return checksums;
  }

  public static List<ChecksumInfo> wrapChecksumDataList(List<ChecksumData> checksums){
    List<ChecksumInfo> checksumList = new ArrayList<>();
    for (ChecksumData checksum: checksums){
      checksumList.add(new ChecksumInfo(checksum));
    }
    return checksumList;
  }
}
