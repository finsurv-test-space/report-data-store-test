package za.co.synthesis.regulatory.report.persist;

import java.util.List;

public class AccountEntryDownload {
    private final String upToSyncpoint;
    private final List<AccountEntryData> accountEntryList;

    public AccountEntryDownload(String upToSyncpoint, List<AccountEntryData> accountEntryList) {
        this.upToSyncpoint = upToSyncpoint;
        this.accountEntryList = accountEntryList;
    }

    public String getUpToSyncpoint() {
        return upToSyncpoint;
    }

    public List<AccountEntryData> getAccountEntryList() {
        return accountEntryList;
    }
}
