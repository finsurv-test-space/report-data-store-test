package za.co.synthesis.regulatory.report.dao;

/**
 * User: jake
 * Date: 1/18/16
 * Time: 3:03 AM
 * Specific column=level configuration for Jdbc
 */
public class JdbcExternalConfig {
  private String columnName;
  private boolean isDateFormat;

  public JdbcExternalConfig(String columnName, boolean dateFormat) {
    this.columnName = columnName;
    this.isDateFormat = dateFormat;
  }

  public String getColumnName() {
    return columnName;
  }

  public boolean isDateFormat() {
    return isDateFormat;
  }
}
