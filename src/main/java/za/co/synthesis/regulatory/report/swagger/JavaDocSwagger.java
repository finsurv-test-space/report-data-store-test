package za.co.synthesis.regulatory.report.swagger;

import com.sun.javadoc.*;
import za.co.synthesis.regulatory.report.validate.FreemarkerUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class JavaDocSwagger {
  public static void main(String[] args) {
    com.sun.tools.javadoc.Main.execute(new String[] {
            "-doclet",
            JavaDocSwagger.class.getName(),
            "-d",
            "./src/main/resources/docs",
            "-sourcepath",
            "./src/main/java",
            "za.co.synthesis.regulatory.report.rest"
    });
  }

  public static boolean start(RootDoc root) throws FileNotFoundException {
    String destDir = readDestinationDir(root.options());
    if (destDir == null)
      destDir = ".";

    PrintWriter pwFile = new PrintWriter(destDir + File.separator + "javadocs.json");

    writeContents(root.classes(), pwFile);

    pwFile.flush();
    pwFile.close();

    return true;
  }

  private static boolean hasRequestMapping(MethodDoc methodDoc) {
    for (AnnotationDesc annotationDesc : methodDoc.annotations()) {
      if (annotationDesc.annotationType().name().equals("RequestMapping")) {
        return true;
      }
    }
    return false;
  }

  private static void writeClass(ClassDoc classDoc, PrintWriter pw) {
    pw.write("  \"class\": ");
    pw.write('"');
    pw.write(classDoc.typeName());
    pw.write('"');
    pw.write(",\n");
    for (AnnotationDesc annotationDesc : classDoc.annotations()) {
      if (annotationDesc.annotationType().name().equals("RequestMapping")) {
        for (AnnotationDesc.ElementValuePair pair : annotationDesc.elementValues()) {
          if (pair.element().name().equals("value")) {
            pw.write("  \"mapping\": ");
            pw.write(pair.value().toString());
            pw.write(",\n");
          }
        }
      }
    }
    pw.write("  \"methods\": [\n");
    boolean bFirstMethod = true;
    for (MethodDoc methodDoc : classDoc.methods()) {
      if (hasRequestMapping(methodDoc)) {
        if (!bFirstMethod) {
          pw.write(",\n");
        } else {
          bFirstMethod = false;
        }
        pw.write("  {\n");
        // @RequestMapping(value = "/data/{schema}", method = RequestMethod.GET, produces = "application/json")
        for (AnnotationDesc annotationDesc : methodDoc.annotations()) {
          if (annotationDesc.annotationType().name().equals("RequestMapping")) {
            for (AnnotationDesc.ElementValuePair pair : annotationDesc.elementValues()) {
              if (pair.element().name().equals("value")) {
                pw.write("    \"mapping\": ");
                pw.write(pair.value().toString());
                pw.write(",\n");
              }
              if (pair.element().name().equals("method")) {
                pw.write("    \"method\": ");
                String method = pair.value().toString();
                if (method.endsWith("GET"))
                  pw.write("\"get\"");
                else if (method.endsWith("POST"))
                  pw.write("\"post\"");
                else if (method.endsWith("PUT"))
                  pw.write("\"put\"");
                else
                  pw.write("\"\"");
                pw.write(",\n");
              }
            }
          }
        }

        pw.write("    \"name\": ");
        pw.write('"');
        pw.write(methodDoc.qualifiedName());
        pw.write('"');
        pw.write(",\n");
        pw.write("    \"comment\": ");
        pw.write('"');
        pw.write(FreemarkerUtil.escapeJavaScript(methodDoc.commentText()));
        pw.write('"');
        pw.write(",\n");
        pw.write("    \"parameters\": [\n");
        boolean bFirstParam = true;
        for (ParamTag paramTag : methodDoc.paramTags()) {
          if (!bFirstParam) {
            pw.write(",\n");
          } else {
            bFirstParam = false;
          }
          pw.write("    {\n");
          pw.write("      \"name\": ");
          pw.write('"');
          pw.write(paramTag.parameterName());
          pw.write('"');
          pw.write(",\n");
          pw.write("      \"comment\": ");
          pw.write('"');
          pw.write(FreemarkerUtil.escapeJavaScript(paramTag.parameterComment()));
          pw.write('"');
          pw.write("\n");
          pw.write("    }");
        }
        pw.write("\n    ]\n");
        pw.write("  }");
      }
    }
    pw.write("\n  ]");
    pw.write("\n");
  }

  private static void writeContents(ClassDoc[] classes, PrintWriter pw) {
    pw.write("[\n");
    boolean bFirstClass = true;
    for (ClassDoc classDoc : classes) {
      if (!bFirstClass) {
        pw.write(",\n");
      }
      else {
        bFirstClass = false;
      }
      pw.write(" {\n");
      writeClass(classDoc, pw);
      pw.write(" }");
    }
    pw.write("\n]\n");
  }

  private static String readDestinationDir(String[][] options) {
    String value = null;
    for (int i = 0; i < options.length; i++) {
      String[] opt = options[i];
      if (opt[0].equals("-d")) {
        value = opt[1];
      }
    }
    return value;
  }

  public static int optionLength(String option) {
    if(option.equals("-d")) {
      return 2;
    }
    return 0;
  }

}
