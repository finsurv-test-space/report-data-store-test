package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.ChannelData;

/**
 * Created by jake on 5/24/17.
 */
@JsonPropertyOrder({
        "ChannelName"
        , "ReportSpace"
        , "DefaultSchema"
})
public class Channel {
  private final ChannelData channelData;

  public Channel() {
    channelData = new ChannelData();
  }

  public Channel(ChannelData channelData) {
    this.channelData = channelData;
  }

  @JsonProperty("ChannelName")
  public String getChannelName() {
    return channelData.getChannelName();
  }

  @JsonProperty("ReportSpace")
  public String getReportSpace() {
    return channelData.getReportSpace();
  }

  @JsonProperty("DefaultSchema")
  public String getDefaultSchema() {
    return channelData.getDefaultSchema();
  }

  public void setChannelName(String value) {
    channelData.setChannelName(value);
  }

  public void setReportSpace(String value) {
    channelData.setReportSpace(value);
  }

  public void setDefaultSchema(String value) {
    channelData.setDefaultSchema(value);
  }

  @JsonIgnore
  public ChannelData getChannelData() {
    return channelData;
  }
}
