package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.support.AccessType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/27/17.
 */
public class AccessSetData {
  private String name;
  private String reportSpace;
  private AccessType accessType;
  private List<CriterionData> accessCriteria = new ArrayList<CriterionData>();

  public AccessSetData() {
  }

  public AccessSetData(String name, AccessType accessType, String reportSpace) {
    this.name = name;
    this.accessType = accessType;
    this.reportSpace = reportSpace;
  }

  public String getName() {
    return name;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public AccessType getAccessType() {
    return accessType;
  }

  public List<CriterionData> getAccessCriteria() {
    return accessCriteria;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public void setAccessType(AccessType accessType) {
    this.accessType = accessType;
  }

  public void setAccessCriteria(List<CriterionData> accessCriteria) {
    this.accessCriteria = accessCriteria;
  }
}
