package za.co.synthesis.regulatory.report.persist;

public class TableConstant {
  public final static String Report = "Report";
  public final static String Document = "Document";
  public final static String DocumentAck = "DocumentAck";
  public final static String Decision = "Decision";
  public final static String DecisionLog = "DecisionLog";
  public final static String AccountEntry = "AccountEntry";
  public final static String Notification = "Notification";
  public final static String ReportDownload = "ReportDownload";
  public final static String DocumentDownload = "DocumentDownload";
  public final static String DecisionDownload = "DecisionDownload";
  public final static String AccountEntryDownload = "AccountEntryDownload";
  public final static String User = "User";
  public final static String AccessSet = "AccessSet";
  public final static String List = "List";
  public final static String ConfigDelQueue = "ConfigDelQueue";
  public final static String Checksum = "Checksum";
  public final static String ChecksumDownload = "ChecksumDownload";
}
