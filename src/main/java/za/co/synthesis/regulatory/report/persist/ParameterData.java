package za.co.synthesis.regulatory.report.persist;

public class ParameterData {
  private String name;
  private String path;

  public ParameterData(String name, String path) {
    this.name = name;
    this.path = path;
  }

  public String getName() {
    return name;
  }

  public String getPath() {
    return path;
  }
}
