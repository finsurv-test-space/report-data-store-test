package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.common.TableDefinition;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecord;
import za.co.synthesis.regulatory.report.persist.types.*;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.support.AccessType;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by james on 2017/08/23.
 */
public class MSSqlDataStore implements IDataStore{
  private static final Logger logger = LoggerFactory.getLogger(MSSqlDataStore.class);


  @Autowired
  IConfigStore configStore;

  /**
   * NOTE:
   * ----
   *
   * Currently we only need to search against the ReportObject.
   * As such, only one index table is actually required, and hence only one IIndexer instance.
   *
   * However, should we require separate indices for each Persisted type, then it could very well
   * mean that we would need a separate IIndexer instance for each PersistenceObject that is to be
   * searchable.
   *
   * Currently, the only time that any "Search" is performed is when Listing reports for a particular
   * user.  Then a search query is built up to return only those report objects that match the criteria
   * intersection of the user access criteria and the List access criteria.
   */
  @Autowired
  IIndexer indexer;

  private final Map<Class, ITablePersist> persistTables = new HashMap<Class, ITablePersist>();

  @Autowired
  DataLogic dataLogic;

  @Autowired
  JdbcTemplate jdbcTemplate;


  public void setIndexer(IIndexer indexer) {
    this.indexer = indexer;
  }

  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void setDataLogic(DataLogic dataLogic) {
    this.dataLogic = dataLogic;
  }

  public MSSqlDataStore() {
  }

  public static List<ITablePersist> getCommonPersistors(JdbcTemplate jdbcTemplate){
    List<ITablePersist> persistors = new ArrayList<>();
    Map<String, TableDefinition> definitions = TableDefinition.getCommonTableDefinitionMap();
    persistors.add(new MSSqlTablePersist<ReportObject>(definitions.get(TableConstant.Report), jdbcTemplate, ReportObject.class));
    persistors.add(new MSSqlTablePersist<DocumentObject>(definitions.get(TableConstant.Document), jdbcTemplate, DocumentObject.class));
    persistors.add(new MSSqlTablePersist<ChecksumObject>(definitions.get(TableConstant.Checksum), jdbcTemplate, ChecksumObject.class));
    persistors.add(new MSSqlTablePersist<DocumentAckObject>(definitions.get(TableConstant.DocumentAck), jdbcTemplate, DocumentAckObject.class));
    persistors.add(new MSSqlTablePersist<DecisionObject>(definitions.get(TableConstant.Decision), jdbcTemplate, DecisionObject.class));
    persistors.add(new MSSqlTablePersist<AccountEntryObject>(definitions.get(TableConstant.AccountEntry), jdbcTemplate, AccountEntryObject.class));

    persistors.add(new MSSqlTablePersist<NotificationObject>(definitions.get(TableConstant.Notification), jdbcTemplate, NotificationObject.class));

    persistors.add(new MSSqlTablePersist<ReportDownloadObject>(definitions.get(TableConstant.ReportDownload), jdbcTemplate, ReportDownloadObject.class));
    persistors.add(new MSSqlTablePersist<DocumentDownloadObject>(definitions.get(TableConstant.DocumentDownload), jdbcTemplate, DocumentDownloadObject.class));
    persistors.add(new MSSqlTablePersist<ChecksumDownloadObject>(definitions.get(TableConstant.ChecksumDownload), jdbcTemplate, ChecksumDownloadObject.class));
    persistors.add(new MSSqlTablePersist<DecisionDownloadObject>(definitions.get(TableConstant.DecisionDownload), jdbcTemplate, DecisionDownloadObject.class));
    persistors.add(new MSSqlTablePersist<AccountEntryDownloadObject>(definitions.get(TableConstant.AccountEntryDownload), jdbcTemplate, AccountEntryDownloadObject.class));

    persistors.add(new MSSqlTablePersist<UserObject>(definitions.get(TableConstant.User), jdbcTemplate, UserObject.class));
    persistors.add(new MSSqlTablePersist<AccessSetObject>(definitions.get(TableConstant.AccessSet), jdbcTemplate, AccessSetObject.class));
    persistors.add(new MSSqlTablePersist<ListObject>(definitions.get(TableConstant.List), jdbcTemplate, ListObject.class));
    return persistors;
  }

  public MSSqlDataStore(List<ITablePersist> persistors) {
    for (ITablePersist persistTable : persistors){
      persistTables.put(persistTable.getPersistClass(), persistTable);
    }
  }

  public void putPersistor(ITablePersist persistor) {
    persistTables.put(persistor.getClass(), persistor);
  }

  public void setPersistors(List<ITablePersist> persistors) {
    for (ITablePersist persistTable : persistors){
      persistTables.put(persistTable.getPersistClass(), persistTable);
    }
  }

  public void loadCommonPersistors(){
    if (jdbcTemplate!=null && (persistTables == null || persistTables.size()==0)){
      setPersistors(getCommonPersistors(jdbcTemplate));
    }
  }



//  public HighAvailabilityDataStore(String dbPath) {
//    this.entityManager = (new EntityManagerCreator(dbPath)).getEntityManager();
//  }


  public synchronized void writeObjectToPersistTable(Class objClass, PersistenceObject obj){
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(objClass);
      if (persistor != null) {
        try {
          persistor.write(obj);
          logger.trace("Persisted " + objClass.getName() +" without failure at MSSwlDataStore level.");
        } catch (PersistException e) {
          logger.error("Error persisting " + objClass.getName(), e);
        }
      }
    }
  }


  //-----------------------------------------------------------------------------------------------------------
  //  Data Store Methods
  //-----------------------------------------------------------------------------------------------------------
  @Override
  public synchronized void setEvaluationDecision(FullyReferencedEvaluationDecision evaluation){
    DecisionObject decision = new DecisionObject(PersistenceObject.generateUUID(), evaluation);
    DecisionDownloadObject decisionDownload = new DecisionDownloadObject(PersistenceObject.generateUUID(), decision);
    writeObjectToPersistTable(decision.getClass(), decision);
    writeObjectToPersistTable(decisionDownload.getClass(), decisionDownload);
  }

  @Override
  public synchronized FullyReferencedEvaluationDecision getEvaluationDecision(String reportSpace, String trnReference) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DecisionObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(trnReference);
        criterionMapList.put("reportKey",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<DecisionObject> objs = (List<DecisionObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(0).getDecision();
        }
      }
    }
    return null;
  }
  
  @Override
  public void setEvaluationDecisionLog(FullyReferencedEvaluationDecisions evaluations) {
    DecisionLogObject decisions = new DecisionLogObject(PersistenceObject.generateUUID(), evaluations);
    writeObjectToPersistTable(decisions.getClass(), decisions);
  }
  
  @Override
  public FullyReferencedEvaluationDecisions getEvaluationDecisionLog(String reportSpace, String trnReference) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DecisionLogObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(trnReference);
        criterionMapList.put("reportKey",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));
      
        List<DecisionLogObject> objs = (List<DecisionLogObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(objs.size() -1).getDecisions();
        }
      }
    }
    return null;
  }
  
  @Override
  public synchronized void setReport(String reportSpace, String trnReference, ReportInfo report, boolean addToDownload) {
    ReportObject reportObject = new ReportObject(PersistenceObject.generateUUID(), report);
    logger.trace("MSSqlDataStore assigning GUID "+reportObject.getGUID()+" to object "+reportSpace+":"+trnReference);
    writeObjectToPersistTable(reportObject.getClass(), reportObject);
    if (addToDownload) {
      //check what DownloadType is assigned and handle accordingly...
      ReportDownloadObject reportDownload = new ReportDownloadObject(PersistenceObject.generateUUID(), reportObject);
      logger.trace("MSSqlDataStore assigning GUID "+reportObject.getGUID()+" to download object "+reportSpace+":"+trnReference+" ("+report.getReportDataWithDownloadType().getDownloadType().toString()+")");
      writeObjectToPersistTable(reportDownload.getClass(), reportDownload);
    }

    //Create index entry - IF the indexer is available.
    if (indexer != null && dataLogic != null){
      IndexedRecord record = dataLogic.getIndexRecordForReportObject(reportObject);
      if (record != null) {
        String text = JsonSchemaTransform.jsonStrToFullTextValueStr(report.getReportData().getMeta());
        text += " " + JsonSchemaTransform.jsonStrToFullTextValueStr(report.getReportData().getReport());
        indexer.indexRecord(record, text);
        logger.trace("MSSqlDataStore indexing object "+reportSpace+":"+trnReference+" => "+record.serialize()+"\n"+text);
      }
    }
  }

  @Override
  public synchronized List<ReportInfo> getReportHistory(String reportSpace, String reportKey) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(reportKey);
        criterionMapList.put("reportKey",inValueList);

        //we will default the sort order of history items by (ascending) chronology...
        List<SortOrderField> sortBy = new ArrayList<>();
        sortBy.add(new SortOrderField(persistor.getTableDefinition().getSyncpointFieldName(), SortOrder.Ascending));

        List<ReportObject> objs = (List<ReportObject>)persistor.queryByMapList(criterionMapList, 0, 0, false, sortBy);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          try {
            List<ReportInfo> reportHistory = new ArrayList<ReportInfo>();
            for (ReportObject reportObject : objs) {
              reportHistory.add(reportObject.getReportData());
            }
            return reportHistory;
          } catch(Exception e){
            logger.error("Error while fetching ReportObject from MSSQL Data Store ("+reportSpace+", "+reportKey+"): "+e.getMessage(), e);
          }
        }
      }
    }
    return null;
  }

  @Override
  public synchronized ReportInfo getReport(String reportSpace, String reportKey) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(reportKey);
        criterionMapList.put("reportKey",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<ReportObject> objs = (List<ReportObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          try {
            return objs.get(0).getReportData();
          } catch(Exception e){
            logger.error("Error while fetching ReportObject from MSSQL Data Store ("+reportSpace+", "+reportKey+"): "+e.getMessage(), e);
          }
        }
      }
    }
    return null;
  }

  private synchronized List<ReportObject> getReportObjectList(String reportSpace, List<String> reportGuids, int maxResults, int page) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        criterionMapList.put("GUID",reportGuids);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        return (List<ReportObject>)persistor.queryByMapList(criterionMapList, maxResults, page, false, null);
      }
    }
    return null;
  }

  private synchronized List<DocumentObject> getDocumentObjectList(String reportSpace, List<DocumentDownloadObject> docDownloads) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DocumentObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        for (DocumentDownloadObject doc : docDownloads) {
          inValueList.add(doc.getDocumentGuid());
        }
        criterionMapList.put("GUID",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        return (List<DocumentObject>)persistor.queryByMapList(criterionMapList, 0, 0, true, null);
      }
    }
    return null;
  }

  private synchronized List<ChecksumObject> getChecksumObjectList(String reportSpace, List<ChecksumDownloadObject> checksumObjects) {
    List<ChecksumObject> result = null;
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ChecksumObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        inValueList = new ArrayList<String>();
        for (ChecksumDownloadObject checksumDownloadObject : checksumObjects) {
          inValueList.add(checksumDownloadObject.getValueDate());
        }
        criterionMapList.put("valueDate",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        result = (List<ChecksumObject>)persistor.queryByMapList(criterionMapList, 0, 0, true, null);
      }
    }
    return result;
  }

  @Override
  public synchronized DocumentData setDocumentContent(DocumentData document, FileData data) {
    return setDocument(document, data);
  }
  @Override
  public synchronized ChecksumData setChecksumContent(ChecksumData checksum) {
    return setChecksum(checksum);
  }

  @Override
  public synchronized FileData getDocumentContent(String reportSpace, String documentHandle) {
    DocumentObject doc = getDocumentObject(reportSpace, documentHandle);
    if (doc!=null){
      return doc.getData();
    }
    return null;
  }

  @Override
  public FileData getChecksumContent(String reportSpace, String valueDate) {
    ChecksumObject obj = getChecksumObject(reportSpace,valueDate);
    if(obj!=null){
      return obj.getData();
    }
    return null;
  }

  @Override
  public synchronized DocumentData getDocument(String reportSpace, String documentHandle) {
    DocumentObject doc = getDocumentObject(reportSpace, documentHandle);
    DocumentData documentData = null;
    if (doc!=null){
      documentData = doc.getDocumentData();
    }

    DocumentAckObject docAck = getDocumentAckObject(reportSpace, documentHandle);
    if (documentData == null){
      if (docAck == null) {
        documentData = dataLogic.getDocumentDataObjFromHandle(reportSpace, documentHandle);
      } else {
        documentData = docAck.getDocumentData();
      }
    } else {
      if (docAck != null) {
        documentData.setAcknowledged(docAck.isAcknowledged());
        documentData.setAcknowledgedComment(docAck.getAcknowledgedComment());
      }
    }
    return documentData;
  }

  @Override
  public synchronized ChecksumData getChecksum(String reportSpace, String valueDate) {
    ChecksumObject checksumObject = getChecksumObject(reportSpace, valueDate);
    ChecksumData checksumData = null;

    if (checksumObject!=null){
      checksumData = checksumObject.getChecksumData();
      try{
   //
      }catch (Exception e){
      }
    }

    return checksumData;
  }

  public synchronized DocumentObject getDocumentObject(String reportSpace, String documentHandle) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DocumentObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put(FieldConstant.ReportSpace,inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(documentHandle);
        criterionMapList.put(FieldConstant.DocumentHandle,inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<DocumentObject> objs = (List<DocumentObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(0);
        }
      }
    }
    return null;
  }

  public synchronized ChecksumObject getChecksumObject(String reportSpace, String valueDate) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ChecksumObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put(FieldConstant.ReportSpace,inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(valueDate);
        criterionMapList.put(FieldConstant.ValueDate,inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<ChecksumObject> objs = (List<ChecksumObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(0);
        }
      }
    }
    return null;
  }

  public synchronized DocumentAckObject getDocumentAckObject(String reportSpace, String documentHandle) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DocumentAckObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put(FieldConstant.ReportSpace,inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(documentHandle);
        criterionMapList.put(FieldConstant.DocumentHandle,inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<DocumentAckObject> objs = (List<DocumentAckObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(0);
        }
      }
    }
    return null;
  }

  @Override
  public synchronized DocumentData setDocument(DocumentData document, FileData data) {
    if (document != null) {
      ReportEventContext eventContext = null;
      DocumentObject doc = new DocumentObject(PersistenceObject.generateUUID(), document, data);
      writeObjectToPersistTable(doc.getClass(), doc);

      if (document.getAcknowledged()) {
        DocumentAckObject docAck = new DocumentAckObject(PersistenceObject.generateUUID(), document, eventContext);
        writeObjectToPersistTable(docAck.getClass(), docAck);
      }
        //...Add to download queue
      DocumentDownloadObject docDownload = new DocumentDownloadObject(PersistenceObject.generateUUID(), doc);
      writeObjectToPersistTable(docDownload.getClass(), docDownload);

//    return doc.getDocumentData();
      return getDocument(document.getReportSpace(), document.getDocumentHandle());
    }
    return null;
  }

  @Override
  public ChecksumData setChecksum(ChecksumData checksumData) {
    if(checksumData != null){
      ReportEventContext eventContext = null;
      ChecksumObject obj = new ChecksumObject(PersistenceObject.generateUUID(), checksumData);
      writeObjectToPersistTable(obj.getClass(), obj);

      //Add to download queue
      ChecksumDownloadObject checksumDownloadObject = new ChecksumDownloadObject(PersistenceObject.generateUUID(), obj);
      writeObjectToPersistTable(checksumDownloadObject.getClass(), checksumDownloadObject);

      return getChecksum(checksumData.getReportSpace(),checksumData.getValueDate());
    }
    return null;
  }

  @Override
  public boolean hasDocument(String reportSpace, String documentHandle) {
    return getDocumentObject(reportSpace, documentHandle) != null;
  }
  @Override
  public boolean hasChecksum(String reportSpace, String valueDate) {
    return getChecksumObject(reportSpace, valueDate) != null;
  }

  @Override
  public List<DocumentData> getDocumentsByReference(String reportSpace, String trnReference) {
    List<DocumentObject> docs = getDocumentObjectsForReport(reportSpace, trnReference);
    ArrayList<DocumentData> returnDocs = new ArrayList<>();
    for (DocumentObject doc : docs){
      returnDocs.add(doc.getDocumentData());
    }
    return returnDocs;
  }


  private synchronized List<DocumentObject> getDocumentObjectsForReport(String reportSpace, String reportKey) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DocumentObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put(FieldConstant.ReportSpace,inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(reportKey);
        criterionMapList.put(FieldConstant.ReportKey,inValueList);

        List<DocumentObject> allDocs = (List<DocumentObject>)persistor.queryByMapList(criterionMapList, 0, 0, true, null);
        if (allDocs != null && allDocs.size() > 0) {
          //ENSURE THE LATEST DOCUMENT VERSIONS ARE RETURNED
          //...ASSIGN TO MAP<[DOC_HANDLE], DOC>
          Map<String, DocumentObject> latestDocs = new HashMap<>();
          for (DocumentObject doc : allDocs) {
            latestDocs.put(doc.getDocumentHandle(), doc);
          }
          //CONVERT THE MAP BACK TO a LIST...
          allDocs.clear();
          for (DocumentObject doc : latestDocs.values()){
            allDocs.add(doc);
          }
        }
        return allDocs;
      }
    }
    return null;
  }



  @Override
  public synchronized ReportInfoDownload getReports(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    ReportInfoDownload result = null;
    String syncPoint;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportDownloadObject.class);
      if (persistor != null) {
        ITablePersist.PersistenceObjectList<ReportDownloadObject> reportDownloadObjects = (ITablePersist.PersistenceObjectList<ReportDownloadObject>)persistor.readFromSyncpoint(fromSyncPoint, reportSpace, maxRecords==null?100:maxRecords);

        if (reportDownloadObjects != null && reportDownloadObjects.getObjects() != null && reportDownloadObjects.getObjects().size() > 0) {
//          syncPoint = reportDownloadObjects.getObjects().get(reportDownloadObjects.getObjects().size() - 1).getObjectDbId()+"";
          syncPoint = reportDownloadObjects.getSyncpoint();
          List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
          List<String> reportGuids = new ArrayList<String>();
          Map<String, ReportDownloadObject> reportGuidDownloadMap = new HashMap<>();
          for (ReportDownloadObject reportDownloadObject : reportDownloadObjects.getObjects()){
            reportGuids.add(reportDownloadObject.getReportGuid());
            reportGuidDownloadMap.put(reportDownloadObject.getReportGuid(), reportDownloadObject);
          }
          List<ReportObject> objectList = getReportObjectList(reportSpace, reportGuids, maxRecords==null?100:maxRecords, 0);
          for (ReportObject reportObject : objectList){
            try {
              reportObject.setReportDownloadType(reportGuidDownloadMap.get(reportObject.getGUID()).getReportDownloadType());
              reportInfoList.add(reportObject.getReportData());
            } catch (Exception e){
              logger.error("Error extracting ReportInfo from ReportObject: "+e.getMessage(),e);
            }
          }
          if (syncPoint == null)
            syncPoint = fromSyncPoint;
          result = new ReportInfoDownload(syncPoint, reportInfoList);
        }
      }
    }
    return result;
  }

  @Override
  public synchronized DocumentDataDownload getDocuments(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    DocumentDataDownload result = null;
    String syncPoint;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DocumentDownloadObject.class);
      if (persistor != null) {
        ITablePersist.PersistenceObjectList<DocumentDownloadObject> documentDownloadObjects = (ITablePersist.PersistenceObjectList<DocumentDownloadObject>)persistor.readFromSyncpoint(fromSyncPoint, reportSpace,0);

        if (documentDownloadObjects != null && documentDownloadObjects.getObjects() != null && documentDownloadObjects.getObjects().size() > 0) {
//          syncPoint = documentDownloadObjects.getObjects().get(documentDownloadObjects.getObjects().size() - 1).getObjectDbId()+"";
          syncPoint = documentDownloadObjects.getSyncpoint();
          List<DocumentData> documentDataList = new ArrayList<DocumentData>();
          List<DocumentObject> docs = getDocumentObjectList(reportSpace, documentDownloadObjects.getObjects());
          for (DocumentObject doc : docs){
            DocumentData docData = getDocument(doc.getReportSpace(), doc.getDocumentHandle());
            documentDataList.add(docData);
          }
          result = new DocumentDataDownload(syncPoint, documentDataList);
        }
      }
    }
    return result;
  }

  @Override
  public synchronized DecisionDownload getDecisions(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    DecisionDownload result = null;
    String syncPoint;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DecisionDownloadObject.class);
      if (persistor != null) {
        ITablePersist.PersistenceObjectList<DecisionDownloadObject> decisionDownloadObjects = (ITablePersist.PersistenceObjectList<DecisionDownloadObject>)persistor.readFromSyncpoint(fromSyncPoint, reportSpace, 0);

        if (decisionDownloadObjects != null && decisionDownloadObjects.getObjects() != null && decisionDownloadObjects.getObjects().size() > 0) {
//          syncPoint = decisionDownloadObjects.getObjects().get(decisionDownloadObjects.getObjects().size() - 1).getObjectDbId()+"";
          syncPoint = decisionDownloadObjects.getSyncpoint();
          List<FullyReferencedEvaluationDecision> decisionList = new ArrayList<FullyReferencedEvaluationDecision>();
          List<String> decisionGuids = new ArrayList<String>();
          for (DecisionDownloadObject decisionDownloadObject : decisionDownloadObjects.getObjects()){
            decisionGuids.add(decisionDownloadObject.getDecisionGuid());
          }
          List<DecisionObject> objectList = getDecisionObjectList(reportSpace, decisionGuids, maxRecords==null?100:maxRecords, 0);
          for (DecisionObject decisionObject : objectList){
            decisionList.add(decisionObject.getDecision());
          }
          result = new DecisionDownload(syncPoint, decisionList);
        }
      }
    }
    return result;
  }

  private synchronized List<DecisionObject> getDecisionObjectList(String reportSpace, List<String> decisionGuids, Integer maxRecords, Integer page) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(DecisionObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("reportSpace",inValueList);
        criterionMapList.put("GUID",decisionGuids);
        return (List<DecisionObject>)persistor.queryByMapList(criterionMapList, maxRecords==null?100:maxRecords, page==null?0:page, true, null);
      }
    }
    return null;
  }



  private List<ReportInfo> getLatestReportsThatMatch(String reportSpace,
                                                            List<CriterionData> criteria,
                                                            Integer maxRecords,
                                                            Integer page) throws Exception {
    List<ReportInfo> matchedReportList = new ArrayList<>();

    //Add ReportSpace as a criterion...
    CriterionData reportSpaceCriterion = new CriterionData(CriterionType.System, "ReportSpace", reportSpace, "");
    criteria.add(reportSpaceCriterion);

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportObject.class);
      if (persistor != null) {

//        List<ReportObject> objs = (List<ReportObject>) persistor.searchRecords(criteria, maxRecords, page, true, null);
        List<ReportObject> objs = (List<ReportObject>) persistor.searchRecords(criteria, maxRecords, page, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          try {
            for (ReportObject reportObject : objs) {
              matchedReportList.add(reportObject.getReportData());
            }
            return matchedReportList;
          } catch (Exception e) {
            logger.error("Error while querying latest ReportObjects from MSSQL Data Store (" + reportSpace + "): " + e.getMessage(), e);
          }
        }
      }
    }
    return matchedReportList;
  }


  @Deprecated
  private Map<String, ReportInfo> getUnsortedLatestReportsThatMatch(String reportSpace,
                                                            List<CriterionData> criteria,
                                                            Integer maxRecords,
                                                            Integer page) throws Exception {
    Map<String, ReportInfo> matchedReportList = null;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ReportObject.class);
      if (persistor != null) {

        List<ReportObject> objs = (List<ReportObject>) persistor.searchRecords(criteria, maxRecords, page, true, null);

        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          try {
            Map<String, ReportInfo> filterMap = new HashMap<>();
            //De-dup the list, leaving only the latest items...  (use a map)
            for (ReportObject reportObject : objs) {
              filterMap.put(reportObject.getReportKey(), reportObject.getReportData());
            }
            return filterMap;
          } catch (Exception e) {
            logger.error("Error while querying latest ReportObjects from MSSQL Data Store (" + reportSpace + "): " + e.getMessage(), e);
          }
        }

      }
    }
    return null;
  }


  public List<ReportInfo> getReportMapFilteredByUserAndList(String reportSpace,
                                                                   ListData listDefinition,
                                                                   Map<String, String> searchMap,
                                                                   UserAccessData userAccessData,
                                                                   Integer maxRecords,
                                                                   Integer page) throws Exception {
    List<ReportInfo> filteredReportList = new ArrayList<>();

    for (AccessSetData accessSetData : userAccessData.getAccessList()) {
      //Assign the relevant access type to the reports?
      boolean writable = accessSetData.getAccessType().equals(AccessType.Write);

      List<CriterionData> criteria = new ArrayList<CriterionData>();
      criteria.addAll(accessSetData.getAccessCriteria());

      for (CriterionData crit : listDefinition.getCriteria()) {
        Pattern searchParamPattern = Pattern.compile("\\$\\{SEARCH:([^}]*)}");
        String searchParam = null;
        if (crit.getValue() != null) {
          Matcher searchParamMatcher = searchParamPattern.matcher(crit.getValue());
          while (searchParamMatcher.find()) {
            searchParam = searchParamMatcher.group(1);
            logger.trace("\tSearch param supported: {}", searchParam);
          }
  
          if (searchParam != null) {
            if (searchMap.containsKey(searchParam)) {
              CriterionData newCrit = new CriterionData(crit.getType(), crit.getName(), searchMap.get(searchParam), "SUPPORTED_SEARCH_PARAM");
              criteria.add(newCrit);
            }
          } else {
            criteria.add(crit);
          }
        }
      }

      //Add FullText Search params...
      String searchKey = "FULL_TEXT_SEARCH";
      if (searchMap.containsKey(searchKey)) {
        CriterionData newCrit = new CriterionData(CriterionType.System, searchKey, searchMap.get(searchKey), "SEARCH");
        criteria.add(newCrit);
      }

      //Add ContainsText Search params...
      searchKey = "CONTAINS_TEXT_SEARCH";
      if (searchMap.containsKey(searchKey)) {
        ArrayList<String> containsTextList = new ArrayList<String>();
        for (String containsStr : searchMap.get(searchKey).split("\\s\\|,\\|\\s")) {
          containsTextList.add(containsStr);
        }
        CriterionData newCrit = new CriterionData(CriterionType.System, searchKey, containsTextList, "SEARCH");
        criteria.add(newCrit);
      }

      filteredReportList = getLatestReportsThatMatch(reportSpace, criteria, maxRecords==null?100:maxRecords, page==null?0:page);

    }
    return filteredReportList;
  }


  @Override
  public ListResult getReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    List<ReportInfo> mainReportList = null;
    try {
      mainReportList = getReportMapFilteredByUserAndList(reportSpace, listDefinition, searchMap, userAccessData, maxRecords==null?100:maxRecords, page==null?0:page);
      return dataLogic.convertToListResult(listDefinition, mainReportList);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return dataLogic.convertToListResult(listDefinition, new ArrayList<ReportInfo>());
  }



  @Override
  public ListResult getOrderedReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, SortDefinition sortDefinition, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    List<ReportInfo> filteredReportList = null;
    try {
      filteredReportList = getReportMapFilteredByUserAndList(reportSpace, listDefinition, searchMap, userAccessData, maxRecords==null?100:maxRecords, page==null?0:page);
      SortedMap<SortingKeyList, ReportInfo> sortedMap = new TreeMap<SortingKeyList, ReportInfo>();
      for (ReportInfo report : filteredReportList) {
        SortingKeyList sortingKeys = new SortingKeyList(sortDefinition.getFields(), report);
        sortedMap.put(sortingKeys, report);
      }
      return dataLogic.convertToListResult(listDefinition, sortedMap.values());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return dataLogic.convertToListResult(listDefinition, new ArrayList<ReportInfo>());
  }


  //-----  Send Notification Support -----//
  public void addNotification(NotificationData notification) {
    NotificationObject notificationObject = new NotificationObject(
            notification.getReportSpace(), notification.getTrnReference(),
            notification.getQueueName(), notification.getContent());
    writeObjectToPersistTable(notificationObject.getClass(), notificationObject);
  }

  public List<NotificationData> getNotifications() {
    List<NotificationData> result = new ArrayList<NotificationData>();

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(NotificationObject.class);
      if (persistor != null) {
        List<NotificationObject> notificationObjects =
                persistor.readBySelector(100);

        for (NotificationObject notificationObject : notificationObjects){
          result.add(notificationObject.getNotification());
        }
      }
    }
    return result;
  }

  public void removeNotification(NotificationData notification) {
    StringBuilder sb = new StringBuilder();
    sb.append("UPDATE [").append(TableConstant.Notification).append("] SET [").append(FieldConstant.Status).append("] = 'S' ");
    sb.append("WHERE [").append(FieldConstant.GUID).append("] = '").append(notification.getGuid()).append("' ");
    jdbcTemplate.execute(sb.toString());
  }
  
  @Override
  public void changeReportKey(String reportSpace, String trnReference, String newTrnReference) {
    if (persistTables != null) {
      List<CriterionData> criteria = new ArrayList<>();

      CriterionData reportSpaceCriterion = new CriterionData();
      reportSpaceCriterion.setName(FieldConstant.ReportSpace);
      reportSpaceCriterion.setValue(reportSpace);
      criteria.add(reportSpaceCriterion);
      
      CriterionData reportKeyCriterion = new CriterionData();
      reportKeyCriterion.setName(FieldConstant.ReportKey);
      reportKeyCriterion.setValue(trnReference);
      criteria.add(reportKeyCriterion);
  
      CriterionData newReportKeyCriterion = new CriterionData();
      newReportKeyCriterion.setName(FieldConstant.ReportKey);
      newReportKeyCriterion.setValue(newTrnReference);
      
      CriterionData newDocumentHandleCriterion = new CriterionData();
      newDocumentHandleCriterion.setName(FieldConstant.DocumentHandle);
      newDocumentHandleCriterion.setValue("REPLACE(["+FieldConstant.DocumentHandle+"],'"+trnReference+"','"+newTrnReference+"')");
  
      List<CriterionData> newCriteria = new ArrayList<>();
      for (ITablePersist persistor : persistTables.values()) {
        if (persistor != null) {
          boolean hasReportSpace = false;
          boolean hasReportKey = false;
          boolean hasDocumentHandle = false;
          for (TableDefinition.Field field : persistor.getTableDefinition().getFields()) {
            if (field.getName().equalsIgnoreCase(FieldConstant.ReportSpace))
              hasReportSpace = true;
            if (field.getName().equalsIgnoreCase(FieldConstant.ReportKey))
              hasReportKey = true;
            if (field.getName().equalsIgnoreCase(FieldConstant.DocumentHandle))
              hasDocumentHandle = true;
          }
          if (hasReportSpace && (hasReportKey || hasDocumentHandle)){
            logger.trace("replacing "+(hasReportKey?"report key":"")+(hasReportKey && hasDocumentHandle?" and ":"")+(hasDocumentHandle?"document handle":"")+
                " in "+persistor.getPersistClass().getSimpleName()+" - for ref: "+trnReference+" => "+newTrnReference);
            newCriteria.clear();
            if(hasReportKey) {
              newCriteria.add(newReportKeyCriterion);
            }
            if(hasDocumentHandle) {
              newCriteria.add(newDocumentHandleCriterion);
            }
            persistor.updateCriteria(criteria, newCriteria);
          }
        }
      }
    }
  }

  @Override
  public synchronized void logActivity(LogActivityType activityType, LogReadWrite ioAction, String Url, String httpAction, String reportSpace, String trnReference, String logBody) {
    //TODO: Implement this DB log function.
  }


  private synchronized List<AccountEntryObject> getAccountEntryObjectList(String reportSpace, List<AccountEntryDownloadObject> accountEntryDownloads){
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(AccountEntryObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put("ReportSpace",inValueList);
        inValueList = new ArrayList<String>();
        for (AccountEntryDownloadObject account : accountEntryDownloads) {
          inValueList.add(account.getAccountEntryGuid());
        }
        criterionMapList.put("GUID",inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        return (List<AccountEntryObject>)persistor.queryByMapList(criterionMapList, 0, 0, false, null);
      }
    }
    return null;
  }

  public synchronized AccountEntryObject getAccountEntryObject(String reportSpace, String trnReference) {
    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(AccountEntryObject.class);
      if (persistor != null) {
        Map<String, List<String>> criterionMapList = new HashMap<>();
        List<String> inValueList = new ArrayList<String>();
        inValueList.add(reportSpace);
        criterionMapList.put(FieldConstant.ReportSpace,inValueList);
        inValueList = new ArrayList<String>();
        inValueList.add(trnReference);
        criterionMapList.put(FieldConstant.ReportKey,inValueList);

//        List<SortOrderField> sortBy = new ArrayList<>();
//        sortBy.add(new SortOrderField("TStamp", SortOrder.Descending));

        List<AccountEntryObject> objs = (List<AccountEntryObject>)persistor.queryByMapList(criterionMapList, 1, 0, true, null);
        if (objs != null && objs.size() > 0 && objs.get(0) != null) {
          return objs.get(0);
        }
      }
    }
    return null;
  }

  @Override
  public synchronized AccountEntryData getAccountEntry(String reportSpace, String reportKey) {
    AccountEntryObject accountEntry = getAccountEntryObject(reportSpace, reportKey);
    AccountEntryData accountEntryData = null;
    if (accountEntry!=null){
      accountEntryData = accountEntry.getAccountEntryData();
    }
    return accountEntryData;
  }


  public synchronized AccountEntryData setAccountEntry(String reportSpace, String reportKey, AccountEntryData data) {
    if (data != null) {
      AccountEntryObject obj = new AccountEntryObject(PersistenceObject.generateUUID(),data);
      writeObjectToPersistTable(obj.getClass(), obj);

      //...Add to download queue
      AccountEntryDownloadObject objDownload = new AccountEntryDownloadObject(PersistenceObject.generateUUID(),reportSpace,reportKey,obj);

      writeObjectToPersistTable(objDownload.getClass(), objDownload);

      return getAccountEntry(objDownload.getReportSpace(), objDownload.getReportKey());
    }
    return null;
  }

  @Override
  public AccountEntryDownload getAccountEntries(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    AccountEntryDownload result = null;
    String syncPoint;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(AccountEntryDownloadObject.class);
      if (persistor != null) {
        ITablePersist.PersistenceObjectList<AccountEntryDownloadObject> accountEntryDownloadObjects = (ITablePersist.PersistenceObjectList<AccountEntryDownloadObject>)persistor.readFromSyncpoint(fromSyncPoint, reportSpace,0);

        if (accountEntryDownloadObjects != null && accountEntryDownloadObjects.getObjects() != null && accountEntryDownloadObjects.getObjects().size() > 0) {
//          syncPoint = documentDownloadObjects.getObjects().get(documentDownloadObjects.getObjects().size() - 1).getObjectDbId()+"";
          syncPoint = accountEntryDownloadObjects.getSyncpoint();
          List<AccountEntryData> accountEntryDataList = new ArrayList<AccountEntryData>();
          List<AccountEntryObject> accountEntryObjects = getAccountEntryObjectList(reportSpace, accountEntryDownloadObjects.getObjects());
          for (AccountEntryObject obj : accountEntryObjects){
            AccountEntryData accountEntryData = getAccountEntry(obj.getReportSpace(), obj.getReportKey());
            accountEntryDataList.add(accountEntryData);
          }
          result = new AccountEntryDownload(syncPoint, accountEntryDataList);
        }
      }
    }
    return result;
  }

  @Override
  public synchronized ChecksumDataDownload getChecksums(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    ChecksumDataDownload result = null;
    String syncPoint;

    loadCommonPersistors();
    if (persistTables != null) {
      ITablePersist persistor = persistTables.get(ChecksumDownloadObject.class);
      if (persistor != null) {

        ITablePersist.PersistenceObjectList<ChecksumDownloadObject> checksumDownloadObjects = (ITablePersist.PersistenceObjectList<ChecksumDownloadObject>)persistor.readFromSyncpoint(fromSyncPoint, reportSpace,0);

        if (checksumDownloadObjects != null && checksumDownloadObjects.getObjects() != null && checksumDownloadObjects.getObjects().size()>0) {
//          syncPoint = documentDownloadObjects.getObjects().get(documentDownloadObjects.getObjects().size() - 1).getObjectDbId()+"";
          syncPoint = checksumDownloadObjects.getSyncpoint();
          List<ChecksumData> checksumDataList = new ArrayList<ChecksumData>();
          List<ChecksumObject> checksumObjects = getChecksumObjectList(reportSpace, checksumDownloadObjects.getObjects());
          for (ChecksumObject doc : checksumObjects){
            ChecksumData docData = getChecksum(doc.getReportSpace(), doc.getValueDate());
            checksumDataList.add(docData);
          }
          return result = new ChecksumDataDownload(syncPoint, checksumDataList);
        }
      }
    }
    return result;
  }
}
