package za.co.synthesis.regulatory.report.persist.types;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.schema.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

public class DecisionLogObject extends PersistenceObject {
  
  private static class DecisionLogContent {
    public List<EvaluationDecision> EvaluationDecisions;
//    public za.co.synthesis.regulatory.report.schema.EvaluationRequest EvaluationRequest;
  }
  
  
  private static final Logger logger = LoggerFactory.getLogger(DecisionLogObject.class);
  private FullyReferencedEvaluationDecisions decisions;
  private String reportSpace;
  private String reportKey; //trnReference
  private Map<String, Object> eventContext;
  private Integer version = 1;


  public DecisionLogObject(String uuid) {
    super(uuid);
  }
  
  public DecisionLogObject(String uuid, FullyReferencedEvaluationDecision decision) {
    super(uuid);
    this.decisions = new FullyReferencedEvaluationDecisions(decision);
  }
  
  public DecisionLogObject(String uuid, FullyReferencedEvaluationDecisions decisions) {
    super(uuid);
    this.decisions = decisions;
  }
  
  public DecisionLogObject(String uuid, DecisionObject decision) {
    super(uuid);
    this.decisions = new FullyReferencedEvaluationDecisions(decision.getDecision());
  }
  
  
  
  
  public FullyReferencedEvaluationDecisions getDecisions() {
    return this.decisions;
  }
  
  public String getUuid(){
    return super.getUuid();
  }
  
  public String getReportSpace() {
    return reportSpace;
  }
  
  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }
  
  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }
  
  public String getReportKey() {
    return reportKey;
  }
  
  public String getTrnReference() {
    return reportKey;
  }
  
  public Integer getVersion() {
    return version;
  }
  
  public void setVersion(Integer version) {
    this.version = version;
  }
  
  public String getAuthToken() {
    if (eventContext == null)
      eventContext = composeEventContext();
    if (eventContext != null)
      return mapToJsonStr(eventContext);
    else
      return null;
  }
  
  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }
  
  public String getContent(){
    ObjectMapper mapper = new ObjectMapper();
    DecisionLogContent decisionContent = new DecisionLogContent();
//    decisionContent.EvaluationRequest = decisions.getParameters();
    decisionContent.EvaluationDecisions = decisions.getEvaluations();
    try {
      String json = mapper.writeValueAsString(decisionContent);
      return json;
    } catch (JsonProcessingException e) {
      logger.error("Unable to serialize json content for DecisionLog object: " + e.getMessage(), e);
    }
    return null;
  }
  
  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      ObjectMapper mapper = new ObjectMapper();
      try {
        DecisionLogContent decisionContent = mapper.readValue(contentJson, DecisionLogContent.class);
        decisions = new FullyReferencedEvaluationDecisions(reportSpace, reportKey);
//        decisions.setParameters(decisionContent.EvaluationRequest);
        decisions.setEvaluations(decisionContent.EvaluationDecisions);
      } catch (IOException e) {
        logger.error("Unable to deserialize json content for DecisionLog object: " + e.getMessage(), e);
      }
    }
  }
  
}
