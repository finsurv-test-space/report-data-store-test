package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.ChecksumData;

@JsonPropertyOrder({
        "ValueDate"
        , "FileName"
        , "FileType"
        , "Content"
})
public class ChecksumInfo extends ChecksumData {

  public ChecksumInfo(ChecksumData checksumData) {
    this(checksumData.getValueDate(),
            checksumData.getReportSpace());
    setFilename(checksumData.getFileName());
    setFileType(checksumData.getFileType());
    setContent(checksumData.getContent());
  }

  public ChecksumInfo(String valueDate, String reportSpace) {
    super(valueDate, reportSpace);
  }

  //---------------------//
  //-----  GETTERS  -----//
  //---------------------//

  @JsonProperty("ValueDate")
  public String getValueDate() {
    return super.getValueDate();
  }

  @JsonProperty("ReportSpace")
  public String getReportSpace() {
    return super.getReportSpace();
  }

  @JsonProperty("FileName")
  public String getFileName() {
    return super.getFileName();
  }

  @JsonProperty("FileType")
  public String getFileType() {
    return super.getFileType();
  }

  @JsonProperty("Content")
  public byte[] getContent() { return super.getContent(); }


  //---------------------//
  //-----  SETTERS  -----//
  //---------------------//

  public void setValueDate(String valueDate) {
    super.setValueDate(valueDate);
  }

  public void setFilename(String fileName){
    super.setFilename(fileName);
  }

  public void setFileType(String fileType){
    super.setFileType(fileType);
  }

  public void setContent(byte[] content) { super.setContent(content); }

}
