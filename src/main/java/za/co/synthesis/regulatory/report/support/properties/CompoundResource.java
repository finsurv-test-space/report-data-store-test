package za.co.synthesis.regulatory.report.support.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by jake on 3/17/17.
 */
public class CompoundResource extends AbstractResource implements ApplicationContextAware {
  static final Logger log = LoggerFactory.getLogger(CompoundResource.class);

  private final String path;
  private String envName;
  private String envParam = null;
  private ApplicationContext applicationContext;
  private Resource resource = null;
  private File file = null;

  //auto-crypt objects
  public static String encryptRegex = "\\{ENCRYPT\\:([^\\}]+)\\}"; //{ENCRYPT:...}  --- TO BE ENCRYPTED
  public static String cryptoRegex = "\\{CRYPTO\\:([^\\}]+)\\}"; //{CRYPTO:...}  --- ENCRYPTED VALUE
  public static String encryptStringPrefix = "{ENCRYPT:";
  public static String encryptStringSuffix = "}";
  public static String cryptoStringPrefix = "{CRYPTO:";
  public static String cryptoStringSuffix = "}";
  public static SettingsCrypto cryptoLib = SettingsCrypto.Standard();



  public CompoundResource(File file, String envName) {
    Assert.notNull(file, "File must not be null");
    this.file = file;
    this.path = StringUtils.cleanPath(file.getPath());
    this.envName = resolveEnvironmentName(envName);
  }

  public CompoundResource(String path, String envName) {
    this.path = path;
    this.envName = resolveEnvironmentName(envName);

    if (path.startsWith("file:")) {
      encryptCompoundResource(new File(path.substring(5)));
    }
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
    this.resource = applicationContext.getResource(path);
    if (this.resource == null)
      this.file = new File(path);
    try {
      // see if the environment is registered, otherwise default
      CompoundProperties compoundProperties = CompoundProperties.loadFromPath(applicationContext, path);
      compoundProperties.getPropertiesForEnvironment(envName);
    } catch (PropertyException e) {
      envName = "default";
      System.setProperty(envParam, envName);
      log.error("Could not load property file: " + path, e);
    }
  }

  private String resolveEnvironmentName(String envName) {
    if (envName.startsWith("${")) {
      envParam = envName.substring(2, envName.length()-1);

      envName = System.getProperty(envParam);
      if (envName == null) {
        envName = System.getProperty("user.name");
        System.setProperty(envParam, envName);
      }
    }

    return envName;
  }

  private InputStream getPropertiesAsStream(String envName) throws IOException {
    try {
      CompoundProperties compoundProperties = CompoundProperties.loadFromPath(applicationContext, path);

      Properties props = compoundProperties.getPropertiesForEnvironment(envName);

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      props.store(baos, null);
      return new ByteArrayInputStream(baos.toByteArray());
    } catch (PropertyException e) {
      throw new IOException("Error retrieving properties as a stream", e);
    }
  }

  private Properties getEnvironmentProperties(String envName) throws IOException {
    try {
      CompoundProperties compoundProperties = CompoundProperties.loadFromPath(applicationContext, path);
      return compoundProperties.getPropertiesForEnvironment(envName);
    } catch (PropertyException e) {
      throw new IOException("Error retrieving properties from file: " + path, e);
    }
  }

  private Properties getEnvironmentParameters(String envName) throws IOException {
    try {
      CompoundProperties compoundProperties = CompoundProperties.loadFromPath(applicationContext, path);
      return compoundProperties.getParametersForEnvironment(envName);
    } catch (PropertyException e) {
      throw new IOException("Error retrieving parameters from file: " + path, e);
    }
  }

  public final String getPath() {
    return this.path;
  }

  public boolean exists() {
    if (resource != null)
      return true;

    return file.exists();
  }

  public boolean isReadable() {
    if (resource != null)
      return true;

    return file.canRead() && !file.isDirectory();
  }

  public synchronized InputStream getInputStream() throws IOException {
    return getPropertiesAsStream(envName);
  }

  public synchronized Properties getEnvironmentParameters() throws IOException {
    return getEnvironmentParameters(envName);
  }

  public synchronized Properties getEnvironmentProperties() throws IOException {
    return getEnvironmentProperties(envName);
  }

  public URL getURL() throws IOException {
    if (resource != null)
      throw new FileNotFoundException(this.getDescription() + " cannot be resolved to URL");

    return file.toURI().toURL();
  }

  public URI getURI() throws IOException {
    if (resource != null)
      throw new FileNotFoundException(this.getDescription() + " cannot be resolved to URI");

    return file.toURI();
  }

  public File getFile() {
    return this.file;
  }

  public Resource createRelative(String relativePath) {
    String pathToUse = StringUtils.applyRelativePath(this.path, relativePath);
    return new CompoundResource(pathToUse, envName);
  }

  public String getFilename() {
    if (resource != null)
      return path;
    return this.file.getName();
  }

  public String getDescription() {
    if (resource != null)
      return "path [" + path + "]";
    return "file [" + this.file.getAbsolutePath() + "]";
  }

  public boolean isWritable() {
    if (resource != null)
      return false;
    return this.file.canWrite() && !this.file.isDirectory();
  }

  public boolean equals(Object obj) {
    return obj == this || obj instanceof CompoundResource && this.path.equals(((CompoundResource)obj).path);
  }

  public int hashCode() {
    return this.path.hashCode();
  }


  //-----  CRYPTO FUNCTIONS  -----//

  public static void encryptCompoundResource(File file) {
    try{
      String fileContents = new Scanner(file).useDelimiter("\\Z").next();
      String encryptedContents = fileContents + "";
      while (encryptedContents.indexOf(encryptStringPrefix) > 0){
        String cryptoRaw = encryptedContents.substring(encryptedContents.indexOf(encryptStringPrefix) + encryptStringPrefix.length());
        cryptoRaw = cryptoRaw.substring(0, cryptoRaw.indexOf(encryptStringSuffix));
        String cryptoStr = cryptoLib.encrypt(cryptoRaw);
//        System.out.println("Encrypted \"" + cryptoRaw + "\" -> \"" + cryptoStr + "\"");
        encryptedContents = encryptedContents.replaceAll(regexEsc(encryptStringPrefix) + cryptoRaw + regexEsc(encryptStringSuffix), cryptoStringPrefix + cryptoStr + cryptoStringSuffix);
      }

      //If the file contents were secured then update the file on disk...
      if (!encryptedContents.equalsIgnoreCase(fileContents)) {
        //write the encrypted contents to file...
        try {
          file.delete();
        } catch (Exception err) {
          log.error("Unable to delete properties file for secure overwrite: {}", file.getAbsoluteFile(), err);
        }
        if (!file.exists()) {
          file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter cryptoOut = new BufferedWriter(fw);
        cryptoOut.write(encryptedContents);
        cryptoOut.flush();
        cryptoOut.close();
        log.info("Properties file secured with encryption on specified fields.");
      } else {
        log.info("No properties to secure with encryption. \n" +
                "To encrypt any value within the file, wrap the raw value (denoted as [VALUE]) in the following manner: \n" +
                "\""+encryptStringPrefix+"[VALUE]"+encryptStringSuffix+"\"");
      }

    } catch (Exception error){
      log.error("Exception while trying to secure properties file: {}", error);
    }
  }

  public static String regexEsc(String str) {
    return str.replaceAll("\\{","\\\\{").replaceAll(":", "\\\\:").replaceAll("}","\\\\}");
  }
}

