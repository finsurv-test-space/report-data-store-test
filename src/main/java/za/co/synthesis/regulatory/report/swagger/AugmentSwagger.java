package za.co.synthesis.regulatory.report.swagger;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.io.Reader;
import java.util.Map;

/**
 * Created by jake on 6/3/17.
 */
public class AugmentSwagger {
  private JSObject swaggerDoc;

  public void load(Reader reader) throws Exception {
    swaggerDoc = JSReaderUtil.loadReaderAsJSObject(reader);
  }

  public void mergeTypeIntoSwagger(String name, JSObject schema) {
    JSObject definitions = (JSObject) swaggerDoc.get("definitions");

    if (!definitions.containsKey(name)) {
      definitions.put(name, schema);
    }
  }

  public void mergeDefinitions(Reader reader) throws Exception {
    JSObject doc = JSReaderUtil.loadReaderAsJSObject(reader);

    if (doc != null) {
      JSObject definitions = (JSObject) doc.get("definitions");

      for (Map.Entry<String, Object> entry : definitions.entrySet()) {
        mergeTypeIntoSwagger(entry.getKey(), (JSObject) entry.getValue());
      }
    }
  }

  public void mergeDefinitions(JSObject doc) throws Exception {
    if (doc != null) {
      JSObject definitions = (JSObject) doc.get("definitions");

      for (Map.Entry<String, Object> entry : definitions.entrySet()) {
        mergeTypeIntoSwagger(entry.getKey(), (JSObject) entry.getValue());
      }
    }
  }

  public void bindInParamToType(String pathname, String verb, String paramName, String type) {
    SwaggerUtils.bindInParamToType(swaggerDoc, pathname, verb, paramName, type);
  }

  public void bindResponseToType(String pathname, String verb, String type) {
    SwaggerUtils.bindResponseToType(swaggerDoc, pathname, verb, type);
  }

  public void extendCallBySchema(String pathname, String verb, Map<String,Map<String,String>> schemaTypeMap) {
    extendCallBySchema(pathname, verb, schemaTypeMap, false);
  }

  public void extendCallBySchema(String pathname, String verb, Map<String,Map<String,String>> schemaTypeMap, boolean removeOriginalPath) {
    SwaggerUtils.extendCallBySchema(swaggerDoc, pathname, verb, schemaTypeMap, removeOriginalPath);
  }

  public void enrichSwaggerDocs(Map<String, JavaDocPath> javaDocPathMap) {
    SwaggerUtils.enrichSwaggerDocs(swaggerDoc, javaDocPathMap);
  }

  public void stripPaths(String pathStartsWith) {
    SwaggerUtils.stripPaths(swaggerDoc, pathStartsWith);
  }

  public void setSwaggerHost(String host) {
    swaggerDoc.put("host", host);
  }

  public void setSwaggerBasePath(String basePath) {
    swaggerDoc.put("basePath", basePath);
  }

  public String toString() {
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent(" ");
    writer.setNewline("\n");
    swaggerDoc.compose(writer);
    return writer.toString();
  }

  public void stripPostPaths(String pathStartsWith) {
    SwaggerUtils.stripPostPaths(swaggerDoc, pathStartsWith);
  }
}