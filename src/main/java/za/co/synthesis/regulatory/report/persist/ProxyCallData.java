package za.co.synthesis.regulatory.report.persist;

import java.util.List;

public class ProxyCallData {
  private final String name;
  private final List<ParameterData> parameters;
  private final RestData rest;
  private final String compose;

  public ProxyCallData(String name, List<ParameterData> parameters, RestData rest, String compose) {
    this.name = name;
    this.parameters = parameters;
    this.rest = rest;
    this.compose = compose;
  }

  public String getName() {
    return name;
  }

  public List<ParameterData> getParameters() {
    return parameters;
  }

  public RestData getRest() {
    return rest;
  }

  public String getCompose() {
    return compose;
  }
}
