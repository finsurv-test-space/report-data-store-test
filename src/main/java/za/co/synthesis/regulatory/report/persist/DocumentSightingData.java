package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.rule.core.FlowType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jake on 7/29/17.
 */
public class DocumentSightingData {
  private String id;
  private String documentTypeName;
  private final List<String> channels;
  private final List<FlowType> flows;
  private final List<String> categories;

  public DocumentSightingData() {
    this.channels = new ArrayList<String>();
    this.flows = new ArrayList<FlowType>();
    this.categories = new ArrayList<String>();
  }

  public DocumentSightingData(String id, String documentTypeName, List<String> channels, List<FlowType> flows, List<String> categories) {
    this.id = id;
    this.documentTypeName = documentTypeName;
    this.channels = channels;
    this.flows = flows;
    this.categories = categories;
  }

  public String getId() {
    return id;
  }

  public String getDocumentTypeName() {
    return documentTypeName;
  }

  public List<String> getChannels() {
    return channels;
  }

  public List<FlowType> getFlows() {
    return flows;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setDocumentTypeName(String documentTypeName) {
    this.documentTypeName = documentTypeName;
  }
}
