package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:21 PM
 * An implementation of the SBValidate_ReversalTrnRef function. Needs to raise the following warning and error:
 * Warning "W01": "Reversal currency is different from the original transaction. Ensure reversal amount is reasonable"
 * Error "E01": "Reveral amount is greater than the amount on the original transaction sequence"
 * Error "E02": "Original transaction is a reversal. Cannot reverse a reversal"
 *
 * This needs three parameters (We expect this to run on the field ReversalTrnRefNumber - value):
 * + Currency
 * + Sequence no
 * + Amount on the sequence
 */
public class SBValidate_ReversalTrnRef implements ICustomValidate {
  static class TranInfo {
    private boolean exists;
    private boolean reversal;
    private String currency;
    private BigDecimal amount;

    public TranInfo(boolean exists, boolean reversal, String currency, BigDecimal amount) {
      this.exists = exists;
      this.reversal = reversal;
      this.currency = currency;
      this.amount = amount;
    }

    public boolean isExists() {
      return exists;
    }

    public String getCurrency() {
      return currency;
    }

    public BigDecimal getAmount() {
      return amount;
    }

    public boolean isReversal() {
      return reversal;
    }
  }

  private JdbcTemplate jdbcTemplate;

  public SBValidate_ReversalTrnRef(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  private TranInfo getTrnInfo(JdbcTemplate jdbcTemplate, String trnReference, Integer sequence) {
    /*
SELECT t.Currency, ma.ForeignValue
  FROM OR_Transaction t
  JOIN OR_MonetaryAmount ma ON ma.TrnReference = t.TrnReference
WHERE t.TrnReference = 'CTID:4642661' AND ma.SequenceNumber = 1
    */
    String sql =
           "SELECT t.CurrencyCode, ma.ForeignValue, ma.CategoryCode " +
           " FROM OR_Transaction t WITH (NOLOCK)" +
           " JOIN OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference" +
           " WHERE t.TrnReference = ? AND ma.SequenceNumber = ?" +
           " UNION ALL " +
           " SELECT t.CurrencyCode, ma.ForeignValue, ma.CategoryCode " +
           " FROM archive.OR_Transaction t WITH (NOLOCK)" +
           " JOIN archive.OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference" +
           " WHERE t.TrnReference = ? AND ma.SequenceNumber = ?";

    List<Map<String, Object>> listResultMap = jdbcTemplate.queryForList(sql, trnReference, sequence, trnReference, sequence);
    if (listResultMap.size() > 0) {
      Map<String, Object> row = listResultMap.get(0);
      String categoryCode = row.get("CategoryCode") == null ? "" : row.get("CategoryCode").toString();
      boolean reversal = categoryCode.endsWith("00");
      return new TranInfo(true, reversal,
              row.get("CurrencyCode") == null ? "" : row.get("CurrencyCode").toString(),
              new BigDecimal(row.get("ForeignValue").toString() == null ? "0.0" : row.get("ForeignValue").toString()));
    }
    else {
      return new TranInfo(false, false, null, null);
    }
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String reversalCurrency = null;
    Integer sequence = null;
    BigDecimal reversalAmount = new BigDecimal(0);
    if (otherInputs.length > 0) {
      reversalCurrency = otherInputs[0].toString();
//      if (reversalCurrency.length() > 1)
//        reversalCurrency = reversalCurrency.substring(0, 1);
    }
    if (otherInputs.length > 1 && otherInputs[1] != null) {
      String seqNum = otherInputs[1].toString();
      if (seqNum.length() > 0)
        sequence = Integer.parseInt(seqNum);
    }
    if (otherInputs.length > 2 && otherInputs[2] != null) {
      String strAmount = otherInputs[2].toString();
      if (strAmount.length() > 0)
        reversalAmount = new BigDecimal(strAmount);
    }

    String trnRef = value.toString();

    // call to find currency and amount of old transaction with trnRef and sequence
    TranInfo trnInfo = getTrnInfo(jdbcTemplate, trnRef, sequence);

    if (!trnInfo.isExists()) {
      // This is an error scenario, however we expect the error to be picked up by Validate_ReversalTrnRef
      return new CustomValidateResult(StatusType.Pass);
    }
    else
    if (trnInfo.isReversal()) {
      return new CustomValidateResult(StatusType.Fail, "E02", "Original transaction is a reversal. Cannot reverse a reversal");
    }
    else
    if (trnInfo.getCurrency().equals(reversalCurrency)) {
      BigDecimal diff = trnInfo.getAmount().subtract(reversalAmount);
      if (diff.floatValue() < 0)
        return new CustomValidateResult(StatusType.Fail, "E01", "Reversal amount is greater than the amount on the original transaction sequence");
      else
        return new CustomValidateResult(StatusType.Pass);
    }
    else {
      return new CustomValidateResult(StatusType.Error, "W01", "Reversal currency is different from the original transaction. Ensure reversal amount is reasonable");
    }
  }
}
