package za.co.synthesis.regulatory.report.schema;

public enum ChangeType {
  Added,
  Removed,
  Updated
}

