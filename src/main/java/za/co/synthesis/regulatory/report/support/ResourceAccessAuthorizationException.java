package za.co.synthesis.regulatory.report.support;

/**
 * Created by james on 2017/08/02.
 */
public class ResourceAccessAuthorizationException extends Exception{
    public ResourceAccessAuthorizationException() {
        this("Access Denied - Insufficient rights to access the requested resource.");
    }

    public ResourceAccessAuthorizationException(String username, String rightRequired, String resourceDescription) {
        this("Access Denied - User "+username+
                " does not have the required "+rightRequired+
                " right to access "+resourceDescription+
                ".");
    }

    public ResourceAccessAuthorizationException(String message) {
        super(message);
    }
}
