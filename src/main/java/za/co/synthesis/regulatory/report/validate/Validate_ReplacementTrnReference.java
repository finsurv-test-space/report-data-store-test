package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:20 PM
 * A tXstream Finsurv direct DB implementation of the Validate_ReplacementTrnReference function.
 * Needs to raise the following SARB errors:
 * SARB ERROR: 389, "If ReplacementTransaction is N and the TrnReference is the same as a transaction previously cancelled, the ReplacementTransaction must be Y"
 * SARB ERROR: 212, "Transaction reference of cancelled transaction not stored on SARB database"
 * Our Error: 212, "ReplacementOriginalReference does not point to a existing transaction"
 * Our Error: 212, "ReplacementOriginalReference has not yet been cancelled (Original transaction must first be cancelled)"
 * <p>
 * Register: registerCustomValidate("Validate_ReplacementTrnReference",
 * new Validate_ReplacementTrnReference(...),  "TrnReference", "ReplacementTransaction", "ReplacementOriginalReference");
 */
public class Validate_ReplacementTrnReference implements ICustomValidate {
    static class TranInfo {
        private boolean exists;
        private boolean cancelled;

        public TranInfo(boolean exists, boolean cancelled) {
            this.exists = exists;
            this.cancelled = cancelled;
        }

        public boolean isExists() {
            return exists;
        }

        public boolean isCancelled() {
            return cancelled;
        }
    }

    private JdbcTemplate jdbcTemplate;

    public Validate_ReplacementTrnReference(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private TranInfo getTranInfo(JdbcTemplate jdbcTemplate, String trnReference) {
/*
WITH cteCancelled AS (
SELECT t.TrnReference
  FROM OR_Transaction t
  JOIN (OR_QueueEntry qeD
    JOIN OR_TransactionSnapshot tsD ON tsD.SnapshotID = qeD.SnapshotID
  ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D'
  LEFT JOIN (OR_QueueEntry qeA
    JOIN OR_TransactionSnapshot tsA ON tsA.SnapshotID = qeA.SnapshotID
  ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded
 WHERE qeA.QueueEntryID IS NULL
 )
SELECT tx.TrnReference, MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 THEN 'Y' WHEN c.TrnReference IS NOT NULL THEN 'Y' ELSE 'N' END) as Cancelled
  FROM OR_Transaction tx
  LEFT JOIN cteCancelled c ON c.TrnReference = tx.TrnReference
WHERE tx.TrnReference = 'CTID:4637212'
GROUP BY tx.TrnReference
*/
        String sql =
        "WITH cteCancelled AS (" +
        "SELECT t.TrnReference, CASE WHEN qeD.EntryStateID IN (4 /*Acknowledged*/, 5 /*AssumedAcknowledged*/) THEN 'Acked' ELSE 'Wait' END DeleteState" +
        " FROM OR_Transaction t WITH (NOLOCK)" +
        " JOIN (OR_QueueEntry qeD WITH (NOLOCK)" +
        "  JOIN OR_TransactionSnapshot tsD WITH (NOLOCK) ON tsD.SnapshotID = qeD.SnapshotID" +
        " ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D'" +
        " LEFT JOIN (OR_QueueEntry qeA WITH (NOLOCK)" +
        "  JOIN OR_TransactionSnapshot tsA WITH (NOLOCK) ON tsA.SnapshotID = qeA.SnapshotID" +
        " ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded " +
        "WHERE t.TrnReference = ? AND qeA.QueueEntryID IS NULL " +
        "UNION ALL \n" +
        " SELECT t.TrnReference, CASE WHEN qeD.EntryStateID IN (4 /*Acknowledged*/, 5 /*AssumedAcknowledged*/) THEN 'Acked' ELSE 'Wait' END DeleteState " +
        " FROM archive.OR_Transaction t WITH (NOLOCK)" +
        " JOIN (archive.OR_QueueEntry qeD WITH (NOLOCK) JOIN archive.OR_TransactionSnapshot tsD WITH (NOLOCK) ON tsD.SnapshotID = qeD.SnapshotID ) ON tsD.TrnReference = t.TrnReference AND qeD.AddDeleteFlag = 'D' \n" +
        " LEFT JOIN (archive.OR_QueueEntry qeA WITH (NOLOCK) JOIN archive.OR_TransactionSnapshot tsA WITH (NOLOCK) ON tsA.SnapshotID = qeA.SnapshotID ) ON tsA.TrnReference = t.TrnReference AND qeA.AddDeleteFlag = 'A' AND qeA.DateAdded >= qeD.DateAdded  \n" +
        " WHERE t.TrnReference = ? AND qeA.QueueEntryID IS NULL)" +
        "SELECT tx.TrnReference, MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 /*NotReportable*/ THEN 'Y' WHEN c.TrnReference IS NOT NULL AND c.DeleteState = 'Acked' THEN 'Y' ELSE 'N' END) as Cancelled" +
        " FROM OR_Transaction tx WITH (NOLOCK)" +
        " LEFT JOIN cteCancelled c WITH (NOLOCK) ON c.TrnReference = tx.TrnReference " +
        "WHERE tx.TrnReference = ? " +
        "GROUP BY tx.TrnReference  UNION ALL\n" +
        " SELECT tx.TrnReference, MAX(CASE WHEN c.TrnReference IS NULL AND tx.StateID = 8 /*NotReportable*/ THEN 'Y' WHEN c.TrnReference IS NOT NULL AND c.DeleteState = 'Acked' THEN 'Y' ELSE 'N' END) as Cancelled" +
        " FROM archive.OR_Transaction tx WITH (NOLOCK)\n" +
        " LEFT JOIN cteCancelled c WITH (NOLOCK) ON c.TrnReference = tx.TrnReference WHERE tx.TrnReference = ?  \n" +
         " GROUP BY tx.TrnReference";

        List<Map<String, Object>> listResultMap = jdbcTemplate.queryForList(sql, trnReference, trnReference, trnReference, trnReference);
        if (listResultMap.size() > 0) {
            if (listResultMap.get(0).get("Cancelled").equals("Y"))
                return new TranInfo(true, true);
            else
                return new TranInfo(true, false);
        } else {
            return new TranInfo(false, false);
        }
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {
        String trnRef = null;
        if (otherInputs.length > 0)
            trnRef = otherInputs[0] != null ? otherInputs[0].toString() : null;
        String replacementYN = null;
        if (otherInputs.length > 1)
            replacementYN = otherInputs[1] != null ? otherInputs[1].toString() : null;
        String replacementTrnRef = null;
        if (otherInputs.length > 2)
            replacementTrnRef = otherInputs[2] != null ? otherInputs[2].toString() : null;

        String errorCode;
        String errorDesc;
    /* Remove following code because tXstream manages the ReplacementTransaction value automatically
    if (replacementYN != null && replacementYN.equals("N")) {
      errorCode = "389";
      errorDesc = "If ReplacementTransaction is N and the TrnReference is the same as a transaction previously cancelled, the ReplacementTransaction must be Y";

      TranInfo trnInfo = getTranInfo(jdbcTemplate, trnRef);
      if (trnInfo.isExists() && trnInfo.isCancelled())
        return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
      else
        return new CustomValidateResult(StatusType.Pass);
    }
    else */
        if (replacementYN != null && replacementYN.equals("Y") && replacementTrnRef != null && replacementTrnRef.length() > 0) {
            TranInfo trnInfo = getTranInfo(jdbcTemplate, replacementTrnRef);
            if (!trnInfo.isExists()) {
                errorCode = "212";
                errorDesc = "ReplacementOriginalReference does not point to a existing transaction";
                return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
            } else {
                if (!trnInfo.isCancelled()) {
                    errorCode = "212";
                    errorDesc = "ReplacementOriginalReference has not yet been cancelled (Original transaction must first be cancelled)";
                    return new CustomValidateResult(StatusType.Fail, errorCode, errorDesc);
                } else
                    return new CustomValidateResult(StatusType.Pass);
            }
        }
        return new CustomValidateResult(StatusType.Pass);
    }
}
