package za.co.synthesis.regulatory.report.persist.types;

import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.ListData;

import javax.persistence.Entity;

/**
 * Created by james on 2017/10/02.
 */
@Entity
public class FieldObject {
  private String label;
  private CriterionType criterionType;
  private String name;

  public FieldObject() {
  }

  public FieldObject(String label, CriterionType criterionType, String name) {
    this.label = label;
    this.criterionType = criterionType;
    this.name = name;
  }

  public FieldObject(ListData.Field field) {
    this.label = field.getLabel();
    this.criterionType = field.getCriterionType();
    this.name = field.getName();
  }



  public String getLabel() {
    return label;
  }

  public CriterionType getCriterionType() {
    return criterionType;
  }

  public String getName() {
    return name;
  }

  public ListData.Field getField() {
    return new ListData.Field(this.label, this.criterionType, this.name);
  }
}
