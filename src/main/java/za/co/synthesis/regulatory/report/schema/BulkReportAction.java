package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;


@JsonPropertyOrder({
    "Actions"
})
public class BulkReportAction {
  private List<ReportAction> actions = new ArrayList<>();

  public BulkReportAction(List<ReportAction> actions) {
    this.actions = actions;
  }

  public BulkReportAction(){}

  @JsonProperty("Actions")
  public List<ReportAction> getActions() {
    return actions;
  }

  public void setActions(List<ReportAction> actions) {
    this.actions = actions;
  }

}
