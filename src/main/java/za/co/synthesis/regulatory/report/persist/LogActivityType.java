package za.co.synthesis.regulatory.report.persist;

public enum LogActivityType {
  USER("USER"),
  SYSTEM("SYSTEM"),
  ERROR("ERROR"),
  EVENT("EVENT"),
  INFO("INFO");
  
  private String val;

  LogActivityType(String val){
    this.val = val;
  }

  public String toString(){
    return this.val;
  }

  public static LogActivityType fromString(String activityType){
    for (LogActivityType enumVal : LogActivityType.values()){
      if (enumVal.toString().equalsIgnoreCase(activityType)){
        return enumVal;
      }
    }
    return LogActivityType.USER;
  }
}
