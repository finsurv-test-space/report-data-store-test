package za.co.synthesis.regulatory.report.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by jake on 8/3/17.
 */
public class RefreshRulesCacheTask implements Callable<Boolean> {
  final static Log log = LogFactory.getLog(RefreshRulesCacheTask.class);

  private final List<String> channelNames;
  private final RulesCache rulesCache;

  public RefreshRulesCacheTask(List<String> channelNames, RulesCache rulesCache) {
    this.channelNames = channelNames;
    this.rulesCache = rulesCache;
  }

  @Override
  public Boolean call() throws Exception {
    Boolean result = true;

    for (String channelName : channelNames) {
      try {
        PackageCache.Entry entry;
        entry = rulesCache.getEvaluationRules(channelName, PackageCache.CacheStrategy.GetFromSource);
        if (entry == null) {
          result = false;
          log.error("Error refreshing EvaluationRules for '" + channelName + "'. Cache may be out of date.");
        }
        entry = rulesCache.getValidationRules(channelName, PackageCache.CacheStrategy.GetFromSource);
        if (entry == null) {
          result = false;
          log.error("Error refreshing Validation for '" + channelName + "'. Cache may be out of date.");
        }
        entry = rulesCache.getDocumentRules(channelName, PackageCache.CacheStrategy.GetFromSource);
        if (entry == null) {
          result = false;
          log.error("Error refreshing Document for '" + channelName + "'. Cache may be out of date.");
        }
        entry = rulesCache.getLookups(channelName, PackageCache.CacheStrategy.GetFromSource);
        if (entry == null) {
          result = false;
          log.error("Error refreshing Lookups for '" + channelName + "'. Cache may be out of date.");
        }
      }
      catch (Exception e) {
        log.error("Error retrieving cache for '" + channelName + "'", e);
      }
    }
    return result;
  }
}
