package za.co.synthesis.regulatory.report.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.UserData;
import za.co.synthesis.regulatory.report.schema.StoredKey;

import java.security.Key;
import java.util.*;


public class JWTConsumer {
  private static final Logger logger = LoggerFactory.getLogger(JWTConsumer.class);


  public static class JwtTokenException extends Exception {
    public JwtTokenException(String message) {
      super(message);
    }
  }

  public final static List<String> externalSubjects = new ArrayList<String>();

  public static List<String> getExternalSubjects() {
    return externalSubjects;
  }

  public static void setExternalSubjects(List<String> newExternalSubjects) {
    externalSubjects.clear();
    externalSubjects.addAll(newExternalSubjects);
  }


  public static boolean useFileAndUrlFriendlyBase64 = true;

  public static boolean getUseFileAndUrlFriendlyBase64() {
    return useFileAndUrlFriendlyBase64;
  }

  public static void setUseFileAndUrlFriendlyBase64(boolean useFileAndUrlFriendlyBase64) {
    JWTProvider.useFileAndUrlFriendlyBase64 = useFileAndUrlFriendlyBase64;
  }

  private static boolean reservedKeyword(String keyword) {
    if (keyword.equals(Claims.ISSUER))
      return true;
    if (keyword.equals(Claims.SUBJECT))
      return true;
    if (keyword.equals(Claims.AUDIENCE))
      return true;
    if (keyword.equals(Claims.EXPIRATION))
      return true;
    if (keyword.equals(Claims.NOT_BEFORE))
      return true;
    if (keyword.equals(Claims.ISSUED_AT))
      return true;
    if (keyword.equals(Claims.ID))
      return true;
    if (keyword.equals(JWTProvider.TOKENUSE))
      return true;
    if (keyword.equals(JWTProvider.USER))
      return true;
    if (keyword.equals(JWTProvider.ACCESS))
      return true;
    if (keyword.equals(JWTProvider.RIGHTS))
      return true;
    return false;
  }

  private static class JWTKeyResolver implements SigningKeyResolver {

    private SystemInformation systemInformation;

    public JWTKeyResolver(SystemInformation systemInformation) {
      this.systemInformation = systemInformation;
    }

    public Key fetchKey(JwsHeader header) {
      String kid = header.getKeyId();
      StoredKey storedKey = null;

      try {
        storedKey = systemInformation.getStoredPublicKey(kid);
      } catch (Exception e) {
        logger.error("Error fetching key to use for JWT signature verification : \n" + kid, e);
      }
      if (kid != null && !kid.trim().isEmpty()) {
        if (storedKey == null) {
          Key key = externalKeyFetch(kid);
          if (key != null) {
            logger.debug("Public key retrieved from external source for KID: " + kid);
            try {
              systemInformation.setStoredKey(kid, key.getAlgorithm(), new String(key.getEncoded()), false);
              logger.debug("Public key added to DB for KID: " + kid);
            } catch (Exception err) {
              logger.error("Unable to add public key to DB for KID: " + kid, err);
            }
            return key;
          }
        } else {
          logger.debug("Public key retrieved from DB for KID: " + kid);
          return storedKey.getKey();
        }
      }
      return null;
    }

    //TODO: perhaps iterate over a list of potential key sources, configured in the properties files?
    public Key externalKeyFetch(String id) {
      Key key = null;
      if (id != null && !id.trim().isEmpty()) {
        //fetch and store the external key(s)...
        for (String source : systemInformation.getPublicKeySources()) {
          try {
            logger.debug("Attempting key (" + id + ") fetch from source: " + source);
            if (systemInformation.getPublicKeySource().fetchKey(source, id) != null) {
              logger.debug("Public key (" + id + ") retrieved from source: " + source);
              StoredKey storedKey = systemInformation.getStoredPublicKey(id);
              if (storedKey != null) {
                return storedKey.getKey();
              }
            }
          } catch (Exception e) {
            logger.debug("External public key (" + id + ") fetch failed from " + source + ": " + e.getMessage());
          }
        }
      }
      return key;
    }

    @Override
    public Key resolveSigningKey(JwsHeader header, Claims claims) {
      return fetchKey(header);
    }

    @Override
    public Key resolveSigningKey(JwsHeader header, String plaintext) {
      return fetchKey(header);
    }
  }


  public static String revertSignature(String encodedJwt) {
    //check for standard Base64 or Url-friendly base64
    boolean containsStdBase64Chars = encodedJwt.contains("+") || encodedJwt.contains("/");
    boolean containsUrlBase64Chars = encodedJwt.contains("-") || encodedJwt.contains("_");
    //TODO: CHECK IF WE NEED TO DO A CONVERSION AUTOMATICALLY, THEN APPLY AS NECESSARY.
    if (!useFileAndUrlFriendlyBase64) {
      int pos = encodedJwt.lastIndexOf('.') + 1;
      String base64UrlEncodedSignature = encodedJwt.substring(pos);
      byte[] signature = Base64.getDecoder().decode(base64UrlEncodedSignature);
      //TextCodec.BASE64URL.decode(base64UrlEncodedSignature); //is this just here to test the function - what purpose does it serve?
      return encodedJwt.substring(0, pos) + TextCodec.BASE64URL.encode(signature);
    }
    return encodedJwt;
  }


  public static JWTAuthToken consumeJWT(SystemInformation systemInformation, UserInformation userInformation, String jwtToken) throws Exception {
    return consumeJWT(systemInformation, userInformation, jwtToken, null, null, true);
  }

  public static JWTAuthToken consumeJWT(SystemInformation systemInformation, UserInformation userInformation, String jwtToken, String allowedExternalSubjects, String defaultTemplateUser, boolean useFileAndUrlFriendlyBase64) throws Exception {
    JWTConsumer.useFileAndUrlFriendlyBase64 = useFileAndUrlFriendlyBase64;
    jwtToken = revertSignature(jwtToken);
    JwtParser jwtParser = Jwts.parser();
    SigningKeyResolver signingKeyResolver = new JWTKeyResolver(systemInformation);
    jwtParser.setSigningKeyResolver(signingKeyResolver);
    boolean isSigned = jwtParser.isSigned(jwtToken) /* && sigKey != null*/;
    if (!isSigned) {
      throw new JwtTokenException("Token supplied is either unsigned or signed using a key which has not been published to this service.");
    }

    Jwt token = null;
    try {
      token = jwtParser
          .requireSubject(JWTProvider.subject)
          .parse(jwtToken);
    } catch (IncorrectClaimException e) {
      logger.error("Parsing JWT with default subject (" + JWTProvider.subject + ") unsuccessful.", e);
      //check if the subject is in the list of allowed external subjects (JWT Providers)...
      //if (allowedExternalSubjects == null || allowedExternalSubjects.isEmpty())
      //  allowedExternalSubjects = JWTProvider.subject;
      if (allowedExternalSubjects != null && !allowedExternalSubjects.isEmpty()) {
        for (String extSubject : allowedExternalSubjects.split("\\s*[,;]\\s*")) {
          try {
            logger.debug("Attempting JWT parse with allowed external subject: " + extSubject);
            token = jwtParser
                .requireSubject(extSubject)
                .parse(jwtToken);
            logger.debug("...external subject (" + extSubject + ") successfully parsed.");
          } catch (IncorrectClaimException e2) {
            logger.error("...external subject (" + extSubject + ") unsuccessful.", e2);
          }
        }
      }
      if (token == null) {
        throw e;
      }
    } catch (Exception e) {
      logger.error("Parsing JWT unsuccessful: " + e.getMessage(), e);
    }


    if (token != null) {
      Map<String, Object> payload = (Map) token.getBody();
      Object username = payload.get(JWTProvider.USER);
      Object claimedTemplateUser = payload.get(JWTProvider.TEMPLATE_USER);
      Object access = payload.get(JWTProvider.ACCESS);
      Object rights = payload.get(JWTProvider.RIGHTS);
      Object tokenUse = payload.get(JWTProvider.TOKENUSE);
      Object tokenIssuer = payload.get(JWTProvider.ISSUER);

      List accessList = new ArrayList<>();
      List rightsList = new ArrayList<>();
      List channelsList = new ArrayList<>();
      String[] rightsArray = null;
      Map<String, Object> other = new HashMap<String, Object>();

    /*
    See if:
    1) the specified user exists in the system - load rights etc  --  should we ??? ...think not.
    2) the specified template user exists in the system - load those rights etc  -- should we ???  ...think not.
    3) failing the above, check if the default template user exists and load those rights instead  ...much safer!
    4) IF any rights are provided in the JWT - APPEND/REPLACE those rights as needed  ... not the safest idea either.

    TODO: UPDATE THE TEMPLATE USERS TO BE SUBJECT-SPECIFIC :: IE: specific JWT Providers/systems can be assigned specific template users.  perhaps use freemarker templates?
     */
      //User Template Lookup...
      if (userInformation != null &&
          !((access instanceof List) || (rights instanceof List)) &&
          !((tokenIssuer instanceof String) && ((String) tokenIssuer).equalsIgnoreCase(JWTProvider.applicationName))
      ) {  //if we have no access lists or rights associated, then check if we need to map to a template user...
        String templateUsername = null;
        if (claimedTemplateUser instanceof String && !((String) claimedTemplateUser).isEmpty()) {
          templateUsername = (String) claimedTemplateUser;
        } else if (defaultTemplateUser instanceof String && !defaultTemplateUser.isEmpty()) {
          templateUsername = defaultTemplateUser;
        }
        if (templateUsername instanceof String && !templateUsername.isEmpty()) {
          if (userInformation.getUser(templateUsername) != null) {
            UserData data = userInformation.getUser(templateUsername);
            other.put(JWTProvider.TEMPLATE_USER, templateUsername);
            other.put(JWTProvider.USER, username);
            if (data != null) {
              List<String> templateAccess = data.getAccess();
              if (templateAccess != null && templateAccess.size() > 0) {
                accessList = templateAccess;
              }

              List<String> templateRights = data.getRights();
              if (templateRights != null && templateRights.size() > 0) {
                rightsList = templateRights;
              }


              List<String> templateChannels = data.getChannels();
              if (templateChannels != null && templateChannels.size() > 0) {
                channelsList = templateChannels;
              }
            }
          }
        }
      }


      if (access instanceof List)
        accessList.addAll((List) access);
      if (rights instanceof List) {
        rightsList.addAll((List) rights);
        rightsArray = new String[rightsList.size()];
        rightsArray = (String[]) rightsList.toArray(rightsArray);
      } else if (rightsList != null && rightsList.size() > 0) {
        rightsArray = new String[rightsList.size()];
        rightsArray = (String[]) rightsList.toArray(rightsArray);
      } else {
        rightsArray = new String[0];
      }

      for (Object key : payload.keySet()) {
        if (!reservedKeyword((String) key)) {
          other.put((String) key, payload.get(key));
        }
      }

      JWTAuthToken jwtAuthToken = new JWTAuthToken((String) username, accessList, other, AuthorityUtils.createAuthorityList(rightsArray), JWTAuthToken.TokenUse.fromString((String) tokenUse), channelsList);
      jwtAuthToken.setAuthenticated(isSigned);
      return jwtAuthToken;
    }
    return null;
  }


}
