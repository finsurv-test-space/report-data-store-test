package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.Entity;

/**
 * Created by jake on 6/13/17.
 */
@JsonPropertyOrder({
        //"ReportSpace",  /*TODO: ReportSpace should supercede the ReportingSpace field below*/
        "ReportingSpace" /*DEPRECATED NAMING CONVENTION*/
        , "TrnReference"
})
@Entity
public class FullyReferencedEvaluationDecision extends EvaluationDecision {
  private final String reportSpace;
  private final String trnReference;

  public FullyReferencedEvaluationDecision(String reportSpace, String trnReference) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
  }

  public FullyReferencedEvaluationDecision(String reportSpace, String trnReference, EvaluationDecision decision) {
    super(decision.getParameters(), decision.getEvaluation());
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
  }

  public FullyReferencedEvaluationDecision(String reportSpace, TrnReferencedEvaluationDecision decision) {
    super(decision.getParameters(), decision.getEvaluation());
    this.reportSpace = reportSpace;
    this.trnReference = decision.getTrnReference();
  }

  public FullyReferencedEvaluationDecision(String reportSpace, String trnReference, EvaluationRequest parameters, EvaluationResponse evaluation) {
    super(parameters, evaluation);
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
  }

  //@JsonProperty("ReportSpace")
  @JsonIgnore //TODO: This shouldn't be ignored!
  public String getReportSpace() {
    return reportSpace;
  }

  @Deprecated
  @JsonProperty("ReportingSpace")
  public String getReportingSpace() {
    return reportSpace;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }
}
