package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.history.ChangeUtils;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.rest.Decision;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/27/17.
 */
@JsonPropertyOrder({
        "ReportSpace",
        "TrnReference",
        "ChangeSets"
})
public class ReportDataHistory {
  private static final Logger logger = LoggerFactory.getLogger(ReportDataHistory.class);

  private final String reportSpace;
  private final String trnReference;
  private final List<ChangeSet> changeSets = new ArrayList<>();


  public ReportDataHistory(String reportSpace, String trnReference, List<ReportInfo> reports) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    try {
      ReportInfo ri_prev = null;
      for (ReportInfo ri : reports) { //assuming oldest first.

        List<ChangeItem> metaChanges = ChangeUtils.getMetaChanges(ri_prev, ri);
        List<ChangeItem> reportChanges = ChangeUtils.getReportChanges(ri_prev, ri);
        ChangeItem stateChange = ChangeUtils.getStateChange(ri_prev, ri);
        ReportData deepCopyReportData = ChangeUtils.deepCopy(ri.getReportData());
        ChangeUtils.addChangesToReportData(metaChanges, reportChanges, deepCopyReportData);
        ChangeSet changeSet = new ChangeSet(
                SerializationTools.serializeEventContext(ri.getEventContext()),
                reportChanges,
                metaChanges,
                stateChange,
                deepCopyReportData); //current snapshot.
        changeSets.add(changeSet);

        ri_prev = ri;
      }
    } catch (IOException e) {
      logger.error("Error composing report history for " + trnReference + "@" + reportSpace, e);
    }
  }

  @JsonProperty("ReportSpace")
  public String getReportSpace() {
    return reportSpace;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }

  @JsonProperty("ChangeSets")
  public List<ChangeSet> getChangeSets() {
    return changeSets;
  }
}

