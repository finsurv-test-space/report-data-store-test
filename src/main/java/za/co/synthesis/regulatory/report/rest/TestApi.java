package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.schema.EvaluationDecisions;
import za.co.synthesis.regulatory.report.schema.EvaluationRequest;
import za.co.synthesis.regulatory.report.schema.EvaluationResponse;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.evaluation.EvaluationTestUtilty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin
@Controller
@RequestMapping("test")
public class TestApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(TestApi.class);

  @Autowired
  private UserInformation userInformation;

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private EvaluationEngineFactory engineFactory;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private RulesCache rulesCache;

  @Autowired
  ApplicationContext ctx;

//-----------------------------------------------------------------------------------------------
//  Investec Test and Simulation APIs

  String SALESLOGIX_RESPONSE_TEMPLATE_PATH = "classpath:/samples/json/GetRolesByGCNSystemNameResult.json";
  String SALESLOGIX_EMPTY_RESPONSE_TEMPLATE_PATH = "classpath:/samples/json/GetRolesByGCNSystemNameEmptyResult.json";
  enum SalesLogixLookups {
    CATEGORY("category", "classpath:/samples/json/category_lookups_response.json")
    ;
    public String lookupName;
    public String templateFile;

    SalesLogixLookups(String lookupName, String templateFile){
      this.lookupName = lookupName;
      this.templateFile = templateFile;
    }

    public String getLookupName() {
      return lookupName;
    }

    public String getTemplateFile() {
      return templateFile;
    }

    public static SalesLogixLookups fromName(String name){
      for (SalesLogixLookups instance : SalesLogixLookups.values()) {
        if (instance.is(name)) {
          return instance;
        }
      }
      return null;
    }

    public boolean is(String name){
      return (this.lookupName.equalsIgnoreCase(name));
    }
  }


  /**
   * functions:
   * - GCN: this simulates the SSO/GCN login at investec.
   *        + if a GCN of 9999 is provided, an empty response is returned - to simulate unregistered GCN response.
   *            src\test\resources\samples\json\GetRolesByGCNSystemNameEmptyResult.json
   *        + if a GCN of 404 is provided, a 404 error is returned - to simulate saleslogix server being unavailable.
   *        + if a GCN of 500 is provided, a 500 error is returned - to simulate saleslogix internal error.
   *        ...any other value will return the static user template from:
   *            src\test\resources\samples\json\GetRolesByGCNSystemNameResult.json
   *
   * - LOOKUP: this simulates a lookup list request from the saleslogix server.
   *        + the lookup name will be used to illicit the relevant response file from the array populated.
   *
   */
  @RequestMapping(value = "/investec/{function}/{param}", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String getSalesLogixSim(
          @PathVariable String function,
          @PathVariable String param) throws Exception {
    if (function == null || ((!"LOOKUP".equalsIgnoreCase(function)) && (!"GCN".equalsIgnoreCase(function)))) {
      throw new ResourceNotFoundException("A valid function name is required ('LOOKUP', 'GCN').");
    }

    if (param == null) {
      throw new ResourceNotFoundException("A valid function param is required.");
    }

    //do lookup simulation
    if ("LOOKUP".equalsIgnoreCase(function)) {
      SalesLogixLookups lookup = SalesLogixLookups.fromName(param);
      if (lookup != null){
        try {
          return JsonSchemaTransform.mapToJsonStr(JSReaderUtil.loadResourceAsJSObject(ctx, lookup.templateFile));
        } catch (Exception e){
          //ignore.
        }
      }
    }

    if ("GCN".equalsIgnoreCase(function)) {
      if ("404".equals(param)) {
        throw new ResourceNotFoundException("SalesLogix Server not found simulation");
      }
      if ("500".equals(param)) {
        throw new Exception("SalesLogix Server internal error simulation");
      }
      if ("9999".equals(param)) {
        return JsonSchemaTransform.mapToJsonStr(JSReaderUtil.loadResourceAsJSObject(ctx, SALESLOGIX_EMPTY_RESPONSE_TEMPLATE_PATH));
      }

      return JsonSchemaTransform.mapToJsonStr(JSReaderUtil.loadResourceAsJSObject(ctx, SALESLOGIX_RESPONSE_TEMPLATE_PATH));
    }

    return JsonSchemaTransform.mapToJsonStr(new JSObject());
  }



//-----------------------------------------------------------------------------------------------
//  alBaraka Bank Test and Simulation APIs

//  String iMAL_BOP_TRAN_RESPONSE = "classpath:/samples/json/albaraka_iMAL_BOP_TRAN_RESPONSE.json";
//  String iMAL_THIRD_PARTY_CIF_RESPONSE = "classpath:/samples/json/albaraka_iMAL_THIRD_PARTY_CIF_RESPONSE_INDIVIDUAL.json";
  String iMAL_BOP_TRAN_RESPONSE = "samples/json/albaraka_iMAL_BOP_TRAN_RESPONSE.json";
  String iMAL_BOP_TRAN_RESPONSE_SPECIFIC = "samples/json/albaraka_iMAL_BOP_TRAN_RESPONSE_[MODULE]_[TRNREFERENCE].json";
  String iMAL_THIRD_PARTY_CIF_RESPONSE = "samples/json/albaraka_iMAL_THIRD_PARTY_CIF_RESPONSE_###.json";

  /**
   */
  @RequestMapping(value = "/albaraka/cif", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public @ResponseBody String doCifDetailsPOSTRequest(
                  HttpServletRequest request) throws Exception {
    return doCifFetch(request);
  }


  @RequestMapping(value = "/albaraka/cif", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public @ResponseBody String doCifDetailsGETRequest(
                  HttpServletRequest request) throws Exception {
    return doCifFetch(request);
  }


  public String doCifFetch(HttpServletRequest request) throws Exception {
    logger.info("...Albaraka CiF details request recieved.");
    int partNo = 0;
    String payload = "";
    String moduleName = "";
    String trnReference = "";
    Map<String, Object> jsonPayload = null;
    boolean thirdPartyCifRequest = false;
    //load the payload...
    if ("POST".equalsIgnoreCase(request.getMethod()))
    {
      payload = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
      if (payload instanceof String && payload.startsWith("{") && payload.endsWith("}")){
        jsonPayload = JsonSchemaTransform.jsonStrToMap(payload);
        if (jsonPayload.containsKey("asModuleName") && jsonPayload.get("asModuleName") instanceof String){
          moduleName = (String)jsonPayload.get("asModuleName");
        }
        if (jsonPayload.containsKey("asTrxReference") && jsonPayload.get("asTrxReference") instanceof String){
          trnReference = (String)jsonPayload.get("asTrxReference");
        }
      }
    }
    //load up any additional params...
    for (Map.Entry<String, String[]> param : request.getParameterMap().entrySet()){
      String paramName = param.getKey();
      if ("al_third_party_CIF".equalsIgnoreCase(paramName)){
        logger.info("  *** THIRD PARTY DATA REQUEST DETECTED: "+paramName+" ***");
        thirdPartyCifRequest=true;
      }
      String[] paramValues = param.getValue();
      logger.info("...param received: "+paramName);
      for (String value: paramValues){
        logger.info("\t\t"+value);
      }
      //fetch param values for specific test case loading...
      if ("trnReference".equalsIgnoreCase(paramName) && paramValues.length > 0){
        trnReference = paramValues[0];
      } else if ("module".equalsIgnoreCase(paramName) && paramValues.length > 0){
        moduleName = paramValues[0];
      }
    }


    /*
    for (Part part :  request.getParts()){
      String contentType = part.getContentType();
      String name = part.getName();

      StringBuilder stringBuilder = new StringBuilder();
      String line = null;

      try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(part.getInputStream(), Charsets.UTF_8))) {
        while ((line = bufferedReader.readLine()) != null) {
          stringBuilder.append(line);
        }
      }

      payload = stringBuilder.toString();

      logger.info("...parsing request part["+partNo+"] \""+name+"\" <"+contentType+">: \n\n"+payload+"\n\n");
      partNo++;
    }
    */
    String responseStr = "<RESPONSE_GONE_WRONG>Erm... ja.</RESPONSE_GONE_WRONG>";
    ClassLoader classLoader = this.getClass().getClassLoader();
    Random random = new Random();
    String randEntInd = (random.nextBoolean() ? "ENTITY": "INDIVIDUAL");
//    URL fileResourceUrl = classLoader.getResource(thirdPartyCifRequest?iMAL_THIRD_PARTY_CIF_RESPONSE.replaceAll("###", randEntInd):iMAL_BOP_TRAN_RESPONSE);
    if (thirdPartyCifRequest) {
      URL fileResourceUrl = classLoader.getResource(iMAL_THIRD_PARTY_CIF_RESPONSE.replaceAll("###", randEntInd));
      if (fileResourceUrl != null) {
        Path bytesPath = Paths.get(fileResourceUrl.toURI());
        if (bytesPath != null) {
          byte[] bytes = Files.readAllBytes(bytesPath);
          if (bytes != null) {
            responseStr = new String(bytes);
          }
        }
      }
    } else {
      try {
        //TRY LOAD THE SPECIFIC TEST CASE WITH THE GIVEN MODULE AND TRNREFERENCE
        URL fileResourceUrl = classLoader.getResource(iMAL_BOP_TRAN_RESPONSE_SPECIFIC.replaceAll("\\[MODULE]", moduleName).replaceAll("\\[TRNREFERENCE]", trnReference));
        if (fileResourceUrl != null) {
          Path bytesPath = Paths.get(fileResourceUrl.toURI());
          if (bytesPath != null) {
            byte[] bytes = Files.readAllBytes(bytesPath);
            if (bytes != null) {
              responseStr = new String(bytes);
            }
          }
        }
      } catch (Exception err){
        //LOAD GENERIC TEST CASE
        URL fileResourceUrl = classLoader.getResource(iMAL_BOP_TRAN_RESPONSE);
        if (fileResourceUrl != null) {
          Path bytesPath = Paths.get(fileResourceUrl.toURI());
          if (bytesPath != null) {
            byte[] bytes = Files.readAllBytes(bytesPath);
            if (bytes != null) {
              responseStr = new String(bytes);
            }
          }
        }
      }
    }

//    for (Map.Entry<String, String[]> param : request.getParameterMap().entrySet()) {
//       String paramName = param.getKey();
//       String paramValue = (param.getValue() != null && param.getValue().length > 0 && param.getValue()[0] instanceof String ? param.getValue()[0] : "null");
//       responseStr.replaceAll("(?i)\\\""+paramName+"\\\"\\s*\\:\\s*(\\\"[^\\\"]*\\\"|\\d+\\.?\\d*|null|{[^}]+})", paramValue);  //Very naive replacement of test values supplied by param.
//    }

    for (Map.Entry<String, String[]> param : request.getParameterMap().entrySet()) {
       String paramName = param.getKey();
       String paramValue = (param.getValue() != null && param.getValue().length > 0 && param.getValue()[0] instanceof String ? param.getValue()[0] : "null");
       responseStr.replaceAll("(?i)\\{"+paramName+"}", paramValue);
    }


//    if (payload == null){
//      File file = new File(getClass().getClassLoader().getResource(iMAL_CIF_MANAGEMENT_RESPONSE_PATH).getFile());
//    }
    
    if ("404".equals(payload)) {
      throw new ResourceNotFoundException("iMAL Server not found simulation");
    }
    if ("500".equals(payload)) {
      throw new Exception("iMAL Server internal error simulation");
    }
    
  
    return responseStr;
  }
  



//-----------------------------------------------------------------------------------------------
//  Standard Bank Test and Simulation APIs

  /**
   * Test verification end-point for SARB and AdInternal Auth reference numbers.
   * ...It is to be noted that this API spec has been banging around for months now with various minor changes being dictated - none of which conform
   * to the spec provided.  The latest of which is to stipulate that the number should not be prefixed with the "016" as previously mandated and
   * more importantly that the request make use of an HTTP GET instead of the previously updated POST.
   *
   * <b><u>Example GET Url:</u></b>
   * <code>http://10.147.129.134/services/compliance/exchange-control/200006/verify</code>
   * <br/>
   *
   * <b><u>Example Success Response:</u></b>
   * <code>
   *   <verifyExconResponse>
   *     <verificationSuccessful>YES</verificationSuccessful>
   *     <details>
   *       <BUYSELLIND>S</BUYSELLIND>
   *       <STATUS_CODE>EXP</STATUS_CODE>
   *       <PURPOSE_CODE>901</PURPOSE_CODE>
   *       <END_DATE>2016-08-30</END_DATE>
   *       <CURRENCY_CODE>USD</CURRENCY_CODE>
   *       <CURRENT_BALANCE>197163.23</CURRENT_BALANCE>
   *       <APPROX_IND>N</APPROX_IND>
   *       <APP_DATE>2016-02-11</APP_DATE>
   *       <VARIANCEPERC>10</VARIANCEPERC>
   *       <LOAN_REF/>
   *     </details>
   *   </verifyExconResponse>
   * </code>
   *
   * <b><u>Example Failure Response:</u></b>
   * <code>
   * <verifyExconResponse>
   *   <verificationSuccessful>No</verificationSuccessful>
   *   <reason>
   *     <number>99</number>
   *     <description>Only 016 Standard bank Application numbers can be verified</description>
   *   </reason>
   * </verifyExconResponse>
   * </code>
   *
   * <b><u>Example Failure Response:</u></b>
   * <code>
   *   <verifyExconResponse>
   *     <verificationSuccessful>No</verificationSuccessful>
   *     <reason>
   *       <number>413</number>
   *       <description>Application number 000006 not found on ARM_REPOSITORY.</description>
   *     </reason>
   *   </verifyExconResponse>
   * </code>
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
//  @RequestMapping(value = "/standardbank/arm/{authRef}", method = RequestMethod.POST, produces = "application/json")
  @RequestMapping(value = "/standardbank/arm/{authRef}/{responseDelay}", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
//  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public
  @ResponseBody
  String doArmVerify(
      @PathVariable String authRef,
      @PathVariable String responseDelay,
      HttpServletRequest request,
                  HttpServletResponse response) throws Exception {
    String errorResponse = "samples/xml/standardbank_arm_failure_response_";
    String successResponse = "samples/xml/standardbank_arm_success_response.xml";
    logger.info("...ARM verify request recieved.");
    String responseStr = null;
    Long hardDelay = null;
    try{
      hardDelay = Long.parseLong(responseDelay);
    } catch (Exception err){
      //dont care
    }
//    Thread.sleep(Math.round(Math.random()*500) +(hardDelay != null?hardDelay:0));
    long errorNo = (Math.round(Math.random()*11)+1);
    if (authRef != null) {
      //LOAD GENERIC TEST CASE
      ClassLoader classLoader = this.getClass().getClassLoader();
      URL fileResourceUrl = classLoader.getResource(authRef.startsWith("ERR") || authRef.startsWith("666") || authRef.matches("^(016)?422(\\d{4}(-\\d{2}){2})?$") ? errorResponse+(errorNo<=9?"0":"")+errorNo+".xml" : successResponse);
      if (fileResourceUrl != null) {
        Path bytesPath = Paths.get(fileResourceUrl.toURI());
        if (bytesPath != null) {
          byte[] bytes = Files.readAllBytes(bytesPath);
          if (bytes != null) {
            responseStr = new String(bytes);
            responseStr = responseStr.replaceAll("number 000006 not", "number "+authRef+" not");
          }
        }
      }
    }

    if ("404".equals(authRef) || "016404".equals(authRef) || "404".equals(authRef)) {
      throw new ResourceNotFoundException("ARM Server not found simulation");
//    } else if ("422".equals(authRef) || "016422".equals(authRef)) {
//      response.setStatus(422);
//      response.getWriter().println(responseStr);
//      response.getWriter().flush();
////      response.sendError(422, responseStr);
//      response.sendError(422);
//      throw new UnprocessableEntityException(responseStr);
    } else if ("500".equals(authRef) || "016500".equals(authRef) || "500".equals(authRef) || authRef == null) {
      throw new Exception("ARM Server internal error simulation");
    } else {
      response.setStatus(200);
    }

    return responseStr;
  }


  
  /**
   * Test verification end-point for IVS lookups.
   * <b><u>Example Post Url:</u></b>
   * <code>...</code>
   * <br/>
   *
   *
   * <b><u>Example Request:</u></b>
   * <code>
   * <IVS>
   *     <Test>1</Test>
   *     <Version>1</Version>
   *     <Import>
   *         <ImportVerification>
   *             <LineNumber>1</LineNumber>
   *             <DateTime>2018-08-29T10:21:55.521+02:00</DateTime>
   *             <BopSequenceNumber>000</BopSequenceNumber>
   *             <MRN>DBN201405275047591</MRN>
   *             <CustomsClientNumber>20440287</CustomsClientNumber>
   *             <TransportDocumentNumber>SUDUSUDUN44041928101</TransportDocumentNumber>
   *         </ImportVerification>
   *     </Import>
   * </IVS>
   * </code>
   * <br/>
   *
   * <b><u>Example Response:</u></b>
   * <code>
   * <IVS>
   *     <Test>0</Test>
   *     <Version>1</Version>
   *     <Import>
   *         <VerificationResponse>
   *             <LineNumber>1</LineNumber>
   *             <IVSReference>IVS016201808290000523580</IVSReference>
   *             <DateTime>2018-08-29T10:22:15</DateTime>
   *             <MRN>DBN201405275047591</MRN>
   *             <MRNConfirmationReference>CR201808290000342651</MRNConfirmationReference>
   *             <TransportDocumentNumber>SUDUSUDUN44041928101</TransportDocumentNumber>
   *         </VerificationResponse>
   *     </Import>
   * </IVS>
   * </code>

   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/standardbank/ivs/{mrn}", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String doIvsVerify(
      HttpServletRequest request,
      HttpServletResponse response,
      @PathVariable String mrn,  //MRN
      @RequestParam (required=false) String ln,  //LineNumber
      @RequestParam (required=false) String dt,  //DateTime
      @RequestParam (required=false) String seq, //BopSequenceNumber
      @RequestParam (required=false) String ccn, //CustomsClientNumber
      @RequestParam (required=false) String tdn  //TransportDocumentNumber
        ) throws Exception {
    String errorResponse = "samples/xml/standardbank_ivs_failure_response.xml";
    String successResponse = "samples/xml/standardbank_ivs_success_response.xml";
    logger.info("...IVS verify request received.");
    ln = (ln == null)?"1":ln;
    dt = (dt == null)?"2020-07-20T11:11:11.111":dt;
    seq = (seq == null)?"1":seq;
    ccn = (ccn == null)?"1234567":ccn;
    tdn = (tdn == null)?"12345":tdn;
    Thread.sleep(Math.round(Math.random()*15000));
    String responseStr = null;
    if ("203".equals(mrn) ||
            "204".equals(mrn) ||
            "210".equals(mrn) ||
            "400".equals(mrn) ||
            "401".equals(mrn) ||
            "408".equals(mrn) ||
            (mrn != null && mrn.startsWith("ERR"))) {
      //LOAD GENERIC TEST CASE
      ClassLoader classLoader = this.getClass().getClassLoader();
      URL fileResourceUrl = classLoader.getResource(errorResponse);
      if (fileResourceUrl != null) {
        Path bytesPath = Paths.get(fileResourceUrl.toURI());
        if (bytesPath != null) {
          byte[] bytes = Files.readAllBytes(bytesPath);
          if (bytes != null) {
            responseStr = new String(bytes);
            responseStr = responseStr.replaceAll("ErrorCode\\s?=\\\"\\d+\\\"", "ErrorCode=\""+mrn+"\"");
          }
        }
      }
    }

    if ("404".equals(mrn)) {
      throw new ResourceNotFoundException("IVS Server not found simulation");
    }
    if ("500".equals(mrn)) {
      throw new Exception("IVS Server internal error simulation");
    }

    if (responseStr == null){
      //LOAD GENERIC TEST CASE
      ClassLoader classLoader = this.getClass().getClassLoader();
      URL fileResourceUrl = classLoader.getResource(successResponse);
      if (fileResourceUrl != null) {
        Path bytesPath = Paths.get(fileResourceUrl.toURI());
        if (bytesPath != null) {
          byte[] bytes = Files.readAllBytes(bytesPath);
          if (bytes != null) {
            responseStr = new String(bytes);
            //responseStr = responseStr.replaceAll("ErrorCode\\s?=\\\"\\d+\\\"", "ErrorCode=\""+mrn+"\"");
          }
        }
      }
    }
  
    return responseStr;
  }
  
  
  
  
//-----------------------------------------------------------------------------------------------
//  SASFIN Test and Simulation APIs
  
  /*
    Sasfin / Andile: simulation public key end-point
    Headers:
      Server: nginx/1.10.3
      Date: Mon, 20 Aug 2018 09:34:43 GMT
      Content-type: application/xml
      Transfer-Encoding: chunked
      Connection: keep-alive
      
    Response:
      <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
      <loadTokenPublicKeyResponse xmlns:sec="https://tbcloud.io/security" xmlns:cred="https://tbcloud.io/security/credentials" xmlns:gl="https://tbcloud.io/generalledger" xmlns:fin="http://tbcloud.io/financial" xmlns:ns6="http://www.urdad.org/services" xmlns:ns7="http://tbcloud.io/bareturns/ba100200" xmlns:ns8="http://tbcloud.io/bareturns/apidatasets" xmlns:ns9="http://tbcloud.io/bareturns/goaml">
          <publicKey>-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuZebgpjHU8xr207+m4QJdE5GsafPolt8y7P2MHtAn0Z7/td1yLHBsWiaWGsYplVgaPDjb/Xgcz2MlWL9bVP66CWGHwW2aS8KNZt3QvdBkRjib6xPn1/o/oY4+P0Q++afCGIKJJLwagfEHHQWyi7OyTCSPXxWpsw96V8c+4WL1iXLxjSBeW5EeB7OTb425K02bu4eZpOiQFRIUakAUtvLPn5+0H3kePDtYUzfIptOhFx5zR0nynbTckzZEY9Cf3lh5rPMvmi8k8QsULcvOmXB+VgvFlJwaOoZGUQF7WRzCqOHpouo+NE3Xk1NM7z1rka/8F6MIh7+hT2Gk8rFazFqPwIDAQAB-----END PUBLIC KEY-----</publicKey>
          <id>a055a63c-7b36-4c35-8791-1b78eef6cb85</id>
      </loadTokenPublicKeyResponse>
   */
  @RequestMapping(value = "/sasfin/publicKey", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String getSasfinPublicKey(HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam (required=false) String keyId) throws Exception {
    logger.info("...Public key request recieved.");
    response.setHeader("Server","nginx/1.10.3");
    response.setHeader("Date","Mon, 20 Aug 2018 09:34:43 GMT");
    response.setHeader("Transfer-Encoding", "chunked");
    response.setHeader("Connection", "keep-alive");
    response.setContentType("application/xml");
    if (keyId == null) {
      keyId = UUID.randomUUID()+"";
    }
    return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<loadTokenPublicKeyResponse xmlns:sec=\"https://tbcloud.io/security\" xmlns:cred=\"https://tbcloud.io/security/credentials\" xmlns:gl=\"https://tbcloud.io/generalledger\" xmlns:fin=\"http://tbcloud.io/financial\" xmlns:ns6=\"http://www.urdad.org/services\" xmlns:ns7=\"http://tbcloud.io/bareturns/ba100200\" xmlns:ns8=\"http://tbcloud.io/bareturns/apidatasets\" xmlns:ns9=\"http://tbcloud.io/bareturns/goaml\">\n" +
        "    <publicKey>-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuZebgpjHU8xr207+m4QJdE5GsafPolt8y7P2MHtAn0Z7/td1yLHBsWiaWGsYplVgaPDjb/Xgcz2MlWL9bVP66CWGHwW2aS8KNZt3QvdBkRjib6xPn1/o/oY4+P0Q++afCGIKJJLwagfEHHQWyi7OyTCSPXxWpsw96V8c+4WL1iXLxjSBeW5EeB7OTb425K02bu4eZpOiQFRIUakAUtvLPn5+0H3kePDtYUzfIptOhFx5zR0nynbTckzZEY9Cf3lh5rPMvmi8k8QsULcvOmXB+VgvFlJwaOoZGUQF7WRzCqOHpouo+NE3Xk1NM7z1rka/8F6MIh7+hT2Gk8rFazFqPwIDAQAB-----END PUBLIC KEY-----</publicKey>\n" + //Andile key
        "    <algorithm>RSA</algorithm>\n" +
        "    <source>RDS</source>\n" +
        "    <expiry>2019-05-05 06:15:23</expiry>\n" +
        "    <id>"+keyId+"</id>\n" +
        "</loadTokenPublicKeyResponse>";
  }

  @RequestMapping(value = "/sasfin/jwt", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String getSasfinJwt(HttpServletRequest request,
                            HttpServletResponse response) throws Exception {
    logger.info("...Public key request recieved.");

    try {
      /*
      API URL:
      https://api.tst.sasfin.tbcloud.io/andile/security/authenticatecredentials

      PAYLOAD:
      <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<authenticateCredentialsRequest xmlns:sec="https://tbcloud.io/security" xmlns:cred="https://tbcloud.io/security/credentials" xmlns:ns8="http://www.urdad.org/services" xmlns:ns7="https://tbcloud.io/financial" xmlns:gl="https://tbcloud.io/generalledger" xmlns:ns9="http://tbcloud.io/bareturns/dailyloans" xmlns:agl="https://tbcloud.io/assembly/generalledger" xmlns:fin="http://tbcloud.io/financial">
    <credentials xsi:type="plainTextUsernameAndPassword" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <password>234589</password>
        <username>synthesis_api</username>
    </credentials>
</authenticateCredentialsRequest>
      */

      URL url = new URL("https://api.tst.sasfin.tbcloud.io/andile/security/authenticatecredentials");
      Map<String,Object> params = new LinkedHashMap<>();
      params.put("name", "Freddie the Fish");
      params.put("email", "fishie@seamail.example.com");
      params.put("reply_to_thread", 10394);
      params.put("message", "Shark attacks in Botany Bay have gotten out of control. " +
              "We need more defensive dolphins to protect the schools here, but " +
              "Mayor Porpoise is too busy stuffing his snout with lobsters. He's so shellfish.");


//      StringBuilder postData = new StringBuilder();
//      for (Map.Entry<String,Object> param : params.entrySet()) {
//        if (postData.length() != 0) postData.append('&');
//        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
//        postData.append('=');
//        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
//      }
//      byte[] postDataBytes = postData.toString().getBytes("UTF-8");

      byte[] payload = ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
              "<authenticateCredentialsRequest xmlns:sec=\"https://tbcloud.io/security\" xmlns:cred=\"https://tbcloud.io/security/credentials\" xmlns:ns8=\"http://www.urdad.org/services\" xmlns:ns7=\"https://tbcloud.io/financial\" xmlns:gl=\"https://tbcloud.io/generalledger\" xmlns:ns9=\"http://tbcloud.io/bareturns/dailyloans\" xmlns:agl=\"https://tbcloud.io/assembly/generalledger\" xmlns:fin=\"http://tbcloud.io/financial\">\n" +
              "    <credentials xsi:type=\"plainTextUsernameAndPassword\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
              "        <password>234589</password>\n" +
              "        <username>synthesis_api</username>\n" +
              "    </credentials>\n" +
              "</authenticateCredentialsRequest>").getBytes("UTF-8");

      HttpURLConnection conn = (HttpURLConnection)url.openConnection();
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
      conn.setRequestProperty("Content-Length", String.valueOf(payload.length));
      conn.setDoOutput(true);
//      conn.getOutputStream().write(postDataBytes);
      conn.getOutputStream().write(payload);

      Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

      for (int c; (c = in.read()) >= 0;)
        System.out.print((char)c);

    } catch (Exception e) {

      logger.error("unable to fetch a JWT from Andile.net directly - perhaps you need certificates setup for SSL or a whitelisted IP :: "+e.getMessage(), e);
    }

    return "eyJhbGciOiJSUzUxMiIsImtpZCI6ImUxNjE0YjhkLWNiNTQtNDhiYS05YTQ1LWVkYzBmMDRiZjQ1OCJ9.eyJzdWIiOiJyZXBvcnQtZGF0YS1zdG9yZSIsImlzcyI6InRiY2xvdWQuaW8iLCJwdWJsaWMta2V5IjoiLS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS1cbk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBajJ0S2JOcXNSSVZPTkdNbmo4S1ZZZUZRbk8zdzBER1BZRDB0Q2x0Kys5a0M5L2drYjFzRi9STTZYdFlEZW8vSXJBQWdwck84ajNkcldlbGoxNzlCNmhhYmZ3NTVaNEFGZWhyc3FjOVpBcDhsUVRDdFhhQkpNMStlRlVMN0NCaDFJbHZjU3dPYVFLMlF2VUkwVkgvZ3JQY2xDYmN3ZXdUVUxTTWt2d3JDTEtpV2ZmbGtMZGtCUEJrbW56NjY2c3hqci9TQ21KWmlSN0R2NFZUd0NrT1gvNDllbVF3clIvemZ2a2o1aXhUS25pK1JCMmVCRU05OWpNZHhJSGxsbHdhVmUrOHl0OVFOSU9URkY3cEMrRlRlait1azE5YktMVjVZbEJMRFQ2NFQxSS9qQW9QaFNPL1dUL2tkcW0yL2p5anYzWEVVZmlRbkxqQmdrOFBQcTZsTEd3SURBUUFCXG4tLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0iLCJleHAiOjE1NDA2MzgxMjAsInVzZXIiOiJhZG1pbiJ9.QCESbBLXFOh01vurvPT2xWThRqAGDX5TR0pX67mU646pR_vH-BaOwrwjyQ6WYotb0PmkNsm7s58AaPPK3yy3nFbrzLkWQCHEJlkjfmM5SkRVIuu5OpEa3GHfCfOtW1cC8yoVFugLJrjHK9UwEapa7vxgwEBWQlLNw3KhTl0uPCgVxiYuTJo7glDHEKs3TFR86toVJ5NmvpawTV8GFeCBO6PwoLKQI-BsYX_yA5ntMBfeBxh3TMRo_QJW0pUqjIogUEBA5xHwDkXuRb6gWBalPywB0mjGCngDpngtFedj9b-8TjMBzq6Sjit4u_kj8oPE99SxI22w26r1h-8EdW1SEw";
  }




//-----------------------------------------------------------------------------------------------
//  Generic Test and Simulation APIs

  @RequestMapping(value = "/notify", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String doNotify(HttpServletRequest request,
                  HttpServletResponse response) throws Exception {
    logger.info("...Notification recieved.");
    return JsonSchemaTransform.mapToJsonStr(new JSObject());
  }




//-----------------------------------------------------------------------------------------------
//  Completeness testing


  @RequestMapping(value = "/evaluation/completeness", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  JSObject doEvaluationCompletenessRun(HttpServletRequest request,
                  @RequestParam String channelName,
                  @RequestParam (required = false) Boolean filtered,
                  HttpServletResponse response) throws Exception {
    logger.info("...Evaluation completeness run requested.");
    filtered = (filtered == null ? false : filtered);

    Decision decisionApi = null;
    Evaluator evaluator = null;
    try {
      decisionApi = new Decision();
      evaluator = decisionApi.getEvaluator(systemInformation, channelName, engineFactory, logger);

    } catch (Exception e) {
      logger.error("...Unable to fetch an instance of the Decision API class. " + e.getMessage(), e);
    }

    JSObject evalObj = new JSObject();
    if (decisionApi != null) {
      String pmtStr = "CustomerPaymentScenarios";
      String rcptStr = "CustomerReceiptScenarios";
      String onUsStr = "CustomerOnUsScenarios";

      JSArray paymentArray = new JSArray();
      JSArray receiptArray = new JSArray();
      JSArray onUsArray = new JSArray();
      evalObj.put(pmtStr, paymentArray);
      evalObj.put(rcptStr, receiptArray);
      evalObj.put(onUsStr, onUsArray);

      //cycle through all permutations
      List<String> bicList = new ArrayList<>();
      bicList.add("SBZAZAJJ");  //local
      bicList.add("CITIUSJJ");  //foreign
      List<String> currencyList = new ArrayList<>();
      currencyList.add("ZAR");  //local
      currencyList.add("USD");  //foreign
      String field72 = null;
      for (BankAccountType accType : BankAccountType.values()) {
        if (accType != BankAccountType.Unknown) {
          for (ResidenceStatus resStatus : ResidenceStatus.values()) {
            if (resStatus != ResidenceStatus.Unknown) {
              for (String bic : bicList) {
                for (String currency : currencyList) {
                  //evaluate customer payment with provided params...
                  try {
                    List<IEvaluationDecision> decisions = null;
                    if (filtered) {
                      decisions = evaluator.evaluateCustomerPaymentDrFiltered(
                              resStatus, accType, bic, currency, null);
                    } else {
                      decisions = evaluator.evaluateCustomerPaymentEx(
                              resStatus, accType, bic, currency, null);
                    }
                    EvaluationRequest evaluationRequest = new EvaluationRequest();
                    evaluationRequest.setChannelName(channelName);
                    evaluationRequest.setEvaluationCustomerPayment(new EvaluationRequest.EvaluationCustomerPayment(
                            resStatus, accType,
                            bic, evaluator.getCounterpartyInstituitionByBIC(bic), currency));
                    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();
                    if (decisions != null) {
                      for (IEvaluationDecision decision : decisions) {
                        evaluations.add(new EvaluationResponse(decision));
                      }
                    }
                    EvaluationDecisions paymentDecisions = new EvaluationDecisions(evaluationRequest, evaluations);
                    paymentArray.add(paymentDecisions);
                  } catch (Exception e) {
                  }

                  //evaluate customer receipt with provided params...
                  try {
                    List<IEvaluationDecision> decisions = null;
                    if (filtered) {
                      decisions = evaluator.evaluateCustomerReceiptCrFiltered(
                              resStatus, accType, bic, currency, null, field72);
                    } else {
                      decisions = evaluator.evaluateCustomerReceiptEx(
                              resStatus, accType, bic, currency, null, field72);
                    }
                    EvaluationRequest evaluationRequest = new EvaluationRequest();
                    evaluationRequest.setChannelName(channelName);
                    evaluationRequest.setEvaluationCustomerReceipt(new EvaluationRequest.EvaluationCustomerReceipt(
                            resStatus, accType,
                            bic, evaluator.getCounterpartyInstituitionByBIC(bic), currency, field72));
                    List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();
                    if (decisions != null) {
                      for (IEvaluationDecision decision : decisions) {
                        evaluations.add(new EvaluationResponse(decision));
                      }
                    }
                    EvaluationDecisions receiptDecisions = new EvaluationDecisions(evaluationRequest, evaluations);
                    receiptArray.add(receiptDecisions);
                  } catch (Exception e) {
                    logger.error("...Unable to evaluate scenario: evaluateCustomerReceipt(\"" + resStatus + "\", \"" + accType + "\", \"" + bic + "\", \"" + currency + "\", \"" + "null" + "\", \"" + field72 + "\") -- " + e.getMessage(), e);
                  }
                }
              }
            }
          }
        }
      }


      //CUSTOMER ON US EVALS...
      for (BankAccountType drAccType : BankAccountType.values()) {
        if (drAccType != BankAccountType.Unknown) {
          for (ResidenceStatus drResStatus : ResidenceStatus.values()) {
            if (drResStatus != ResidenceStatus.Unknown) {
              for (AccountHolderStatus drAccountHolderStatus : AccountHolderStatus.values()) {
                if (drAccountHolderStatus != AccountHolderStatus.Unknown) {

                  for (BankAccountType crAccType : BankAccountType.values()) {
                    if (crAccType != BankAccountType.Unknown) {
                      for (ResidenceStatus crResStatus : ResidenceStatus.values()) {
                        if (crResStatus != ResidenceStatus.Unknown) {
                          for (AccountHolderStatus crAccountHolderStatus : AccountHolderStatus.values()) {
                            if (crAccountHolderStatus != AccountHolderStatus.Unknown) {
                              //evaluate customer on us with provided params...
                              try {
                                List<IEvaluationDecision> decisions = null;
                                if (filtered) {
                                  decisions = evaluator.evaluateCustomerOnUsFiltered(
                                          drResStatus, drAccType, drAccountHolderStatus,
                                          crResStatus, crAccType, crAccountHolderStatus);
                                } else {
                                  decisions = evaluator.evaluateCustomerOnUs(
                                          drResStatus, drAccType, drAccountHolderStatus,
                                          crResStatus, crAccType, crAccountHolderStatus);
                                }
                                HashMap<String, String> additional = new HashMap<>();
                                additional.put("drAccountHolderStatus", drAccountHolderStatus.name());
                                additional.put("crAccountHolderStatus", crAccountHolderStatus.name());
                                EvaluationRequest evaluationRequest = new EvaluationRequest();
                                evaluationRequest.setChannelName(channelName);
                                evaluationRequest.setEvaluationCustomerOnUs(new EvaluationRequest.EvaluationCustomerOnUs(
                                        drResStatus, drAccType,
                                        crResStatus, crAccType, additional
                                ));
                                List<EvaluationResponse> evaluations = new ArrayList<EvaluationResponse>();
                                if (decisions != null) {
                                  for (IEvaluationDecision decision : decisions) {
                                    evaluations.add(new EvaluationResponse(decision));
                                  }
                                }
                                EvaluationDecisions onUsDecisions = new EvaluationDecisions(evaluationRequest, evaluations);
                                onUsArray.add(onUsDecisions);
                              } catch (Exception e) {
                                logger.error("...Unable to evaluate scenario: evaluateCustomerOnUs(\"" + drResStatus + "\", \"" + drAccType + "\", \"" + drAccountHolderStatus + "\", \"" + crResStatus + "\", \"" + crAccType + "\", \"" + drAccountHolderStatus + "\") -- " + e.getMessage(), e);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return evalObj;
  }


//-----------------------------------------------------------------------------------------------
//  Correctness testing


  private boolean doChannelTestCorrectness(String channelName, String testsName, Evaluator evaluator, JSObject result) {
    result.put("Tests", testsName);

    String tests = rulesCache.getTests(channelName, testsName);
    if (tests != null) {
      JSStructureParser parser = new JSStructureParser(tests);
      Object obj;
      try {
        obj = parser.parse();
      } catch (Exception e) {
        result.put("Status", "FAILED");
        result.put("Reason", "Cannot parse tests: " + e.getMessage());
        return false;
      }
      JSObject testCaseContainer = JSUtil.findObjectWith(obj, "test_cases");
      JSArray cases = (JSArray) testCaseContainer.get("test_cases");

      EvaluationTestUtilty.TestOutput output = new EvaluationTestUtilty.TestOutput();
      EvaluationTestUtilty.doCases(evaluator, null, cases, null, output);

      if (output.getErrorCount() == 0) {
        result.put("Status", "SUCCESS");
      }
      else {
        result.put("Status", "FAILED");
      }
      result.put("TotalCount", output.getTestCount());
      result.put("ErrorCount", output.getErrorCount());

      JSArray errors = new JSArray();
      for (EvaluationTestUtilty.TestResult res : output.getResults()) {
        if (!res.hasPassed()) {
          JSObject jsObj = new JSObject();
          jsObj.put("Test", res.getTestName());
          jsObj.put("Note", res.getNote());
          errors.add(jsObj);
        }
      }
      if (errors.size() > 0) {
        result.put("Errors", errors);
      }
      return output.getErrorCount() == 0;
    }
    else {
      result.put("Status", "INFO");
      result.put("Reason", "No tests available");
      return true;
    }
  }

  private boolean doChannelCorrectness(String channelName, JSObject result) {
    Decision decisionApi = null;
    Evaluator evaluator = null;
    try {
      decisionApi = new Decision();
      evaluator = decisionApi.getEvaluator(systemInformation, channelName, engineFactory, logger);

    } catch (Exception e){
      logger.error("...Unable to fetch an instance of the Decision API class. "+e.getMessage(), e);
    }

    result.put("Channel", channelName);

    JSObject jsTestResult1 = new JSObject();
    boolean testResult1 = doChannelTestCorrectness(channelName, "testEvalCases", evaluator, jsTestResult1);

    JSObject jsTestResult2 = new JSObject();
    boolean testResult2 = doChannelTestCorrectness(channelName, "testScenarioEvalCases", evaluator, jsTestResult2);

    if (testResult1 && testResult2) {
      result.put("Status", "SUCCESS");
    }
    else {
      result.put("Status", "FAILED");
    }

    JSArray testResults = new JSArray();
    testResults.add(jsTestResult1);
    testResults.add(jsTestResult2);
    result.put("Tests", testResults);

    return testResult1 && testResult2;
  }

  @RequestMapping(value = "/evaluation/correctness", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  JSObject doEvaluationCorrectnessRun() throws Exception {
    logger.info("...Evaluation correctness run requested.");

    JSObject result = new JSObject();


    String[] channels = {"coreSARB", "coreBON", "coreRBM", "sbZA", "sbNA", "sbMW", "stdBankLibra", "hsbc", "albaraka"};
    JSArray testResults = new JSArray();
    boolean isSuccessful = true;

    for (String channelName : channels) {
      JSObject jsTestResult = new JSObject();
      boolean testResult = doChannelCorrectness(channelName, jsTestResult);
      isSuccessful = isSuccessful && testResult;
      testResults.add(jsTestResult);
    }

    if (isSuccessful) {
      result.put("Status", "SUCCESS");
    }
    else {
      result.put("Status", "FAILED");
    }

    result.put("Channels", testResults);

    return result;
  }



//-----------------------------------------------------------------------------------------------



}
