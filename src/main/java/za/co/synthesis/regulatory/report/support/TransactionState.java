package za.co.synthesis.regulatory.report.support;

/**
 * Created by jake on 3/16/17.
 */
public enum TransactionState {
  Unmatched((short)1, "Unmatched"),
  Incomplete((short)2, "Incomplete"),
  Complete((short)3, "Complete"),
  Authorise((short)4, "Authorise"),
  ReadyToGo((short)5, "ReadyToGo"),
  Submitted((short)6, "Submitted"),
  Reported((short)7, "Reported"),
  NotReportable((short)8, "NotReportable"),
  Error((short)9, "Error"),
  Split((short)10, "Split");

  private short _stateID;
  private String _stateName;

  TransactionState(short stateID, String stateName) {
    _stateID = stateID;
    _stateName = stateName;
  }

  public short getValue() {
    return _stateID;
  }

  public String getName() {
    return _stateName;
  }

  public static TransactionState findByValue(int stateID) {
    for(TransactionState enumEntry : TransactionState.values()) {
      if(enumEntry.getValue() == stateID) {
        return enumEntry;
      }
    }
    return null;
  }

  public static TransactionState findByName(String stateName) {
    for(TransactionState enumEntry : TransactionState.values()) {
      if(enumEntry.getName().equals(stateName)) {
        return enumEntry;
      }
    }
    return null;
  }
}