package za.co.synthesis.regulatory.report.support;

import za.co.synthesis.rule.core.ICustomValue;

import java.util.Map;

/**
 * Created by jake on 6/6/17.
 */
public class MapCustomValues implements ICustomValue {
  private Map<String, Object> customValues;

  public MapCustomValues(Map<String, Object> customValues) {
    this.customValues = customValues;
  }

  @Override
  public Object get(String field) {
    return customValues.get(field);
  }
}
