package za.co.synthesis.regulatory.report.validate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * User: jake
 * Date: 1/19/16
 * Time: 11:42 AM
 * Test function for validating loan references
 * A tXstream Finsurv direct DB implementation of the Validate_LoanRef function.
 * Needs to raise the following SARB error: 374, "Invalid loan reference number"
 *
 * Register: registerCustomValidate("Validate_LoanRef",
 *   new Validate_LoanRef(...), "transaction::ValueDate");

 */
public class Validate_LoanRef implements ICustomValidate {
  private JdbcTemplate jdbcTemplate;
  private DateTimeFormatter formatter;

  public Validate_LoanRef(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String loanRef = value.toString();
    Timestamp valueDate = null;

    if (otherInputs.length > 0) {
      try {
        LocalDate dt = LocalDate.parse(otherInputs[0].toString(), formatter);
        valueDate = Timestamp.valueOf(dt.atStartOfDay());
      }
      catch (IllegalArgumentException e) {
        valueDate = null;
      }
    }

    Integer resultCount;

    StringBuilder sb = new StringBuilder();
    sb.append("SELECT COUNT(*) FROM OR_LoanReference WITH (NOLOCK)");
    sb.append(" WHERE [LoanRefNumber] = ?");
		sb.append(" AND [Status] IN ('C', 'A')");
/*    if (valueDate != null) {
      sb.append(" AND (([").append(OR_LoanReference.Field_DateReceived).append("] IS NULL AND [").append(OR_LoanReference.Field_AuthDate).append("] <= ?)");
			sb.append("  OR ([").append(OR_LoanReference.Field_DateReceived).append("] IS NOT NULL AND [").append(OR_LoanReference.Field_DateReceived).append("] <= ?))");
      resultCount = jdbcTemplate.queryForObject(sb.toString(), Integer.class, loanRef, valueDate, valueDate);
    }
    else {
      resultCount = jdbcTemplate.queryForObject(sb.toString(), Integer.class, loanRef);
    }*/
		resultCount = jdbcTemplate.queryForObject(sb.toString(), Integer.class, loanRef);

    if (resultCount != null) {
      if (resultCount > 0)
        return new CustomValidateResult(StatusType.Pass);
      else
        return new CustomValidateResult(StatusType.Fail, "374", "Invalid loan reference number");
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
