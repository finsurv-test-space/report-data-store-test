package za.co.synthesis.regulatory.report.support;

import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 6/4/17.
 */
public class JSReaderUtil {

  public static String readFileFromClasspath(ClassLoader classLoader, final String fileName) throws IOException, URISyntaxException {
    //byte[] array = ByteStreams.toByteArray(this.getClass().getClassLoader().getResourceAsStream(resourceName));
    return new String(Files.readAllBytes(
        Paths.get(classLoader
            .getResource(fileName)
            .toURI())));
  }


  private static Reader loadResourceAsReader(ApplicationContext ctx, String path) throws IOException {
    Resource resource = ctx.getResource(path);
    if (resource != null ) {
      return new BufferedReader(new InputStreamReader(resource.getInputStream()));
    }
    return null;
  }


  private Reader loadResourceAsReader(String path) throws IOException {
    InputStream istrm = this.getClass().getResourceAsStream(path);
    if (istrm != null ) {
      return new BufferedReader(new InputStreamReader(istrm));
    }
    return null;
  }

  public static JSObject loadResourceAsJSObject(Path path) throws Exception {
    byte[] fileBytes = Files.readAllBytes(path);
    String data = new String(fileBytes);
    Reader reader = new BufferedReader(new StringReader(data));
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();
      if (obj instanceof JSObject)
        return (JSObject) obj;
    }
    return null;
  }

  public static JSObject loadResourceAsJSObject(ApplicationContext ctx, String path) throws Exception {
    Reader reader = loadResourceAsReader(ctx, path);
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();
      if (obj instanceof JSObject)
        return (JSObject) obj;
    }
    return null;
  }

  public static JSArray loadResourceAsJSArray(ApplicationContext ctx, String path) throws Exception {
    Reader reader = loadResourceAsReader(ctx, path);
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();
      if (obj instanceof JSArray)
        return (JSArray) obj;
    }
    return null;
  }

  public static JSObject loadReaderAsJSObject(Reader reader) throws Exception {
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();
      if (obj instanceof JSObject)
        return (JSObject) obj;
    }
    return null;
  }

  public static JSArray loadReaderAsJSArray(Reader reader) throws Exception {
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);

      Object obj = parser.parse();
      if (obj instanceof JSArray)
        return (JSArray) obj;
    }
    return null;
  }

  public static List<String> getListOfStringsFromProperty(Map map, String property) {
    Object objValue = map.get(property);
    if (objValue instanceof String) {
      return Arrays.asList(((String)objValue).split("\\s*,\\s*"));
    }
    if (objValue instanceof List) {
      List list = (List)objValue;
      List<String> result = new ArrayList<String>();
      for (Object entry : list) {
        result.add(entry.toString());
      }
      return result;
    }
    return new ArrayList<String>();
  }
}
