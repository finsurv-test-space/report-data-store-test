package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class JWTAuthToken extends AbstractAuthenticationToken {
  public enum TokenUse {
    ACCESS("access"),
    ID("id");

    String value;

    TokenUse(String value){
      this.value = value;
    }

    public String toString(){
      return this.value;
    }

    public static TokenUse fromString(String value){
      if (ACCESS.value.equals(value)){
        return ACCESS;
      }
      return ID;
    }

  }

  private List<String> access;
  private List<String> channels;
  private String username;
  private TokenUse tokenUse = TokenUse.ID;
  private Map<String,Object> other;


  /**
   * Creates a token with the supplied array of authorities.
   *
   * @param username The name associated with this user
   * @param access The access sets associated with this user
   * @param other Other information used to identify this user
   * @param authorities The list of Granted Authorities (rights).
   */
  public JWTAuthToken(String username, List<String> access, Map<String,Object> other, Collection<? extends GrantedAuthority> authorities, TokenUse tokenUse) {
    this(username, access, other, authorities, tokenUse, new ArrayList<String>());
  }
  public JWTAuthToken(String username, List<String> access, Map<String,Object> other, Collection<? extends GrantedAuthority> authorities, TokenUse tokenUse, List<String> channels) {
    super(authorities);
    this.username = username;
    this.access = access;
    this.other = other;
    this.tokenUse = tokenUse==null?TokenUse.ID:tokenUse;
    this.channels = channels;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return username;
  }

  public List<String> getAccess() {
    return access;
  }

  public TokenUse getTokenUse() {
    return tokenUse;
  }

  public void setTokenUse(String value) {
    tokenUse = TokenUse.fromString(value);
  }


  public Map<String, Object> getOther() {
    return other;
  }

  public List<String> getChannels() {
    return channels;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }
}