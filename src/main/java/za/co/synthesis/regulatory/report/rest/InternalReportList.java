package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.ListData;
import za.co.synthesis.regulatory.report.persist.UserAccessData;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.ListResult;
import za.co.synthesis.regulatory.report.schema.SortDefinition;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ValidationEngineFactory;

import java.util.Map;

/**
 * Created by jake on 6/23/17.
 */
@Controller
@RequestMapping("/internal/producer/api")
public class InternalReportList extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalReportList.class);

  @Autowired
  private ValidationEngineFactory engineFactory;

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private UserInformation userInformation;

  @Autowired
  private DataLogic dataLogic;

  
  @RequestMapping(value = "/reports/{listName}", method = RequestMethod.GET, produces = "application/json")
  @Deprecated  //TODO: Phase out this end-point... this is ONLY here for backward compatibility.
  public @ResponseBody
  ListResult getInternalReportListOld(
          @PathVariable String listName,
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam(required = false) String search,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) Integer page,
          @RequestParam(required = false) String[] sort) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportListOld()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }
    return getInternalReportListCommon(listName, reportSpace, search, maxRecords, page, sort);
  }

  @RequestMapping(value = "/report/list", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody  //TODO: Ensure that all new developments make use of this end-point instead of the old one above.
  ListResult getInternalReportListNew(
          @RequestParam String listName,
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam(required = false) String search,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) Integer page,
          @RequestParam(required = false) String[] sort) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportListNew()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }
    return getInternalReportListCommon(listName, reportSpace, search, maxRecords, page, sort);
  }

  
  public  ListResult getInternalReportListCommon(
                                                  String listname,
                                                  String reportSpace,
                                                  String search,
                                                  Integer maxRecords,
                                                  Integer page,
                                                  String[] sort) throws Exception {
    ListData listDef = systemInformation.getListDataByName(listname);
    if (listDef == null) {
      throw new PreconditionFailedException("A valid list name must be specified");
    }
    UserAccessData userAccess = userInformation.getLoggedOnUserAccessData();


    Map<String, String> searchTokens = SerializationTools.parseSearchString(search);
    if (sort != null && sort.length > 0){
      return dataLogic.getOrderedReportList(reportSpace, listDef, searchTokens, new SortDefinition(sort, listDef), userAccess, maxRecords, page);
    }

    return dataLogic.getReportList(reportSpace, listDef, searchTokens, userAccess, maxRecords, page);
  }


  //************************************************************************************************
  //  ** NOTE: **    ...For a list of available ReportLists, check the InternalConfig end-points.  *
  //************************************************************************************************
//  @RequestMapping(value = "/lists", method = RequestMethod.GET, produces = "application/json")
//  public @ResponseBody
//  List<String> getReportLists() throws Exception {
//    return systemInformation.getListNames();
//  }
}
