package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jake on 6/12/17.
 */

public class UploadStatus {
  public enum Status {
    InProgess,
    Complete
  }
  private Status status;

  public UploadStatus(Status status) {
    this.status = status;
  }

  @JsonProperty("Status")
  public Status getStatus() {
    return status;
  }
}