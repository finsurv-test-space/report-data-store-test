package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ChecksumData;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.ChecksumInfo;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@CrossOrigin
@Controller
@RequestMapping("producer/api/checkSum")
public class Checksum {

    static final Logger logger = LoggerFactory.getLogger(Checksum.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    private DataLogic dataLogic;

    /**
     * This API is used to retrieve uploaded checksums.
     * You need to provide either a valid channelName or reportSpace.
     * The valueDate is required to select the specific checksum.
     * @param channelName (optional / mutually-exclusive) If provided, the ReportSpace is not required as the channelName is directly linked to a report space
     * @param reportSpace (optional / mutually-exclusive) <b>Deprecated</b>
     * @param valueDate Required to select the specified accounting entry.
     * @return a JSON object containing the file contents of the checksum.
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public
    @ResponseBody
    void getChecksum(
            @RequestParam(required = false) String channelName,
            @RequestParam(required = false) String reportSpace,
            @RequestParam String valueDate,
            HttpServletResponse response) throws Exception {
        Channel channel = null;

        if (channelName != null) {
            channel = systemInformation.getChannel(channelName);
        }

        reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);

        if (channelName != null && channel == null) {
            throw new PreconditionFailedException("A valid channelName must be specified");
        }

        if (!systemInformation.isValidReportSpace(reportSpace)) {
            throw new PreconditionFailedException("A valid reportSpace must be specified");
        }

        if(!valueDate.matches("\\d{4}(-\\d{2}){2}")) {
            throw new PreconditionFailedException("Date must be in the following format: YYYY-MM-DD");
        }

        getChecksumContent(systemInformation,dataLogic,channelName, valueDate,response);
    }

    /**
     * This API is used to get a downloadable checksum file.
     * You need to provide either a valid channelName or reportSpace.
     * The valueDate is required to select the specific checksum.
     * @param channelName (optional / mutually-exclusive) If provided, the ReportSpace is not required as the channelName is directly linked to a report space
     * @param reportSpace (optional / mutually-exclusive) <b>Deprecated</b>
     * @param valueDate Required to select the specified accounting entry.
     * @return a JSON object containing the file contents of the checksum.
     */
    @RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
    public
    @ResponseBody
    void getChecksumFile(
            @RequestParam(required = false) String channelName,
            @RequestParam(required = false) String reportSpace,
            @RequestParam String valueDate,
            HttpServletResponse response) throws Exception {
        Channel channel = null;

        if (channelName != null) {
            channel = systemInformation.getChannel(channelName);
        }

        reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);

        if (channelName != null && channel == null) {
            throw new PreconditionFailedException("A valid channelName must be specified");
        }

        if (!systemInformation.isValidReportSpace(reportSpace)) {
            throw new PreconditionFailedException("A valid reportSpace must be specified");
        }

        if(!valueDate.matches("\\d{4}(-\\d{2}){2}")) {
            throw new PreconditionFailedException("Date must be in the following format: YYYY-MM-DD");
        }

        FileData data = dataLogic.getChecksumContent(channel.getReportSpace(), valueDate);
        if (data != null && data.getContent() != null && data.getContentType() != null) {
            //ChecksumData checksumData = checksums.getChecksumDataList().get(0);
            //return new DownloadChecksumInfo(checksums.getUpToSyncpoint(), DownloadChecksumInfo.wrapChecksumDataList(checksums.getChecksumDataList()));
            response.setHeader("Content-Disposition", "attachment; filename=" + data.getFilename());
            response.setHeader("Content-Type", data.getContentType());
            response.setHeader("ValueDate", valueDate);
            try {
                PrintWriter writer = response.getWriter();
                writer.print(new String(data.getContent()));
                writer.flush();
            } catch (Exception error){
                //throw some error
                throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
            }
        } else {
            throw new ResourceNotFoundException("No available checksum file to download for the following value date: "+valueDate+".");
        }
    }

    /**
     * This API uploads a checksum for a specific date and channel
     * @param channelName (Optional/mutually exclusive)
     * @param reportSpace (Optional/mutually exclusive) <b>Deprecated</b>
     * @param valueDate Unique identifier for checksum
     * @param checksumFile File containing the checksum data
     * @return JSON object containing a value date, a file name, file type and checksum content
     * @throws IOException
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    ChecksumInfo setChecksum(
            @RequestParam(required = false) String channelName,
            @RequestParam(required = false) String reportSpace,
            @RequestParam String valueDate,
            @RequestParam MultipartFile checksumFile) throws IOException {
        Channel channel = null;

        if (channelName != null) {
            channel = systemInformation.getChannel(channelName);
        }

        if (channelName != null && channel == null) {
            throw new PreconditionFailedException("A valid channelName must be specified");
        }

        reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);

        if (!systemInformation.isValidReportSpace(reportSpace)) {
            throw new PreconditionFailedException("A valid reportSpace must be specified");
        }

        if(!valueDate.matches("\\d{4}(-\\d{2}){2}")) {
            throw new PreconditionFailedException("Date must be in the following format: YYYY-MM-DD");
        }

        if (dataLogic.isWhiteListed(checksumFile)) {
            String fileName = checksumFile.getOriginalFilename();
            if (!dataLogic.isNameSafe(checksumFile.getOriginalFilename())) {
                try {
                    fileName = dataLogic.makeNameSafe(checksumFile.getOriginalFilename());
                } catch (Exception e) {
                    throw new PreconditionFailedException(e.getMessage());
                }
            }
            FileData data = new FileData(checksumFile.getContentType(), fileName, checksumFile.getBytes());
            return dataLogic.setChecksumContent(reportSpace, valueDate, data);
        }
        throw new PreconditionFailedException("Invalid file type (or derived mime type) provided");
    }

    public static void getChecksumContent(SystemInformation systemInformation, DataLogic dataLogic, String channelName, String valueDate, HttpServletResponse response) throws Exception {
        if (channelName != null) {
            Channel channel = systemInformation.getChannel(channelName);
            if (channel != null) {
                if (dataLogic.hasChecksum(channel.getReportSpace(), valueDate)) {
                    FileData data = dataLogic.getChecksumContent(channel.getReportSpace(), valueDate);
                    if (data != null && data.getContent() != null && data.getContentType() != null) {
                        response.setContentType(data.getContentType());
                        response.getOutputStream().write(data.getContent());
                        response.getOutputStream().flush();
                        return;
                    } else {
                        throw new ResourceNotFoundException("Checksum content '" + valueDate + "' is not set");
                    }
                } else {
                    throw new ResourceNotFoundException("Value Date '" + valueDate + "' not found");
                }
            }
        }
        throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
}
