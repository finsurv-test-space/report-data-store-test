package za.co.synthesis.regulatory.report.persist;

import java.util.List;


public class ChecksumDataDownload {
  private final String upToSyncpoint;
  private final List<ChecksumData> checksumInfoList;

  public ChecksumDataDownload(String upToSyncpoint, List<ChecksumData> checksumInfoList) {
    this.upToSyncpoint = upToSyncpoint;
    this.checksumInfoList = checksumInfoList;
  }

  public String getUpToSyncpoint() {
    return upToSyncpoint;
  }

  public List<ChecksumData> getChecksumDataList() {
    return checksumInfoList;
  }

}
