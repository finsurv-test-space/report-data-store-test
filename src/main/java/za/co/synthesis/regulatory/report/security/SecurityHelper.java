package za.co.synthesis.regulatory.report.security;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.schema.ReportDataWithState;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.schema.UserInfo;
import za.co.synthesis.regulatory.report.support.ResourceAccessAuthorizationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by james on 2017/07/31.
 */
public class SecurityHelper {
  public static final String PRODUCER_API = "PRODUCER_API";
  public static final String CONSUMER_API = "CONSUMER_API";
  public static final String INTERNAL_API = "INTERNAL_API";
  private static final Logger logger = LoggerFactory.getLogger(SecurityHelper.class);

  public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
  public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";
  public static final String REQUEST_AUTH_TOKEN = "report-data-store.authtoken";

  /*
  * SCrypt params:
  * @param N         CPU cost parameter.
  * @param r         Memory cost parameter.
  * @param p         Parallelization parameter.
  * @param dkLen     Intended length of the derived key.
  */
  //  public static String scryptSalt = "1hgeby99s1vlu0sutk958|743a0229qtp1349182676-18";
  //  public static int scryptN_CPU = 16384;
  //  public static int scryptR_Memory = 16;
  //  public static int scryptP_Parallelization = 16;
  //  public static int scryptLength = 64;
  //  public static final SCryptPasswordEncoder scryptor = new SCryptPasswordEncoder();
  public static long expiresMinutes = 30L;

  public static int keysize = 2048;//4096;

  /*****************
   * Internal cache for keeping BCrypt encode results - the encode method is expensive to run for every stateless call.
   */
  public static LoadingCache<String, String> cache = Caffeine.newBuilder()
          .expireAfterWrite(expiresMinutes, TimeUnit.MINUTES)
          .maximumSize(10_000)
          .build(plaintext -> BCrypt.hashpw(plaintext, BCrypt.gensalt(5))); //BCrypt hashing
//          .build(plaintext -> new String(SCrypt.scrypt(plaintext.getBytes(), scryptSalt.getBytes(), scryptN_CPU, scryptR_Memory, scryptP_Parallelization, scryptLength))); //lambdaworks scrypt
//          .build(plaintext -> scryptor.encode(plaintext)); //Spring Scrypt impl - requires bouncy castle


  public static boolean matchPasswordHash(String plaintext, String hash){
    //***  Spring(Bouncycastle) SCrypt implementation  ***//
    //    return scryptor.matches(plaintext, hash); //SLOW - WILL CALCULATE THE HASH EACH TIME.

    //***  Lambdaworks SCrypt implementation  ***//
//    if (plaintext != null && hash != null && !plaintext.isEmpty() && !hash.isEmpty()) {
//      return getPasswordHash(plaintext).equals(hash);
//    }

    //***  BCrypt  ***//
    if (plaintext != null && hash != null && !plaintext.isEmpty() && !hash.isEmpty() /*&& !plaintext.equals(hash)*/) {
      if (!plaintext.equals(hash)) {
        return BCrypt.checkpw(plaintext, hash);
      } /*else
        return true; //someone is trying to use the hash as the password?  ...or the passwords in the store are not hashed!
        */
    }
    return false;
  }

  /**
   * This is used to perform a hash of a password or retrieve a cached one that matches
   * The password hash is used in the configs instead of storing plaintext passwords
   * @param plainText
   * @return
   */
  public static String getPasswordHash(String plainText) {
    try {
      return cache.get(plainText);
    } catch (Exception e) {
      throw new AuthenticationServiceException("Error resolving hash for plain text token.", e);
    }
  }

  public static String authorizedUser() {
    Authentication authentication = getAuthMeta();
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      return authentication.getName();
    }
    return "";
  }

  public static boolean authenticationIsRequired(String username) {
    // Only reauthenticate if username doesn't match SecurityContextHolder and user
    // isn't authenticated
    // (see SEC-53)
    Authentication existingAuth = getAuthMeta();

    if (existingAuth == null || !existingAuth.isAuthenticated()) {
      return true;
    }

    // Limit username comparison to providers which use usernames (ie
    // UsernamePasswordAuthenticationToken)
    // (see SEC-348)

    if (existingAuth instanceof UsernamePasswordAuthenticationToken
            && !existingAuth.getName().equals(username)) {
      return true;
    }

    // Handle unusual condition where an AnonymousAuthenticationToken is already
    // present. This shouldn't happen very often, as the Filter is perhaps meant to
    // be earlier in the chain than AnonymousAuthenticationFilter.
    // Nevertheless, presence of both an AnonymousAuthenticationToken
    // together with a url username and password parameters should indicate
    // reauthentication is desirable. This behaviour is also consistent with that
    // provided by form and digest,
    // both of which force re-authentication if the respective header is detected (and
    // in doing so replace
    // any existing AnonymousAuthenticationToken). See SEC-610.
    if (existingAuth instanceof AnonymousAuthenticationToken) {
      return true;
    }

    return false;
  }


  public static void logoutActiveSessions(HttpServletRequest request, HttpServletResponse response) {
    logoutActiveSessions(SecurityContextHolder.getContext().getAuthentication(), request, response);
  }

  public static void logoutActiveSessions(Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
    if (authentication != null) {
      String currentUserName = authentication.getName();
      if (currentUserName != null && !currentUserName.isEmpty()) {
        new SecurityContextLogoutHandler().logout(request, response, authentication);
        clearContext();
        logger.info("Active User Session cleared for '{}'", currentUserName);
      }
    }
  }


  public static boolean userRightsAuthorization(UserInfo userInfo, String requiredRight) {
    if ((userInfo != null && userInfo.getAuth() != null) &&
            (requiredRight != null)) {
      for (String userRight : userInfo.getAuth().getRights()) {
        if (userRight.equals(requiredRight)) {
          return true;
        }
      }
    }
    return requiredRight == null || requiredRight.isEmpty();
  }

  public static void checkUserAuthority(String requiredRight, String resourceDescription) throws ResourceAccessAuthorizationException {
    Authentication authentication = getAuthMeta();
    String currentUserName = authentication.getName();
    boolean hasAuth = false;
    for (GrantedAuthority auth : authentication.getAuthorities()) {
      if (auth.getAuthority() instanceof String && requiredRight instanceof String && requiredRight.equalsIgnoreCase(auth.getAuthority())) {
        hasAuth = true;
        break;
      }
    }
    if (!hasAuth) {
      throw new ResourceAccessAuthorizationException(currentUserName, requiredRight, resourceDescription);
    }
  }

  public static void checkUserAuthority(UserInfo userInfo, String requiredRight, String resourceDescription) throws ResourceAccessAuthorizationException {
    if (!userRightsAuthorization(userInfo, requiredRight)) {
      throw new ResourceAccessAuthorizationException(userInfo.getAuth().getUsername(), requiredRight, resourceDescription);
    }
  }


  public static ReportServicesAuthentication getAuthMeta() {
    return getAuthMeta((IConfigStore)null);
  }

  public static ReportServicesAuthentication getAuthMeta(IConfigStore config) {
    return getAuthMeta(config, (HttpServletRequest)null);
  }

  public static ReportServicesAuthentication getAuthMeta(IConfigStore config, HttpServletRequest request) {
    return getAuthMeta(null, config, request);
  }

  public static ReportServicesAuthentication getAuthMeta(Authentication authentication) {
    return getAuthMeta(authentication, null, null);
  }

  public static ReportServicesAuthentication getAuthMeta(Authentication authentication, HttpServletRequest request) {
    return getAuthMeta(authentication, null, request);
  }

  public static ReportServicesAuthentication getAuthMeta(Authentication authentication, IConfigStore config) {
    return getAuthMeta(authentication, config, null);
  }

  public static ReportServicesAuthentication getAuthMeta(Authentication authentication, IConfigStore config, HttpServletRequest request) {
    if (request == null){
      ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
      request = servletRequestAttributes != null ? servletRequestAttributes.getRequest() : null;
    }
    if (authentication == null){
      SecurityContext ctx = SecurityContextHolder.getContext();
      if (ctx != null) {
        authentication = ctx.getAuthentication();
      }
    }
    if (authentication != null) {
      ReportServicesAuthentication rsAuth = authentication instanceof ReportServicesAuthentication ?
              (ReportServicesAuthentication) authentication : new ReportServicesAuthentication(authentication);
      if (config != null && request != null) {
        //populate the meta info fields as necessary
        List<RequestParamData> requestParams = config.getAuthTokenPopulateMetaParamList();
        Map<String, RequestParamData> metaParams = new HashMap<>();
        if (requestParams != null && requestParams.size() > 0) {
          metaParams = rsAuth.getAuthenticationMeta();
          populateMeta(requestParams, metaParams, request);
        }

        //populate the Audit fields as necessary
        List<RequestParamData> auditFields = config.getAuditParamList();
        if (auditFields != null && auditFields.size() > 0) {
          Map<String, RequestParamData> audit = getAuditFields(auditFields, metaParams, request);
          if (audit != null && audit.size() > 0) {
            Map<String, RequestParamData> rsAuthAudit = rsAuth.getAuditFields();
            if (rsAuthAudit != null) {
              rsAuthAudit.putAll(audit);
            }
          }
        }
      } else if (config != null) {
        List<RequestParamData> requestParams = config.getAuthTokenPopulateMetaParamList();
        if (requestParams != null && requestParams.size() > 0) {
          Map<String, RequestParamData> metaParams = rsAuth.getAuthenticationMeta();
          populateMeta(requestParams, metaParams, request);
        }
      } else if (request != null) {
        Map<String, RequestParamData> metaParams = new HashMap<>();
        Enumeration<String> enumerator = request.getHeaderNames();
        while (enumerator.hasMoreElements()) {
          String elem = enumerator.nextElement();
          RequestParamData requestParam = metaParams.get(elem);
          if (requestParam == null) {
            requestParam = new RequestParamData(elem);
            metaParams.put(elem, requestParam);
          }
          requestParam.setValue(request.getHeader(elem));
        }
        enumerator = request.getParameterNames();
        while (enumerator.hasMoreElements()) {
          String elem = enumerator.nextElement();
          RequestParamData requestParam = metaParams.get(elem);
          if (requestParam == null) {
            requestParam = new RequestParamData(elem);
            metaParams.put(elem, requestParam);
          }
          requestParam.setValue(request.getParameter(elem));
        }
        rsAuth.getAuthenticationMeta().putAll(metaParams);
      }
      return rsAuth;
    }
    return null;
  }


  public static Map<String, String> populateMetaStringsByName(List<String> requestParamsList) {
    Map<String, RequestParamData> metaParams = new HashMap<>();
    Map<String, String> meta = new HashMap<>();
    populateMetaByName(requestParamsList, metaParams);
    for (Map.Entry<String, RequestParamData> entry : metaParams.entrySet()) {
      meta.put(entry.getKey(), entry.getValue().csvValues(false));
    }
    return meta;
  }

  public static Map<String, RequestParamData> populateMetaByName(List<String> requestParamsList) {
    Map<String, RequestParamData> metaParams = new HashMap<>();
    populateMetaByName(requestParamsList, metaParams);
    return metaParams;
  }

  public static void populateMetaByName(List<String> requestParamsList, Map<String, RequestParamData> metaParams) {
    ServletRequestAttributes rqst = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
    HttpServletRequest req = null;
    if (rqst != null) {
      req = rqst.getRequest();
    }

    List<RequestParamData> requestParams = new ArrayList<>();
    for (String paramName : requestParamsList) {
      requestParams.add(new RequestParamData(paramName));
    }
    populateMeta(requestParams, metaParams, req);
  }

  public static void populateMetaByName(List<String> requestParamsList, Map<String, RequestParamData> metaParams, HttpServletRequest request) {
    List<RequestParamData> requestParams = new ArrayList<>();
    for (String paramName : requestParamsList) {
      requestParams.add(new RequestParamData(paramName));
    }
    populateMeta(requestParams, metaParams, request);
  }

  public static void populateMeta(List<RequestParamData> requestParams, Map<String, RequestParamData> metaParams, HttpServletRequest request) {
    if (requestParams != null && requestParams.size() > 0 && metaParams != null && request != null) {
      for (RequestParamData param : requestParams) {
        Map<String, Object> paramMeta = param.getMeta();
        Object hopObj = paramMeta.get("headersOrRequestParams");
        String headersOrParams = (hopObj instanceof String?(String)hopObj:null);
        boolean readHeader = (headersOrParams==null || headersOrParams.equalsIgnoreCase("HEADERS") || headersOrParams.equalsIgnoreCase("BOTH"));
        boolean readRequestParams = (headersOrParams==null || headersOrParams.equalsIgnoreCase("PARAMETERS")  || headersOrParams.equalsIgnoreCase("BOTH"));
        String paramValue = null;
        //Populate from Request Headers
        if (readHeader) {
          paramValue = request.getHeader(param.getName());
          if (paramValue != null) {
            RequestParamData metap = metaParams.get(param.getName());
            if (metap != null) {
              metap.setValue(paramValue);
            } else {
              param.setValue(paramValue);
              metaParams.put(param.getName(), param);
            }
          }
        }
        //Populate from Request Parameters
        if (readRequestParams && paramValue == null){
          paramValue = request.getParameter(param.getName());
          if (paramValue != null) {
            RequestParamData metap = metaParams.get(param.getName());
            if (metap != null) {
              metap.setValue(paramValue);
            } else {
              param.setValue(paramValue);
              metaParams.put(param.getName(), param);
            }
          }
        }
      }
    }
  }

  public static Map<String, RequestParamData> getAuditFields(List<RequestParamData> auditFields, Map<String, RequestParamData> metaParams, HttpServletRequest request) {
    Map<String, RequestParamData> auditFieldValueMap = new HashMap<>();

    //fetch any specified authentication meta information...
    if (metaParams != null && metaParams.size() > 0) {
      for (RequestParamData field : auditFields) {
        RequestParamData metaParam = metaParams.get(field.getName());
        if (metaParam != null) {
          field.setValue(metaParam.getValues());
        }
        auditFieldValueMap.put(field.getName(), field);
      }
    }

    //fetch any specified request headers and params...
    if (request != null) {
      Map<String, RequestParamData> requestHeadersAndParams = new HashMap<>();
      populateMeta(auditFields, requestHeadersAndParams, request);
      if (requestHeadersAndParams.size() > 0) {
        for (Map.Entry<String, RequestParamData> entry : requestHeadersAndParams.entrySet()) {
          RequestParamData field = entry.getValue();
          RequestParamData auditFieldValue = auditFieldValueMap.get(entry.getKey());
          if (auditFieldValue != null) {
            field.setValue(auditFieldValue.getValues());
          }
          auditFieldValueMap.put(field.getName(), field);
        }
      }
    }

    return auditFieldValueMap;
  }

  public static boolean isAnonymousAuth(Authentication authentication) {
    boolean isAnonymous = false;
    if ((authentication instanceof AnonymousAuthenticationToken) || (authentication != null)) {
      isAnonymous = true;
    }
    if ((authentication instanceof ReportServicesAuthentication) && (
            (((ReportServicesAuthentication) authentication).getWrappedInstance() == null) ||
                    (((ReportServicesAuthentication) authentication).getWrappedInstance() instanceof AnonymousAuthenticationToken)
    )) {
      isAnonymous = true;
    }
    return isAnonymous;
  }

  public static void clearContext() {
//    SecurityContextHolder.clearContext();
  }

  /**
   * returns a positive value (1) if the system is being accessed as a local service running on localhost or 127.0.0.1
   * returns a negative value (-1) if the system is being accessed as a remote service
   * returns a zero value (0) if unable to determine local access or not.
   * @return
   */
  public static int isRunningLocal(){
    ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
    HttpServletRequest request = servletRequestAttributes != null ? servletRequestAttributes.getRequest() : null;
    if (request != null && request.getRequestURI() != null && !request.getRequestURI().isEmpty()) {
      if ((request.getRequestURI().contains("localhost")) || (request.getRequestURI().contains("127.0.0.1"))) {
        return 1;
      }
      return -1;
    }
    return 0;
  }

//  public static boolean isAuthorisedIntercept(IConfigStore configStore, ReportObject reportObject){
  public static boolean isAuthorisedIntercept(IConfigStore configStore, ReportData reportData){
    ReportServicesAuthentication userAuth = getAuthMeta();
    if (userAuth != null && configStore != null){
      UserData userData = configStore.getUser(userAuth.getName());
      List<String> accessSets = userData.getAccess();
      Map<String, AccessSetData> accessMap = configStore.getAccessSetMap();
      if (accessSets != null && accessSets.size() > 0 && accessMap != null && accessMap.size() > 0) {

        //cycle through each access sets to see if any one of them grants access as required.
        for (String accessSetName : accessSets) {
          AccessSetData accessSet = accessMap.get(accessSetName);
          boolean hasAccess = true;
          if (accessSet != null){
            List<CriterionData> accessCriteria = accessSet.getAccessCriteria();
            if (accessCriteria != null){

              //cycle through each criterion see that ALL of the criterion are fulfilled...
              for (CriterionData  criterion : accessCriteria) {
                boolean matches = false;

                //Get the relevant report value for comparison...
                //TODO: This may not necessarily be a string... but we are only supporting string values for now.
                List<String> reportValues = null;
                try {
//                  ReportInfo report = reportObject.getReportData();
//                  if (report != null) {
//                    if (criterion.getType().equals(CriterionType.System)) {
//                      reportValue = report.getSystemValue(criterion.getName());
//                    } else {
//                      ReportData reportData = report.getReportData();
                      if (reportData != null) {
                      if (criterion.getType().equals(CriterionType.Meta) &&
                              reportData.getMeta() != null &&
                              reportData.getMeta().size() > 0 &&
                              reportData.getMeta().containsKey(criterion.getName()) &&
                              reportData.getMeta().get(criterion.getName()) != null
                              ) {
                          reportValues = (List<String>) reportData.getMeta().get(criterion.getName());
                      } else if (criterion.getType().equals(CriterionType.Report) &&
                              reportData.getReport() != null &&
                              reportData.getReport().size() > 0 &&
                              reportData.getReport().containsKey(criterion.getName()) &&
                              reportData.getReport().get(criterion.getName()) != null
                              ) {
                          reportValues = (List<String>) reportData.getReport().get(criterion.getName());
                        }
                      }
//                    }
//                  }
                } catch(Exception e){}

                //check if the report value matches any of the criterion values
                if (reportValues != null &&
                        reportValues.size() > 0 &&
                        criterion.getRuntimeValues() != null &&
                        criterion.getRuntimeValues().size() > 0) {
                  for (String criterionValue : criterion.getRuntimeValues()){
                    for (String reportValue : reportValues) {
                      if (reportValue.equals(criterionValue)) {
                        matches = true;
                        break;
                      }
                    }
                    if (matches)
                      break;
                  }
                }

                //did at least one of the criterion values match? If not, we need to check the next access set.
                if (! matches ){
                  hasAccess = false;
                  break;
                }
              }
            }
          }
          if (hasAccess)
            return true;
        }
      }
    }
    return false;
  }


  public static ReportEventContext getEventContext(){
    return new ReportEventContext(getAuthMeta(), LocalDateTime.now());
  }

  public static boolean authorizationInterceptor(IConfigStore configStore, ReportDataWithState report, boolean throwExceptionOnFailure, ResourceAccessAuthorizationException authException) throws ResourceAccessAuthorizationException {
    return authorizationInterceptor(configStore, report.getReportData(), throwExceptionOnFailure, authException);
  }

  public static boolean authorizationInterceptor(IConfigStore configStore, ReportData report, boolean throwExceptionOnFailure, ResourceAccessAuthorizationException authException) throws ResourceAccessAuthorizationException {
    //TODO: Re-enable the functionality to check for read and write access on objects within the system (based on meta queries etc)
    //disable this function for now...
    if (1==1) return true;

    if (!isAuthorisedIntercept(configStore, report)) {
      logger.error("**********\n\n" +
              "SECURITY: User not authorised to retrieve the report history requested...\n" +
              "User Details: " + SerializationTools.serializeEventContext(SecurityHelper.getEventContext()) + "\n\n" +
              //TODO: Add the user access sets etc to this (retrieve user data from configstore).
              "Request Details: *Excluded at present*" +//Here we should add the actual, serialized request object with url, headers, and params.
              "\n\n**********\n");

      if (throwExceptionOnFailure && authException != null) {
        throw authException;
      } else {
        return false;
      }
    }
    return true;
  }

  public static List<String> convertGrantedAuthoritiesToRights(Collection<? extends GrantedAuthority> authorities) {
    List<String> result = new ArrayList<String>();
    for (GrantedAuthority authority : authorities) {
      result.add(authority.getAuthority());
    }
    return result;
  }



  /**
   * PARAMS REFERENCE: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#KeyPairGenerator
   */
  public static KeyPair generatePPK() throws NoSuchProviderException, NoSuchAlgorithmException {
    //source: https://docs.oracle.com/javase/tutorial/security/apisign/step2.html
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
    keyGen.initialize(keysize, random);
    KeyPair pair = keyGen.generateKeyPair();
    return pair;
  }

  public static PrivateKey getPrivateKey(KeyPair pair){
    PrivateKey priv = pair.getPrivate();
    return priv;
  }

  public static PublicKey getPublicKey(KeyPair pair){
    PublicKey pub = pair.getPublic();
    return pub;
  }

  public static String serializeKey(Key key){
    String keyText = null;
    if (key != null){
      keyText = new String(key.getEncoded());
    }
    return keyText;
  }



}
