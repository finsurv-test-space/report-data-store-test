package za.co.synthesis.regulatory.report.support;

/**
 * Created by jake on 7/27/17.
 */
public enum AccessType {
  Read,
  Write
}
