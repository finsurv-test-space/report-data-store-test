package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jake on 6/13/17.
 */
public class UploadReference {
  private String uploadReference;

  public UploadReference(String uploadReference) {
    this.uploadReference = uploadReference;
  }

  @JsonProperty("UploadReference")
  public String getUploadReference() {
    return uploadReference;
  }
}
