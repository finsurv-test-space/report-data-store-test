package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import za.co.synthesis.regulatory.report.support.TransactionState;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:21 PM
 * A Test implementation of the Validate_ReversalTrnRef function. Needs to raise the following SARB errorS:
 * 410, "Original transaction and SequenceNumber combination not stored on database"
 * 410, "Original transaction and SequenceNumber combination stored on database, but not with an opposite flow"
 * 411, "Incorrect reversal category used with original transaction category"
 */
public class Validate_ReversalTrnRef implements ICustomValidate {
  static class TranInfo {
    private boolean exists;
    private String flow;
    private String originalCategory;
    private short stateId;

    public TranInfo(boolean exists, String flow, String originalCategory, short stateId) {
      this.exists = exists;
      this.flow = flow;
      this.originalCategory = originalCategory;
      this.stateId = stateId;
    }

    public boolean isExists() {
      return exists;
    }

    public String getFlow() {
      return flow;
    }

    public String getOriginalCategory() {
      return originalCategory;
    }

    public short getStateId() {
      return stateId;
    }
  }

  private JdbcTemplate jdbcTemplate;

  public Validate_ReversalTrnRef(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  private TranInfo getTrnInfo(JdbcTemplate jdbcTemplate, String trnReference, Integer sequence) {
    /*
SELECT t.Flow, ma.CategoryCode, t.StateID
    FROM OR_Transaction t WITH (NOLOCK)
    JOIN OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference
	WHERE t.TrnReference = 'W1608110045TT9806' AND ma.SequenceNumber = 1
    UNION ALL
SELECT t.Flow, ma.CategoryCode, t.StateID
    FROM archive.OR_Transaction t WITH (NOLOCK)
    JOIN archive.OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference
    WHERE t.TrnReference = 'W1608110045TT9806' AND ma.SequenceNumber = 1
    */
    String sql =
            "SELECT t.Flow, ma.CategoryCode, t.StateID" +
            "  FROM OR_Transaction t WITH (NOLOCK)" +
            "  JOIN OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference" +
            " WHERE t.TrnReference = ? AND ma.SequenceNumber = ?" +
            " UNION ALL " +
            "SELECT t.Flow, ma.CategoryCode, t.StateID" +
            "  FROM archive.OR_Transaction t WITH (NOLOCK)" +
            "  JOIN archive.OR_MonetaryAmount ma WITH (NOLOCK) ON ma.TrnReference = t.TrnReference" +
            " WHERE t.TrnReference = ? AND ma.SequenceNumber = ?";

    List<Map<String, Object>> listResultMap = jdbcTemplate.queryForList(sql, trnReference, sequence, trnReference, sequence);
    if (listResultMap.size() > 0) {
      Map<String, Object> row = listResultMap.get(0);
      Object flow = row.get("Flow");
      Object categoryCode = row.get("CategoryCode");
      short stateId = (Short)row.get("StateID");
      return new TranInfo(true, flow != null ? flow.toString() : "", categoryCode != null ? categoryCode.toString() : "", stateId);
    }
    else {
      return new TranInfo(false, "", "", TransactionState.Error.getValue());
    }
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    Integer sequence = null;
    String flow = null;
    String category = null;
    if (otherInputs.length > 0) {
      flow = otherInputs[0].toString();
      if (flow.length() > 1)
        flow = flow.substring(0, 1);
    }
    if (otherInputs.length > 1 && otherInputs[1] != null) {
      String seqNum = otherInputs[1].toString();
      if (seqNum.length() > 0)
        sequence = Integer.parseInt(seqNum);
    }
    if (otherInputs.length > 2 && otherInputs[2] != null) {
      category = otherInputs[2].toString();
    }

    String trnRef = value.toString();

    // call tp find flow of old transaction with trnRef and sequence
    TranInfo trnInfo = getTrnInfo(jdbcTemplate, trnRef, sequence);

    if (!trnInfo.isExists()) {
      return new CustomValidateResult(StatusType.Fail, "410", "Original transaction and SequenceNumber combination not stored on database");
    }
    else
    if (trnInfo.getFlow().equals(flow)) {
      return new CustomValidateResult(StatusType.Fail, "410", "Original transaction and SequenceNumber combination stored on database, but not with an opposite flow");
    }
    else
    if (category != null && category.length() > 0 && trnInfo.getOriginalCategory().length() > 0 &&
            category.charAt(0) != trnInfo.getOriginalCategory().charAt(0)) {
      return new CustomValidateResult(StatusType.Fail, "411", "Incorrect reversal category used with original transaction category. Original category is " + trnInfo.getOriginalCategory());
    }
    else
    if (trnInfo.getStateId() != TransactionState.Reported.getValue()) {
      return new CustomValidateResult(StatusType.Fail, "410", "Original transaction stored on database, but has not been reported to the SARB");
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
