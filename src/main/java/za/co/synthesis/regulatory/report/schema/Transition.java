package za.co.synthesis.regulatory.report.schema;

/**
 * Created by james on 2017/07/25.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.TransitionData;
import za.co.synthesis.regulatory.report.support.EventType;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({
        "Event",
        "Action",
        "ReportSpaces",
        "Channels",
        "CurrentStates",
        "NewState"
})
public class Transition {
  private final TransitionData transitionData;

  public Transition() {
    this.transitionData = new TransitionData();
  }

  public Transition(TransitionData transitionData) {
    this.transitionData = transitionData;
  }

  public static List<Transition> wrapList(List<TransitionData> infoList) {
    List<Transition> result = new ArrayList<Transition>();
    for (TransitionData info : infoList) {
      result.add(new Transition(info));
    }
    return result;
  }

  @JsonProperty("Event")
  public EventType getEvent() {
    return transitionData.getEvent();
  }

  @JsonProperty("Action")
  public String getAction() {
    return transitionData.getAction();
  }

  @JsonProperty("ReportSpaces")
  public List<String> getReportSpaces() {
    return transitionData.getReportSpaces();
  }

  @JsonProperty("Channels")
  public List<String> getChannels() {
    return transitionData.getChannels();
  }

  @JsonProperty("CurrentStates")
  public List<String> getCurrentStates() {
    return transitionData.getCurrentStates();
  }

  @JsonProperty("NewState")
  public String getNewState() {
    return transitionData.getNewState();
  }

  public void setEvent(EventType event) {
    transitionData.setEvent(event);
  }

  public void setNewState(String newState) {
    transitionData.setNewState(newState);
  }

  @JsonIgnore
  public TransitionData getTransitionData() {
    return transitionData;
  }
}
