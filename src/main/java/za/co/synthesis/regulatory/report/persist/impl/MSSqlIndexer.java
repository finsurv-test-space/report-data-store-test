package za.co.synthesis.regulatory.report.persist.impl;

import org.apache.lucene.document.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.persist.IIndexer;
import za.co.synthesis.regulatory.report.persist.index.IndexField;
import za.co.synthesis.regulatory.report.persist.index.IndexFieldValue;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by james on 2017/08/08.
 */
public class MSSqlIndexer implements IIndexer {
  private static final Logger logger = LoggerFactory.getLogger(MSSqlIndexer.class);
  @Autowired
  ApplicationContext applicationContext;

  private final JdbcTemplate jdbcTemplate;
  private final String indexTableName;
  private final String fulltextTableName;
  private final String ObjectIDFieldName;
  HashMap<String, Field> recordFields = null;
  long indexCount = 0L;


  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public MSSqlIndexer(String indexTableName, String fulltextTableName, String ObjectIDFieldName, JdbcTemplate jdbcTemplate) {
    this.indexTableName = indexTableName;
    this.fulltextTableName = fulltextTableName;
    this.ObjectIDFieldName = ObjectIDFieldName;
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Add documents to the index
   */
//    public void addDocuments(List<Object> jsonObjects) {
//        Document doc = new Document();
//        for (Object obj : jsonObjects) {
//            if ((obj instanceof Map) || (obj instanceof List)){
//                doc.clear();
//                deepIndexJson(doc, null, obj);
//
//                JSWriter jsWriter = new JSWriter();
//                jsWriter.setQuoteAttributes(true);
//                if (obj instanceof Map) {
//                    JSObject jsReportData = (JSObject) obj;
//                    jsReportData.compose(jsWriter);
//                } else if (obj instanceof Map) {
//                    JSArray jsReportData = (JSArray) obj;
//                    jsReportData.compose(jsWriter);
//                }
//                doc.add(new StoredField("JsonContent", jsWriter.toString()));
//
//                try {
//                    indexWriter.addDocument(doc);
//                } catch (IOException ex) {
//                    System.err.println("Error adding documents to the index. " + ex.getMessage());
//                }
//            }
//        }
//    }
//
//
//    private void deepIndexJson(Document doc, String fieldName, Object fieldObj) {
//        if (fieldObj instanceof Map) {
//            Map<String, Object> jsObj = (Map<String, Object>) fieldObj;
//            for (String field : (Set<String>) jsObj.keySet()) {
//                deepIndexJson(doc, field, jsObj.get(field));
//            }
//        } else if (fieldObj instanceof List) {
//            for (Object item : (List<Object>) fieldObj) {
//                deepIndexJson(doc, fieldName, item);
//            }
//        } else if (fieldName != null &&
//                !fieldName.isEmpty() &&
//                fieldObj != null){
//            doc.add(new StringField(fieldName, fieldObj.toString(), Field.Store.NO));
//        }
//    }


  @Override
  public synchronized void clear() {
    //Delete the Index set from the MSSql list?
    //TODO: Do nothing here for now.
  }

  @Override
  public void indexRecord(IndexedRecord record, String fulltext) {
    if ((jdbcTemplate != null) && (record != null)) {

      //For reports, the following fields are compulsory and should be added to EACH index entry...
      String ReportSpace = null;
      String TrnReference = null;
      String GUID = record.getGUID();
      for (IndexFieldValue field : record.getFields()) {
        if (field.getName() != null && field.getValue() != null) {
          if (field.getType().equals(CriterionType.System)) {
            if (field.getName().equals(FieldConstant.ReportSpace)) {
              ReportSpace = field.getValue();
//            } else if (field.getName().equals(FieldConstant.GUID)) {
//              GUID = field.getValue();
            }
          } else if (
                  ((field.getType().equals(CriterionType.System)) ||
                          (field.getType().equals(CriterionType.Report))
                  )&&
                  (field.getName().equals(FieldConstant.TrnReference))
                  ){
            TrnReference = field.getValue();
          }
        }
        if (ReportSpace != null && TrnReference != null /*&& GUID != null*/)
          break;
      }

      Object[] delParams = new Object[2];
      delParams[0] = ReportSpace;
      delParams[1] = TrnReference;

      List<String> values = new ArrayList<>();
      List<Object[]> params = new ArrayList<>();

      for (IndexFieldValue field : record.getFields()) {
        if (field.getName() != null && field.getValue() != null) {
          String indexStr = field.getValue().trim();
          if (indexStr.startsWith("[") && indexStr.endsWith("]")){
            indexStr = indexStr.substring(1,indexStr.length()-2);
          }
          String[] indices = indexStr.split("\\s*,\\s*");
          if (indices.length >= 1 && !indices[0].equals(indexStr)){
            for(String indexVal : indices){
              params.add(
                      new Object[]{
                              ReportSpace, TrnReference, GUID, //Mandatory fields for report indexes.
                              field.getType().toString(), field.getName(), indexVal
                      }
              );
            }
          }
          params.add(
                  new Object[]{
                          ReportSpace, TrnReference, GUID, //Mandatory fields for report indexes.
                          field.getType().toString(), field.getName(), field.getValue() //index fields - secondary indices.
                  }
          );
        }
      }

      //Create bulk SQL insert statement here...
      if (params.size()>0){
        StringJoiner joiner = new StringJoiner(",\n  ");
        for (String item : values) {
          joiner.add(item);
        }
        String sql = " DELETE ["+this.indexTableName+"] WHERE [ReportSpace] = ? AND [ReportKey] = ?";
        jdbcTemplate.update(sql, delParams);

        sql = " INSERT INTO ["+this.indexTableName+"] ([ReportSpace], [ReportKey], [ReportGuid], [IndexType], [IndexField], [IndexValue]) \nVALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.batchUpdate(sql, params);

        if (fulltext != null) {
          sql = " DELETE [" + this.fulltextTableName + "] WHERE [ReportSpace] = ? AND [ReportKey] = ?";
          jdbcTemplate.update(sql, delParams);

          Object[] fullTextParams = new Object[4];
          fullTextParams[0] = ReportSpace;
          fullTextParams[1] = TrnReference;
          fullTextParams[2] = GUID;
          fullTextParams[3] = fulltext;
          sql = " INSERT INTO [" + this.fulltextTableName + "] ([ReportSpace], [ReportKey], [ReportGuid], [FullText]) VALUES (?, ?, ?, ?)";
          jdbcTemplate.update(sql, fullTextParams);
        }
      }
    }
  }

  @Override
  public List<IndexedRecord> searchRecords(List<CriterionData> criteria) {
    int resultLimit = 0;
    if (jdbcTemplate!=null && criteria != null && criteria.size() > 0) {
      try {
        List<String> queries = new ArrayList<>();
        List<String> params = new ArrayList<>();
        if (criteria.size() > 0) {
          for (CriterionData criterion : criteria) {
            List<String> values = criterion.getRuntimeValues();
            if (values.size() > 1) {
              params.add(criterion.getType().toString());
              params.add(criterion.getName());
              String placeHolders = "";
              for (String value : values){
                params.add(value);
                placeHolders += (placeHolders.length()>0?", ":"")+"?";
              }
              queries.add(" SELECT ["+this.ObjectIDFieldName+"] AS IndexedObjectId FROM ["+this.indexTableName+"] WHERE IndexType = ? AND IndexField = ? AND IndexValue IN ("+placeHolders+")");
            } else if (values.size() == 1){
              queries.add(" SELECT [" + this.ObjectIDFieldName + "] AS IndexedObjectId FROM [" + this.indexTableName + "] WHERE IndexType = ? AND IndexField = ? AND IndexValue = ?");
              params.add(criterion.getType().toString());
              params.add(criterion.getName());
              params.add(values.get(0));
            } //if not 1 or more... then don't add the criteria to the lookup.
          }
        }
        /*
          select q_001.IndexedObjectId from
            (...) as q_001
            JOIN
            (...) as q_002
            ON q_001.IndexedObjectId = q_002.IndexedObjectId
            JOIN
            ...
            GROUP BY q_001.uuid
         */
        int cnt = 0;
        String prevQueryName = null;
        String thisQueryName = null;
        String firstQueryName = null;
        StringBuilder query = new StringBuilder();
        if (queries.size() > 0) {
          for (String subquery : queries) {
            if (thisQueryName != null) {
              prevQueryName = thisQueryName;
            }
            thisQueryName = String.format("q_%02d", cnt);
            if (prevQueryName == null) {
              firstQueryName = thisQueryName;
              query.append("SELECT ");
              if (resultLimit > 0) {
                query.append("TOP ").append(resultLimit).append(" \n");
              }
              query.append("  ").append(firstQueryName).append(".IndexedObjectId \n FROM \n");
            } else {
              //join clause
              query.append(" JOIN \n");
            }
            //sub query
            query.append("  (").append(subquery).append(") AS ").append(thisQueryName).append(" \n");
            //on clause (guid : prev=current)
            if (prevQueryName != null) {
              query.append(" ON ").append(prevQueryName).append(".IndexedObjectId").
                      append(" = ").append(thisQueryName).append(".IndexedObjectId");
            }
            cnt++;
          }
          if (cnt > 0)
            query.append(" GROUP BY \n ").append(firstQueryName).append(".IndexedObjectId");
        }


        List<String> guids = new ArrayList<>();
        String queryString = query.toString();
        String logQueryString = "--GENERATED SQL CODE--\n"+queryString+"\n----------------------";
        if (!queryString.isEmpty()) {
          try {
            for (String param: params){
              logQueryString = logQueryString.replaceFirst("\\?", "('"+param+"')");
            }
            logger.trace("Indexer SQL Query: \n\n"+logQueryString);
            guids = jdbcTemplate.queryForList(queryString, params.toArray(new String[params.size()]), String.class);
          } catch (Exception e) {
            if (e.getMessage().indexOf("did not return a result set.") < 0) {
              logger.error("Indexer query failed to run as expected.\n\nSQL: " + queryString, e);
            }
          }
        } else {
          String CriterionSet = "";
          for (CriterionData criterion : criteria){
            String criterionValues ="";
            for (String value : criterion.getRuntimeValues()){
              criterionValues += (criterionValues.length()>0?", ":"")+value;
            }
            CriterionSet+= (CriterionSet.length()>0?",\n":"")+"\t"+criterion.getName()+" => ["+criterionValues+"]";
          }
          logger.warn("Insufficient " + this.indexTableName + " criteria provided to generate meaningful SQL query" + (CriterionSet.length() > 0 ? ": \n" + CriterionSet : " - criteria list empty"));
        }

        List<IndexedRecord> list = new ArrayList<IndexedRecord>();
        if (guids != null && guids.size()>0){
          for (String guid : guids){
            IndexedRecord ir = new IndexedRecord();
            IndexFieldValue ifv = new IndexFieldValue(CriterionType.System, IndexField.Usage.Store, FieldConstant.GUID);
            ifv.setValue(guid);
            ir.getFields().add(ifv);
            list.add(ir);
          }
        }

        return list;
      } catch (Exception e) {
        logger.error("Exception searching MSSql index (" + indexTableName + ") with search terms[" + "" + "]: " + e.getMessage(), e);
      }
    } else if (criteria == null && criteria.size() == 0){
      logger.error("No criteria supplied for searching MSSql index (" + indexTableName + ") - security violation.");
    }

    return null;
  }
}