package za.co.synthesis.regulatory.report.persist;

public enum LogReadWrite {
  READ("READ"),
  WRITE("WRITE");

  private String val;

  LogReadWrite(String val){
    this.val = val;
  }

  public String toString(){
    return this.val;
  }

  public static LogReadWrite fromString(String activityType){
    for (LogReadWrite enumVal : LogReadWrite.values()){
      if (enumVal.toString().equalsIgnoreCase(activityType)){
        return enumVal;
      }
    }
    return LogReadWrite.READ;
  }
}
