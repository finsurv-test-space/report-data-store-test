package za.co.synthesis.regulatory.report.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonMapper.mapJsonObject;

/**
 * Created by james on 2017/06/06.
 */
public class JsonSchemaTransform implements ApplicationContextAware {
    private static final Logger logger = LoggerFactory.getLogger(JsonSchemaTransform.class);

//    @Autowired
    public ApplicationContext applicationContext;

    private static JsonSchemaTransform instance = null;
    private boolean initialised = false;

    Map<String, Map<String,String>> SynonymMap = new HashMap<>();
    Map<String, String> SynonymFiles = new HashMap<>();
    Map<String, Map<String,Object>> DefaultMap = new HashMap<>();
    Map<String, String> DefaultFiles = new HashMap<>();
    Map<String, Map<String,String>> OperationMap = new HashMap<>();
    Map<String, String> OperationFiles = new HashMap<>();

    public boolean RemoveDefaultIffEqual = true;
    public boolean RemoveDefaultValues = true;


    /**
     * naive singleton implementation
     * @return
     */
    public static JsonSchemaTransform getInstance(){
        if (instance == null){
            instance = new JsonSchemaTransform();
            instance.onInit();
        }
        return instance;
    }

    /**
     * Hard-coded defaulting of the various conversion permutation files required between the different schemas (sarb, bon, sadc, rbm, genv3)
     * TODO: This should be externalized to a configuration file somewhere, somehow!  ...scan the relevant path for the different file types(synonym, default, operation)?  ...perhaps a bean? ...uncomment the naive implementation which allows for files stipulations as params?
     */
    public void onInit() {
        if (instance == null) {
            instance = this;
            if (!initialised) {
                //TODO: these should be configurable from the external properties file or some such...
                //Translations between schemas...
                SynonymFiles.put("genv3-sarb", "classpath:type/transform/synonym.genv3-sarb.json");
                SynonymFiles.put("genv3-sadc", "classpath:type/transform/synonym.genv3-sadc.json");
                SynonymFiles.put("genv3-bon", "classpath:type/transform/synonym.genv3-bon.json");
                SynonymFiles.put("genv3-rbm", "classpath:type/transform/synonym.genv3-rbm.json");
                SynonymFiles.put("bon-sarb", "classpath:type/transform/synonym.bon-sarb.json");
                SynonymFiles.put("bon-rbm", "classpath:type/transform/synonym.bon-rbm.json");
                SynonymFiles.put("bon-sadc", "classpath:type/transform/synonym.bon-sadc.json");
                SynonymFiles.put("sadc-rbm", "classpath:type/transform/synonym.sadc-rbm.json");
                SynonymFiles.put("sadc-sarb", "classpath:type/transform/synonym.sadc-sarb.json");
                SynonymFiles.put("rbm-sarb", "classpath:type/transform/synonym.rbm-sarb.json");
                //SynonymFiles.put("rbm-bon", "classpath:type/transform/synonym.rbm-bon.json");
                //Defaults for schemas...
                DefaultFiles.put("sadc", "classpath:type/transform/default.sadc.json");
                DefaultFiles.put("rbm", "classpath:type/transform/default.rbm.json");
                DefaultFiles.put("genv3", "classpath:type/transform/default.genv3.json");
                DefaultFiles.put("sarb", "classpath:type/transform/default.sarb.json");
                DefaultFiles.put("bon", "classpath:type/transform/default.bon.json");
                //TODO: schema transform operation DSL to be implemented...
//        OperationFiles.put("sadc-sarb","classpath:config/transform/operation.sadc-sarb.json");
//        OperationFiles.put("sarb-sadc","classpath:config/transform/operation.sarb-sadc.json");
                loadFiles();
                initialised = true;
            }
        } else if (!instance.initialised) {
            instance.onInit();
        }
    }

//    public JsonSchemaTransform() {
//    }
//
//    public JsonSchemaTransform(Map<String, String> synonymFiles, Map<String, String> defaultFiles, Map<String, String> operationFiles) {
//        if (synonymFiles == null && defaultFiles == null && operationFiles == null) {
//            //DEFAULT TO THESE FILES...
//            onInit();
//        } else {
//            if (synonymFiles != null)
//                SynonymFiles.putAll(synonymFiles);
//            if (defaultFiles != null)
//                DefaultFiles.putAll(defaultFiles);
//            if (operationFiles != null)
//                OperationFiles.putAll(operationFiles);
//            loadFiles();
//            initialised = true;
//        }
//    }

    public void loadFiles(){
        loadSynonymFiles();
        loadDefaultFiles();
    }

    /**
     * Loads the files containing the key-to-key synonym substitutions relevant to the various schema-to-schema conversions
     */
    public void loadSynonymFiles(){
        if (SynonymFiles!= null && SynonymFiles.size() > 0) {
            Map<String, JSObject> synMaps = loadJsonFromFiles(SynonymFiles);
            if (synMaps != null) {
                for (String schemaKey : synMaps.keySet()) {
                    Map<String, String> synonyms = new HashMap<>();
                    //we expect every one of these synonyms to be a string value...
                    JSObject synonymObj = synMaps.get(schemaKey);
                    if (synonymObj != null) {
                        for (String objKey : synonymObj.keySet()) {
                            synonyms.put(objKey, (String) synonymObj.get(objKey));
                        }
                    }
                    SynonymMap.put(schemaKey, synonyms);
                    String[] reverseKeys = schemaKey.split("-");
                    String reverseKey = reverseKeys[1] + "-" + reverseKeys[0];
                    SynonymMap.put(reverseKey, getReverseIndex(synonyms));
                }
            }
        }
    }

    /**
     * loads the files containing the defaulted fields for the schema-to-schema conversions
     */
    public void loadDefaultFiles(){
        if (DefaultFiles!= null && DefaultFiles.size() > 0) {
            Map<String, JSObject> defMaps = loadJsonFromFiles(DefaultFiles);
            if (defMaps != null) {
                for (String schemaKey : defMaps.keySet()) {
                    Map<String, Object> defaults = new HashMap<>();
                    JSObject defaultObj = defMaps.get(schemaKey);
                    if (defaultObj != null) {
                        for (String objKey : defaultObj.keySet()) {
                            defaults.put(objKey, defaultObj.get(objKey));
                        }
                    }
                    DefaultMap.put(schemaKey, defaults);
                }
            }
        }
    }

    public void loadOperationFiles(){
        if (OperationFiles!= null && OperationFiles.size() > 0) {
            loadJsonFromFiles(OperationFiles);
            //TODO: add custom transformation operations here... should be defined using External DSL
        }
    }

    /**
     * Load and parse a set JSON strings from the set of files provided - Map&lt;String filename, String JSONString&gt;
     * @param pathMap Map&lt;String filename, String JSONString&gt; for which we need to parse and return a Map of JSObject instances - Map&lt;String filename, JSObject jso&gt;
     * @return Map&lt;String filename, JSObject jso&gt; analogous to the provided Map&lt;String filename, String JSONStr&gt;
     */
    public Map<String, JSObject> loadJsonFromFiles(Map<String, String> pathMap){
        Map<String, JSObject> schemaJsonMap = new HashMap<>();
        for (String schemaKey : pathMap.keySet()){
            try {
                schemaJsonMap.put(schemaKey,loadJsonFromFile(pathMap.get(schemaKey)));
            } catch (Exception e) {
                //ignore.
                logger.warn("Unable to load json transformation schema mapping files...",e);
            }
        }
        return schemaJsonMap;
    }

    /**
     * Load and parse a JSON string from a given file path
     * @param filePath string containing the file path to load and parse the JSON from
     * @return a JSObject instance containing the parse JSON string
     * @throws Exception
     */
    public JSObject loadJsonFromFile(String filePath) throws Exception {
        if (filePath != null && !filePath.isEmpty()) {
            if (filePath.startsWith("classpath:")){
                return loadJsonFromResource(filePath);
            } else {
                File file = new File(filePath);
                if (file.exists()) {
                    if (file.isFile()) {
                        return loadJsonFromFile(file);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Load and parse a JSON string from a File instance
     * @param file File instance containing the JSON string to load and parse
     * @return JSObject containing the parsed JSON string contents of the provided File instance
     * @throws Exception
     */
    public JSObject loadJsonFromFile(File file) throws Exception {
        return JSReaderUtil.loadResourceAsJSObject(applicationContext, file.getAbsolutePath());
    }

    /**
     * Load and parse a JSON string from a classpath resource
     * @param resourcePath
     * @return
     * @throws Exception
     */
    public JSObject loadJsonFromResource(String resourcePath) throws Exception {
        return JSReaderUtil.loadResourceAsJSObject(applicationContext, resourcePath);
    }

    /**
     * Creates a Map of (Key:Value) AND (Value:Key) pairs from a given (Key:Value) Map instance.
     * Ideal for Synonym-to-Synonym lookups - provide a unidirectional Synonym map {Key:Value} and generate a bidirectional map {Key:Value, Value:Key}
     * NOTE: May clash and overwrite reversed Key:Value pairs, for an improved outcome to handle Key:[Value] pairs, a separate, improved implementation is recommended.
     * @param index Uni-directional Key:Value Map instance to be converted to a bi-directional key-to-key Map {Key:Value, Value:Key}
     */
    private void createBiDirectionalIndex(Map<String, String> index){
        Map<String,String> indexRev = new HashMap<>();
        for (String key: index.keySet()){
            indexRev.put(index.get(key), key);
        }
        index.putAll(indexRev);
    }

    /**
     * Returns a Map containing the (Value:Key) pairs for a given Map of (Key:Value) pairs
     * @param index (Key:Value) Map instance to invert/reverse
     * @return (Value:Key) Map of the provided (Key:Value) Map instance
     */
    private Map<String, String> getReverseIndex(Map<String, String> index){
        Map<String,String> indexRev = new HashMap<>();
        for (String key: index.keySet()){
            indexRev.put(index.get(key), key);
        }
        return indexRev;
    }

    /**
     * Convert a ReportInfo instance to a JSObject instance
     * @param report ReportInfo instance to convert to a JSObject instance
     * @return JSObject instance of the ReportInfo instance provided
     */
    public static JSObject mapToJson(ReportInfo report){
        return mapToJson(report.getReportData().getReport());
    }

    /**
     * Convert a Map instance to a JSObject instance
     * @param map Map instance to convert to a JSObject instance
     * @return JSObject instance of the Map instance provided
     */
    public static JSObject mapToJson(Map<String, Object> map){
        JSObject obj = new JSObject();
        if (map != null && map.size() > 0) {
            for (String key : map.keySet()) {
                Object item = map.get(key);
                if (item instanceof List) {
                    obj.put(key, mapToJson((List) item));
                } else if (item instanceof Map) {
                    obj.put(key, mapToJson((Map) item));
                } else { //TODO: check for null?
                    obj.put(key, item);
                }
            }
        }
        return obj;
    }


    /**
     * Convert a List instance to a JSArray instance
     * @param list List instance to convert to a JSArray instance
     * @return JSArray instance of the List instance provided
     */
    public static JSArray mapToJson(List<Object> list){
        JSArray arr = new JSArray();
        if (list != null && list.size() > 0) {
            for (Object item : list) {
                if (item instanceof List) {
                    arr.add(mapToJson((List) item));
                } else if (item instanceof Map) {
                    arr.add(mapToJson((Map) item));
                } else { //TODO: check for null?
                    arr.add(item);
                }
            }
        }
        return arr;
    }

    public static String listToJsonStr(List list) {
        return mapToJsonStr(list);
    }

    /**
     * Stringify a List instance of data
     * @param list the List instance containing the data to stringify
     * @return the JSON string for the provided List instance
     */
    public static String mapToJsonStr(List list) {
        JSArray jsArray = JsonSchemaTransform.mapToJson(list);
        JSWriter writer = new JSWriter();
        writer.setQuoteAttributes(true);
        writer.setIndent("  ");
        writer.setNewline("\n");
        jsArray.compose(writer);
        return writer.toString();
    }

    /**
     * Stringify a Map instance of data
     * @param map the Map instance containing the data to stringify
     * @return the JSON string for the provided Map instance
     */
    public static String mapToJsonStr(Map<String, Object> map) {
        JSObject jsObject = JsonSchemaTransform.mapToJson(map);
        JSWriter writer = new JSWriter();
        writer.setQuoteAttributes(true);
        writer.setIndent("  ");
        writer.setNewline("\n");
        jsObject.compose(writer);
        return writer.toString();
    }


    /**
     * Convert a JSON string to a List instance (when it is expected to contain a list of data [])
     * @param str JSON Str
     * @return List (and Map, and Object) instance of the provided data
     * @throws Exception
     */
    public static List jsonStrToList(String str) throws Exception {
        if (str != null) {
            JSStructureParser parser = new JSStructureParser(str);
            return (List) parser.parse();
        }
        return new ArrayList<Object>();
    }

    /**
     * Convert a JSON string to a Map instance (when it is expected to contain a map of data {})
     * @param str JSON String
     * @return Map (and List, and Object) instance of the provided data
     * @throws Exception
     */
    public static Map<String, Object> jsonStrToMap(String str) throws Exception {
        if (str != null) {
            JSStructureParser parser = new JSStructureParser(str);
            return (Map<String, Object>) parser.parse();
        }
        return new HashMap<String, Object>();
    }

    public static String jsonStrToFullTextValueStr(String str) throws Exception {
        StringBuffer FullTextValueString = new StringBuffer();
        if (str != null && !str.isEmpty()) {
            Map<String, Object> jsonObj = jsonStrToMap(str);

            return jsonStrToFullTextValueStr(jsonObj);
        }
        return FullTextValueString.toString().trim();
    }

    /**
     * For the given json object (Map), extract all values and return as a full-text searchable string
     * @param jsonObj the JSON object as a Map
     * @return the complete set of values contained within the Map - concatenated together to form a String of values (without keys) for indexing and/or searching against.
     */
    public static String jsonStrToFullTextValueStr(Map<String, Object> jsonObj) {
        StringBuffer FullTextValueString = new StringBuffer();
        Map<String, Object> keyValueMap = new HashMap<>();
        mapJsonObject(jsonObj, "", keyValueMap);

        for (Object value : keyValueMap.values()) {
            if (value != null) {
                String valueStr = null;
                try {
                    valueStr = value.toString();
                } catch (Exception e) {
                }
                FullTextValueString.append(valueStr).append(" ");
            }
        }
        return FullTextValueString.toString().trim();
    }

    /**
     * For a given LIST of report object instances (ReportInfo), convert them to the destination schema
     * @param reports bop report instances (List&lt;ReportInfo&gt;)
     * @param destSchema destination (target) schema (for which we may need to assign specific default values and/or apply-key-to-key synonym-transformations)
     */
    public List<ReportInfo> convert(List<ReportInfo> reports, String destSchema) {
        if (destSchema != null && !destSchema.isEmpty()) {
            for (ReportInfo report : reports){
                convert(report, destSchema);
            }
        }
        return reports;
    }

    /**
     * For a given report object instance (ReportInfo), convert it to the destination schema
     * @param report bop report instance (ReportInfo)
     * @param destSchema destination (target) schema (for which we may need to assign specific default values and/or apply-key-to-key synonym-transformations)
     */
    public ReportInfo convert(ReportInfo report, String destSchema) {
        if (report != null &&
                destSchema != null &&
                !destSchema.isEmpty() &&
                !report.getSchema().equalsIgnoreCase(destSchema)) {
            String srcSchema = report.getSchema();
            convertSynonyms(report.getReportData().getReport(), srcSchema, destSchema);
            applyDefaults(report.getReportData().getReport(), srcSchema, destSchema);
            //TODO: Apply DSL / logical conversion here. perhaps a freemarker template?
        }
        return report;
    }

    /**
     * For a given object instance (JSObject or Map), convert it from the source schema to the destination schema
     * @param obj object instance (JSObject or Map)
     * @param srcSchema source (current) schema of the object instance provided (for which we may need to remove specific defaulted values and/or apply-key-to-key synonym-transformations)
     * @param destSchema destination (target) schema (for which we may need to assign specific default values and/or apply-key-to-key synonym-transformations)
     */
    public JSObject convert(JSObject obj, String srcSchema, String destSchema) {
        if (!srcSchema.equalsIgnoreCase(destSchema)) {
            convertSynonyms(obj, srcSchema, destSchema);
            applyDefaults(obj, srcSchema, destSchema);
            //TODO: Apply DSL / logical conversion here.  Perhaps a freemarker template?
        }
        return obj;
    }

    /**
     * For a given object instance (JSObject or Map), and for the given source and destination schemas, populate the templated default fields
     * @param obj object instance (JSObject or Map)
     * @param srcSchema source (current) schema of the object instance provided (for which we may need to remove specific defaulted values)
     * @param destSchema destination (target) schema (for which we may need to assign specific default values)
     */
    private void applyDefaults(JSObject obj, String srcSchema, String destSchema) {
        applyDefaults((Map)obj, srcSchema, destSchema);
    }

    /**
     * For a given object instance (JSObject or Map), and for the given source and destination schemas, populate the templated default fields
     * @param obj object instance (JSObject or Map)
     * @param srcSchema source (current) schema of the object instance provided (for which we may need to remove specific defaulted values)
     * @param destSchema destination (target) schema (for which we may need to assign specific default values)
     */
    private void applyDefaults(Map<String, Object> obj, String srcSchema, String destSchema) {
        //remove defaults from src format (if the values are the same as the defaults)
        if (RemoveDefaultValues) {
            Map<String, Object> defaultSet = DefaultMap.get(srcSchema);
            if (defaultSet != null) {
                for (String path : defaultSet.keySet()) {
                    ArrayList<String> pathTokens = new ArrayList<String>();
                    for (String step : path.split("\\.")) {
                        pathTokens.add(step);
                    }
                    removeDefault(obj, pathTokens, defaultSet.get(path));
                }
            }
        }

        //set defaults for target format
        Map<String, Object> defaultSet = DefaultMap.get(destSchema);
        if (defaultSet != null) {
            for (String path : defaultSet.keySet()) {
                ArrayList<String> pathTokens = new ArrayList<String>();
                for (String step : path.split("\\.")) {
                    pathTokens.add(step);
                }
                setDefault(obj, pathTokens, defaultSet.get(path));
            }
        }
    }


    /**
     * Default a value for the given path on the given object instance (JSObject, JSArray, Map or List).
     * @param obj object instance (JSObject, JSArray, Map or List) onto which we want to set the specified default value.
     * @param path the path in the object for which we want to set the default value.
     * @param value the value to set on the provided path in the given object instance.
     */
    private void setDefault(Object obj, List<String> path, Object value){
        if (path.size() > 1) {
            if (obj instanceof Map) {
                String step = path.remove(0);
                Object childObj = ((Map)obj).get(step);
                if (childObj == null){
//                    if (path.get(0).startsWith("[")){ //JSArray
//                        //TODO: decide whether or not we need to create an array type and populate ... or ignore and exit?
//                    } else { //JSObject
                        childObj = new JSObject();
                        ((Map)obj).put(step, childObj); //TODO: THIS COULD BE WHERE WE ARE INTRODUCING THE {} - EMPTY OBJECT NOTATIONS IN THE JSON BEING RETURNED... ???
//                    }
                }
                setDefault(childObj, path, value);
            } else if (obj instanceof List) {
                for (Object childObj : ((List)obj)){
                    setDefault(childObj, path, value);
                }
            }
        } else {
            String step = path.remove(0);
            logger.trace("**Setting default: {}->{}",step, value.toString());
            if (obj instanceof Map) {
                //TODO: Shall we check if the default exists first or just blindly overwrite?
                ((Map)obj).put(step, value);
            } else if (obj instanceof List) {
                String arrayIndex = "";
//                if (step.startsWith("[")) {
//                    arrayIndex = step.replaceAll("\\[", "").replaceAll("]", "");
//                } else {//ommitted array designation... assume "ALL" array items and add path back again...
                    path.add(step);
//                }
                for (Object childObj : ((List)obj)){
                    setDefault(childObj, path, value);
                }
            } else {
                //EEK!   we shouldn't be here and still need to add a value!
                logger.error("Trying to assign default child to leaf node value...  this should be added to a branch");
            }
        }
    }

    /**
     * Given a map of synonym key-to-key pairs, and given a lookup key, return a synonym key if one exists. otherwise return the original lookup key.
     * @param synonyms Map of synonym key-to-key values for a source and target schema configuration
     * @param lookup the lookup key for which to check if a synonym exists.
     * @return
     */
    public String fetchSynonym(Map<String, String> synonyms, String lookup){
        if (synonyms.containsKey(lookup)) {
            String synonym = synonyms.get(lookup);
            return (synonym!=null && !synonym.isEmpty()?synonym:lookup);
        }
        return lookup;
    }

    /**
     * Given an object (JSObject, JSArray, Map or List), remove any defaulted fields specific to the source schema and data
     * @param obj JSObject, JSArray, Map or List containing the relevant (assumedly) source schema compliant data
     * @param path the path of the defaulted value to check
     * @param value the defaulted value to check for and/or remove
     */
    private void removeDefault(Object obj, List<String> path, Object value){
        if (path.size() > 1) {
            if (obj instanceof JSObject) {
                String step = path.remove(0);
                Object childObj = ((JSObject)obj).get(step);
                if (childObj != null){
                    removeDefault(childObj, path, value);
                }
            } else if (obj instanceof JSArray) {
                for (Object childObj : ((JSArray)obj)){
                    removeDefault(childObj, path, value);
                }
            }
        } else {
            String step = path.remove(0);
            logger.trace("**Removing default: {}->{}", step, value.toString());
            if (obj instanceof JSObject) {
                if ((!RemoveDefaultIffEqual) ||
                        (((JSObject)obj).get(step)!=null &&
                                value!=null &&
                                value.getClass().equals(((JSObject)obj).get(step).getClass()) &&
                                ((JSObject)obj).get(step).toString().equalsIgnoreCase((value).toString()))) {
                    ((JSObject) obj).remove(step);
                }
            } else if (obj instanceof JSArray) {
                path.add(step);
                for (Object childObj : ((JSArray)obj)){
                    removeDefault(childObj, path, value);
                }
            } else {
                //EEK!   we shouldn't be here and still need to add a value!
                logger.error("Trying to assign default child to leaf node value...  this should be added to a branch");
            }
        }
    }

    /**
     * Given an object (JSObject or Map), convert all the synonym fields from the source schema (assumed to be the schema in which the object is currently populated) to the target schema
     * @param obj JSObject, JSArray, Map or List containing the relevant (assumedly) source schema compliant data
     * @param srcSchema the source schema of the data provided
     * @param destSchema the target schema to convert the provided data to
     */
    private void convertSynonyms(Map<String, Object> obj, String srcSchema, String destSchema) {
        Map<String, String> synonyms = SynonymMap.get(srcSchema+"-"+destSchema);
        if (synonyms != null && synonyms.size() > 0){
            replaceSynonyms(obj, synonyms, "");
        }
    }

    /**
     * Given an object (JSObject, JSArray, Map or List), convert all the synonym fields from the source schema (assumed to be the schema in which the object is currently populated) to the target schema
     * @param obj JSObject, JSArray, Map or List containing the relevant (assumedly) source schema compliant data
     * @param srcSchema the source schema of the data provided
     * @param destSchema the target schema to convert the provided data to
     */
    private void convertSynonyms(JSObject obj, String srcSchema, String destSchema) {
        convertSynonyms((Map)obj, srcSchema, destSchema);
    }


    /**
     * Given a JSObject, replace all child instances of keynames which correspond to the given map of keyname synonyms for the given path.
     * @param obj JSObject, JSArray, Map or List containing the relevant data to synonym-transform
     * @param synonyms the map of keyname synonyms to transform ("X"->"Y")
     * @param path the path in the object for which the synonym-transform should be applied
     */
    private void replaceSynonyms(JSObject obj, Map<String, String> synonyms, String path){
        replaceSynonyms((Map)obj, synonyms, path);
    }

    /**
     * Given a Map of objects(JSObject, JSArray, Map or List), replace all child instances of keynames which correspond to the given map of keyname synonyms for the given path.
     * @param obj JSObject, JSArray, Map or List containing the relevant data to synonym-transform
     * @param synonyms the map of keyname synonyms to transform ("X"->"Y")
     * @param path the path in the object for which the synonym-transform should be applied
     */
    private void replaceSynonyms(Map<String, Object> obj, Map<String, String> synonyms, String path){
        path = path==null?"":path;
        if (!obj.isEmpty()){
            List<String> removeKeys = new ArrayList<>();
            Map<String, Object> replacementKeys = new HashMap<>();
            for (String key : obj.keySet()){
                Object child = obj.get(key);
                String newKey = fetchSynonym(synonyms, key);
                if (!newKey.equals(key)) {
                    removeKeys.add(key);
                    replacementKeys.put(newKey, child);
                    logger.trace("**Converting synonym field: {}=>{}",key,newKey);
                }
                if (child instanceof Map){
                    replaceSynonyms((Map)child, synonyms, (path.isEmpty()?"":path+".")+newKey);
                } else if (child instanceof List){
                    replaceSynonyms((List)child, synonyms, (path.isEmpty()?"":path+".")+newKey);
                } else {
                    //logger.trace("{}.{}:{}",path,newKey,child.toString());
                }
            }
            for (String key : removeKeys){
                obj.remove(key);
            }
            for (String key : replacementKeys.keySet()){
                obj.put(key, replacementKeys.get(key));
            }
        }
    }

    /**
     * Given a JSArray, replace all child instances of keynames which correspond to the given map of keyname synonyms for the given path.
     * @param obj JSObject, JSArray, Map or List containing the relevant data to synonym-transform
     * @param synonyms the map of keyname synonyms to transform ("X"->"Y")
     * @param path the path in the object for which the synonym-transform should be applied
     */
    private void replaceSynonyms(JSArray obj, Map<String, String> synonyms, String path){
        replaceSynonyms((List)obj, synonyms, path);
    }

    /**
     * Given a list of objects (JSObject, JSArray, Map or List), replace all child instances of keynames which correspond to the given map of keyname synonyms for the given path.
     * @param obj JSObject, JSArray, Map or List containing the relevant data to synonym-transform
     * @param synonyms the map of keyname synonyms to transform ("X"->"Y")
     * @param path the path in the object for which the synonym-transform should be applied
     */
    private void replaceSynonyms(List<Object> obj, Map<String, String> synonyms, String path){
        path = path==null?"":path;
        if (!obj.isEmpty()){
            int i = 0;
            for (Object child : obj){
                if (child instanceof Map){
                    replaceSynonyms((Map)child, synonyms, path+"["+(i++)+"]");
                } else if (child instanceof List){
                    replaceSynonyms((List)child, synonyms, path+"["+(i++)+"]");
                } else {
                    //logger.trace("{}[{}]:{}",path,i++,child.toString());
                }
            }
        }
    }

    /**
     * Check if the object (JSObject, JSArray or some variation of Map/List) contains a field with matching key to the synonymField name provided.
     * @param obj the Map or List (or JSObject) instance to recursively navigate
     * @param synonymField the field name to look for
     * @return true if the synonymField name is found as a key somewhere within the provided object (Map, List, JSObject or JSArray)
     */
    private boolean containsSynonymField(Object obj, String synonymField){

        if (obj != null && synonymField != null && !synonymField.isEmpty()){

            if (obj instanceof Map){
                for (Map.Entry<String, Object> entry : ((Map<String, Object>) obj).entrySet()){
                    if (entry.getKey().equals(synonymField)){
                        return true;
                    }
                    if (containsSynonymField(entry.getValue(), synonymField)){
                        return true;
                    }
                }
            }

            if (obj instanceof List) {
                for (Object listObj : (List)obj){
                    if (listObj instanceof Map || listObj instanceof List) {
                        if (containsSynonymField(listObj, synonymField)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Attempt to get the relevant report schema based on fields present in the data provided.
     * This is done by looking for the existence of fields specified in the synonym files.
     * Not a guaranteed match, but a naive/rudimentary implementation.
     *
     * Notes:
     * - If only one set of synonyms match from a particular schema, then that schema is returned.
     * - If synonyms from multiple schemas match then:
     *   + If any of the schemas is the same as the default schema, it is returned.
     *   + If no schemas match to the default schema, the first in the list is returned.
     * - If no synonyms match from any schemas, then the specified default schema is returned.
     *
     * @param obj  the JSObject to schema-check.
     * @param defaultSchema the default schema to return if no specific schema is identified in the data.
     * @return the assumed or defaulted schema name corresponding to the data provided (based on synonym fields specific to the schema, which are present in the data)
     */
    public String getReportSchema(Map<String, Object> obj, String defaultSchema){
        if (SynonymMap == null || SynonymMap.size() == 0){
            loadSynonymFiles();
        }

        HashMap<String, Boolean> hasSchemaFields = new HashMap<>();
        if (SynonymMap != null || SynonymMap.size() > 0) {
            for (Map.Entry<String, Map<String, String>> entry : SynonymMap.entrySet()) {
                String schema = entry.getKey();
                if (schema.indexOf("-")>0){
                    schema = schema.split("-")[0];
                }
                for (Map.Entry<String, String> keyVal : entry.getValue().entrySet()) {
                    if (containsSynonymField(obj, keyVal.getKey())) {
                        hasSchemaFields.put(schema, true);
                        break;
                    }
                }
            }
            if (hasSchemaFields.size() == 1) {
                return hasSchemaFields.keySet().toArray(new String[1])[0];
            }
            if (hasSchemaFields.size() >= 1) {
                String[] possibleSchemas = hasSchemaFields.keySet().toArray(new String[1]);
                for (String schema : possibleSchemas){
                    if (schema.equalsIgnoreCase(defaultSchema)) {
                        return schema;
                    }
                }
                return hasSchemaFields.keySet().toArray(new String[1])[0];
            }
        }
        return defaultSchema;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * Replace the complete set (map) of Synonym files as such:
     * each map entry will contain the schema-schema key and related synonym file path as follows:
     *  {"genv3-sarb", "classpath:type/transform/synonym.genv3-sarb.json",...}
     *
     * This will tell the transformation engine the schema-to-schema conversion it provides and the files with the relevant fields.
     * File contents will look as follows (see resources/type/transform):
     * {
     *   "DomesticValue":"RandValue",
     *   "ForeignCardHoldersCashWithdrawalsDomesticValue":"ForeignCardHoldersCashWithdrawalsRandValue",
     *   "ForeignCardHoldersPurchasesDomesticValue":"ForeignCardHoldersPurchasesRandValue",
     *   "RegulatorAuth":"SARBAuth",
     *   "CBAuthAppNumber":"SARBAuthAppNumber",
     *   "CBAuthRefNumber":"SARBAuthRefNumber",
     *   "REInternalAuthNumber":"ADInternalAuthNumber",
     *   "REInternalAuthNumberDate":"ADInternalAuthNumberDate"
     * }
     *
     * @param synonymFiles the map containing the schema-to-schema key name and related file with the relevant field synonyms.
     */
    public void setSynonymFiles(Map<String, String> synonymFiles) {
        SynonymFiles = synonymFiles;
    }

    /**
     * Assign (append/replace) a set (map) of Synonym files as such:
     * each map entry will contain the schema-schema key and related synonym file path as follows:
     *  {"genv3-sarb", "classpath:type/transform/synonym.genv3-sarb.json",...}
     *
     * This will tell the transformation engine the schema-to-schema conversion it provides and the files with the relevant fields.
     * File contents will look as follows (see resources/type/transform):
     * {
     *   "DomesticValue":"RandValue",
     *   "ForeignCardHoldersCashWithdrawalsDomesticValue":"ForeignCardHoldersCashWithdrawalsRandValue",
     *   "ForeignCardHoldersPurchasesDomesticValue":"ForeignCardHoldersPurchasesRandValue",
     *   "RegulatorAuth":"SARBAuth",
     *   "CBAuthAppNumber":"SARBAuthAppNumber",
     *   "CBAuthRefNumber":"SARBAuthRefNumber",
     *   "REInternalAuthNumber":"ADInternalAuthNumber",
     *   "REInternalAuthNumberDate":"ADInternalAuthNumberDate"
     * }
     *
     * @param synonymFiles the map containing the schema-to-schema key name and related file with the relevant field synonyms.
     */
    public void addSynonymFiles(Map<String, String> synonymFiles) {
        SynonymFiles.putAll(synonymFiles);
    }
}
