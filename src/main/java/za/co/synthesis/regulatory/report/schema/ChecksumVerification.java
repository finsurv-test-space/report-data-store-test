package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
        "Checksums"
        , "Matched"
})
public class ChecksumVerification {
  private Map<String, String> checksums;

  public ChecksumVerification() {
    checksums = new HashMap<String, String>();
  }

  public ChecksumVerification(Map<String, String> checksums) {
    this.checksums = (checksums != null) ? checksums : new HashMap<String, String>();
  }

  @JsonProperty("Checksums")
  public Map<String, String> getChecksums() {
    return checksums;
  }

  @JsonProperty("Matched")
  public Boolean getMatched() {
    return new Boolean(checksums.containsKey("MATCH") ? checksums.get("MATCH").equalsIgnoreCase("YES") : false);
  }

  public void setChecksums(Map<String, String> checksums) {
    this.checksums = checksums;
  }

}
