package za.co.synthesis.regulatory.report.support;

import java.util.Map;

/**
 * Created by petruspretorius on 06/06/2017.
 */
public interface IChannelCache {
  String getVendorCode() throws Exception;

  String getPackage(String channelName) throws Exception;

  String getAppCode() throws Exception;

  String getForm(String channelName, String data,  Boolean internal, Map<String, String[]> requestParams, String _uiContextAndProxyPath, Boolean packedRules, Boolean allowNew) throws Exception;

  /**
   * Note that these are the disk-based lookups and should not be used by api's
   * @return
   * @throws Exception
   */
  String getLookups(String channelName) throws Exception;
}
