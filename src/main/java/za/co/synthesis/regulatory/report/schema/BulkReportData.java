package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;


@JsonPropertyOrder({
    "Reports"
})
public class BulkReportData {
  private List<ReportData> reports = new ArrayList<>();

  public BulkReportData(List<ReportData> reports) {
    this.reports = reports;
  }

  public BulkReportData(){}

  @JsonProperty("Reports")
  public List<ReportData> getReports() {
    return reports;
  }

  public void setReports(List<ReportData> reports) {
    this.reports = reports;
  }

}
