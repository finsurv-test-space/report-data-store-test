package za.co.synthesis.regulatory.report.security;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

import java.util.Collection;
import java.util.Properties;

/**
 * Created by james on 2017/06/20.
 */
public class ReportServicesSecurityMetaDataSource implements
        FilterInvocationSecurityMetadataSource {

    private Properties urlProperties = new Properties();

    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    public Collection<ConfigAttribute> getAttributes(Object filter)
            throws IllegalArgumentException {
//        return SecurityConfig.createListFromCommaDelimitedString("ADMIN");
        FilterInvocation filterInvocation = (FilterInvocation) filter;
        String url = filterInvocation.getRequestUrl();

//get the roles for requested page from the property file
        String urlPropsValue = urlProperties.getProperty(url);

        if (urlPropsValue == null){
            for (String name :urlProperties.stringPropertyNames()){
                String regex = name.replaceAll("\\*\\*", ".*");
                if (url.matches(regex)){
                    urlPropsValue = urlProperties.getProperty(name);
                    break;
                }
            }
        }

        StringBuilder rolesStringBuilder = new StringBuilder();
        if(urlPropsValue != null) {
            rolesStringBuilder.append(urlPropsValue).append(",");
        }

        if(!url.endsWith("/")) {
            int lastSlashIndex = url.lastIndexOf("/");
            url = url.substring(0, lastSlashIndex + 1);
        }


        String [] urlParts = url.split("/");

        StringBuilder urlBuilder = new StringBuilder();
        for (String urlPart : urlParts) {
            if(urlPart.trim().length() == 0) {
                continue;
            }
            urlBuilder.append("/").append(urlPart);
            urlPropsValue = urlProperties.getProperty(urlBuilder.toString() + "/**");

            if(urlPropsValue != null) {
                rolesStringBuilder.append(urlPropsValue).append(",");
            }
        }

        if(rolesStringBuilder.toString().endsWith(",")) {
            rolesStringBuilder.deleteCharAt(rolesStringBuilder.length()-1);
        }


        if(rolesStringBuilder.length() == 0) {
            return null;
        }

        return SecurityConfig.createListFromCommaDelimitedString(rolesStringBuilder.toString());
    }

    public boolean supports(Class<?> arg0) {
        return true;
    }

    public void setUrlProperties(Properties urlProperties) {
        this.urlProperties = urlProperties;
    }

    public Properties getUrlProperties() {
        return urlProperties;
    }

}
