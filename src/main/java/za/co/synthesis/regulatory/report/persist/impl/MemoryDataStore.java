package za.co.synthesis.regulatory.report.persist.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.history.HistoryUtils;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecision;
import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecisions;
import za.co.synthesis.regulatory.report.schema.ListResult;
import za.co.synthesis.regulatory.report.schema.SortDefinition;

import java.net.URLDecoder;
import java.util.*;

/**
 * Created by jake on 5/24/17.
 */
public class MemoryDataStore implements IDataStore {
  private static final Logger logger = LoggerFactory.getLogger(MemoryDataStore.class);

  @Autowired
  IConfigStore configStore;

  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  private static class FileDocument {
    private final FileData data;
    private final DocumentData doc;

    public FileDocument(FileData data, DocumentData doc) {
      this.data = data;
      this.doc = doc;
      if (data != null && doc != null) {
        doc.setFilename(data.getFilename());
        doc.setFileType(data.getContentType());
      }
    }

    public FileData getData() {
      return data;
    }

    public DocumentData getInfo() {
      return getDocumentData();
    }

    public DocumentData getDocumentData() {
      return doc;
    }
  }

  private final Map<String, ReportInfo> reportInfoMap = new HashMap<String, ReportInfo>(); /*Report Handle -> ReportInfo*/
  private final Map<String, List<String>> reportIndex = new HashMap<String, List<String>>(); /*Transaction Key -> List<Report Handle>*/
  private final Map<String, SortedMap<Long, String>> reportDownLoadMap = new HashMap<String, SortedMap<Long, String>>(); /*Report Space -> (Report Download Syncpoint -> Report Handle)*/

  private final Map<String, FullyReferencedEvaluationDecision> decisionMap = new HashMap<String, FullyReferencedEvaluationDecision>(); /*Decision Handle -> EvaluationDecision*/
  private final Map<String, List<String>> decisionIndex = new HashMap<String, List<String>>(); /*Transaction Key -> List<Decision Handle>*/
  private final Map<String, SortedMap<Long, String>> decisionDownLoadMap = new HashMap<String, SortedMap<Long, String>>(); /*Report Space -> (Decision Download Syncpoint -> Decision Handle)*/
  private final Map<String, FullyReferencedEvaluationDecisions> decisionLogMap = new HashMap<String, FullyReferencedEvaluationDecisions>(); /*Decision Handle -> EvaluationDecision*/
  private final Map<String, List<String>> decisionLogIndex = new HashMap<String, List<String>>(); /*Transaction Key -> List<Decision Handle>*/

  private final Map<String, FileDocument> documentMap = new HashMap<String, FileDocument>(); /*Document Handle -> File Document*/
  private final Map<String, List<String>> reportDocumentIndex = new HashMap<String, List<String>>(); /*Transaction Key -> List<Document Handle>*/
  private final Map<String, SortedMap<Long, String>> documentDownLoadMap = new HashMap<String, SortedMap<Long, String>>(); /*Report Space -> (Document Download Syncpoint -> Document Handle)*/


  private String generateHandle() {
    return UUID.randomUUID().toString();
  }

  private String getKey(String reportSpace, String trnReference) {
    return reportSpace + "@" + trnReference;
  }

  private String getDocumentKey(String reportSpace, String documentHandle) {
    if (!documentHandle.startsWith(reportSpace + "@")) {
      return reportSpace + "@" + documentHandle;
    }
    return documentHandle;
  }


  private void addForDownload(Map<String, SortedMap<Long, String>> downloadMap, String reportSpace, String handle) {
    SortedMap<Long, String> sortedMap;
    Long key;
    if (downloadMap.containsKey(reportSpace)) {
      sortedMap = downloadMap.get(reportSpace);
      key = sortedMap.lastKey() + 1;
    } else {
      sortedMap = new TreeMap<Long, String>();
      downloadMap.put(reportSpace, sortedMap);
      key = 1L;
    }
    sortedMap.put(key, handle);
  }

  @Override
  public void setEvaluationDecision(FullyReferencedEvaluationDecision decision) {
    String transactionKey = getKey(decision.getReportSpace(), decision.getTrnReference());
    String decisionHandle = generateHandle();

    decisionMap.put(decisionHandle, decision);
    if (decisionIndex.containsKey(transactionKey)) {
      decisionIndex.get(transactionKey).add(decisionHandle);
    } else {
      List<String> decisionItems = new ArrayList<String>();
      decisionItems.add(decisionHandle);
      decisionIndex.put(transactionKey, decisionItems);
    }

    addForDownload(decisionDownLoadMap, decision.getReportSpace(), decisionHandle);
  }

  @Override
  public FullyReferencedEvaluationDecision getEvaluationDecision(String reportSpace, String trnReference) {
    String transactionKey = getKey(reportSpace, trnReference);
    List<String> decisionHandles = decisionIndex.get(transactionKey);
    if (decisionHandles != null) {
      return decisionMap.get(decisionHandles.get(decisionHandles.size() - 1));
    }
    return null;
  }


  @Override
  public void setEvaluationDecisionLog(FullyReferencedEvaluationDecisions evaluations) {
    String transactionKey = getKey(evaluations.getReportSpace(), evaluations.getTrnReference());
    String decisionHandle = generateHandle();

    decisionLogMap.put(decisionHandle, evaluations);
    if (decisionLogIndex.containsKey(transactionKey)) {
      decisionLogIndex.get(transactionKey).add(decisionHandle);
    } else {
      List<String> decisionItems = new ArrayList<String>();
      decisionItems.add(decisionHandle);
      decisionLogIndex.put(transactionKey, decisionItems);
    }
  }

  @Override
  public FullyReferencedEvaluationDecisions getEvaluationDecisionLog(String reportSpace, String trnReference) {
    String transactionKey = getKey(reportSpace, trnReference);
    List<String> decisionHandles = decisionLogIndex.get(transactionKey);
    if (decisionHandles != null) {
      return decisionLogMap.get(decisionHandles.get(decisionHandles.size() - 1));
    }
    return null;
  }

  @Override
  public void setReport(String reportSpace, String trnReference, ReportInfo report, boolean addToDownload) {
    String transactionKey = getKey(reportSpace, trnReference);
    String reportHandle = generateHandle();

    reportInfoMap.put(reportHandle, report);
    if (reportIndex.containsKey(transactionKey)) {
      //Print out audit diffs on console...
      logger.trace(
          HistoryUtils.getJsonDiffText(
              reportInfoMap.get(reportIndex.get(transactionKey).get(reportIndex.get(transactionKey).size() - 1)),
              report)
      );
      reportIndex.get(transactionKey).add(reportHandle);
    } else {
      List<String> reportItems = new ArrayList<String>();
      reportItems.add(reportHandle);
      reportIndex.put(transactionKey, reportItems);
    }
    if (addToDownload) {
      addForDownload(reportDownLoadMap, reportSpace, reportHandle);
    }
  }

  @Override
  public ReportInfo getReport(String reportSpace, String trnReference) {
    //TODO: add security permissions/criteria checking here!
    String transactionKey = getKey(reportSpace, trnReference);
    List<String> reportHandles = reportIndex.get(transactionKey);

    //try a URL decode if the reference doesn't return a value...
    if (reportHandles == null && trnReference.indexOf("%") >= 0) {
      try {
        reportHandles = reportIndex.get(getKey(reportSpace, java.net.URLDecoder.decode(trnReference, "UTF-8")));
      } catch (Exception e) {
      }
    }

    if (reportHandles != null) {
      return reportInfoMap.get(reportHandles.get(reportHandles.size() - 1));
    }
    return null;
  }

  @Override
  public List<ReportInfo> getReportHistory(String reportSpace, String trnReference) {
    return null;
  }

//    @Override
//    public List<DocumentData> getDocumentsForReport(String reportSpace, String trnReference) {
//        String transactionKey = getKey(reportSpace, trnReference);
//        List<String> documentHandles = reportDocumentIndex.get(transactionKey);
//        if (documentHandles != null) {
//            List<DocumentData> result = new ArrayList<DocumentData>();
//            for (String documentHandle : documentHandles) {
//                result.add(documentMap.get(documentHandle).getInfo());
//            }
//            return result;
//        }
//        return null;
//    }

  @Override
  public DocumentData setDocumentContent(DocumentData doc, FileData data) {
    String transactionKey = getKey(doc.getReportSpace(), doc.getTrnReference());
    String documentHandle = DataLogic.getDocumentHandle(doc);
    doc.setDocumentHandle(documentHandle);  //TODO: WAIT... WHAT?  IS THIS RIGHT?

    documentMap.put(documentHandle, new FileDocument(data, doc));
    if (reportDocumentIndex.containsKey(transactionKey)) {
      List<String> documentList = reportDocumentIndex.get(transactionKey);
      if (!documentList.contains(documentHandle)) {
        documentList.add(documentHandle);
      }
    } else {
      List<String> documentList = new ArrayList<String>();
      documentList.add(documentHandle);
      reportDocumentIndex.put(transactionKey, documentList);
    }

    addForDownload(documentDownLoadMap, doc.getReportSpace(), documentHandle);
    return doc;
  }

  @Override
  public ChecksumData setChecksumContent(ChecksumData document) {
    return null;
  }

  @Override
  public boolean hasDocument(String reportSpace, String documentHandle) {
    return documentMap.containsKey(getDocumentKey(reportSpace, documentHandle));
  }

  @Override
  public boolean hasChecksum(String reportSpace, String valueDate) {
    return false;
  }

  @Override
  public List<DocumentData> getDocumentsByReference(String reportSpace, String trnReference) {
    ArrayList<DocumentData> docs = new ArrayList<>();
    String transactionKey = getKey(reportSpace, trnReference);
    if (reportDocumentIndex.containsKey(transactionKey)) {
      List<String> documentList = reportDocumentIndex.get(transactionKey);
      for (String documentHandle : documentList) {
        FileDocument fd = documentMap.get(documentHandle);
        if (fd != null) {
          docs.add(fd.getDocumentData());
        }
      }
    }
    return docs;
  }

  @Override
  public AccountEntryData setAccountEntry(String reportSpace, String reportKey, AccountEntryData data) {
    return null;
  }

  @Override
  public AccountEntryData getAccountEntry(String reportSpace, String trnReference) {
    return null;
  }

  @Override
  public FileData getDocumentContent(String reportSpace, String documentHandle) {
    FileDocument fd = documentMap.get(getDocumentKey(reportSpace, documentHandle));
    fd = fd != null ? fd : documentMap.get(documentHandle);
    return fd != null ? fd.getData() : null;
  }

  @Override
  public FileData getChecksumContent(String reportSpace, String valueDate) {
    return null;
  }

  @Override
  public DocumentData getDocument(String reportSpace, String documentHandle) {
    FileDocument fd = documentMap.get(getDocumentKey(reportSpace, documentHandle));
    return fd != null ? fd.getDocumentData() : null;
  }

  @Override
  public ChecksumData getChecksum(String reportSpace, String valueDate) {
    return null;
  }


  @Override
  public DocumentData setDocument(DocumentData document, FileData data) {
    if (document != null) {
      FileDocument fd = documentMap.get(getDocumentKey(document.getReportSpace(), document.getDocumentHandle()));
      if (fd != null && data == null) {
        data = fd.getData();
      }
      FileDocument doc = new FileDocument(data, document);
      documentMap.put(getDocumentKey(document.getReportSpace(), document.getDocumentHandle()), doc);
      return documentMap.get(getDocumentKey(document.getReportSpace(), document.getDocumentHandle())).getDocumentData();
    }
    return null;
  }

  @Override
  public ChecksumData setChecksum(ChecksumData document) {
    return null;
  }


  @Override
  public ReportInfoDownload getReports(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    ReportInfoDownload result = null;
    SortedMap<Long, String> repInstanceReports;
    Long longFromSyncPoint = 0L;
    try {
      longFromSyncPoint = (fromSyncPoint != null && !fromSyncPoint.isEmpty()) ? Long.parseLong(fromSyncPoint) : longFromSyncPoint;
    } catch (Exception err) {
      //ignore any parsing errors here and use default.
    }
    if (reportDownLoadMap != null && reportDownLoadMap.size() > 0 && reportDownLoadMap.containsKey(reportSpace)) {
      repInstanceReports = reportDownLoadMap.get(reportSpace).subMap(longFromSyncPoint + 1, longFromSyncPoint + maxRecords + 1);
      Collection<String> handles = repInstanceReports.values();
      List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
      for (String handle : handles) {
        //max records enforcement...
        if (maxRecords >= 0 && reportInfoList.size() == maxRecords) {
          break;
        }
        reportInfoList.add(reportInfoMap.get(handle));
      }
      result = new ReportInfoDownload(
          repInstanceReports.size() > 0 ? repInstanceReports.lastKey().toString() : fromSyncPoint,
          reportInfoList);
    }
    return result;
  }


  /**
   * This returns a map of transactionKey -> ReportInfo
   * with the latest ReportInfo object for a specified
   * reporting space.
   *
   * @param reportSpace
   * @return
   */
  private Map<String, ReportInfo> getLatestReports(String reportSpace) {
    Map<String, ReportInfo> result = new HashMap<String, ReportInfo>();
    List<String> reportHandleList = null;
    List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
    for (Map.Entry<String, List<String>> entity : reportIndex.entrySet()) {
      String transactionKey = entity.getKey();
      String latestReportHandle = entity.getValue().get(entity.getValue().size() - 1);
      ReportInfo reportInfo = reportInfoMap.get(latestReportHandle);
      if (reportInfo.getReportSpace().equals(reportSpace)) {
        result.put(transactionKey, reportInfo);
      }
    }
    return result;
  }


  @Override
  public DecisionDownload getDecisions(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    //TODO: add security permissions/criteria checking here!

    DecisionDownload result = null;
    SortedMap<Long, String> repInstanceDecisions;
    Long longFromSyncPoint = (fromSyncPoint != null) ? Long.parseLong(fromSyncPoint) : 0L;
    if (decisionDownLoadMap.containsKey(reportSpace)) {
      repInstanceDecisions = decisionDownLoadMap.get(reportSpace).subMap(longFromSyncPoint + 1, longFromSyncPoint + maxRecords + 1);
      Collection<String> handles = repInstanceDecisions.values();
      List<FullyReferencedEvaluationDecision> decisionList = new ArrayList<FullyReferencedEvaluationDecision>();
      for (String handle : handles) {
        decisionList.add(decisionMap.get(handle));
      }
      result = new DecisionDownload(
          repInstanceDecisions.size() > 0 ? repInstanceDecisions.lastKey().toString() : fromSyncPoint,
          decisionList);
    }
    return result;
  }


  @Override
  public DocumentDataDownload getDocuments(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    //TODO: add security permissions/criteria checking here!

    DocumentDataDownload result = null;
    SortedMap<Long, String> repInstanceDocuments;
    Long longFromSyncPoint = (fromSyncPoint != null) ? Long.parseLong(fromSyncPoint) : 0L;
    if (documentDownLoadMap.containsKey(reportSpace)) {
      repInstanceDocuments = documentDownLoadMap.get(reportSpace).subMap(longFromSyncPoint + 1, longFromSyncPoint + maxRecords + 1);
      Collection<String> handles = repInstanceDocuments.values();
      List<DocumentData> documentList = new ArrayList<DocumentData>();
      for (String handle : handles) {
        documentList.add(documentMap.get(handle).getInfo());
      }
      result = new DocumentDataDownload(
          (repInstanceDocuments.size() > 0 ? repInstanceDocuments.lastKey().toString() : fromSyncPoint),
          documentList);
    }
    return result;
  }

  @Override
  public AccountEntryDownload getAccountEntries(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return null;
  }

  @Override
  public ChecksumDataDownload getChecksums(String reportSpace, String fromSyncPoint, Integer maxRecords) {
    return null;
  }


  private boolean reportMatchesCriteria(ReportInfo report, Map<String, List<CriterionData>> criteria, boolean failIfEmptyCriteria) {
    boolean matches = true;
    if (criteria != null && criteria.size() > 0) {
      for (Map.Entry<String, List<CriterionData>> entry : criteria.entrySet()) {

        boolean hasOrMatched = false;
        for (CriterionData criterion : entry.getValue()) {
          List<String> criterionValues = criterion.getRuntimeValues();
          for (String criterionValue : criterionValues) {
            Object val = DataLogic.getFieldValue(report, criterion.getType(), criterion.getName());
            if (val instanceof List) {
              for (Object valInst : (List<Object>) val) {
                if (valInst != null && valInst.equals(criterionValue)) {
                  hasOrMatched = true;
                  break;
                }
              }
            } else {
              if (val != null && val.equals(criterionValue)) {
                hasOrMatched = true;
                break;
              }
            }
          }
        }
        if (!hasOrMatched) {
          matches = false;
          break;
        }
      }
    } else {
      matches = !failIfEmptyCriteria;
    }
    return matches;
  }

  private Map<String, ReportInfo> getLatestReportsThatMatch(String reportSpace, List<CriterionData> criteria) {
    Map<String, ReportInfo> matchedReportList = null;
    Map<String, List<CriterionData>> mappedCriteria = DataLogic.mapByCriterionName(criteria);
    Map<String, ReportInfo> reports = getLatestReports(reportSpace);
    if (reports != null && reports.size() > 0) {
      for (Map.Entry<String, ReportInfo> entry : reports.entrySet()) {
        if (reportMatchesCriteria(entry.getValue(), mappedCriteria, false)) {
          if (matchedReportList == null)
            matchedReportList = new HashMap<String, ReportInfo>();
          matchedReportList.put(entry.getKey(), entry.getValue());
        }
      }
    }
    return matchedReportList;
  }


  public Map<String, ReportInfo> getReportMapFilteredByUserAndList(String reportSpace, ListData listDefinition, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    Map<String, ReportInfo> filteredReportMap = new HashMap<String, ReportInfo>();

    for (AccessSetData accessSetData : userAccessData.getAccessList()) {
      List<CriterionData> criteria = new ArrayList<CriterionData>();
      criteria.addAll(accessSetData.getAccessCriteria());
      criteria.addAll(listDefinition.getCriteria());

      Map<String, ReportInfo> reportMap = getLatestReportsThatMatch(reportSpace, criteria);
      if (reportMap != null && reportMap.size() > 0) {
        for (Map.Entry<String, ReportInfo> entry : reportMap.entrySet()) {
          if (!filteredReportMap.containsKey(entry.getKey())) {
            filteredReportMap.put(entry.getKey(), entry.getValue());
          }
        }
      }
    }
    return filteredReportMap;
  }


  @Override
  public ListResult getReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    Map<String, ReportInfo> mainReportMap = getReportMapFilteredByUserAndList(reportSpace, listDefinition, userAccessData, maxRecords, page);

    return DataLogic.convertToListResult(listDefinition, mainReportMap.values());
  }


  @Override
  public ListResult getOrderedReportList(String reportSpace, ListData listDefinition, Map<String, String> searchMap, SortDefinition sortDefinition, UserAccessData userAccessData, Integer maxRecords, Integer page) {
    Map<String, ReportInfo> filteredReportMap = getReportMapFilteredByUserAndList(reportSpace, listDefinition, userAccessData, maxRecords, page);
    SortedMap<SortingKeyList, ReportInfo> sortedMap = new TreeMap<SortingKeyList, ReportInfo>();
    for (ReportInfo report : filteredReportMap.values()) {
      SortingKeyList sortingKeys = new SortingKeyList(sortDefinition.getFields(), report);
      sortedMap.put(sortingKeys, report);
    }
    return DataLogic.convertToListResult(listDefinition, sortedMap.values());
  }

  @Override
  public void addNotification(NotificationData notification) {

  }

  @Override
  public List<NotificationData> getNotifications() {
    return null;
  }

  @Override
  public void removeNotification(NotificationData notification) {

  }

  @Override
  public void changeReportKey(String reportSpace, String trnReference, String newTrnReference) {
    String transactionKey = getKey(reportSpace, trnReference);
    String newTransactionKey = getKey(reportSpace, newTrnReference);
    List<String> reportHandles = reportIndex.get(transactionKey);
    List<String> decisionHandles = decisionIndex.get(transactionKey);
    List<String> documentHandles = reportDocumentIndex.get(transactionKey);

    //try a URL decode if the reference doesn't return a value...
    if (reportHandles == null && trnReference.indexOf("%") >= 0) {
      try {
        String decodedReportKey = getKey(reportSpace, URLDecoder.decode(trnReference, "UTF-8"));
        reportHandles = reportIndex.get(decodedReportKey);
        decisionHandles = decisionIndex.get(decodedReportKey);
        documentHandles = reportDocumentIndex.get(decodedReportKey);
      } catch (Exception e) {
      }
    }

    if (reportHandles != null) {
      reportIndex.put(newTransactionKey, reportHandles);
      ReportInfo report = reportInfoMap.get(reportHandles.get(reportHandles.size() - 1));
      report.getReportData().getReport().put(FieldConstant.TrnReference, newTrnReference);
      setReport(reportSpace, newTrnReference, report, false);
    }
    if (decisionHandles != null) {
      decisionIndex.put(newTransactionKey, decisionHandles);
    }
    if (documentHandles != null) {
      ArrayList<String> revisedDocumentHandles = new ArrayList<String>();
      for (String docHandle : documentHandles) {
        String revisedHandle = docHandle.replace(trnReference, newTrnReference);
        documentMap.put(revisedHandle, documentMap.get(docHandle));
        revisedDocumentHandles.add(revisedHandle);
      }
      reportDocumentIndex.put(newTransactionKey, revisedDocumentHandles);
    }
  }

  @Override
  public void logActivity(LogActivityType activityType, LogReadWrite ioAction, String Url, String httpAction, String reportSpace, String trnReference, String logBody) {
    //DO NOTHING FOR THE MEMORY DATA STORE.
  }
}

