package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.DocumentInfo;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceAccessAuthorizationException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by jake on 3/31/16.
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/document")
@MultipartConfig(maxFileSize = 10485760, maxRequestSize = 15728640)
public class Document extends ControllerBase {
  static final Logger logger = LoggerFactory.getLogger(Document.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private DataLogic dataLogic;

  /**
   * Store a document in the specified reporting space, with the provided document handle.
   *
   * @param channelName    The channelName to use to derive the relevant reportSpace into which the document should be inserted.
   * @param documentHandle The unique reference for the required document - comprised of trnReference, monetary amount index, import-export index and document type
   * @param file
   * @return
   * @throws IOException
   */
  @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  DocumentInfo setDocument(
      @RequestParam String channelName,
      @RequestParam String documentHandle,
      @RequestParam MultipartFile file) throws IOException, ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setDocument()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    String reportSpace = (channel != null ? channel.getReportSpace() : null);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be configured for the given channel");
    }
//    if (dataLogic.isMimeTypeWhiteListed(file)) {
    if (dataLogic.isWhiteListed(file)) {
      String fileName = file.getOriginalFilename();
      if (!dataLogic.isNameSafe(file.getOriginalFilename())) {
        try {
          fileName = dataLogic.makeNameSafe(file.getOriginalFilename());
        } catch (Exception e) {
          throw new PreconditionFailedException(e.getMessage());
        }
      }
      FileData data = new FileData(file.getContentType(), fileName, file.getBytes());
      return dataLogic.setDocumentContent(reportSpace, documentHandle, data);
    }
    throw new PreconditionFailedException("Invalid file type (or derived mime type) provided");
  }


  /**
   * Store an Adhoc document in the specified reporting space, with the provided document handle - related to the specified trnReference.
   *
   * @param channelName    The channelName to use to derive the relevant reportSpace into which the document should be inserted.
   * @param trnReference   The reference for the related transaction
   * @param documentId     The unique reference for the required document (will overwrite/any previously documents with the same reportSpace, trnReference and documentHandle).
   * @param description    A free-text field to provide a description or additional instruction regarding the file.
   * @param file
   * @return
   * @throws IOException
   */
  @RequestMapping(value = "/adhoc", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  DocumentInfo setAdhocDocument(
      @RequestParam String channelName,
      @RequestParam String trnReference,
      @RequestParam String documentId,
      @RequestParam(required = false) String description,
      @RequestBody MultipartFile file) throws IOException, ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setAdhocDocument()");

    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    String reportSpace = (channel != null ? channel.getReportSpace() : null);
    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid channelName must be specified");

    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid reportSpace must be configured for the given channel");
//    if (dataLogic.isMimeTypeWhiteListed(file)) {
    if (dataLogic.isWhiteListed(file)) {
      String fileName = file.getOriginalFilename();
      if (!dataLogic.isNameSafe(file.getOriginalFilename())) {
        try {
          fileName = dataLogic.makeNameSafe(file.getOriginalFilename());
        } catch (Exception e) {
          throw new PreconditionFailedException(e.getMessage());
        }
      }
      FileData data = new FileData(file.getContentType(), fileName, file.getBytes());
      return dataLogic.setDocumentContent(reportSpace, channel, trnReference, documentId, data, description);
    }
    throw new PreconditionFailedException("Invalid file type (or derived mime type) provided");
  }


  /**
   * This end-point will return any <b><i>potentially</i></b> required documents for a given channel, category, flow and section.
   * The list of documents is determined by performing an exhaustive scan of all document requirement scenarios for the provided channel, flow, category and section (~Reporting Qualifier).
   * <b><i>This API will return every possible document required for the given 4 parameters</i></b> - it will not be a specific and precise list as per the more definitive Document validation which takes into account any BOP data provided to narrow down the list to only those required for the specific scenario and data.
   *
   * @param channelName - the channel's name
   * @param category    - compound category code, eg "401" or "101/01"
   * @param flow        - flow - either "OUT" or "IN"
   * @param section     - the Regulatory section, e.g. "A" or "C".
   * @return A list of (potentially) required document types with accompanying message.
   * @throws Exception
   */
  @RequestMapping(value = "/potential", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getPotentialDocs(
      @RequestParam String channelName,
      @RequestParam String category,
      @RequestParam(required = true) String flow,
      @RequestParam(required = false) String section) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getPotentialDocs()");

    return dataLogic.getPotentialDocs(channelName, category, flow, section);

  }


  /**
   * This end-point is used to acknowledge an excon document requirement as being "complete" or "approved".
   * The document object need not be uploaded for this to take effect, and could be uploaded after the fact as well if so desired.
   * Typically, this would be used in cases where the excon document process is performed outside of the system, against a separate document store - but where the acknowledgement must be provided in order for the configured report lifecycle to move on to the next state (ie: all required docs are acknowledged and are thus approved and stored in the external system).
   *
   * @param channelName            The channelName to use to derive the relevant reportSpace into which to apply the document acknowledgement.
   * @param documentHandle         The unique reference for the required document - comprised of trnReference, monetary amount index, import-export index and document type
   * @param acknowledgement        The boolean acknowledgement flag (true or false)
   * @param acknowledgementComment A related comment for the acknowledgment - reasons for failing acknowledgement, additional notes or meta information.
   * @return a JSON object containing a DocumentHandle, Scope, reportSpace, TrnReferece, Type, Description, Sequence, SubSequence, Uploaded, FileName and FileType of the selected document
   * @throws IOException
   */
  @RequestMapping(value = "/acknowledgement", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  DocumentInfo setDocumentAcknowledgement(
      @RequestParam String channelName,
      @RequestParam String documentHandle,
      @RequestParam boolean acknowledgement,
      @RequestParam(required = false) String acknowledgementComment) throws IOException, ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setDocumentAcknowledgement()");

    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    String reportSpace = (channel != null ? channel.getReportSpace() : null);
    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid channelName must be specified");

    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid reportSpace must be configured for the given channel");

    return setDocumentAcknowledgement(reportSpace, documentHandle, acknowledgement, acknowledgementComment,
        dataLogic, systemInformation);
  }


  public static DocumentInfo setDocumentAcknowledgement(
      String reportSpace,
      String documentHandle,
      boolean acknowledgement,
      String acknowledgementComment,
      DataLogic dataLogic,
      SystemInformation systemInformation) throws IOException {
    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid reportSpace must be specified");

    DocumentInfo info = dataLogic.getDocument(reportSpace, documentHandle);
    FileData data = dataLogic.getDocumentContent(reportSpace, documentHandle);
    if (info == null) {
      info = dataLogic.getDocumentInfoFromDocumentData(
          systemInformation.getConfigStore(),
          dataLogic.getDataStore(),
          dataLogic.getDocumentDataObjFromHandle(dataLogic.getDefaultChannel(reportSpace), documentHandle)
      );
    }
    if (info != null) {
      info.setAcknowledged(new Boolean(acknowledgement));
      info.setAcknowledgedComment(acknowledgementComment);
      return dataLogic.setDocument(info, data);
    }
    throw new ResourceNotFoundException("Unable to locate or set the document specified.");
  }

  /**
   * Retrieve the last stored instance of the document stored with the specified document handle.
   *
   * @param channelName    The channel relating to the report space from which to fetch the document object
   * @param documentHandle The unique reference for the required document - comprised of trnReference, monetary amount index, import-export index and document type
   * @param response       This will return the document object
   * @throws Exception
   */
  @RequestMapping(value = "", method = RequestMethod.GET)
  public
  @ResponseBody
  void getDocument(
      @RequestParam String channelName,
      @RequestParam String documentHandle,
      HttpServletResponse response) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getDocument()");
    getDocumentContent(systemInformation, dataLogic, channelName, documentHandle, response);
  }


  /**
   * Retrieves a list of specifically required documents based on the report data already submitted and stored in the system - with the trnReference provided.
   *
   * @param channelName  The channel providing the relevant document evaluation rules and relating to the report space from which to fetch the report data (based on trnReference)
   * @param trnReference The trnReference for the report to evaluate for document requirements
   * @return
   */
  @RequestMapping(value = "/report", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<DocumentInfo> getDocumentInfoListForReportRef(
      @RequestParam String channelName,
      @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getDocumentInfoListForReportRef()");
    return dataLogic.getDocumentInfoForReportOrTrnReference(channelName, trnReference, null);
  }


  /**
   * Retrieves a list of specifically required documents based on the report data provided on the POST request.
   *
   * @param channelName The channel package name containing the relevant document evaluation and validation rules
   * @param report      The report data against which to perform document evaluation and validation to retrieve the required documents list
   * @return
   */
  @RequestMapping(value = "/report", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  List<DocumentInfo> getDocumentInfoListForReport(
      @RequestParam String channelName,
      @RequestBody ReportData report) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getDocumentInfoListForReport()");
    return dataLogic.getDocumentInfoForReportOrTrnReference(channelName, null, report);
  }


  /**
   * Retrieves the list of available documents and document objects in the system, for the provided trnReference, in the report space related to the channel specified.
   *
   * @param channelName  The channel providing the relevant document evaluation rules and relating to the report space from which to fetch the report data (based on trnReference)
   * @param trnReference The trnReference of the report for which the required and available documents list should be generated
   * @return
   */
  @RequestMapping(value = "/documents", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  List<DocumentInfo> getReportDocuments(@RequestParam String channelName,
                                        @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReportDocuments()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    String reportSpace = (channel != null ? channel.getReportSpace() : null);
    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid channelName must be specified");

    List<DocumentInfo> documentList = dataLogic.getDocumentsForReport(channel, trnReference);
    if (documentList == null)
      throw new ResourceNotFoundException("No Report Documents for '" + trnReference + "@" + reportSpace + "' found");

    return documentList;
  }


  /**
   * Retrieves the list of available document objects in the system, for the provided trnReference, in the report space related to the channel specified.
   *
   * @param channelName  The channel providing the relevant document evaluation rules and relating to the report space from which to fetch the report data (based on trnReference)
   * @param trnReference The trnReference of the report for which the required and available documents list should be generated
   * @return
   */
  @RequestMapping(value = "/uploaded", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  List<DocumentInfo> getUploadedReportDocuments(@RequestParam String channelName,
                                                @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getUploadedReportDocuments()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    String reportSpace = (channel != null ? channel.getReportSpace() : null);
    if (!systemInformation.isValidReportSpace(reportSpace))
      throw new PreconditionFailedException("A valid channelName must be specified");

    List<DocumentInfo> documentList = dataLogic.getUploadedDocumentsForTrnReference(channel, trnReference);
    if (documentList == null)
      throw new ResourceNotFoundException("No Report Documents for '" + trnReference + "@" + reportSpace + "' found");

    return documentList;
  }


  public static void getDocumentContent(SystemInformation systemInformation, DataLogic dataLogic, String channelName, String documentHandle, HttpServletResponse response) throws Exception {
    if (channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      if (channel != null) {
        if (dataLogic.hasDocument(channel.getReportSpace(), documentHandle)) {
          FileData data = dataLogic.getDocumentContent(channel.getReportSpace(), documentHandle);
          if (data != null && data.getContent() != null && data.getContentType() != null) {
            response.setContentType(data.getContentType());
            response.getOutputStream().write(data.getContent());
            response.getOutputStream().flush();
            return;
          } else {
            throw new ResourceNotFoundException("Document content '" + documentHandle + "' is not set");
          }
        } else {
          throw new ResourceNotFoundException("Document Handle '" + documentHandle + "' not found");
        }
      }
    }
    throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
  }

}
