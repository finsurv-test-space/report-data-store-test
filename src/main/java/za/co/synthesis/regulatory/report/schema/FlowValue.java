package za.co.synthesis.regulatory.report.schema;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.FlowType;

@JsonPropertyOrder({
    "Flow"
})
public enum FlowValue {
  Unknown("Unknown", FlowType.Unknown),
  IN("IN", FlowType.Inflow),
  OUT("OUT", FlowType.Outflow);

  private String value;
  private FlowType flowType;

  FlowValue(String value, FlowType flowType) {
    this.value=value;
    this.flowType = flowType;
  }

  @JsonProperty("Flow")
  public String toString(){
    return this.value;
  }
  public FlowType getFlowType(){
    return this.flowType;
  }

  public static FlowValue fromString(String in){
    if(in != null) {
      for(FlowValue eventType : FlowValue.values()) {
        if (in.equalsIgnoreCase(eventType.value)) {
          return eventType;
        }
      }
    }
    return FlowValue.Unknown;
  }

  public static FlowValue fromFlowType(FlowType in){
    if(in != null) {
      for(FlowValue eventType : FlowValue.values()) {
        if (in.toString().equalsIgnoreCase(eventType.value)) {
          return eventType;
        }
      }
    }
    return FlowValue.Unknown;
  }
}