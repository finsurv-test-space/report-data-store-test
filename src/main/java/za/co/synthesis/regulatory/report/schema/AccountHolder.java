package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "Reference"
        , "Individual"
        , "Entity"
})
public class AccountHolder {
  @JsonPropertyOrder({
          "AddressLine1"
          , "AddressLine2"
          , "Suburb"
          , "City"
          , "Province"
          , "PostalCode"
  })
  public static class StreetAddress {
    private String addressLine1;
    private String addressLine2;
    private String suburb;
    private String city;
    private String province;
    private String postalCode;

    @JsonProperty("AddressLine1")
    public String getAddressLine1() { return addressLine1; }
    @JsonProperty("AddressLine1")
    public void setAddressLine1(String value) { this.addressLine1 = value; }
    @JsonProperty("AddressLine2")
    public String getAddressLine2() { return addressLine2; }
    @JsonProperty("AddressLine2")
    public void setAddressLine2(String value) { this.addressLine2 = value; }
    @JsonProperty("Suburb")
    public String getSuburb() { return suburb; }
    @JsonProperty("Suburb")
    public void setSuburb(String value) { this.suburb = value; }
    @JsonProperty("City")
    public String getCity() { return city; }
    @JsonProperty("City")
    public void setCity(String value) { this.city = value; }
    @JsonProperty("Province")
    public String getProvince() { return province; }
    @JsonProperty("Province")
    public void setProvince(String value) { this.province = value; }
    @JsonProperty("PostalCode")
    public String getPostalCode() { return postalCode; }
    @JsonProperty("PostalCode")
    public void setPostalCode(String value) { this.postalCode = value; }
  }

  @JsonPropertyOrder({
          "AddressLine1"
          , "AddressLine2"
          , "Suburb"
          , "City"
          , "Province"
          , "PostalCode"
  })
  public static class PostalAddress {
    private String addressLine1;
    private String addressLine2;
    private String suburb;
    private String city;
    private String province;
    private String postalCode;

    @JsonProperty("AddressLine1")
    public String getAddressLine1() { return addressLine1; }
    @JsonProperty("AddressLine1")
    public void setAddressLine1(String value) { this.addressLine1 = value; }
    @JsonProperty("AddressLine2")
    public String getAddressLine2() { return addressLine2; }
    @JsonProperty("AddressLine2")
    public void setAddressLine2(String value) { this.addressLine2 = value; }
    @JsonProperty("Suburb")
    public String getSuburb() { return suburb; }
    @JsonProperty("Suburb")
    public void setSuburb(String value) { this.suburb = value; }
    @JsonProperty("City")
    public String getCity() { return city; }
    @JsonProperty("City")
    public void setCity(String value) { this.city = value; }
    @JsonProperty("Province")
    public String getProvince() { return province; }
    @JsonProperty("Province")
    public void setProvince(String value) { this.province = value; }
    @JsonProperty("PostalCode")
    public String getPostalCode() { return postalCode; }
    @JsonProperty("PostalCode")
    public void setPostalCode(String value) { this.postalCode = value; }
  }

  @JsonPropertyOrder({
          "ContactSurname"
          , "ContactName"
          , "Email"
          , "Fax"
          , "Telephone"
  })
  public static class ContactDetails {
    private String contactSurname;
    private String contactName;
    private String email;
    private String fax;
    private String telephone;

    @JsonProperty("ContactSurname")
    public String getContactSurname() { return contactSurname; }
    @JsonProperty("ContactSurname")
    public void setContactSurname(String value) { this.contactSurname = value; }
    @JsonProperty("ContactName")
    public String getContactName() { return contactName; }
    @JsonProperty("ContactName")
    public void setContactName(String value) { this.contactName = value; }
    @JsonProperty("Email")
    public String getEmail() { return email; }
    @JsonProperty("Email")
    public void setEmail(String value) { this.email = value; }
    @JsonProperty("Fax")
    public String getFax() { return fax; }
    @JsonProperty("Fax")
    public void setFax(String value) { this.fax = value; }
    @JsonProperty("Telephone")
    public String getTelephone() { return telephone; }
    @JsonProperty("Telephone")
    public void setTelephone(String value) { this.telephone = value; }
  }

  @JsonPropertyOrder({
          "Surname"
          , "Name"
          , "Gender"
          , "DateOfBirth"
          , "IDNumber"
          , "TempResPermitNumber"
          , "ForeignIDNumber"
          , "ForeignIDCountry"
          , "PassportNumber"
          , "PassportCountry"
          , "AccountName"
          , "AccountIdentifier"
          , "AccountNumber"
          , "CustomsClientNumber"
          , "TaxNumber"
          , "VATNumber"
          , "StreetAddress"
          , "PostalAddress"
          , "ContactDetails"
  })
  public static class Individual {
    private String surname;
    private String name;
    private String gender;
    private String dateOfBirth;
    private String idNumber;
    private String tempResPermitNumber;
    private String foreignIDNumber;
    private String foreignIDCountry;
    private String passportNumber;
    private String passportCountry;
    private String accountName;
    private String accountIdentifier;
    private String accountNumber;
    private String customsClientNumber;
    private String taxNumber;
    private String vatNumber;
    private StreetAddress streetAddress;
    private PostalAddress postalAddress;
    private ContactDetails contactDetails;

    @JsonProperty("Surname")
    public String getSurname() { return surname; }
    @JsonProperty("Surname")
    public void setSurname(String value) { this.surname = value; }
    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }
    @JsonProperty("Gender")
    public String getGender() { return gender; }
    @JsonProperty("Gender")
    public void setGender(String value) { this.gender = value; }
    @JsonProperty("DateOfBirth")
    public String getDateOfBirth() { return dateOfBirth; }
    @JsonProperty("DateOfBirth")
    public void setDateOfBirth(String value) { this.dateOfBirth = value; }
    @JsonProperty("IDNumber")
    public String getIDNumber() { return idNumber; }
    @JsonProperty("IDNumber")
    public void setIDNumber(String value) { this.idNumber = value; }
    @JsonProperty("TempResPermitNumber")
    public String getTempResPermitNumber() { return tempResPermitNumber; }
    @JsonProperty("TempResPermitNumber")
    public void setTempResPermitNumber(String value) { this.tempResPermitNumber = value; }
    @JsonProperty("ForeignIDNumber")
    public String getForeignIDNumber() { return foreignIDNumber; }
    @JsonProperty("ForeignIDNumber")
    public void setForeignIDNumber(String value) { this.foreignIDNumber = value; }
    @JsonProperty("ForeignIDCountry")
    public String getForeignIDCountry() { return foreignIDCountry; }
    @JsonProperty("ForeignIDCountry")
    public void setForeignIDCountry(String value) { this.foreignIDCountry = value; }
    @JsonProperty("PassportNumber")
    public String getPassportNumber() { return passportNumber; }
    @JsonProperty("PassportNumber")
    public void setPassportNumber(String value) { this.passportNumber = value; }
    @JsonProperty("PassportCountry")
    public String getPassportCountry() { return passportCountry; }
    @JsonProperty("PassportCountry")
    public void setPassportCountry(String value) { this.passportCountry = value; }
    @JsonProperty("AccountName")
    public String getAccountName() { return accountName; }
    @JsonProperty("AccountName")
    public void setAccountName(String value) { this.accountName = value; }
    @JsonProperty("AccountIdentifier")
    public String getAccountIdentifier() { return accountIdentifier; }
    @JsonProperty("AccountIdentifier")
    public void setAccountIdentifier(String value) { this.accountIdentifier = value; }
    @JsonProperty("AccountNumber")
    public String getAccountNumber() { return accountNumber; }
    @JsonProperty("AccountNumber")
    public void setAccountNumber(String value) { this.accountNumber = value; }
    @JsonProperty("CustomsClientNumber")
    public String getCustomsClientNumber() { return customsClientNumber; }
    @JsonProperty("CustomsClientNumber")
    public void setCustomsClientNumber(String value) { this.customsClientNumber = value; }
    @JsonProperty("TaxNumber")
    public String getTaxNumber() { return taxNumber; }
    @JsonProperty("TaxNumber")
    public void setTaxNumber(String value) { this.taxNumber = value; }
    @JsonProperty("VATNumber")
    public String getVATNumber() { return vatNumber; }
    @JsonProperty("VATNumber")
    public void setVATNumber(String value) { this.vatNumber = value; }
    @JsonProperty("StreetAddress")
    public StreetAddress getStreetAddress() { return streetAddress; }
    @JsonProperty("StreetAddress")
    public void setStreetAddress(StreetAddress value) { this.streetAddress = value; }
    @JsonProperty("PostalAddress")
    public PostalAddress getPostalAddress() { return postalAddress; }
    @JsonProperty("PostalAddress")
    public void setPostalAddress(PostalAddress value) { this.postalAddress = value; }
    @JsonProperty("ContactDetails")
    public ContactDetails getContactDetails() { return contactDetails; }
    @JsonProperty("ContactDetails")
    public void setContactDetails(ContactDetails value) { this.contactDetails = value; }
  }

  @JsonPropertyOrder({
          "EntityName"
          , "TradingName"
          , "RegistrationNumber"
          , "InstitutionalSector"
          , "IndustrialClassification"
          , "AccountName"
          , "AccountIdentifier"
          , "AccountNumber"
          , "CustomsClientNumber"
          , "TaxNumber"
          , "VATNumber"
          , "StreetAddress"
          , "PostalAddress"
          , "ContactDetails"
  })
  public static class Entity {
    private String entityName;
    private String tradingName;
    private String registrationNumber;
    private String institutionalSector;
    private String industrialClassification;
    private String accountName;
    private String accountIdentifier;
    private String accountNumber;
    private String customsClientNumber;
    private String taxNumber;
    private String vatNumber;
    private StreetAddress streetAddress;
    private PostalAddress postalAddress;
    private ContactDetails contactDetails;

    @JsonProperty("EntityName")
    public String getEntityName() { return entityName; }
    @JsonProperty("EntityName")
    public void setEntityName(String value) { this.entityName = value; }
    @JsonProperty("TradingName")
    public String getTradingName() { return tradingName; }
    @JsonProperty("TradingName")
    public void setTradingName(String value) { this.tradingName = value; }
    @JsonProperty("RegistrationNumber")
    public String getRegistrationNumber() { return registrationNumber; }
    @JsonProperty("RegistrationNumber")
    public void setRegistrationNumber(String value) { this.registrationNumber = value; }
    @JsonProperty("InstitutionalSector")
    public String getInstitutionalSector() { return institutionalSector; }
    @JsonProperty("InstitutionalSector")
    public void setInstitutionalSector(String value) { this.institutionalSector = value; }
    @JsonProperty("IndustrialClassification")
    public String getIndustrialClassification() { return industrialClassification; }
    @JsonProperty("IndustrialClassification")
    public void setIndustrialClassification(String value) { this.industrialClassification = value; }
    @JsonProperty("AccountName")
    public String getAccountName() { return accountName; }
    @JsonProperty("AccountName")
    public void setAccountName(String value) { this.accountName = value; }
    @JsonProperty("AccountIdentifier")
    public String getAccountIdentifier() { return accountIdentifier; }
    @JsonProperty("AccountIdentifier")
    public void setAccountIdentifier(String value) { this.accountIdentifier = value; }
    @JsonProperty("AccountNumber")
    public String getAccountNumber() { return accountNumber; }
    @JsonProperty("AccountNumber")
    public void setAccountNumber(String value) { this.accountNumber = value; }
    @JsonProperty("CustomsClientNumber")
    public String getCustomsClientNumber() { return customsClientNumber; }
    @JsonProperty("CustomsClientNumber")
    public void setCustomsClientNumber(String value) { this.customsClientNumber = value; }
    @JsonProperty("TaxNumber")
    public String getTaxNumber() { return taxNumber; }
    @JsonProperty("TaxNumber")
    public void setTaxNumber(String value) { this.taxNumber = value; }
    @JsonProperty("VATNumber")
    public String getVATNumber() { return vatNumber; }
    @JsonProperty("VATNumber")
    public void setVATNumber(String value) { this.vatNumber = value; }
    @JsonProperty("StreetAddress")
    public StreetAddress getStreetAddress() { return streetAddress; }
    @JsonProperty("StreetAddress")
    public void setStreetAddress(StreetAddress value) { this.streetAddress = value; }
    @JsonProperty("PostalAddress")
    public PostalAddress getPostalAddress() { return postalAddress; }
    @JsonProperty("PostalAddress")
    public void setPostalAddress(PostalAddress value) { this.postalAddress = value; }
    @JsonProperty("ContactDetails")
    public ContactDetails getContactDetails() { return contactDetails; }
    @JsonProperty("ContactDetails")
    public void setContactDetails(ContactDetails value) { this.contactDetails = value; }
  }

  private String reference;
  private Individual individual;
  private Entity entity;

  @JsonProperty("Reference")
  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  @JsonProperty("Individual")
  public Individual getIndividual() {
    return individual;
  }

  public void setIndividual(Individual individual) {
    this.individual = individual;
  }

  @JsonProperty("Entity")
  public Entity getEntity() {
    return entity;
  }

  public void setEntity(Entity entity) {
    this.entity = entity;
  }
}
