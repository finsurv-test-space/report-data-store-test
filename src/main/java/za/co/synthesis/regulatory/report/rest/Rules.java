package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ChecksumData;
import za.co.synthesis.regulatory.report.schema.ChecksumVerification;
import za.co.synthesis.regulatory.report.support.IChannelCache;
import za.co.synthesis.regulatory.report.support.PackageCache;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

/**
 * User: jake
 * Date: 5/1/16
 * Time: 8:53 AM
 * This REST service provides access to all the evaluation, validation and lookup rules that are each consolidated into
 * a single file based on the configuration system configuration
 */
@Controller
@RequestMapping("producer/api/rules")
public class Rules {
  Logger logger = LoggerFactory.getLogger(Rules.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private JsonSchemaTransform jsonSchemaTransform;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private IChannelCache channelCache;

  @Autowired
  private RulesCache rulesCache;


  /**
   * Authentication-less API to download the channel-specific validation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the validation rules should be fetched.
   * @return a JSON file containing all the validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/validation", method = RequestMethod.GET, produces = "application/javascript")
  public @ResponseBody
  String getValidations(@PathVariable String channelName,
                        HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry validationRules = rulesCache.getValidationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (validationRules == null){
      throw new Exception("Requested validation rule set not found.");
    }
    String dataStr = validationRules.getData();
    dataLogic.addChecksumHeaders(dataStr, response);
    return dataStr;
  }

  /**
   * Authentication-less API to download the channel-specific validation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the validation rules should be fetched.
   * @return a JSON file containing all the validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/validation", method = RequestMethod.GET, produces = "application/javascript")
  public void getValidationsFile(@PathVariable String channelName,
                        HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry validationRules = rulesCache.getValidationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (validationRules == null){
      throw new Exception("Requested validation rule set not found.");
    }
    String dataStr = validationRules.getData();
    if (dataStr.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=validation.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(dataStr, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(dataStr);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available validation.js file to download.");
    }
  }


  /**
   * Authentication-less API to download the channel-specific external validation call information (end-point + params etc).
   * When using the stand-alone, back-end Rules Engine (Java, Android, IOS etc) the external validation calls need to be configured with this information
   * so that the validation engine is able to dynamically determine which parameters to load and which validation call to make etc.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the doc-sighting validation rules should be fetched.
   * @return a JSON file containing all the doc-sighting validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/externalValidations", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  String getExternalValidations(@PathVariable String channelName,
                                HttpServletResponse response) throws Exception {
    String validations = FormApi.makeExtValidationString(channelName, rulesCache, false);
    dataLogic.addChecksumHeaders(validations, response);
    return validations;
  }

  /**
   * Authentication-less API to download the channel-specific external validation call information (end-point + params etc).
   * When using the stand-alone, back-end Rules Engine (Java, Android, IOS etc) the external validation calls need to be configured with this information
   * so that the validation engine is able to dynamically determine which parameters to load and which validation call to make etc.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the doc-sighting validation rules should be fetched.
   * @return a JSON file containing all the doc-sighting validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/externalValidations", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  void getExternalValidationsFile(@PathVariable String channelName,
                                HttpServletResponse response) throws Exception {
    String validations = FormApi.makeExtValidationString(channelName, rulesCache, false);
    if (validations.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=externalValidations.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(validations, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(validations);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available externalValidations.js file to download.");
    }
  }


  /**
   * Authentication-less API to download the channel-specific (excon) doc-sighting validation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the doc-sighting validation rules should be fetched.
   * @return a JSON file containing all the doc-sighting validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/document", method = RequestMethod.GET, produces = "application/javascript")
  public @ResponseBody
  String getDocuments(@PathVariable String channelName,
                      HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry documentRules = rulesCache.getDocumentRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (documentRules == null){
      throw new Exception("Requested document rule set not found.");
    }
    String dataStr = documentRules.getData();
    dataLogic.addChecksumHeaders(dataStr, response);
    return dataStr;
  }

  /**
   * Authentication-less API to download the channel-specific (excon) doc-sighting validation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the doc-sighting validation rules should be fetched.
   * @return a JSON file containing all the doc-sighting validation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/document", method = RequestMethod.GET, produces = "application/javascript")
  public void getDocumentsFile(@PathVariable String channelName,
                      HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry documentRules = rulesCache.getDocumentRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (documentRules == null){
      throw new Exception("Requested document rule set not found.");
    }
    String dataStr = documentRules.getData();
    if (dataStr.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=document.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(dataStr, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(dataStr);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available document.js file to download.");
    }
  }

  /**
   * Authentication-less API to download the channel-specific evaluation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the evaluation rules should be fetched.
   * @return a JSON file containing all the evaluation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/evaluation", method = RequestMethod.GET, produces = "application/javascript")
  public @ResponseBody String getEvaluations(@PathVariable String channelName,
                                             HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry evaluationRules = rulesCache.getEvaluationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (evaluationRules == null){
      throw new Exception("Requested evaluation rule set not found.");
    }
    String dataStr = evaluationRules.getData();
    dataLogic.addChecksumHeaders(dataStr, response);
    return dataStr;
  }

  /**
   * Authentication-less API to download the channel-specific evaluation rules file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the evaluation rules should be fetched.
   * @return a JSON file containing all the evaluation rules required for the specified channel package.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/evaluation", method = RequestMethod.GET, produces = "application/javascript")
  public void getEvaluationsFile(@PathVariable String channelName,
                                             HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry evaluationRules = rulesCache.getEvaluationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (evaluationRules == null){
      throw new Exception("Requested evaluation rule set not found.");
    }
    String dataStr = evaluationRules.getData();
    if (dataStr.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=evaluation.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(dataStr, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(dataStr);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available evaluation.js file to download.");
    }
  }



  /**
   * Authentication-less API to download the Lookups file used for validations.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the Lookups should be fetched.
   * @return a JSON file containing all the lookups required for the specified channel package validations and form functions.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/lookups", method = RequestMethod.GET, produces = "application/javascript")
  public @ResponseBody String getLookups(@PathVariable String channelName,
                                         HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry lookupsSet = rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (lookupsSet == null){
      throw new Exception("Requested lookups set not found.");
    }
    String dataStr = lookupsSet.getData();
    dataLogic.addChecksumHeaders(dataStr, response);
    return dataStr;
  }

  /**
   * Authentication-less API to download the Lookups file used for validations.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * @param channelName This is the Channel Package (aka channelName) for which the Lookups should be fetched.
   * @return a JSON file containing all the lookups required for the specified channel package validations and form functions.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/lookups", method = RequestMethod.GET, produces = "application/javascript")
  public void getLookupsFile(@PathVariable String channelName,
                                         HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry lookupsSet = rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (lookupsSet == null){
      throw new Exception("Requested lookups set not found.");
    }
    String dataStr = lookupsSet.getData();
    if (dataStr.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=lookups.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(dataStr, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(dataStr);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available lookups.js file to download.");
    }
  }




//  /**
//   * Authentication-less API to download the channel-specific FormJS file.
//   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
//   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
//   * (Overloaded instance of "producer/api/js", no auth required) -- Provides a self-contained javascript artefact to be embedded, which will render the relevant bop form with the relevant rules, partials and styling relating to the specified channel package.
//   * @param channelName The channel package rules to apply in the client-side validations of the data provided, as well as the set of related partials and styling artefacts to package into the resulting javascript
//   * @return the full, self-contained FormJS artefact.
//   * @throws Exception
//   */
//  @Deprecated
//  @RequestMapping(value = "/js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
//  @ResponseStatus(HttpStatus.OK)
//  public
//  @ResponseBody
//  void getFormJSDep(@RequestParam String channelName,
//                 @RequestParam(required = false) Boolean packedRules,
//                 @RequestParam(required = false) Boolean allowNew,
//                 HttpServletResponse response) throws Exception {
//    allowNew = (allowNew == null)?false:allowNew;
//    packedRules = (packedRules == null)?false:packedRules;
//    FormApi.getFormAPI(channelName, null, packedRules, response, systemInformation, dataLogic, channelCache, rulesCache, allowNew);
//
//  }


  /**
   * Authentication-less API to download the channel-specific FormJS file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * (Overloaded instance of "producer/api/js", no auth required) -- Provides a self-contained javascript artefact to be embedded, which will render the relevant bop form with the relevant rules, partials and styling relating to the specified channel package.
   * @param channelName The channel package rules to apply in the client-side validations of the data provided, as well as the set of related partials and styling artefacts to package into the resulting javascript
   * @return the full, self-contained FormJS artefact.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getFormJS(@PathVariable String channelName,
                 @RequestParam(required = false) Boolean packedRules,
                 @RequestParam(required = false) Boolean allowNew,
                 HttpServletResponse response) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
    FormApi.getFormAPI(channelName, null, packedRules, response, systemInformation, dataLogic, channelCache, rulesCache, allowNew);
  }

  /**
   * Authentication-less API to download the channel-specific FormJS file.
   * When caching the FormJS, you will require a lookups JSON, and validation JSON (and possibly an evaluation JSON) to get the form to load with all the necessary functionality.
   * This API will provide checksum hashes (MD5, SHA1, SHA256) in the response header to provide some assurance of and means to check the object integrity.
   * (Overloaded instance of "producer/api/js", no auth required) -- Provides a self-contained javascript artefact to be embedded, which will render the relevant bop form with the relevant rules, partials and styling relating to the specified channel package.
   * @param channelName The channel package rules to apply in the client-side validations of the data provided, as well as the set of related partials and styling artefacts to package into the resulting javascript
   * @return the full, self-contained FormJS artefact.
   * @throws Exception
   */
  @RequestMapping(value = "/{channelName}/download/js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getFormJSFile(@PathVariable String channelName,
                 @RequestParam(required = false) Boolean packedRules,
                 @RequestParam(required = false) Boolean allowNew,
                 HttpServletResponse response) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
    String formJSFile = FormApi.getFormAPIContent(channelName, null, packedRules, systemInformation, dataLogic, channelCache, rulesCache, allowNew);
    if (formJSFile.length() >= 1) {
      response.setHeader("Content-Disposition", "attachment; filename=form.js" );
      response.setHeader("Content-Type","application/javascript; charset=UTF-8");
      dataLogic.addChecksumHeaders(formJSFile, response);
//      response.setHeader("ValueDate", checksumData.getValueDate());
      try {
        PrintWriter writer = response.getWriter();
        writer.print(formJSFile);
        writer.flush();
      } catch (Exception error){
        //throw some error
        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
      }
    } else {
      throw new ResourceNotFoundException("No available form.js file to download.");
    }
  }




  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific Validation Rules.
   */
  @RequestMapping(value = "/{channelName}/check/validation", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ChecksumVerification getValidationChecksums(@PathVariable String channelName,
                                             @RequestParam(required = false) String checksumAlgorithm,
                                             @RequestParam(required = false) String checksumValue,
                                             HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry validationRules = rulesCache.getValidationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (validationRules == null){
      throw new Exception("Requested validation rule set not found.");
    }
    String dataStr = validationRules.getData();
    ChecksumVerification checksumVerification = new ChecksumVerification(dataLogic.checkForChecksumMatch(dataStr, checksumAlgorithm, checksumValue));
    return checksumVerification;
  }

  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific Document Rules.
   */
  @RequestMapping(value = "/{channelName}/check/document", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ChecksumVerification getDocumentChecksums(@PathVariable String channelName,
                                           @RequestParam(required = false) String checksumAlgorithm,
                                           @RequestParam(required = false) String checksumValue,
                                           HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry documentRules = rulesCache.getDocumentRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (documentRules == null){
      throw new Exception("Requested document rule set not found.");
    }
    String dataStr = documentRules.getData();
    return new ChecksumVerification(dataLogic.checkForChecksumMatch(dataStr, checksumAlgorithm, checksumValue));
  }

  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific Evaluation Rules.
   */
  @RequestMapping(value = "/{channelName}/check/evaluation", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ChecksumVerification getEvaluationChecksums(@PathVariable String channelName,
                                             @RequestParam(required = false) String checksumAlgorithm,
                                             @RequestParam(required = false) String checksumValue,
                                             HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry evaluationRules = rulesCache.getEvaluationRules(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (evaluationRules == null){
      throw new Exception("Requested evaluation rule set not found.");
    }
    String dataStr = evaluationRules.getData();
    return new ChecksumVerification(dataLogic.checkForChecksumMatch(dataStr, checksumAlgorithm, checksumValue));
  }

  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific Lookups.
   */
  @RequestMapping(value = "/{channelName}/check/lookups", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ChecksumVerification getLookupChecksums(@PathVariable String channelName,
                                         @RequestParam(required = false) String checksumAlgorithm,
                                         @RequestParam(required = false) String checksumValue,
                                         HttpServletResponse response) throws Exception {
    if (rulesCache == null) {
      throw new Exception("RulesCache not set");
    }
    PackageCache.Entry lookupsSet = rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource);
    if (lookupsSet == null){
      throw new Exception("Requested lookups set not found.");
    }
    String dataStr = lookupsSet.getData();
    return new ChecksumVerification(dataLogic.checkForChecksumMatch(dataStr, checksumAlgorithm, checksumValue));
  }

  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific FormJS.
   */
  @RequestMapping(value = "/{channelName}/check/js", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ChecksumVerification getFormJSChecksums(@PathVariable String channelName,
                          @RequestParam(required = false) Boolean packedRules,
                 @RequestParam(required = false) Boolean allowNew,
                 @RequestParam(required = false) String checksumAlgorithm,
                 @RequestParam(required = false) String checksumValue,
                 HttpServletResponse response) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
    String dataStr = FormApi.getFormAPIContent(channelName, null, packedRules, systemInformation, dataLogic, channelCache, rulesCache, allowNew);
    return new ChecksumVerification(dataLogic.checkForChecksumMatch(dataStr, checksumAlgorithm, checksumValue));
  }



  /**
   * Authentication-less API to fetch and/or confirm checksums against the channel-specific external validation calls information.
   */
  @RequestMapping(value = "/{channelName}/check/externalValidations", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ChecksumVerification getExternalValidationsChecksum(@PathVariable String channelName,
                                                      @RequestParam(required = false) String checksumAlgorithm,
                                                      @RequestParam(required = false) String checksumValue,
                                HttpServletResponse response) throws Exception {
    String validations = FormApi.makeExtValidationString(channelName, rulesCache, false);
    return  new ChecksumVerification(dataLogic.checkForChecksumMatch(validations, checksumAlgorithm, checksumValue));
  }


}
