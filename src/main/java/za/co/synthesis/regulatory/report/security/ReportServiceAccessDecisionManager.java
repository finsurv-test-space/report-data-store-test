package za.co.synthesis.regulatory.report.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by james on 2017/06/20.
 */
public class ReportServiceAccessDecisionManager implements AccessDecisionManager {
  private static final Logger logger = LoggerFactory.getLogger(ReportServiceAccessDecisionManager.class);

  @Override
  public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
    boolean bAnonymous = false;
    for (GrantedAuthority granted : authentication.getAuthorities()) {
      String role = granted.getAuthority();
      if ("ROLE_ANONYMOUS".equals(role)) {
        bAnonymous = true;
        break;
      }
    }
    if (bAnonymous || !authentication.isAuthenticated()) {
      String authRequirement = null;

      for (ConfigAttribute attr : configAttributes) {
        String needsAttrib = attr.getAttribute();
        if (needsAttrib.startsWith("Auth")) {
          authRequirement = needsAttrib;
        }
        if (needsAttrib.equals("None")) {
          authRequirement = null;
          break;
        }
      }
      if (authRequirement != null) {
        String message;
        int pos = authRequirement.indexOf("@");
        if (pos >= 0) {
          message = "REDIRECT:" + authRequirement.substring(pos + 1);
        } else {
          message = "AUTH: You are not authorised";
          if (authentication.getDetails() != null) {
            message += " (" + authentication.getDetails().toString() + ")";
          }
        }
        throw new AccessDeniedException(message);
      }
    }


    boolean bAuthed = false;
    String message;
    if (configAttributes.size() > 1) {
      message = "You require one of the following " + configAttributes.toString() + " roles";
    }
    else {
      message = "You require the following " + configAttributes.toString() + " role";
    }
    if (!authentication.isAuthenticated()) {
      message += ". You are not authenticated";
      if (authentication.getDetails() != null) {
        message += " (" + authentication.getDetails().toString() + ")";
      }
    }

    for (ConfigAttribute attr : configAttributes) {
      String needsAttrib = attr.getAttribute();
      if (needsAttrib != null) {
        if (needsAttrib.equals("None")) {
          bAuthed = true;
          break;
        }
//          if (needsAttrib.equals("Auth") && !bAuthed) {
//            message = "AUTH: You need authorisation";
//            throw new AccessDeniedException(message);
//          }
        for (GrantedAuthority granted : authentication.getAuthorities()) {
          String role = granted.getAuthority();
          if (role.startsWith("ROLE_"))
            role = role.substring(5);
          if (needsAttrib.equals(role)) {
            bAuthed = true;
            break;
          }
        }
      }
    }
    if (!bAuthed)
      throw new AccessDeniedException(message);
  }

  @Override
  public boolean supports(ConfigAttribute attribute) {
    return false;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }
}
