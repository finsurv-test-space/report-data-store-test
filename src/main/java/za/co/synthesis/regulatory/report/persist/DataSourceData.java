package za.co.synthesis.regulatory.report.persist;

/**
 * Created by jake on 9/14/17.
 */
public class DataSourceData {
  private String driverClassName;
  private String url;
  private String username;
  private String password;

  public DataSourceData(String driverClassName, String url, String username, String password) {
    this.driverClassName = driverClassName;
    this.url = url;
    this.username = username;
    this.password = password;
  }

  public String getDriverClassName() {
    return driverClassName;
  }

  public String getUrl() {
    return url;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }
}
