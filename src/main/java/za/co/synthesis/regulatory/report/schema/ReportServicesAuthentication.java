package za.co.synthesis.regulatory.report.schema;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import za.co.synthesis.regulatory.report.persist.RequestParamData;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;

import java.util.*;

/**
 * Created by james on 2017/10/09.
 */
public class ReportServicesAuthentication implements Authentication {
  private Authentication wrappedInstance;
  private final Map<String, RequestParamData> AuthenticationMeta = new HashMap<String, RequestParamData>();
  private final Map<String, RequestParamData> AuditFields = new HashMap<String, RequestParamData>();
  private List<String> accessSets = null;
  private String userTemplateName;

  public void setUserTemplateName(String userTemplateName) {
    this.userTemplateName = userTemplateName;
    RequestParamData param = new RequestParamData(userTemplateName);
    param.setIsList(false);
    this.AuthenticationMeta.put("TemplateUser", param);
  }

  public ReportServicesAuthentication(Authentication wrappedInstance) {
    this(wrappedInstance, new HashMap<String, String>());
  }

  public ReportServicesAuthentication(Authentication wrappedInstance, Map<String, String> meta) {
    assert(wrappedInstance != null);
    this.wrappedInstance = wrappedInstance;
    if (wrappedInstance instanceof JWTAuthToken) {
      JWTAuthToken jwtAuthToken = (JWTAuthToken)wrappedInstance;
      if (jwtAuthToken.getOther() != null) {
        for (Map.Entry<String, Object> entry : jwtAuthToken.getOther().entrySet()) {
          meta.put(entry.getKey(), (String)entry.getValue());
        }
      }
      accessSets = jwtAuthToken.getAccess();
    }
    for (Map.Entry<String, String> entry : meta.entrySet()) {
      RequestParamData paramData = AuthenticationMeta.get(entry.getKey());
      if (paramData == null){
        paramData = new RequestParamData(entry.getKey(), entry.getValue());
        AuthenticationMeta.put(entry.getKey(), paramData);
      } else {
        paramData.setValue(entry.getValue());
      }
    }
    //user fields...
    RequestParamData userParamData = new RequestParamData("Username", wrappedInstance.getName());
    userParamData.setIsList(false);
    RequestParamData isAuthenticatedParamData = new RequestParamData("IsAuthenticated", wrappedInstance.isAuthenticated() ? "true" : "false");
    isAuthenticatedParamData.setIsList(false);
    AuthenticationMeta.put(userParamData.getName(), userParamData);
//    AuthenticationMeta.put("Name", new RequestParamData(wrappedInstance.getName()));
    AuthenticationMeta.put(isAuthenticatedParamData.getName(), isAuthenticatedParamData);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.getAuthorities():null;
  }

  @Override
  public Object getCredentials() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.getCredentials():null;
  }

  @Override
  public Object getDetails() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.getDetails():null;
  }

  @Override
  public Object getPrincipal() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.getPrincipal():null;
  }

  @Override
  public boolean isAuthenticated() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.isAuthenticated():false;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    if (wrappedInstance!=null) {
      wrappedInstance.setAuthenticated(isAuthenticated);
      AuthenticationMeta.put("IsAuthenticated", new RequestParamData(wrappedInstance.isAuthenticated()?"true":"false"));
    }
  }

  @Override
  public String getName() {
    return (this.wrappedInstance!=null)?this.wrappedInstance.getName():null;
  }

  public List<String> getAccessSets() {
    return accessSets;
  }

  public Map<String, RequestParamData> getAuthenticationMeta() {
    return AuthenticationMeta;
  }

  public Map<String, RequestParamData> getAuditFields() {
    return AuditFields;
  }

  public Authentication getWrappedInstance() {
    return wrappedInstance;
  }

  public String getUserTemplateName() {
    return userTemplateName;
  }


  public static void setMetaFieldList(String fieldString,
                                      List<String> metaFields,
                                      List<String> requiredFields,
                                      Map<String, String> requiredFieldValues){
    if (fieldString !=null && !fieldString.isEmpty()) {
      String[] fields = fieldString.split("\\s*,\\s*");
      for (String field : fields) {
        if (field.startsWith("*") /*|| field.endsWith("!")*/){
          field = field.substring(1);
          requiredFields.add(field);
        } else if (field.indexOf("=") > 0){
          String[] fieldVal = field.split("=");
          if (fieldVal.length == 2 && !fieldVal[0].isEmpty() && !fieldVal[1].isEmpty()) {
            field = fieldVal[0];
            requiredFields.add(field);
            requiredFieldValues.put(field, fieldVal[1]);
          }
        }
        metaFields.add(field);
      }
    }
  }
}
