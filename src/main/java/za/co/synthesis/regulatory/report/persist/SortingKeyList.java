package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.schema.SortDefinition;
import za.co.synthesis.regulatory.report.schema.SortOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/07/03.
 */
public class SortingKeyList extends SortDefinition implements Comparable {
    public class SortingKey extends SortDefinition.SortField implements Comparable<String> {
        private String value;

        public String getValue(){
            return value;
        }

        public SortingKey(String label, CriterionType criterionType, String name, SortOrder order, Comparable value) {
            super(label, criterionType, name, order);
            if (value instanceof String) {
                this.value = (String) value;
            } else if (value != null){
                this.value = value.toString();
            }
        }

        public SortingKey(SortField field, ReportInfo report) {
            this(field.getLabel(), field.getCriterionType(), field.getName(), field.getOrder(), report);
        }

        public SortingKey(String label, CriterionType criterionType, String name, SortOrder order, ReportInfo report) {
            super(label, criterionType, name, order);
            Object val = DataLogic.getFieldValue(report, getCriterionType(), getName());
            if (val instanceof String) {
                this.value = (String)val;
            } else if (val != null){
                value = val.toString();
            }
        }

        @Override
        public int compareTo(String o) {
            return this.value.compareTo(o);
        }

        public int compareTo(SortingKey o) {
            return this.value.compareTo(o.getValue());
        }

        public String toString(){
            return this.getLabel()+":"+(this.getOrder().equals(SortOrder.Descending)?"-":"")+value;
        }
    }

    private final List<SortingKey> sortingKeys = new ArrayList<SortingKey>();


    public SortingKeyList(List<SortField> fields, ReportInfo report) {
        super(fields);
        for (SortField field : super.getFields()){
            this.sortingKeys.add(new SortingKey(field, report));
        }
    }

    public List<SortingKey> getKeys(){
        return sortingKeys;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof SortingKeyList){
            List<SortingKey> comp = ((SortingKeyList) o).getKeys();
            for (int i=0; i< sortingKeys.size(); i++){
                SortingKey lhs = sortingKeys.get(i);
                SortingKey rhs = comp.get(i);
                if (i < comp.size() && lhs != null && rhs != null){
                    int res = lhs.compareTo(rhs)*(lhs.getOrder().equals(SortOrder.Descending)?-1:1);
                    if (res != 0){
                        return res;
                    }
                } else if ((i >= comp.size() || rhs == null) && lhs != null){
                    return -1*(lhs.getOrder().equals(SortOrder.Descending)?-1:1);
                } else if (rhs != null && lhs == null) {
                    return 1*(lhs.getOrder().equals(SortOrder.Descending)?-1:1);
                }
            }
        }
        return 0;
    }

    @Override
    public String toString(){
        String ret = "";
        for (SortingKey key : sortingKeys){
            ret += (ret.length()>0?", ":"") + key.toString();
        }
        return ret;
    }
}
