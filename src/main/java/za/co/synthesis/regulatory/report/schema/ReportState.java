package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 7/27/17.
 */
@JsonPropertyOrder({
        "ReportSpace"
        , "TrnReference"
        , "State"
})public class ReportState {
  private final String reportSpace;
  private final String trnReference;
  private final String state;

  public ReportState(String reportSpace, String trnReference, String state) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.state = state;
  }

  @JsonProperty("ReportSpace")
  public String getReportSpace() {
    return reportSpace;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }

  @JsonProperty("State")
  public String getState() {
    return state;
  }
}
