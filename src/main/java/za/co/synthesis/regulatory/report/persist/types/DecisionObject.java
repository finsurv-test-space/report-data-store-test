package za.co.synthesis.regulatory.report.persist.types;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.schema.EvaluationRequest;
import za.co.synthesis.regulatory.report.schema.EvaluationResponse;
import za.co.synthesis.regulatory.report.schema.FullyReferencedEvaluationDecision;

import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */

@Entity
public class DecisionObject extends PersistenceObject {
  private static class DecisionContent {
    public EvaluationResponse EvaluationResponse;
    public EvaluationRequest EvaluationRequest;
  }

  private static final Logger logger = LoggerFactory.getLogger(DecisionObject.class);

  private FullyReferencedEvaluationDecision decision;
  private String reportSpace;
  private String reportKey; //trnReference
  private Map<String, Object> eventContext;
  private Integer version = 1;

  public DecisionObject(String uuid) {
    super(uuid);
  }

  public DecisionObject(String uuid, FullyReferencedEvaluationDecision decision) {
    super(uuid);
    this.reportSpace = decision.getReportSpace();
    this.reportKey = decision.getTrnReference();
    this.decision = decision;
  }

  public FullyReferencedEvaluationDecision getDecision() {
    return decision;
  }

  public String getUuid(){
    return super.getUuid();
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public String getReportKey() {
    return reportKey;
  }

  public String getTrnReference() {
    return reportKey;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getAuthToken() {
    if (eventContext == null)
      eventContext = composeEventContext();
    if (eventContext != null)
      return mapToJsonStr(eventContext);
    else
      return null;
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public String getContent(){
    ObjectMapper mapper = new ObjectMapper();
    DecisionContent decisionContent = new DecisionContent();
    decisionContent.EvaluationRequest = decision.getParameters();
    decisionContent.EvaluationResponse = decision.getEvaluation();
    try {
      String json = mapper.writeValueAsString(decisionContent);
      return json;
    } catch (JsonProcessingException e) {
      logger.error("Unable to serialize json content for Decision object: " + e.getMessage(), e);
    }
    return null;
  }

  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      ObjectMapper mapper = new ObjectMapper();
      try {
        DecisionContent decisionContent = mapper.readValue(contentJson, DecisionContent.class);
        decision = new FullyReferencedEvaluationDecision(reportSpace, reportKey);
        decision.setParameters(decisionContent.EvaluationRequest);
        decision.setEvaluation(decisionContent.EvaluationResponse);
      } catch (IOException e) {
        logger.error("Unable to deserialize json content for Decision object: " + e.getMessage(), e);
      }
    }
  }
}
