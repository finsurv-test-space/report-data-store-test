package za.co.synthesis.regulatory.report.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import za.co.synthesis.regulatory.report.persist.IDataStore;
import za.co.synthesis.regulatory.report.persist.NotificationData;
import za.co.synthesis.regulatory.report.persist.ProxyCallData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class SendNotificationsTask implements Callable<Integer> {
  final static Log log = LogFactory.getLog(SendNotificationsTask.class);

  private final IDataStore dataStore;
  private final ProxyCaller proxy;
  private static boolean running = false;

  public SendNotificationsTask(IDataStore dataStore, ProxyCaller proxy) {
    this.dataStore = dataStore;
    this.proxy = proxy;
  }

  @Override
  public Integer call() {
    Integer resultCount = 0;

    if (!running) {
      try {
        running = true;
        List<NotificationData> list = dataStore.getNotifications();
        if (list != null && list.size() > 0) {
          for (NotificationData notification : list) {
            ProxyCallData proxyCallData = proxy.getProxyCallData("send_" + notification.getQueueName());
            if (proxyCallData != null) {
              Map<String, Object> parameters = new HashMap<String, Object>();
              parameters.put("reportSpace", notification.getReportSpace());
              parameters.put("trnReference", notification.getTrnReference());
              parameters.put("queueName", notification.getQueueName());
              parameters.put("content", notification.getContent());

              try {
                proxy.call(proxyCallData, parameters);
                resultCount++;
                dataStore.removeNotification(notification);
              }
              catch (Exception e) {
                log.error("Error sending notification via 'send_" + notification.getQueueName() + "' for trn: " + notification.getTrnReference(), e);
              }
            }
            else {
              log.error("Cannot find proxy call configured for 'send_" + notification.getQueueName() + "'");
            }
          }
        }
      } catch (Exception e) {
        log.error("Error sending notification", e);
      } finally {
        running = false;
      }
    }
    return resultCount;
  }
}
