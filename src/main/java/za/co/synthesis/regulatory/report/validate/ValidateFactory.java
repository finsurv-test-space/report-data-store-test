package za.co.synthesis.regulatory.report.validate;

import za.co.synthesis.regulatory.report.persist.ParameterData;
import za.co.synthesis.regulatory.report.persist.ValidationData;
import za.co.synthesis.regulatory.report.schema.ValidateResponse;
import za.co.synthesis.regulatory.report.schema.ValidationResponse;
import za.co.synthesis.regulatory.report.support.PackageCache;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.ICustomValidateFactory;
import za.co.synthesis.rule.core.ValidationEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 3/21/16
 * Time: 4:28 PM
 * Used to register all the test custom validate functions
 */
public class ValidateFactory implements ICustomValidateFactory {
  public static class ValidationTask implements ICustomValidate {
    private String packageName;
    private ValidationRunner validationRunner;
    private ValidationData validationData;

    public ValidationTask(String packageName, ValidationRunner validationRunner, ValidationData validationData) {
      this.packageName = packageName;
      this.validationRunner = validationRunner;
      this.validationData = validationData;
    }

    @Override
    public CustomValidateResult call(Object value, Object... otherInputs) {

      Map<String, Object> parameters = new HashMap<String, Object>();

      int i=0;
      for (ParameterData parameter : validationData.getParameters()) {
        if (parameter.getPath().equals("value")) {
          parameters.put(parameter.getName(), value);
        }
        else {
          if (otherInputs.length > i) {
            parameters.put(parameter.getName(), otherInputs[i]);
          }
          i++;
        }
      }
      return validationRunner.runValidation(packageName, validationData.getEndpoint(), parameters);
    }
  }

  private RulesCache rulesCache;
  private String packageName;
  private ValidationRunner validationRunner;

  public ValidateFactory(RulesCache rulesCache, String packageName, ValidationRunner validationRunner) {
    this.rulesCache = rulesCache;
    this.packageName = packageName;
    this.validationRunner = validationRunner;
  }

  @Override
  public void register(ValidationEngine engine) {
    List<ValidationData> validationList = rulesCache.getValidations(packageName, PackageCache.CacheStrategy.GetFromCache);

    for (ValidationData validationData : validationList) {
      List<String> params = new ArrayList<String>();
      for (ParameterData parameter : validationData.getParameters()) {
        if (!parameter.getPath().equals("value")) {
          params.add(parameter.getPath());
        }
      }
      String[] strParams = new String[params.size()];
      params.toArray(strParams);
      engine.registerCustomValidate(
              validationData.getName(),
              new ValidationTask(packageName, validationRunner, validationData),
              strParams);
    }
  }
}
