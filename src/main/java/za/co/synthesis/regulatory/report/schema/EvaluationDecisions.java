package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 5/25/17.
 */
@JsonPropertyOrder({
        "Parameters"
        , "Evaluation"
})
public class EvaluationDecisions {
  private EvaluationRequest parameters;
  @ManyToMany(fetch= FetchType.EAGER)
  private List<EvaluationResponse> evaluations;

  public EvaluationDecisions(EvaluationRequest parameters, List<EvaluationResponse> evaluations) {
    this.parameters = parameters;
    this.evaluations = evaluations;
  }

  @JsonProperty("Parameters")
  public EvaluationRequest getParameters() {
    return parameters;
  }

  @JsonProperty("Evaluations")
  public List<EvaluationResponse> getEvaluations() {
    return evaluations;
  }
}
