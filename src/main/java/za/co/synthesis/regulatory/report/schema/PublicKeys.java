package za.co.synthesis.regulatory.report.schema;

import java.util.List;

public class PublicKeys {
  private List<PublicKey> keys;

  public PublicKeys(List<PublicKey> keys) {
    this.keys = keys;
  }

  public List<PublicKey> getKeys() {
    return keys;
  }
}
