package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSStructureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 7/26/17.
 */
public class PackageInfo {
  private static final Logger log = LoggerFactory.getLogger(PackageInfo.class);

  private final PackageCache.Entry entry;
  private final List<String> parentPackageNames = new ArrayList<String>();
  private final List<String> featurePackageNames = new ArrayList<String>();

  private static void parsePackage(Map jsPackage, List<String> parentPackageNames, List<String> featurePackageNames) {
    if (jsPackage.containsKey("Features")) {
      Object obj = jsPackage.get("Features");
      if (obj instanceof List) {
        List list = (List)obj;
        for (Object objFeature : list) {
          if (objFeature != null)
            featurePackageNames.add(objFeature.toString());
        }
      }
    }
    if (jsPackage.containsKey("Parent")) {
      Object obj = jsPackage.get("Parent");
      if (obj instanceof Map) {
        Map jsParent = (Map) (obj);
        if (jsParent.containsKey("Name")) {
          Object objName = jsParent.get("Name");
          if (objName != null) {
            parentPackageNames.add(objName.toString());
            parsePackage(jsParent, parentPackageNames, featurePackageNames);
          }
        }
      }
    }
  }

  public PackageInfo(PackageCache.Entry entry) {
    this.entry = entry;
    if (entry == null){
      throw new RuntimeException("No package structure available");
    }
    JSStructureParser parser = new JSStructureParser(entry.getData());

    try {
      Object obj = parser.parse();
      if (obj instanceof Map) {
        Map jsDoc = (Map)obj;
        if (jsDoc.containsKey("Package")) {
          Object objPackage = jsDoc.get("Package");
          if (objPackage instanceof Map) {
            Map jsPackage = (Map) jsDoc.get("Package");
            if (jsPackage.containsKey("Name")) {
              parsePackage(jsPackage, parentPackageNames, featurePackageNames);
            }
          }
        }
      }
    } catch (Exception e) {
      log.error("Cannot parse package structure: " + entry.getData(), e);
    }
  }

  public PackageCache.Entry getEntry() {
    return entry;
  }

  public List<String> getParentPackageNames() {
    return parentPackageNames;
  }

  public List<String> getFeaturePackageNames() {
    return featurePackageNames;
  }
}
