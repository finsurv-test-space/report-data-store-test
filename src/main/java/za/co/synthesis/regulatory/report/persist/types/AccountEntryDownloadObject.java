package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountEntryDownloadObject  extends PersistenceObject{
    private static final Logger logger = LoggerFactory.getLogger(AccountEntryDownloadObject.class);

    private String reportSpace;
    private String reportKey;
    private String accountEntryGuid;

    public AccountEntryDownloadObject(String uuid){super(uuid);}


    public AccountEntryDownloadObject(String uuid, String reportSpace, String reportKey, AccountEntryObject accountEntryObj){
        super(uuid);
        this.reportSpace = reportSpace;
        this.reportKey = reportKey;
        this.accountEntryGuid = accountEntryObj.getGUID();
    }

    public String getReportSpace() {
        return reportSpace;
    }

    public void setReportSpace(String reportSpace) {
        this.reportSpace = reportSpace;
    }

    public String getReportKey() {
        return reportKey;
    }

    public void setReportKey(String reportKey) {
        this.reportKey = reportKey;
    }

    public String getAccountEntryGuid() {
        return accountEntryGuid;
    }

    public void setAccountEntryGuid(String accountEntryGuid) {
        this.accountEntryGuid = accountEntryGuid;
    }

}
