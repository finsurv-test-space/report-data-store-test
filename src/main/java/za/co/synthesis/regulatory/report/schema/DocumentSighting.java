package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.DocumentSightingData;
import za.co.synthesis.regulatory.report.persist.DocumentTypeData;
import za.co.synthesis.rule.core.FlowType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jake on 7/30/17.
 */
@JsonPropertyOrder({
        "DocumentType"
        , "Flow"
        , "Category"
})
public class DocumentSighting {
  private final DocumentSightingData documentSightingData;

  public static List<DocumentSighting> wrapList(Collection<DocumentSightingData> dataList) {
    List<DocumentSighting> result = new ArrayList<DocumentSighting>();
    for (DocumentSightingData data : dataList) {
      result.add(new DocumentSighting(data));
    }
    return result;
  }

  public DocumentSighting() {
    this.documentSightingData = new DocumentSightingData();
  }

  public DocumentSighting(DocumentSightingData documentSightingData) {
    this.documentSightingData = documentSightingData;
  }

  @JsonProperty("DocumentType")
  public String getDocumentType() {
    return documentSightingData.getDocumentTypeName();
  }

  @JsonProperty("Flow")
  public List<FlowType> getFlows() {
    return documentSightingData.getFlows();
  }

  @JsonProperty("Category")
  public List<String> getCategories() {
    return documentSightingData.getCategories();
  }

  public DocumentSightingData getDocumentSightingData() {
    return documentSightingData;
  }
}
