package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by james on 2017/05/30.
 */

    /*
      flow       : 'IN',
      code       : '103/01',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payments',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Payment', 'Undertaking']
     */
@JsonPropertyOrder({
        "Flow",
        "Code",
        "OldCodes",
        "Section",
        "Subsection",
        "Description"
})
public class Category {

//    public static enum FlowType{IN, OUT}
//    private FlowType flow;
    private String flow;
    private String code;
    private String oldCodes;
    private String section;
    private String subsection;
    private String description;

    public Category(Object flow, Object code, Object section, Object subsection, Object description) {
        this(flow, code, null, section, subsection, description);
    }

    public Category(Object flow, Object code, Object oldCodes, Object section, Object subsection, Object description) {
        this.flow = (String)flow;
        this.code = (String)code;
        this.oldCodes = (String)oldCodes; //*optional
        this.section = (String)section;
        this.subsection = (String)subsection;
        this.description = (String)description;
    }

    @JsonProperty("Flow")
    public String getFlow() {
        return flow;
    }

    @JsonProperty("Code")
    public String getCode() {
        return code;
    }

    @JsonProperty("OldCodes")
    public String getOldCodes() {
        return oldCodes;
    }

    @JsonProperty("Section")
    public String getSection(){
        return section;
    }

    @JsonProperty("Subsection")
    public String getSubsection(){
        return subsection;
    }

    @JsonProperty("Description")
    public String getDescription(){
        return description;
    }
}

