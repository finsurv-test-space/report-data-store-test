package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.IEvaluationScenarioDecision;
import za.co.synthesis.rule.core.impl.EvaluationScenarioDecision;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by jake on 5/25/17.
 */
@JsonPropertyOrder({
        "Parameters"
        , "Scenario"
        , "Evaluations"
        , "Information"
})
public class ConsolidatedEvaluationDecision {
  private EvaluationRequest parameters;
  @ManyToMany(fetch= FetchType.EAGER)
  private List<EvaluationResponse> evaluations;
  private String scenario;
  private String information;

  public ConsolidatedEvaluationDecision(EvaluationRequest parameters, IEvaluationScenarioDecision evaluation) {
    this.parameters = parameters;
    this.evaluations = EvaluationResponse.getEvaluationResponseArray(evaluation.getDecisions());
    this.scenario = evaluation.getScenario();
    this.information = evaluation.getInformation();
  }

  @JsonProperty("Parameters")
  public EvaluationRequest getParameters() {
    return parameters;
  }

  @JsonProperty("Evaluations")
  public List<EvaluationResponse> getEvaluations() {
    return evaluations;
  }

  @JsonProperty("Scenario")
  public String getScenario() {
    return scenario;
  }

  @JsonProperty("Information")
  public String getInformation() {
    return information;
  }
}
