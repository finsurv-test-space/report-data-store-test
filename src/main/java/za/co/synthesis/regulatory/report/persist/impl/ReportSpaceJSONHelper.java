package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.ChannelData;
import za.co.synthesis.regulatory.report.persist.ReportSpaceData;
import za.co.synthesis.regulatory.report.persist.StateData;
import za.co.synthesis.regulatory.report.persist.TransitionData;
import za.co.synthesis.regulatory.report.support.ActionType;
import za.co.synthesis.regulatory.report.support.BehaviourType;
import za.co.synthesis.regulatory.report.support.EventType;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 7/27/17.
 */
public class ReportSpaceJSONHelper {
  private static final Logger logger = LoggerFactory.getLogger(ReportSpaceJSONHelper.class);

  public static Map<String, ReportSpaceData> parseReportSpaceList(Map doc) {
    Map<String, ReportSpaceData> reportSpace_definitions = new HashMap<String, ReportSpaceData>();
    if (doc != null) {
      if (doc.size() > 0 && doc.containsKey("reportspaces")) {
        Object objReportSpaces = doc.get("reportspaces");
        if (objReportSpaces instanceof List) {
          List reportSpaceConfigs = (List) objReportSpaces;
          if (reportSpaceConfigs.size() > 0) {
            for (Object reportSpaceConfigObj : reportSpaceConfigs) {
              if (reportSpaceConfigObj instanceof Map) {
                ReportSpaceData data = parseReportSpace((Map) reportSpaceConfigObj);
                if (data != null)
                  reportSpace_definitions.put(data.getName(), data);
              }
            }
          }
        }
      }
    }
    return reportSpace_definitions;
  }

  private static List<BehaviourType> getListOfBehavioursFromProperty(Map map, String property) {
    List<String> list = JSReaderUtil.getListOfStringsFromProperty(map, property);
    List<BehaviourType> result = new ArrayList<BehaviourType>();
    for (String entry : list) {
      if (entry.equalsIgnoreCase("AllowReportEdit"))
        result.add(BehaviourType.AllowReportEdit);
      else
      if (entry.equalsIgnoreCase("DisallowReportEdit"))
        result.add(BehaviourType.DisallowReportEdit);
      else
      if (entry.equalsIgnoreCase("AllowDocUpload"))
        result.add(BehaviourType.AllowDocUpload);
      else
      if (entry.equalsIgnoreCase("DisallowDocUpload"))
        result.add(BehaviourType.DisallowDocUpload);
    }
    return result;
  }

  private static List<ActionType> getListOfActionsFromProperty(Map map, String property) {
    List<String> list = JSReaderUtil.getListOfStringsFromProperty(map, property);
    List<ActionType> result = new ArrayList<ActionType>();
    for (String entry : list) {
      if (entry.equalsIgnoreCase("QueueForDownload"))
        result.add(ActionType.QueueForDownload);
      else
      if (entry.equalsIgnoreCase("Cancel"))
        result.add(ActionType.Cancel);
      else
      if (entry.equalsIgnoreCase("CancelWithReplace"))
        result.add(ActionType.CancelWithReplace);
      else
      if (entry.equalsIgnoreCase("SendNotification"))
        result.add(ActionType.SendNotification);
    }
    return result;
  }

  public static ReportSpaceData parseReportSpace(Map doc) {
    String reportSpaceName = (String) doc.get("name");
    if (reportSpaceName != null) {
      String defaultChannel = null;
      if (doc.containsKey("defaultChannel")) {
        defaultChannel = (String) doc.get("defaultChannel");
      }

      //State list
      List<StateData> states = null;
      if (doc.containsKey("states")) {
        JSArray stateList = (JSArray) doc.get("states");
        states = new ArrayList<StateData>();
        for (Object stateObj : stateList) {
          JSObject jsState = (JSObject) stateObj;
          if (jsState.containsKey("name")) {
            String name = (String) jsState.get("name");
            boolean defaultState = jsState.containsKey("defaultState") &&
                    jsState.get("defaultState") != null &&
                    ((Boolean) jsState.get("defaultState"));
            List<BehaviourType> behaviourTypeList = getListOfBehavioursFromProperty(jsState, "behaviour");
            List<ActionType> actionTypeList = getListOfActionsFromProperty(jsState, "action");
            List<String> userEventList = JSReaderUtil.getListOfStringsFromProperty(jsState, "userEvents");
            StateData state = new StateData(name, behaviourTypeList, actionTypeList, userEventList, defaultState);
            states.add(state);
          }
        }
      }

      //Transition list
      List<TransitionData> transitions = null;
      if (doc.containsKey("transitions")) {
        JSArray transitionList = (JSArray) doc.get("transitions");
        transitions = new ArrayList<TransitionData>();
        for (Object transObj : transitionList) {
          JSObject jsTransition = (JSObject) transObj;
          if (jsTransition.containsKey("event")) {
            String eventTypeString = (String) jsTransition.get("event");
            EventType event = EventType.fromString(eventTypeString);
            if (event != null) {
              //Transition Initial States
              List<String> initialStates = null;
              if (jsTransition.containsKey("currentStates")) {
                JSArray jsStateList = (JSArray) jsTransition.get("currentStates");
                initialStates = new ArrayList<String>();
                for (Object state : jsStateList) {
                  String stateName = (String) state;
                  if (stateName != null)
                    initialStates.add(stateName);
                }
              }

              //Transition Source Report Spaces
              List<String> currentReportSpaces = null;
              if (jsTransition.containsKey("reportSpace")) {
                JSArray jsReportSpaceList = (JSArray) jsTransition.get("reportSpace");
                currentReportSpaces = new ArrayList<String>();
                for (Object space : jsReportSpaceList) {
                  String reportSpace = (String) space;
                  if (reportSpace != null)
                    currentReportSpaces.add(reportSpace);
                }
              }

              //Transition Source Channels
              List<String> currentChannels = null;
              if (jsTransition.containsKey("channels")) {
                JSArray jsChannelList = (JSArray) jsTransition.get("channels");
                currentChannels = new ArrayList<String>();
                for (Object channel : jsChannelList) {
                  String channelName = (String) channel;
                  if (channelName != null)
                    currentChannels.add(channelName);
                }
              }

              //Transition Target State
              String newState = null;
              if (jsTransition.containsKey("newState") &&
                      jsTransition.get("newState") != null) {
                newState = ((String) jsTransition.get("newState"));
              }

              //User Action
              String action = null;
              if (jsTransition.containsKey("action") &&
                      jsTransition.get("action") != null) {
                action = ((String) jsTransition.get("action"));
              }

              TransitionData transition = new TransitionData(event, action, currentReportSpaces, currentChannels, initialStates, newState);
              transitions.add(transition);
            } else {
              logger.warn("Unknown EventType (" + eventTypeString + ")in transition @ " + reportSpaceName);
            }
          }
        }
      }


      ReportSpaceData reportSpaceConfig = new ReportSpaceData(reportSpaceName, states, transitions);
      if (defaultChannel != null) {
        reportSpaceConfig.setDefaultChannel(defaultChannel);
      }
      return reportSpaceConfig;
    }
    return null;
  }

  public static Map<String, ChannelData> parseChannelDataMap(Map doc) {
    Map<String, ChannelData> result = new HashMap<String, ChannelData>();
    if (doc.containsKey("packages")) {
      Object objPackages = doc.get("packages");
      if (objPackages instanceof Map) {
        Map jsPackages = (Map) objPackages;
        for (Object key : jsPackages.keySet()) {
          Object objChannel = jsPackages.get(key);
          if (objChannel instanceof Map) {
            Map channel = (Map) objChannel;
            result.put(key.toString(), new ChannelData(key.toString(), (String) channel.get("reportspace"), (String) channel.get("calls"), (String)channel.get("defaultSchema")));
          }
        }
      }
    }
    return result;
  }
}
