package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.persist.common.TableDefinition;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.schema.SortOrderField;

import java.util.List;
import java.util.Map;

/**
 * This is the interface for persisting tables either locally or remotely
 */
public interface ITablePersist<T extends PersistenceObject> {


  public static class PersistenceObjectList<T extends PersistenceObject> {
    private String syncpoint;
    private List<T> objects;

    public PersistenceObjectList(String syncpoint, List<T> objects) {
      this.syncpoint = syncpoint;
      this.objects = objects;
    }

    public String getSyncpoint() {
      return syncpoint;
    }

    public List<T> getObjects() {
      return objects;
    }

  }

  /**
   * The implementation of this write method must ensure that the UUID record does not exist before adding as
   * this will ensure that no duplicates are created.
   * @param obj - the object to write to the database
   * @throws PersistException
   */
  void write(T obj) throws PersistException;

  /**
   * The implementation of this write method must ensure that the UUID record does not exist before adding as
   * this will ensure that no duplicates are created.
   * @param objects - the list of objects to write to the database
   * @throws PersistException
   */
  void write(List<T> objects) throws PersistException;

  /**
   * A syncpoint of null must be catered for. It means that nothing has been synchronised.
   * @param syncpoint
   * @param maxRecords
   * @return
   */
  PersistenceObjectList<T> readFromSyncpoint(String syncpoint, int maxRecords);

  PersistenceObjectList<T> readFromSyncpoint(String syncpoint, String reportSpace, int maxRecords);

  List<T> queryByMapList(Map<String, List<String>> queryMapList, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy);
  List<T> searchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy);

  Class getPersistClass();

  List<T> readCurrent();
  List<T> readBySelector(int maxRecords);

  TableDefinition getTableDefinition();

  List<T> fullTextSearchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy);

  boolean fullTextSearchSupported();
  void setFullTextSearchSupported(boolean fullTextSearchSupported);
  
  int updateCriteria(List<CriterionData> criteria, List<CriterionData> newCriteria);
}
