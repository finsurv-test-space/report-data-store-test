package za.co.synthesis.regulatory.report.persist;

import org.springframework.web.bind.annotation.RequestMethod;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import java.util.List;

/**
 * Created by jake on 9/15/17.
 */
public class RestData {
  private RequestMethod verb = null;
  private String url;
  private int timeout = 10;
  private String body;
  private List<HeaderData> headers;
  private List<Object> acceptHttpCodes;
  public RequestMethod getVerb() {
    return verb;
  }

  public void setVerb(RequestMethod requestMethodVerb) {
    this.verb = requestMethodVerb;
  }

  public void setVerb(String requestMethod) {
    if (requestMethod != null) {
      this.verb = (requestMethod.equalsIgnoreCase("POST") ? RequestMethod.POST : (
              requestMethod.equalsIgnoreCase("GET") ? RequestMethod.GET : (
                      requestMethod.equalsIgnoreCase("DELETE") ? RequestMethod.DELETE : (
                              requestMethod.equalsIgnoreCase("HEAD") ? RequestMethod.HEAD : (
                                      requestMethod.equalsIgnoreCase("PATCH") ? RequestMethod.PATCH : (
                                              requestMethod.equalsIgnoreCase("PUT") ? RequestMethod.PUT : (
                                                      requestMethod.equalsIgnoreCase("TRACE") ? RequestMethod.TRACE :
                                                              RequestMethod.OPTIONS
                                              )
                                      )
                              )
                      )
              )
      )
      );
    } else {
      this.verb = RequestMethod.POST;//default to POST.
    }
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public List<HeaderData> getHeaders() {
    return headers;
  }

  public void setHeaders(List<HeaderData> headers) {
    this.headers = headers;
  }

  public String toString(){
    String restDataString = "\nRequest Method: "+getVerb()+"\nURL: " + getUrl() + "\nHeaders"+ JsonSchemaTransform.mapToJsonStr(getHeaders()) + "\nBody" + getBody() + "\n";
    return restDataString;
  }

  public List<Object> getAcceptHttpCodes() {
    return acceptHttpCodes;
  }

  public void setAcceptHttpCodes(List<Object> acceptCodes) {
    this.acceptHttpCodes = acceptCodes;
  }

  public int getTimeout() {
    return timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }
}
