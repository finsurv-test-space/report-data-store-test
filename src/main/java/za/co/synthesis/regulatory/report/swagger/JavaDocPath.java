package za.co.synthesis.regulatory.report.swagger;

import za.co.synthesis.javascript.JSObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaDocPath {
  public String getComment() {
    return comment;
  }

  public static class Parameter {
    private final String name;
    private final String comment;

    public Parameter(String name, String comment) {
      this.name = name;
      this.comment = comment;
    }

    public String getName() {
      return name;
    }

    public String getComment() {
      return comment;
    }
  }
  private final String path;
  private final String method;
  private final String comment;
  private final List<Parameter> parameters = new ArrayList<Parameter>();

  public JavaDocPath(String path, String method, String comment) {
    this.path = path;
    this.method = method;
    this.comment = comment;
  }

  public String getPath() {
    return path;
  }

  public String getMethod() {
    return method;
  }

  public List<Parameter> getParameters() {
    return parameters;
  }

  public Parameter getParameterByName(final String name) {
    for (Parameter param : parameters) {
      if (param.getName().equals(name))
        return param;
    }
    return null;
  }

  private static String getKey(String path, String method) {
    return path + "#" + method;
  }

  public static JavaDocPath getJavaDocPath(String path, String method, Map<String, JavaDocPath> javaDocPathMap) {
    String key = getKey(path, method);
    return javaDocPathMap.get(key);
  }

  public static Map<String, JavaDocPath> parseJavaDocs(Object doc) {
    Map<String, JavaDocPath> javadocMap = new HashMap<String, JavaDocPath>();
    if (doc != null) {
      if (doc instanceof List) {
        List classList = (List) doc;
        if (classList.size() > 0) {
          for (Object classObj : classList) {
            if (classObj instanceof Map) {
              parseClassDoc((Map) classObj, javadocMap);
            }
          }
        }
      }
    }
    return javadocMap;
  }

  private static void parseClassDoc(Map doc, Map<String, JavaDocPath> javadocMap) {
    String className = (String) doc.get("class");
    if (className != null) {
      String classMapping = (String) doc.get("mapping");

      Object methodsObj = doc.get("methods");
      if (methodsObj instanceof List) {
        List methodList = (List) methodsObj;
        for (Object methodObj : methodList) {
          JSObject jsMethod = (JSObject) methodObj;
          String methodMapping = (String) jsMethod.get("mapping");
          String methodVerb = (String) jsMethod.get("method");
          String methodComment = (String) jsMethod.get("comment");

          String fullMapping = classMapping != null ? classMapping : "";
          if (!fullMapping.startsWith("/")) {
            fullMapping = "/" + fullMapping;
          }
          if (methodMapping != null && methodMapping.length() > 0 && !classMapping.endsWith("/") && !methodMapping.startsWith("/")) {
            fullMapping += "/";
          }
          fullMapping += methodMapping;
          JavaDocPath docPath = new JavaDocPath(fullMapping, methodVerb, methodComment);
          javadocMap.put(getKey(fullMapping, methodVerb), docPath);

          Object parametersObj = jsMethod.get("parameters");
          if (parametersObj instanceof List) {
            List parameterList = (List) parametersObj;
            for (Object paramObj : parameterList) {
              JSObject jsParam = (JSObject) paramObj;
              String paramName = (String) jsParam.get("name");
              String paramComment = (String) jsParam.get("comment");
              docPath.getParameters().add(new JavaDocPath.Parameter(paramName, paramComment));
            }
          }
        }
      }
    }
  }
}
