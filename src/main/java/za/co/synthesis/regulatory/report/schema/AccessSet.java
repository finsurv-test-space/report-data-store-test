package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.AccessSetData;
import za.co.synthesis.regulatory.report.support.AccessType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nishaal on 9/6/17.
 */
@JsonPropertyOrder({
        "AccessType"
        ,"AccessCriteria"
})
public class AccessSet {
    private final AccessSetData accessSetData;
    private final List<Criterion> accessCriteria;

    public AccessSet() {
        this.accessSetData = new AccessSetData();
        this.accessCriteria = Criterion.wrapList(accessSetData.getAccessCriteria());
    }

    public AccessSet(AccessSetData accessSetData) {
        this.accessSetData = accessSetData;
        this.accessCriteria = Criterion.wrapList(accessSetData.getAccessCriteria());
    }

    public static List<AccessSet> wrapList(List<AccessSetData> dataList) {
        List<AccessSet> result = new ArrayList<AccessSet>();
        for (AccessSetData data : dataList) {
            result.add(new AccessSet(data));
        }
        return result;
    }

    @JsonProperty("Name")
    public String getName() {
        return accessSetData.getName();
    }

    @JsonProperty("ReportSpace")
    public String getReportSpace() {
        return accessSetData.getReportSpace();
    }

    @JsonProperty("AccessType")
    public AccessType getAccessType() {
        return accessSetData.getAccessType();
    }

    @JsonProperty("AccessCriteria")
    public List<Criterion> getAccessCriteria() {
        return accessCriteria;
    }

    public void setReportSpace(String reportSpace) {
        accessSetData.setReportSpace(reportSpace);
    }

    public void setAccessType(AccessType accessType) {
        accessSetData.setAccessType(accessType);
    }

    public void setName(String name) {
        accessSetData.setName(name);
    }

    public void setAccessCriteria(List<Criterion> accessCriteria) {
        for(Criterion criterion : accessCriteria){
            accessSetData.getAccessCriteria().add(criterion.getCriterionData());
        }
    }

    @JsonIgnore
    public AccessSetData getAccessSetData() {
        return accessSetData;
    }

    @JsonIgnore
    public void syncData() {
        accessSetData.getAccessCriteria().clear();
        for (Criterion criterion : accessCriteria) {
            accessSetData.getAccessCriteria().add(criterion.getCriterionData());
        }
    }
}
