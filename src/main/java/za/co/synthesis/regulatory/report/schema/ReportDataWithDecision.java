package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({
    "Meta"
    , "Report"
    , "Decision"
})
public class ReportDataWithDecision extends ReportData{
  private EvaluationDecision decision;
  
  public ReportDataWithDecision() {
    this(null, null, null);
  }
  
  public ReportDataWithDecision(Map<String, Object> meta, Map<String, Object> report) {
    this(meta, report, null);
  }
  
  public ReportDataWithDecision(@JsonProperty("Meta") Map<String, Object> meta, @JsonProperty("Report") Map<String, Object> report, @JsonProperty("Decision") EvaluationDecision decision) {
    super(meta, report);
    this.decision = (decision != null) ? decision : null;
  }
  
  @JsonProperty("Meta")
  public Map<String, Object> getMeta() {
    return super.getMeta();
  }
  
  @JsonProperty("Report")
  public Map<String, Object> getReport() {
    return super.getReport();
  }
  
  //  @JsonProperty("Decision")
  @JsonIgnore
  public EvaluationDecision getDecision() {
    return decision;
  }
  
  @JsonIgnore
  public String getTrnReference() {
    return (String)getReport().get("TrnReference");
  }
  
}
