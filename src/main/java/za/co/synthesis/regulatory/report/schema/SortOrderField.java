package za.co.synthesis.regulatory.report.schema;

/**
 * Created by james on 2017/10/05.
 */
public class SortOrderField {
  private SortOrder order = SortOrder.Ascending;
  private String name;

  public SortOrderField(String name, SortOrder order){
    this.order = order;
    this.name = name;
  }

  public SortOrder getOrder() {
    return order;
  }

  public void setOrder(SortOrder order) {
    this.order = order;
  }

  public String getOrderStr() {
    return order.toString();
  }

  public String getName(){
    return this.name;
  }

  public String toString(){
    if (name != null) {
      return name + " " + getOrderStr();
    }
    return "";
  }
}
