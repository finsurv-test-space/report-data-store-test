package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.dao.IExternalLookup;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.LookupData;
import za.co.synthesis.regulatory.report.validate.ValidationUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/15/17.
 */
public class RestCallExternalLookup implements IExternalLookup {
  private static final Logger log = LoggerFactory.getLogger(RestCallExternalLookup.class);

  private final IConfigStore configStore;
  private final LookupData lookupData;

  public RestCallExternalLookup(IConfigStore configStore, LookupData lookupData) {
    this.configStore = configStore;
    this.lookupData = lookupData;
  }

  private static JSArray parseComposedResponse(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      Object valRes = parser.parse();

      if (valRes instanceof JSArray) {
        log.info("Retrieved {} lookup values.", ((JSArray) valRes).size());
        return (JSArray)valRes;
      }
    }
    return null;
  }


  @Override
  public JSArray getFullLookupList(String listName) {
    Map<String, Object> parameterValue = new HashMap<String, Object>();

    String response = null;
    try {
      log.info("Updating Lookup List '{}' from {}",listName,lookupData.getRest().getUrl());
      response = ValidationUtils.makeRESTCall(lookupData.getRest(), parameterValue, configStore, log);
    } catch (Exception e) {
      log.error("Error making call for lookup " + lookupData.getName(), e);
      return null;
    }

    try {
      if (lookupData.getCompose() != null) {
        String responseName = "response";
        if (response != null) {
          parameterValue.put(responseName, response);
        } else {
          parameterValue.put(responseName, null);
        }

        String composedResult = ValidationUtils.compose(lookupData.getName() + " " + responseName,
                lookupData.getCompose(),
                parameterValue, null, log);
        return parseComposedResponse(composedResult);
      }
      else {
        return parseComposedResponse(response);
      }
    } catch (Exception e) {
      log.error("Response returned: " + response + "\nError composing response for lookup " + lookupData.getName(), e);
      return null;
    }
  }
}
