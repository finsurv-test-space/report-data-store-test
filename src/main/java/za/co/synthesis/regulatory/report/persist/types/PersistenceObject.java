package za.co.synthesis.regulatory.report.persist.types;

import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

/**
 * Created by jake on 8/21/17.
 */
@MappedSuperclass
public class PersistenceObject implements Serializable {

  private String uuid;

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private long objectDbId;

  private String version;

  public PersistenceObject(String uuid) {
    this.uuid = uuid;
  }

  public String getUuid() {
    return uuid;
  }
  public String getGUID() {
    return uuid;
  }
  public void setGUID(String uuid) {
    this.uuid = uuid;
  }

  public long getObjectDbId() {
    return objectDbId;
  }


  //----------------------------------------//
  //----------  HELPER FUNCTIONS  ----------//
  //----------------------------------------//

  /**
   * Generate UUID specifically for the HA data store.
   * ...implementation may differ from that available in the DataLogic class.
   * @return
   */
  public static String generateUUID() {
    return UUID.randomUUID().toString();
  }


  public static Map<String, Object> composeEventContext() {
    Map<String, Object> result = null;
    try{
      ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
      if (user != null) {
        result = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
      }
    } catch (Exception e){}
    return result;
  }
}
