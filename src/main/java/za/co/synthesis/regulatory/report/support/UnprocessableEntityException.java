package za.co.synthesis.regulatory.report.support;

/**
 * Created by jake on 5/19/17.
 */
public class UnprocessableEntityException extends RuntimeException {
  public UnprocessableEntityException(String message) {
    super(message);
  }
}
