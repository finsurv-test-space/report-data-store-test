package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.UserData;
import za.co.synthesis.regulatory.report.security.investec.OVIDHelper;

import java.util.HashMap;
import java.util.Map;

public class GCNAuthModule implements IJWTAuthModule {
  public static class GCNAuthException extends AuthenticationException {
    public GCNAuthException(String msg) {
      super(msg);
    }
  }
  private static final String templateUser = "investecSSO";
  private OVIDHelper ovidHelper;
  private UserInformation userInformation;

  @Required
  public void setOvidHelper(OVIDHelper ovidHelper) {
    this.ovidHelper = ovidHelper;
  }

  @Required
  public void setUserInformation(UserInformation userInformation) {
    this.userInformation = userInformation;
  }

  @Override
  public boolean isAvailable() {
    return (ovidHelper != null);
  }

  @Override
  public String getName() {
    return "gcn";
  }

  @Override
  public String authAndComposeJWTIdToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    Map<String, Object> other = new HashMap<String, Object>();
    return provider.produceJWTID(username, getName(), other);
  }

  @Override
  public String composeJWTAccessTokenForAuthenticatedUser(JWTProvider provider, String username) throws AuthenticationException {
    UserData userData = userInformation.getUser(templateUser);

    String ovids = ovidHelper.getOVIDfromSaleslogix(username);
    if (ovids == null || ovids.length() == 0) {
      throw new GCNAuthException("GCN has no related OneViewIDs");
    }

    if (userData != null) {
      Map<String, Object> other = new HashMap<String, Object>();
      other.put("GCN", username);
      other.put("OVID", ovids);
      return provider.produceJWTAccess(userData.getUsername(), userData.getAccess(), userData.getRights(), other);
    }
    else {
      throw new GCNAuthException("Cannot find '" + templateUser + "' internal credentials");
    }
  }

  @Override
  public String authAndComposeJWTAccessToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    return composeJWTAccessTokenForAuthenticatedUser(provider, username);
  }

  @Override
  public JWTAuthToken authAndGenerateAccessToken(String username, String password) throws AuthenticationException {
    UserData userData = userInformation.getUser(templateUser);

    boolean hasGCNWithOVID = false;
    String ovids = null;
    try {
      ovids = ovidHelper.getOVIDfromSaleslogix(username);
      if (ovids != null && ovids.length() > 0) {
        hasGCNWithOVID = true;
      }
    }
    catch (AuthenticationServiceException e) {
      hasGCNWithOVID = false;
    }
    if (!hasGCNWithOVID) {
      JWTAuthToken jwtAuthToken = new JWTAuthToken(username, null,null, null, JWTAuthToken.TokenUse.ACCESS);
      jwtAuthToken.setAuthenticated(false);
      return jwtAuthToken;
    }

    if (userData != null) {
      Map<String, Object> other = new HashMap<String, Object>();
      other.put("GCN", username);
      other.put("OVID", ovids);

      String[] rightsArray = new String[userData.getRights().size()];
      userData.getRights().toArray(rightsArray);

      JWTAuthToken jwtAuthToken = new JWTAuthToken(userData.getUsername(), userData.getAccess(),other, AuthorityUtils.createAuthorityList(rightsArray), JWTAuthToken.TokenUse.ACCESS);
      jwtAuthToken.setAuthenticated(true);
      return jwtAuthToken;
    }
    else {
      throw new GCNAuthException("Cannot authenticate '" + username + "' against internal credentials");
    }
  }
}