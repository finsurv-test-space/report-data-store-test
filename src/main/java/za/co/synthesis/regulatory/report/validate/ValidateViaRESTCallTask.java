package za.co.synthesis.regulatory.report.validate;

import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import org.apache.lucene.analysis.CharArrayMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.ParameterData;
import za.co.synthesis.regulatory.report.persist.ValidationData;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.StatusType;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jake on 9/15/17.
 */
public class ValidateViaRESTCallTask implements Callable<CustomValidateResult> {
  private static final Logger log = LoggerFactory.getLogger(ValidateViaRESTCallTask.class);


  private final IConfigStore configStore;
  private final ValidationData validationData;
  private final Map<String, Object> parameterValues;
  private final static Map<String, JdbcTemplate> jdbcTemplateMap = new ConcurrentHashMap<String, JdbcTemplate>();

  public SystemInformation getSystemInformation() {
    return systemInformation;
  }

  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  private SystemInformation systemInformation;

  public ValidateViaRESTCallTask(IConfigStore configStore, ValidationData validationData, Map<String, Object> parameterValues) {
    this.configStore = configStore;
    this.validationData = validationData;
    this.parameterValues = parameterValues != null ? parameterValues : new HashMap<String, Object>();
    fleshOutParameterValues();
  }

  private void fleshOutParameterValues() {
    for (ParameterData param : validationData.getParameters()) {
      if (!parameterValues.containsKey(param.getName())) {
        parameterValues.put(param.getName(), null);
      }
    }
  }

  @Override
  public CustomValidateResult call() {
    String externalCallsDebugging = systemInformation.getConfigStore().getEnvironmentParameters().getProperty("externalCallsDebugging");

    boolean failOnError = (validationData.getFailOnError() instanceof String )?
            ("true".equalsIgnoreCase(validationData.getFailOnError())) :
            (validationData.getFailOnError()  == null ? false : true);
    StatusType transportFailureType = failOnError?StatusType.Fail : StatusType.Error;
    String response = null;
    try {
      if("true".equalsIgnoreCase(externalCallsDebugging)){
        String params = "";
        for(Map.Entry<String,Object> entry:  parameterValues.entrySet()) {
          params+= "\n"+entry.getKey()+": "+entry.getValue();
        }
        log.info("External Validation Call Params: \n"+params + "\n\n");
      }
      if("true".equalsIgnoreCase(externalCallsDebugging)){
        String restData = validationData.getRest().toString();
        log.info("Rest data sent: \n"+restData);
      }
      ValidationUtils.systemInformation = systemInformation;
      response = ValidationUtils.makeRESTCall(validationData.getRest(), parameterValues, configStore, log);

      CustomValidateResult result = null;
      if (validationData.getCompose() != null) {
        String responseName = "response";
        if (response != null) {
          if("true".equalsIgnoreCase(externalCallsDebugging)){
            log.info("Response received: \n"+response);
          }
          parameterValues.put(responseName, response);
          parameterValues.put(responseName+"Body", Base64.getEncoder().encodeToString(response.getBytes()));
        } else {
          parameterValues.put(responseName, null);
        }

        String composedResult = ValidationUtils.compose(validationData.getName() + " " + responseName,
                validationData.getCompose(), parameterValues, configStore, log);
        if (composedResult instanceof String) {
          log.info("Composed Result: \n" + composedResult);
        }
        result = ValidationUtils.parseComposedResponse(composedResult);
      }
      else {
        result = ValidationUtils.parseComposedResponse(response);
      }
      if (result == null) {
        result = new CustomValidateResult(transportFailureType, "NOR", "No valid result for " + validationData.getEndpoint());
      }
      if("true".equalsIgnoreCase(externalCallsDebugging)){
        log.info("Final Result: \n" + result.getStatus() +" -- "+result.getCode() +" -- " + result.getMessage());
      }
      return result;
    } catch (Exception e) {
      log.error("Exception running task for endpoint " + validationData.getEndpoint(), e);
      try {
        log.info("API Call response: \n" + validationData.getRest().getUrl() + "\n");
        for (ParameterData pd : validationData.getParameters()){
          String paramName = pd.getName();
          String paramVal = parameterValues.containsKey(paramName) && parameterValues.get(paramName) instanceof String ? (String) parameterValues.get(paramName) : "";
          log.info("param <"+paramName+"> => \""+paramVal+"\"");
        }
      } catch (Exception ex) {
        log.error("Exception printing External Validation call details.", ex);
      }
      if (response != null) {
        log.info("EXT API Call Response: \n" + response);
      }
      return new CustomValidateResult(transportFailureType, "NOP", "Exception running task for endpoint " + validationData.getEndpoint());
    }
  }
}
