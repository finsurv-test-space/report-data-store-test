package za.co.synthesis.regulatory.report.dao;

import za.co.synthesis.regulatory.report.dao.PullReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 3/22/17.
 */
public class SimplePullReference implements PullReference {
  private final List<String> list;
  private int index = 0;

  public SimplePullReference(String trnRef) {
    list = new ArrayList<String>();
    list.add(trnRef);
  }

  public SimplePullReference(List<String> list) {
    this.list = list;
  }

  @Override
  public boolean isMore() {
    return index < list.size();
  }

  @Override
  public boolean isRefString() {
    return false;
  }

  @Override
  public String getNextReference() {
    if (index < list.size())
      return list.get(index++);
    else
      return null;
  }
}
