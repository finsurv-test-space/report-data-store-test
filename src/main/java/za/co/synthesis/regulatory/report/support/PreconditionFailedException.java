package za.co.synthesis.regulatory.report.support;

/**
 * Created by jake on 5/24/17.
 * 412
 */
public class PreconditionFailedException extends RuntimeException {
  public PreconditionFailedException(String message) {
    super(message);
  }
}