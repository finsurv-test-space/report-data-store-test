package za.co.synthesis.regulatory.report.security.jwt;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.NotImplementedException;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ProxyCallData;
import za.co.synthesis.regulatory.report.schema.AccountHolderRef;
import za.co.synthesis.regulatory.report.schema.PublicKeyRef;
import za.co.synthesis.regulatory.report.support.ProxyCaller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PublicKeySourceImpl implements IPublicKeySource {

  @Autowired
  private ProxyCaller proxy;
  
  @Autowired
  private SystemInformation systemInformation;


  public ProxyCaller getProxy() {
    return proxy;
  }

  public void setProxy(ProxyCaller proxy) {
    this.proxy = proxy;
  }


  public PublicKeyRef fetchKey(String keySource, String keyId) throws Exception {
    System.out.println("Begin public key fetch (ID: "+keyId+") from '" + keySource + "'...");
    String proxyName = "public_key_fetch" + (keySource != null && !keySource.isEmpty() ? "_" + keySource : "");
    List<PublicKeyRef> publicKeys = null;
    PublicKeyRef returnKey = null;
    Map<String, Object> newlyStoredKeys = new HashMap<>();
    if (proxy != null) {
      //TODO: Use proxy caller to fetch the key from the configured proxy entry...
      ProxyCallData proxyCallData = proxy.getProxyCallData(proxyName);
      if (proxyCallData != null) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("keySource", keySource);
        parameters.put("id", keyId);

        try {
          Object object = proxy.call(proxyCallData, parameters);
          //attempt to map to the object type we want...
          final ObjectMapper mapper = new ObjectMapper();
          publicKeys = mapper.convertValue(object, new TypeReference<List<PublicKeyRef>>() {
          });
          for (PublicKeyRef keyRef : publicKeys) {
            System.out.println("Storing public key: " + keyRef.getId() + " -> (" + keyRef.getAlgorithm() + ") " + keyRef.getPublicKey());
            if (systemInformation != null) {
              systemInformation.setStoredKey(keyRef.getId(), keyRef.getAlgorithm(), keyRef.getPublicKey(), false);
              newlyStoredKeys.put(keyRef.getId(), keyRef);
            } else {
              System.err.println("...1: SystemInformation instance not set -- autowire failed.  Unable to set key");
            }
          }
        } catch (Exception e) {
          throw new Exception("Proxy call failed for public key source: " + proxyName + " -- " + e.getMessage(), e);
        }
      }
      else {
        throw new NotImplementedException("Proxy caller configuration not implemented for a public key source named: "+ proxyName);
      }
    }
    else {
      throw new NotImplementedException("Proxy caller instance not instantiated when attempting to use config: "+ proxyName);
    }
    
    
    //Register the received {provider : id : key : alg : isPublic} in the RDS cert storage DB...
    if (publicKeys != null && publicKeys.size() > 0) {
      for (Object obj : publicKeys) {
        if (obj instanceof PublicKeyRef) {
          PublicKeyRef keyRef = (PublicKeyRef)obj;
          if (keyId != null && keyRef.getId() != null && keyId.equals(keyRef.getId())) {
            returnKey = keyRef;
          }
          if (keyRef.getId() != null && keyRef.getPublicKey() != null && newlyStoredKeys.get(keyRef.getId()) == null) {
            if (systemInformation != null) {
              try {
                systemInformation.setStoredKey(keyRef.getId(), keyRef.getAlgorithm(), keyRef.getPublicKey(), false);
                newlyStoredKeys.put(keyRef.getId(), keyRef);
              } catch(Exception e) {
                System.err.println("...2: Error while attempting to set key ("+keyRef.getId()+") from "+keyRef.getSource()+": "+e.getMessage());
              }
            } else {
              System.err.println("...2: SystemInformation instance not set -- autowire failed.  Unable to set key");
            }
          }
        }
      }
    }
    
    return returnKey;
  }

  @Override
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }


}