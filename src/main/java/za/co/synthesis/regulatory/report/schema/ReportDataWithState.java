package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Map;

/**
 * Created by jake on 7/27/17.
 */
@JsonPropertyOrder({
        "State"
        , "Meta"
        , "Report"
})
public class ReportDataWithState {
  private final String state;
  private final Map<String, Object> meta;
  private final Map<String, Object> report;

  public ReportDataWithState(ReportData reportData, String state) {
    this.meta = reportData.getMeta();
    this.report = reportData.getReport();
    this.state = state;
  }

  @JsonProperty("State")
  public String getState() {
    return state;
  }

  @JsonProperty("Meta")
  public Map<String, Object> getMeta() {
    return meta;
  }

  @JsonProperty("Report")
  public Map<String, Object> getReport() {
    return report;
  }

  //TODO: add another getter for the security info / context - user, timestamp etc

  @JsonIgnore
  public String getTrnReference() {
    return (String)getReport().get("TrnReference");
  }

  @JsonIgnore
  public ReportData getReportData(){
    return new ReportData(this.meta, this.report);
  }
}

