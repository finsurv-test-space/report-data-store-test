package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;


@JsonPropertyOrder({
    "ChannelName",
    "ReportSpace",
    "TrnReference",
    "Action"
})
public class ReportAction {
  private Channel channel = null;
  private String trnReference = null;
  private String action = null;

  public ReportAction(Channel channelName, String trnReference, String action) {
    this.channel = channel;
    this.trnReference = trnReference;
    this.action = action;
  }

  @JsonProperty("ChannelName")
  public String getChannelName() {
    if (channel != null) {
      return channel.getChannelName();
    }
    return null;
  }

  @JsonProperty("ReportSpace")
  public String getReportSpace() {
    if (channel != null) {
      return channel.getReportSpace();
    }
    return null;
  }

  public void setChannel(Channel channel) {
    this.channel = channel;
  }

  @JsonIgnore
  public Channel getChannel(){
    return channel;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }

  public void setTrnReference(String trnReferfence) {
    this.trnReference = trnReferfence;
  }

  @JsonProperty("Action")
  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

}
