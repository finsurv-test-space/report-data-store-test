package za.co.synthesis.regulatory.report.swagger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import za.co.synthesis.rule.core.ReportingAccountType;

import java.io.IOException;

/**
 * Created by jake on 3/31/16.
 */
public class SarbAccountTypeSerializer extends StdSerializer<ReportingAccountType> {

  protected SarbAccountTypeSerializer() {
    super(ReportingAccountType.class);
  }

  @Override
  public void serialize(ReportingAccountType value, JsonGenerator generator,
                        SerializerProvider provider) throws IOException {

    generator.writeString(value.getName());
  }
}
