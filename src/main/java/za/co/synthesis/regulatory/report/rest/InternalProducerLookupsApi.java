package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.rule.core.FlowType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("internal/producer/api/lookup")
public class InternalProducerLookupsApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Lookup.class);
  Map<String, RegisteredLookupConfig> lookupConfigs = new HashMap<>(); //TODO: write unit to populate from config.

  @Autowired
  private RulesCache rulesCache;

  private final Map<String, Lookup.LookupEntry> channelLookupMap = new HashMap<String, Lookup.LookupEntry>();

  //-----------------------------------------------------------------------------------------------
  //  Validation APIs
  //-----------------------------------------------------------------------------------------------
  /**
   * expose request for "producer/api/lookup/"
   * Exposed as "internal/producer/api/lookup/"
   */

  //-----------------------------------------------------------------------------------------------
  //  currencies

  @RequestMapping(value = "/currencies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Currency> getInternalCurrencies(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalCurrencies()");
    return Lookup.getCurrencies(channelName, channelLookupMap, rulesCache);
  }


  //-----------------------------------------------------------------------------------------------
  //  countries

  @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Country> getInternalCountries(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalCountries()");
    return Lookup.getCountries(channelName, channelLookupMap, rulesCache);
  }


  //-----------------------------------------------------------------------------------------------
  //  categories;

  @RequestMapping(value = "/categories", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Category> getInternalCategories(@RequestParam String channelName, @RequestParam FlowValue flow) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalCategories()");
    return Lookup.getCategories(channelName, flow.getFlowType(), channelLookupMap, rulesCache);
  }

  //-----------------------------------------------------------------------------------------------
  //  institutionalSectors

  @RequestMapping(value = "/institutionalSectors", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<InstitutionalSector> getInternalInstitutionalSectors(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalInstitutionalSectors()");
    return Lookup.getInstitutionalSectors(channelName, channelLookupMap, rulesCache);
  }


  //-----------------------------------------------------------------------------------------------
  //  industrialClassifications

  @RequestMapping(value = "/industrialClassifications", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<IndustrialClassification> getInternalIndustrialClassifications(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalIndustrialClassifications()");
    return Lookup.getIndustrialClassifications(channelName, channelLookupMap, rulesCache);
  }



//-----------------------------------------------------------------------------------------------
//  customsOffices

  @RequestMapping(value = "/customsOffices", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<CustomsOffice> getInternalCustomsOffices(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalCustomsOffices()");
    return Lookup.getCustomsOffices(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  reportingQualifiers

  @RequestMapping(value = "/reportingQualifiers", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalReportingQualifiers(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportingQualifiers()");
    return Lookup.getReportingQualifiers(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  nonResExceptionNames

  @RequestMapping(value = "/nonResExceptionNames", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalNonResExceptionNames(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalNonResExceptionNames()");
    return Lookup.getNonResExceptionNames(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  resExceptionNames

  @RequestMapping(value = "/resExceptionNames", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalResExceptionNames(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalResExceptionNames()");
    return Lookup.getResExceptionNames(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  moneyTransferAgents

  @RequestMapping(value = "/moneyTransferAgents", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalMoneyTransferAgents(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalMoneyTransferAgents()");
    return Lookup.getMoneyTransferAgents(channelName, channelLookupMap, rulesCache);
  }

//-----------------------------------------------------------------------------------------------
//  provinces

  @RequestMapping(value = "/provinces", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalProvinces(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalProvinces()");
    return Lookup.getProvinces(channelName, channelLookupMap, rulesCache);
  }

//-----------------------------------------------------------------------------------------------
//  cardTypes

  @RequestMapping(value = "/cardTypes", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getInternalCardTypes(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalCardTypes()");
    return Lookup.getCardTypes(channelName, channelLookupMap, rulesCache);
  }

//-----------------------------------------------------------------------------------------------
//  Holdco Companies
// ...Configurable External lookup?

  @RequestMapping(value = "/holdcoCompanies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<HoldcoCompany> getInternalHoldcoCompanies(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalHoldcoCompanies()");
    return Lookup.getHoldcoCompanies(channelName, channelLookupMap, rulesCache);
  }


  @RequestMapping(value = "/mtaAccounts", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<MTAAccount> getInternalMTAAccounts(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalMTAAccounts()");
    return Lookup.getMTAAccounts(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  IHQ Companies
// ...Configurable External lookup?

  @RequestMapping(value = "/ihqCompanies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<IhqCompany> getInternalIhqCompanies(@RequestParam String channelName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalIhqCompanies()");
    return Lookup.getIhqCompanies(channelName, channelLookupMap, rulesCache);
  }


//-----------------------------------------------------------------------------------------------
//  Externally Registered Lookups

  @RequestMapping(value = "/custom/{lookupName}", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Object> getInternalRegisteredLookup(@RequestParam String channelName,
                                   @PathVariable String lookupName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalRegisteredLookup()");
    return Lookup.getRegisteredLookup(channelName, lookupName, lookupConfigs, channelLookupMap, rulesCache);
  }


}
