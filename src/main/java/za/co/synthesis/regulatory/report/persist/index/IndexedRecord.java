package za.co.synthesis.regulatory.report.persist.index;

import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IndexedRecord {
  public enum IndexedRecordField{
    Type("Type"),//Report, Meta, System...
    Usage("Usage"),//Usage is used to determine whether the indexedRecordField is used for : Store, Search... etc
    Name("Name"),//Field name - TrnReference, ValueDate etc...
    Value("Value");//duh.


    private final String text;

    /**
     * @param text
     */
    private IndexedRecordField(final String text) {
      this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }
  }

  private final List<IndexFieldValue> fields = new ArrayList<IndexFieldValue>();

  public List<IndexFieldValue> getFields() {
    return fields;
  }

  public String getGUID() {
    for (IndexFieldValue value : fields) {
      if (value.getType().equals(CriterionType.System) && value.getName().equals(FieldConstant.GUID)) {
        return value.getValue();
      }
    }
    return null;
  }

  public String serialize() {
    List<Object> fieldMaps = new ArrayList<Object>();
    for (IndexFieldValue field : fields){
      Map<String, Object> map = new HashMap<String, Object>();
      map.put(IndexedRecordField.Type.toString(), field.getType().name());
      map.put(IndexedRecordField.Usage.toString(), field.getUsage().name());
      map.put(IndexedRecordField.Name.toString(), field.getName());
      map.put(IndexedRecordField.Value.toString(), field.getValue());
      fieldMaps.add(map);
    }
    return JsonSchemaTransform.mapToJsonStr(fieldMaps);
  }

  public void deserialize(String json) throws Exception{
    List<Object> fieldMaps = JsonSchemaTransform.jsonStrToList(json);
    for (Object field : fieldMaps){
      if (field instanceof Map) {
        Map<String, Object> map = (Map<String, Object>) field;
        String type = (String)map.get(IndexedRecordField.Type.toString());
        String usage = (String)map.get(IndexedRecordField.Usage.toString());
        String name = (String)map.get(IndexedRecordField.Name.toString());
        String value = (String)map.get(IndexedRecordField.Value.toString());
        IndexFieldValue ifv = new IndexFieldValue(CriterionType.valueOf(type), IndexField.Usage.valueOf(usage), name);
        ifv.setValue(value);
        this.fields.add(ifv);
      }
    }
  }
}
