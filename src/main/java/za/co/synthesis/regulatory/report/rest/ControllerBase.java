package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.rule.core.EvaluationException;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jake on 3/31/16.
 */
@ControllerAdvice
public class ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(ControllerBase.class);
  
  @ExceptionHandler(MultipartException.class)
  public void handleException(HttpServletResponse response, MultipartException e) throws IOException {
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    String desc = "File Size";
    HttpResponseUtil.writeError(response, desc, e.getMessage() + " (MAX file size: 10MB)");
    logger.error(desc + ": " + e.getMessage());
  }
  
  
  @ExceptionHandler(EvaluationException.class)
  public void handleEvaluationException(HttpServletResponse response, EvaluationException e) throws IOException {
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    String desc = "EvaluationException";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.error(desc + ": " + e.getMessage(), e);
  }

  @ExceptionHandler(Exception.class)
  public void handleException(HttpServletResponse response, Exception e) throws IOException {
    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    String desc = "Exception";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.error(desc + ": " + e.getMessage(), e);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public void handleException(HttpServletResponse response, ResourceNotFoundException e) throws IOException {
    response.setStatus(HttpStatus.NOT_FOUND.value());
    String desc = "ResourceNotFoundException";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(UnprocessableEntityException.class)
  public void handleException(HttpServletResponse response, UnprocessableEntityException e) throws IOException {
    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
    String desc = "UnprocessableEntityException";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(ResourceAccessAuthorizationException.class)
  public void handleException(HttpServletResponse response, ResourceAccessAuthorizationException e) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN.value());
    String desc = "ResourceAccessAuthorizationException";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(PreconditionFailedException.class)
  public void handleException(HttpServletResponse response, PreconditionFailedException e) throws IOException {
    response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
    String desc = "PreconditionFailedException";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.info(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public void handleException(HttpServletResponse response, HttpMessageNotReadableException e) throws IOException {
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    String desc = "BadRequest";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  public void handleException(HttpServletResponse response, MissingServletRequestParameterException e) throws IOException {
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    String desc = "BadRequest";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(AccessDeniedException.class)
  public void handleException(HttpServletResponse response, AccessDeniedException e) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN.value());
    String desc = "AccessDenied";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(InsufficientAuthenticationException.class)
  public void handleException(HttpServletResponse response, InsufficientAuthenticationException e) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN.value());
    String desc = "InsufficientAuthentication";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(InternalAuthenticationServiceException.class)
  public void handleException(HttpServletResponse response, InternalAuthenticationServiceException e) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN.value());
    String desc = "Authentication";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }

  @ExceptionHandler(AuthenticationException.class)
  public void handleException(HttpServletResponse response, AuthenticationException e) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN.value());
    String desc = "Authentication";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }
  
  @ExceptionHandler(AuthenticationServiceException.class)
  public void handleException(HttpServletResponse response, AuthenticationServiceException e) throws IOException {
    response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value()); //FORBIDDEN
    String desc = "Authentication";
    HttpResponseUtil.writeError(response, desc, e.getMessage());
    logger.warn(desc + ": " + e.getMessage());
  }
  
  @ExceptionHandler(IllegalStateException.class)
  public void handleException(HttpServletResponse response, IllegalStateException e) throws IOException {
    response.setStatus(HttpStatus.BAD_REQUEST.value());
    String desc = "Possible Request Size Error";
    HttpResponseUtil.writeError(response, desc, e.getMessage() + " (MAX file size: 5MB, MAX request size: 10MB)");
    logger.warn(desc + ": " + e.getMessage());
  }
  
}
