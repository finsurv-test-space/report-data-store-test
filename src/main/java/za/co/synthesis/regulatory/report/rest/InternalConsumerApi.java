package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("internal/consumer/api/")
public class InternalConsumerApi extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalConsumerApi.class);

    @Autowired
    ApplicationContext ctx;




//-----------------------------------------------------------------------------------------------
//  Form APIs
    //Keep the path the same, but prefix with "in-prx" to make it obvious that this is a proxy of another call.

//    /**
//     * proxy request for "producer/api/form"
//     * Exposed as "internal/producer/api/form"
//     * @param channelName
//     * @param trnReference
//     * @param response
//     * @throws Exception
//     */
//    @RequestMapping(value = "form", method = RequestMethod.GET, produces = "text/html")
//    @ResponseStatus(HttpStatus.OK)
//    public
//    @ResponseBody
//    void getInternalForm(@RequestParam String channelName,
//                 @RequestParam(required = false) String trnReference,
//                 HttpServletResponse response) throws Exception {
//        Form.getForm(channelName, trnReference, false, response, configStore, dataStore, channelCache, SecurityHelper.getAuthMeta());
//    }
//
//    //TODO: proxy Report APIs
//    //TODO: proxy Lookup APIs
//    //TODO: proxy Rules APIs
//    //TODO: proxy Document APIs
//    //TODO: proxy Enquiry APIs
//    //TODO: proxy Validation APIs
}
