package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.IDataStore;

import java.util.concurrent.ExecutorService;

/**
 * Created by jake on 8/3/17.
 */
public class HousekeepingProcess implements Runnable {
  private static final Logger logger = LoggerFactory.getLogger(HousekeepingProcess.class);

  private final ExecutorService executorService;
  private final SystemInformation systemInformation;
  private final RulesCache rulesCache;
  private final IDataStore dataStore;
  private final ProxyCaller proxyCaller;

  private boolean bStop;
  private boolean bRunning;
  private Runnable callback;

  // Internal timing for RulesCache
  private long rulesCacheMillis = 5*60000; // 5 Minutes
  private long sysTimeRulesCache = 0;

  // Internal timing for Sending Notifications
  private long sendNotificationsMillis = 1*60000; // 1 Minute
  private long sysTimeSendNotifications = 0;

  public HousekeepingProcess(ExecutorService executorService, SystemInformation systemInformation, RulesCache rulesCache,
                             IDataStore dataStore, ProxyCaller proxyCaller) {
    this.executorService = executorService;
    this.systemInformation = systemInformation;
    this.rulesCache = rulesCache;
    this.dataStore = dataStore;
    this.proxyCaller = proxyCaller;
  }

  public boolean isRunning() {
    return bRunning;
  }

  public void stop(Runnable callback) {
    this.callback = callback;
    this.bStop = true;
  }

  public void stop() {
    this.callback = null;
    this.bStop = true;
  }

  private void refreshRulesCache(long currentTimeMillis) {
    if (currentTimeMillis - sysTimeRulesCache > rulesCacheMillis) {
      sysTimeRulesCache = currentTimeMillis;
      executorService.submit(
              new RefreshRulesCacheTask(systemInformation.getChannelNames(), rulesCache)
      );
    }
  }

  private void sendNotifications(long currentTimeMillis) {
    if (currentTimeMillis - sysTimeSendNotifications > sendNotificationsMillis) {
      sysTimeSendNotifications = currentTimeMillis;
      executorService.submit(
              new SendNotificationsTask(dataStore, proxyCaller)
      );
    }
  }


  @Override
  public void run() {
    bStop = false;
    bRunning = true;
    while (!bStop) {
      long currentTimeMillis = System.currentTimeMillis();

      refreshRulesCache(currentTimeMillis);

      sendNotifications(currentTimeMillis);

      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        logger.error("Interrupted while housekeeping", e);
      }
    }
    bRunning = false;
    if (callback != null) {
      callback.run();
    }
  }
}
