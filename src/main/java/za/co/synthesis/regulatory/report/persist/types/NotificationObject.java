package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.persist.NotificationData;

import javax.persistence.Entity;

@Entity
public class NotificationObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(NotificationObject.class);

  private String queueName;
  private String content;
  private String reportSpace;
  private String reportKey; //trnReference

  public NotificationObject(String uuid) {
    super(uuid);
  }

  public NotificationObject(String reportSpace, String reportKey, String queueName, String content) {
    super(PersistenceObject.generateUUID());
    this.reportSpace = reportSpace;
    this.reportKey = reportKey;
    this.queueName = queueName;
    this.content = content;
  }

  public String getQueueName() {
    return queueName;
  }

  public void setQueueName(String queueName) {
    this.queueName = queueName;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public NotificationData getNotification() {
    return new NotificationData(getGUID(), reportSpace, reportKey, queueName, content);
  }
}
