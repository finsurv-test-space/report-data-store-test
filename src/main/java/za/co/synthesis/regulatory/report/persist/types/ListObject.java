package za.co.synthesis.regulatory.report.persist.types;

import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.ListData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/10/02.
 */
public class ListObject extends PersistenceObject {

  private String name;
  private List<FieldObject> fields = new ArrayList<FieldObject>();
  private List<CriterionObject> criteria = new ArrayList<CriterionObject>();


  public ListObject(String uuid) {
    super(uuid);
  }

  public ListObject(String uuid, ListData listData) {
    super(uuid);
    this.name = listData.getName();
    setCriteria(listData.getCriteria());
    setFields(listData.getFields());
  }

  public void setCriteria(List<CriterionData> criteria){
    if (criteria != null && criteria.size() > 0) {
      for (CriterionData criterion : criteria) {
        this.criteria.add(new CriterionObject(criterion.getType(), criterion.getName(), criterion.getValues(), "LIST_CRITERION"));
      }
    }
  }

  public void setFields(List<ListData.Field> fields) {
    if (fields != null && fields.size() > 0) {
      for (ListData.Field field: fields){
        this.fields.add(new FieldObject(field));
      }
    }
  }


  public ListData getListData(){
    ListData listData = new ListData(this.name);
    List<CriterionData> listCriteria = listData.getCriteria();
    if (this.criteria != null && this.criteria.size() > 0){
      for (CriterionObject criterionObject : this.criteria){
        listCriteria.add(criterionObject.getCriterionData());
      }
    }
    List<ListData.Field> listFields = listData.getFields();
    if (this.fields != null && this.fields.size() > 0){
      for (FieldObject field : this.fields){
        listFields.add(field.getField());
      }
    }
    return listData;
  }
}
