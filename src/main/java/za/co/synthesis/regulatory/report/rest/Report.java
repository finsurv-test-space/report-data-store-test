package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.ListData;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.persist.UserAccessData;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ValidationStats;
import za.co.synthesis.rule.core.Validator;

import java.util.List;
import java.util.Map;

/**
 * Created by jake on 5/24/17.
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/report")
public class Report extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Decision.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private UserInformation userInformation;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private ValidationEngineFactory engineFactory;

  public Report() {}

  public Report(SystemInformation systemInformation, UserInformation userInformation, DataLogic dataLogic,
                ValidationEngineFactory engineFactory) {
    this.systemInformation = systemInformation;
    this.userInformation = userInformation;
    this.dataLogic = dataLogic;
    this.engineFactory = engineFactory;
  }



  private Validator getValidator(String packageName) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getValidator()");
    return engineFactory.getEngineForCurrentDate(packageName).issueValidator();
  }
  
  
  /**
   * <p>
   * API End point to fetch a full history of the report matching the provided reference.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * Each version of the report through time will be included in the result set - <i>in descending order, chronologically</i>.
   * Any changes between each sequential version will be highlighted/listed in the relevant context object:
   * <ul>
   *   <li>ReportChanges</li>
   *   <li>MetaChanges</li>
   *   <li>StateChanges</li>
   * </ul>
   * <br/>
   * Such change objects will contain the type of change, the field and the relevant values that were changed.
   *
   * The user (and any additionally configured values) responsible for the change will be available in the ChangeContext object, along with a timestamp.
   * </p>
   * <p>
   *   <i>Example history format:</i><br/>
   *   <pre>
   *   {
   *   	"ReportSpace":"SARB",
   *   	"TrnReference":"TRN0412365275",
   *   	"ChangeSets":[
   *   		{
   *   			"ChangeContext":{
   *   				"Audit":{...},
   *   				"Username":"userX",
   *   				"DateTime":"2017-11-27 15:42:24"
   *   			},
   *   			"ReportChanges":[...],
   *   			"MetaChanges":[...],
   *   			"StateChange":{
   *   				"Type":"Added",
   *   				"Field":"state",
   *   				"To":"Incomplete"
   *   			},
   *   			"ReportData":{...}
   *   		},
   *   		{
   *   			"ChangeContext":{...},
   *   			"ReportChanges":[...],
   *   			"MetaChanges":[...],
   *   			"ReportData":{...}
   *   		}
   *   	]
   *   }
   *   </pre>
   * </p>
   * @param channelName (binary-optional) channel name from which the reporting space can be derived to find the relevant data
   * @param reportSpace (binary-optional) reporting space instance in which the report can be found. If a valid channelName is provided, the derived reportSpace therefrom will take precedence.
   * @param trnReference The reference for the report.
   * @return The report history object {@link ReportDataHistory} - containing the full report history, change context and field diffs, ordered by timestamp descending.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/history", method = RequestMethod.GET, produces = "application/json")
//  @PreAuthorize("ROLE_USER")
  public @ResponseBody
  ReportDataHistory getReportHistory(@RequestParam(required=false) String channelName,
                       @RequestParam(required=false) String reportSpace,
                       @RequestParam String trnReference) throws ResourceAccessAuthorizationException{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReportHistory()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }
    List<ReportInfo> reportHistory = dataLogic.getReportInfoHistory(reportSpace, trnReference, SecurityHelper.getAuthMeta());
    if (reportHistory == null || reportHistory.size() == 0)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    //check authorization (based on latest version of the report)...
    ReportInfo reportInfo = reportHistory.get(reportHistory.size() - 1);
    ReportDataWithState report = new ReportDataWithState(reportInfo.getReportData(), reportInfo.getState());
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to retrieve this resource."));

    return new ReportDataHistory(reportSpace, trnReference, reportHistory);
  }

  
  
  //----------  Get/Set Reports in the native schema  ----------//
  
  /**
   * <p>
   * Retreive the specified report from the reporting space stipulated, using the trnReference provided, in the schema requested.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * Should the persisted report be in a schema other than that provided, a schema transformation will be applied.
   * Should a transformation be required, wherever possible the source schema field will be carried across (or converted) as necessary to the target schema field analogue.
   * The return JSON object will contain the Meta and Report data sections, as well as a current state indicator for the report in question.
   * </p>
   * @param channelName (optional) the channel configuration required - used to determine the relevant report space, if the report space is not specified
   * @param reportSpace (optional) the report space to use, if the relevant channel name is not provided
   * @param schema the json schema layout to return the report in. If unknown the schema can be ommitted or populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param trnReference The report reference to retreive
   * @return The requested report object JSON (with state) in the schema specified {@link ReportDataWithState}
   */
  @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
//  @PreAuthorize("ROLE_USER")
  public @ResponseBody
  ReportDataWithState getReportDefault(
                       @RequestParam(required = false) String channelName,
                       @RequestParam(required = false) String reportSpace,
                       @RequestParam(required = false) String schema,
                       @RequestParam String trnReference) throws Exception{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReportDefault()");
    ReportDataWithState report = dataLogic.getReportData(schema, channelName, reportSpace, trnReference, SecurityHelper.getAuthMeta());
    if (report == null) {//the above method should already handle this case.
      throw new ResourceNotFoundException("Unable to find the specified report.  Please ensure you specify the correct channel or reporting space and the correct trnReference for the report in question.");
    }
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to retrieve this resource."));
    return report;
  }

  /**
   * <p>
   * <b>DEPRECATED</b> Make use of the getReportDefault API instead.
   * <br/><br/>
   * Retreive the specified report from the reporting space stipulated, using the trnReference provided, in the schema requested.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * Should the persisted report be in a schema other than that provided, a schema transformation will be applied.
   * Should a transformation be required, wherever possible the source schema field will be carried across (or converted) as necessary to the target schema field analogue.
   * The return JSON object will contain the Meta and Report data sections, as well as a current state indicator for the report in question.
   * </p>
   * @param schema the json schema layout to return the report in. If unknown or irrelevant, the schema can be populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param channelName (optional) the channel configuration required - used to determine the relevant report space, if the report space is not specified
   * @param reportSpace (optional) the report space to use, if the relevant channel name is not provided
   * @param trnReference The report reference to retreive
   * @return The requested report object JSON (with state) in the schema specified {@link ReportDataWithState}
   */
  @RequestMapping(value = "/data/{schema}", method = RequestMethod.GET, produces = "application/json")
//  @PreAuthorize("ROLE_USER")
  @Deprecated
  public @ResponseBody
  ReportDataWithState getReport(@PathVariable String schema,
                       @RequestParam(required = false) String channelName,
                       @RequestParam(required = false) String reportSpace,
                       @RequestParam String trnReference) throws Exception{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReport()");
    ReportDataWithState report = dataLogic.getReportData(schema, channelName, reportSpace, trnReference, SecurityHelper.getAuthMeta());
    if (report == null) {//the above method should already handle this case.
      throw new ResourceNotFoundException("Unable to find the specified report.  Please ensure you specify the correct channel or reporting space and the correct trnReference for the report in question.");
    }
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to retrieve this resource."));
    return report;
  }
  
  
  /**<p>
   * Inserts the report provided <b><i>(containing both the report data and the relevant evaluation decision)</i></b> into the system, in the schema specified (JSON converted, if not in the schema specified).
   * Upon insertion, the backend server will perform a validation against the channel rules - as per the channel specified.
   * The channel specified has a significant impact on the validation outcomes as the rules are inherited from the base regulatory rules, but each channel may add or override specific rules.
   * For example: a report may fail against the base rules (ie: coreSARB, coreSADC, coreBON...) but may pass without error or warning on a channel specific package which disables a number of rules against data which the channel may not have knowledge of.
   * Similarly, the schema of the report is important, as each set of rules is designed to work against a specific schema with specific fields.
   * Attempting to validate a report from say a SADC schema against a SARB validation rule may fail because expected fields will not be present or populated or the transformation between schemas may not be able to add fields where no analogue is available in the source schema.
   * Depending on the current state and the configured report lifecycle, a successful or failed validation may change the state of the report.
   * </p>
   * @param channelName the channel package to use when validating the report data provided, and related report space to store it in
   * @param schema the JSON schema to store the report in the system as.  If unknown, the schema can be ommitted or populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param report the JSON report object to store (with the selected evaluation decision included in the JSON)
   * @return The validation result object {@link ValidationResult}, including all warnings and errors regarding issues detected within the report data.<br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/decision/data", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  ValidationResult setReportWithDecisionDefault(
          @RequestParam String channelName,
          @RequestParam(required = false) String schema,
          @RequestBody ReportDataWithDecision report) throws ResourceAccessAuthorizationException{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setReportWithDecisionDefault()");
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));
    if (report != null && report.getDecision() != null && report.getTrnReference() != null){
      Decision.setDecision(channelName, report.getTrnReference(), report.getDecision(), systemInformation, dataLogic);
    }
    return dataLogic.setReportData(schema, channelName, report, SecurityHelper.getAuthMeta());
  }


  /**<p>
   * <b>DEPRECATED</b> Make use of the setReportWithDecisionDefault API instead.
   * <br/><br/>
   * Inserts the report provided <b><i>(containing both the report data and the relevant evaluation decision)</i></b> into the system, in the schema specified (JSON converted, if not in the schema specified).
   * Upon insertion, the backend server will perform a validation against the channel rules - as per the channel specified.
   * The channel specified has a significant impact on the validation outcomes as the rules are inherited from the base regulatory rules, but each channel may add or override specific rules.
   * For example: a report may fail against the base rules (ie: coreSARB, coreSADC, coreBON...) but may pass without error or warning on a channel specific package which disables a number of rules against data which the channel may not have knowledge of.
   * Similarly, the schema of the report is important, as each set of rules is designed to work against a specific schema with specific fields.
   * Attempting to validate a report from say a SADC schema against a SARB validation rule may fail because expected fields will not be present or populated or the transformation between schemas may not be able to add fields where no analogue is available in the source schema.
   * Depending on the current state and the configured report lifecycle, a successful or failed validation may change the state of the report.
   * </p>
   * @param schema the JSON schema to store the report in the system as.  If unknown, the schema can be populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param channelName the channel package to use when validating the report data provided, and related report space to store it in
   * @param report the JSON report object to store (with the selected evaluation decision included in the JSON). <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @return The validation result object {@link ValidationResult}, including all warnings and errors regarding issues detected within the report data.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/decision/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  @Deprecated
  public
  @ResponseBody
  ValidationResult setReportWithDecision(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody ReportDataWithDecision report) throws ResourceAccessAuthorizationException{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setReportWithDecision()");
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));
    if (report != null && report.getDecision() != null && report.getTrnReference() != null){
      Decision.setDecision(channelName, report.getTrnReference(), report.getDecision(), systemInformation, dataLogic);
    }
    return dataLogic.setReportData(schema, channelName, report, SecurityHelper.getAuthMeta());
  }

  
  
  /**<p>
   * Inserts the report provided into the system, in the schema specified (JSON converted, if not in the schema specified).
   * Upon insertion, the backend server will perform a validation against the channel rules - as per the channel specified.
   * The channel specified has a significant impact on the validation outcomes as the rules are inherited from the base regulatory rules, but each channel may add or override specific rules.
   * For example: a report may fail against the base rules (ie: coreSARB, coreSADC, coreBON...) but may pass without error or warning on a channel specific package which disables a number of rules against data which the channel may not have knowledge of.
   * Similarly, the schema of the report is important, as each set of rules is designed to work against a specific schema with specific fields.
   * Attempting to validate a report from say a SADC schema against a SARB validation rule may fail because expected fields will not be present or populated or the transformation between schemas may not be able to add fields where no analogue is available in the source schema.
   * Depending on the current state and the configured report lifecycle, a successful or failed validation may change the state of the report.
   * </p>
   * @param channelName the channel package to use when validating the report data provided, and related report space to store it in
   * @param schema the JSON schema to store the report in the system as.  If unknown, the schema can be ommitted or populated with the value <b>"default"</b> to make use of the default channel schema (assuming one is configured) where necessary.
   * @param report the JSON report object to store. <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @return The validation result object {@link ValidationResult}, including all warnings and errors regarding issues detected within the report data.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/data", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  ValidationResult setReportDefault(
          @RequestParam String channelName,
          @RequestParam(required = false) String schema,
          @RequestBody ReportData report) throws ResourceAccessAuthorizationException{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setReportDefault()");
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));
    return dataLogic.setReportData(schema, channelName, report, SecurityHelper.getAuthMeta());
  }



  /**<p>
   * <b>DEPRECATED</b> Make use of the setReportDefault API instead.
   * <br/><br/>
   * Inserts the report provided into the system, in the schema specified (JSON converted, if not in the schema specified).
   * Upon insertion, the backend server will perform a validation against the channel rules - as per the channel specified.
   * The channel specified has a significant impact on the validation outcomes as the rules are inherited from the base regulatory rules, but each channel may add or override specific rules.
   * For example: a report may fail against the base rules (ie: coreSARB, coreSADC, coreBON...) but may pass without error or warning on a channel specific package which disables a number of rules against data which the channel may not have knowledge of.
   * Similarly, the schema of the report is important, as each set of rules is designed to work against a specific schema with specific fields.
   * Attempting to validate a report from say a SADC schema against a SARB validation rule may fail because expected fields will not be present or populated or the transformation between schemas may not be able to add fields where no analogue is available in the source schema.
   * Depending on the current state and the configured report lifecycle, a successful or failed validation may change the state of the report.
   * </p>
   * @param schema the JSON schema to store the report in the system as.  If unknown, the schema can be populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param channelName the channel package to use when validating the report data provided, and related report space to store it in
   * @param report the JSON report object to store. <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @return The validation result object {@link ValidationResult}, including all warnings and errors regarding issues detected within the report data.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  @Deprecated
  public
  @ResponseBody
  ValidationResult setReport(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody ReportData report) throws ResourceAccessAuthorizationException{
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setReport()");
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));
    return dataLogic.setReportData(schema, channelName, report, SecurityHelper.getAuthMeta());
  }

  
  /**
   * <p>
   * Retreives the current state of the report specified.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * The state of the report will be dependent on the report life cycle configuration and validity of the report - amongst other factors.
   * </p>
   * @param channelName (optional) Used to retreive the relevant reporting space if not provided
   * @param reportSpace (optional) The reporting space from which to retreive the report specified
   * @param trnReference The report reference to retreive
   * @return ReportState object which will provide the reportSpace in which the report resides, the trnReference and the current state of the report in the configured report lifecycle.
   */
  @RequestMapping(value = "/state", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ReportState getReportState(@RequestParam(required = false) String channelName,
                             @RequestParam(required = false) String reportSpace,
                             @RequestParam String trnReference) throws ResourceAccessAuthorizationException {

    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReportState()");
    if (reportSpace == null && channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null)
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");

    ReportInfo report = dataLogic.getReport(reportSpace, trnReference);
    if (report == null)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    return new ReportState(reportSpace, trnReference, report.getState());
  }
  
  /**
   * <p>
   * Explicitly sets the specified report into the state stipuldated - regardless of the current state, validity or configured
   * report lifecycle.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * </p>
   * @param channelName (optional) the channel configuration containing the relevant reporting space to use
   * @param reportSpace (optional) the report space in which the report resides
   * @param trnReference The report reference
   * @param state The state into which the report should be placed
   * @return A report state object {@link ReportState} which reveals the current state of the report
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/state", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  ReportState setReportState(
          @RequestParam(required = false) String channelName,
          @RequestParam(required = false) String reportSpace,
          @RequestParam String trnReference,
          @RequestParam String state) throws ResourceAccessAuthorizationException {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "setReportState()");
    if (reportSpace == null && channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null)
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");

    if (!systemInformation.hasReportSpaceStateData(reportSpace, state)) {
      throw new PreconditionFailedException("A valid state must be specified for reportSpace " + reportSpace);
    }

    ReportInfo report = dataLogic.getReport(reportSpace, trnReference);
    if (report == null)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report.getReportData(), true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));

    ReportInfo returnedReport = dataLogic.setReportState(report, channelName, state, SecurityHelper.getAuthMeta());

    return new ReportState(reportSpace, trnReference, returnedReport.getState());
  }

  private static MapCustomValues getCustomValues(Map<String, Object> meta) {
    if (meta == null) {
      meta = new JSObject();
    }
    if (!meta.containsKey("DealerType"))
      meta.put("DealerType", "AD");

    return new MapCustomValues(meta);
  }


  /*
   * <p>
   * Stateless validation of the provided report JSON object (not stored)
   * The schema specified, together with the channelName are important as they dictate which set of rules to apply in the validation.
   * With regard to the rules, it is important to note that because the rules are inherited from the base regulatory rules, but are amended, over-ridden, disabled or augmented by each subsequent channel set - a report may fail validation against the base rule set, but pass on one of the channel rule sets.
   * This is by design, as certain up-stream channels may have only a partial set of data regarding the transaction in question and hence rules for such a channel would disable any validation rules against data which it does not have.
   * This enables a report to be channel-complete and progress to the next point in the report lifecycle, to be enriched and re-validated by the next channel in the lifecycle.
   *
   * For each validation rule that fails, a warning or error will be generated, with a detailed description and added to the result set.
   * </p>
   * @param schema the schema for the report object (translated as required).  If unknown, the schema can be populated with the value <b>"default"</b> to make use of the default channel schema where necessary.
   * @param channelName The channel package and rules to apply to the validation
   * @param report the report JSON object {@ReportData}
   * @return Validation response objects {@link ValidationResponse} for all warnings and errors within the report data
   * @throws Exception
   */
//  @RequestMapping(value = "/validate/{schema}", method = RequestMethod.POST, produces = "application/json")
//  public
//  @ResponseBody List<ValidationResponse> validate(
//      @PathVariable String schema, //this is a deprecated requirement
//      @RequestParam String channelName,
//      @RequestBody ReportData report) throws Exception {
//    Channel channel = systemInformation.getChannel(channelName);
//
//    ValidationStats stats = new ValidationStats();
//    List<ResultEntry> validationResults;
//    try {
//      Validator validator = getValidator(channelName);
//      validationResults = validator.validate (new MapFinsurvContext(report.getReport()), getCustomValues(report.getMeta()), stats);
//    }
//    catch (Exception e) {
//      logger.error("Cannot validate transaction", e);
//      throw e;
//    }
//
//    return dataLogic.filterValidationResults(validationResults);
//  }


  /**
   * <p>
   * Stateless validation of the provided report JSON object (not stored)
   * The schema specified, together with the channelName are important as they dictate which set of rules to apply in the validation.
   * With regard to the rules, it is important to note that because the rules are inherited from the base regulatory rules, but are amended, over-ridden, disabled or augmented by each subsequent channel set - a report may fail validation against the base rule set, but pass on one of the channel rule sets.
   * This is by design, as certain up-stream channels may have only a partial set of data regarding the transaction in question and hence rules for such a channel would disable any validation rules against data which it does not have.
   * This enables a report to be channel-complete and progress to the next point in the report lifecycle, to be enriched and re-validated by the next channel in the lifecycle.
   *
   * For each validation rule that fails, a warning or error will be generated, with a detailed description and added to the result set.
   * </p>
   * The schema for the report object is derived from the provided channelName (JSON is translated as required).
   * @param channelName The channel package and rules to apply to the validation
   * @param report the report JSON object {@ReportData}
   * @return Validation response objects {@link ValidationResponse} for all warnings and errors within the report data
   * @throws Exception
   */
  @RequestMapping(value = "/validate/{channelName}", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody List<ValidationResponse> validateByChannel(
      @PathVariable String channelName,
      @RequestBody ReportData report) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "validateByChannel()");
    Channel channel = systemInformation.getChannel(channelName);

    ValidationStats stats = new ValidationStats();
    List<ResultEntry> validationResults;
    try {
      Validator validator = getValidator(channelName);
      validationResults = validator.validate (new MapFinsurvContext(report.getReport()), getCustomValues(report.getMeta()), stats);
    }
    catch (Exception e) {
      logger.error("Cannot validate transaction", e);
      throw e;
    }

    return dataLogic.filterValidationResults(validationResults);
  }


  /**
   * <p>
   * Apply the action requested against the report specified.  The action specified must be one configured in the
   * report lifecycle.
   *
   * It is important to note that actions can be configured to only be allowed for specific report states, schemas, channels and reporting spaces.
   * Thus, should an action be called for a report which is not in the required state - the action will not be applied and the report will remain in the current state.
   *
   * However, should the report be in the necessary state (etc) the action applied, will move the report into the configured, new state and may result in additional internal-actions being triggered should they be configured as such (for example : sending of notifications).
   *
   * Requires either a channel name to be provided or a report space.
   * </p>
   * @param channelName (optional) the channel package configuration containing the target reporting space, should it not be specified.  <br/><b><u>PLEASE NOTE:</u> The channel specified may have an impact on the outcome of the action carried out</b>
   * @param reportSpace (optional) the reporting space in which the report resides
   * @param trnReference The reference of the report to action
   * @param action the action to apply
   * @return Returns the resulting report state after the action has been applied - as per the configured report life cycle
   * @throws Exception
   */
  @RequestMapping(value = "/action", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  ReportState doUserAction(
          @RequestParam(required = false) String channelName,
          @RequestParam(required = false) String reportSpace,
          @RequestParam String trnReference,
          @RequestParam String action) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "doUserAction()");
    if (reportSpace == null && channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null)
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");

    ReportState reportState = dataLogic.doAction(reportSpace, channelName, trnReference, action, SecurityHelper.getAuthMeta());
    logger.debug("Final state of action '"+action+"' on report '"+trnReference+"' : "+reportState.getState());
    return reportState;
  }
  
  
  /**
   * <p>
   * Change the reference of a report to a new, unused reference number.  Any report history pulled after the fact will
   * clearly show the transition from one report reference value to another.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * This feature is particularly valuable when a report is generated prior to a transaction reference being created or known.
   * This report can then be built up using some up-stream payment initiation channel and migrated to the relevant transaction reference as soon as a down-stream channel initiates the relevant transaction.
   * When the report is "migrated" to the new reference, all historical versions are migrated as well, and the original reference is added to a "RDS_AssociatedReferences" field in the meta section.
   * This means that:
   * <ul>
   * <li>Should a history be requested for the report, the full history - predating the reference change - will be included.</li>
   * <li>Any history request will show the change in trnReference at the point it is done, as a separate historical item</li>
   * <li>Any searches for the previous report reference will yield the latest version of the report - at the very least, when performing a fullText search.</li>
   * <li>Should the client require the original reference, it is readily available in the meta section of the report</li>
   * </ul>
   * </p>
   * @param channelName (optional/mutually exclusive) the channel configuration containing the report space in which the report resides
   * @param reportSpace (optional/mutually exclusive) the report space in which the report resides
   * @param trnReference the current reference value for the report
   * @param newReference the new, target reference value for the report
   * @return Returns the report JSON object {@link ReportInfo} after the new reference value has been applied
   * @throws Exception
   */
  @RequestMapping(value = "/reference", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  ReportInfo doChangeReportReference(
          @RequestParam(required = false) String channelName,
          @RequestParam(required = false) String reportSpace,
          @RequestParam String trnReference,
          @RequestParam String newReference) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "doChangeReportReference()");
    if (reportSpace == null && channelName == null)
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");

    if (reportSpace == null && channelName != null) {
      Channel channel = systemInformation.getChannel(channelName);
      reportSpace = channel != null ? channel.getReportSpace() : null;
    }
    if (reportSpace == null )
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    
    logger.debug("Call to change report reference '"+trnReference+"' => "+newReference);
    ReportInfo report = dataLogic.getReport(reportSpace, trnReference);
    if (report == null)
      throw new ResourceNotFoundException("The specified resource was not found, please provide a valid trnReference");
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report.getReportData(), true, new ResourceAccessAuthorizationException("You are not authorized to retrieve or alter this resource."));
    
    return dataLogic.renameReportReference(report, newReference, SecurityHelper.getAuthMeta());
  }
  
  
  /**
   * <p>
   * End-point for listing reports which match the criteria for the specified report list requested.
   * The columns returned are those as are specified in the list configuration.
   * The criteria from the list configuration is intersected with criteria from authentication objects and search queries.
   * List results can be limited to a set of a specific size by providing a value to the maxResults parameter.
   * Pagination is supported for the resultset.
   *
   * Requires either a channel name to be provided or a report space.
   *
   * Searching can be done in a manner similar to a google search, whereby relevant values are provided and a server side
   * script will parse the provided values and determine backend queries to run on the dataset to return only the most
   * relevant reports.
   * More specific searches can be performed if a "search param" has been configured for the relevant list.  such a param
   * allows the user to specify values for a specific field in the result JSON.
   * Additionally, logic has been provided to allow for wild-card searches whereby a percentage symbol can be used to
   * match against a string of unknown characters, but where any other specified characters must match in order otherwise.
   * Contains (and partial match) searches are also possible if the relevant wild-card pattern or search string
   * (containing spaces etc) is surronded  by double quotes.
   * </p>
   * <br/><br/>
   * <p>
   * <b>Searching for Reports using the List API:</b><br/>
   * It is important to understand that List definitions are defined such that particular criteria are stipulated, and when a list is called, a search is initiated against the data store to return a list of items that match the configured criteria for the particular list. see
   * The search function has been enhanced to allow for the following search types:
     * <p>
     * <b>Google-like search:</b><br/>
     * Terms are entered into the search string which is passed to a fulltext index search, and the matching items are returned in descending order of their ranking - the highest ranked match is returned in the first position.
     * Ranking is determined by a heuristic which takes into account - among other possible measures:
     * <ul>
     * <li>Number of terms that match</li>
     * <li>Distance scores between terms (in relation to each other)</li>
     * <li>Distance scores between search term and stemmed match term</li>
     * <li>Term's non-noise rating</li>
     * </ul>
     * </p>
     * <p>
     * <b>Partial match / Wild-card and Contains search:</b><br/>
     * If a search term is provided which contains a wild-card character ("%" or "*"), the search mechanism will automatically perform a "Contains" search (Intersected with any additional authority, list definition or search criteria) and retrieve any items that match the provided wild-card pattern.
     * IE: "D%KEY K%G" will match to "DONKEY KONG" and index:value searches too.
     * <br/>
     * To search for items that begins with a particular term (IE: "PONKE..."), one could use the following syntax:  "PONKE%"  → matches to "PONKETTE" or "PONKEMNITY", ...AKA a Wild-card search.
     * *Note: if the search term contains a space " " between two words, then Double-Quotes (") should be used to encompass the entire search term (AKA: Contains search),
     * IE:
     * "%THING ELSE" → matches to "SOMETHING ELSE" or "EVERYTHING ELSE"    ...this is a contains search - note the double quotes in the original search string.
     * %THING ELSE → matches to "SOMETHING FROM SOMEONE ELSE" or even "THERES SOMEONE ELSE THAT WANTS NOTHING TO DO WITH YOU"  ...this is translated into a freetext search for "ELSE" and a Wild-card or Contains search for "%THING"
     * </p>
     * <p>
     * <b>Index Match:</b><br/>
     * As part of a list definition, certain fields in the report JSON can be specified as "SEARCH:" indices.  if in the search string, the search index is specified with a value (syntax =>  [searchIndex]:[value]), the system will assign a criteria to the list search such that only reports with the specified value, for the associated field are returned.
     * For example, if we had a list definition as follows:
     * <pre>
     * {
     *   "name":"Trades",
     *   "fields": [
     *     {"label": "Reference", "report": "TrnReference"},
     *     {"label": "Status", "system": "State"},
     *     {"label": "Value Date", "report": "ValueDate"},
     *     {"label": "Currency", "report": "FlowCurrency"},
     *     {"label": "Amount", "report": "TotalForeignValue"},
     *     {"label": "Corporate", "meta": "Corporate"},
     *     {"label": "Trade Reference", "meta": "TradeReference"},
     *     {"label": "ZAR", "report": "ZAREquivalent"},
     *     {"label": "Flow", "report": "Flow"}
     *   ],
     *   "criteria": [
     *     {"system": "State", "value": "Incomplete"},
     *     {"meta": "TradeReference", "values": "${SEARCH:tradeRef}"}
     *   ]
     * }
     * </pre>
     * <br/>
     * then the search string to find all reports with a "tradeRef" (meta.TradeReference) value of '123456' (in a state of "Incomplete", as per the list definition) might look as follows:
     * </p>
     *
     *
     * <p>
     * Furthermore, it is important to note that should the search string contain a number of terms, some of which containing wildcards, some without, perhaps even some index search terms, the underlying logic will parse the terms and group them into separate queries, which are joined as an intersect - such that the only items that will be returned are those that are a match for each query.  IE: If we specify a search string of 'JOSHUA SNOW% tradeRef:65412 "Rampart Road" somewhere around the world' against the "Trades" list as defined above, the only reports that will be returned in the list are those that have results in queries as follows:
     * <br/>
     * <b>freetext (google-like) search</b> containing at least one of the following terms "JOSHUA somewhere around the world"  (note that "the" is considered a noise word and will likely be excluded from the search)
     * Wild-card search where the report contains atleast one value that matches to "SNOW..." → ie: "SNOWDEN" or "SNOWPATROL"
     * Contains seach where the report contains atleast one instance of the string/term "Rampart Road"
     * Search index where the report has a meta.TradeReference value of '65412'.
     * List Definition Criteria (as per the "Trades" list definition above) - Report state must be "Incomplete"
     * ...ONLY reports that match on ALL of the above queries will be returned in the list (ignoring any authorization criteria).
     * </p>
   * </p>
   * @param channelName Optional channel name from which to derive the reporting space in which the data resides.
   * @param reportSpace Optional reporting space against which to search for matching records. If a valid channelName is supplied, the derived reportSpace will take precedence.
   * @param listName The name of the configured list to use
   * @param search the search string to be parsed for generating additional search specifications
   * @param maxRecords The maximum number of results for the search to return
   * @param page The page at which to begin returning results for
   * @param sort
   * @return the list of matching results in order of rank - most relevant to least relevant {@link ListResult}
   * @throws Exception
   */
  @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ListResult getReportList(
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam String listName,
          @RequestParam(required = false) String search,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) Integer page,
          @RequestParam(required = false) String[] sort) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getReportList()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }

    ListData listDef = systemInformation.getListDataByName(listName);
    if (listDef == null) {
      throw new PreconditionFailedException("A valid list name must be specified");
    }
    UserAccessData userAccess = userInformation.getLoggedOnUserAccessData();


    Map<String, String> searchTokens = SerializationTools.parseSearchString(search);
    if (sort != null && sort.length > 0){
      return dataLogic.getOrderedReportList(reportSpace, listDef, searchTokens, new SortDefinition(sort, listDef), userAccess, maxRecords, page);
    }

    return dataLogic.getReportList(reportSpace, listDef, searchTokens, userAccess, maxRecords, page);
  }
}
