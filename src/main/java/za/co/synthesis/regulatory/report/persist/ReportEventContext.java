package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.jwt.JWTAuthToken;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by james on 2017/09/05.
 */
public class ReportEventContext {
  private final String username;
  private final LocalDateTime dateTime;
  private final Map<String, String> audit = new HashMap<>();
  private final Map<String, Object> other = new HashMap<>();

  public ReportEventContext(String username, LocalDateTime dateTime) {
    this.username = username;
    this.dateTime = dateTime;
  }

  public ReportEventContext(ReportServicesAuthentication authentication, LocalDateTime dateTime) {
    this.username = authentication.getName();
    this.dateTime = dateTime;
    for (Map.Entry<String,RequestParamData> entry : authentication.getAuditFields().entrySet()) {
      this.audit.put(entry.getKey(), entry.getValue().csvValues(false));
    }
    if (authentication.getWrappedInstance() instanceof JWTAuthToken) {
      JWTAuthToken jwtAuth = (JWTAuthToken)authentication.getWrappedInstance();
      if (jwtAuth.getOther() != null) {
        this.other.putAll(jwtAuth.getOther());
      }
    }
  }

  public String getUsername() {
    return username;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }


  public Map<String, String> getAuditFields() {
    return audit;
  }

  public void putAudit(String key, String value){
    if ((key != null && !key.isEmpty()) &&
            (key != null && !key.isEmpty()))
      audit.put(key, value);
  }

  public Map<String, Object> getOther() {
    return other;
  }
}

