package za.co.synthesis.regulatory.report.persist;

public class JsonConstant {
  public static String MonetaryAmount = "MonetaryAmount";
  public static String ImportExport = "ImportExport";
  public static String UniqueObjectInstanceID = "#UOIID";
  public static String SequenceNumber = "SequenceNumber";
}

