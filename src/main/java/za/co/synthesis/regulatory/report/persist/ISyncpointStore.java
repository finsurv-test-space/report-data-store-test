package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.persist.common.TableDefinition;

/**
 * Created by jake on 8/21/17.
 */
public interface ISyncpointStore {
  String getLocalSyncpoint(TableDefinition tableDefinition);
  void setLocalSyncpoint(TableDefinition tableDefinition, String syncpoint);

  String getRemoteSyncpoint(TableDefinition tableDefinition);
  void setRemoteSyncpoint(TableDefinition tableDefinition, String syncpoint);
}
