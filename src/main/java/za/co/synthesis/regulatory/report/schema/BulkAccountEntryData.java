package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.AccountEntryData;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({
        "AccountEntries"
})
public class BulkAccountEntryData {
    private List<AccountEntryData> accountEntries = new ArrayList<>();

    public BulkAccountEntryData(List<AccountEntryData> accountEntries){ this.accountEntries = accountEntries;}
    public BulkAccountEntryData(){}

    @JsonProperty("AccountEntries")
    public List<AccountEntryData> getAccountEntries(){return accountEntries;}

    public void setAccountEntries( List<AccountEntryData> accountEntries) { this.accountEntries = accountEntries;}
}
