package za.co.synthesis.regulatory.report.persist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.Map;

@JsonPropertyOrder({
    "ReportSpace"
        ,"TrnReference"
        ,"Content"
        ,"Version"
        })
public class AccountEntryData {
    private String reportSpace;

    private String trnReference;
    private FileData content;
    private ReportEventContext eventContext;
    private Integer version = 1;



    public AccountEntryData(String reportSpace, String trnReference, FileData content, ReportEventContext eventContext, Integer version) {
        this.reportSpace = reportSpace;
        this.trnReference = trnReference;
        this.content = content;
        this.eventContext = eventContext;
        this.version = (version == null ? 1 : version);
    }


    @JsonProperty("ReportSpace")
    public String getReportSpace() {
        return reportSpace;
    }

    public void setReportSpace(String reportSpace) {
        this.reportSpace = reportSpace;
    }

    @JsonProperty("TrnReference")
    public String getTrnReference() {
        return trnReference;
    }
    public void setTrnReference(String trnReference) {
        this.trnReference = trnReference;
    }

    @JsonProperty("Content")
    public FileData getContent() {
        return content;
    }

    public void setContent(FileData content) {
        this.content = content;
    }

    //    public ReportEventContext getEventContext() {
//        return eventContext;
//    }

    //@JsonProperty("EventContext")
    @JsonIgnore
    public void setEventContext(ReportEventContext eventContext) {
        this.eventContext = eventContext;
    }

    @JsonProperty("Version")
    public Integer getVersion() {
        return version == null ? 1 : version;
    }

    public void setVersion(Integer version) {
        this.version = version == null ? 1 : version;
    }
}
