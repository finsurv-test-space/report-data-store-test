package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */

@Entity
public class ActivityLogObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(ActivityLogObject.class);

  private String reportSpace;
  private String reportKey;
  private String channelName;
  //  private final ReportData reportData;
  @ManyToMany(fetch=FetchType.EAGER)
  private Map<String, Object> activityData = new HashMap<String,Object>();
  private Map<String, Object> eventContext;
  private Integer version = 1;
  private LogActivityType activityType = LogActivityType.SYSTEM;
  


  public ActivityLogObject(String uuid) {
    super(uuid);
    try{
      ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
      if (user != null) {
        this.activityData.put("User", user.getName());
        if (this.eventContext == null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      }
    } catch (Exception e){}
  }

  public ActivityLogObject(String uuid, LogActivityType activityType, LogReadWrite ioAction, String Url, String httpAction, ReportInfo reportData, String logBody, Map<String, Object> dataParams) {
    super(uuid);
    if (reportData != null) {
      this.reportSpace = reportData.getReportSpace();
      this.channelName = reportData.getChannelName();

      if (reportData.getReportData() != null) {
        this.reportKey = reportData.getReportData().getTrnReference();
      }

      if (reportData.getEventContext() == null) {
        try {
          //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
          ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
          if (user != null) {
            this.eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
          }
        } catch (Exception e) {
        }
      } else {
        this.eventContext = SerializationTools.serializeEventContext(reportData.getEventContext());
      }
    }
  }


  public String getReportSpace(){
    return reportSpace;
  }

  public void setReportSpace(String reportSpace){
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public ReportEventContext getEventContext() {
    return eventContext != null ? SerializationTools.deserializeEventContext(eventContext) : null;
  }

  public String getAuthToken() {

    if (this.eventContext == null) {
      try {
        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
        if (user != null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      } catch (Exception e) {
        //we seem to be running into null pointers where we really shouldn't be.
        logger.warn("Unable to persist the ActivityLogObject eventContext / Audit detail : " + e.getMessage());
        e.printStackTrace();
      }
    }

    return mapToJsonStr(eventContext);
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getContent(){
    JSObject jsonMap = new JSObject();
    jsonMap.putAll(this.activityData);
    return mapToJsonStr(jsonMap);
  }

  public void setContent(String contentJson) {
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);
        this.activityData.putAll(jsonMap);
      } catch (Exception e){
        logger.error("Unable to deserialize json content for ActivityLog object: " + e.getMessage(), e);
      }
    }
  }

  public String getSystemValue(String name) {
    if (FieldConstant.ReportSpace.equals(name))
      return reportSpace;
    if (FieldConstant.Channel.equals(name))
      return channelName;
    if (FieldConstant.GUID.equals(name))
      return getGUID();
    return null; 
  }
  
}
