package za.co.synthesis.regulatory.report.support;

import net.sf.jmimemagic.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public class FileUploadUtil {
  static final Logger logger = LoggerFactory.getLogger(FileUploadUtil.class);
  
  /**
   * Enumeration to specify which mimetype to trust or use for determining validity against a configured whitelist
   * The mimetypes are currently derived from the file content using a magic headers approach OR by the content type specified on the http post.
   * The whitelisting can pass with either passing, both having to pass OR either the magic headers method only or the supplied content type only.
   */
  public static enum MimeTypeWhiteListPass{
    Any("Any"),
    All("All"),
    MagicHeaderAndContentType("MagicHeaderAndContentType"),
    MagicHeaderOrContentTypeOnly("MagicHeaderOrContentType"),
    MagicHeaderOnly("MagicHeaderOnly"),
    ContentTypeOnly("ContentTypeOnly"),
    ExtensionOnly("ExtensionOnly");
  
    private final String text;
  
    /**
     * @param text
     */
    private MimeTypeWhiteListPass(final String text) {
      this.text = text;
    }
  
    @Override
    public String toString() {
      return text;
    }

    public static MimeTypeWhiteListPass fromString(String inputStr) {
      for (MimeTypeWhiteListPass enumVal : MimeTypeWhiteListPass.values()){
        if (enumVal.toString().equalsIgnoreCase(inputStr)) {
          return enumVal;
        }
      }
      return MimeTypeWhiteListPass.Any;
    }
  }
  
  public static MimeTypeWhiteListPass passWhiteListMechanism = MimeTypeWhiteListPass.Any;
  public static List<String> mimeTypeWhiteList = new ArrayList<>();
  public static List<String> extensionWhiteList = new ArrayList<>();
  //OWASP filename pattern recommendation: [a-zA-Z0-9]{1,200}\.[a-zA-Z0-9]{1,10}
  public static String filenamePattern = "(?<FILENAME>(?<PREFIX>[\\w\\-]{1,200})\\.(?<EXTENSION>[a-zA-Z0-9]{1,50}))";
  public static String filenameReplaceIllegalCharsPattern = "[^\\w\\-]";
  
  public static void setMimeTypeWhiteList(List<String> list){
    logger.info("Updating Content Type Whitelist...");
    if (list != null) {
      mimeTypeWhiteList = list;
    }
  }
  
  public static void setSafeExtensionWhiteList(List<String> list){
    logger.info("Updating Extension Whitelist (with normalized values): ");
    if (list != null) {
      extensionWhiteList.clear();
      String logout = "";
      for (String ext : list){
        String extS = replaceAllIllegalChars(ext);
        extensionWhiteList.add(extS);
        logout += (logout.length()>0?", ":"")+"('"+ext+"'=>'"+extS+"')";
      }
      logger.info(logout);
    }
  }
  
  public static void setExtensionWhiteList(List<String> list){
    if (list != null) {
      extensionWhiteList = list;
    }
  }
  
  public static boolean isExtensionWhiteListed(String filename) {
    boolean white = ((extensionWhiteList == null) || extensionWhiteList.isEmpty()) || extensionWhiteList.contains(getSafeFileExtension(filename));
    if (!white){
      logger.warn("Non-Whitelisted extension for provided filename: "+filename+" => ("+getSafeFileExtension(filename)+")");
    }
    return white;
  }

  public static String getContentMimeType(String  content){
    return getContentMimeType(content.getBytes());
  }

  public static String getContentMimeType(byte[]  content){
    String magicMime = null;
    try {
      Magic parser = new Magic();
      MagicMatch match = parser.getMagicMatch(content);
      magicMime = match.getMimeType();
      if (magicMime != null)
        logger.info("MultipartFile byte analysis reveals mime type of: " + magicMime);
    } catch (Exception e) {
      logger.info("Unable to get mime type from MultipartFile bytes: " + e.getMessage());
    }
    return magicMime;
  }
  
  public static boolean isMimeTypeWhiteListed(MultipartFile file){
    if (passWhiteListMechanism == MimeTypeWhiteListPass.ExtensionOnly) {
      //don't evaluate mime types - just fail the mime type evaluation for all and rely purely on the extension.
      return false;
    }
    
    if (file == null) {
      logger.debug("Null MultipartFile object provided for mime type whitelist checking.");
      return false;
    }
    
    if ((mimeTypeWhiteList == null) || mimeTypeWhiteList.isEmpty()) {
      //whitelisting currently disabled for mimetypes - allow whatever.
      logger.info("File upload mime and content type whitelisting disabled (no whitelist provided)");
      return true;
    }
    
    //Unfortunately, the java function to probe mime types is only file-based: Files.probeContentType(source);
    //But, JMimeMagic performs some magic header checks on byte[] and has only one dependency (slf4j)...
    String magicMime = null;
    if (passWhiteListMechanism != MimeTypeWhiteListPass.ContentTypeOnly) {
      try {
        Magic parser = new Magic();
        MagicMatch match = parser.getMagicMatch(file.getBytes());
        magicMime = match.getMimeType();
        if (magicMime != null)
          logger.info("MultipartFile byte analysis reveals mime type of: " + magicMime);
      } catch (Exception e) {
        logger.info("Unable to get mime type from MultipartFile bytes: " + e.getMessage());
      }
    } else {
      logger.info("Magic header mime type derivation not required - mechanism specified: '"+passWhiteListMechanism+"'");
    }
    
    String fileContentType = file.getContentType();
    if ((magicMime == null || magicMime.isEmpty()) && (fileContentType == null || fileContentType.isEmpty())) {
      //Unable to get file mime type - return true ONLY if the whitelist is completely empty (no stipulation on file types based on mime type)
      logger.info(((passWhiteListMechanism != MimeTypeWhiteListPass.ContentTypeOnly)?"Both magic header analysis and s":"S")+"upplied mime type : Indeterminate");
      logger.warn("...Whitelisting of mime types enabled - ambiguous file type not allowed.");
      return false;
    }
    
    if (fileContentType == null || fileContentType.isEmpty())
      fileContentType = magicMime;
    
    if (magicMime == null || magicMime.isEmpty())
      magicMime = fileContentType;
  
    boolean magicWhite = false;
    boolean suppliedWhite = false;
    if (!fileContentType.equalsIgnoreCase(magicMime)){
      logger.warn("Magic header analysis and supplied mime type have differing values: (" + magicMime + " vs " + fileContentType + ")");
    }
    if (passWhiteListMechanism != MimeTypeWhiteListPass.ContentTypeOnly) {
      if (mimeTypeWhiteList.contains(magicMime)) {
        logger.info("Magic header mime type is white listed: " + magicMime);
        magicWhite = true;
      }
    }
    if (mimeTypeWhiteList.contains(fileContentType)) {
      logger.warn("File content type is white listed: "+fileContentType);
      suppliedWhite = true;
    }
    
    boolean white = (
                (passWhiteListMechanism == MimeTypeWhiteListPass.ContentTypeOnly && suppliedWhite) ||
                (passWhiteListMechanism == MimeTypeWhiteListPass.MagicHeaderOnly && magicWhite) ||
                (passWhiteListMechanism == MimeTypeWhiteListPass.All && magicWhite && suppliedWhite) ||
                (magicWhite || suppliedWhite)
              );
    if (!white){
      logger.warn("Non-Whitelisted content type provided: "+file.getOriginalFilename());
    }
    return white;
  }
  
  public static boolean isContentTypeWhiteListed(String contentType) {
    boolean white = ((mimeTypeWhiteList == null) || mimeTypeWhiteList.isEmpty()) || mimeTypeWhiteList.contains(contentType);
    if (!white){
      logger.warn("Non-Whitelisted content type provided: "+contentType);
    }
    return white;
  }
  
  public static boolean isWhiteListed(MultipartFile file) {
    boolean isSafe = true;
    if (file != null) {
      try {
        boolean extensionSafe = isExtensionWhiteListed(file.getOriginalFilename());
        boolean mimeSafe = isMimeTypeWhiteListed(file);
        isSafe =  (((passWhiteListMechanism == MimeTypeWhiteListPass.ExtensionOnly || passWhiteListMechanism == MimeTypeWhiteListPass.Any) && extensionSafe) ||
            ((passWhiteListMechanism == MimeTypeWhiteListPass.All) && extensionSafe && mimeSafe) ||
            ((passWhiteListMechanism != MimeTypeWhiteListPass.All) && (passWhiteListMechanism != MimeTypeWhiteListPass.ExtensionOnly) && mimeSafe)
        );
      } catch (Exception e) {
        isSafe = false;
      }
      if (!isSafe) {
        logger.warn("Non-Whitelisted file provided with specified content type: '" + file.getContentType() + "' and name: '" + file.getOriginalFilename()+"' - failed whitelist validation using '"+passWhiteListMechanism+"' mechanism.");
      }
    } else {
      logger.warn("Null file provided for whitelist checking.");
    }
    return isSafe;
  }
  
  public static boolean isSafeAsIs(MultipartFile file){
    return isWhiteListed(file) && isNameSafe(file.getOriginalFilename());
  }
  
  public static boolean isNameSafe(String filename){
    boolean isSafe = false;
    if(filename != null) {
      isSafe = filename.matches(filenamePattern) && filename.length() <= 255;
      if (!isSafe) {
        logger.info("Unsafe filename of length (" + filename.length() + "): '" + filename + "'");
      }
    }
    return isSafe;
  }
  
  public static String makeNameSafe(String filename) throws Exception{
    if (!isNameSafe(filename)){
      logger.info("Unsafe filename normalization in progress...");
      int extIndex = filename.lastIndexOf(".");
      if (extIndex > 0 && extIndex < filename.length()-1) {
        //split into name and extension objects (only support filename with one of each : /[\w\-]+\.[\w\-]+/ )
        String ext = filename.substring(extIndex+1);
        String prefix = filename.substring(0, extIndex);
        //check for illegal chars and normalize
        prefix = replaceAllIllegalChars(prefix);
        logger.info("...normalization - convert characters for prefix: "+prefix);
        ext = replaceAllIllegalChars(ext);
        logger.info("...normalization - convert characters for extension: "+ext);
        //check for length issues (NTFS supports up to 255 chars)
        if (ext.length() + prefix.length() >= 255) {
          //check individual lengths...
          if (ext.length() > 254) {
            ext = ext.substring(0, Math.min(254, Math.max(1, 254 - prefix.length())));
            logger.info("...normalization - truncate extension: " + ext);
          }
          if (prefix.length() > 254) {
            ext = ext.substring(0, Math.min(254, Math.max(1, 254 - prefix.length())));
            logger.info("...normalization - truncate extension: " + ext);
          }
          //check combination lengths...
          if (ext.length() > prefix.length()){
            ext = ext.substring(0, Math.min(254, Math.max(1, 254 - prefix.length())));
            logger.info("...normalization - truncate extension for combined length: "+ext);
          } else {
            prefix = prefix.substring(0, Math.min(254, Math.max(1, 254 - ext.length())));
            logger.info("...normalization - truncate prefix for combined length: "+ext);
          }
        }
        filename = prefix + "." + ext;
        logger.info("...normalized filename: "+filename);
        if (isNameSafe(filename)) {
          logger.info("...normalization successful.");
          return filename;
        }
        logger.warn("...normalization failed - a 'safe' name could not be easily resolved from the one provided.");
      }
      throw new Exception("Filename invalid - cannot be made safe for persistence. Ensure that the filename has a single file type extension and contains only alpha-numeric characters, dashes (\"-\") and underscores (\"_\"). Other characters will be converted.");
    }
    return filename;
  }
  
  public static String getSafeFileExtension(String filename) {
    return replaceAllIllegalChars(getFileExtension(filename));
  }

  public static String getFileExtension(String filename){
    String ext = "";
    if (filename != null && !filename.isEmpty()) {
      int extIndex = filename.lastIndexOf(".");
      if (extIndex > 0 && extIndex < filename.length() - 1) {
        ext = filename.substring(extIndex + 1);
      }
    }
    return ext;
  }
  
  public static String replaceAllIllegalChars(String text){
    return text.replaceAll(filenameReplaceIllegalCharsPattern, "_");
  }
}
