package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;

/**
 * Created by james on 2017/09/05.
 */
@Entity
public class DecisionDownloadObject extends PersistenceObject {

  private static final Logger logger = LoggerFactory.getLogger(DecisionDownloadObject.class);

  private String reportSpace;
  private String reportKey;
  private String decisionGuid;

  public DecisionDownloadObject(String uuid) {
    super(uuid);
  }

  public DecisionDownloadObject(String uuid, String reportSpace, String reportKey, String decisionGuid) {
    super(uuid);
    this.reportSpace = reportSpace;
    this.reportKey = reportKey;
    this.decisionGuid = decisionGuid;
  }

  public DecisionDownloadObject(String uuid, DecisionObject decisionObject) {
    super(uuid);
    this.reportSpace = decisionObject.getReportSpace();
    this.reportKey = decisionObject.getReportKey();
    this.decisionGuid = decisionObject.getUuid();
  }


  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public String getDecisionGuid() {
    return decisionGuid;
  }

  public void setDecisionGuid(String decisionGuid) {
    this.decisionGuid = decisionGuid;
  }
}
