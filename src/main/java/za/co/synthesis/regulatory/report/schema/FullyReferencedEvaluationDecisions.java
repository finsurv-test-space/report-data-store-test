package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/13/17.
 */
@JsonPropertyOrder({
        //"ReportSpace", /*TODO: ReportSpace should supercede the ReportingSpace field below*/
        "ReportingSpace" /*THIS NAMING CONVENTION IS BEING PHASED OUT/REMOVED*/
        , "TrnReference"
        , "Decisions"
})
@Entity
public class FullyReferencedEvaluationDecisions  {
  private final String reportSpace;
  private final String trnReference;
  private List<EvaluationDecision> decisions = new ArrayList<EvaluationDecision>();

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference, EvaluationDecision decision) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference, List<EvaluationDecision> decisions) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    for (EvaluationDecision decision : decisions) {
      this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
    }
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference, EvaluationDecisions decisions) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    for (EvaluationResponse response : decisions.getEvaluations()) {
      this.decisions.add(new EvaluationDecision(decisions.getParameters(), response));
    }
  }

  public FullyReferencedEvaluationDecisions(FullyReferencedEvaluationDecision decision) {
    this.reportSpace = decision.getReportSpace();
    this.trnReference = decision.getTrnReference();
    this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
  }

  public FullyReferencedEvaluationDecisions(List<FullyReferencedEvaluationDecision> decisions) {
    String rep = null;
    String trn = null;
    for (FullyReferencedEvaluationDecision decision : decisions) {
      if (rep == null) {
        rep = decision.getReportSpace();
      }
      if (trn == null) {
        trn = decision.getTrnReference();
      }
      this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
    }
    this.reportSpace = rep;
    this.trnReference = trn;
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, TrnReferencedEvaluationDecision decision) {
    this.reportSpace = reportSpace;
    this.trnReference = decision.getTrnReference();
    this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, List<TrnReferencedEvaluationDecision> decisions) {
    this.reportSpace = reportSpace;
    String trn = null;
    for (TrnReferencedEvaluationDecision decision : decisions) {
      if (trn == null) {
        trn = decision.getTrnReference();
      }
      this.decisions.add(new EvaluationDecision(decision.getParameters(), decision.getEvaluation()));
    }
    this.trnReference = trn;
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference, EvaluationRequest parameters, EvaluationResponse evaluation) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.decisions.add(new EvaluationDecision(parameters, evaluation));
  }

  public FullyReferencedEvaluationDecisions(String reportSpace, String trnReference, EvaluationRequest parameters, List<EvaluationResponse> evaluations) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    for (EvaluationResponse evaluation : evaluations){
      this.decisions.add(new EvaluationDecision(parameters, evaluation));
    }
  }

  //@JsonProperty("ReportSpace")
  @JsonIgnore //TODO: This shouldn't be ignored!
  public String getReportSpace() {
    return reportSpace;
  }

  @Deprecated
  @JsonProperty("ReportingSpace")
  public String getReportingSpace() {
    return reportSpace;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }
  
  public List<EvaluationResponse> getEvaluationResponses() {
    ArrayList<EvaluationResponse> responses = new ArrayList<>();
    for (EvaluationDecision evalDec : decisions){
      responses.add(evalDec.getEvaluation());
    }
    return responses;
  }


  @JsonProperty("Decisions")
  public List<EvaluationDecision> getEvaluations() {
    return this.decisions;
  }
  
  public EvaluationRequest getParameters() {
    if (decisions.size() > 0) {
      return decisions.get(0).getParameters();
    }
    return null;
  }
  
  public void setEvaluations(List<EvaluationDecision> evaluationDecisions) {
    this.decisions = evaluationDecisions;
  }
}
