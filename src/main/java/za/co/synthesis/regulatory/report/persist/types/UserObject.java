package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.UserData;
import za.co.synthesis.regulatory.report.persist.impl.ListAndUserDataJSONHelper;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/09/26.
 */

@Entity
public class UserObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(UserObject.class);

  private String username;
  private String password;
  private Boolean active;
  @ManyToMany(fetch= FetchType.EAGER)
  private Map<String, Object> details;
  @ManyToMany(fetch= FetchType.EAGER)
  private List<String> rights;
  @ManyToMany(fetch=FetchType.EAGER)
  private List<String> access;
  @ManyToMany(fetch=FetchType.EAGER)
  private List<String> channels;
  @ManyToMany(fetch=FetchType.EAGER)
  private List<String> userEvents;
  @ManyToMany(fetch=FetchType.EAGER)
  private List<String> reportLists;

  private Map<String, Object> eventContext;
  private Integer version = 1;

  public UserObject(String uuid) {
    super(uuid);
  }

  public UserObject(String uuid, UserData userData) {
    super(uuid);
    this.username = userData.getUsername();
    this.password = userData.getPassword();
    this.rights = userData.getRights();
    this.access = userData.getAccess();
    this.channels = userData.getChannels();
    this.active = userData.getActive();
    this.details = userData.getDetails();
    this.userEvents = userData.getUserEvents();
    this.reportLists = userData.getReportLists();
  }

  public UserObject(String uuid, String username, String password, List<String> rights, List<String> access, List<String> channels, Boolean active, Map<String, Object> details, List<String> reportLists, List<String> userEvents) {
    super(uuid);
    this.username = username;
    this.password = password;
    this.rights = rights;
    this.access = access;
    this.channels = channels;
    this.active = active;
    this.details = details;
    this.reportLists = reportLists;
    this.userEvents = userEvents;
  }

  public UserData getUserData(){
    UserData userData = new UserData();
    userData.setUsername(this.username);
    userData.setPassword(this.password);
    userData.setRights(this.rights);
    userData.setAccess(this.access);
    userData.setChannels(this.channels);
    userData.setActive(this.active);
    userData.setDetails(this.details);
    userData.setReportLists(this.reportLists);
    userData.setUserEvents(this.userEvents);
    return userData;
  }

  public String getAuthToken() {
    if (eventContext == null)
      eventContext = composeEventContext();
    if (eventContext != null)
      return mapToJsonStr(eventContext);
    else
      return null;
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getContent() {
    JSObject jsonMap = new JSObject();
    jsonMap.put("username", username);
    jsonMap.put("password", password);
    jsonMap.put("rights", rights);
    jsonMap.put("access", access);
    jsonMap.put("channels", channels);
    jsonMap.put("active", active);
    jsonMap.put("details", details);
    jsonMap.put("reportLists",reportLists);
    jsonMap.put("userEvents",userEvents);
    return mapToJsonStr(jsonMap);
  }

  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);
        if (version == 1) {
          username = (String) jsonMap.get("username");
          password = (String) jsonMap.get("password");
          rights = ListAndUserDataJSONHelper.getListOfStringsFromProperty(jsonMap,"rights");
          access = ListAndUserDataJSONHelper.getListOfStringsFromProperty(jsonMap,"access");
          channels = ListAndUserDataJSONHelper.getListOfStringsFromProperty(jsonMap,"channels");
          active = (Boolean) (jsonMap.containsKey("active") ? jsonMap.get("active") : true);
          details = (Map<String,Object>) jsonMap.get("details");
          reportLists = ListAndUserDataJSONHelper.getListOfStringsFromProperty(jsonMap,"reportLists");
          userEvents = ListAndUserDataJSONHelper.getListOfStringsFromProperty(jsonMap,"userEvents");
        }
      } catch (Exception e){
        logger.error("Unable to deserialize json content for User object: " + e.getMessage(), e);
      }
    }
  }

  public String getUsername() {
    return username;
  }

  public String getKey() {
    return username;
  }

  public void setKey(String key) {
    this.username = key;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Map<String, Object> getDetails() {
    return details;
  }

  public void setDetails(Map<String, Object> details) {
    this.details = details;
  }

  public List<String> getRights() {
    return rights;
  }

  public void setRights(List<String> rights) {
    this.rights = rights;
  }

  public List<String> getAccess() {
    return access;
  }

  public void setAccess(List<String> access) {
    this.access = access;
  }

  public List<String> getChannels() {
    return channels;
  }

  public void setChannels(List<String> channels) {
    this.channels = channels;
  }

  public List<String> getReportLists() {
    return reportLists;
  }

  public void setReportLists(List<String> reportLists) {
    this.reportLists = reportLists;
  }

  public List<String> getUserEvents() {
    return userEvents;
  }

  public void setUserEvents(List<String> userEvents) {
    this.userEvents = userEvents;
  }
}
