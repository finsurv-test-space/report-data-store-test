package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.schema.ReportDownloadType;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.schema.ValidationResponse;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */

@Entity
public class ReportObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(ReportObject.class);

  private String reportSpace;
  private String reportKey;

  //  private final IDataStore.ReportInfo reportData;
  private String state;
  private String channelName;
  private String schema;
  //  private final ReportData reportData;
  @ManyToMany(fetch= FetchType.EAGER)
  private Map<String, Object> meta;
  @ManyToMany(fetch=FetchType.EAGER)
  private Map<String, Object> report;
  private List<Map<String, Object>> validationResponses;
  private Map<String, Object> eventContext;
  private Integer version = 1;
  private ReportDownloadType reportDownloadType = ReportDownloadType.Update;
  


  public ReportObject(String uuid) {
    super(uuid);
    try{
      ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
      if (user != null) {
        this.meta.put("User", user.getName());
        if (this.eventContext == null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      }
    } catch (Exception e){}
  }

  public ReportObject(String uuid, ReportInfo reportData) {
    super(uuid);
    if (reportData != null) {
      this.reportSpace = reportData.getReportSpace();
      this.channelName = reportData.getChannelName();
      this.state = reportData.getState();
      this.schema = reportData.getSchema();
      this.reportDownloadType = reportData.getDownloadType();

      if (reportData.getReportData() != null) {
        this.reportKey = reportData.getReportData().getTrnReference();
        this.meta = reportData.getReportData().getMeta();
        this.report = reportData.getReportData().getReport();
      }
      this.validationResponses = SerializationTools.serializeValidations(reportData.getValidations());

      if (reportData.getEventContext() == null) {
        try {
          //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
          ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
          if (user != null) {
            this.eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
          }
        } catch (Exception e) {
        }
      } else {
        this.eventContext = SerializationTools.serializeEventContext(reportData.getEventContext());
      }
    }
  }

  public ReportInfo getReportData() throws Exception {
    ReportData reportContent = new ReportData(this.meta, this.report);
    List< ValidationResponse > validations = SerializationTools.deserializeValidations(this.validationResponses);
    ReportInfo reportData = new ReportInfo(
            this.state,
            this.schema,
            this.reportSpace,
            this.channelName,
            reportContent,
            validations,
            eventContext != null ? SerializationTools.deserializeEventContext(eventContext) : null,
            this.reportDownloadType
        );
    return reportData;
  }

  public String getReportSpace(){
    return reportSpace;
  }

  public void setReportSpace(String reportSpace){
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public List<ValidationResponse> getValidations() {
    return validationResponses != null ? SerializationTools.deserializeValidations(validationResponses) : null;
  }

  public ReportEventContext getEventContext() {
    return eventContext != null ? SerializationTools.deserializeEventContext(eventContext) : null;
  }

  public String getAuthToken() {

    if (this.eventContext == null) {
      try {
        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases, seems to be a spring thing.
        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
        if (user != null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      } catch (Exception e) {
        //we seem to be running into null pointers where we really shouldn't be.
        logger.warn("Unable to persist the ReportObject eventContext / Audit detail : " + e.getMessage());
        e.printStackTrace();
      }
    }

    return mapToJsonStr(eventContext);
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }

  public String getSchema() {
    return schema;
  }

  public void setSchema(String schema) {
    this.schema = schema;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getContent(){
    JSObject jsonMap = new JSObject();
    //Add the user to the meta section on persisting...
    if (this.meta != null) {
//      try {
//        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
//        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
//        if (user != null) {
//          this.meta.put("User", user.getName());
//        }
//      } catch(Exception e){
//        e.printStackTrace();
//      }
      jsonMap.put(FieldConstant.Meta, this.meta);
    }
    jsonMap.put(FieldConstant.Report, this.report);
    jsonMap.put(FieldConstant.State, this.state);
//    jsonMap.put(FieldConstant.Schema, this.schema);
    jsonMap.put(FieldConstant.Channel, this.channelName);
    if (this.validationResponses != null)
      jsonMap.put("ValidationResponses", this.validationResponses);
    return mapToJsonStr(jsonMap);
  }

  public void setContent(String contentJson) {
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);
        meta = (Map) jsonMap.get(FieldConstant.Meta);
        report = (Map) jsonMap.get(FieldConstant.Report);
        state = (String) jsonMap.get(FieldConstant.State);
//        schema = (String) jsonMap.get(FieldConstant.Schema);
        channelName = (String) jsonMap.get(FieldConstant.Channel);
        Object obj = jsonMap.get("ValidationResponses");
        if (obj instanceof List) {
          validationResponses = (List<Map<String, Object>>)obj;
        }
      } catch (Exception e){
        logger.error("Unable to deserialize json content for Report object: " + e.getMessage(), e);
      }
    }
  }

  public String getSystemValue(String name) {
    if (FieldConstant.State.equals(name))
      return state;
    if (FieldConstant.ReportSpace.equals(name))
      return reportSpace;
    if (FieldConstant.Schema.equals(name))
      return schema;
    if (FieldConstant.Channel.equals(name))
      return channelName;
    if (FieldConstant.GUID.equals(name))
      return getGUID();
    return null; 
  }
  
  public ReportDownloadType getReportDownloadType() {
    return reportDownloadType;
  }
  
  public void setReportDownloadType(ReportDownloadType reportDownloadType) {
    this.reportDownloadType = reportDownloadType;
  }
}
