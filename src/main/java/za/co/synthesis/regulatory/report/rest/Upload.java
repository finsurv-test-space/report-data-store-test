package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.AccountEntryData;
import za.co.synthesis.regulatory.report.persist.AccountEntryResponse;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.regulatory.report.validate.EvaluationUploadSet;
import za.co.synthesis.regulatory.report.validate.ValidationUploadSet;
import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.core.impl.EvaluationDecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import static za.co.synthesis.regulatory.report.security.SecurityHelper.getAuthMeta;

/**
 * Created by jake on 5/19/17.
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/upload")
public class Upload extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Upload.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  ReportServiceExecutorService reportServiceExecutorService;

  private Map<String, ValidationUploadSet> mapUploadSet = new ConcurrentHashMap<String, ValidationUploadSet>();

  public static Map<String, List<ReportInfo>> bulkReportList = new HashMap<>();

  @Autowired
  ApplicationContext ctx;

  @Autowired
  ValidationEngineFactory validationEngineFactory;

  @Autowired
  EvaluationEngineFactory evaluationEngineFactory;

  public synchronized Validator getValidator(String packageName) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getValidator()");
    return validationEngineFactory.getEngineForCurrentDate(packageName).issueValidator();
  }



  public List<ValidationResult> uploadBulkReportsSynch(String schema,
                                                       String channelName,
                                                       List<ReportData> reports) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsSynch()");
    long startTime = System.currentTimeMillis();
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if ((schema == null || schema.equalsIgnoreCase("default")) && (channel.getDefaultSchema() != null)){
      schema = channel.getDefaultSchema();
    }
    ValidationUploadSet uploadSet =
            new ValidationUploadSet(reportServiceExecutorService.getExecutorService(), schema, channel, dataLogic, SecurityHelper.getAuthMeta());
    Validator reportValidator = null;
    try {
      reportValidator = getValidator(channelName);
    } catch (Exception err){
      logger.error("Unable to get a validator for "+channelName, err);
    }
    if (reportValidator!=null) {
      for (ReportData report : reports) {
        uploadSet.addValidation(reportValidator, report);
      }
    }
    uploadSet.closeSet(true);
    List<ValidationResult> resultset = uploadSet.getUploadResultList();
    logger.info("Upload.uploadBulkReportsSynch => "+(System.currentTimeMillis() - startTime) + "ms");
    return resultset;
  }
  
  
  /**
   * Synchronous bulk upload end-point for submitting report data (in the schema format specified).
   * The bulk validation results for the reports uploaded will be returned when complete.
   * @param schema The report schema format used
   * @param channelName The channel package containing the relevant server-side validation rules that should be run and related reporting space in which to store the uploaded reports
   * @param reports The bulk list of report objects to upload
   * @return <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @throws Exception
   */
  @RequestMapping(value = "/reports/sync/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  List<ValidationResult> uploadBulkReportsSynchSchema(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody List<ReportData> reports) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsSynchSchema()");
    return uploadBulkReportsSynch(schema, channelName, reports);
  }


  /**
   * Synchronous bulk upload end-point for submitting report data (in the schema format specified), wrapped in a JSON object notation for support for Legacy systems unable to work with JSON Arrays as primary objects.
   * The bulk validation results for the reports uploaded will be returned when complete.
   * @param schema The report schema format used
   * @param channelName The channel package containing the relevant server-side validation rules that should be run and related reporting space in which to store the uploaded reports
   * @param reportsObj The object containing the bulk list of report objects to upload
   * @return <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @throws Exception
   */
  @RequestMapping(value = "/reports/sync/data/obj/{schema}", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  List<ValidationResult> uploadBulkReportsObjSynchSchema(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody BulkReportData reportsObj) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsObjSynchSchema()");
    return uploadBulkReportsSynch(schema, channelName, reportsObj.getReports());
  }


//  @Deprecated
//  @RequestMapping(value = "/reports/sync/sadc", method = RequestMethod.POST, produces = "application/json")
//  public
//  @ResponseBody
//  List<ValidationResult> uploadBulkReportsSynchSadc(
//          @RequestParam String channelName,
//          @RequestBody List<ReportData> reports) throws Exception {
//    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsSynchSadc()");
//    return uploadBulkReportsSynch("sadc", channelName, reports);
//  }
//
//
//  @Deprecated
//  @RequestMapping(value = "/reports/sync/sarb", method = RequestMethod.POST, produces = "application/json")
//  public
//  @ResponseBody
//  List<ValidationResult> uploadBulkReportsSynchSarb(
//          @RequestParam String channelName,
//          @RequestBody List<ReportData> reports) throws Exception {
//    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsSynchSarb()");
//    return uploadBulkReportsSynch("sarb", channelName, reports);
//  }


  public UploadReference uploadBulkReportsAsync(String schema,
                                                String channelName,
                                                List<ReportData> reports) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsAsync()");
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if ((schema == null || schema.equalsIgnoreCase("default")) && (channel.getDefaultSchema() != null)){
      schema = channel.getDefaultSchema();
    }
    ValidationUploadSet uploadSet =
            new ValidationUploadSet(reportServiceExecutorService.getExecutorService(), schema, channel, dataLogic, SecurityHelper.getAuthMeta());
    Validator reportValidator = null;
    try {
      reportValidator = getValidator(channelName);
      if (reportValidator==null){
        throw new Exception ("Unable to acquire a validator ("+channelName+")");
      }
    } catch (Exception err){
      logger.error("Unable to get a validator for "+channelName, err);
    }
    for (ReportData report : reports) {
      uploadSet.addValidation(reportValidator, report);
    }
    uploadSet.closeSet();
    mapUploadSet.put(uploadSet.getUploadReference(), uploadSet);

    return new UploadReference(uploadSet.getUploadReference());
  }
  
  
  /**
   * Asynchronous bulk upload end-point for submitting report data (in the schema format specified).
   * The bulk validation results for the reports uploaded will be run asynchronously in the background on the server.
   * A reference token will be provided as a response, which can be used to check progress of server-side validations and storage and also retrieve validation results for the report uploaded
   * @param schema The report schema format used
   * @param channelName The channel package containing the relevant server-side validation rules that should be run and related reporting space in which to store the uploaded reports
   * @param reports The bulk list of report objects to upload
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/reports/async/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  UploadReference uploadBulkReportsAsyncSchema(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody List<ReportData> reports) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsAsyncSchema()");
    return uploadBulkReportsAsync(schema, channelName, reports);
  }


  @RequestMapping(value = "/reports/async/data/obj/{schema}", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  UploadReference uploadBulkReportsObjAsyncSchema(
          @PathVariable String schema,
          @RequestParam String channelName,
          @RequestBody BulkReportData reportsObj) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsObjAsyncSchema()");
    return uploadBulkReportsAsync(schema, channelName, reportsObj.getReports());
  }

  @RequestMapping(value = "/accountEntries/sync/data/", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  List<AccountEntryResponse> uploadBulkAccountEntries(
          @RequestParam String channelName,
          @RequestBody BulkAccountEntryData accountEntries) throws Exception {
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }

    List<AccountEntryResponse> accountEntryResponses = new ArrayList<>();
    for (AccountEntryData accountEntryData : accountEntries.getAccountEntries()) {
      try {
        dataLogic.setAccountEntry(channel.getReportSpace(), accountEntryData.getTrnReference(), accountEntryData, dataLogic.getReportEventContext(getAuthMeta(systemInformation.getConfigStore())));
        accountEntryResponses.add(new AccountEntryResponse(accountEntryData.getTrnReference(),"Success",null,accountEntryData));
      }catch (Exception e){
        accountEntryResponses.add(new AccountEntryResponse(accountEntryData.getTrnReference(),"Failure",e.getMessage(),accountEntryData));
      }
    }
    return accountEntryResponses;
  }


//  @Deprecated
//  @RequestMapping(value = "/reports/async/sadc", method = RequestMethod.POST, produces = "application/json")
//  public
//  @ResponseBody
//  UploadReference uploadBulkReportsAsyncSadc(
//          @RequestParam String channelName,
//          @RequestBody List<ReportData> reports) throws Exception {
//    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsAsyncSadc()");
//    return uploadBulkReportsAsync("sadc", channelName, reports);
//  }
//
//
//  @Deprecated
//  @RequestMapping(value = "/reports/async/sarb", method = RequestMethod.POST, produces = "application/json")
//  public
//  @ResponseBody
//  UploadReference uploadBulkReportsAsyncSarb(
//          @RequestParam String channelName,
//          @RequestBody List<ReportData> reports) throws Exception {
//    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkReportsAsyncSarb()");
//    return uploadBulkReportsAsync("sarb", channelName, reports);
//  }
  
  
  /**
   * Asynchronous bulk end-point for checking the status of an asynchronous bulk upload (for which a reference was returned).
   * @param uploadReference The response reference of the asynchronous bulk upload that was initiated
   * @return
   */
  @RequestMapping(value = "/reports/async/status", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  UploadStatus getBulkReportUploadStatus(
          @RequestParam String uploadReference) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getBulkReportUploadStatus()");
    if (mapUploadSet.containsKey(uploadReference)) {
      return mapUploadSet.get(uploadReference).getUploadStatus();
    }
    else {
      throw new ResourceNotFoundException("Cannot find upload reference '" + uploadReference + "'");
    }
  }
  
  /**
   * Asynchronous bulk end-point for retrieving the validation results of an asynchronous bulk upload (for which a reference was returned).
   * @param uploadReference The response reference of the asynchronous bulk upload that was initiated
   * @return <br><b>PLEASE NOTE:</b> all parameter and field instances are being aligned to a common naming convention, as such "<b>reportingSpace</b>" is being deprecated and is now superceded by "<b>reportSpace</b>". Where necessary, systems making use of this API should move to the newly provided "reportSpace" field for all usages.
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @RequestMapping(value = "/reports/async/result", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<ValidationResult> getBulkReportUploadResults(
          @RequestParam String uploadReference) throws ExecutionException, InterruptedException, ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getBulkReportUploadResults()");

    if (mapUploadSet.containsKey(uploadReference)) {
      List<ValidationResult> result = mapUploadSet.get(uploadReference).getUploadResultList();
      if (result == null) {
        throw new PreconditionFailedException("Still validating upload '" + uploadReference + "'");
      }
      return result;
    }
    else {
      throw new ResourceNotFoundException("Cannot find upload reference '" + uploadReference + "'");
    }
  }
  
  /**
   * Bulk upload for inserting evaluation decisions into the system
   * @param channelName The channel package containing the relevant reporting space into which the decision objects should be stored.
   * @param decisions The list of decision objects to be uploaded into the system
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @RequestMapping(value = "/decisions", method = RequestMethod.POST, produces = "application/json")
  public void uploadBulkDecisions(
          @RequestParam String channelName,
          @RequestBody List<TrnReferencedEvaluationDecision> decisions) throws ExecutionException, InterruptedException, ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkDecisions()");
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    for (TrnReferencedEvaluationDecision decision : decisions) {
      dataLogic.setEvaluationDecision(new FullyReferencedEvaluationDecision(channel.getReportSpace(), decision));
    }
  }



  /**
   * Bulk upload for inserting evaluation decisions into the system
   * @param channelName The channel package containing the relevant reporting space into which the decision objects should be stored.
   * @param evaluationRequests The list of evaluation request objects to be uploaded for processing by the system
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @RequestMapping(value = "/evaluation/sync", method = RequestMethod.POST, produces = "application/json")
  public
  @ResponseBody
  List<EvaluationDecisions> uploadBulkEvaluationsSynch(
      @RequestParam String channelName,
      @RequestBody List<EvaluationRequest> evaluationRequests,
      @RequestParam (required=false) Boolean mostRelevantResultsOnly) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkEvaluationsSynch()");
    mostRelevantResultsOnly = mostRelevantResultsOnly==null?false:mostRelevantResultsOnly;
    long startTime = System.currentTimeMillis();
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }

    for (EvaluationRequest evalReq : evaluationRequests){
      if (evalReq.getChannelName() == null){
        evalReq.setChannelName(channelName);
      }
      if (!evalReq.getChannelName().equals(channelName)){
        throw new PreconditionFailedException("Multiple channels not supported for a single batch evaluation upload");
      }
    }

    EvaluationUploadSet uploadSet =
        new EvaluationUploadSet(reportServiceExecutorService.getExecutorService(), channel, dataLogic, SecurityHelper.getAuthMeta());
    Evaluator evaluator = null;
    try {
      evaluator = Decision.getEvaluator(systemInformation, channelName, evaluationEngineFactory, logger);
    } catch (Exception err){
      logger.error("Unable to get a validator for "+channelName, err);
    }
    if (evaluator!=null) {
      for (EvaluationRequest evaluationRequest : evaluationRequests) {
        if (evaluationRequest.getTrnReference() == null || evaluationRequest.getTrnReference().isEmpty()){
          throw new PreconditionFailedException("TrnReference field not populated.");
        }
        uploadSet.addDecision(evaluator, evaluationRequest.getTrnReference(), evaluationRequest, mostRelevantResultsOnly);
      }
    }
    uploadSet.closeSet(true);
    List<EvaluationDecisions> resultset = uploadSet.getUploadResultList();
    logger.info("Upload.EvaluationSynch => "+(System.currentTimeMillis() - startTime) + "ms");
    return resultset;
  }






  public static EvaluationDecisions evaluateAnyScenario(Evaluator evaluator, EvaluationRequest evaluationRequest, Boolean relevantResultsOnly) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "evaluateAnyScenario()");

    int scenarioCount = (evaluationRequest.getEvaluationCard() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCorrespondentBank() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationBankPayment() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationBankReceipt() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerOnUs() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerPayment() != null ? 1 : 0) +
        (evaluationRequest.getEvaluationCustomerReceipt() != null ? 1 : 0);

    if (scenarioCount > 1) {
      throw new PreconditionFailedException("Too many evaluation scenarios have been specified");
    }
    if (scenarioCount == 0) {
      throw new PreconditionFailedException("No evaluation scenarios have been specified");
    }

    List<IEvaluationDecision> decisions = new ArrayList<>();
    if (evaluationRequest.getEvaluationCard() != null){
      EvaluationRequest.EvaluationCard card = evaluationRequest.getEvaluationCard();
      IEvaluationDecision decision = evaluator.evaluateCARD(
          card.getIssuingCountry(),
          card.getUseCountry()
      );
      decisions.add(decision);
    } else if (evaluationRequest.getEvaluationCorrespondentBank() != null){
      EvaluationRequest.EvaluationCorrespondentBank correspondentBank = evaluationRequest.getEvaluationCorrespondentBank();
      IEvaluationDecision decision = evaluator.evaluateCorrespondentBank(
          correspondentBank.getBeneficiaryInstitutionBIC(),
          correspondentBank.getOrderingInstitutionBIC(),
          correspondentBank.getOrderingCustomer()
      );
      decisions.add(decision);
    } else if (evaluationRequest.getEvaluationBankPayment() != null){
      EvaluationRequest.EvaluationBankPayment bankPayment = evaluationRequest.getEvaluationBankPayment();
      IEvaluationDecision decision = evaluator.evaluateBankPayment(
          bankPayment.getBeneficiaryBIC(),
          bankPayment.getBeneficiaryCurrency()
      );
      decisions.add(decision);
    } else if (evaluationRequest.getEvaluationBankReceipt() != null){
      EvaluationRequest.EvaluationBankReceipt bankReceipt = evaluationRequest.getEvaluationBankReceipt();
      IEvaluationDecision decision = evaluator.evaluateBankReceipt(
          bankReceipt.getOrderingBIC(),
          bankReceipt.getOrderingCurrency()
      );
      decisions.add(decision);
    } else if (evaluationRequest.getEvaluationCustomerOnUs() != null){
      EvaluationRequest.EvaluationCustomerOnUs customerOnUsRequest = evaluationRequest.getEvaluationCustomerOnUs();
      if (relevantResultsOnly) {
        decisions = evaluator.evaluateCustomerOnUsFiltered(
            customerOnUsRequest.getDrResStatus(),
            customerOnUsRequest.getDrAccType(),
            customerOnUsRequest.getDrAccountHolderStatus(),
            customerOnUsRequest.getCrResStatus(),
            customerOnUsRequest.getCrAccType(),
            customerOnUsRequest.getCrAccountHolderStatus()
        );
      } else {
        decisions = evaluator.evaluateCustomerOnUs(
            customerOnUsRequest.getDrResStatus(),
            customerOnUsRequest.getDrAccType(),
            customerOnUsRequest.getDrAccountHolderStatus(),
            customerOnUsRequest.getCrResStatus(),
            customerOnUsRequest.getCrAccType(),
            customerOnUsRequest.getCrAccountHolderStatus()
        );
      }
    } else if (evaluationRequest.getEvaluationCustomerPayment() != null){
      EvaluationRequest.EvaluationCustomerPayment customerPayment = evaluationRequest.getEvaluationCustomerPayment();
      if (relevantResultsOnly) {
        IEvaluationDecision decision = evaluator.evaluateCustomerPayment(
            customerPayment.getDrResStatus(),
            customerPayment.getDrAccType(),
            customerPayment.getBeneficiaryBIC(),
            customerPayment.getBeneficiaryCurrency(),
            customerPayment.getDrAccountHolderStatus()
        );
        if (decision != null) {
          decisions.add(decision);
        }
      } else {
        List<IEvaluationDecision> decision = evaluator.evaluateCustomerPaymentEx(
            customerPayment.getDrResStatus(),
            customerPayment.getDrAccType(),
            customerPayment.getBeneficiaryBIC(),
            customerPayment.getBeneficiaryCurrency(),
            customerPayment.getDrAccountHolderStatus()
        );
        if (decision!= null) {
          decisions.addAll(decision);
        }
      }
    } else if (evaluationRequest.getEvaluationCustomerReceipt() != null){
      EvaluationRequest.EvaluationCustomerReceipt customerReceipt = evaluationRequest.getEvaluationCustomerReceipt();
      if (relevantResultsOnly) {
        IEvaluationDecision decision = evaluator.evaluateCustomerReceipt(
            customerReceipt.getCrResStatus(),
            customerReceipt.getCrAccType(),
            customerReceipt.getOrderingBIC(),
            customerReceipt.getOrderingCurrency(),
            customerReceipt.getCrAccountHolderStatus(),
            customerReceipt.getField72()
        );
        if (decision != null) {
          decisions.add(decision);
        }
      } else {
        List<IEvaluationDecision> decision = evaluator.evaluateCustomerReceiptEx(
            customerReceipt.getCrResStatus(),
            customerReceipt.getCrAccType(),
            customerReceipt.getOrderingBIC(),
            customerReceipt.getOrderingCurrency(),
            customerReceipt.getCrAccountHolderStatus(),
            customerReceipt.getField72()
        );
        if (decision != null) {
          decisions.addAll(decision);
        }
      }
    }

    List<EvaluationResponse> responses = new ArrayList<EvaluationResponse>();
    if (decisions.size() > 0) {
      for (IEvaluationDecision decision : decisions) {
        responses.add(new EvaluationResponse(decision));
      }
    } else {
      responses.add(new EvaluationResponse(new EvaluationDecision(null, ReportableType.Unknown)));
    }
    EvaluationDecisions evalDecisions = new EvaluationDecisions(evaluationRequest, responses);
    return evalDecisions;
  }





  /**
   * Bulk upload for actions
   * @param channelName The channel package containing the relevant reporting space into which the decision objects should be stored.
   * @param decisions The list of decision objects to be uploaded into the system
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @RequestMapping(value = "/reports/sync/actions", method = RequestMethod.POST, produces = "application/json")
  public void uploadBulkActions(
      @RequestParam String channelName,
      @RequestBody List<TrnReferencedEvaluationDecision> decisions) throws ExecutionException, InterruptedException, ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "uploadBulkActions()");
    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    for (TrnReferencedEvaluationDecision decision : decisions) {
      dataLogic.setEvaluationDecision(new FullyReferencedEvaluationDecision(channel.getReportSpace(), decision));
    }
  }



}
