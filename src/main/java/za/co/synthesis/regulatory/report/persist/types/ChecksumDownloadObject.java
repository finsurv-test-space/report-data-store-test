package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;

/**
 * Created by james on 2017/09/05.
 */
@Entity
public class ChecksumDownloadObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(ChecksumDownloadObject.class);

  private String reportSpace;
  private String valueDate;

  public ChecksumDownloadObject(String uuid) {
    super(uuid);
  }

  public ChecksumDownloadObject(String uuid, String reportSpace, String valueDate) {
    super(uuid);
    this.reportSpace = reportSpace;
    this.valueDate = valueDate;
  }

  public ChecksumDownloadObject(String uuid, ChecksumObject documentObject) {
    super(uuid);
    this.reportSpace = documentObject.getReportSpace();
    this.valueDate = documentObject.getValueDate();
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getValueDate() {
    return valueDate;
  }

  public void setValueDate(String valueDate) {
    this.valueDate = valueDate;
  }

}
