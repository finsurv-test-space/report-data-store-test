package za.co.synthesis.regulatory.report.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.ValidationData;
import za.co.synthesis.regulatory.report.schema.ValidateResponse;
import za.co.synthesis.regulatory.report.support.JdbcConnectionManager;
import za.co.synthesis.regulatory.report.support.PackageCache;
import za.co.synthesis.regulatory.report.support.ReportServiceExecutorService;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.StatusType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * This class is responsible for launching validation tasks and for monitoring task progress.
 * Decisions that determine when tasks should be run (e.g. if database connection is down) are made by this class
 */
public class ValidationRunner {
  private static final Logger logger = LoggerFactory.getLogger(ValidationRunner.class);

  private IConfigStore configStore;
  private RulesCache rulesCache;
  private ReportServiceExecutorService reportServiceExecutorService;
  private JdbcConnectionManager jdbcConnectionManager;
  private long timeOutMillis = 10000;
  private String timedOutCode = "TOT";
  private String timedOutMessage = "Timed out waiting for response";
  private String noEndpointCode = "NOP";
  private String noEndpointMessage = "No external validation has been defined for this call";
  private int maxRunners = 60;
  private String maxedRunnerCode = "MVT";
  private String maxedRunnerMessage = "There are too many timed out validations running to initiate another validation task";

  private final Map<String, List<Future<CustomValidateResult>>> runningTasks = new HashMap<>();

  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  @Required
  public void setRulesCache(RulesCache rulesCache) {
    this.rulesCache = rulesCache;
  }

  public SystemInformation getSystemInformation() {
    return systemInformation;
  }

  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  private SystemInformation systemInformation;

  @Required
  public void setReportServiceExecutorService(ReportServiceExecutorService reportServiceExecutorService) {
    this.reportServiceExecutorService = reportServiceExecutorService;
  }

  @Required
  public void setJdbcConnectionManager(JdbcConnectionManager jdbcConnectionManager) {
    this.jdbcConnectionManager = jdbcConnectionManager;
  }

  public void setTimeOutMillis(long timeOutMillis) {
    this.timeOutMillis = timeOutMillis;
  }

  public void setTimedOutCode(String timedOutCode) {
    this.timedOutCode = timedOutCode;
  }

  public void setTimedOutMessage(String timedOutMessage) {
    this.timedOutMessage = timedOutMessage;
  }

  public void setNoEndpointCode(String noEndpointCode) {
    this.noEndpointCode = noEndpointCode;
  }

  public void setNoEndpointMessage(String noEndpointMessage) {
    this.noEndpointMessage = noEndpointMessage;
  }

  public void setMaxRunners(int maxRunners) {
    this.maxRunners = maxRunners;
  }

  public void setMaxedRunnerCode(String maxedRunnerCode) {
    this.maxedRunnerCode = maxedRunnerCode;
  }

  public void setMaxedRunnerMessage(String maxedRunnerMessage) {
    this.maxedRunnerMessage = maxedRunnerMessage;
  }

  private String getValidationTaskKey(String channelName, String endpoint) {
    return endpoint + "@" + channelName;
  }

  private synchronized void addFutureToRunningTasks(String channelName, String endpoint,
                                       Future<CustomValidateResult> validateResponseFuture) {
    String key = getValidationTaskKey(channelName, endpoint);
    List<Future<CustomValidateResult>> listOfTasks;
    if (runningTasks.containsKey(key)) {
      listOfTasks = runningTasks.get(key);
    }
    else {
      listOfTasks = new ArrayList<Future<CustomValidateResult>>();
      runningTasks.put(key, listOfTasks);
    }
    listOfTasks.add(validateResponseFuture);
  }

  private synchronized boolean areThereTooManyToRunningValidations(String channelName, String endpoint) {
    boolean result = false;
    String key = getValidationTaskKey(channelName, endpoint);
    List<Future<CustomValidateResult>> listOfTasks;
    if (runningTasks.containsKey(key)) {
      listOfTasks = runningTasks.get(key);
      int runningCount = 0;
      List<Future<CustomValidateResult>> finishedTasks = new ArrayList<>();
      for (Future<CustomValidateResult> future : listOfTasks) {
        if (!future.isDone())
          runningCount++;
        else
          finishedTasks.add(future);
      }
      listOfTasks.removeAll(finishedTasks);
      if (runningCount >= maxRunners) {
        result = true;
      }
    }
    return result;
  }

  private void addMagicValidations(Map<String, Object> parameters) {
    if (parameters.containsKey("UCR")) {
      Object ucr = parameters.get("UCR");
      if (ucr instanceof String) {
        if (((String) ucr).length() >= 11) {
          String ccn = ucr.toString().substring(3, 11);
          parameters.put("CCN", ccn);
        }
      }
    }
  }

  public CustomValidateResult runValidation(String channelName, String endpoint, Map<String, Object> parameters) {
    if (areThereTooManyToRunningValidations(channelName, endpoint)) {
      logger.warn("Too many running validation calls '" + endpoint + "' running on channel '" + channelName + "'");
      return new CustomValidateResult(StatusType.Error, maxedRunnerCode, maxedRunnerMessage);
    }

    ValidationData connVal = null;
    if (channelName != null) {
      connVal = rulesCache.getValidationByEndpoint(
              channelName, PackageCache.CacheStrategy.CacheOrSource, endpoint);
    }

    if (connVal != null) {
      //Do some specific "magic" trigger stuff for CCN, VAT and UCR numbers...
      addMagicValidations(parameters);

      // Make the call
      ExecutorService executorService = reportServiceExecutorService.getExecutorService();
      Future<CustomValidateResult> validateResponseFuture = null;
      if (connVal.getSql() != null) {
        ValidateViaDatabaseTask task = new ValidateViaDatabaseTask(jdbcConnectionManager, configStore, connVal, parameters);
        validateResponseFuture = executorService.submit(task);
      }
      else if (connVal.getRest() != null) {
        ValidateViaRESTCallTask task = new ValidateViaRESTCallTask(configStore, connVal, parameters);
        task.setSystemInformation(systemInformation);
        validateResponseFuture = executorService.submit(task);
      }

      if (validateResponseFuture != null) {
        try {
          return validateResponseFuture.get(timeOutMillis, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
          addFutureToRunningTasks(channelName, endpoint, validateResponseFuture);

          logger.warn("Timed out waiting for external validation '" + endpoint + "'");
          return new CustomValidateResult(StatusType.Error, timedOutCode, timedOutMessage);
        }
      }
    }
    logger.warn("No external validation call '" + endpoint + "' defined for channel '" + channelName + "'");
    return new CustomValidateResult(StatusType.Error, noEndpointCode, noEndpointMessage);
  }

}
