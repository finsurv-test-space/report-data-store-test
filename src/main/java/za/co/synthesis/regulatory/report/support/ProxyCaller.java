package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ProxyCallData;
import za.co.synthesis.regulatory.report.validate.ValidationUtils;

import java.util.Base64;
import java.util.Map;

/**
 * Support class for making calls to other web services as defined in proxycall_config.json
 * Created by Marais Neethling on 2017/10/13.
 */
public class ProxyCaller {
  private static final Logger logger = LoggerFactory.getLogger(ProxyCaller.class);

  private SystemInformation systemInformation;

  public SystemInformation getSystemInformation() {
    return systemInformation;
  }

  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  /**
   * Makes a call to the specified service end-point with the supplied parameters.
   * @param proxyCallData
   * @param parameters
   * @return
   * @throws Exception
   */
  public Object call(ProxyCallData proxyCallData, Map<String, Object> parameters) throws Exception {
    String response = ValidationUtils.makeRESTCall(proxyCallData.getRest(), parameters, systemInformation.getConfigStore(), logger);

    if (response != null && proxyCallData.getCompose() != null) {
      String responseName = "response";
      if (response != null) {
        parameters.put(responseName, response);
        parameters.put(responseName+"Body", Base64.getEncoder().encodeToString(response.getBytes()));
      } else {
        parameters.put(responseName, null);
      }

      String composedResult = ValidationUtils.compose(proxyCallData.getName() + " " + responseName,
              proxyCallData.getCompose(), parameters, systemInformation.getConfigStore(), logger);

      JSStructureParser parser = new JSStructureParser(composedResult);
      return parser.parse();
    } else if (response != null) {
      try {
        JSStructureParser parser = new JSStructureParser(response);
        return parser.parse();
      } catch (Exception e) {
        return response;
      }
    }
    return null;
  }


  public ProxyCallData getProxyCallData(String name) {
    Map<String, ProxyCallData> callmap = systemInformation.getConfigStore().getProxyCalls();
    if (callmap != null) {
      return callmap.get(name);
    }
    return null;
  }
}
