package za.co.synthesis.regulatory.report.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.persist.ProxyCallData;
import za.co.synthesis.regulatory.report.schema.AccountHolder;
import za.co.synthesis.regulatory.report.schema.AccountHolderRef;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.ProxyCaller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("internal/api/accountholder")
public class InternalAccountHolderApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalAccountHolderApi.class);

  @Autowired
  private ProxyCaller proxy;

  @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasRole('VIEW_ACCOUNTHOLDERS')")
  public
  @ResponseBody
  List<AccountHolderRef> listAccountHolders(
          @RequestParam String filter1, @RequestParam String filter2) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "listAccountHolders()");

    ProxyCallData proxyCallData = proxy.getProxyCallData("accountholder_list");
    if (proxyCallData != null) {
      Map<String, Object> parameters = new HashMap<String, Object>();
      parameters.put("filter1", filter1);
      parameters.put("filter2", filter2);

      Object object = proxy.call(proxyCallData, parameters);

      final ObjectMapper mapper = new ObjectMapper();
      return mapper.convertValue(object, new TypeReference<List<AccountHolderRef>>() {});
    }
    else {
      if (1==1) throw new NotImplementedException("Still needs to be implemented");
      List<AccountHolderRef> list = new ArrayList<AccountHolderRef>();
      AccountHolderRef ref1 = new AccountHolderRef();
      ref1.setReference("ref1");
      ref1.setDescription("Acme Ltd");
      AccountHolderRef ref2 = new AccountHolderRef();
      ref2.setReference("ref2");
      ref2.setDescription("Spacious Org");
      list.add(ref1);
      list.add(ref2);
      return list;
    }
  }

  @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasRole('VIEW_ACCOUNTHOLDERS')")
  public
  @ResponseBody
  AccountHolder getAccountHolder(
          @RequestParam String reference) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getAccountHolder()");
    ProxyCallData proxyCallData = proxy.getProxyCallData("accountholder_detail");
    if (proxyCallData != null) {
      Map<String, Object> parameters = new HashMap<String, Object>();
      parameters.put("reference", reference);

      Object object = proxy.call(proxyCallData, parameters);
      final ObjectMapper mapper = new ObjectMapper();
      return mapper.convertValue(object, AccountHolder.class);
    }
    else {
      if (1==1) throw new NotImplementedException("Still needs to be implemented");
      AccountHolder result = new AccountHolder();
      result.setReference(reference);
      result.setIndividual(new AccountHolder.Individual());
      result.getIndividual().setName("Acme");
      return result;
    }
  }
}
