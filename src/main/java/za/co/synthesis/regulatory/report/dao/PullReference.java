package za.co.synthesis.regulatory.report.dao;

/**
 * Created by jake on 3/22/17.
 */
public interface PullReference {
  boolean isMore();
  boolean isRefString();
  String getNextReference();
}
