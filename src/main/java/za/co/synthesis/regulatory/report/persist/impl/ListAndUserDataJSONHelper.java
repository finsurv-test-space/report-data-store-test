package za.co.synthesis.regulatory.report.persist.impl;

import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.support.AccessType;

import java.util.*;


/**
 * Created by jake on 7/27/17.
 */
public class ListAndUserDataJSONHelper {
  private static final String REPORT = "report";
  private static final String META = "meta";
  private static final String SYSTEM = "system";
//  private static final String VALIDATION = ""; //validation data fields?
//  private static final String EVALUATION = ""; //decision data fields?

  public static Map<String, ListData> parseListDataMap(Map doc) {
    Map<String, ListData> result = new HashMap<String, ListData>();
    if (doc != null && doc.containsKey("lists")) {
      Object objList = doc.get("lists");
      if (objList instanceof List) {
        for (Object entry : (List)objList) {
          if (entry instanceof Map) {
            ListData data = parseListData((Map)entry);
            result.put(data.getName(), data);
          }
        }
      }
    }
    return result;
  }

  public static ListData parseListData(Map doc) {
    ListData result = null;
    if (doc.containsKey("name") && doc.get("name") != null) {
      result = new ListData(doc.get("name").toString());
  
      List objExclusionFilterList = (List) doc.get("listingExclusionFilter");
      if (objExclusionFilterList != null) {
        for (Object exclusionFilterEntry : objExclusionFilterList) {
          if (exclusionFilterEntry instanceof String)
          result.getListingExclusionFilter().add((String)exclusionFilterEntry);
        }
      }
      
      List objFilterList = (List) doc.get("listingFilter");
      if (objFilterList != null) {
        for (Object filterEntry : objFilterList) {
          if (filterEntry instanceof String)
          result.getListingFilter().add((String)filterEntry);
        }
      }
      
      
      List objFieldList = (List) doc.get("fields");
      if (objFieldList != null) {
        for (Object fieldEntry : objFieldList) {
          if (fieldEntry instanceof Map) {
            Map fieldEntryMap = (Map) fieldEntry;
            String label = (String) fieldEntryMap.get("label");
            if (label != null) {
              if (fieldEntryMap.containsKey(META)) {
                result.getFields().add(new ListData.Field(label, CriterionType.Meta,
                    fieldEntryMap.get(META).toString()));
              }
              if (fieldEntryMap.containsKey(REPORT)) {
                result.getFields().add(new ListData.Field(label, CriterionType.Report,
                    fieldEntryMap.get(REPORT).toString()));
              }
              if (fieldEntryMap.containsKey(SYSTEM)) {
                result.getFields().add(new ListData.Field(label, CriterionType.System,
                    fieldEntryMap.get(SYSTEM).toString()));
              }
            }
          }
        }
      }

      List objCriterionList = (List) doc.get("criteria");
      if (objCriterionList != null) {
        for (Object criterionEntry : objCriterionList) {
          if (criterionEntry instanceof Map) {
            Map criterionEntryMap = (Map) criterionEntry;
            String valueKey = criterionEntryMap.containsKey("values") ? "values" : "value";
            if (criterionEntryMap.containsKey(META)) {
              result.getCriteria().add(new CriterionData(CriterionType.Meta,
                  criterionEntryMap.get(META).toString(), criterionEntryMap.get(valueKey), "LIST_DATA"));
            }
            if (criterionEntryMap.containsKey(REPORT)) {
              result.getCriteria().add(new CriterionData(CriterionType.Report,
                  criterionEntryMap.get(REPORT).toString(), criterionEntryMap.get(valueKey), "LIST_DATA"));
            }
            if (criterionEntryMap.containsKey(SYSTEM)) {
              result.getCriteria().add(new CriterionData(CriterionType.System,
                  criterionEntryMap.get(SYSTEM).toString(), criterionEntryMap.get(valueKey), "LIST_DATA"));
            }
          }
        }
      }
    }
    return result;
  }

  public static AccessSetData parseAccessSet(Map jsAccessSet) {
    AccessSetData accessSetData = null;
    if (jsAccessSet.containsKey("name")) {
      String accessSetName = jsAccessSet.get("name").toString();
      String accessType = "read";
      String accessreportSpace = null;
      AccessType type = AccessType.Read;
      if (jsAccessSet.containsKey("write")) {
        accessType = "write";
        type = AccessType.Write;
      }
      if (jsAccessSet.containsKey("reportSpace") && jsAccessSet.get("reportSpace") != null) {
        accessreportSpace = jsAccessSet.get("reportSpace").toString();
      }
      accessSetData = new AccessSetData(accessSetName, type, accessreportSpace);
      List objAccessList = (List) jsAccessSet.get(accessType);
      for (Object accessEntry : objAccessList) {
        if (accessEntry instanceof Map) {
          Map accessEntryMap = (Map)accessEntry;
          String valueKey = accessEntryMap.containsKey("values")?"values":"value";
          if (accessEntryMap.containsKey(META)) {
            accessSetData.getAccessCriteria().add(new CriterionData(CriterionType.Meta,
                    accessEntryMap.get(META).toString(), getListOfStringsFromProperty(accessEntryMap,valueKey), "ACCESS_SET"));
          }
          if (accessEntryMap.containsKey(REPORT)) {
            accessSetData.getAccessCriteria().add(new CriterionData(CriterionType.Report,
                    accessEntryMap.get(REPORT).toString(), getListOfStringsFromProperty(accessEntryMap, valueKey), "ACCESS_SET"));
          }
          if (accessEntryMap.containsKey(SYSTEM)) {
            accessSetData.getAccessCriteria().add(new CriterionData(CriterionType.System,
                    accessEntryMap.get(SYSTEM).toString(), getListOfStringsFromProperty(accessEntryMap,valueKey), "ACCESS_SET"));
          }
        }
      }
    }
    return accessSetData;
  }

  public static Map<String, AccessSetData> parseAccessSetMap(Map doc) {
    Map<String, AccessSetData> result = new HashMap<String, AccessSetData>();
    if (doc != null && doc.containsKey("access_sets")) {
      List objAccessSetList = (List) doc.get("access_sets");
      for (Object objAccessSet : objAccessSetList) {
        if (objAccessSet instanceof Map) {
          AccessSetData accessSetData = parseAccessSet((Map) objAccessSet);
          if (accessSetData != null) {
            result.put(accessSetData.getName(), accessSetData);
          }
        }
      }
    }
    return result;
  }

  public static Map<String, Object> getMapFromProperty(Map map, String property) {
      // TODO : Complete Function
    return new HashMap<String, Object>();
  }
  public static List<String> getListOfStringsFromProperty(Map map, String property) {
    Object objValue = map.get(property);
    if (objValue instanceof String) {
      return Arrays.asList(((String)objValue).split("\\s*,\\s*"));
    }
    if (objValue instanceof List) {
      List list = (List)objValue;
      List<String> result = new ArrayList<String>();
      for (Object entry : list) {
        result.add(entry.toString());
      }
      return result;
    }
    return new ArrayList<String>();
  }

  public static Map<String, UserData> parseUserDataMap(Map doc) {
    Map<String, UserData> result = new HashMap<String, UserData>();
    if (doc.containsKey("users")) {
      Object objUsers = doc.get("users");
      if (objUsers instanceof List) {
        for (Object userObj : (List)objUsers) {
          Map jsUserObj = (Map)userObj;
          String username = (String)jsUserObj.get("username");
          String password = (String)jsUserObj.get("password");
          Boolean active = (Boolean)jsUserObj.get("active");

          UserData u = new UserData(username,
                  password,
                  getListOfStringsFromProperty(jsUserObj, "rights"),
                  getListOfStringsFromProperty(jsUserObj, "access"),
                  getListOfStringsFromProperty(jsUserObj, "channels"),
                  active,
                  getMapFromProperty(jsUserObj, "details"),
                  getListOfStringsFromProperty(jsUserObj,"reportLists"),
                  getListOfStringsFromProperty(jsUserObj,"userEvents"));
          result.put(username, u);
        }
      }
    }
    return result;
  }
}
