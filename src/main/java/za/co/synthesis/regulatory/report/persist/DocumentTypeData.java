package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.rule.core.Scope;

/**
 * Created by jake on 7/27/17.
 */
public class DocumentTypeData {
  private Scope scope;
  private String type;
  private String description;

  public DocumentTypeData() {
  }

  public DocumentTypeData(Scope scope, String type, String description) {
    this.scope = scope;
    this.type = type;
    this.description = description;
  }

  public Scope getScope() {
    return scope;
  }

  public String getType() {
    return type;
  }

  public String getDescription() {
    return description;
  }

  public void setScope(Scope scope) {
    this.scope = scope;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}