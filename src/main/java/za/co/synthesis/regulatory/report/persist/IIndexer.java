package za.co.synthesis.regulatory.report.persist;


import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecord;

import java.util.List;

public interface IIndexer {
  void clear();
  void indexRecord(IndexedRecord record, String fulltext);
  List<IndexedRecord> searchRecords(List<CriterionData> fields);
}
