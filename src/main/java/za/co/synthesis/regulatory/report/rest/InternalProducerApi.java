package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.List;

@Controller
@RequestMapping("internal/producer/api")
public class InternalProducerApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalProducerApi.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private IChannelCache channelCache;

  @Autowired
  private RulesCache rulesCache;



  public InternalProducerApi() {
  }


  //-----------------------------------------------------------------------------------------------
//  Form APIs

  /**
   * expose request for "producer/api/form"
   * Exposed as "internal/producer/api/form"
   *
   * @param channelName
   * @param trnReference
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "/form", method = RequestMethod.GET, produces = "text/html; charset=UTF-8" )
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getInternalForm(@RequestParam String channelName,
                       @RequestParam(required = false) String trnReference,
                       @RequestParam(required = false) Boolean allowNew,
                       HttpServletRequest request,
                       HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalForm()");
    String resultHtml = Form.getForm(channelName, trnReference, systemInformation, dataLogic, channelCache, true, request.getParameterMap(), null, false, allowNew);
    response.getWriter().print(resultHtml);
  }


//-----------------------------------------------------------------------------------------------
//  JS APIs

  /**
   * expose request for "producer/api/form"
   * Exposed as "internal/producer/api/form"
   *
   * @param channelName
   * @param trnReference
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "/js", method = RequestMethod.GET, produces = "text/html")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getInternalFormAPI(@RequestParam String channelName,
                          @RequestParam(required = false) String trnReference,
                          HttpServletResponse response) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalFormAPI()");
    FormApi.getFormAPI(channelName, trnReference, false, response, systemInformation, dataLogic, channelCache, rulesCache, false);
  }


//-----------------------------------------------------------------------------------------------
//  Report APIs

  /**
   * expose request for "producer/api/report/history"
   * Exposed as "internal/producer/api/report/history"
   *
   * @param channelName Optional channel name parameter - if not specified, the reporting space must be provided.
   * @param reportSpace Optional reporting space parameter - if not provided, a valid channel name must be provided.
   * @param trnReference The transaction reference number for which the history must be returned.
   * @return Returns the entire history of report objects and related information for the given TrnReference.
   * @throws Exception
   */
  @RequestMapping(value = "/report/history", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ReportDataHistory getInternalReportHistory(@RequestParam(required=false) String channelName,
                                             @RequestParam(required=false) String reportSpace,
                                        @RequestParam String trnReference) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportHistory()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }

    List<ReportInfo> reportHistory = dataLogic.getReportInfoHistory(reportSpace, trnReference, SecurityHelper.getAuthMeta());
    if (reportHistory == null || reportHistory.size() == 0)
      throw new ResourceNotFoundException("Report for '" + trnReference.replaceAll("[<|>]","") + "@" + reportSpace.replaceAll("[<|>]","") + "' not found");

    ReportInfo reportInfo = reportHistory.get(reportHistory.size() - 1);
    ReportDataWithState report = new ReportDataWithState(reportInfo.getReportData(), reportInfo.getState());
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to retrieve this resource."));

    return new ReportDataHistory(reportSpace, trnReference, reportHistory);
  }


  /**
   * expose request for "producer/api/report/{schema}"
   * Exposed as "internal/producer/api/report/{schema}"
   */
  @RequestMapping(value = "/report/data/{schema}", method = RequestMethod.GET, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ReportDataWithState getInternalReportSchema(@PathVariable String schema,
                                        @RequestParam(required = false) String channelName,
                                        @RequestParam(required = false) String reportSpace,
                                        @RequestParam String trnReference) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportSchema()");
    if (trnReference.matches(".*%\\d\\d.*")){ //it seems that when running tests, the parameters are often still urlencoded.   ...?
      trnReference = URLDecoder.decode(trnReference, "UTF-8");
    }
    ReportDataWithState report = dataLogic.getReportData(schema, channelName, reportSpace, trnReference, SecurityHelper.getAuthMeta());
    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to retrieve this resource."));

    return report;
  }



  /**
   * expose request for "producer/api/report/data/{schema}"
   * Exposed as "internal/producer/api/report/data/{schema}"
   */
  @RequestMapping(value = "/report/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ValidationResult setInternalReportSchema(@PathVariable String schema,
                                           @RequestParam(required = false) String channelName,
                                           @RequestBody ReportData report) throws Exception {

    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setInternalReportSchema()");
//    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));

    return dataLogic.setReportData(schema, channelName, report, SecurityContextHolder.getContext().getAuthentication());
  }
  
  
  
  /**
   * expose request for "producer/api/report/decision/data/{schema}"
   * Exposed as "internal/producer/api/report/decision/data/{schema}"
   */
  @RequestMapping(value = "/report/decision/data/{schema}", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ValidationResult setInternalReportWithDecision(
      @PathVariable String schema,
      @RequestParam String channelName,
      @RequestBody ReportDataWithDecision report) throws ResourceAccessAuthorizationException{
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setInternalReportWithDecision()");
//    SecurityHelper.authorizationInterceptor(systemInformation.getConfigStore(), report, true, new ResourceAccessAuthorizationException("You are not authorized to update this resource."));
    if (report != null && report.getDecision() != null && report.getTrnReference() != null){
      Decision.setDecision(channelName, report.getTrnReference(), report.getDecision(), systemInformation, dataLogic);
    }
    return dataLogic.setReportData(schema, channelName, report, SecurityHelper.getAuthMeta());
  }

//-----------------------------------------------------------------------------------------------
//  UserAction APIs

  /**
   * expose request for "producer/api/action"
   * Exposed as "internal/producer/api/action"
   */
  @RequestMapping(value = "/action", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ReportState doInternalUserAction(
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam String trnReference,
          @RequestParam String action) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "doInternalUserAction()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid reportSpace must be specified");
    }
    ReportState state = dataLogic.doAction(reportSpace, channelName, trnReference, action, SecurityHelper.getAuthMeta());
    return state;
  }


//-----------------------------------------------------------------------------------------------
  //TODO: expose Lookup APIs
  //TODO: expose Rules APIs
  //TODO: expose Enquiry APIs
//-----------------------------------------------------------------------------------------------
}
