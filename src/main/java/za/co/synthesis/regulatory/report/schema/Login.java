package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "Username"
        , "Password"
        , "SetCookie"
})
public class Login {
  private String username;
  private String password;
  private boolean setCookie = true;

  @JsonProperty("Username")
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @JsonProperty("Password")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @JsonProperty("SetCookie")
  public boolean isSetCookie() {
    return setCookie;
  }

  public void setSetCookie(boolean setCookie) {
    this.setCookie = setCookie;
  }
}
