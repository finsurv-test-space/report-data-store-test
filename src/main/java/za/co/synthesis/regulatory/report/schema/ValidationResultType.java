package za.co.synthesis.regulatory.report.schema;

/**
 * Created by jake on 6/13/17.
 */
public enum ValidationResultType {
  Unspecified,
  Success,
  Failure
}
