package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.security.IPasswordStrength;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.regulatory.report.support.properties.SettingsCrypto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@Controller
@RequestMapping("utils")
public class UtilitiesApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(UtilitiesApi.class);

  @Autowired
  private UserInformation userInformation;

  @Autowired
  private SystemInformation systemInformation;
  
  private SettingsCrypto crypto = SettingsCrypto.Standard();

  @Autowired
  public IPasswordStrength passwordStrength;

  @Autowired
  ApplicationContext ctx;

//-----------------------------------------------------------------------------------------------
//  User Management APIs

//  /**
//   * Create password hash of the provided text - to be used for storing a local/internal user with a password hash instead of plaintext
//   * This is provided to assist when creating config files or SQL files with user objects therein - while not divulging the associated passwords.
//   * @param plaintext The original text to be provided by the user
//   * @return The hashed data produced from the original text
//   * @throws Exception
//   */
//  @Deprecated
//  @RequestMapping(value = "/hash", method = RequestMethod.GET, produces = "text/plain")
//  @ResponseStatus(HttpStatus.OK)
////  @PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
//  public
//  @ResponseBody
//  String getHash(
//          @RequestParam String plaintext,
//          HttpServletResponse response) throws Exception {
//
//    if (plaintext == null) {
//      throw new ResourceNotFoundException("A valid string is required to perform a hash.");
//    }
//    return SecurityHelper.getPasswordHash(plaintext);
//  }

  /**
   * Create password hash of the provided text - to be used for storing a local/internal user with a password hash instead of plaintext
   * This is provided to assist when creating config files or SQL files with user objects therein - while not divulging the associated passwords.
   * Any text provided is checked for basic password complexity. This password complexity check can be configured (to some extent) for the defaulted <b>passwordStrength</b> bean located in the <i>report-service-spring-root.xml</i> file.
   * Current, default checks catered for are a length checks and combination of regex pattern checks.
   * For more information please see the relevant bean configuration mentioned above.
   * @param plaintext The original text to be provided by the user
   * @return The hashed data produced from the original text
   * @throws Exception
   */
  @RequestMapping(value = "/passwordhash", method = RequestMethod.GET, produces = "text/plain")
  @ResponseStatus(HttpStatus.OK)
//  @PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  String getPasswordHash(
          @RequestParam String plaintext,
          HttpServletResponse response) throws Exception {

    if (plaintext == null) {
      throw new ResourceNotFoundException("A valid string is required to perform a hash.");
    }
    boolean passwordStrength = this.passwordStrength.getPasswordStrength(plaintext);
    if(!passwordStrength)
    {
      throw new Exception("Password is not strong enough");
    }

    return SecurityHelper.getPasswordHash(plaintext);
  }


  
  /**
   * Create an encrypted config value for the properties.json (or related) configuration file.
   * Alternatively, if you simply wrap the plaintext value in the config file with {ENCRYPT:[plaintext_value]}, the system will automagically encrypt the value and overwrite the config file at startup.
   * @param plaintext The original text to be provided by the user
   * @return The encrypted config value to be stored in the json config file ("parameter" section entries only).
   * @throws Exception
   */
  @RequestMapping(value = "/configCrypto", method = RequestMethod.GET, produces = "text/plain")
  @ResponseStatus(HttpStatus.OK)
//  @PreAuthorize("hasRole('VIEW_USER_DETAILS') OR hasRole('SET_USER_DETAILS')")
  public
  @ResponseBody
  String getEncryptedConfig(
      @RequestParam String plaintext,
      HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    
    if (plaintext == null) {
      throw new ResourceNotFoundException("A valid string is required to perform this encryption.");
    }
    if (request.getParameter("decrypt") != null) {
      return crypto.decrypt(plaintext);
    }
    return "{CRYPTO:"+crypto.encrypt(plaintext)+"}";
  }


}
