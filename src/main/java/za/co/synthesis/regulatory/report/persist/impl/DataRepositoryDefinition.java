package za.co.synthesis.regulatory.report.persist.impl;

import za.co.synthesis.regulatory.report.persist.DataSourceData;
import za.co.synthesis.regulatory.report.persist.LookupData;
import za.co.synthesis.regulatory.report.persist.ValidationData;

import java.util.HashMap;

/**
 * Created by james on 2017/07/05.
 * "jdbc.driverClassName" : "com.microsoft.sqlserver.jdbc.SQLServerDriver",
 * "jdbc.url" : "jdbc:sqlserver://${txstream.host};databaseName=${txstream.database};sendStringParametersAsUnicode=false",
 * "jdbc.username": "${txstream.username}",
 * "jdbc.password" : "${txstream.password}"
 */
public class DataRepositoryDefinition {
  private String name;
  private DataSourceData dataSource;
  private final HashMap<String, LookupData> lookupDefinitions = new HashMap<>();
  private final HashMap<String, ValidationData> validationDefinitions = new HashMap<>();

  public DataRepositoryDefinition(String name) {
    this.name = name;
  }

  public DataSourceData getDataSource() {
    return dataSource;
  }

  public void setDataSource(DataSourceData dataSource) {
    this.dataSource = dataSource;
  }

  public HashMap<String, LookupData> getLookupDefinitions() {
    return lookupDefinitions;
  }

  public void addLookupDefinition(String lookupName, LookupData lookupDefinition) {
    lookupDefinitions.put(lookupName, lookupDefinition);
  }

  public HashMap<String, ValidationData> getValidationDefinitions() {
    return validationDefinitions;
  }

  public void addValidationDefinition(String validationName, ValidationData validationDefinition) {
    validationDefinitions.put(validationName, validationDefinition);
  }

}
