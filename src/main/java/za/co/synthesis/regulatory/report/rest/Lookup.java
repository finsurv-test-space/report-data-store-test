package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.support.PackageCache;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.rule.core.FlowType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 5/30/17.
 */
@CrossOrigin
@Controller
@RequestMapping("producer/api/lookup")
public class Lookup extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Lookup.class);
  Map<String, RegisteredLookupConfig> lookupConfigs = new HashMap<>(); //TODO: write unit to populate from config.

  public static class LookupEntry {
    private PackageCache.Entry entry;
    private JSObject jsObject;

    public LookupEntry(PackageCache.Entry entry) {
      this.entry = entry;
    }

    public PackageCache.Entry getEntry() {
      return entry;
    }

    public void setEntry(PackageCache.Entry entry) {
      this.entry = entry;
    }

    public JSObject getJsObject() {
      return jsObject;
    }

    public void setJsObject(JSObject jsObject) {
      this.jsObject = jsObject;
    }
  }

  @Autowired
  private RulesCache rulesCache;

  private final Map<String, LookupEntry> channelLookupMap = new HashMap<String, LookupEntry>();

  public static JSArray getLookupArray(String channelName, String lookupName, Map<String, LookupEntry> channelLookupMap, RulesCache rulesCache) throws Exception {
    LookupEntry lookupEntry;
    if (channelLookupMap.containsKey(channelName)) {
      lookupEntry = channelLookupMap.get(channelName);
      if (rulesCache.isEntryOld(channelName, lookupEntry.getEntry())) {
        lookupEntry.setEntry(rulesCache.getLookups(channelName, PackageCache.CacheStrategy.GetFromSource));
      } else {
        lookupEntry = new LookupEntry(rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource));
      }
//        lookupEntry = new LookupEntry(rulesCache.getLookups(channelName, PackageCache.CacheStrategy.GetFromCache));
      JSStructureParser parser = new JSStructureParser(lookupEntry.getEntry().getData());
      Object data = parser.parse();

      JSObject lookups = JSUtil.findObjectWith(data, lookupName);
      lookupEntry.setJsObject(lookups);

    } else {
      lookupEntry = new LookupEntry(rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource));
      channelLookupMap.put(channelName, lookupEntry);

      JSStructureParser parser = new JSStructureParser(lookupEntry.getEntry().getData());
      Object data = parser.parse();

      JSObject lookups = JSUtil.findObjectWith(data, lookupName);
      lookupEntry.setJsObject(lookups);
    }

    if (lookupEntry.getJsObject() != null && lookupEntry.getJsObject().containsKey(lookupName)) {
      Object jsObj = lookupEntry.getJsObject().get(lookupName);
      if (jsObj instanceof JSArray) {
        return (JSArray) jsObj;
      }
    }
    return new JSArray();
  }

//-----------------------------------------------------------------------------------------------
//  currencies

  @RequestMapping(value = "/currencies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Currency> getCurrencies(@RequestParam String channelName) throws Exception {
    return getCurrencies(channelName, channelLookupMap, rulesCache);
  }

  public static List<Currency> getCurrencies(String channelName,
                                             Map<String, Lookup.LookupEntry> channelLookupMap,
                                             RulesCache rulesCache) throws Exception {
    List<Currency> result = new ArrayList<Currency>();
    JSArray jsArray = getLookupArray(channelName, "currencies", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new Currency(jsEntry.get("code"), jsEntry.get("name")));
      }
    }
    return result;
  }

//-----------------------------------------------------------------------------------------------
//  countries

  @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Country> getCountries(@RequestParam String channelName) throws Exception {
    return getCountries(channelName, channelLookupMap, rulesCache);
  }

  public static List<Country> getCountries(String channelName,
                                           Map<String, Lookup.LookupEntry> channelLookupMap,
                                           RulesCache rulesCache) throws Exception {
    List<Country> result = new ArrayList<Country>();
    JSArray jsArray = getLookupArray(channelName, "countries", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new Country(jsEntry.get("code"), jsEntry.get("name")));
      }
    }
    return result;
  }

//-----------------------------------------------------------------------------------------------
//  categories;

  @RequestMapping(value = "/categories", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Category> getCategories(@RequestParam String channelName, @RequestParam FlowValue flow) throws Exception {
    return getCategories(channelName, flow.getFlowType(), channelLookupMap, rulesCache);
  }

  public static List<Category> getCategories(String channelName,
                                             FlowType flow,
                                             Map<String, Lookup.LookupEntry> channelLookupMap,
                                             RulesCache rulesCache) throws Exception {
    List<Category> result = new ArrayList<Category>();
    JSArray jsArray = getLookupArray(channelName, "categories", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        String flowStr = (String) jsEntry.get("flow");
        if ((flow == FlowType.Inflow && FlowType.Inflow.toString().equals(flowStr)) || (flow == FlowType.Outflow && FlowType.Outflow.toString().equals(flowStr))) {
          result.add(new Category(jsEntry.get("flow"), jsEntry.get("code"), jsEntry.get("oldCodes"), jsEntry.get("section"), jsEntry.get("subsection"), jsEntry.get("description")));
        }
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  institutionalSectors

  @RequestMapping(value = "/institutionalSectors", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<InstitutionalSector> getInstitutionalSectors(@RequestParam String channelName) throws Exception {
    return getInstitutionalSectors(channelName, channelLookupMap, rulesCache);
  }

  public static List<InstitutionalSector> getInstitutionalSectors(String channelName,
                                                                  Map<String, Lookup.LookupEntry> channelLookupMap,
                                                                  RulesCache rulesCache) throws Exception {
    List<InstitutionalSector> result = new ArrayList<InstitutionalSector>();
    JSArray jsArray = getLookupArray(channelName, "institutionalSectors", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new InstitutionalSector(jsEntry.get("code"), jsEntry.get("description")));
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  industrialClassifications

  @RequestMapping(value = "/industrialClassifications", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<IndustrialClassification> getIndustrialClassifications(@RequestParam String channelName) throws Exception {
    return getIndustrialClassifications(channelName, channelLookupMap, rulesCache);
  }

  public static List<IndustrialClassification> getIndustrialClassifications(String channelName,
                                                              Map<String, Lookup.LookupEntry> channelLookupMap,
                                                              RulesCache rulesCache) throws Exception {
    List<IndustrialClassification> result = new ArrayList<IndustrialClassification>();
    JSArray jsArray = getLookupArray(channelName, "industrialClassifications", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new IndustrialClassification(jsEntry.get("code"), jsEntry.get("description")));
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  customsOffices

  @RequestMapping(value = "/customsOffices", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<CustomsOffice> getCustomsOffices(@RequestParam String channelName) throws Exception {
    return getCustomsOffices(channelName, channelLookupMap, rulesCache);
  }

  public static List<CustomsOffice> getCustomsOffices(String channelName,
                                                      Map<String, Lookup.LookupEntry> channelLookupMap,
                                                      RulesCache rulesCache) throws Exception {
    List<CustomsOffice> result = new ArrayList<CustomsOffice>();
    JSArray jsArray = getLookupArray(channelName, "customsOffices", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new CustomsOffice(jsEntry.get("code"), jsEntry.get("name")));
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  reportingQualifiers

  @RequestMapping(value = "/reportingQualifiers", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getReportingQualifiers(@RequestParam String channelName) throws Exception {
    return getReportingQualifiers(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getReportingQualifiers(String channelName,
                                                    Map<String, Lookup.LookupEntry> channelLookupMap,
                                                    RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "ReportingQualifier", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  nonResExceptionNames

  @RequestMapping(value = "/nonResExceptionNames", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getNonResExceptionNames(@RequestParam String channelName) throws Exception {
    return getNonResExceptionNames(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getNonResExceptionNames(String channelName,
                                                     Map<String, Lookup.LookupEntry> channelLookupMap,
                                                     RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "nonResidentExceptions", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  resExceptionNames

  @RequestMapping(value = "/resExceptionNames", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getResExceptionNames(@RequestParam String channelName) throws Exception {
    return getResExceptionNames(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getResExceptionNames(String channelName,
                                                  Map<String, Lookup.LookupEntry> channelLookupMap,
                                                  RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "residentExceptions", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  moneyTransferAgents

  @RequestMapping(value = "/moneyTransferAgents", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getMoneyTransferAgents(@RequestParam String channelName) throws Exception {
    return getMoneyTransferAgents(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getMoneyTransferAgents(String channelName,
                                                    Map<String, Lookup.LookupEntry> channelLookupMap,
                                                    RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "moneyTransferAgents", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  provinces

  @RequestMapping(value = "/provinces", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getProvinces(@RequestParam String channelName) throws Exception {
    return getProvinces(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getProvinces(String channelName,
                                          Map<String, Lookup.LookupEntry> channelLookupMap,
                                          RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "provinces", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }

//-----------------------------------------------------------------------------------------------
//  cardTypes

  @RequestMapping(value = "/cardTypes", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getCardTypes(@RequestParam String channelName) throws Exception {
    return getCardTypes(channelName, channelLookupMap, rulesCache);
  }

  public static List<String> getCardTypes(String channelName,
                                          Map<String, Lookup.LookupEntry> channelLookupMap,
                                          RulesCache rulesCache) throws Exception {
    List<String> result = new ArrayList<String>();
    JSArray jsArray = getLookupArray(channelName, "cardTypes", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof String) {
        result.add(entry.toString());
      }
    }
    return result;
  }

//-----------------------------------------------------------------------------------------------
//  Holdco Companies
// ...Configurable External lookup?

  @RequestMapping(value = "/holdcoCompanies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<HoldcoCompany> getHoldcoCompanies(@RequestParam String channelName) throws Exception {
    return getHoldcoCompanies(channelName, channelLookupMap, rulesCache);
  }

  public static List<HoldcoCompany> getHoldcoCompanies(String channelName,
                                         Map<String, Lookup.LookupEntry> channelLookupMap,
                                         RulesCache rulesCache) throws Exception {
    List<HoldcoCompany> result = new ArrayList<HoldcoCompany>();
    JSArray jsArray = getLookupArray(channelName, "holdcoCompanies", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new HoldcoCompany(jsEntry.get("accountNumber"), jsEntry.get("registrationNumber"), jsEntry.get("companyName")));
      }
    }
    return result;
  }

  @RequestMapping(value = "/mtaAccounts", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<MTAAccount> getMTAAccounts(@RequestParam String channelName) throws Exception {
    return getMTAAccounts(channelName, channelLookupMap, rulesCache);
  }


  public static List<MTAAccount> getMTAAccounts(String channelName,
                                                Map<String, Lookup.LookupEntry> channelLookupMap,
                                                RulesCache rulesCache) throws Exception {
    List<MTAAccount> result = new ArrayList<MTAAccount>();
    JSArray jsArray = getLookupArray(channelName, "mtaAccounts", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new MTAAccount(jsEntry.get("accountNumber"), jsEntry.get("MTA"), jsEntry.get("rulingSection"), jsEntry.get("ADLALevel"), jsEntry.get("isADLA")));
      }
    }
    return result;
  }

//-----------------------------------------------------------------------------------------------
//  IHQ Companies
// ...Configurable External lookup?

  @RequestMapping(value = "/ihqCompanies", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<IhqCompany> getIhqCompanies(@RequestParam String channelName) throws Exception {
    return getIhqCompanies(channelName, channelLookupMap, rulesCache);
  }

  public static List<IhqCompany> getIhqCompanies(String channelName,
                                                 Map<String, Lookup.LookupEntry> channelLookupMap,
                                                 RulesCache rulesCache) throws Exception {
    List<IhqCompany> result = new ArrayList<IhqCompany>();
    JSArray jsArray = getLookupArray(channelName, "ihqCompanies", channelLookupMap, rulesCache);

    for (Object entry : jsArray) {
      if (entry instanceof JSObject) {
        JSObject jsEntry = (JSObject) entry;
        result.add(new IhqCompany(jsEntry.get("ihqCode"), jsEntry.get("registrationNumber"), jsEntry.get("companyName")));
      }
    }
    return result;
  }


//-----------------------------------------------------------------------------------------------
//  Externally Registered Lookups

  /**
   * This is the generic, custom lookup API which is used to access and call the relevant lookup configurations as defined for a specific channel.
   * For a list of possible lookup names, please use the '/customList' API.
   * Due to the dynamic nature of the custom API configuration, all required parameters are retreived and evaluated at runtime.  Should parameters be expected and a resulting failure occur, the list of all parameters should be returned as a response.
   * @param channelName Channel lookup configuration to load
   * @param lookupName Name of the lookup configuration to invoke
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/custom/{lookupName}", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<Object> getRegisteredLookup(@RequestParam String channelName,
                                   @PathVariable String lookupName) throws Exception {
    return getRegisteredLookup(channelName, lookupName, lookupConfigs, channelLookupMap, rulesCache);
  }



  //TODO: Update customList API to return the optional and mandatory params as well.
  /**
   * Returns the list of all lookups defined in the custom lookup configuration for the given channel.
   * @param channelName Channel Lookup configuration to load
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/customList", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  List<String> getRegisteredLookupList(@RequestParam String channelName) throws Exception {
    List<String> customLookupList = new ArrayList<>();
    getRegisteredLookup(channelName, "currencies", lookupConfigs, channelLookupMap, rulesCache); //not ideal, but we are "forcing" the lookups to be refreshed by looking for "currencies" lookup - which we *always* expect to be present.
    Object channelLookups = channelLookupMap.get(channelName);
    if (channelLookups instanceof LookupEntry && ((LookupEntry)channelLookups).jsObject instanceof JSObject) {
      for (String key : ((LookupEntry)channelLookups).jsObject.keySet()){
        customLookupList.add(key);
      }
    }
    return customLookupList;
  }



  public static List<Object> getRegisteredLookup(String channelName,
                                                 String lookupName,
                                                 Map<String, RegisteredLookupConfig> lookupConfigs,
                                                 Map<String, Lookup.LookupEntry> channelLookupMap,
                                                 RulesCache rulesCache) throws Exception {
    List<Object> result = new ArrayList<Object>();
    //This should be pulled as per some external configuration against a lookup registry...


    JSArray jsArray = null;
    RegisteredLookupConfig config = lookupConfigs.get(lookupName);
    if (config == null || config.sourceType == RegisteredLookupConfig.LookupSourceType.STATIC) {
      //Static lookup
      jsArray = getLookupArray(channelName, lookupName, channelLookupMap, rulesCache);
    }
    //TODO: DB lookup (Cached/not)
    //TODO: File lookup (cached/not)
    //TODO: REST/URL lookup (cached/not)

    //TODO: conversions between XML, JSON & CSV?
    if (jsArray != null) {
      for (Object entry : jsArray) {
        if (entry instanceof JSObject) {
          JSObject jsEntry = (JSObject) entry;
          result.add(jsEntry);
        }
      }
    } else {
      //we never get here because of the way the "getLookupArray" caching works...
      throw new ResourceNotFoundException("Lookup requested (" + lookupName + ") is unregistered or does not exist, Please check casing and composition of the lookup name.");
    }
    return result;
  }


  //TODO: implement lookup cache listing end-points?
  //TODO: implement lookup cache management end-points?
  //TODO: implement lookup config management end-points?


}
