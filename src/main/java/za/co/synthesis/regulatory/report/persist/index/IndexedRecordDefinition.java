package za.co.synthesis.regulatory.report.persist.index;

import za.co.synthesis.regulatory.report.persist.CriterionType;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the definition of our index. Only the fields below need be indexed for either searching or just for storage
 */
public class IndexedRecordDefinition {
  private String name;
  private final List<IndexField> fields = new ArrayList<IndexField>();

  public IndexedRecordDefinition(String name) {
    this.name = name;
  }

  public List<IndexField> getFields() {
    return fields;
  }

  public IndexField getFieldByTypeAndName(CriterionType type, String name) {
    for (IndexField field : fields) {
      if (field.getType().equals(type) && field.getName().equals(name)) {
        return field;
      }
    }
    return null;
  }

  public IndexField getField(IndexField byField) {
    return getFieldByTypeAndName(byField.getType(), byField.getName());
  }

  /**
   * This will only add the field if needed. I.e. Search fields take precedence over store only fields. Duplicate
   * fields will not ne added.
   * @param field the field to be added if needed
   */
  public void intelligentAddField(IndexField field) {
    IndexField existing = getField(field);
    if (existing != null) {
      if (existing.getUsage().equals(IndexField.Usage.Store) && field.getUsage().equals(IndexField.Usage.Search)) {
        fields.remove(existing);
        fields.add(field);
      }
    }
    else {
      fields.add(field);
    }
  }
}
