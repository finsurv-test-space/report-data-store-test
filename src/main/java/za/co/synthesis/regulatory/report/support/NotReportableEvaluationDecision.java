package za.co.synthesis.regulatory.report.support;

import za.co.synthesis.rule.core.*;
import za.co.synthesis.rule.evaluation.Context;

import java.util.List;

/**
 * Created by jake on 8/29/17.
 */
public class NotReportableEvaluationDecision implements IEvaluationDecision {
  private DrCr reportingSide;
  private String reportingQualifier;

  public NotReportableEvaluationDecision(DrCr reportingSide) {
    this.reportingSide = reportingSide;
    this.reportingQualifier = ReportableType.NotReportable.getReportingQualifier();
  }

  public NotReportableEvaluationDecision(DrCr reportingSide, String reportingQualifier) {
    this.reportingSide = reportingSide;
    this.reportingQualifier = reportingQualifier;
  }

  @Override
  public boolean equals(IEvaluationDecision other) {
    return false;
  }

  @Override
  public List<BankAccountType> getPossibleDrAccountTypes() {
    return null;
  }

  @Override
  public List<BankAccountType> getPossibleCrAccountTypes() {
    return null;
  }

  @Override
  public ReportableDecision getDecision() {
    return ReportableDecision.DoNotReport;
  }

  @Override
  public String getReportingQualifier() {
    return this.reportingQualifier;
  }

  @Override
  public ReportableType getReportable() {
    return ReportableType.NotReportable;
  }

  @Override
  public FlowType getFlow() {
    return null;
  }

  @Override
  public DrCr getReportingSide() {
    return reportingSide;
  }

  @Override
  public DrCr getNonResSide() {
    return null;
  }

  @Override
  public ReportingAccountType getNonResAccountType() {
    return null;
  }

  @Override
  public String getNonResException() {
    return null;
  }

  @Override
  public DrCr getResSide() {
    return null;
  }

  @Override
  public ReportingAccountType getResAccountType() {
    return null;
  }

  @Override
  public String getResException() {
    return null;
  }

  @Override
  public String getSubject() {
    return null;
  }

  @Override
  public String getLocationCountry() {
    return null;
  }

  @Override
  public String getManualSection() {
    return null;
  }

  @Override
  public String getDrSideSwift() {
    return null;
  }

  @Override
  public AccountHolderStatus getAccStatusFilter() {
    return null;
  }

  @Override
  public List<String> getCategory() {
    return null;
  }

  @Override
  public List<String> getNotCategory() {
    return null;
  }

  @Override
  public boolean isZZ1Reportable() { return false; }

  @Override
  public Context getContext() {
    return null;
  }
}
