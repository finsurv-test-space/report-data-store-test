package za.co.synthesis.regulatory.report.swagger.plugin;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 5/29/17.
 */
//@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class TestOperation implements ApiListingScannerPlugin {

  // tag::api-listing-plugin[]
  @Override
  public List<ApiDescription> apply(DocumentationContext context) {

    return new ArrayList<ApiDescription>(
            Arrays.asList( //<1>
                    new ApiDescription(
                            "/bugs/1767",
                            "This is a bug",
                            Arrays.asList( //<2>
                                    new OperationBuilder(
                                            new CachingOperationNameGenerator())
                                            .authorizations(new ArrayList())
                                            .codegenMethodNameStem("bug1767GET") //<3>
                                            .method(HttpMethod.POST)
                                            .notes("This is a test method")
                                            .summary("validateSARB")
                                            .parameters(
                                                    Arrays.asList( //<4>
                                                            new ParameterBuilder()
                                                                    .description("search by description")
                                                                    .type(new TypeResolver().resolve(Map.class))
                                                                    .name("description")
                                                                    .parameterType("body")
                                                                    .parameterAccess("access")
                                                                    .required(true)
                                                                    .modelRef(getMyModel())
                                                                    .allowMultiple(false)
                                                                    .build()))
                                            .build()),
                            false)));
  }
  // tag::api-listing-plugin[]

  private ModelRef getMyModel() {
    return new ModelRef("Person");
  }

  @Override
  public boolean supports(DocumentationType delimiter) {
    return DocumentationType.SWAGGER_2.equals(delimiter);
  }
}
