package za.co.synthesis.regulatory.report.persist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/26/17.
 */
public class ReportSpaceData {
  private String name;
  private String defaultChannel;
  private final List<StateData> states;
  private final List<TransitionData> transitions;

  public ReportSpaceData() {
    this.states = new ArrayList<StateData>();
    this.transitions = new ArrayList<TransitionData>();
  }

  public ReportSpaceData(String name, List<StateData> states, List<TransitionData> transitions) {
    this.name = name;
    this.states = states;
    this.transitions = transitions;
  }

  public String getName() {
    return name;
  }

  public String getDefaultChannel() {
    return defaultChannel;
  }

  public List<StateData> getStates() {
    return states;
  }

  public List<TransitionData> getTransitions() {
    return transitions;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDefaultChannel(String defaultChannel) {
    this.defaultChannel = defaultChannel;
  }
}
