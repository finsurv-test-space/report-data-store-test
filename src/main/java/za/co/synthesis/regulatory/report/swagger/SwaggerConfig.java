package za.co.synthesis.regulatory.report.swagger;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Lists;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.configuration.ObjectMapperConfigured;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Predicates.or;

/**
 * Created by jake on 3/31/16.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements ApplicationListener<ObjectMapperConfigured> {
  @Bean
  public Docket api() {
    List<ResponseMessage> defaultResponseMessages = new ArrayList<ResponseMessage>();
    defaultResponseMessages.add(new ResponseMessageBuilder()
        .code(401)
        .message("Unauthorized")
        .responseModel(new ModelRef("ErrorModel"))
        .build());
    defaultResponseMessages.add(new ResponseMessageBuilder()
        .code(403)
        .message("Forbidden - Insufficient rights")
        .responseModel(new ModelRef("ErrorModel"))
        .build());
    defaultResponseMessages.add(new ResponseMessageBuilder()
        .code(404)
        .message("Resource Not Found")
        .responseModel(new ModelRef("ErrorModel"))
        .build());
    defaultResponseMessages.add(new ResponseMessageBuilder()
        .code(412)
        .message("Precondition Failed - missing parameters")
        .responseModel(new ModelRef("ErrorModel"))
        .build());

    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(or(
            PathSelectors.regex("/internal/api/.*"),
            PathSelectors.regex("/internal/producer/.*"),
            PathSelectors.regex("/internal/consumer/.*"),
            PathSelectors.regex("/internal/form/.*"),
            PathSelectors.regex("/consumer/api/.*"),
            PathSelectors.regex("/producer/api/js.*"),
            PathSelectors.regex("/producer/api/rules.*"),
            PathSelectors.regex("/producer/api/validation/.*"),
//            PathSelectors.regex("/producer/api/accountEntry/.*"),
            PathSelectors.regex("/producer/api/.*"),
            PathSelectors.regex("/utils/.*"),
            PathSelectors.regex("/login/api/.*")
        ))
        .build()
        .apiInfo(apiInfo())
        .pathProvider(new BasePathAwareRelativePathProvider("/report-data-store"))
        .globalResponseMessage(RequestMethod.GET, defaultResponseMessages)
        .globalResponseMessage(RequestMethod.POST, defaultResponseMessages)
        .ignoredParameterTypes(WebRequest.class)
        .securitySchemes(Lists.newArrayList(new BasicAuth("mykey")))
        .securityContexts(Lists.newArrayList(securityContext()));
  }


  private String getVersion() {
    String versionString = "--";
    try {
      String jsonStr = JSReaderUtil.readFileFromClasspath(this.getClass().getClassLoader(), "config/version.json");
      if (jsonStr != null){
        JSStructureParser parser = new JSStructureParser(jsonStr);
        JSObject jso = (JSObject) parser.parse();
        if (jso != null && jso.get("version") != null) {
          versionString = ((String) jso.get("version"));
        }
      }
    } catch (Exception e) {
      //suppress.
    }
    return versionString;
  }

  private ApiInfo apiInfo() {
    ApiInfo apiInfo = new ApiInfo(
        "tXstream Report Data REST API",
        //TODO ...WE COULD INJECT CODE INTO THE UI HERE TO INSERT DOCUMENTATION AND EXAMPLES INTO THE END-POINT DESCRIPTIONS ETC...
        "The tXstream Report Services REST API provides access to all Report API and Form related functionality.",//+"  To substitute \"{schema}\" placeholders in the swagger UI, add a parameter \"substitute-schemas=true\" to the url.", //" <b><a href='http://www.google.com' target='_new'>clickme</a></b>",
        getVersion(),
        "API Terms Of Service URL",
        new Contact("Synthesis", "http://www.synthesis.co.za", "info@synthesis.co.za"),
        "API License",
        "API License URL",
        new ArrayList<VendorExtension>()
    );
    return apiInfo;
  }

  @Override
  public void onApplicationEvent(ObjectMapperConfigured objectMapperConfigured) {
    ObjectMapper objectMapper = objectMapperConfigured.getObjectMapper();

    SimpleModule module = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null));
    module.addSerializer(new SarbAccountTypeSerializer()); // assuming serializer declares correct class to bind to
    objectMapper.registerModule(module);
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder()
        .securityReferences(defaultAuth())
        .forPaths(PathSelectors.regex("/anyPath.*"))
        .build();
  }

  List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope
        = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Lists.newArrayList(new SecurityReference("mykey", authorizationScopes));
  }
}
