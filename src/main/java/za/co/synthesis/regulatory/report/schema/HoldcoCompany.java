package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 5/24/17.
 */
/*
holdcoCompanies : [
    {
      accountNumber : "1234567",
      registrationNumber : "2013/1234567/07",
      companyName : "holdco1"
    }
 */
@JsonPropertyOrder({
        "AccountNumber"
        , "RegistrationNumber"
        , "CompanyName"
})
public class HoldcoCompany {
  private String accountNumber;
  private String registrationNumber;
  private String companyName;

  public HoldcoCompany(Object accountNumber, Object registrationNumber, Object companyName) {
    this.accountNumber = (String)accountNumber;
    this.registrationNumber = (String)registrationNumber;
    this.companyName = (String)companyName;
  }

  @JsonProperty("AccountNumber")
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  @JsonProperty("RegistrationNumber")
  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  @JsonProperty("CompanyName")
  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
}
