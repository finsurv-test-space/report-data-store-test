package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.support.FieldDisplay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.listToJsonStr;

@JsonPropertyOrder({
        "Regulator"
})
public class ColumnDisplay {

    @JsonPropertyOrder({
            "ListName",
            "NotDisplayed"
    })
    public class FieldDisplay {
        private String listName;
        private ArrayList<String> notDisplayed;

        FieldDisplay() {
            this.listName = "";
            this.notDisplayed = new ArrayList<String>();
        }
        @JsonProperty("ListName")
        public String getList() {
            return this.listName;
        }
        @JsonProperty("NotDisplayed")
        public ArrayList<String> getNotDisplayed() {
            return this.notDisplayed;
        }

        public String toString() {
            return "{ \"ListName\":\""+listName+"\", \"NotDisplayed\" : " + listToJsonStr(notDisplayed) + " }";
        }
    }

    private Map<String, FieldDisplay> regulator;

    ColumnDisplay() {
        this.regulator = new HashMap<String, FieldDisplay>();
    }

    @JsonProperty("Regulator")
    public Map<String, FieldDisplay> getRegulator() {
        return this.regulator;
    }

//    @JsonProperty("Lists")
//    public ArrayList<FieldDisplay> getLists() {
//        return this.lists;
//    }
}
