package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.DocumentData;
import za.co.synthesis.rule.core.Scope;

/**
 * Created by jake on 5/29/17.
 */
@JsonPropertyOrder({
        "DocumentHandle"
        , "Scope"
        //, "ReportSpace"  /*TODO: ReportSpace should supercede the ReportingSpace field below.*/
        , "ReportingSpace" /*DEPRECATED NAMING CONVENTION*/
        , "TrnReference"
        , "Type"
        , "Description"
        , "Sequence"
        , "SubSequence"
        , "Uploaded"
        , "FileName"
        , "FileType"
})
public class DocumentInfo extends DocumentData {
  private String description;
  private Boolean uploaded;

  public DocumentInfo(DocumentData documentData, Boolean uploaded) {
    this(documentData.getDocumentHandle(),
            documentData.getReportSpace(),
            documentData.getTrnReference(),
            documentData.getScope(),
            documentData.getType(),
            documentData.getDescription(),
            documentData.getSequence(),
            documentData.getSubSequence(),
            uploaded);
    setAcknowledged(documentData.getAcknowledged());
    setAcknowledgedComment(documentData.getAcknowledgedComment());
    setFilename(documentData.getFileName());
    setFileType(documentData.getFileType());
  }

  public DocumentInfo(DocumentData documentData) {
    this(documentData.getDocumentHandle(),
            documentData.getReportSpace(),
            documentData.getTrnReference(),
            documentData.getScope(),
            documentData.getType(),
            documentData.getDescription(),
            documentData.getSequence(),
            documentData.getSubSequence(),
            documentData.getFileName() != null && !documentData.getFileName().isEmpty());
    setAcknowledged(documentData.getAcknowledged());
    setAcknowledgedComment(documentData.getAcknowledgedComment());
    setFilename(documentData.getFileName());
    setFileType(documentData.getFileType());
  }

  public DocumentInfo(String documentHandle, String reportSpace, String trnReference, Scope scope, String type, String description, Integer sequence, Integer subSequence, Boolean uploaded) {
    super(documentHandle, reportSpace, trnReference, scope, type, description, sequence, subSequence);
    this.description = description;
    this.uploaded = uploaded;
  }

  //---------------------//
  //-----  GETTERS  -----//
  //---------------------//

  @JsonProperty("DocumentHandle")
  public String getDocumentHandle() {
    return super.getDocumentHandle();
  }

  //@JsonProperty("ReportSpace")
  @JsonIgnore //TODO: This shouldn't be ignored!
  public String getReportSpace() {
    return super.getReportSpace();
  }

  @Deprecated
  @JsonProperty("ReportingSpace")
  public String getReportingSpace() {
    return super.getReportSpace();
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return super.getTrnReference();
  }

  @JsonProperty("Scope")
  public Scope getScope() {
    return super.getScope();
  }

  @JsonProperty("Type")
  public String getType() {
    return super.getType();
  }

  @JsonProperty("Description")
  public String getDescription() {
    return description;
  }

  @JsonProperty("Sequence")
  public Integer getSequence() {
    return super.getSequence();
  }

  @JsonProperty("SubSequence")
  public Integer getSubSequence() {
    return super.getSubSequence();
  }

  @JsonProperty("Uploaded")
  public Boolean getUploaded() {
    return uploaded;
  }

  @JsonProperty("Acknowledged")
  public Boolean getAcknowledged() {
    return super.getAcknowledged() == true;
  }

  @JsonProperty("AcknowledgedComment")
  public String getAcknowledgedComment() {
    return super.getAcknowledgedComment();
  }

  @JsonProperty("FileName")
  public String getFileName() {
    return super.getFileName();
  }

  @JsonProperty("FileType")
  public String getFileType() {
    return super.getFileType();
  }


  //---------------------//
  //-----  SETTERS  -----//
  //---------------------//

  public void setDocumentHandle(String documentHandle) {
    super.setDocumentHandle(documentHandle);
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setUploaded(Boolean uploaded) {
    this.uploaded = uploaded;
  }

  public void setAcknowledged(Boolean acknowledged) {
    super.setAcknowledged(acknowledged);
  }

  public void setAcknowledgedComment(String acknowledgedComment) {
    super.setAcknowledgedComment(acknowledgedComment);
  }

  public void setFilename(String fileName){
    super.setFilename(fileName);
  }

  public void setFileType(String fileType){
    super.setFileType(fileType);
  }


}
