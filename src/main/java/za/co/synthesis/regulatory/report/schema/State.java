package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.StateData;
import za.co.synthesis.regulatory.report.support.ActionType;
import za.co.synthesis.regulatory.report.support.BehaviourType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/07/25.
 */
@JsonPropertyOrder({
        "Name",
        "DefaultState",
        "Behaviours",
        "Actions",
        "UserEvents"
})
public class State {
  private final StateData stateData;

  public State() {
    this.stateData = new StateData();
  }

  public State(StateData stateData) {
    this.stateData = stateData;
  }

  public static List<State> wrapList(List<StateData> infoList) {
    List<State> result = new ArrayList<State>();
    for (StateData info : infoList) {
      result.add(new State(info));
    }
    return result;
  }

  @JsonProperty("Name")
  public String getName() {
    return stateData.getName();
  }

  @JsonProperty("DefaultState")
  public boolean isDefaultState() {
    return stateData.isDefaultState();
  }

  @JsonProperty("Behaviours")
  public List<BehaviourType> getBehaviours() {
    return stateData.getBehaviours();
  }

  @JsonProperty("Actions")
  public List<ActionType> getActions() {
    return stateData.getActions();
  }

  @JsonProperty("UserEvents")
  public List<String> getUserEvents() {
    return stateData.getUserEvents();
  }

  public void setName(String name) {
    stateData.setName(name);
  }

  public void setDefaultState(boolean defaultState) {
    stateData.setDefaultState(defaultState);
  }

  @JsonIgnore
  public StateData getStateData() {
    return stateData;
  }
}
