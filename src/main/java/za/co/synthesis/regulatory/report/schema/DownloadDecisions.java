package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * Created by jake on 6/8/17.
 */
@JsonPropertyOrder({
        "SyncPoint"
        , "Decisions"
})
public class DownloadDecisions {
  private String syncPoint;
  private List<FullyReferencedEvaluationDecision> decisions;

  public DownloadDecisions(String syncPoint, List<FullyReferencedEvaluationDecision> decisions) {
    this.syncPoint = syncPoint;
    this.decisions = decisions;
  }

  @JsonProperty("SyncPoint")
  public String getSyncPoint() {
    return syncPoint;
  }

  @JsonProperty("Decisions")
  public List<FullyReferencedEvaluationDecision> getDecisions() {
    return decisions;
  }
}
