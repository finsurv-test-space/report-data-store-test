package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import za.co.synthesis.regulatory.report.persist.ChannelData;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.LookupData;
import za.co.synthesis.regulatory.report.persist.ValidationData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: jake
 * Date: 5/12/16
 * Time: 10:39 AM
 * This class implements the functionality to retrieve and cache package rules and lookups used by the
 * validation and evaluation engines. The core concept is that a centralised Rules Management system with
 * store and provide access to the package rules required by this system and that this object will be
 * responsible for retrieving the rules from this system.
 */
public class PackageCache {
  private static final Logger log = LoggerFactory.getLogger(PackageCache.class);

  public enum CacheType {
    Validation,
    Evaluation,
    Data,
    Document,
    ChannelStructure
  }

  public enum CacheStrategy {
    GetFromCache,
    GetFromSource,
    CacheOrSource
  }

  public class Entry {
    private final long cacheTime;
    private final String name;
    private final String data;

    public Entry(long cacheTime, String name, String data) {
      this.cacheTime = cacheTime;
      this.name = name;
      this.data = data;
    }

    public long getCacheTime() {
      return cacheTime;
    }

    public String getName() {
      return name;
    }

    public String getData() {
      return data;
    }
  }

  // What rules are to be used
  private final String rulesManagementUrl;
  private final String packageName;
  private PackageInfo packageInfo = null;

  // Cache of the rules
  private final IConfigStore configStore;
  private long cachePeriod = 60000;
  private final Map<CacheType, Entry> cacheMap = new ConcurrentHashMap<CacheType, Entry>();
  private List<ValidationData> connectionValidations = null;

  // Connection Manager
  private final JdbcConnectionManager jdbcConnectionManager;

  public PackageCache(JdbcConnectionManager jdbcConnectionManager, IConfigStore configStore, String rulesManagementUrl, String packageName) {
    this.jdbcConnectionManager = jdbcConnectionManager;
    this.configStore = configStore;
    this.rulesManagementUrl = rulesManagementUrl;
    this.packageName = packageName;
    updateEntriesFromCache();
  }

  public long getCachePeriod() {
    return cachePeriod;
  }

  public void setCachePeriod(long cachePeriod) {
    this.cachePeriod = cachePeriod;
  }

  private String getContentFromURL(final URL fileUrl) {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));

      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = buffReader.readLine()) != null) {
        sb.append(line).append('\n');
      }
      return sb.toString();
    } catch (IOException e) {
      log.warn("Cannot load content from URL '" + fileUrl.toString() + "'", e);
      return null;
    }
  }

  private String updateContent(CacheType cachedType, String content) {
    if (cachedType == CacheType.Data) {
      try {
        List<LookupData> lookups = getConnectionLookups(CacheStrategy.CacheOrSource);

        return MergeUtil.mergeDBIntoLookups(jdbcConnectionManager, configStore, content, lookups);
      } catch (Exception e) {
        log.error("Cannot update lookups from DB", e);
      }
    }
    return content;
  }

  public boolean isEntryOld(Entry entry) {
    long currentTime = System.currentTimeMillis();
    if (currentTime - entry.getCacheTime() > cachePeriod) {
      return true;
    }
    return false;
  }

  public Entry getRuleContent(CacheType cachedType, String suffix, CacheStrategy cacheStrategy) {
    if (cacheStrategy == CacheStrategy.CacheOrSource || cacheStrategy == CacheStrategy.GetFromSource) {
      try {
        long currentTime = System.currentTimeMillis();
        if (cacheStrategy == CacheStrategy.CacheOrSource && cacheMap.containsKey(cachedType)) {
          Entry entry = cacheMap.get(cachedType);
          if (currentTime - entry.getCacheTime() > cachePeriod) {
            URL url = new URL(rulesManagementUrl + packageName + (suffix.length() > 0 ? "/" + suffix : ""));
            String content = getContentFromURL(url);

            if (content != null) {
              if (cachedType == CacheType.ChannelStructure) {
                // The list of connectionValidations is dependent on the channel structure so will be refreshed when the
                // channel structure is refreshed
                connectionValidations = null;
              }
              String updatedContent = updateContent(cachedType, content);
              Entry newEntry = new Entry(currentTime, url.toString(), updatedContent);
              cacheMap.remove(cachedType);
              cacheMap.put(cachedType, newEntry);
              writeCachedEntry(cachedType, newEntry);
              return newEntry;
            } else
              return cacheMap.get(cachedType);
          } else {
            return cacheMap.get(cachedType);
          }
        } else {
          // CacheStrategy.GetFromSource
          URL url = new URL(rulesManagementUrl + packageName + "/" + suffix);
          String content = getContentFromURL(url);

          if (content != null) {
            String updatedContent = updateContent(cachedType, content);
            Entry newEntry = new Entry(currentTime, url.toString(), updatedContent);
            cacheMap.put(cachedType, newEntry);
            writeCachedEntry(cachedType, newEntry);
            return newEntry;
          }
        }
      } catch (MalformedURLException e) {
        log.error("Cannot create URL for " + suffix + " rules");
      }
    } else if (cacheStrategy == CacheStrategy.GetFromCache) {
      return cacheMap.get(cachedType);
    }
    return null;
  }

  public Entry getValidationRules(CacheStrategy cacheStrategy) {
    return getRuleContent(CacheType.Validation, "validation", cacheStrategy);
  }

  public Entry getDocumentRules(CacheStrategy cacheStrategy) {
    return getRuleContent(CacheType.Document, "document", cacheStrategy);
  }

  public Entry getEvaluationRules(CacheStrategy cacheStrategy) {
    return getRuleContent(CacheType.Evaluation, "evaluation", cacheStrategy);
  }

  public Entry getLookups(CacheStrategy cacheStrategy) {
    return getRuleContent(CacheType.Data, "lookups", cacheStrategy);
  }

  public String getTests(String fileName) {
    String testsManagementUrl = rulesManagementUrl.replace("api/rules/", "api/tests/");
    try {
      URL url = new URL(testsManagementUrl + packageName + "/" + fileName);
      return getContentFromURL(url);
    } catch (MalformedURLException e) {
      log.error("Cannot create URL for " + fileName + " tests");
    }
    return null;
  }

  private Entry getChannelStructure(CacheStrategy cacheStrategy) {
    return getRuleContent(CacheType.ChannelStructure, "", cacheStrategy);
  }

  public PackageInfo getPackageInfo(CacheStrategy cacheStrategy) {
    if (packageInfo == null) {
      Entry channelStructure = getChannelStructure(cacheStrategy);
      packageInfo = new PackageInfo(channelStructure);
    } else {
      Entry channelStructure = getChannelStructure(cacheStrategy);
      if (packageInfo.getEntry() != channelStructure) {
        packageInfo = new PackageInfo(channelStructure);
      }
    }
    return packageInfo;
  }

  public List<String> getParentPackageNames(CacheStrategy cacheStrategy) {
    return getPackageInfo(cacheStrategy).getParentPackageNames();
  }

  public List<String> getFeaturePackageNames(CacheStrategy cacheStrategy) {
    return getPackageInfo(cacheStrategy).getFeaturePackageNames();
  }

  private Entry loadCachedEntry(CacheType cacheType) {
    String data = configStore.getCacheByName(packageName + cacheType.name());
    if (data != null)
      return new Entry(0, packageName + cacheType.name(), data);
    return null;
  }

  private void updateEntriesFromCache() {
    CacheType ct = CacheType.Validation;
    Entry entry = loadCachedEntry(ct);
    if (entry != null)
      cacheMap.put(ct, entry);
    ct = CacheType.Evaluation;
    entry = loadCachedEntry(ct);
    if (entry != null)
      cacheMap.put(ct, entry);
    ct = CacheType.Data;
    entry = loadCachedEntry(ct);
    if (entry != null)
      cacheMap.put(ct, entry);
    ct = CacheType.ChannelStructure;
    entry = loadCachedEntry(ct);
    if (entry != null)
      cacheMap.put(ct, entry);
  }

  private void writeCachedEntry(CacheType cacheType, Entry entry) {
    configStore.setCacheByName(packageName + cacheType.name(), entry.getData());
  }


  //----- Lookups and Validations  -----//
  private static boolean containsLookup(String lookupName, List<LookupData> list) {
    for (LookupData lookup : list) {
      if (lookup.getName().endsWith(lookupName)) {
        return true;
      }
    }
    return false;
  }

  private static boolean containsValidation(String lookupName, List<ValidationData> list) {
    for (ValidationData validation : list) {
      if (validation.getName().endsWith(lookupName)) {
        return true;
      }
    }
    return false;
  }

  private List<LookupData> getConnectionLookups(CacheStrategy cacheStrategy) {
    List<LookupData> result = new ArrayList<LookupData>();

    ChannelData channelData = configStore.getChannelData().get(packageName);

    if (channelData != null) {
      Map<String, LookupData> lookups = configStore.getLookupsForRepo(channelData.getCallRepo());
      for (LookupData lookup : lookups.values()) {
        if (!containsLookup(lookup.getName(), result)) {
          result.add(lookup);
        }
      }
      List<String> parents = getParentPackageNames(cacheStrategy);
      for (String parentName : parents) {
        ChannelData parentChannelData = configStore.getChannelData().get(parentName);
        if (parentChannelData != null) {
          Map<String, LookupData> parentLookups = configStore.getLookupsForRepo(parentChannelData.getCallRepo());
          for (LookupData lookup : parentLookups.values()) {
            if (!containsLookup(lookup.getName(), result)) {
              result.add(lookup);
            }
          }
        }
      }
    }
    return result;
  }

  private List<ValidationData> getConnectionValidationList(CacheStrategy cacheStrategy) {
    List<ValidationData> result = new ArrayList<ValidationData>();

    ChannelData channelData = configStore.getChannelData().get(packageName);

    if (channelData != null) {
      Map<String, ValidationData> validations = configStore.getValidationsForRepo(channelData.getCallRepo());
      for (ValidationData validation : validations.values()) {
        if (!containsValidation(validation.getName(), result)) {
          result.add(validation);
        }
      }
      List<String> parents = getParentPackageNames(cacheStrategy);
      for (String parentName : parents) {
        ChannelData parentChannelData = configStore.getChannelData().get(parentName);
        if (parentChannelData != null) {
          Map<String, ValidationData> parentValidations = configStore.getValidationsForRepo(parentChannelData.getCallRepo());
          for (ValidationData parentValidation : parentValidations.values()) {
            if (!containsValidation(parentValidation.getName(), result)) {
              result.add(parentValidation);
            }
          }
        }

      }
    }
    return result;
  }

  public List<ValidationData> getConnectionValidations(CacheStrategy cacheStrategy) {
    if (connectionValidations == null) {
      connectionValidations = getConnectionValidationList(cacheStrategy);
    }
    return connectionValidations;
  }

  public ValidationData getConnectionValidationByEndpoint(CacheStrategy cacheStrategy, String endpoint) {
    ValidationData result = null;
    List<ValidationData> tmpList = getConnectionValidations(cacheStrategy);

    if (tmpList != null && endpoint != null) {
      for (ValidationData val : tmpList) {
        if (endpoint.equals(val.getEndpoint())) {
          result = val;
          break;
        }
      }
    }
    return result;
  }
}