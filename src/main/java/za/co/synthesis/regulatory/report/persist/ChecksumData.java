package za.co.synthesis.regulatory.report.persist;

import javax.persistence.Embeddable;

/**
 * Created by jake on 5/29/17.
 */
@Embeddable
public class ChecksumData {
  private String valueDate;
  private final String reportSpace;
  private String fileType;
  private String fileName;
//  private String dataContent;
  private byte[] dataContent;

  public ChecksumData(String valueDate, String reportSpace) {
   this.valueDate = valueDate;
   this.reportSpace = reportSpace;
  }

  public ChecksumData(String fileName, String fileType, String valueDate, String reportSpace, byte[] dataContent) {
    this.fileName = fileName;
    this.fileType = fileType;
    this.valueDate = valueDate;
    this.reportSpace = reportSpace;
    this.dataContent = dataContent;
  }

  public void setValueDate(String valueDate) {
    this.valueDate = valueDate;
  }

  public String getValueDate() {
    return valueDate;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public void setFilename(String fileName) {
    this.fileName = fileName;
  }

  public void setContent(byte[] content) { this.dataContent = content; }

  public String getFileType() {
    return fileType;
  }

  public String getFileName() {
    return fileName;
  }

  public byte[] getContent() { return dataContent; }

}
