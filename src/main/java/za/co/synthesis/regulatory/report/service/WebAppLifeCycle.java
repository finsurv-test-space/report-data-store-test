package za.co.synthesis.regulatory.report.service;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.SmartLifecycle;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.IDataStore;
import za.co.synthesis.regulatory.report.support.HousekeepingProcess;
import za.co.synthesis.regulatory.report.support.ProxyCaller;
import za.co.synthesis.regulatory.report.support.ReportServiceExecutorService;
import za.co.synthesis.regulatory.report.support.RulesCache;

import javax.persistence.EntityManager;

/**
 * Created by jake on 8/3/17.
 */
public class WebAppLifeCycle implements SmartLifecycle {
  private ReportServiceExecutorService reportServiceExecutorService;
  private SystemInformation systemInformation;
  private RulesCache rulesCache;
  private IDataStore dataStore;
  private ProxyCaller proxyCaller;

  private HousekeepingProcess housekeepingProcess = null;

  @Required
  public void setReportServiceExecutorService(ReportServiceExecutorService reportServiceExecutorService) {
    this.reportServiceExecutorService = reportServiceExecutorService;
  }

  @Required
  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  @Required
  public void setRulesCache(RulesCache rulesCache) {
    this.rulesCache = rulesCache;
  }

  @Required
  public void setDataStore(IDataStore dataStore) {
    this.dataStore = dataStore;
  }

  @Required
  public void setProxyCaller(ProxyCaller proxyCaller) {
    this.proxyCaller = proxyCaller;
  }

  @Override
  public boolean isAutoStartup() {
    return true;
  }

  @Override
  public void stop(Runnable callback) {
    if (housekeepingProcess != null) {
      housekeepingProcess.stop(callback);
    }
  }

  @Override
  public void start() {
    if (housekeepingProcess != null) {
      housekeepingProcess.stop();
    }

    housekeepingProcess = new HousekeepingProcess(
            reportServiceExecutorService.getExecutorService(),
            systemInformation,
            rulesCache,
            dataStore,
            proxyCaller);

    reportServiceExecutorService.getExecutorService().submit(housekeepingProcess);
  }

  @Override
  public void stop() {
    if (housekeepingProcess != null) {
      housekeepingProcess.stop();
    }
  }

  @Override
  public boolean isRunning() {
    if (housekeepingProcess != null) {
      return housekeepingProcess.isRunning();
    }
    else
      return false;
  }

  @Override
  public int getPhase() {
    return 0;
  }
}
