package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.rule.core.Scope;

import javax.persistence.Embeddable;

/**
 * Created by jake on 5/29/17.
 */
@Embeddable
public class DocumentData {
  private String documentHandle;
  private Boolean acknowledged;
  private String acknowledgedComment;
  private final String reportSpace;
  private final String trnReference;
  private final Scope scope;
  private final String type;
  private final String description;
  private final Integer sequence;
  private final Integer subSequence;
  private String fileType;
  private String fileName;

  public DocumentData(String reportSpace, String trnReference, Scope scope, String type, String description, Integer sequence, Integer subsequence) {
    this(null, reportSpace, trnReference, scope, type, description, sequence, subsequence);
  }

  public DocumentData(String documentHandle, String reportSpace, String trnReference, Scope scope, String type, String description, Integer sequence, Integer subSequence) {
    this(documentHandle, reportSpace, trnReference, scope, type, description, sequence, subSequence, null);
  }

  public DocumentData(String documentHandle, String reportSpace, String trnReference, Scope scope, String type, String description, Integer sequence, Integer subSequence, Boolean acknowledged) {
    this.documentHandle = documentHandle;
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.scope = scope;
    this.type = type;
    this.description = description;
    this.sequence = sequence;
    this.subSequence = subSequence;
    this.acknowledged = acknowledged != null ? acknowledged : false;
  }

  public void setDocumentHandle(String documentHandle) {
    this.documentHandle = documentHandle;
  }

  public String getDocumentHandle() {
    return documentHandle;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public String getTrnReference() {
    return trnReference;
  }

  public Scope getScope() {
    return scope;
  }

  public String getType() {
    return type;
  }

  public String getDescription() {
    return description;
  }

  public Integer getSequence() {
    return sequence;
  }

  public Integer getSubSequence() {
    return subSequence;
  }

  public Boolean getAcknowledged() {
    return acknowledged != null ? acknowledged : false;
  }

  public void setAcknowledged(Boolean acknowledged) {
    this.acknowledged = acknowledged != null ? acknowledged : false;
  }

  public String getAcknowledgedComment() {
    return acknowledgedComment;
  }

  public void setAcknowledgedComment(String acknowledgedComment) {
    this.acknowledgedComment = acknowledgedComment;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public void setFilename(String fileName) {
    this.fileName = fileName;
  }

  public String getFileType() {
    return fileType;
  }

  public String getFileName() {
    return fileName;
  }

}
