package za.co.synthesis.regulatory.report.support;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by james on 2017/06/23.
 */
public class HttpResponseUtil {

    public static void writeError(HttpServletResponse response, String exceptionName, String message) throws IOException {
        JSObject jsError = new JSObject();
        jsError.put("ErrorType", exceptionName);
        jsError.put("Message", message);

        JSWriter jsWriter = new JSWriter();
        jsWriter.setNewline("\n");
        jsWriter.setIndent("  ");
        jsWriter.setQuoteAttributes(true);
        jsError.compose(jsWriter);
        response.getWriter().print(jsWriter.toString());
    }}
