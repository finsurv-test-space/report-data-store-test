package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/27/17.
 */
@JsonPropertyOrder({
        "Type"
        , "Name"
        , "Value"
        , "Values"
})
public class Criterion {
  private final CriterionData criterionData;

  public Criterion(CriterionData criterionData) {
    this.criterionData = criterionData;
  }

  public Criterion() {
    this.criterionData = new CriterionData();
  }

  public static List<Criterion> wrapList(List<CriterionData> dataList) {
    List<Criterion> result = new ArrayList<Criterion>();
    for (CriterionData data : dataList) {
      result.add(new Criterion(data));
    }
    return result;
  }

  @JsonProperty("Type")
  public CriterionType getType() {
    return criterionData.getType();
  }

  @JsonProperty("Name")
  public String getName() {
    return criterionData.getName();
  }

  @JsonProperty("Value")
  public String getValue() {
    return criterionData.getValue();
  }

  @JsonProperty("Values")
  public List<String> getValues() {
    return criterionData.getValues();
  }

  public void setType(CriterionType type) {
    criterionData.setType(type);
  }

  public void setName(String name) {
    criterionData.setName(name);
  }

  public void setValue(String value) {
    criterionData.setValue(value);
  }

  public void setValues(List<String> values) {
    criterionData.setValues(values);
  }

  @JsonIgnore
  public CriterionData getCriterionData() {
    return criterionData;
  }
}
