package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 6/11/17.
 */
@JsonPropertyOrder({
        "DownloadType"
        , "Meta"
        , "Report"
})
public class ReportDataWithDownloadType extends ReportData {
  
  private ReportDownloadType downloadType = ReportDownloadType.Update;


  public ReportDataWithDownloadType() {
    this(new HashMap<String, Object>(), new HashMap<String, Object>(), ReportDownloadType.Update);
  }

  public ReportDataWithDownloadType(ReportData report, ReportDownloadType downloadType) {
    this(report.getMeta(), report.getReport(), downloadType);
  }

  public ReportDataWithDownloadType(Map<String, Object> meta, Map<String, Object> report, ReportDownloadType downloadType) {
    super((meta != null) ? meta : new HashMap<String, Object>(), (report != null) ? report : new HashMap<String, Object>());
    this.downloadType = downloadType!=null?downloadType:ReportDownloadType.Update;
  }


  @JsonProperty("DownloadType")
  public String getDownloadTypeStr() {
    return downloadType.toString();
  }

  @JsonIgnore
  public ReportDownloadType getDownloadType() {
    return downloadType;
  }

//  @JsonIgnore
  public void setDownloadType(ReportDownloadType downloadType) {
    this.downloadType = downloadType!=null?downloadType:ReportDownloadType.Update;
  }

  public void setDownloadType(String downloadTypeStr) {
    this.downloadType = downloadType.fromString(downloadTypeStr);
  }

}
