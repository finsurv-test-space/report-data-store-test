package za.co.synthesis.regulatory.report.swagger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 6/4/17.
 */
public class SwaggerUtils {
  private final static Logger log = LoggerFactory.getLogger(SwaggerUtils.class);

  private final static String DEFINITIONS = "#/definitions/";

  public static void stripPaths(JSObject swagger, String pathStartsWith) {
    JSObject paths = (JSObject)swagger.get("paths");
    List<String> removeKeys = new ArrayList<String>();
    if (paths != null) {
      for (Map.Entry<String, Object> entry : paths.entrySet()) {
        if (entry.getKey().startsWith(pathStartsWith)) {
          removeKeys.add(entry.getKey());
        }
      }
      for (String key : removeKeys) {
        paths.remove(key);
      }
    }
  }


  public static void stripPostPaths(JSObject swagger, String pathStartsWith) {
    JSObject paths = (JSObject)swagger.get("paths");
    List<String> removeKeys = new ArrayList<String>();
    if (paths != null) {
      for (Map.Entry<String, Object> entry : paths.entrySet()) {
        if (entry.getKey().startsWith(pathStartsWith)) {
          if (entry.getValue() instanceof Map){
            Map map = (Map)entry.getValue();
            map.remove("post");
            if (map.size() == 0){
              removeKeys.add(entry.getKey());
            }
          }
        }
      }
      for (String key : removeKeys) {
        paths.remove(key);
      }
    }
  }


  public static void mergeTypes(JSObject coreType, JSObject customType) {
    if (customType != null) {
      JSObject core_defs = (JSObject) coreType.get("definitions");
      JSObject custom_defs = (JSObject) customType.get("definitions");
      if (core_defs != null && custom_defs != null) {
        for (Map.Entry<String, Object> custom_def : custom_defs.entrySet()) {
          if (core_defs.containsKey(custom_def.getKey())) {
            JSObject custom_def_props = (JSObject)((JSObject) custom_def.getValue()).get("properties");
            JSObject core_def_props = (JSObject)((JSObject) core_defs.get(custom_def.getKey())).get("properties");

            for (Map.Entry<String, Object> custom_prop : custom_def_props.entrySet()) {
              if (!core_def_props.containsKey(custom_prop.getKey())) {
                core_def_props.put(custom_prop.getKey(), custom_prop.getValue());
              }
            }
          }
          else {
            core_defs.put(custom_def.getKey(), custom_def.getValue());
          }
        }
      }
    }
  }

  private static void renameRefs(JSObject jsObj, String namespacePrefix) {
    for (Map.Entry<String, Object> jsEntry : jsObj.entrySet()) {
      Object value = jsEntry.getValue();
      if ("$ref".equals(jsEntry.getKey())) {
        String refName = jsEntry.getValue().toString();
        String typedRefName = refName;
        if (refName.startsWith(DEFINITIONS)) {
          String coreRefName = refName.substring(DEFINITIONS.length());
          typedRefName = DEFINITIONS + namespacePrefix + coreRefName;
        }
        jsObj.put(jsEntry.getKey(), typedRefName);
      }
      else
      if (value instanceof JSObject) {
        renameRefs((JSObject)value, namespacePrefix);
      }
    }
  }

  /***
   * This function adds a prefix to all types referenced and defined within the given type and returnes the new
   * namespaced type of the root object defined within the type. This is necessary so that types do not bleed into
   * one another as is the case with current springfox behaviour.
   * @param type
   * @param namespacePrefix
   * @return The name of the namespaced types of all contained types in the given set of types. E.g. if the type is
   * Report and the prefix is set to 'sarb_' then this will return a map containing 'Report' -> 'sarb_Report'
   */
  public static Map<String, String> namespaceTypes(JSObject type, String namespacePrefix) {
    Map<String, String> typeMap = new HashMap<String, String>();
    if (type != null) {
      JSObject defs = (JSObject) type.get("definitions");
      JSObject new_defs = new JSObject();
      for (Map.Entry<String, Object> def : defs.entrySet()) {
        new_defs.put(namespacePrefix + def.getKey(), def.getValue());

        String typedRefName = namespacePrefix + def.getKey();
        typeMap.put(def.getKey(), typedRefName);
      }
      defs.clear();
      defs.putAll(new_defs);
      renameRefs(type, namespacePrefix);
    }
    return typeMap;
  }

  public static void bindInParamToType(JSObject swagger, String pathname, String verb, String paramName, String type) {
    JSObject paths = (JSObject)swagger.get("paths");
    if (paths != null) {
      JSObject path = (JSObject)paths.get(pathname);
      if (path != null) {
        JSObject method = (JSObject)path.get(verb);
        if (method != null) {
          JSArray params = (JSArray)method.get("parameters");
          if (params != null) {
            for (Object param : params) {
              JSObject jsParam = (JSObject)param;
              if (paramName.equals(jsParam.get("name"))) {
                JSObject schema = (JSObject)jsParam.get("schema");
                if (schema != null) {
                  schema.remove("type");
                  schema.put("$ref", DEFINITIONS + type);
                }
              }
            }
          }
        }
      }
    }
  }

  private static JSObject copy(JSObject from) {
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent(" ");
    writer.setNewline("\n");
    from.compose(writer);
    String fromStr = writer.toString();
    JSStructureParser parser = new JSStructureParser(fromStr);
    try {
      return (JSObject) parser.parse();
    } catch (Exception e) {
      log.error("Cannot copy JSObject", e);
    }
    return from;
  }

  public static void bindResponseToType(JSObject swagger, String pathname, String verb, String type) {
    JSObject paths = (JSObject)swagger.get("paths");
    if (paths != null) {
      JSObject path = (JSObject)paths.get(pathname);
      if (path != null) {
        JSObject method = (JSObject)path.get(verb);
        if (method != null) {
          JSObject responses = (JSObject)method.get("responses");
          if (responses != null) {
            JSObject jsResponse = (JSObject)responses.get("200");
            if (jsResponse != null) {
              JSObject schema = (JSObject)jsResponse.get("schema");
              if (schema != null) {
                schema.remove("type");
                schema.put("$ref", DEFINITIONS + type);
              }
            }
          }
        }
      }
    }
  }
  
  public static String convertToSwaggerMarkup(String javadoc){
    //https://sourceforge.net/p/swaggggg/discussion/markdown_syntax#md_ex_pre
    //preformatted blocks of text...
    String markup = javadoc.replaceAll("</?pre>","\n~~~\n"); //can be enhanced with code-highlighting (see "code highlighting") using specific lexers: http://pygments.org/docs/lexers/
    markup = markup.replaceAll("\\*","\\*");
    markup = markup.replaceAll("<li>\\s*", "* ").replaceAll("</?ul>","\n");
    markup = markup.replaceAll("</?i>","*");
    markup = markup.replaceAll("</?b>","**");
    markup = markup.replaceAll("<br/?>","\r\n");
    markup = markup.replaceAll("<hr/?>","\r\n\r\n* * * * *\r\n\r\n");
    String hashes = "";
    for (int i=1; i<7; i++) {
      hashes+= "#";
      markup = markup.replaceAll("</?h"+i+">", hashes);
    }
    //markup = markup.replaceAll("</h\\d>","\r\n-----\r\n").replaceAll("<h\\d>", "");
    return markup;
  }

  public static void enrichSwaggerDocs(JSObject swagger, Map<String, JavaDocPath> javaDocPathMap) {
    JSObject paths = (JSObject)swagger.get("paths");
    if (paths != null) {
      for (String path : paths.keySet()) {
        JSObject jsPath = (JSObject) paths.get(path);
        if (jsPath != null) {
          for (String method : jsPath.keySet()) {
            JSObject jsMethod = (JSObject) jsPath.get(method);
            JavaDocPath javaDocPath = JavaDocPath.getJavaDocPath(path, method, javaDocPathMap);
            if (javaDocPath != null) {
              if(javaDocPath.getComment() != null && javaDocPath.getComment().length() > 0) {
                jsMethod.put("description", convertToSwaggerMarkup(javaDocPath.getComment()));
              }
              Object parametersObj = jsMethod.get("parameters");
              if (parametersObj instanceof List) {
                List paramList = (List)parametersObj;
                for (Object param : paramList) {
                  if (param instanceof JSObject) {
                    JSObject paramMap = (JSObject)param;
                    String name = (String)paramMap.get("name");
                    JavaDocPath.Parameter jdParam = javaDocPath.getParameterByName(name);
                    if (jdParam != null && jdParam.getComment() != null && jdParam.getComment().length() > 0) {
                      paramMap.put("description", convertToSwaggerMarkup(jdParam.getComment()));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  private static String getMappedSchemaRef(String ref, Map<String,String> typeMap) {
    if (ref != null && ref.startsWith(DEFINITIONS)) {
      String coreRef = ref.substring(DEFINITIONS.length());
      if (typeMap.containsKey(coreRef)) {
        return DEFINITIONS + typeMap.get(coreRef);
      }
    }
    return ref;
  }

  private static void bindMethodInParamToType(JSObject method, Map<String,String> typeMap) {
    if (method != null) {
      JSArray params = (JSArray)method.get("parameters");
      if (params != null) {
        for (Object param : params) {
          JSObject jsParam = (JSObject)param;
          JSObject schema = (JSObject)jsParam.get("schema");
          if (schema != null) {
            if (schema.containsKey("$ref")) {
              schema.put("$ref", getMappedSchemaRef(schema.get("$ref").toString(), typeMap));
            }
            else
            if (schema.containsKey("type")) {
              String typeName = schema.get("type").toString();
              if ("array".equals(typeName)) {
                JSObject items = (JSObject) schema.get("items");
                if (items != null && items.containsKey("$ref")) {
                  items.put("$ref", getMappedSchemaRef(items.get("$ref").toString(), typeMap));
                }
              }
            }
          }
        }
      }
    }
  }

  private static void bindResponseToType(JSObject method, Map<String,String> typeMap) {
    if (method != null) {
      JSObject responses = (JSObject)method.get("responses");
      if (responses != null) {
        JSObject jsResponse = (JSObject)responses.get("200");
        if (jsResponse != null) {
          JSObject schema = (JSObject)jsResponse.get("schema");
          if (schema != null) {
            if (schema.containsKey("$ref")) {
              schema.put("$ref", getMappedSchemaRef(schema.get("$ref").toString(), typeMap));
            }
            else
            if (schema.containsKey("type")) {
              String typeName = schema.get("type").toString();
              if ("array".equals(typeName)) {
                JSObject items = (JSObject) schema.get("items");
                if (items != null && items.containsKey("$ref")) {
                  items.put("$ref", getMappedSchemaRef(items.get("$ref").toString(), typeMap));
                }
              }
            }
          }
        }
      }
    }
  }

  public static void extendCallBySchema(JSObject swagger, String pathname, String verb, Map<String,Map<String,String>> schemaTypeMap, boolean removeOriginalPath) {
    if (pathname.contains("{schema}")) {
      JSObject paths = (JSObject)swagger.get("paths");
      if (paths != null) {
        JSObject path = (JSObject) paths.get(pathname);
        if (path != null) {
          JSObject method = (JSObject) path.get(verb);
          if (method != null) {
            if (removeOriginalPath) {
              path.remove(verb);
              if (path.size() == 0) {
                paths.remove(pathname);
              }
            }

            for (Map.Entry<String,Map<String,String>> schemaType : schemaTypeMap.entrySet()) {
              JSObject schemaMethod = copy(method);

              // Remove schema as one of the input parameters
              JSArray new_params = new JSArray();
              JSArray params = (JSArray)schemaMethod.get("parameters");
              if (params != null) {
                for (Object param : params) {
                  JSObject jsParam = (JSObject) param;
                  if (!"schema".equals(jsParam.get("name"))) {
                    new_params.add(jsParam);
                  }
                }
                if (new_params.size() > 0) {
                  schemaMethod.put("parameters", new_params);
                }
                else {
                  schemaMethod.remove("parameters");
                }
              }

              // Bind the correct type to the input parameters or responses
              bindMethodInParamToType(schemaMethod, schemaType.getValue());
              bindResponseToType(schemaMethod, schemaType.getValue());

              // Insert the new paths and methods
              String schemaPathName = pathname.replace("{schema}", schemaType.getKey());
              JSObject schemaPath;
              if (paths.containsKey(schemaPathName)) {
                schemaPath = (JSObject)paths.get(schemaPathName);
                schemaPath.put(verb, schemaMethod);
              }
              else {
                schemaPath = new JSObject();
                schemaPath.put(verb, schemaMethod);
                paths.put(schemaPathName, schemaPath);
              }
            }
          }
        }
      }
    }
  }
}
