package za.co.synthesis.regulatory.report.persist.common;

import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.persist.TableConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 8/21/17.
 */
public class TableDefinition {

  public static enum FieldType {
    UUID,
    SyncPoint,
    DateTime,
    String,
    Integer,
    JsonMap
  }

  public static enum FieldComparator {
    Equals,
    GreaterThan,
    GreaterThanOrEquals,
    LessThan,
    LessThanOrEquals,
    NotEquals,
    Between,
    NotBetween,
    In,
    NotIn,
    Like,
    NotLike,
    IsNull,
    IsNotNull
  }

  public static class Field {
    private final String name;
    private final FieldType type;

    public Field(String name, FieldType type) {
      this.name = name;
      this.type = type;
    }

    public String getName() {
      return name;
    }

    public FieldType getType() {
      return type;
    }
  }

  public static class Criterion {
    private final Field field;
    private final FieldComparator comparator;
    private final List<String> values;

    public Criterion(Field field, FieldComparator comparator, String value) {
      List<String> values = new ArrayList<>();
      values.add(value);
      this.field = field;
      this.comparator = comparator;
      this.values = values;
    }

    public Criterion(Field field, FieldComparator comparator, List<String> values) {
      this.field = field;
      this.comparator = comparator;
      this.values = values;
    }

    public Field getField() {
      return field;
    }

    public FieldComparator getComparator() {
      return comparator;
    }

    public List<String> getValues() {
      return values;
    }
  }

  private String tablename;
  private final List<Field> fields = new ArrayList<Field>();
  private String indexTablename;
  private final List<Field> indexFields = new ArrayList<Field>();
  private String fullTextTablename;
  private final List<Field> fullTextFields = new ArrayList<Field>();

  public boolean supportsFullTextSearch() {
    return fullTextSearchSupported;
  }

  public void setFullTextSearchSupported(boolean fullTextSearchSupported) {
    this.fullTextSearchSupported = fullTextSearchSupported;
  }

  private boolean fullTextSearchSupported = false;

  public TableDefinition(String tablename) {
    this.tablename = tablename;
  }

  public String getTablename() {
    return tablename;
  }


  public String getIndexTablename() {
    return indexTablename;
  }

  public void setIndexTablename(String indexTablename) {
    this.indexTablename = indexTablename;
  }

  public List<Field> getIndexFields() {
    return indexFields;
  }


  public String getFullTextTablename() {
    return fullTextTablename;
  }

  public void setFullTextTablename(String fullTextTablename) {
    this.fullTextTablename = fullTextTablename;
  }

  public List<Field> getFullTextFields() {
    return fullTextFields;
  }

  public List<Field> getFields() {
    return fields;
  }

  public String getIndexUUIDFieldName() {
    for (Field f : indexFields) {
      if (f.getType() == FieldType.UUID)
        return f.getName();
    }
    return null;
  }

  public String getIndexTypeFieldName() {
    return FieldConstant.IndexType;
  }

  public String getIndexFieldFieldName() {
    return FieldConstant.IndexField;
  }

  public String getIndexValueFieldName() {
    return FieldConstant.IndexValue;
  }

  public String getUUIDFieldName() {
    for (Field f : fields) {
      if (f.getType() == FieldType.UUID)
        return f.getName();
    }
    return null;
  }

  public String getSyncpointFieldName() {
    for (Field f : fields) {
      if (f.getType() == FieldType.SyncPoint)
        return f.getName();
    }
    return null;
  }


  public void populateLessCommonFields() {
    populateLessCommonFields(this);
  }

  public void populateCommonFields() {
    populateCommonFields(this);
  }

  public void populateCommonConfigFields() {
    populateCommonConfigFields(this);
  }

  public void addIndexField(String fieldConstant, TableDefinition.FieldType fieldType) {
    getIndexFields().add(new TableDefinition.Field(fieldConstant, fieldType));
  }

  public static void addIndexField(TableDefinition tableDefinition, String fieldConstant, TableDefinition.FieldType fieldType) {
    tableDefinition.getIndexFields().add(new TableDefinition.Field(fieldConstant, fieldType));
  }

  public static void addFullTextField(TableDefinition tableDefinition, String fieldConstant, TableDefinition.FieldType fieldType) {
    tableDefinition.getFullTextFields().add(new TableDefinition.Field(fieldConstant, fieldType));
  }

  public void addField(String fieldConstant, TableDefinition.FieldType fieldType) {
    getFields().add(new TableDefinition.Field(fieldConstant, fieldType));
  }

  public static void addField(TableDefinition tableDefinition, String fieldConstant, TableDefinition.FieldType fieldType) {
    tableDefinition.getFields().add(new TableDefinition.Field(fieldConstant, fieldType));
  }

  public static void populateLessCommonFields(TableDefinition tableDefinition) {
    addField(tableDefinition, FieldConstant.GUID, FieldType.UUID);
    addField(tableDefinition, FieldConstant.TStamp, FieldType.SyncPoint);
    addField(tableDefinition, FieldConstant.ReportSpace, FieldType.String);
  }

  public static void populateCommonFields(TableDefinition tableDefinition) {
    addField(tableDefinition, FieldConstant.GUID, FieldType.UUID);
    addField(tableDefinition, FieldConstant.TStamp, FieldType.SyncPoint);
    addField(tableDefinition, FieldConstant.ReportSpace, FieldType.String);
    addField(tableDefinition, FieldConstant.ReportKey, FieldType.String);
  }

  public static void populateCommonConfigFields(TableDefinition tableDefinition) {
    addField(tableDefinition, FieldConstant.GUID, FieldType.UUID);
    addField(tableDefinition, FieldConstant.Key, FieldType.String);
    addField(tableDefinition, FieldConstant.TStamp, FieldType.SyncPoint);
    addField(tableDefinition, FieldConstant.Version, FieldType.Integer);
    addField(tableDefinition, FieldConstant.Content, FieldType.String);
  }

  //---------------------------------------------------------------------------------------------------//
  //-- COMMON TABLE DEFINITIONS                                                                      --//
  //---------------------------------------------------------------------------------------------------//
  public static TableDefinition getLessCommonDefinition(String name) {
    assert (name != null && !name.isEmpty());
    TableDefinition tableDefinition = new TableDefinition(name);
    tableDefinition.populateLessCommonFields();
    return tableDefinition;
  }

  public static TableDefinition getCommonDefinition(String name) {
    assert (name != null && !name.isEmpty());
    TableDefinition tableDefinition = new TableDefinition(name);
    tableDefinition.populateCommonFields();
    return tableDefinition;
  }


  public static TableDefinition getCommonConfigDefinition(String name) {
    assert (name != null && !name.isEmpty());
    TableDefinition tableDefinition = new TableDefinition(name);
    tableDefinition.populateCommonConfigFields();
    return tableDefinition;
  }

  //---------------------------------------------------------------------------------------------------//
  //-- CONTENT TABLE DEFINITIONS                                                                     --//
  //---------------------------------------------------------------------------------------------------//
  //TODO: This COULD perhaps be more dynamic, IE: generated from configs -- [{name, [field(name, type)]}]
  public static TableDefinition getReportTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.Report;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.setFullTextSearchSupported(true); //TODO: This should be driven by the underlying implementation - not just set as true!!
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField(FieldConstant.Schema, FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    //FullText Index Definition...
    tableDefinition.setFullTextTablename(tableDefinition.tablename+"FullText");
    addFullTextField(tableDefinition, FieldConstant.ReportSpace, FieldType.String);
    addFullTextField(tableDefinition, FieldConstant.ReportKey, FieldType.String);
    addFullTextField(tableDefinition, "ReportGuid", FieldType.UUID);
    addFullTextField(tableDefinition, "FullText", FieldType.String);
    //Index Definition...
    tableDefinition.setIndexTablename(tableDefinition.tablename+"Index");
    addIndexField(tableDefinition, FieldConstant.ReportSpace, FieldType.String);
    addIndexField(tableDefinition, FieldConstant.ReportKey, FieldType.String);
    addIndexField(tableDefinition, "ReportGuid", FieldType.UUID);
    addIndexField(tableDefinition, FieldConstant.IndexType, FieldType.String);
    addIndexField(tableDefinition, FieldConstant.IndexField, FieldType.String);
    addIndexField(tableDefinition, FieldConstant.IndexValue, FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getChecksumTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.Checksum;
    TableDefinition tableDefinition = getLessCommonDefinition(name);
    tableDefinition.addField(FieldConstant.ValueDate, FieldType.String);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    tableDefinition.addField("DataContent", FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDocumentTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.Document;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField("DocumentHandle", FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    tableDefinition.addField("DataContent", FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDocumentAckTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.DocumentAck;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField("DocumentHandle", FieldType.String);
    tableDefinition.addField("Acknowledged", FieldType.String);
    tableDefinition.addField("AcknowledgedComment", FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDecisionTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.Decision;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDecisionLogTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.DecisionLog;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getAccountEntryTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.AccountEntry;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    tableDefinition.addField(FieldConstant.Version, FieldType.Integer);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    tableDefinition.addField("DataContent", FieldType.String);
    return tableDefinition;
  }

  //---------------------------------------------------------------------------------------------------//
  //-- NOTIFICATION TABLE DEFINITIONS                                                                     --//
  //---------------------------------------------------------------------------------------------------//
  public static TableDefinition getNotificationTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.Notification;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField(FieldConstant.Content, FieldType.String);
    tableDefinition.addField(FieldConstant.QueueName, FieldType.String);
    return tableDefinition;
  }

  //---------------------------------------------------------------------------------------------------//
  //-- DOWNLOAD TABLE DEFINITIONS                                                                    --//
  //---------------------------------------------------------------------------------------------------//
  public static TableDefinition getReportDownloadTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.ReportDownload;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField("ReportGuid", FieldType.UUID);
    tableDefinition.addField("ReportDownloadType", FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDocumentDownloadTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.DocumentDownload;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField("DocumentHandle", TableDefinition.FieldType.String);
    tableDefinition.addField("DocumentGuid", FieldType.UUID);
    return tableDefinition;
  }

  public static TableDefinition getChecksumDownloadTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.ChecksumDownload;
    TableDefinition tableDefinition = getLessCommonDefinition(name);
    tableDefinition.addField("ValueDate", TableDefinition.FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getDecisionDownloadTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.DecisionDownload;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField("DecisionGuid", FieldType.UUID);
    return tableDefinition;
  }

  public static TableDefinition getAccountEntryDownloadTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.AccountEntryDownload;
    TableDefinition tableDefinition = getCommonDefinition(name);
    tableDefinition.addField("AccountEntryGuid", FieldType.UUID);
    return tableDefinition;
  }

  //---------------------------------------------------------------------------------------------------//
  //-- CONFIG TABLE DEFINITIONS                                                                      --//
  //---------------------------------------------------------------------------------------------------//
  public static TableDefinition getUserTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.User;
    TableDefinition tableDefinition = getCommonConfigDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getAccessSetTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.AccessSet;
    TableDefinition tableDefinition = getCommonConfigDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    return tableDefinition;
  }

  public static TableDefinition getListTableDefinition(String name) {
    if (name == null || name.isEmpty())
      name = TableConstant.List;
    TableDefinition tableDefinition = getCommonConfigDefinition(name);
    tableDefinition.addField(FieldConstant.AuthToken, FieldType.String);
    return tableDefinition;
  }

  //---------------------------------------------------------------------------------------------------//
  //-- COMPLETE TABLE DEFINITION LIST                                                                --//
  //---------------------------------------------------------------------------------------------------//
  public static List<TableDefinition> getCommonTableDefinitionList() {
    List<TableDefinition> tables = new ArrayList<>();
    tables.add(getReportTableDefinition(null));
    tables.add(getChecksumTableDefinition(null));
    tables.add(getDocumentTableDefinition(null));
    tables.add(getDocumentAckTableDefinition(null));
    tables.add(getDecisionTableDefinition(null));
    tables.add(getDecisionLogTableDefinition(null));
    tables.add(getAccountEntryTableDefinition(null));

    tables.add(getNotificationTableDefinition(null));

    tables.add(getReportDownloadTableDefinition(null));
    tables.add(getDocumentDownloadTableDefinition(null));
    tables.add(getChecksumDownloadTableDefinition(null));
    tables.add(getDecisionDownloadTableDefinition(null));
    tables.add(getAccountEntryDownloadTableDefinition(null));

    tables.add(getUserTableDefinition(null));
    tables.add(getAccessSetTableDefinition(null));
    tables.add(getListTableDefinition(null));
    return tables;
  }

  public static Map<String, TableDefinition> getCommonTableDefinitionMap() {
    List<TableDefinition> tables = getCommonTableDefinitionList();
    Map<String, TableDefinition> tableMap = new HashMap<>();
    for (TableDefinition table : tables) {
      tableMap.put(table.getTablename(), table);
    }
    return tableMap;
  }

}
