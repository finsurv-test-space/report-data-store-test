package za.co.synthesis.regulatory.report.support;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by james on 2017/07/25.
 */
public enum EventType {
    NewReportOnChannel("NewReportOnChannel"),
    NewReportForReportSpace("NewReportForReportSpace"),
    UpdatedReportOnChannel("UpdatedReportOnChannel"),
    UpdatedReportForReportSpace("UpdatedReportForReportSpace"),
    ReportValidationSuccess("ReportValidationSuccess"),
    ReportValidationFailure("ReportValidationFailure"),
    DocumentsUploadValidationSuccess("DocumentsUploadValidationSuccess"),
    DocumentsUploadValidationFailure("DocumentsUploadValidationFailure"),
    DocumentsAcknowledgedValidationSuccess("DocumentsAcknowledgedValidationSuccess"),
    DocumentsAcknowledgedValidationFailure("DocumentsAcknowledgedValidationFailure"),
    UserAction("UserAction");

    private String value;

    EventType(String value){
        this.value = value;
    }

    @JsonValue
    public String toString(){
        return value;
    }

    @JsonCreator
    public static EventType fromString(String stringEventType) {
        if(stringEventType != null) {
            for(EventType eventType : EventType.values()) {
                if (stringEventType.equalsIgnoreCase(eventType.value)) {
                    return eventType;
                }
            }
        }
        return null;
    }
}
