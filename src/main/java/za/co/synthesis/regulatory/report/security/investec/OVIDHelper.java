package za.co.synthesis.regulatory.report.security.investec;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationServiceException;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.regulatory.report.persist.ProxyCallData;
import za.co.synthesis.regulatory.report.support.ProxyCaller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * This class calls and parses the Saleslogix web service response
 * to determine the OVIDs from a given GCN.
 * Created by Marais Neethling on 2017/10/12.
 */

public class  OVIDHelper {

  private static final Logger logger = LoggerFactory.getLogger(OVIDHelper.class);

  private ProxyCaller proxy;

  @Required
  public void setProxy(ProxyCaller proxy) {
    this.proxy = proxy;
  }

  /*****************
   * Internal cache for keeping saleslogix call results
   */
  LoadingCache<String, String> cache = Caffeine.newBuilder()
          .expireAfterWrite(5, TimeUnit.MINUTES)
          .maximumSize(10_000)
          .build(gcn -> callSaleslogix(gcn));


  /**
   *
   * @param gcn
   * @return
   */
  public String getOVIDfromSaleslogix(String gcn) {
    try {
      return cache.get(gcn);
    } catch (Exception e) {
      throw new AuthenticationServiceException("Error in resolving GCN to OneViewIDs.", e);
    }
  }

  /**
   * This method parses  json object hat is returned by the Investec
   * Saleslogix GetRolesByGCNSystemName web service call. The json
   * object has the following structure.
   *
   * @param jsob
   * @return
   */

  public List<String> getOVIDSfromJSON(JSObject jsob) throws IOException {
    List<String> result = new ArrayList<>();
    List<JSObject> companies = new ArrayList<>();
    JSUtil.findObjectsWith(jsob,"ONEVIEWID", companies);
    if (companies.size() > 0) {
      for (JSObject company : companies) {
        Object systems = company.get("SYSTEMS");
        if (systems instanceof JSObject) {
          String name = (String) ((JSObject)systems).get("SYSTEMNAME");
          if ("BMS".equalsIgnoreCase(name)) // we can take the OneView ID if the GCN has access to BMS
            result.add((String) company.get("ONEVIEWID"));
        }
      }
    }
    return result;
  }

  /**
   * Makes a REST call to Saleslogix and returns a comma delimited string of OneViewIDs
   * if available for the provided GCN.
   *
   * @param gcn
   * @return
   * @throws Exception
   */
  private String callSaleslogix(String gcn) throws Exception {
    ProxyCallData proxyCallData = proxy.getProxyCallData("resolve_ovid_from_gcn");
    if (proxyCallData != null) {
      Map<String, Object> parameters = new HashMap<String, Object>();
      parameters.put("gcn", gcn);
      Object response = proxy.call(proxyCallData, parameters);
      if (response instanceof JSObject) {
        List<String> ovids = getOVIDSfromJSON((JSObject) response);
        String result = StringUtils.join(ovids,",");
        logger.info("GCN {} has OVIDs [{}]", gcn, result);
        return result;
      }
    }
    return null;
  }

}
