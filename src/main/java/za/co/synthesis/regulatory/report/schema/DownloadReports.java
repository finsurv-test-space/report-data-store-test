package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 6/8/17.
 */
@JsonPropertyOrder({
        "SyncPoint"
        , "Reports"
})
public class DownloadReports {
  private String syncPoint;
  private List<ReportDataWithDownloadType> reports;

  public DownloadReports(String syncPoint, List<ReportDataWithDownloadType> reports) {
    this.syncPoint = syncPoint;
    this.reports = reports;
  }

  @JsonProperty("SyncPoint")
  public String getSyncPoint() {
    return syncPoint;
  }

  @JsonProperty("Reports")
  public List<ReportDataWithDownloadType> getReports() {
    return reports;
  }
}
