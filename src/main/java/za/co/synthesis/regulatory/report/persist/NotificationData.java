package za.co.synthesis.regulatory.report.persist;

public class NotificationData {
  private String guid;
  private String reportSpace;
  private String trnReference;
  private String queueName;
  private String content;

  public NotificationData(String reportSpace, String trnReference, String queueName, String content) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.queueName = queueName;
    this.content = content;
  }

  public NotificationData(String guid, String reportSpace, String trnReference, String queueName, String content) {
    this.guid = guid;
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.queueName = queueName;
    this.content = content;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getTrnReference() {
    return trnReference;
  }

  public void setTrnReference(String trnReference) {
    this.trnReference = trnReference;
  }

  public String getQueueName() {
    return queueName;
  }

  public void setQueueName(String queueName) {
    this.queueName = queueName;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
