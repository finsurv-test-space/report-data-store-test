package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.history.HistoryUtils;

import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
        "Report"
        , "Changes"
})
public class ReportDataHistoryItem {
  public final ReportDataWithState report;
  public Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> diffWithPrev;

  public ReportDataHistoryItem(ReportDataWithState report) {
    this.report = report;
  }

  @JsonProperty("Changes")
  public Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> getDiffWithPrev() {
    return diffWithPrev;
  }

  public void setDiffWithPrev(Map<HistoryUtils.ReportDataSection, Map<HistoryUtils.DiffActionType, List<Object>>> diffWithPrev) {
    this.diffWithPrev = diffWithPrev;
  }

  @JsonProperty("Report")
  public ReportDataWithState getReport() {
    return report;
  }
}


