package za.co.synthesis.regulatory.report.common;

import java.util.Comparator;

/**
 * Created by james on 2017/05/26.
 */
public class SyncComparator implements Comparator {
    public int compare(Long o1, Long o2) {
        return o1.compareTo(o2);
    }

    public int compare(long o1, long o2) {
        return (o2>o1?-1:(o2<o1?1:0));
    }

    public int compare(Long o1, long o2) {
        return compare(o1, new Long(o2));
    }

    public int compare(long o1, Long o2) {
        return compare(new Long(o1), o2);
    }

    public int compare(String o1, String o2) {
        Long l1 = null;
        Long l2 = null;
        try{
            l1.parseLong(o1);
            l2.parseLong(o2);
            return compare(l1, l2);
        } catch(Exception e){}
        return o1.compareTo(o2);
    }

    public int compare(String o1, Long o2) {
        Long l1 = null;
        try{
            l1.parseLong(o1);
            return compare(l1, o2);
        } catch(Exception e){}
        return (o1!=null?o1:"").compareTo(o2!=null?o2.toString():"");
    }

    public int compare(Long o1, String o2) {
        Long l2 = null;
        try{
            l2.parseLong(o2);
            return compare(o1, l2);
        } catch(Exception e){}
        return (o1!=null?o1.toString():"").compareTo(o2!=null?o2:"");
    }

    @Override
    public int compare(Object o1, Object o2) {
        if (o1.getClass().equals(Long.class)) {
            return ((Long)o2).compareTo(((Long)o1));
        }
        //throw new Exception("Unknown type comparison ("+o1.getClass().getName()+" vs "+o2.getClass().getName()+")");
        return 0;
    }
}
