package za.co.synthesis.regulatory.report.validate;

import za.co.synthesis.rule.core.ILookups;

import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:26 PM
 * Test implementation of the lookups. This needs to be specialised if the testing data is more limiting than the
 * equivalent SARB data used by default
 */
public class LookupsTest implements ILookups {

  @Override
  public Boolean isValidCountryCode(String code) {
    return null;
  }

  @Override
  public Boolean isValidCurrencyCode(String code) {
    return null;
  }

  @Override
  public Boolean isValidCategory(String flow, String code) {
    return null;
  }

  @Override
  public Boolean isValidSubCategory(String flow, String code, String subcode) {
    return null;
  }

  @Override
  public Boolean isReportingQualifier(String value) {
    return null;
  }

  @Override
  public Boolean isNonResExceptionName(String value) {
    return null;
  }

  @Override
  public Boolean isResExceptionName(String value) {
    return null;
  }

  @Override
  public Boolean isMoneyTransferAgent(String value) {
    return null;
  }

  @Override
  public Boolean isValidProvince(String value) {
    return null;
  }

  @Override
  public Boolean isValidCardType(String value) {
    return null;
  }

  @Override
  public Boolean isValidInstitutionalSector(String value) {
    return null;
  }

  @Override
  public Boolean isValidIndustrialClassification(String value) {
    return null;
  }

  @Override
  public Boolean isValidCustomsOfficeCode(String value) {
    return null;
  }

  @Override
  public String getLookupField(String lookup, Map<String, String> key, String fieldName) {
    return null;
  }
}
