package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.UserData;
import za.co.synthesis.regulatory.report.security.SecurityHelper;


public class InternalAuthModule implements IJWTAuthModule {
  public static class InternalAuthException extends AuthenticationException {

    public InternalAuthException(String msg) {
      super(msg);
    }
  }

  private UserInformation userInformation;
  

  private AuthenticationCache authCache = new AuthenticationCache(1000, 120000, 7200000);
  
  @Override
  public boolean isAvailable() {
    return (userInformation != null);
  }

  @Required
  public void setUserInformation(UserInformation userInformation) {
    this.userInformation = userInformation;
  }

  @Override
  public String getName() {
    return "internal";
  }

  private UserData authAndGetInternalUser(String username, String password) {
    UserData data = userInformation.getUser(username);
    if (data != null && (data.getActive() == null || data.getActive())) {
      long currentTime = System.currentTimeMillis();
      //TODO: WHEN UPDATING A USER IN THE SYSTEM, INVALIDATE THE CACHE FOR THE USER
      //TODO: CHECK THAT ACTIVE STATUS IS RETURNED FOR WHOAMI
      //TODO: WHEN AUTHENTICATING A USER (BELOW) CHECK THE ACTIVE STATUS FIRST.  IF INACTIVE, FAIL THE AUTH.
      AuthenticationCache.CachedLogin loginStatus = authCache.checkCacheWithPassword(currentTime, username, password);
      if (loginStatus == AuthenticationCache.CachedLogin.CacheCanBeUsed) {
        return data;
      }
      if (password != null && SecurityHelper.matchPasswordHash(password, data.getPassword())) {
        authCache.onAuthenticate(currentTime, username, password, null);
        return data;
      }

      //      if (password != null && password.equals(data.getPassword())) {
//        return data;
//      }

    }
    return null;
  }

  @Override
  public String authAndComposeJWTIdToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    UserData userData = authAndGetInternalUser(username, password);
    if (userData != null) {
      return provider.produceJWTID(userData.getUsername(),  getName(),null);
    }
    else {
      throw new InternalAuthException("Cannot authenticate '" + username + "' against internal credentials");
    }
  }

  @Override
  public String composeJWTAccessTokenForAuthenticatedUser(JWTProvider provider, String username) throws AuthenticationException {
    UserData userData = userInformation.getUser(username);
    if (userData != null) {
      return provider.produceJWTAccess(userData.getUsername(), userData.getAccess(), userData.getRights(), null);
    }
    else {
      throw new InternalAuthException("Cannot find '" + username + "' in the internal store");
    }
  }

  @Override
  public String authAndComposeJWTAccessToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    UserData userData = authAndGetInternalUser(username, password);
    if (userData != null) {
      return provider.produceJWTAccess(userData.getUsername(), userData.getAccess(), userData.getRights(), null);
    }
    else {
      throw new InternalAuthException("Cannot authenticate '" + username + "' against internal credentials");
    }
  }

  @Override
  public JWTAuthToken authAndGenerateAccessToken(String username, String password) {
    UserData userData = authAndGetInternalUser(username, password);
    if (userData == null || !userData.getActive())
    {
      JWTAuthToken jwtAuthToken = new JWTAuthToken(username, null,null, null, JWTAuthToken.TokenUse.ACCESS);
      jwtAuthToken.setAuthenticated(false);
      return jwtAuthToken;
    }
    String[] rightsArray = new String[userData.getRights().size()];
    userData.getRights().toArray(rightsArray);

    JWTAuthToken jwtAuthToken = new JWTAuthToken((String)username, userData.getAccess(), null, AuthorityUtils.createAuthorityList(rightsArray), JWTAuthToken.TokenUse.ACCESS);
    jwtAuthToken.setAuthenticated(true);
    return jwtAuthToken;
  }
  
  
  public void setMaxCacheSize(int maxCacheSize) {
    this.authCache.setMaxCacheSize(maxCacheSize);
  }
  
  public void setNocheckMillis(long nocheckMillis){
    this.authCache.setNocheckMillis(nocheckMillis);
  }
  
  public void setExpiryMillis(long expiryMillis){
    this.authCache.setExpiryMillis(expiryMillis);
  }
  
}
