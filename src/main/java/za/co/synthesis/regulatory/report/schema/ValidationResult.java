package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by jake on 6/12/17.
 */
@JsonPropertyOrder({
        //"ReportSpace",  /*TODO: ReportSpace should supercede the ReportingSpace field below*/
        "ReportingSpace" /*DEPRECATED NAMING CONVENTION*/
        , "TrnReference"
        , "ResultType"
        , "ValidationTime"
        , "Validations"
})
public class ValidationResult {
  private final String reportSpace;
  private final String trnReference;
  private LocalDateTime validationTime;
  private ValidationResultType resultType;
  private List<ValidationResponse> validations;

  public ValidationResult(String reportSpace, String trnReference, ValidationResultType resultType, List<ValidationResponse> validations) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.resultType = resultType;
    this.validations = validations;
  }

  public ValidationResult(String reportSpace, String trnReference, ValidationResultType resultType) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.resultType = resultType;
  }

  public ValidationResult(String reportSpace, String trnReference) {
    this.reportSpace = reportSpace;
    this.trnReference = trnReference;
    this.resultType = ValidationResultType.Unspecified;
  }

  //@JsonProperty("ReportSpace")
  @JsonIgnore //TODO: This shouldn't be ignored!
  public String getReportSpace() {
    return reportSpace;
  }

  @Deprecated
  @JsonProperty("ReportingSpace")
  public String getReportingSpace() {
    return reportSpace;
  }

  @JsonProperty("TrnReference")
  public String getTrnReference() {
    return trnReference;
  }

  @JsonProperty("ResultType")
  public ValidationResultType getResultType() {
    return resultType;
  }

  public void setResultType(ValidationResultType resultType) {
    this.resultType = resultType;
  }

  @JsonProperty("Validations")
  public List<ValidationResponse> getValidations() {
    return validations;
  }

  public void setValidations(List<ValidationResponse> validations) {
    this.validations = validations;
  }

  @JsonProperty("ValidationTime")
  public LocalDateTime getValidationTime() {
    return validationTime;
  }

  public void setValidationTime(LocalDateTime validationTime) {
    this.validationTime = validationTime;
  }
}
