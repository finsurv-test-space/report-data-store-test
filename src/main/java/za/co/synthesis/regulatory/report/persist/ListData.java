package za.co.synthesis.regulatory.report.persist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 6/27/17.
 */
public class ListData {
  public static class Field {
    private String label;
    private CriterionType criterionType;
    private String name;

    public Field(String label, CriterionType criterionType, String name) {
      this.label = label;
      this.criterionType = criterionType;
      this.name = name;
    }

    public String getLabel() {
      return label;
    }

    public CriterionType getCriterionType() {
      return criterionType;
    }

    public String getName() {
      return name;
    }
  }

  private final String name;
  private final List<Field> fields = new ArrayList<Field>();
  private final List<CriterionData> criteria = new ArrayList<CriterionData>();
  private final List<String> listingExclusionFilter = new ArrayList<String>();
  private final List<String> listingFilter = new ArrayList<String>();

  public ListData(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public List<Field> getFields() {
    return fields;
  }

  public List<CriterionData> getCriteria() {
    return criteria;
  }
  
  public List<String> getListingExclusionFilter() {
    return listingExclusionFilter;
  }
  
  public List<String> getListingFilter() {
    return listingFilter;
  }
}
