package za.co.synthesis.regulatory.report.persist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;

/**
 * Created by jake on 5/29/17.
 */
@JsonPropertyOrder({
        "FileName"
        ,"FileType"
        ,"Content"
})
public class FileData {
  private final String contentType;
  private final String filename;
  private final byte[] content;

  public FileData(String contentType, String filename, byte[] content) {
    this.contentType = contentType;
    this.filename = filename;
    this.content = content;
  }

  @JsonProperty("FileType")
  public String getContentType() {
    return contentType;
  }

  @JsonProperty("FileName")
  public String getFilename() {
    return filename;
  }

  @JsonIgnore
  public byte[] getContent() {
    return content;
  }

  @JsonProperty("Content")
  public String getFileContent() {
    return new String(DataLogic.Base64Encode(content));
  }
}
