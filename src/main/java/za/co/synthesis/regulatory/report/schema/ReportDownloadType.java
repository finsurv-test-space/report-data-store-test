package za.co.synthesis.regulatory.report.schema;

public enum ReportDownloadType {
  Update("Update"),
  Cancel("Cancel"),
  CancelWithReplace("CancelWithReplace");

  private String name;

  ReportDownloadType(String name){
    this.name = name;
  }

  public static ReportDownloadType fromString(String description){
    if (description != null) {
      if (Cancel.name.equalsIgnoreCase(description)) {
        return Cancel;
      } else if (CancelWithReplace.name.equalsIgnoreCase(description)) {
        return CancelWithReplace;
      }
    }
    return Update;
  }

  public String toString(){
    return this.name;
  }
}
