package za.co.synthesis.regulatory.report.support;

import java.util.Map;

/**
 * Created by james on 2017/08/11.
 */
public class ProbeEntry implements Map.Entry{

  private final String key;
  private final String alias;
  private Object value;

  public ProbeEntry(String key, String alias) {
    this(key, alias, null);
  }

  public ProbeEntry(String key, String alias, Object value) {
    this.key = key;
    this.alias = alias;
    this.value = value;
  }

  @Override
  public String getKey() {
    return this.key;
  }

  public String getAlias() {
    return this.alias;
  }

  @Override
  public Object getValue() {
    return this.value;
  }

  @Override
  public Object setValue(Object value) {
    this.value = value;
    return this.value;
  }
}
