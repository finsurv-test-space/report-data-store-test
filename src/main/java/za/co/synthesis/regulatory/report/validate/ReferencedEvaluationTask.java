package za.co.synthesis.regulatory.report.validate;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import za.co.synthesis.regulatory.report.rest.Upload;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.support.MapFinsurvContext;
import za.co.synthesis.rule.core.Evaluator;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ValidationStats;
import za.co.synthesis.rule.core.Validator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by jake on 6/13/17.
 */
public class ReferencedEvaluationTask implements Callable<EvaluationDecisions> {
  final static Log log = LogFactory.getLog(ReferencedEvaluationTask.class);

  private final Evaluator evaluator;
  private final EvaluationRequest evaluationRequest;
  private final Boolean relevantResultsOnly;

  public ReferencedEvaluationTask(Evaluator evaluator, EvaluationRequest evaluationRequest, Boolean relevantResultsOnly) {
    this.evaluator = evaluator;
    this.evaluationRequest = evaluationRequest;
    this.relevantResultsOnly = relevantResultsOnly;
  }

  @Override
  public EvaluationDecisions call() throws Exception {
    // NOTE: THIS CODE MUST BE WELL BEHAVED and follow the following rules:
    // 1. All selects from the database should be with NO LOCKING since we will be running at the same time as
    // other processes responsible for updating the database and DB deadlocks could then result.
    // 2. NO UPDATING OF THE DATABASE AT ALL!!!!!!!!
    return Upload.evaluateAnyScenario(evaluator, evaluationRequest, relevantResultsOnly);
  }
}
