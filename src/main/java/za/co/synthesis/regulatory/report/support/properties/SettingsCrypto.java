package za.co.synthesis.regulatory.report.support.properties;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jake on 3/17/17.
 */
public class SettingsCrypto {
  private static final Logger logger = LoggerFactory.getLogger(SettingsCrypto.class);

  private Charset charset = Charset.forName("UTF-8");
  private Cipher cipher;
  private SecretKeySpec key;
  private IvParameterSpec ivSpec;

  public static SettingsCrypto Standard() {
    SettingsCrypto crypto = new SettingsCrypto();
    try {
      crypto.initialise("H!D5js3d", "J*&6f4%@");
    }
    catch(Exception e) {
      crypto.logger.warn("Cannot setup Standard crypto", e);
    }

    return crypto;
  }

  public static SettingsCrypto UserPassword(String cryptoKey) {
    SettingsCrypto crypto = new SettingsCrypto();
    try {
      crypto.initialise(cryptoKey, "K*&6f3%@");
    }
    catch(Exception e) {
      crypto.logger.warn("Cannot setup UserPassword crypto", e);
    }

    return crypto;
  }

  public static SettingsCrypto NonStandard(String cryptoKey, String ivSpecKey) {
    SettingsCrypto crypto = new SettingsCrypto();
    try {
      crypto.initialise(cryptoKey, ivSpecKey);
    }
    catch(Exception e) {
      crypto.logger.warn("Cannot setup NonStandard crypto", e);
    }

    return crypto;
  }

  private byte[] getNBytes(int n, byte[] seed) {
    byte[] result = new byte[n];
    int j = 0;
    for (int i=0; i<n; i++) {
      result[i] = seed[j++];
      if (j >= seed.length)
        j = 0;
    }
    return result;
  }

  private void initialise(String cryptoKey, String ivSpecKey) throws NoSuchPaddingException, NoSuchAlgorithmException {
    // wrap key data in Key/IV specs to pass to cipher
    byte[] keyBytes = cryptoKey.getBytes(charset);
    byte[] ivBytes = ivSpecKey.getBytes(charset);

    key = new SecretKeySpec(getNBytes(8, keyBytes), "DES");
    ivSpec = new IvParameterSpec(getNBytes(8, ivBytes));
    // create the cipher with the algorithm you choose
    // see javadoc for Cipher class for more info, e.g.
    cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
  }

  private byte[] encryptBytes(byte[] input) {
    if (cipher != null) {
      try {
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
        byte[] encrypted= new byte[cipher.getOutputSize(input.length)];
        int enc_len = cipher.update(input, 0, input.length, encrypted, 0);
        enc_len += cipher.doFinal(encrypted, enc_len);
        return encrypted;
      }
      catch(Exception e) {
        logger.warn("Cannot encrypt setting", e);
      }
    }
    return input;
  }

  private byte[] decryptBytes(byte[] encrypted) {
    if (cipher != null) {
      try {
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
        byte[] decrypted = new byte[cipher.getOutputSize(encrypted.length)];
        int dec_len = cipher.update(encrypted, 0, encrypted.length, decrypted, 0);
        dec_len += cipher.doFinal(decrypted, dec_len);
        return getNBytes(dec_len, decrypted);
      }
      catch(Exception e) {
        logger.warn("Cannot decrypt setting", e);
      }
    }
    return encrypted;
  }

  public String encrypt(String cleanText) {
    return new String(Base64Coder.encode(encryptBytes(cleanText.getBytes(charset))));
  }

  public String decrypt(String encrypted) {
    return new String(decryptBytes(Base64Coder.decode(encrypted)), charset);
  }
}
