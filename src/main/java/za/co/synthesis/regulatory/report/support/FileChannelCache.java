package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.regulatory.report.persist.IConfigStore;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by petruspretorius on 06/06/2017.
 */
public class FileChannelCache implements IChannelCache {
  private static final Logger logger = LoggerFactory.getLogger(FileChannelCache.class);

  private String libraryPath;
  private String packagePath;
  private String htmlPath;
  private JdbcConnectionManager jdbcConnectionManager;
  private IConfigStore configStore;

  public String getUiContextAndProxyPath() {
    return uiContextAndProxyPath;
  }

  private String uiContextAndProxyPath;

  private final String LIB_JS = "lib.js";
  private final String MAIN_JS = "main.js";
  private final String HTML = "channel.html";

  Map<String, LocalDateTime> cacheExpiry = new HashMap<>();
  Duration lifetime = Duration.ofMinutes(1);

  @Required
  public void setLibraryPath(String libraryPath) {
    this.libraryPath = libraryPath;
  }

  @Required
  public void setPackagePath(String packagePath) {
    this.packagePath = packagePath;
  }

  @Required
  public void setHtmlPath(String htmlPath) {
    this.htmlPath = htmlPath;
  }

  @Required
  public void setUiContextAndProxyPath(String contextAndProxyPath) {
    this.uiContextAndProxyPath = contextAndProxyPath;
  }

  @Required
  public void setJdbcConnectionManager(JdbcConnectionManager jdbcConnectionManager) {
    this.jdbcConnectionManager = jdbcConnectionManager;
  }

  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  public String getVendorCode() throws Exception {
    return getCachedLibraryArtifact(libraryPath,LIB_JS);
  }

  public String getCachedLibraryArtifact(String path, String key) throws Exception {

    String result = configStore.getCacheByName(key);
    if (result == null) {
      //load it into cache
      result = getContentFromURL(new URL( path + key));
      configStore.setCacheByName(key,result);
      cacheExpiry.put(key, LocalDateTime.now().plus(lifetime));
    } else {
      //check if expired
      LocalDateTime loadedDate = cacheExpiry.get(key);
      if (loadedDate == null || loadedDate.isBefore(LocalDateTime.now()) ) {
        //try to read, and if success, reset cached Date, otherwise do nothing.
        try {
          String newResult = getContentFromURL(new URL( path + key));
          if (newResult != null)  {
            configStore.setCacheByName(key,newResult);
            cacheExpiry.put(key, LocalDateTime.now().plus(lifetime));
            result = newResult;
          }
        } catch (Exception e)  {
          //do nothing and return the current result we have.
        }
      }
    }
    return result;
  }

  public String getPackage(String channelName) throws Exception {
    String key = channelName + "/pack.min.js";
    return getCachedLibraryArtifact(packagePath,key);
  }

  public String getAppCode() throws Exception {
    return getCachedLibraryArtifact(libraryPath,MAIN_JS);
  }

  public String getForm(String channelName, String data, Boolean internal, Map<String, String[]> requestParams,String _uiContextAndProxyPath, Boolean packedRules, Boolean allowNew) throws Exception {

    _uiContextAndProxyPath=(_uiContextAndProxyPath!=null)?_uiContextAndProxyPath:uiContextAndProxyPath;

    //TODO: THIS NEEDS TO BE EXTERNALISED FOR EACH CHANNEL AS PART OF THE ... PACKAGE CONFIG?
    String key = "channel.html";
    String pageHTML = getCachedLibraryArtifact(htmlPath,key);
    
    //Check for "access_token" param on request url, inject if present...
    String accessToken = "";
    if (requestParams != null && requestParams.containsKey("access_token")) {
      //get the first non-null token and attach that.
      for (String token : requestParams.get("access_token")){
        accessToken = token;
        if (accessToken != null && !accessToken.isEmpty())
          break;
      }
      if (accessToken != null && !accessToken.isEmpty()) {
        pageHTML = pageHTML.replaceAll("#\\{access_token_url}", "&access_token=" + accessToken)
            .replaceAll("#\\{access_token_property}", "access_token:'" + accessToken + "',");
      }
    }
    pageHTML = pageHTML.replaceAll("#\\{access_token_url}", "")
        .replaceAll("#\\{access_token_property}", "");
  
    //Resolve nulls.
    packedRules = packedRules == null ? false : packedRules;
    
    return pageHTML
        .replace("#{channel}", channelName)
        .replace("#{data}", data)
        .replace("#{internal}", internal ? "internal/" : "")
        .replace("#{packedRules}", packedRules ? "&packedRules=true" : "")
        .replace("#{allowNew}", allowNew ? "&allowNew=true" : "")
        .replace("#{validationRulesURL}", packedRules ? "\nvalidationRulesURL: 'rules/validation',\n" : "")
        .replace("#{rdsProxyPath}",_uiContextAndProxyPath); //TODO we need a better implementation to get the proxy path.
  }

  @Override
  public String getLookups(String channelName) throws Exception {
    return null;
  }

  private String getContentFromURL(final URL fileUrl) {
    try {
      BufferedReader buffReader = new BufferedReader(new InputStreamReader(fileUrl.openStream()));

      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = buffReader.readLine()) != null) {
        sb.append(line).append('\n');
      }
      return sb.toString();
    } catch (IOException e) {
      logger.warn("Cannot load content from URL '" + fileUrl.toString() + "'", e);
      return null;
    }
  }

}
