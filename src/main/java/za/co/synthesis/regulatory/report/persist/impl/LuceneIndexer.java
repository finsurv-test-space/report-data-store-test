package za.co.synthesis.regulatory.report.persist.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.CriterionType;
import za.co.synthesis.regulatory.report.persist.IIndexer;
import za.co.synthesis.regulatory.report.persist.index.IndexField;
import za.co.synthesis.regulatory.report.persist.index.IndexFieldValue;
import za.co.synthesis.regulatory.report.persist.index.IndexedRecord;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by james on 2017/08/08.
 */
public class LuceneIndexer implements IIndexer {
  private static final Logger logger = LoggerFactory.getLogger(LuceneIndexer.class);

  @Autowired
  ApplicationContext applicationContext;


  String indexPath;
  IndexWriter indexWriter = null;
  Directory indexDirectory = null;
  IndexReader indexReader = null;
  IndexSearcher indexSearcher = null;
  Document indexDocument = null;
  HashMap<String, Field> recordFields = null;
  long indexCount = 0L;


  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public LuceneIndexer(String indexPath) {
    this.indexPath = indexPath;
    createIndex();
  }

  public boolean createIndex() {
    if (indexWriter == null && indexReader == null && indexDocument == null)
    try {
      Directory dir = FSDirectory.open(new File(indexPath).toPath());
      Analyzer analyzer = new StandardAnalyzer();
      IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
      //Create indexWriter instance -- Always overwrite the directory...
      iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
      indexWriter = new IndexWriter(dir, iwc);
      indexDocument = new Document();
    } catch (Exception e) {
      logger.error("Error opening index Writer. " + e.getMessage(), e);
    }
    if (indexWriter != null && indexReader == null)
      try {
      //Create indexReader and indexSearcher instances...
//      Directory indexDirectory = FSDirectory.open(new File(indexPath).toPath());
//      indexReader = DirectoryReader.open(indexDirectory);
      indexReader = DirectoryReader.open(indexWriter);
    } catch (Exception e) {
      logger.error("Error opening index Reader. " + e.getMessage(), e);
    }
    if (indexReader != null && indexSearcher == null)
    try {
      indexSearcher = new IndexSearcher(indexReader);
      return true;
    } catch (Exception e) {
      logger.error("Error creating index Searcher. " + e.getMessage(), e);
    }
    return false;
  }


  /**
   * Add documents to the index
   */
//    public void addDocuments(List<Object> jsonObjects) {
//        Document doc = new Document();
//        for (Object obj : jsonObjects) {
//            if ((obj instanceof Map) || (obj instanceof List)){
//                doc.clear();
//                deepIndexJson(doc, null, obj);
//
//                JSWriter jsWriter = new JSWriter();
//                jsWriter.setQuoteAttributes(true);
//                if (obj instanceof Map) {
//                    JSObject jsReportData = (JSObject) obj;
//                    jsReportData.compose(jsWriter);
//                } else if (obj instanceof Map) {
//                    JSArray jsReportData = (JSArray) obj;
//                    jsReportData.compose(jsWriter);
//                }
//                doc.add(new StoredField("JsonContent", jsWriter.toString()));
//
//                try {
//                    indexWriter.addDocument(doc);
//                } catch (IOException ex) {
//                    System.err.println("Error adding documents to the index. " + ex.getMessage());
//                }
//            }
//        }
//    }
//
//
//    private void deepIndexJson(Document doc, String fieldName, Object fieldObj) {
//        if (fieldObj instanceof Map) {
//            Map<String, Object> jsObj = (Map<String, Object>) fieldObj;
//            for (String field : (Set<String>) jsObj.keySet()) {
//                deepIndexJson(doc, field, jsObj.get(field));
//            }
//        } else if (fieldObj instanceof List) {
//            for (Object item : (List<Object>) fieldObj) {
//                deepIndexJson(doc, fieldName, item);
//            }
//        } else if (fieldName != null &&
//                !fieldName.isEmpty() &&
//                fieldObj != null){
//            doc.add(new StringField(fieldName, fieldObj.toString(), Field.Store.NO));
//        }
//    }


  /**
   * Write the document to the index and close it
   */
  public void flush() {
    try {
      indexWriter.commit();
      indexCount = 0;
    } catch (IOException ex) {
      logger.error("We had a problem flushing the index: " + ex.getMessage(), ex);
    }
  }

  /**
   * Write the document to the index and close it
   */
  public void terminate() {
    if (indexWriter != null)
      try {
        indexWriter.commit();
        indexWriter.close();
        indexWriter = null;
        indexCount = 0;
      } catch (IOException ex) {
        logger.error("We had a problem closing the index(writer): " + ex.getMessage(), ex);
      }
    if (indexReader != null)
      try {
        indexReader.close();
        indexReader = null;
      } catch (IOException ex) {
        logger.error("We had a problem closing the index(reader): " + ex.getMessage(), ex);
      }
  }

  @Override
  public synchronized void clear() {
    if (indexWriter != null || indexReader != null) {
      terminate();
    }
    createIndex();
  }

  @Override
  public void indexRecord(IndexedRecord record, String fulltext) {
    if (indexWriter == null) {
      createIndex();
    }
    if ((indexWriter != null) && (record != null)) {
      boolean createRecordFields = false;
      if (recordFields == null) {
        indexDocument.clear();
        recordFields = new HashMap<String, Field>();
        createRecordFields = true;
      }

      //write the serialized version of the record to the list for storage...
      IndexFieldValue ifv = new IndexFieldValue(CriterionType.System, IndexField.Usage.Store, "IndexedRecord");
      ifv.setValue(record.serialize());
      record.getFields().add(ifv);

      //populate the document with the field values for indexing and storage in lucene...
      for (IndexFieldValue field : record.getFields()) {
        if (field.getName() != null && field.getValue() != null) {
          boolean recordFieldExists = !createRecordFields;
          boolean usageSearch = (field.getUsage().equals(IndexField.Usage.Search));
          //boolean usageStore = (field.getUsage().equals(IndexField.Usage.Store));
          String key = (usageSearch ? "Search" : "Store") + "_" + field.getName();
          if (recordFieldExists) {
            Field recordField = recordFields.get(key);
            if (recordField != null) {
              recordField.setStringValue(field.getValue());
            } else {
              recordFieldExists = false;
            }
          }
          if (createRecordFields || !recordFieldExists) {
            Field recordField = (usageSearch ?
                    (new StringField(field.getName(), field.getValue(), Field.Store.YES)) :
                    (new StoredField(field.getName(), field.getValue()))
            );
            recordFields.put(key, recordField);
            indexDocument.add(recordField);
          }
        }
      }

      if (indexDocument.getFields().size() > 0) {
        try {
          indexWriter.addDocument(indexDocument);
          indexCount++;
        } catch (IOException ex) {
          logger.error("Error adding documents to the index. " + ex.getMessage(), ex);
        }
      }

      //Flushing is very costly - batch flushes!
      if (indexCount % 500 == 0 && indexCount > 0) {
        flush();
      }
    }
  }

  @Override
  public List<IndexedRecord> searchRecords(List<CriterionData> criteria) {
    if (indexSearcher == null) {
      createIndex();
    }
//        if (indexCount > 0){
//            flush();
//        }
    if ((indexSearcher != null) && (criteria != null)) {
      try {
        Query query = null;
        if (criteria.size() > 1) {
          BooleanQuery.Builder boolQueryBuilder = new BooleanQuery.Builder();
          for (CriterionData criterion : criteria) {
            boolQueryBuilder.add(new TermQuery(new Term(criterion.getName(), criterion.getValue())), BooleanClause.Occur.MUST);
//                        boolQueryBuilder.add(new TermQuery(new Term(criterion.getName(), criterion.getValue())), BooleanClause.Occur.SHOULD);
          }
          query = boolQueryBuilder.build();
        } else if (criteria.size() == 1) {
          query = new TermQuery(new Term(criteria.get(0).getName(), criteria.get(0).getValue()));
        }
        TopDocs topDocs = indexSearcher.search(query, 1000);
        List<IndexedRecord> list = new ArrayList<IndexedRecord>();
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for (ScoreDoc scoreDoc : scoreDocs) {
          Document d = indexSearcher.doc(scoreDoc.doc);
          //Just get the serialized, stored value and rebuild the record from that...
          String jsons[] = d.getValues("IndexedRecord");
          for (String json : jsons) {
            if (json.length() > 0) {
              IndexedRecord ir = new IndexedRecord();
              ir.deserialize(json);
              list.add(ir);
            }
          }
        }
        return list;
      } catch (Exception e) {
        logger.error("Exception searching index (" + indexPath + ") with search terms[" + "" + "]: " + e.getMessage(), e);
      }
    }
    return null;
  }
}