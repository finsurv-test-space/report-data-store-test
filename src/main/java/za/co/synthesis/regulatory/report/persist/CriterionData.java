package za.co.synthesis.regulatory.report.persist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jake on 6/27/17.
 */
@Entity
public class CriterionData {
  private static final Logger logger = LoggerFactory.getLogger(CriterionData.class);

  private CriterionType type;
  private String name;
  private List<String> values = new ArrayList<>();
  private String source;

  public CriterionData() {
  }

  public CriterionData(CriterionType type, String name, Object value, String source) {
    this.type = type;
    this.name = name;
    if (value != null) {
      if (value instanceof List) {
        this.values.addAll((List)value);
      } else {
        this.values.add(value.toString());
      }
    }
    this.source = source;
  }

  public CriterionData(CriterionType type, String name, String value) {
    this.type = type;
    this.name = name;
    if (value != null)
      this.values.add(value);
    this.source = source;
  }

  public CriterionData(CriterionType type, String name, List<String> values) {
    this.type = type;
    this.name = name;
    if (values != null)
     this.values.addAll(values);
    this.source = source;
  }

  public CriterionType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    if (values.size() > 0){
      return values.get(0);
    }
    return null;
  }

  public List<String> getRuntimeValues() {
    List<String> runtimeValues = new ArrayList<>();
    for (String value : values){
      runtimeValues.addAll(injectRequestParamsIntoValues(value));
    }
    return runtimeValues;
  }

  public List<String> getValues() {
    return values;
  }

  public void setType(CriterionType type) {
    this.type = type;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setValue(String value) {
    this.values.clear();
    this.values.add(value);
  }

  public void addValue(String value) {
    this.values.add(value);
  }

  public void setValues(List<String> values) {
    if (values != null && values.size()>0) {
      this.values.addAll(values);
    }
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public static List<String> injectRequestParamsIntoValues(Object valObj){
    List<String> values = new ArrayList<>();
    if (valObj instanceof List) {
//      values.addAll((List)valObj);
      for (Object obj : (List<Object>)valObj){
        values.addAll(injectRequestParamsIntoValues(obj));
      }
    }
    if (valObj instanceof String){
      String valStr = (String)valObj;
      Pattern requestParamPattern = Pattern.compile("\\$\\{REQUEST:([^}]*)}");
      Matcher requestParamMatcher = requestParamPattern.matcher(valStr);
      String requestParam = null;
      while (requestParamMatcher.find()) {
        requestParam = requestParamMatcher.group(1);
        logger.trace("\tRequestParam required: {}", requestParam);
      }

      if (requestParam != null){
        Map<String, RequestParamData> authMeta = SecurityHelper.getAuthMeta().getAuthenticationMeta();
        if (authMeta.containsKey(requestParam)){
          RequestParamData data = authMeta.get(requestParam);
          values.addAll(data.getValues());
        }
      } else if (valStr.indexOf(",")>0) { //check for comma separated lists...
        String[] vals = valStr.split("\\s*,\\s*");
        for (String val : vals) {
          values.addAll(injectRequestParamsIntoValues(val.trim()));
        }
      } else {
        values.add(valStr);
      }
    } else if (valObj != null){
      values.add(valObj.toString());
    }
    return values;
  }
}
