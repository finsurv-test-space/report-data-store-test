package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.schema.ValidateResponse;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.EvaluationEngineFactory;
import za.co.synthesis.regulatory.report.support.IChannelCache;
import za.co.synthesis.regulatory.report.support.ReportServiceExecutorService;
import za.co.synthesis.regulatory.report.support.RulesCache;
import za.co.synthesis.regulatory.report.support.properties.CompoundResource;
import za.co.synthesis.regulatory.report.validate.*;
import za.co.synthesis.rule.core.Evaluator;
import za.co.synthesis.rule.core.StatusType;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("internal/producer/api")
public class InternalProducerValidationApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(InternalProducerValidationApi.class);

  @Autowired
  private ValidationRunner validationRunner;


  //-----------------------------------------------------------------------------------------------
  //  Validation APIs
  //-----------------------------------------------------------------------------------------------
  /**
   * expose request for "producer/api/importUndertakingCCN"
   * Exposed as "internal/producer/api/validation/importUndertakingCCN"
   */

  @RequestMapping(value = "/validation/importUndertakingCCN", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_importUndertakingCCN(
          @RequestParam String channelName,
          @RequestParam String CCN,
          @RequestParam(required = false) String valueDate) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_importUndertakingCCN()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("CCN", CCN);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "importUndertakingCCN", parameters));
  }

  /**
   * expose request for "producer/api/Validate_LoanRef"
   * Exposed as "internal/producer/api/validation/Validate_LoanRef"
   */
  @RequestMapping(value = "/validation/loanRef", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_loanRef(
          @RequestParam String channelName,
          @RequestParam String loanRef,
          @RequestParam(required = false) String valueDate) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_loanRef()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("loanRef", loanRef);
    parameters.put("valueDate", valueDate);

    return new ValidateResponse(validationRunner.runValidation(channelName, "loanRef", parameters));
  }

  /**
   * expose request for "producer/api/replacementTrnReference"
   * Exposed as "internal/producer/api/validation/replacementTrnReference"
   */
  @RequestMapping(value = "/validation/replacementTrnReference", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_replacementTrnReference(
          @RequestParam String channelName,
          @RequestParam(required = false) String replacementTrnReference) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_replacementTrnReference()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("replacementTrnReference", replacementTrnReference);

    return new ValidateResponse(validationRunner.runValidation(channelName, "replacementTrnReference", parameters));
  }

  /**
   * expose request for "producer/api/reversalTrnRef"
   * Exposed as "internal/producer/api/validation/reversalTrnRef"
   */
  @RequestMapping(value = "/validation/reversalTrnRef", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_reversalTrnRef(
          @RequestParam String channelName,
          @RequestParam String trnReference,
          @RequestParam String flow,
          @RequestParam String sequence,
          @RequestParam String category) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_reversalTrnRef()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("trnReference", trnReference);
    parameters.put("flow", flow);
    parameters.put("sequence", sequence);
    parameters.put("category", category);
    return new ValidateResponse(validationRunner.runValidation(channelName, "reversalTrnRef", parameters));
  }

  /**
   * expose request for "producer/api/validCCN"
   * Exposed as "internal/producer/api/validation/validCCN"
   */
  @RequestMapping(value = "/validation/validCCN", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_validCCN(
          @RequestParam String channelName,
          @RequestParam String CCN,
          @RequestParam(required = false) String valueDate) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_validCCN()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("CCN", CCN);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "validCCN", parameters));
  }


  /**
   * expose request for "producer/api/validCCNinUCR"
   * Exposed as "internal/producer/api/validation/validCCNinUCR"
   */
  @RequestMapping(value = "/validation/validCCNinUCR", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_validCCNinUCR(
          @RequestParam String channelName,
          @RequestParam String UCR,
          @RequestParam(required = false) String valueDate) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_validCCNinUCR()");
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("UCR", UCR);
    parameters.put("valueDate", valueDate);
    return new ValidateResponse(validationRunner.runValidation(channelName, "validCCNinUCR", parameters));
  }

  /**
   * expose request for "producer/api/validMRNonIVS"
   * Exposed as "internal/producer/api/validation/validMRNonIVS"
   */
  @RequestMapping(value = "/validation/validMRNonIVS", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  ValidateResponse internalValidate_validMRNonIVS(
          @RequestParam String channelName,
          @RequestParam String customsClientNumber,
          @RequestParam String transportDocumentNumber,
          @RequestParam String importControlNumber) throws Exception {
    SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "internalValidate_validMRNonIVS()");
    return new ValidateResponse(StatusType.Pass);
  }

  /**
   * expose request for "producer/api/sbvalidReversalTrnRef"
   * Exposed as "internal/producer/api/validation/sbvalidReversalTrnRef"
   */
  //TODO this should not require the JDBC template at this level. It should be accessed via some data layer.
//  @RequestMapping(value = "/validation/sbvalidReversalTrnRef", method = RequestMethod.GET, produces = "application/json")
//  public @ResponseBody
//  ValidateResponse sbInternalValidate_ReversalTrnRef(
//          @RequestParam String channelName,
//          @RequestParam String reversalTrn,
//          @RequestParam String reversalTrnSeqNumber,
//          @RequestParam String flowCurrency,
//          @RequestParam String foreignValue) throws Exception {
//    SBValidate_ReversalTrnRef validate = new SBValidate_ReversalTrnRef(jdbcTemplate);
//    return new ValidateResponse(validate.call(reversalTrn, flowCurrency, reversalTrnSeqNumber, foreignValue));
//  }

}
