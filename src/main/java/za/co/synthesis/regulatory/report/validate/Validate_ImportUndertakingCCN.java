package za.co.synthesis.regulatory.report.validate;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ICustomValidate;
import za.co.synthesis.rule.core.StatusType;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 12/20/15
 * Time: 7:12 PM
 * A tXstream Finsurv direct DB implementation of the Validate_ImportUndertakingCCN function.
 * Needs to raise the following SARB error: 323, "Not a registered import undertaking client"
 * Should only be run when: the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used
 *
 * Register: registerCustomValidate("Validate_ImportUndertakingCCN",
 *   new Validate_ImportUndertakingCCN(...), "transaction::ValueDate");
 */
public class Validate_ImportUndertakingCCN implements ICustomValidate {
  private JdbcTemplate jdbcTemplate;
  private DateTimeFormatter formatter;

  public Validate_ImportUndertakingCCN(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
    this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  }

  @Override
  public CustomValidateResult call(Object value, Object... otherInputs) {
    String ccn = value.toString();
    Timestamp valueDate = null;

    if (otherInputs.length > 0) {
      try {
        LocalDate dt = LocalDate.parse(otherInputs[0].toString(), formatter);
        valueDate = Timestamp.valueOf(dt.atStartOfDay());
      }
      catch (IllegalArgumentException e) {
        valueDate = null;
      }
    }

    List<Map<String, Object>> listResultMap;

    StringBuilder sb = new StringBuilder();
    sb.append("SELECT LetterOfUndertaking FROM OR_CustomsClientNumber WITH (NOLOCK)");
    sb.append(" WHERE CCN = ?");
    if (valueDate != null) {
      sb.append(" AND (ValidFrom IS NULL OR ValidFrom <= ?)");
      sb.append(" AND (ValidTo IS NULL OR ValidTo >= ?)");
      listResultMap = jdbcTemplate.queryForList(sb.toString(), ccn, valueDate, valueDate);
    }
    else {
      listResultMap = jdbcTemplate.queryForList(sb.toString(), ccn);
    }

    if (listResultMap != null && listResultMap.size() > 0) {
      if (listResultMap.get(0).get("LetterOfUndertaking").equals("Y") )
        return new CustomValidateResult(StatusType.Pass);
      else
        return new CustomValidateResult(StatusType.Fail, "323", "Not a registered import undertaking entity");
    }
    else {
      return new CustomValidateResult(StatusType.Pass);
    }
  }
}
