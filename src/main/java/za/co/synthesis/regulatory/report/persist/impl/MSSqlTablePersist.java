package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.FieldConstant;
import za.co.synthesis.regulatory.report.persist.ITablePersist;
import za.co.synthesis.regulatory.report.persist.PersistException;
import za.co.synthesis.regulatory.report.persist.common.TableDefinition;
import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;
import za.co.synthesis.regulatory.report.schema.SortOrder;
import za.co.synthesis.regulatory.report.schema.SortOrderField;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 8/21/17.
 */
public class MSSqlTablePersist<T extends PersistenceObject> implements ITablePersist<T> {
  private static final Logger logger = LoggerFactory.getLogger(MSSqlTablePersist.class);
  
  private final TableDefinition tableDefinition;
  private final JdbcTemplate haJdbcTemplate;
  private final Class<T> type;
  
  public void setFullTextSearchSupported(boolean fullTextSearchSupported) {
    isFullTextSearchSupported = fullTextSearchSupported;
  }
  
  @Override
  public int updateCriteria(List<CriterionData> criteria, List<CriterionData> newCriteria) {
    int resultCount = 0;
    if (haJdbcTemplate != null && (criteria != null && criteria.size() > 0)) {
      List<String> queries = new ArrayList<>();
      List<String> params = new ArrayList<>();
      boolean overrideSortOrder = false;
      int fullTextSubQueryIndex = -1;
      String fullTextSubQueryName = "";
      if ((newCriteria.size() > 0) && (criteria.size() > 0)) {
        String setPlaceHolders = "";
        String wherePlaceHolders = "";
        String fieldExistsTests = "";
        for (CriterionData newCriterion : newCriteria) {
          List<String> values = newCriterion.getRuntimeValues();
          if (values.size() >= 1) {
            String newVal = values.get(0).trim();
            if (newVal.indexOf("REPLACE(") >=0 || newVal.indexOf("SUBSTRING(") >=0) {
              String concatVal = "";
              for (String val : values){
                concatVal += (concatVal.length()>0?", ":"")+val;
              }
              setPlaceHolders += (setPlaceHolders.length() > 0 ? ", " : "") + "[" + newCriterion.getName() + "] = "+concatVal;
            } else {
              setPlaceHolders += (setPlaceHolders.length() > 0 ? ", " : "") + "[" + newCriterion.getName() + "] = ?";
              params.add(newVal);
            }
            fieldExistsTests += (fieldExistsTests.length() > 0 ? " AND " : "") + "COL_LENGTH('REPLACE_THIS_WITH_TABLE_NAME', '" + newCriterion.getName() + "') IS NOT NULL";
          }
        }
        for (CriterionData criterion : criteria) {
          List<String> values = criterion.getRuntimeValues();
          if (values.size() >= 1) {
            wherePlaceHolders += (wherePlaceHolders.length() > 0 ? " AND " : "") + "[" + criterion.getName() + "] = ?";
            params.add(values.get(0).trim());
          }
        }

        //**NOTE: we are assuming that all index and search tables have the same criterion columns to be changed as the targeted storage/persistence table.**//
        if (tableDefinition.supportsFullTextSearch()) {
          queries.add("UPDATE [" + tableDefinition.getFullTextTablename() + "] \n" +
              "SET " + setPlaceHolders + " \n" +
              "WHERE " + wherePlaceHolders + " \n");
        }
        if (tableDefinition.getIndexTablename() != null && tableDefinition.getIndexTablename().length() > 0) {//DIRTY!  The indexer should perhaps do this if there is one.
          queries.add("UPDATE [" + tableDefinition.getIndexTablename() + "] \n" +
              "SET " + setPlaceHolders + " \n" +
              "WHERE " + wherePlaceHolders + " \n");
        }
        queries.add("UPDATE [" + tableDefinition.getTablename() + "] \n" +
            "SET " + setPlaceHolders + " \n" +
            "WHERE " + wherePlaceHolders + " \n");
      }
    
      // *****  Generate the queries and output for debugging  ***** //
      for (String query : queries) {
        String queryString = query.toString();
        String logQueryString = "\n--GENERATED SQL CODE--\n\n" + queryString + "\n\n----------------------\n";
        if (!queryString.isEmpty()) {
          try {
//              logger.trace(logCriterionSet);
            for (String param : params) {
              logQueryString = logQueryString.replaceFirst("\\?", "'" + param + "'");
            }
            logger.trace("Update Criteria SQL Query: \n\n" + logQueryString);
          
            resultCount = haJdbcTemplate.update(query.toString(), (Object[]) params.toArray(new String[params.size()]));
          } catch (Exception e) {
            if (e.getMessage().indexOf("did not return a result set.") < 0) {
              logger.error("Indexer query failed to run as expected.\n\nSQL: " + queryString, e);
            }
          }
        }
      }
    } else if (criteria == null || criteria.size() == 0) {
      logger.error("No criteria supplied for accessing MSSql data store (Report) - security violation.");
    }
  
    return resultCount;
  }
  
  private boolean isFullTextSearchSupported = true;
  
  public MSSqlTablePersist(TableDefinition tableDefinition, JdbcTemplate haJdbcTemplate, Class<T> type) {
    this.tableDefinition = tableDefinition;
    this.haJdbcTemplate = haJdbcTemplate;
    this.type = type;
  }
  
  private String mapToJSON(Map<String, Object> map) {
    JSObject jsObject = JsonSchemaTransform.mapToJson(map);
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent("  ");
    writer.setNewline("\n");
    jsObject.compose(writer);
    return writer.toString();
  }
  
  private Map<String, Object> jsonToMap(String str) throws Exception {
    JSStructureParser parser = new JSStructureParser(str);
    return (Map<String, Object>) parser.parse();
  }
  
  public T getInstanceOfT(String uuidValue) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
    Constructor c = type.getConstructor(String.class);
    return (T) c.newInstance(uuidValue);
  }
  
  @Override
  public void write(List<T> objects) throws PersistException {
    StringBuilder sb = new StringBuilder();
    List<Object> valueList = new ArrayList<Object>();
    for (T obj : objects) {
      sb.append("IF NOT EXISTS (SELECT * FROM [").append(tableDefinition.getTablename()).append("] WITH (NOLOCK) WHERE [").append(tableDefinition.getUUIDFieldName()).append("] = ?) ");
      sb.append("BEGIN");
      sb.append("  INSERT ").append(tableDefinition.getTablename()).append("(");
      boolean bFirst = true;
      for (TableDefinition.Field f : tableDefinition.getFields()) {
        if (f.getType() != TableDefinition.FieldType.SyncPoint) {
          if (bFirst) {
            bFirst = false;
          } else {
            sb.append(",");
          }
          sb.append("[").append(f.getName()).append("]");
        }
      }
      sb.append(")");
  
      String fieldNameToInvoke = tableDefinition.getUUIDFieldName();
      String objectTypeToInvoke = null;
      try {
        sb.append(" VALUES(");
        Method method = obj.getClass().getMethod("get" + tableDefinition.getUUIDFieldName());
  
        valueList.add(method.invoke(obj));
        bFirst = true;
        for (TableDefinition.Field f : tableDefinition.getFields()) {
          fieldNameToInvoke = f.getName();
          if (f.getType() != TableDefinition.FieldType.SyncPoint) {
            if (bFirst) {
              bFirst = false;
            } else {
              sb.append(",");
            }
            sb.append("?");
            objectTypeToInvoke = obj.getClass().getTypeName();
            method = obj.getClass().getMethod("get" + f.getName());
            Object objectValue = null;
            try {
              if (f.getType() == TableDefinition.FieldType.JsonMap) {
                objectValue = mapToJSON((Map) method.invoke(obj));
              } else {
                objectValue = method.invoke(obj);
              }
            } catch (InvocationTargetException | IllegalAccessException e) {
              logger.error("Error reflecting getter for writing SQL data - field name: " + fieldNameToInvoke + "@" + objectTypeToInvoke, e);
            }
            if (objectValue != null) {
              String objectStr = objectValue.toString();
//              System.out.println("\t" + f.getName() + "\t\t->\t" + objectStr + "\t(" + objectStr.length() + ")");
              valueList.add(objectValue);
            } else {
              valueList.add(null);
            }
          }
  
        }
        sb.append(")");
      } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
        logger.error("X_Error reflecting getter for writing SQL data - field name: " + fieldNameToInvoke + "@" + objectTypeToInvoke, e);
        throw new PersistException("X_Error reflecting getter for writing SQL data", e);
      }
      sb.append("END\n\n");
    }
    
    haJdbcTemplate.update(sb.toString(), valueList.toArray(new Object[valueList.size()]));
  }
  
  @Override
  public void write(T obj) throws PersistException {
    if (obj != null) {
      StringBuilder sb = new StringBuilder();
      sb.append("IF NOT EXISTS (SELECT * FROM [").append(tableDefinition.getTablename()).append("] WITH (NOLOCK) WHERE [").append(tableDefinition.getUUIDFieldName()).append("] = ?) ");
      sb.append("BEGIN");
      sb.append("  INSERT [").append(tableDefinition.getTablename()).append("](");
      boolean bFirst = true;
      for (TableDefinition.Field f : tableDefinition.getFields()) {
        if (f.getType() != TableDefinition.FieldType.SyncPoint) {
          if (bFirst) {
            bFirst = false;
          } else {
            sb.append(",");
          }
          sb.append("[").append(f.getName()).append("]");
        }
      }
      sb.append(")");
  
      List<Object> valueList = new ArrayList<Object>();
      String currentMethodName = "";
      String currentClassName = "";
      try {
        sb.append(" VALUES(");
        Method method = obj.getClass().getMethod("get" + tableDefinition.getUUIDFieldName());
  
        valueList.add(method.invoke(obj));
        bFirst = true;
        for (TableDefinition.Field f : tableDefinition.getFields()) {
          if (f.getType() != TableDefinition.FieldType.SyncPoint) {
            if (bFirst) {
              bFirst = false;
            } else {
              sb.append(",");
            }
            sb.append("?");
            currentMethodName = "get" + f.getName();
            currentClassName = obj.getClass().getSimpleName();
            method = obj.getClass().getMethod(currentMethodName);
            Object objectValue;
            if (f.getType() == TableDefinition.FieldType.JsonMap) {
              objectValue = mapToJSON((Map) method.invoke(obj));
            } else {
              objectValue = method.invoke(obj);
            }
            if (objectValue != null) {

              if ((objectValue instanceof Integer) ||
                      (objectValue instanceof Float) ||
                      (objectValue instanceof Long) ||
                      (objectValue instanceof Short) ||
                      (objectValue instanceof String)
                ){
                      valueList.add(objectValue);
              } else if (objectValue.getClass().isPrimitive()) { //primitives
                valueList.add(objectValue);
              } else if (objectValue instanceof Enum) { //try coerce all Enums to the relevant string analogue.
                String objectStr = objectValue.toString();  //((Enum)objectValue).getDeclaringClass();
                valueList.add(objectStr);
              } else if (objectValue.getClass().isEnum()) { //try coerce all enums to the relevant string analogue.
                String objectStr = objectValue.toString();
                valueList.add(objectStr);
              } else {  //some other object class...
                valueList.add(objectValue);  //this may throw an exception (within JDBC etc) - IE: where the instance is of an "UNKNOWN" object type.
              }
            } else {
              valueList.add(null);
            }
          }
  
        }
        sb.append(")");
      } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
        throw new PersistException("Error reflecting getter for writing SQL data: " + currentClassName + "." + currentMethodName + " - " + e.getMessage(), e);
      }
      sb.append("END");
  
      int returnVal = haJdbcTemplate.update(sb.toString(), valueList.toArray(new Object[valueList.size()]));
      logger.trace("MSSqlTablePersist update returned: " + returnVal + " rows updated, without error.");
    } else {
      logger.trace("MSSqlTablePersist ignoring persist of null object");
    }
  }
  
  private List<T> getObjectList(List<Map<String, Object>> queryResponseList) {
    List<T> objList = new ArrayList<T>();
    for (Map<String, Object> row : queryResponseList) {
      try {
        T obj = getInstanceOfT((String) row.get(tableDefinition.getUUIDFieldName()));
  
        for (TableDefinition.Field f : tableDefinition.getFields()) {
          if (f.getType() != TableDefinition.FieldType.SyncPoint /*&& f.getType() != TableDefinition.FieldType.UUID*/) {
            if (f.getType() == TableDefinition.FieldType.UUID) {
              Method method = obj.getClass().getMethod("set" + f.getName(), String.class);
              method.invoke(obj, (String) row.get(f.getName()));
            } else if (f.getType() == TableDefinition.FieldType.String) {
              Method method = obj.getClass().getMethod("set" + f.getName(), String.class);
              method.invoke(obj, (String) row.get(f.getName()));
            } else if (f.getType() == TableDefinition.FieldType.Integer) {
              Method method = obj.getClass().getMethod("set" + f.getName(), Integer.class);
              method.invoke(obj, (Integer) row.get(f.getName()));
            } else if (f.getType() == TableDefinition.FieldType.DateTime) {
              Method method = obj.getClass().getMethod("set" + f.getName(), Timestamp.class);
              method.invoke(obj, (Timestamp) row.get(f.getName()));
            } else if (f.getType() == TableDefinition.FieldType.JsonMap) {
              Method method = obj.getClass().getMethod("set" + f.getName(), Map.class);
              method.invoke(obj, jsonToMap((String) row.get(f.getName())));
            }
          }
        }
        objList.add(obj);
      } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
        logger.error("Error reflecting setters for writing SQL data to object", e);
      } catch (Exception e) {
        logger.error("Error parsing JSON before writing SQL data to object", e);
      }
    }
    return objList;
  }
  
  @Override
  public PersistenceObjectList<T> readFromSyncpoint(String syncpoint, int maxRecords) {
    return readFromSyncpoint(syncpoint, null,  maxRecords);
  }

  @Override
  public PersistenceObjectList<T> readFromSyncpoint(String syncpoint, String reportSpace, int maxRecords) {
    ArrayList<Object> params = new ArrayList<>();
    StringBuilder sb = new StringBuilder();
    
    sb.append("SELECT ");
    if (maxRecords > 0) {
      sb.append("TOP ").append(maxRecords).append(" ");
    }
    boolean bFirst = true;
    for (TableDefinition.Field f : tableDefinition.getFields()) {
      if (bFirst) {
        bFirst = false;
      } else {
        sb.append(",");
      }
      if (f.getType() == TableDefinition.FieldType.SyncPoint) {
        sb.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), [").append(f.getName()).append("])) as ").append(f.getName());
      } else {
        sb.append("[").append(f.getName()).append("]");
      }
    }
    sb.append(" FROM [").append(tableDefinition.getTablename()).append("] WITH (NOLOCK)");
    
    if (syncpoint != null || reportSpace != null) {
      sb.append(" WHERE ");
      if (syncpoint != null) {
        sb.append("[").append(tableDefinition.getSyncpointFieldName()).append("] > CONVERT(VARBINARY(8), ?, 1)");
        params.add(syncpoint);
      }
      if (syncpoint != null && reportSpace != null) {
        sb.append(" AND ");
      }
      if (reportSpace != null) {
        String reportSpaceFieldName = FieldConstant.ReportSpace;
        List<TableDefinition.Field> fields = tableDefinition.getFields();
        if (fields != null && fields.size() > 0){
          for(TableDefinition.Field field : fields){
            if (FieldConstant.ReportSpace.equalsIgnoreCase(field.getName())){
              reportSpaceFieldName = field.getName();
              break;
            }
          }
        }
        sb.append("[").append(reportSpaceFieldName).append("] = ?");
        params.add(reportSpace);
      }
    }
    sb.append(" ORDER BY [").append(tableDefinition.getSyncpointFieldName()).append("]");
    
    List<Map<String, Object>> list;
    if (params.size() > 0) {
      list = haJdbcTemplate.queryForList(sb.toString(), (Object[]) params.toArray(new String[0]));
    } else {
      list = haJdbcTemplate.queryForList(sb.toString());
    }
    
    String newSyncpoint = syncpoint;
    if (list != null && list.size() > 0) {
      newSyncpoint = (String) list.get(list.size() - 1).get(tableDefinition.getSyncpointFieldName());
    }
    
    return new PersistenceObjectList<T>(newSyncpoint != null ? newSyncpoint : syncpoint, getObjectList(list));
  }
  
  
  @Override
  public List<T> queryByMapList(Map<String, List<String>> queryMapList, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    if ((queryMapList != null && queryMapList.size() > 0) || (maxResults > 0)) {
      StringBuilder sb = new StringBuilder();
  
      List<String> params = new ArrayList<>();
  
      sb.append("SELECT");
      //return first x results only - NOT PAGINATING
      if (maxResults > 0 && page <= 0) {
        sb.append(" TOP ").append(maxResults);
      }
      sb.append(" ");
      boolean bFirst = true;
      for (TableDefinition.Field f : tableDefinition.getFields()) {
        if (bFirst) {
          bFirst = false;
        } else {
          sb.append(",");
        }
        if (f.getType() == TableDefinition.FieldType.SyncPoint) {
          sb.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), [").append(f.getName()).append("])) as ").append(f.getName());
        } else {
          sb.append("[").append(f.getName()).append("]");
        }
      }
      sb.append(" FROM [").append(tableDefinition.getTablename());
      if (currentItemsOnly)
        sb.append("View");
      sb.append("] WITH (NOLOCK)");
  
      if (queryMapList != null && queryMapList.size() > 0) {
        sb.append(" WHERE ");
        int conc = 0;
        for (Map.Entry<String, List<String>> entry : queryMapList.entrySet()) {
          List<String> values = entry.getValue();
          if (values.size() > 1) {
            if (conc > 0) {
              sb.append(" AND ");
            }
            sb.append(" [").append(entry.getKey()).append("] IN (");
            int cnt = 0;
            for (String value : values) {
              if (cnt > 0) {
                sb.append(", ");
              }
              sb.append("?");
              params.add(value);
              cnt++;
            }
            sb.append(")");
          } else if (values.size() == 1) {
            if (conc > 0) {
              sb.append(" AND ");
            }
            sb.append(" [").append(entry.getKey()).append("] = ? ");
            params.add(values.get(0));
          } else {
            //sb.append(" [").append(entry.getKey()).append("] = ? ");
            //params.add(values.get(0));
            logger.info ("Unknown criterion provided to query generator: "+entry.getKey()+" -- with no associated value.");
          }
          conc++;
        }
      }
  
      if (sortBy != null && sortBy.size() > 0) {
        sb.append(" ORDER BY ");
        int cnt = 0;
        for (SortOrderField field : sortBy) {
          if (cnt > 0)
            sb.append(", ");
          sb.append("[").append(field.getName()).append("]");
          if (field.getOrder().equals(SortOrder.Descending))
            sb.append(" ").append(field.getOrderStr());
          cnt++;
        }
      }
  
      //PAGINATION - return X results from page Y
      //TODO: This could be made far more efficient by using only the indexes (specifically compound indices) and avoiding table lookups
      //see article here: https://sqlperformance.com/2015/01/t-sql-queries/pagination-with-offset-fetch
      if (maxResults > 0 && page > 0) {
        if (sortBy == null || sortBy.size() == 0) {
          //ORDER BY is required before the OFFSET command - if one wasn't added, use ascending, chronological order.
          sb.append(" ORDER BY ").append("[").append(tableDefinition.getSyncpointFieldName()).append("]");
        }
        sb.append(" OFFSET ").append(maxResults * (page - 1)).append(" ROWS FETCH NEXT ").append(maxResults).append(" ROWS ONLY ");
      }
  
      List<Map<String, Object>> list;
      list = haJdbcTemplate.queryForList(sb.toString(), (Object[]) params.toArray(new String[0]));
  
      return getObjectList(list);
    } else if (queryMapList == null && queryMapList.size() == 0) {
      logger.error("No criteria supplied for accessing MSSql data store (Report) - security violation.");
    }
    return new ArrayList<>();
  }
  
  
  @Override
  public List<T> searchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    return fullTextSearchRecords(criteria, maxResults, page, currentItemsOnly, sortBy);
  }
  
  
  @Override
  public List<T> fullTextSearchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    int resultLimit = 0;
    List<Map<String, Object>> list;
    if (haJdbcTemplate != null && ((criteria != null && criteria.size() > 0) || maxResults > 0)) {
      List<String> queries = new ArrayList<>();
      List<String> params = new ArrayList<>();
      boolean overrideSortOrder = false;
      int fullTextSubQueryIndex = -1;
      String fullTextSubQueryName = "";
      if (criteria.size() > 0) {
        for (CriterionData criterion : criteria) {
          if ("FULL_TEXT_SEARCH".equals(criterion.getName()) && tableDefinition.supportsFullTextSearch() && this.isFullTextSearchSupported) {
            List<String> values = criterion.getRuntimeValues();
            String fullTextValues = "";
            if (values.size() >= 1) {
//              params.add(criterion.getType().toString());
//              params.add(criterion.getName());
              String placeHolders = "?";
              for (String value : values) {
                fullTextValues += value + " ";
              }
              params.add(fullTextValues.trim());
              //if we are doing fulltext search at all, then we want to drive the selections based on the ranking order...
              queries.add("    SELECT " + (maxResults > 0 && page <= 0 ? ("TOP " + (maxResults) + " ") : "") +
                  "[KEY] AS IndexedObjectId, [RANK] as IndexedObjectRank FROM FREETEXTTABLE([" + tableDefinition.getFullTextTablename() + "], [FullText], " + placeHolders + ") ORDER BY RANK DESC " +
                  (maxResults > 0 && page > 0 ? (" OFFSET " + (maxResults * (page - 1)) + " ROWS  FETCH NEXT " + (maxResults) + " ROWS ONLY ") : "")
              );
              if (fullTextSubQueryIndex < 0) {
                fullTextSubQueryIndex = queries.size() - 1;
              }
              overrideSortOrder = true;
            } else {
              logger.trace("Full text search not supported for [{}]/[{}], field [FullText] - search text: '{}'", tableDefinition.getTablename(), tableDefinition.getFullTextTablename(), fullTextValues);
            }
          } else if ("CONTAINS_TEXT_SEARCH".equals(criterion.getName())) {
            List<String> values = criterion.getRuntimeValues();
            String containsTextValues = "";
            if (values.size() >= 1) {
              //Loop through each value and create a separate contains statement?
              for (String containsText : values) {
                if (containsText != null && !containsText.isEmpty()) {
                  String placeHolders = "?";
                  if (containsText.indexOf("*") < 0) { //ONLY REMOVE THE QUOTES IF THE TEXT IS NOT A WILDCARD SEARCH
                    params.add(containsText.replace("^\\s*\\\"", "").replace("\\\"\\s*$", ""));
                  } else {
                    params.add(containsText.trim());
                  }
                  queries.add("    SELECT " + //(maxResults > 0 && page <= 0 ? ("TOP " + (maxResults) + " ") : "") +
                      "[" + tableDefinition.getIndexUUIDFieldName() + "] AS IndexedObjectId \n" +
                      "    FROM [" + tableDefinition.getFullTextTablename() + "] \n" +
                      "    WHERE CONTAINS([FullText], " + placeHolders + ")"
                  );
                } else {
                  logger.trace("Contains text search not supported for [{}]/[{}], field [FullText] - contains text: '{}'", tableDefinition.getTablename(), tableDefinition.getFullTextTablename(), containsText);
                }
              }
            }
          } else {
            List<String> values = criterion.getRuntimeValues();
            if (values.size() > 1) {
              params.add(criterion.getType().toString());
              params.add(criterion.getName());
              String placeHolders = "";
              for (String value : values) {
                params.add(value);
                placeHolders += (placeHolders.length() > 0 ? ", " : "") + "?";
              }
              queries.add("    SELECT [" + tableDefinition.getIndexUUIDFieldName() + "] AS IndexedObjectId \n    FROM [" + tableDefinition.getIndexTablename() + "] WITH (NOLOCK) \n    WHERE IndexType = ? AND IndexField = ? AND IndexValue IN (" + placeHolders + ") ");
            } else if (values.size() == 1) {
              params.add(criterion.getType().toString());
              params.add(criterion.getName());
              params.add(values.get(0));
              queries.add("    SELECT [" + tableDefinition.getIndexUUIDFieldName() + "] AS IndexedObjectId \n    FROM [" + tableDefinition.getIndexTablename() + "] WITH (NOLOCK) \n    WHERE IndexType = ? AND IndexField = ? AND IndexValue = ? ");
            } //if not 1 or more... then don't add the criteria to the lookup.
            else if (values.size() == 0) {
              params.add(criterion.getType().toString());
              params.add(criterion.getName());
              queries.add("    SELECT [" + tableDefinition.getIndexUUIDFieldName() + "] AS IndexedObjectId \n    FROM [" + tableDefinition.getIndexTablename() + "] WITH (NOLOCK) \n    WHERE IndexType = ? AND IndexField = ? AND (IndexValue = '' OR IndexValue IS NULL) ");
            }
          }
        }
      }
      int cnt = 0;
      String prevQueryName = null;
      String thisQueryName = null;
      String firstQueryName = null;
      StringBuilder query = new StringBuilder();
      if (queries.size() > 0) {
  
        query.append("SELECT");
        //return first x results only - NOT PAGINATING
        if (maxResults > 0 && page <= 0) {
          query.append(" TOP ").append(maxResults);
        }
        query.append(" \n ");
        boolean bFirst = true;
        for (TableDefinition.Field f : tableDefinition.getFields()) {
          if (bFirst) {
            bFirst = false;
          } else {
            query.append("\n ,");
          }
          if (f.getType() == TableDefinition.FieldType.SyncPoint) {
            query.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), DX.[").append(f.getName()).append("])) AS '").append(f.getName()).append("'");
          } else {
            query.append("DX.[").append(f.getName()).append("]");
          }
        }
  
        //Add FullText ranking if necessary
        if (fullTextSubQueryIndex >= 0) {
          query.append("\n ,IndexedObjectRank");
        }
        query.append(" \nFROM \n ( \n");
  
        //Index lookup...
        for (String subquery : queries) {
          if (thisQueryName != null) {
            prevQueryName = thisQueryName;
          }
          thisQueryName = String.format("q_%02d", cnt);
  
          //check for FullText search sub-query
          if (fullTextSubQueryIndex >= 0 && cnt == fullTextSubQueryIndex) {
            fullTextSubQueryName = thisQueryName;
          }
  
          if (prevQueryName == null) {
            firstQueryName = thisQueryName;
            query.append("  SELECT ");
            if (resultLimit > 0) {
              query.append("TOP ").append(resultLimit).append(" ");
            }
            query.append("\n   ").append(firstQueryName).append(".IndexedObjectId \n");
            //Add FullText seach ranking to select if required...
            if (fullTextSubQueryIndex >= 0) {
              query.append("   ,IndexedObjectRank \n");
            }
            query.append("  FROM \n");
    
          } else {
            //join clause
            query.append("  JOIN \n");
          }
          //sub query
          query.append("   ( \n ").append(subquery).append(" \n   ) AS ").append(thisQueryName).append(" \n");
          //on clause (guid : prev=current)
          if (prevQueryName != null) {
            query.append(" ON ").append(prevQueryName).append(".IndexedObjectId").
                append(" = ").append(thisQueryName).append(".IndexedObjectId");
          }
          cnt++;
        }
        if (cnt > 0) {
          query.append(" GROUP BY \n  ").append(firstQueryName).append(".IndexedObjectId \n");
          //Add FullText ranking if necessary
          if (fullTextSubQueryIndex >= 0) {
            query.append("  ,IndexedObjectRank \n");
          }
        }
        //...End Index lookup.
  
        query.append("\n ) AS IndexLookupResult\n ");
        query.append("JOIN [").append(tableDefinition.getTablename());
//        if (currentItemsOnly)
//          query.append("View");
        query.append("] AS DX\n ");
        query.append("ON IndexLookupResult.[IndexedObjectId] ").
            append(" = DX.[").append(tableDefinition.getUUIDFieldName()).append("] \n");
        //
        //***
        //
      } else if (maxResults > 0) {
        //
        //***  IF NO CRITERIA ARE SUPPLIED, BUT WE HAVE A VALID LIMIT - THEN DO SELECT WITH LIMIT AND NO WHERE CLAUSE  ***//
        //
        query.append("SELECT");
        //return first x results only - NOT PAGINATING
        if (maxResults > 0 && page <= 0) {
          query.append(" TOP ").append(maxResults);
        }
        query.append(" \n ");
        boolean bFirst = true;
        for (TableDefinition.Field f : tableDefinition.getFields()) {
          if (bFirst) {
            bFirst = false;
          } else {
            query.append("\n ,");
          }
          if (f.getType() == TableDefinition.FieldType.SyncPoint) {
            query.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), DX.[").append(f.getName()).append("])) AS '").append(f.getName()).append("'");
          } else {
            query.append("DX.[").append(f.getName()).append("]");
          }
        }
  
        query.append(" \nFROM \n [" + tableDefinition.getTablename() + (currentItemsOnly?"View":"") + "] AS DX\n");
        //
        //***
        //
      }
  
  
      if ((sortBy != null && sortBy.size() > 0) || fullTextSubQueryIndex >= 0) {
        query.append(" \nORDER BY \n ");
        boolean separate = false;
    
        //for FullText searches, the ranking takes precedence...
        if (overrideSortOrder && fullTextSubQueryIndex >= 0 && !fullTextSubQueryName.isEmpty()) {
          query.append("IndexedObjectRank DESC");
          separate = true;
        }
    
        //sort by stipulated fields...
        if (sortBy != null && sortBy.size() > 0) {
          for (SortOrderField field : sortBy) {
            if (separate)
              query.append("\n ,");
            query.append("DX.[").append(field.getName()).append("]");
            if (field.getOrder().equals(SortOrder.Descending))
              query.append(" ").append(field.getOrderStr());
            separate = true;
          }
        }
      }
  
      //PAGINATION - return X results from page Y
      //TODO: This could be made far more efficient by using only the indexes (specifically compound indices) and avoiding table lookups
      //see article here: https://sqlperformance.com/2015/01/t-sql-queries/pagination-with-offset-fetch
      if (maxResults > 0 && page > 0 && fullTextSubQueryIndex < 0) {
        if (sortBy == null || sortBy.size() == 0) {
          //ORDER BY is required before the OFFSET command - if one wasn't added, use ascending, chronological order.
          query.append(" \nORDER BY \n ").append("DX.[").append(tableDefinition.getSyncpointFieldName()).append("] ");
        }
        query.append(" \nOFFSET ").append(maxResults * (page - 1)).append(" ROWS \nFETCH NEXT ").append(maxResults).append(" ROWS ONLY ");
      }


//      if (queryString.isEmpty() || logger.isDebugEnabled() || logger.isTraceEnabled() || logger.isInfoEnabled()){
      String CriterionSet = "";
      for (CriterionData criterion : criteria) {
        String criterionValues = "";
        for (String value : criterion.getRuntimeValues()) {
          criterionValues += (criterionValues.length() > 0 ? ", " : "") + value;
        }
        CriterionSet += (CriterionSet.length() > 0 ? ",\n" : "") + "\t" + criterion.getName() + "<" + criterion.getSource() + "> => [" + criterionValues + "]";
      }
      String logCriterionSet = tableDefinition.getIndexTablename() + " criteria provided to generate SQL query" + (CriterionSet.length() > 0 ? ": \n" + CriterionSet : " - criteria list empty");
  
  
      // *****  Generate the query and output for debugging  ***** //
      String queryString = query.toString();
      String logQueryString = "\n--GENERATED SQL CODE--\n\n" + queryString + "\n\n----------------------\n";
      if (!queryString.isEmpty()) {
        try {
          logger.trace(logCriterionSet);
          for (String param : params) {
            logQueryString = logQueryString.replaceFirst("\\?", "'" + param + "'");
          }
          logger.trace("Indexer SQL Query: \n\n" + logQueryString);
  
          list = haJdbcTemplate.queryForList(query.toString(), (Object[]) params.toArray(new String[params.size()]));
          return getObjectList(list);
        } catch (Exception e) {
          if (e.getMessage().indexOf("did not return a result set.") < 0) {
            logger.error("Indexer query failed to run as expected.\n\nSQL: " + queryString, e);
          }
        }
      }
  
  
      if (queryString.isEmpty()) {
        logger.warn("Insufficient " + tableDefinition.getIndexTablename() + " criteria provided to generate meaningful SQL query" + (CriterionSet.length() > 0 ? ": \n" + CriterionSet : " - criteria list empty"));
      }
    }
    if (criteria == null || criteria.size() == 0) {
      logger.error("No criteria supplied for accessing MSSql data store (Report) - security violation.");
    }
    
    return new ArrayList<>();
  }
  
  
  @Override
  public boolean fullTextSearchSupported() {
    return this.isFullTextSearchSupported;
  }
  
  
  @Override
  public Class getPersistClass() {
    return this.type;
  }
  
  @Override
  public List<T> readCurrent() {
    StringBuilder sb = new StringBuilder();
    
    sb.append("SELECT ");
    boolean bFirst = true;
    for (TableDefinition.Field f : tableDefinition.getFields()) {
      if (bFirst) {
        bFirst = false;
      } else {
        sb.append(",");
      }
      if (f.getType() == TableDefinition.FieldType.SyncPoint) {
        sb.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), t.[").append(f.getName()).append("])) as ").append(f.getName());
      } else {
        sb.append("t.[").append(f.getName()).append("]");
      }
    }
    sb.append(" FROM [").append(tableDefinition.getTablename()).append("View] t WITH (NOLOCK) ");
    
    List<Map<String, Object>> list = haJdbcTemplate.queryForList(sb.toString());
    
    return getObjectList(list);
  }
  
  @Override
  public List<T> readBySelector(int maxRecords) {
    StringBuilder sb = new StringBuilder();
    
    sb.append("SELECT TOP ").append(maxRecords).append(" ");
    boolean bFirst = true;
    for (TableDefinition.Field f : tableDefinition.getFields()) {
      if (bFirst) {
        bFirst = false;
      } else {
        sb.append(",");
      }
      if (f.getType() == TableDefinition.FieldType.SyncPoint) {
        sb.append("master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), t.[").append(f.getName()).append("])) as ").append(f.getName());
      } else {
        sb.append("t.[").append(f.getName()).append("]");
      }
    }
    sb.append(" FROM [").append(tableDefinition.getTablename()).append("] t WITH (NOLOCK)");
    sb.append(" JOIN [").append(tableDefinition.getTablename()).append("Sel] sel ON sel.GUID = t.GUID");
    
    sb.append(" ORDER BY t.[").append(tableDefinition.getSyncpointFieldName()).append("]");
    
    List<Map<String, Object>> list = haJdbcTemplate.queryForList(sb.toString());
    
    return getObjectList(list);
  }
  
  public TableDefinition getTableDefinition() {
    return tableDefinition;
  }
  
}
