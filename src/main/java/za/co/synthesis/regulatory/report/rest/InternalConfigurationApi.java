package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ListData;
import za.co.synthesis.regulatory.report.schema.ReportSpace;
import za.co.synthesis.regulatory.report.schema.State;
import za.co.synthesis.regulatory.report.schema.Transition;
import za.co.synthesis.regulatory.report.security.SecurityHelper;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("internal/producer/api/system")
public class InternalConfigurationApi extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalConfigurationApi.class);


    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    ApplicationContext ctx;


    @RequestMapping(value = "/rights", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getInternalRights(
            HttpServletResponse response) throws Exception {
        return systemInformation.getRightsList();
    }


    /**
     * Return all the report spaces configured for this instance of Report Data Store
     *
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getInternalListOfReportSpaces(HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalListOfReportSpaces()");
        return systemInformation.getReportSpaces();
    }

    /**
     * Return the configuration details for the specified report space
     *
     * @param reportSpace the report space configuration to return (any of the report spaces configured for this instance)
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/details", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ReportSpace getInternalReportSpace(
            @PathVariable String reportSpace) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportSpace()");
        return systemInformation.getReportSpace(reportSpace);
    }

    /**
     * The list of all channels available for the specified report space
     *
     * @param reportSpace The reporting space for which the allowed channels should be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/channels", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getInternalChannelsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalChannelsForReportSpace()");
        return systemInformation.getReportSpaceChannels(reportSpace);
    }


    /**
     * The list of all possible report states, for a given reporting space configuration
     *
     * @param reportSpace The reporting space configuration for which the possible (or allowed) states must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/states", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<State> getInternalStatesForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalStatesForReportSpace()");
        return systemInformation.getReportSpaceStates(reportSpace);
    }


    /**
     * The list of all valid state transitions (including user actions) configured for the provided reporting space.
     *
     * @param reportSpace The reporting space for which the full set of configured state transitions must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reportSpace/{reportSpace}/transitions", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getInternalTransitionsForReportSpace(
            @PathVariable String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalTransitionsForReportSpace()");
        return systemInformation.getReportSpaceTransitions(reportSpace);
    }


    //-------------------------------//
    //----- Channel Interfaces  -----//

    /**
     * The complete list of configured channel names for the provided reporting space
     *
     * @param reportSpace The reporting space for which all configured channels must be returned
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getInternalChannels(
            @RequestParam(required = false) String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalChannels()");

        if (reportSpace != null) {
            return systemInformation.getReportSpaceChannels(reportSpace);
        }
        return systemInformation.getChannelNames();
    }

    /**
     * The list of all configured states that are applicable to the report space relating to the provided channel name.
     *
     * @param channelName The channel name for which all possible, configured report states may be applied or expected.
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/states", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<State> getInternalStatesForChannel(
            @PathVariable String channelName,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalStatesForChannel()");
        return systemInformation.getChannelStates(channelName);
    }


    /**
     * The list of all transitions that can be applied for a given channel name (and if provided, specified report state).
     * The transitions that will be returned are all those configured transitions (including user actions/events) belonging to the
     * related reporting space, where the channel name exists in the allowed channels list or where the allowed channels list is empty or null,
     * and if the state is specified - where the current state list for the respective transitions are empty or null or contain the state specified.
     *
     * @param channelName The channel to/in which the relevant transitions may be applied
     * @param state       (optional) The current state for which the transitions may be applied
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/transitions", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getInternalTransitionsForChannel(
            @PathVariable String channelName,
            @RequestParam(required = false) String state,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalTransitionsForChannel()");
        return systemInformation.getChannelTransitionsForState(channelName, state);
    }

    /**
     * The list of all user events which can be applied for the given channel, and if provided - for the current report state stipulated.
     * The transitions that will be returned are all those configured transitions which are user actions/events, belonging to the
     * reporting space related to the specified channel name, where the channel name exists in the allowed channels list or where the allowed channels list is empty or null,
     * ...and where if the state is specified - the current state list for the respective transitions are empty or null or contain the state specified.
     *
     * @param channelName The channel to/in which the relevant user actions/events may be applied
     * @param state       (optional) The current state for which the user actions/events may be applied
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/channel/{channelName}/user_events", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<Transition> getInternalUserActionsForChannel(
            @PathVariable String channelName,
            @RequestParam(required = false) String state,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalUserActionsForChannel()");
        return systemInformation.getChannelUserActionsForState(channelName, state);
    }

    //-------------------------------//


    /**
     * The list of all configured user actions/events for the current instance
     *
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/user_events", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    List<String> getInternalUserEvents(
//            @RequestParam(required = true) String reportSpace,
            HttpServletResponse response) throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalUserEvents()");
        return systemInformation.getUserEventNames();
    }


    /**
     * the list of all configured report lists for the current instance
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/lists", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    List<String> getProducerReportLists() throws Exception {
        SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getProducerReportLists()");
        return systemInformation.getListNames();
    }


    /**
     * the full list definition/configuration for the specified list name
     *
     * @param listName the name of the list for which the configured list definition should be returned
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/report/list/{listName}", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    ListData getProducerReportList(
            @PathVariable String listName
    ) throws Exception {

        return systemInformation.getListDataByName(listName);
    }

    @RequestMapping(value = "/report/list/complete", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasRole('VIEW_TYPES')")
    public
    @ResponseBody
    Map<String, ListData> getProducerReportList(
    ) throws Exception {
        return systemInformation.getAllListDetails();
    }

    @RequestMapping(value = "/report/list/extConfig", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    JSObject getDisplayConfig(
            @RequestParam String name
    ) throws Exception {
        return systemInformation.getExtConfig(name);
    }

    @RequestMapping(value = "/report/list/extConfig", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody

    void setDisplayConfig(
            @RequestParam String name,
            @RequestBody JSObject configData

    ) throws Exception {
        systemInformation.setExtConfig(name, configData);
    }
}
