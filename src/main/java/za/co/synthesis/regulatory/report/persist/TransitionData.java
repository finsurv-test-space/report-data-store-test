package za.co.synthesis.regulatory.report.persist;

import za.co.synthesis.regulatory.report.support.EventType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 7/26/17.
 */
public class TransitionData {
  private EventType event;
  private String action;
  private final List<String> reportSpaces;
  private final List<String> channels;
  private final List<String> currentStates;
  private String newState;

  public TransitionData() {
    this.reportSpaces = new ArrayList<String>();
    this.channels = new ArrayList<String>();
    this.currentStates = new ArrayList<String>();
  }

  public TransitionData(EventType event, String action, List<String> reportSpaces, List<String> channels, List<String> currentStates, String newState) {
    this.event = event;
    this.action = action;
    this.reportSpaces = reportSpaces;
    this.channels = channels;
    this.currentStates = currentStates;
    this.newState = newState;
  }

  public EventType getEvent() {
    return event;
  }

  public String getAction() {
    return action;
  }

  public List<String> getReportSpaces() {
    return reportSpaces;
  }

  public List<String> getChannels() {
    return channels;
  }

  public List<String> getCurrentStates() {
    return currentStates;
  }

  public String getNewState() {
    return newState;
  }

  public void setEvent(EventType event) {
    this.event = event;
  }

  public void setNewState(String newState) {
    this.newState = newState;
  }
}
