package za.co.synthesis.regulatory.report.dao;

import za.co.synthesis.mapping.source.ISourceData;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 1/18/16
 * Time: 3:07 AM
 * Jdbc specific source data
 */
public class JdbcSourceData implements ISourceData {
  private final int level;
  private final List<Map<String, String>> level1Buffer;
  private final List<List<Map<String, String>>> level2Buffer;

  public JdbcSourceData(int level, List<Map<String, String>> level1Buffer, List<List<Map<String, String>>> level2Buffer) {
    this.level = level;
    this.level1Buffer = level1Buffer;
    this.level2Buffer = level2Buffer;
  }

  @Override
  public int getRowCount() {
    return level1Buffer.size();
  }

  @Override
  public int getRowCount(int indexLvl1) {
    return level2Buffer == null ? 0 : (((level2Buffer.size() <= indexLvl1) || (level2Buffer.get(indexLvl1) == null)) ? 0 : level2Buffer.get(indexLvl1).size());
  }

  @Override
  public int getLevel() {
    return level;
  }

  private String formatData(JdbcExternalConfig config, String data) {
    if (config != null && data != null && config.isDateFormat() && data.length() > 10) {
      return data.substring(0, 10);
    }
    return data;
  }

  @Override
  public String getDataPoint(Object externalConfig) {
    JdbcExternalConfig config = (JdbcExternalConfig)externalConfig;

    if (level1Buffer.size() > 0) {
      Map<String, String> row = level1Buffer.get(0);
      return formatData(config, row.get(config.getColumnName()));
    }
    return null;
  }

  @Override
  public String getDataPoint(int indexLvl1, Object externalConfig) {
    JdbcExternalConfig config = (JdbcExternalConfig)externalConfig;

    Map<String, String> row = level1Buffer.get(indexLvl1);
    return formatData(config, row.get(config.getColumnName()));
  }

  @Override
  public String getDataPoint(int indexLvl1, int indexLvl2, Object externalConfig) {
    JdbcExternalConfig config = (JdbcExternalConfig)externalConfig;

    List<Map<String, String>> rows = level2Buffer.get(indexLvl1);
    Map<String, String> row = rows.get(indexLvl2);
    return formatData(config, row.get(config.getColumnName()));
  }
}
