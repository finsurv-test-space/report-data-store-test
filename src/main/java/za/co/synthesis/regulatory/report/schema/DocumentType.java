package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.DocumentTypeData;
import za.co.synthesis.rule.core.Scope;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jake on 5/29/17.
 */
@JsonPropertyOrder({
        "Scope"
        , "Type"
        , "Description"
})
public class DocumentType {
  private final DocumentTypeData documentTypeData;

  public DocumentType() {
    this.documentTypeData = new DocumentTypeData();
  }

  public DocumentType(DocumentTypeData documentTypeData) {
    this.documentTypeData = documentTypeData;
  }

  public static List<DocumentType> wrapList(Collection<DocumentTypeData> dataList) {
    List<DocumentType> result = new ArrayList<DocumentType>();
    for (DocumentTypeData data : dataList) {
      result.add(new DocumentType(data));
    }
    return result;
  }

  @JsonProperty("Scope")
  public Scope getScope() {
    return documentTypeData.getScope();
  }

  @JsonProperty("Type")
  public String getType() {
    return documentTypeData.getType();
  }

  @JsonProperty("Description")
  public String getDescription() {
    return documentTypeData.getDescription();
  }

  public void setScope(Scope scope) {
    documentTypeData.setScope(scope);
  }

  public void setType(String type) {
    documentTypeData.setType(type);
  }

  public void setDescription(String description) {
    documentTypeData.setDescription(description);
  }

  @JsonIgnore
  public DocumentTypeData getDocumentTypeData() {
    return documentTypeData;
  }
}
