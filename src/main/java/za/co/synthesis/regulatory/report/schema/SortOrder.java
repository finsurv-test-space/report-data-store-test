package za.co.synthesis.regulatory.report.schema;

/**
 * Created by james on 2017/10/05.
 */
public enum SortOrder {
  Ascending("ASC"),
  Descending("DESC")
  ;

  private final String text;

  /**
   * @param text
   */
  private SortOrder(final String text) {
    this.text = text;
  }

  /* (non-Javadoc)
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
    return text;
  }
}
