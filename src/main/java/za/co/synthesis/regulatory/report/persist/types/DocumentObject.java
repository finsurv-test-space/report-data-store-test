package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.persist.DocumentData;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.persist.ReportEventContext;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.ReportServicesAuthentication;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.rule.core.Scope;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Map;

import static za.co.synthesis.regulatory.report.businesslogic.DataLogic.parseIntDef;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.jsonStrToMap;
import static za.co.synthesis.regulatory.report.transform.JsonSchemaTransform.mapToJsonStr;

/**
 * Created by james on 2017/08/31.
 */
@Entity
public class DocumentObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(DocumentObject.class);

  private String documentHandle;
  private String reportSpace;
  private String reportKey;
  private String scope;
  private String type;
  private String description;
  private Integer sequence;
  private Integer subSequence;
  private String fileType;
  private String fileName;
  private byte[] dataContent;
  private String authToken;
  private Map<String, Object> eventContext;
  private Integer version = 1;


  public DocumentObject(String uuid) {
    this(uuid, null, null, null);
  }

  public DocumentObject(String uuid, DocumentData doc, FileData data) {
    this(uuid, doc, data, null);
  }

  public DocumentObject(String uuid, DocumentData doc, FileData data, ReportEventContext eventContext) {
    super(uuid);
    setDocumentData(doc);
    setFileData(data);

    try {
      //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
      ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
      if (user != null) {
        eventContext = new ReportEventContext(user, LocalDateTime.now());
      }
    } catch (Exception e) {
    }
    this.eventContext = SerializationTools.serializeEventContext(eventContext);
  }


  public FileData getData() {
    return new FileData(fileType, fileName, dataContent);
  }


//  public byte[] getDataContent() {
//    return dataContent;
//  }

  public String getDataContent() {
    return new String(DataLogic.Base64Encode(dataContent));
  }

  public void setDataContent(byte[] dataContent) {
    this.dataContent = dataContent;
  }


  public void setDataContent(String dataContent) {
    this.dataContent = DataLogic.Base64Decode(dataContent.getBytes()
    );
  }

  public void setFileData(FileData data) {
    if (data != null) {
      this.fileType = data.getContentType();
      this.fileName = data.getFilename();
      this.dataContent = data.getContent();
    }
  }

  public static Scope getScopeEnum(String scope){
    Scope scopeEnum = Scope.Transaction;
    if (scope != null){
      if (scope.equals(Scope.ImportExport.toString())){
        scopeEnum = Scope.ImportExport;
      } else if (scope.equals(Scope.Money.toString())){
        scopeEnum = Scope.Money;
      }
    }
    return scopeEnum;
  }

  public DocumentData getDocumentData() {
    DocumentData doc = new DocumentData(documentHandle, reportSpace, reportKey, getScopeEnum(scope), type, description, sequence, subSequence);
    doc.setFilename(this.fileName);
    doc.setFileType(this.fileType);
    return doc;
  }

  public void setDocumentData(DocumentData doc){
    if (doc != null) {
      this.documentHandle = doc.getDocumentHandle();
      this.reportSpace = doc.getReportSpace();
      this.reportKey = doc.getTrnReference();
      this.scope = doc.getScope()==null?Scope.Transaction.toString():doc.getScope().toString();
      this.type = doc.getType();
      this.sequence = doc.getSequence();
      this.subSequence = doc.getSubSequence();

      this.description = doc.getDescription();

      this.fileType = doc.getFileType();
      this.fileName = doc.getFileName();
    }
  }

  public String getDocumentHandle() {
    return documentHandle;
  }

  public void setDocumentHandle(String documentHandle){
    this.documentHandle = documentHandle;
  }

  public String getReportSpace() {
    return this.reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getReportKey(){
    return reportKey;
  }

  public void setReportKey(String reportKey){
    this.reportKey = reportKey;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getContent(){
    JSObject jsonMap = new JSObject();

    jsonMap.put("Scope", scope);
    jsonMap.put("Type", type);
    jsonMap.put("Description", description);
    jsonMap.put("Sequence", sequence);
    jsonMap.put("SubSequence", subSequence);
    jsonMap.put("User", SecurityHelper.getAuthMeta().getName());

    if (dataContent != null && dataContent.length > 0) {
      jsonMap.put("FileName", fileName);
      jsonMap.put("ContentType", fileType);
//      jsonMap.put("Content", new String(DataLogic.Base64Encode(dataContent)));
    }
    if (!jsonMap.isEmpty()) {
      return mapToJsonStr(jsonMap);
    }
    return null;
  }

  public void setContent(String contentJson){
    if (contentJson != null && !contentJson.isEmpty()){
      try {
        Map jsonMap = jsonStrToMap(contentJson);

        Object obj = jsonMap.get("Scope");
        this.scope = obj!=null?obj.toString():null;
        obj = jsonMap.get("Type");
        this.type = obj!=null?obj.toString():null;
        obj = jsonMap.get("Description");
        this.description = obj!=null?obj.toString():null;
        obj = jsonMap.get("Sequence");
        this.sequence = obj!=null?parseIntDef(obj.toString(), 0):0;
        obj = jsonMap.get("SubSequence");
        this.subSequence = obj!=null?parseIntDef(obj.toString(), 0):0;


        obj = jsonMap.get("FileName");
        this.fileName = obj!=null?obj.toString():null;
        obj = jsonMap.get("ContentType");
        this.fileType = obj!=null?obj.toString():null;
//        obj = jsonMap.get("Content");
//        String content = obj!=null?obj.toString():null;
//        if (content != null) {
//          this.dataContent = DataLogic.Base64Decode(content.getBytes());
//        }
      } catch (Exception e){
        logger.error("Unable to deserialize json content for Document object: "+e.getMessage(), e);
      }
    }
  }


  public String getAuthToken() {

    if (this.eventContext == null) {
      try {
        //...this may throw null pointer exceptions - despite the code explicitly checking for such cases.seems to be a spring thing.
        ReportServicesAuthentication user = SecurityHelper.getAuthMeta();
        if (user != null) {
          eventContext = SerializationTools.serializeEventContext(new ReportEventContext(user, LocalDateTime.now()));
        }
      } catch (Exception e) {
        //we seem to be running into null pointers where we really shouldn't be.
        logger.warn("Unable to persist the ReportObject eventContext / Audit detail : " + e.getMessage());
        e.printStackTrace();
      }
    }

    return mapToJsonStr(eventContext);
  }

  public void setAuthToken(String authTokenJson) {
    try {
      this.eventContext = jsonStrToMap(authTokenJson);
    } catch (Exception e){
      logger.error("Unable to deserialize json content for Event Context object: " + e.getMessage(), e);
    }
  }


}
