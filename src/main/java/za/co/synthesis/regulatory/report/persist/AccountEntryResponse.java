package za.co.synthesis.regulatory.report.persist;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "TrnReference"
        , "Result"
        , "Message"
        , "AccountEntry"
})
public class AccountEntryResponse {
    private String trnReference;
    private String result;
    private String message;
    private AccountEntryData accountEntryData;

    public AccountEntryResponse(@JsonProperty("TrnReference") String trnReference,@JsonProperty("Result") String result, @JsonProperty("Message") String message, @JsonProperty("AccountEntry") AccountEntryData accountEntryData){
        this.trnReference = trnReference;
        this.result = result;
        this.message = message;
        this.accountEntryData = accountEntryData;
    }

    @JsonProperty("TrnReference")
    public String getTrnReference(){return this.trnReference;}

    @JsonProperty("Result")
    public String getResult(){return this.result;}

    @JsonProperty("Message")
    public String getMessage(){return this.message;}

    @JsonProperty("AccountEntry")
    public AccountEntryData getAccountEntryData(){return this.accountEntryData;}
}
