package za.co.synthesis.regulatory.report.rest;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ParameterData;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.persist.ValidationData;
import za.co.synthesis.regulatory.report.persist.utils.SerializationTools;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.*;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("producer/api")
public class FormApi extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Lookup.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private JsonSchemaTransform jsonSchemaTransform;

  @Autowired
  private DataLogic dataLogic;

  @Autowired
  private IChannelCache channelCache;

  @Autowired
  private RulesCache rulesCache;


  @Autowired
  ApplicationContext ctx;


  public void setSystemInformation(SystemInformation systemInformation) {
    this.systemInformation = systemInformation;
  }

  public void setJsonSchemaTransform(JsonSchemaTransform jsonSchemaTransform) {
    this.jsonSchemaTransform = jsonSchemaTransform;
  }

  public void setDataLogic(DataLogic dataLogic) {
    this.dataLogic = dataLogic;
  }

  public void setChannelCache(IChannelCache channelCache) {
    this.channelCache = channelCache;
  }

  public void setRulesCache(RulesCache rulesCache) {
    this.rulesCache = rulesCache;
  }

  public void setCtx(ApplicationContext ctx) {
    this.ctx = ctx;
  }


  public FormApi(SystemInformation systemInformation, JsonSchemaTransform jsonSchemaTransform, DataLogic dataLogic, IChannelCache channelCache, RulesCache rulesCache, ApplicationContext ctx) {
    this.systemInformation = systemInformation;
    this.jsonSchemaTransform = jsonSchemaTransform;
    this.dataLogic = dataLogic;
    this.channelCache = channelCache;
    this.rulesCache = rulesCache;
    this.ctx = ctx;
  }

  private static String authorisationHeader(Authentication authentication) {
//        Authentication authentication = SecurityHelper.getAuthMeta();
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      String currentUserName = authentication.getName();
      Object credentials = authentication.getCredentials();
      if (credentials != null) {
        return "Basic " + Base64.encode((currentUserName + ":" + credentials.toString()).getBytes());
      }
    }
    return "";
  }
  
  /**
   * Provides a self-contained javascript artefact to be embedded, which will render the relevant bop form with the relevant rules, partials and styling relating to the specified channel package.
   * @param channelName The channel package rules to apply in the client-side validations of the data provided, as well as the set of related partials and styling artefacts to package into the resulting javascript
   * @param trnReference The trnReference for the resulting report data (can be derived from supplied report data object)
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/js", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getFormAPIExternal(@RequestParam String channelName,
                          @RequestParam(required = false) String trnReference,
                          @RequestParam(required = false) Boolean packedRules,
                          @RequestParam(required = false) Boolean allowNew,
                          HttpServletResponse response) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
    getFormAPI(channelName, trnReference, packedRules, response, systemInformation, dataLogic, channelCache, rulesCache, allowNew);

  }

  public static String makeExtValidationString(String channelName, RulesCache rulesCache, boolean asJSVariable) {
    JSArray jsEndpoints = new JSArray();
    List<ValidationData> validations =
            rulesCache.getValidations(channelName, PackageCache.CacheStrategy.CacheOrSource);

    for (ValidationData validation : validations) {
      JSObject jsEndpoint = new JSObject();
      jsEndpoint.put("name", validation.getName());
      jsEndpoint.put("endpoint", validation.getEndpoint());
      jsEndpoint.put("failOnError", validation.getFailOnError());
      jsEndpoint.put("failOnBusy", validation.getFailOnBusy());
      JSArray jsParameters = new JSArray();
      for (ParameterData param : validation.getParameters()) {
        JSObject jsParameter = new JSObject();
        jsParameter.put("name",param.getName());
        jsParameter.put("path",param.getPath());
        jsParameters.add(jsParameter);
      }
      jsEndpoint.put("parameters",jsParameters);
      jsEndpoints.add(jsEndpoint);

    }

    JSWriter jsWriter = new JSWriter();
    jsWriter.setQuoteAttributes(true);
    jsWriter.setIndent("  ");
    jsWriter.setNewline("\n");

    jsEndpoints.compose(jsWriter);
    String config = jsWriter.toString();
    String out = (asJSVariable?"validationParams = ":"")+config;
    out += (asJSVariable?";":"");

    return out;
  }

  public static String getFormAPIContent(String channelName,
                                String trnReference,
                                Boolean packedRules,
                                SystemInformation channelInformation,
                                DataLogic dataLogic,
                                IChannelCache channelCache,
                                RulesCache rulesCache,
                                Boolean allowNew) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;

    Channel channel = channelInformation.getChannel(channelName);
    if (channel == null)
    {
      throw new PreconditionFailedException("A valid channelName must be provided.");
    }
    String reportSpace = channel.getReportSpace();

    JSObject jsReportData = new JSObject();
    jsReportData.put("ChannelName", channelName);

    if (trnReference != null) {
      ReportInfo data = dataLogic.getReport(reportSpace, trnReference);
      if (data != null) {
        JSObject jsMeta = JsonSchemaTransform.mapToJson(data.getReportData().getMeta());
        JSObject jsReport = JsonSchemaTransform.mapToJson(data.getReportData().getReport());
        jsReportData.put("Meta", jsMeta);
        jsReportData.put("Report", jsReport);
      } else if(allowNew) {
        jsReportData.put("Meta", "{}");
        jsReportData.put("Report", "{}");
      } else {
        throw new ResourceNotFoundException("Report with trnReference '" + trnReference + "' was not found");
      }
    } else {
      jsReportData.put("Meta", "{}");
      jsReportData.put("Report", "{}");
    }
    JSWriter jsWriter = new JSWriter();
    jsWriter.setQuoteAttributes(true);
    jsReportData.compose(jsWriter);

    String validations = makeExtValidationString(channelName, rulesCache, true);
  
    //resolve null packedRules instance.
    packedRules = packedRules==null?false:packedRules;

    //TODO: Align the below injection of artefacts to the BOP Form getReportAPI_Packed function located in the bopserver/src/bootstrapper.ts file.
    /*
    The bootstrapper function is looking for the evaluation, validation and document items to be split into the separate components and injected as such.

    require([
      "data/lookups",
      "evaluation/evalRules",
      "evaluation/evalContext",
      "evaluation/evalScenarios",
      "validation/transaction",
      "validation/money",
      "validation/importexport",
      'document/transaction',
      'document/money',
      'document/importexport',
    ]

     */

    String resultJS = validations.concat("(function(){\n")
        .concat(channelCache.getVendorCode()).concat("\n")
        .concat(channelCache.getPackage(channelName)).concat("\n")
        .concat(
            rulesCache.getLookups(channelName, PackageCache.CacheStrategy.CacheOrSource).getData()
                .replace("define(", "define(\"data/lookups\",")
        ).concat("\n")
        .concat(packedRules?getPackedRulesStr(channelName, rulesCache):"")
        .concat(packedRules?getPackedDocumentRulesStr(channelName, rulesCache):"")
        .concat(packedRules?getPackedEvaluationRulesStr(channelName, rulesCache):"")
        .concat(channelCache.getAppCode()).concat("\n")
        .concat("; window.ReportInfo = " + jsWriter.toString() + ";")
        .concat("\nwindow.define = define;\n})();\n");
    return resultJS;
  }


  public static void getFormAPI(String channelName,
                                String trnReference,
                                Boolean packedRules,
                                HttpServletResponse response,
                                SystemInformation channelInformation,
                                DataLogic dataLogic,
                                IChannelCache channelCache,
                                RulesCache rulesCache,
                                Boolean allowNew) throws Exception {
    response.setHeader("Content-Type","application/javascript; charset=UTF-8");
    String resultJS = getFormAPIContent(channelName, trnReference, packedRules, channelInformation, dataLogic, channelCache, rulesCache, allowNew);
    dataLogic.addChecksumHeaders(resultJS, response);
    response.getWriter().print(resultJS);
  }

  private static String getPackedRulesStr(String channelName, RulesCache rulesCache) {
    return
                rulesCache.getValidationRules(channelName, PackageCache.CacheStrategy.CacheOrSource).getData()
                    .replace("define(", "define('rules/validation',");

  }

  private static String getPackedDocumentRulesStr(String channelName, RulesCache rulesCache) {
    return rulesCache.getDocumentRules(channelName, PackageCache.CacheStrategy.CacheOrSource).getData()
              .replace("define(", "define('rules/document',");
  }

  private static String getPackedEvaluationRulesStr(String channelName, RulesCache rulesCache) {
    return rulesCache.getEvaluationRules(channelName, PackageCache.CacheStrategy.CacheOrSource).getData()
              .replace("define(", "define('rules/evaluation',");
  }

  /**
   * Submission end-point for an embedded javascript bop form.
   * @param channelName The channel package rules to apply in the server-side validations of the data provided, and the related report space into which the data should be stored
   * @param trnReference (optional) The report reference to be used (can be derived from the report data if not provided)
   * @param report The report data submitted by the form, in the schema stipulated.
   * @return  <b>PLEASE NOTE:</b>  The return type includes a revised parameter return list.  This is due to an internal naming convention update which has deprecated all instances of "<b>reportingSpace</b>" and replaces them with "<b>reportSpace</b>"  Please ensure that for all usages of this API, that the fields are used/ignored accordingly.
   * @throws Exception
   */
  @RequestMapping(value = "/js", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  public
  @ResponseBody
  ValidationResult setForm(
      @RequestParam String channelName,
      @RequestParam(required = false) String trnReference, //TODO: shouldn't this be encoded into the report body and read from there?  This can probably be entirely ommitted.
      @RequestBody Map<String, Object> report) throws Exception {

    Channel channel = systemInformation.getChannel(channelName);
    if (channel == null)
      throw new PreconditionFailedException("A valid channelName must be specified");

    if (report == null)
      throw new PreconditionFailedException("Valid json report data must be submitted");

    ReportData reportData = new ReportData((Map)report.get("Meta"), (Map)report.get("Report"));
    String schema = "genv3";
    try {
      schema = (jsonSchemaTransform != null ? jsonSchemaTransform.getReportSchema(reportData.getReport(), "sarb") : "sarb");
    } catch (Exception e) {
      logger.error("Unable to determine report schema automatically, defaulting to 'genv3' schema : "+e.getMessage(), e);
    }

    //TODO: REMOVE THIS DEBUG OUTPUT...
    logger.trace("Data Received("+channelName+ (trnReference!=null?"->"+trnReference:"") +") : \n\n"+JsonSchemaTransform.mapToJsonStr(report)+"\n\n");

    ValidationResult validationResult = dataLogic.setReportData(schema, channelName, reportData, SecurityHelper.getAuthMeta());

    //TODO: REMOVE THIS DEBUG OUTPUT...
    String text = "";
    int cnt = 1;
    for (ValidationResponse response : validationResult.getValidations()){
      text += "###  "+cnt+"  ###\n";
      text += "scope: "+response.getScope()+"\n";
      text += "money: "+response.getMoneyInstance()+"\n";
      text += "import-export: "+response.getImportExportInstance()+"\n";
      text += "type: "+response.getType()+"\n";
      text += "code: "+response.getCode()+"\n";
      text += "name: "+response.getName()+"\n";
      text += "message: "+response.getMessage()+"\n\n";
      cnt++;
    }
    logger.trace("Validation Responses ("+trnReference+"):\n"+text);

    return validationResult;
  }
  
  /**
   * Submission end-point for a bop form.
   * @param schema Data schema which the bop form will be submitting and which will be stored in the system
   * @param channelName The channel package rules to apply in the server-side validations of the data provided, and the related report space into which the data should be stored
   * @param data The report data submitted by the form, in the schema stipulated.
   * @param response
   * @return
   * @throws Exception
   */
  @RequestMapping(value = "/submit/{schema}", method = RequestMethod.POST, produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  ValidationResult submitFormData(@PathVariable String schema,
                                  @RequestParam (required = true) String channelName,
                                  @RequestBody Map data,
                                  HttpServletResponse response) throws Exception{
    SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "submitFormData()");
    if (data != null) {
      /*TODO: we will need to flesh out a datatype with some sort of minimum mandatory information
          IE: Version, Report, Meta...
       */
      String branch = "Version";
      if (data.containsKey(branch)){
        logger.debug("Form data version: {}", data.getOrDefault(branch, "UNKNOWN"));
      }
      //JSObject formData = JsonSchemaTransform.mapToJson(data);
      Map <String, Object> metaData = null;
      Map <String, Object> reportData = null;
      //fetch Report data
      branch = "Report";
      if (data.containsKey(branch)){
        Object branchObj = data.get(branch);
        if (branchObj instanceof Map) {
          reportData = (Map) branchObj;
        }
      }
      //fetch Meta data
      branch = "Meta";
      if (data.containsKey(branch)){
        Object branchObj = data.get(branch);
        if (branchObj instanceof Map) {
          metaData = (Map) branchObj;
        }
      }
      if (reportData == null) {
        //"Report" data is not supplied - check for "transaction" instead...
        branch = "transaction";
        if (data.containsKey(branch)){
          Object branchObj = data.get(branch);
          if (branchObj instanceof Map) {
            reportData = (Map) branchObj;
          }
        }
      }
      if (metaData == null) {
        //"Meta" data is not supplied - check for "customData" instead...
        branch = "customData";
        if (data.containsKey(branch)){
          Object branchObj = data.get(branch);
          if (branchObj instanceof Map) {
            metaData = (Map) branchObj;
          }
        }
      }
      if (reportData != null) {
        ReportData report = new ReportData(metaData, reportData);
        return dataLogic.setReportData(schema, channelName, report, SecurityContextHolder.getContext().getAuthentication());
      }
      //TODO: perform configured actions with remaining data (if any)
    }
    throw new PreconditionFailedException("One or more of the minimum mandatory fields were absent from the data provided: 'Meta' & 'Report', or the legacy 'transaction' & 'customData'");
  }
  
  
  /**
   * This returns a complete javascript artefact containing all the necessary rules, validations, partials, logic and styling - derived from the channel package specified
   * @param channelName The channel package name containing the relevant rule set and associated artefacts to be injected into the resulting javascript
   * @param trnReference The trnReference to configure the javascript with
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "/js/document", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getDocumentDownloadAPIExternal(@RequestParam String channelName,
                          @RequestParam(required = true) String trnReference,
                          HttpServletResponse response) throws Exception {
    response.setHeader("Content-Type","application/javascript; charset=UTF-8");
    URL url = Resources.getResource("pages/index.js");
    //TODO: the below should probably be templated somehow as an externally configurable file per channel...
    response.getWriter().print(
        "(function(){\n" +
        "window.trnReference = '" + trnReference.replaceAll("'","") + "';\n" +
            "window.channel = '" + channelName.replaceAll("'","") + "';\n" +
            "window.baseUrl = window.baseUrl?window.baseUrl:'" + ((FileChannelCache)channelCache).getUiContextAndProxyPath() + "';\n" +
            Resources.toString(url, Charsets.UTF_8) +
            "})();\n"
    );
  }
  
  /**
   * This end point returns the stand-alone document upload form.
   * Typical use would be to provide an url to an iFrame (or clickable link) with the necessary parameters to render the form for a specific BOP transaction which requires document uploads.
   * <br/>
   * This particular end-point has been moved to the "Form" API group (which provide complete, stand-alone forms on request) and is thus deprecated.
   * The "Form-api" group is targeted for the end-points associated with providing the form javascript artefacts which would be embedded.
   * <br/>
   * ...please use the <b><i>producer/api/form/document</i></b> api
   * @param channelName The channel package name containing the relevant rule set and associated with the required report space
   * @param trnReference The report reference (for a report currently stored in the system) for which the required documents list should be generated
   * @param request
   * @param response
   * @throws Exception
   */
  @Deprecated
  @RequestMapping(value = "/js/document/form", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getDocumentDownloadFormExternal(@RequestParam String channelName,
                                      @RequestParam(required = true) String trnReference,
                                       @RequestParam(required = false) String proxyPath,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
    Form.getDocumentDownloadFormGeneric(channelName, trnReference, request, response, systemInformation, channelCache, proxyPath);
  }
}
