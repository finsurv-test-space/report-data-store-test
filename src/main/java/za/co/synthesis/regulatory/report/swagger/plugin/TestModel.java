package za.co.synthesis.regulatory.report.swagger.plugin;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.classmate.types.ResolvedPrimitiveType;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ModelBuilder;
import springfox.documentation.builders.ModelPropertyBuilder;
import springfox.documentation.schema.ModelProperty;
import springfox.documentation.schema.ResolvedTypes;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.service.AllowableValues;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

/**
 * Created by jake on 6/3/17.
 */
//@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class TestModel implements ModelBuilderPlugin {
  private final TypeResolver typeResolver;
  private final TypeNameExtractor typeNameExtractor;

  public TestModel(TypeResolver typeResolver, TypeNameExtractor typeNameExtractor) {
    this.typeResolver = typeResolver;
    this.typeNameExtractor = typeNameExtractor;
  }

  @Override
  public void apply(ModelContext context) {
    ModelBuilder builder = context.getBuilder();

    Map<String, ModelProperty> props = new HashMap<String, ModelProperty>();
    props.put("Prop1", new ModelPropertyBuilder()
            .name("Prop1")
            .example("example prop")
            .description("prop desc")
            .qualifiedType("string")
            .type(typeResolver.resolve(String.class))
            .isHidden(false)
            .required(true)
            .build().updateModelRef(ResolvedTypes.modelRefFactory(context,typeNameExtractor)));

    builder.name("TestModel")
            .description("My First Model")
            .discriminator("discriminator")
            .example("example")
            .properties(props)
            .build();
  }

  @Override
  public boolean supports(DocumentationType delimiter) {
    if (delimiter.equals(SWAGGER_2))
      return true;
    return false;
  }
}