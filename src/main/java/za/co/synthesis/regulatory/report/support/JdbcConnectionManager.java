package za.co.synthesis.regulatory.report.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.persist.DataSourceData;

import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 9/14/17.
 * This is a simple class that centrally manages the creation and handing out of JdbcTemplate objects for specific
 * given DataSourceData
 */
public class JdbcConnectionManager {
  private static final  Logger log = LoggerFactory.getLogger(JdbcConnectionManager.class);

  private final Map<String, JdbcTemplate> jdccTemplateMap = new HashMap<String, JdbcTemplate>();
  private int minPoolSize = 2;
  private int maxPoolSize = 30;
  private int maxStatements = 0;
  private int idleConnectionTestPeriod = 3000;
  private int maxIdleTimeExcessConnections = 120;
  private int loginTimeout = 300;
  private int numHelperThreads = 3;
  private int maxConnectionAge = 14400;
  private int unreturnedConnectionTimeout = 20;
  private int queryTimeout = 120;

  public void setMinPoolSize(int minPoolSize) {
    this.minPoolSize = minPoolSize;
  }

  public void setMaxPoolSize(int maxPoolSize) {
    this.maxPoolSize = maxPoolSize;
  }

  public void setMaxStatements(int maxStatements) {
    this.maxStatements = maxStatements;
  }

  public void setIdleConnectionTestPeriod(int idleConnectionTestPeriod) {
    this.idleConnectionTestPeriod = idleConnectionTestPeriod;
  }

  public void setMaxIdleTimeExcessConnections(int maxIdleTimeExcessConnections) {
    this.maxIdleTimeExcessConnections = maxIdleTimeExcessConnections;
  }

  public void setLoginTimeout(int loginTimeout) {
    this.loginTimeout = loginTimeout;
  }

  public void setNumHelperThreads(int numHelperThreads) {
    this.numHelperThreads = numHelperThreads;
  }

  public void setMaxConnectionAge(int maxConnectionAge) {
    this.maxConnectionAge = maxConnectionAge;
  }

  public void setUnreturnedConnectionTimeout(int unreturnedConnectionTimeout) {
    this.unreturnedConnectionTimeout = unreturnedConnectionTimeout;
  }

  public void setQueryTimeout(int queryTimeout) {
    this.queryTimeout = queryTimeout;
  }

  private String getBeanName(DataSourceData dsData) {
    long fullhash = 0;
    fullhash += dsData.getDriverClassName().hashCode();
    fullhash += dsData.getUrl().hashCode();
    fullhash += dsData.getUsername().hashCode();
    return Long.toHexString(fullhash);
  }

  public JdbcTemplate getJdbcTemplate(DataSourceData dsData) {
    String jdbcTemplateBeanName = getBeanName(dsData);
    JdbcTemplate jdbcTemplate = jdccTemplateMap.get(jdbcTemplateBeanName);
    if (jdbcTemplate != null) {
      return jdbcTemplate;
    }

    com.mchange.v2.c3p0.ComboPooledDataSource dataSource = new com.mchange.v2.c3p0.ComboPooledDataSource();
    try {
      dataSource.setDriverClass(dsData.getDriverClassName());
      dataSource.setJdbcUrl(dsData.getUrl());
      dataSource.setUser(dsData.getUsername());
      dataSource.setPassword(dsData.getPassword());
      dataSource.setMinPoolSize(minPoolSize);
      dataSource.setMaxPoolSize(maxPoolSize);
      dataSource.setMaxStatements(maxStatements);
      dataSource.setIdleConnectionTestPeriod(idleConnectionTestPeriod);
      dataSource.setMaxIdleTimeExcessConnections(maxIdleTimeExcessConnections);
      dataSource.setLoginTimeout(loginTimeout);
      dataSource.setNumHelperThreads(numHelperThreads);
      dataSource.setMaxConnectionAge(maxConnectionAge);
      dataSource.setUnreturnedConnectionTimeout(unreturnedConnectionTimeout);

      jdbcTemplate = new JdbcTemplate(dataSource);
      jdbcTemplate.setQueryTimeout(queryTimeout);
      jdbcTemplate.setSkipUndeclaredResults(true);

      jdccTemplateMap.put(jdbcTemplateBeanName, jdbcTemplate);
      return jdbcTemplate;
    } catch (PropertyVetoException | SQLException e) {
      log.error("Cannot create new datasource for " + dsData.getUrl(), e);
    }
    return null;
  }

}
