package za.co.synthesis.regulatory.report.validate;

import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.HeaderData;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.RestData;
import za.co.synthesis.regulatory.report.schema.ValidationResponse;
import za.co.synthesis.regulatory.report.support.MapCustomValues;
import za.co.synthesis.rule.core.CustomValidateResult;
import za.co.synthesis.rule.core.ResultEntry;
import za.co.synthesis.rule.core.ResultType;
import za.co.synthesis.rule.core.StatusType;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by jake on 6/13/17.
 */
public class ValidationUtils {
  private final static Pattern dotPattern = Pattern.compile("\\.");

  public static SystemInformation systemInformation;

  public static MapCustomValues getCustomValues(Map<String, Object> meta) {
    if (meta == null) {
      meta = new JSObject();
    }
    if (!meta.containsKey("DealerType"))
      meta.put("DealerType", "AD");

    return new MapCustomValues(meta);
  }

  public static List<ValidationResponse> filterValidationResults(List<ResultEntry> validationResults) {
    List<ValidationResponse> result = new ArrayList<ValidationResponse>();

    if (validationResults != null) {
      for (ResultEntry resultEntry : validationResults) {
        if (resultEntry.getType() == ResultType.Error || resultEntry.getType() == ResultType.Warning)
          result.add(new ValidationResponse(resultEntry));
      }
    }
    return result;
  }

  private static void addValueToMap(String[] fields, String value, Map<String, Object> map) {
    if (fields.length == 1) {
      String key = fields[0];
      if (!map.containsKey(key)) {
        map.put(key, value);
      }
    }
    else
    if (fields.length > 1) {
      String key = fields[0];
      Map<String, Object> subMap = null;
      if (!map.containsKey(key)) {
        subMap = new HashMap<String, Object>();
        map.put(key, subMap);
      }
      else {
        Object obj = map.get(key);
        if (obj instanceof Map) {
          subMap = (Map)obj;
        }
      }
      if (subMap != null) {
        addValueToMap(Arrays.copyOfRange(fields, 1, fields.length), value, subMap);
      }
    }
  }

  public static String compose(String name, String str, Map<String, Object> parameterValues, IConfigStore configStore, Logger log) throws Exception {
    if (str != null) {
      Version version = new Version(2, 3, 23);
      Configuration config = new Configuration(version);
      Map<String, Object> context = new HashMap<String, Object>();
      context.putAll(parameterValues);
      context.put("logic", new FreemarkerLogic());
      context.put("util", new FreemarkerUtil());
      if (configStore != null) {
        Map<String, Object> configSettings = new HashMap<String, Object>();
        Properties p = configStore.getEnvironmentParameters();
        Enumeration e = p.propertyNames();
        while (e.hasMoreElements()) {
          String key = (String) e.nextElement();
          addValueToMap(dotPattern.split(key), p.getProperty(key), configSettings);
        }
        p = configStore.getEnvironmentProperties();
        e = p.propertyNames();
        while (e.hasMoreElements()) {
          String key = (String) e.nextElement();
          addValueToMap(dotPattern.split(key), p.getProperty(key), configSettings);
        }
        context.put("config",configSettings);
      }
      try {
        Template t = new Template(name, new StringReader(str), config);
        Writer out = new StringWriter();
        Environment env = t.createProcessingEnvironment(context, out);
        env.process();
        return out.toString();

      } catch (TemplateException | IOException e) {
        log.error("Cannot compose '" + name + "'", e);
        throw new Exception("Cannot compose '" + name + "'", e);
      }
    }
    else
      return null;
  }

  public static CustomValidateResult parseComposedResponse(String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      Object valRes = parser.parse();

      if (valRes instanceof Map) {
        Map valResMap = (Map)valRes;
        Object status = valResMap.get("status");
        Object code = valResMap.get("code");
        Object message = valResMap.get("message");

        StatusType statusType = StatusType.Fail;
        if (status != null) {
          if (status.equals("pass"))
            statusType = StatusType.Pass;
          if (status.equals("error"))
            statusType = StatusType.Error;

          return new CustomValidateResult(statusType, (String)code, (String)message);
        }
      }
    }
    return null;
  }

  public static String makeRESTCall(RestData rest, Map<String, Object> parameterValues, IConfigStore configStore, Logger log) throws Exception {
    String externalCallsDebugging = systemInformation.getConfigStore().getEnvironmentParameters().getProperty("externalCallsDebugging");
    int timeout = 10000; //default to 10 seconds
    if(parameterValues instanceof Map){
      for (Map.Entry<String, Object> entry : parameterValues.entrySet()){
        if ("timeout".equalsIgnoreCase(entry.getKey())){
          if (entry.getValue() instanceof String) {
            try {
              timeout = Integer.parseInt((String)entry.getValue());
            } catch (Exception err) {
              //meh.
            }
          }
          break;
        }
      }
    }
    try {
      URL url = new URL(ValidationUtils.compose(rest.getUrl(), rest.getUrl(), parameterValues, configStore, log));
      if("true".equalsIgnoreCase(externalCallsDebugging)){
        log.info("\n\nComposed URL: "+url.toString()+"\n");
      }
      HttpURLConnection httpCon = (HttpURLConnection)url.openConnection();
      if (rest.getHeaders() != null) {
        for (HeaderData headerData : rest.getHeaders()) {
          String value = ValidationUtils.compose(headerData.getName(), headerData.getValue(), parameterValues, configStore, log);
          if("true".equalsIgnoreCase(externalCallsDebugging)){
            log.info("\n\nPre-composed header value: "+headerData.getName()+ ":"+headerData.getValue());
            log.info("\nComposed header : "+value + "\n");
          }
          httpCon.setRequestProperty(headerData.getName(), value);
        }
      }

      int respCode = 200;
      IOException ioException = null;
      if (rest.getVerb() == RequestMethod.POST || rest.getVerb() == RequestMethod.PUT) {
        String body = ValidationUtils.compose(rest.getUrl(), rest.getBody(), parameterValues, configStore, log);
        if ("true".equalsIgnoreCase(externalCallsDebugging)) {
          log.info("Composed request body : " + body);
        }
        httpCon.setRequestMethod(rest.getVerb() == RequestMethod.POST ? "POST" : "PUT");
        httpCon.setDoOutput(true);
        httpCon.setConnectTimeout(timeout);
        try {
          OutputStream os = httpCon.getOutputStream();
          OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
          osw.write(body);
          osw.flush();
          osw.close();
          httpCon.connect();
        } catch (IOException iox) {
          ioException = iox;
        }
      } else {
        httpCon.setRequestMethod("GET");
        httpCon.connect();
      }
      respCode = httpCon.getResponseCode();
      boolean allowedResponse = false;
      if (respCode >= 200 && respCode < 300) {
        allowedResponse = true;
      } else {
        log.warn("Received HTTP response code: "+respCode);
      }
      if ((ioException != null || respCode >= 300 || respCode < 200) && rest.getAcceptHttpCodes() instanceof List && rest.getAcceptHttpCodes().size() >= 1) {
        if (rest.getAcceptHttpCodes().get(0) instanceof Integer) {
          for (Object allowedCode : rest.getAcceptHttpCodes()) {
            if (((Integer)allowedCode).equals(respCode)) {
              allowedResponse = true;
              log.warn("Response code ("+respCode+") allowed for further processing as per REST config...");
              break;
            }
          }
        } else if (rest.getAcceptHttpCodes().get(0) instanceof String) {
          for (Object allowedCode : rest.getAcceptHttpCodes()) {
            if (((String)allowedCode).equalsIgnoreCase(""+respCode)) {
              allowedResponse = true;
              log.warn("Response code ("+respCode+") allowed for further processing as per REST config...");
              break;
            }
          }
        }
      }
      if (!allowedResponse){
        log.warn("Response code ("+respCode+") recieved, but not allowed for processing.");
        if (ioException != null) {
          throw new Exception(ioException.getMessage(), ioException);
        } else {
          throw new Exception("Unexpected http response code, no ioException provided.");
        }
      }

      BufferedReader buffReader = new BufferedReader(new InputStreamReader((respCode >= 200 && respCode < 300)?httpCon.getInputStream():httpCon.getErrorStream()));

      StringBuilder sb = new StringBuilder();
      String line;
      while ((line = buffReader.readLine()) != null) {
        sb.append(line).append('\n');
      }
      buffReader.close();
      return sb.toString();
    } catch (IOException e) {
      log.warn("Cannot execute REST call on URL '" + rest.getUrl() + "'", e);
      throw e;
    } catch (Exception e) {
      log.warn("Cannot compose body for REST call on URL '" + rest.getUrl() + "'", e);
      throw e;
    }
  }


}
