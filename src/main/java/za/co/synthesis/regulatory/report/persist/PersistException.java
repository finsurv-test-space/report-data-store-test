package za.co.synthesis.regulatory.report.persist;

public class PersistException extends Exception {

  public PersistException() {
    super();
  }

  public PersistException(String message) {
    super(message);
  }

  public PersistException(String message, Exception cause) {
    super(message,cause);
  }

  public PersistException(Throwable cause) {
    super(cause);
  }
}