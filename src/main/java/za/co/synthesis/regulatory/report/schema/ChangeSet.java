package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
        "ChangeContext"
        , "ReportChanges"
        , "MetaChanges"
        , "StateChange"
        , "ReportData"
})
public class ChangeSet {
  private final Map<String, Object> eventContext;
  private final List<ChangeItem> reportChanges;
  private final List<ChangeItem> metaChanges;
  private final ChangeItem stateChange;
  private final ReportData reportData;

  public ChangeSet(Map<String, Object> eventContext, List<ChangeItem> reportChanges, List<ChangeItem> metaChanges, ChangeItem stateChange, ReportData reportData) {
    this.eventContext = eventContext;
    this.reportChanges = reportChanges;
    this.metaChanges = metaChanges;
    this.stateChange = stateChange;
    this.reportData = reportData;
  }

  @JsonProperty("ChangeContext")
  public Map<String, Object> getEventContext() {
    return eventContext;
  }

  @JsonProperty("MetaChanges")
  public List<ChangeItem> getMetaChanges() {
    return metaChanges;
  }

  @JsonProperty("ReportChanges")
  public List<ChangeItem> getReportChanges() {
    return reportChanges;
  }

  @JsonProperty("StateChange")
  public ChangeItem getStateChange()
  {
    return stateChange;
  }

  @JsonProperty("ReportData")
  public ReportData getReportSnapshot() {
    return reportData;
  }
}
