package za.co.synthesis.regulatory.report.service;

import org.apache.commons.cli.*;
import org.eclipse.jetty.security.UserStore;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import za.co.synthesis.regulatory.report.swagger.SwaggerDocFilter;

import javax.servlet.DispatcherType;
import javax.servlet.MultipartConfigElement;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumSet;

/**
 * Created by jake on 3/31/16.
 */
@EnableWebSecurity
public class StartJetty {
  static final Logger logger = LoggerFactory.getLogger(StartJetty.class);
  public static final String REALM = "FINSURV_REPORT_SERVICES";
  private static final UserStore USER_STORE = new UserStore();
  private static Server server;

  private static Options getOptions() {
    Options options = new Options();

    Option port = Option.builder("port")
            .hasArg()
            .desc("the post to listen on (default 8080)")
            .build();
    options.addOption(port);

    Option confidentialPort = Option.builder("confidentialPort")
            .hasArg()
            .desc("the post to listen on (default 8443)")
            .build();
    options.addOption(confidentialPort);

    Option help = Option.builder("help")
            .desc("provide help")
            .build();
    options.addOption(help);

    return options;
  }




  public static void main(String[] args) throws Exception {
    Options options = getOptions();
    // create the parser
    CommandLineParser parser = new DefaultParser();
    CommandLine cmdline;
    try {
      // parse the command line arguments
      cmdline = parser.parse(options, args);

      if (cmdline.hasOption("help")) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("report-data-store", options);
        return;
      }
    } catch (ParseException e) {
      System.err.println("Commandline parameters not valid.  Reason: " + e.getMessage());

      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("report-data-store", options);
      return;
    }

    int port = 8087;
    int confidentialPort = 8443;
    if (cmdline.hasOption("port")) {
      port = Integer.parseInt(cmdline.getOptionValue("port"));
    }
    if (cmdline.hasOption("confidentialPort")) {
      confidentialPort = Integer.parseInt(cmdline.getOptionValue("confidentialPort"));
    }

    server = new Server();

    // We expect the startip directory to be set to the base txstream folder
    ServerConnector connector = new ServerConnector(server, 10, 0);
    connector.setPort(port);

    server.setConnectors(new Connector[]{connector});

    ServletContextHandler handler = new ServletContextHandler(server, "/report-data-store", true, false);

    //*** TRY FORCE COOKIE PATHS TO ROOT (/) ***//
    handler.getSessionHandler().getSessionCookieConfig().setPath("/");


    String externalXMLpath = System.getProperty("spring-external-root-path");
    if (externalXMLpath != null){
      if (Files.isRegularFile(Paths.get(externalXMLpath))) {
        String filePath = Paths.get(externalXMLpath).toUri().toString();
        handler.setInitParameter("contextConfigLocation", filePath);
      }
    } else {
      handler.setInitParameter("contextConfigLocation", "file:./src/test/resources/report-service-external-root.xml");
    }
    handler.addEventListener(new ContextLoaderListener());
    handler.addFilter(SwaggerDocFilter.class, "/v2/api-docs", EnumSet.of(DispatcherType.REQUEST));

//    //Add Spring security filter:
//    handler.addFilter(
//            new FilterHolder(new DelegatingFilterProxy(AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME)),
//            "/*",
//            EnumSet.allOf(DispatcherType.class)
//    );


//    /* Add the Spring Security filter. */
//    handler.addFilter("SpringSecurityFilterChain",
//            new DelegatingFilterProxy()).addMappingForUrlPatterns(null,
//            false, "/*");


    DelegatingFilterProxy targetDelegateFilter = new DelegatingFilterProxy("reportServiceSecurityFilterChain");


    /* Add the Spring Security filter. */
    handler.addFilter(new FilterHolder(targetDelegateFilter), "/*",
            EnumSet.of(DispatcherType.REQUEST, DispatcherType.ERROR, DispatcherType.ASYNC));


    ServletHolder servletHolder = new ServletHolder("/", DispatcherServlet.class);
    servletHolder.setInitParameter("contextClass", "org.springframework.web.context.support.AnnotationConfigWebApplicationContext");
    servletHolder.setInitParameter("contextConfigLocation", "za.co.synthesis.regulatory.report.service.WebAppConfig");
    handler.addServlet(servletHolder, "/*");


    // Register a MultipartConfigElement
    // upload temp file will put here
    File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
  
    logger.debug("...Upload temp dir (java.io.tmpdir) set to: \"" + uploadDirectory + "\".");
  
    MultipartConfigElement multipartConfigElement = GetMultipartLimitConfigs(logger, uploadDirectory);
    
    servletHolder.getRegistration().setMultipartConfig(multipartConfigElement);
  
    server.start();
  }
  
  public static MultipartConfigElement GetMultipartLimitConfigs(Logger logger, File uploadDirectory) {
    //***  NOTE: For reference on upload limits see 3. With Servlet 3.0: http://www.baeldung.com/spring-file-upload  ***//
  
    int megabyteToBytes = 1024 * 1024;
    int maxFileSizeInMb = 10; // 10 MB
    String maxFileSizeInMbStr = System.getProperty("max-file-size");
    if (maxFileSizeInMbStr != null) {
      logger.debug("...Max individual file upload size system property (max-file-size) provided: \"" + maxFileSizeInMbStr + "\" megabytes.");
      try{
        maxFileSizeInMb = Integer.parseInt(maxFileSizeInMbStr);
      } catch(Exception e){
      }
    } else {
      logger.debug("...Max individual file upload size (max-file-size) defaulted ([10MB per file]): \"" + maxFileSizeInMb + "\" megabytes.");
    }
  
    int flushSizeInMb = maxFileSizeInMb / 5; // 2 MB
    String flushSizeInMbStr = System.getProperty("flush-threshold-size");
    if (flushSizeInMbStr != null) {
      logger.debug("...Flush to disk file upload threshold system property (flush-threshold-size) provided: \"" + flushSizeInMbStr + "\" megabytes.");
      try{
        flushSizeInMb = Integer.parseInt(flushSizeInMbStr);
      } catch(Exception e){
      }
    } else {
      logger.debug("...Flush to disk file upload threshold (flush-threshold-size) defaulted ([0.2] * [max-file-size]): \"" + flushSizeInMb + "\" megabytes.");
    }
  
    int maxUploadSizeInMb = 10 * maxFileSizeInMb; //assume a request may contain +-10 files of +-10MB
    String maxUploadSizeInMbStr = System.getProperty("max-upload-size");
    if (maxUploadSizeInMbStr != null) {
      logger.debug("...Max upload size system property (max-upload-size) provided: \"" + maxUploadSizeInMbStr + "\" megabytes.");
      try{
        maxUploadSizeInMb = Integer.parseInt(maxUploadSizeInMbStr);
      } catch(Exception e){
      }
    } else {
      logger.debug("...Max upload size (max-upload-size) defaulted ([10 files] * [max-file-size]): \"" + maxUploadSizeInMb + "\" megabytes.");
    }
  
    logger.debug("...Max *individual file* upload size (max-file-size) set to : \"" + (maxFileSizeInMb > 0 ? maxFileSizeInMb : 1) + "\" megabytes.");
    logger.debug("...Threshold *flush to disk* size (flush-threshold-size) set to : \"" + (flushSizeInMb > 0 ? flushSizeInMb : 1) + "\" megabytes.");
    logger.debug("...Max *request* upload size (max-upload-size) set to : \"" + (maxUploadSizeInMb > 0 ? maxUploadSizeInMb : 1) + "\" megabytes.");
  
    MultipartConfigElement multipartConfigElement =
        new MultipartConfigElement(uploadDirectory.getAbsolutePath(),
            (maxFileSizeInMb > 0 ? maxFileSizeInMb : 1) * megabyteToBytes,
            (maxUploadSizeInMb > 0 ? maxUploadSizeInMb : 1) * megabyteToBytes,
            (flushSizeInMb > 0 ? flushSizeInMb : 1) * megabyteToBytes);
  
    logger.info("\n\n" +
        "NOTE:\n" +
        "-----\n" +
        "To configure the maximum allowable POST payload, please see documentation for your container application \n\n" +
        "** Apache Tomcat documentation is available at:\n\thttps://tomcat.apache.org/tomcat-8.5-doc/config/http.html#Common_Attributes\n" +
        "For Tomcat, you would need to set the 'maxPostSize' param (measured in bytes) in the <connector> configuration.\n" +
        "For example - setting a maximum upload size of say 4Mb you would use the following: \n\tmaxPostSize=\"4194304\"\n\n");
    
    return multipartConfigElement;
  }
  
  public void stopServer(){
    try {
      server.stop();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
