package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.regulatory.report.schema.*;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceAccessAuthorizationException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 5/19/17.
 */
@CrossOrigin
@Controller
@RequestMapping("consumer/api/download")
public class Download extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Download.class);

  @Autowired
  private SystemInformation systemInformation;

  @Autowired
  private DataLogic dataLogic;

  public static Map<String, List<ReportInfo>> bulkReportList = new HashMap<>();

  @Autowired
  ApplicationContext ctx;


  /**
   * Bulk download API for retreiving (valid) BOP Reports from the system.
   * Requires either a valid Channel Name or Report Space to determine which pool of data to reference (for which regulator etc).
   * @param channelName (Mutually-optional) used to determine the default schema for the reports as well as the Report Space from which to pull the data.  Takes precedence over a provided Report Space.
   * @param reportSpace (Mutually-optional) if the channel name is not provided, then this parameter is used to determine the report pool to access and the default schema in which to provide them.
   * @param schema The schema to provide the requested reports in.  It may be that the reports are pushed into the system in various schemas - this provides a mechanism of unifying the data type across all records.
   * @param maxRecords Max records returned is defaulted to 10 if not otherwise stipulated - recommended to keep this number below 100.
   * @param syncPoint The syncpoint is used to determine the last received record from the download queue, and if provided, only those items in the download queue with a syncpoint greater than that provided will be returned (to the maximum count stipulated/defaulted).  If not provided, the function will return all records from the beginning of the download queue to the count specified.
   * @return Bulk Report Object in JSON format as per the schema stipulated, defaulted or as is currently stored in the database.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/reports", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  DownloadReports getReportDownloadsDefault(
                             @RequestParam(required=false) String channelName,
                             @RequestParam(required=false) String reportSpace,
                             @RequestParam(required=false) String schema,
                             @RequestParam(required = false) Integer maxRecords,
                             @RequestParam(required = false) String syncPoint) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.CONSUMER_API, "getReportDownloadsDefault()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }

    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }


    if (schema == null || schema.equalsIgnoreCase("default") || schema.trim().isEmpty()){
      //default to the channel default if possible...
      if (channel != null){
        schema = channel.getDefaultSchema();
      }
      //otherwise ... default to a hard "genv3" schema.
      if (schema == null || schema.equalsIgnoreCase("default") || schema.trim().isEmpty()) {
        schema = "genv3";
      }
    }
    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();

    ReportInfoDownload reports = dataLogic.getReports(reportSpace, syncPoint, maxRecords, schema);
    if (reports == null){
      throw new ResourceNotFoundException("No reports available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }
    List<ReportDataWithDownloadType> reportList = new ArrayList<ReportDataWithDownloadType>();
    if (reports!=null) {
      for (ReportInfo entry : reports.getReportInfoList()) {
        reportList.add(entry.getReportDataWithDownloadType());
      }
    }

    return new DownloadReports(reports==null?syncPoint:reports.getUpToSyncpoint(), reportList);
  }


  /**
   * Bulk download API for retreiving (valid) BOP Reports from the system.
   * Requires either a valid Channel Name or Report Space to determine which pool of data to reference (for which regulator etc).
   * @param channelName (Mutually-optional) used to determine the default schema for the reports as well as the Report Space from which to pull the data.  Takes precedence over a provided Report Space.
   * @param reportSpace (Mutually-optional) if the channel name is not provided, then this parameter is used to determine the report pool to access and the default schema in which to provide them.
   * @param schema (Mandatory path variable) The schema to provide the requested reports in.  It may be that the reports are pushed into the system in various schemas - this provides a mechanism of unifying the data type across all records.
   * @param maxRecords Max records returned is defaulted to 10 if not otherwise stipulated - recommended to keep this number below 100.
   * @param syncPoint The syncpoint is used to determine the last received record from the download queue, and if provided, only those items in the download queue with a syncpoint greater than that provided will be returned (to the maximum count stipulated/defaulted).  If not provided, the function will return all records from the beginning of the download queue to the count specified.
   * @return Bulk Report Object in JSON format as per the schema stipulated, defaulted or as is currently stored in the database.
   * @throws ResourceAccessAuthorizationException
   */
  @Deprecated
  @RequestMapping(value = "/reports/{schema}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  DownloadReports getReportDownloads(@PathVariable String schema,
                             @RequestParam(required=false) String channelName,
                             @RequestParam(required=false) String reportSpace,
                             @RequestParam(required = false) Integer maxRecords,
                             @RequestParam(required = false) String syncPoint) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.CONSUMER_API, "getReportDownloads()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }

    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();

    ReportInfoDownload reports = dataLogic.getReports(reportSpace, syncPoint, maxRecords, schema);
    if (reports == null){
      throw new ResourceNotFoundException("No reports available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }
    List<ReportDataWithDownloadType> reportList = new ArrayList<ReportDataWithDownloadType>();
    if (reports!=null) {
      for (ReportInfo entry : reports.getReportInfoList()) {
        reportList.add(entry.getReportDataWithDownloadType());
      }
    }

    return new DownloadReports(reports==null?syncPoint:reports.getUpToSyncpoint(), reportList);
  }


  /**
   * Bulk download for evaluation decisions stored in the system
   * Requires either a valid Channel Name or Report Space to determine which pool of data to reference (for which regulator etc).
   * @param channelName (Mutually-optional) used to determine the default schema for the reports as well as the Report Space from which to pull the data.  Takes precedence over a provided Report Space.
   * @param reportSpace (Mutually-optional) if the channel name is not provided, then this parameter is used to determine the report pool to access and the default schema in which to provide them.
   * @param maxRecords Max records returned is defaulted to 10 if not otherwise stipulated - recommended to keep this number below 100.
   * @param syncPoint The syncpoint is used to determine the last received record from the download queue, and if provided, only those items in the download queue with a syncpoint greater than that provided will be returned (to the maximum count stipulated/defaulted).  If not provided, the function will return all records from the beginning of the download queue to the count specified.
   * @return Map containing the relevant TrnReference and associated Evaluation Decision data
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/decisions", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  DownloadDecisions getDecisionDownloads(
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) String syncPoint) throws ResourceAccessAuthorizationException {
    SecurityHelper.checkUserAuthority(SecurityHelper.CONSUMER_API, "getDecisionDownloads()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();

    DecisionDownload decisions = dataLogic.getDecisions(reportSpace, syncPoint, maxRecords);
    if (decisions == null){
      throw new ResourceNotFoundException("No decisions available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }
    return new DownloadDecisions(decisions.getUpToSyncpoint(), decisions.getEvaluationDecisionList());
  }

  /**
   * This API is used to retrieve a list of uploaded account entries.
   * You need to provide either a valid channelName or reportSpace.
   *
   * @param channelName (optional / mutually-exclusive) If provided, the ReportSpace is not required as the channelName is directly linked to a report space
   * @param reportSpace (optional / mutually-exclusive)
   * @param maxRecords (optional) If this is not provided all records are returned
   * @param syncPoint (optional) If this is provided, only records after this syncpoint will be returned
   * @return a JSON object containing a syncpoint and a list of AccountEntries (a JSON object containing Trn Reference, Report Space, Version and file contents).
   */
  @RequestMapping(value = "/accountEntries", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  DownloadAccountEntryData getAccountEntryDownloads(
          @RequestParam(required = false) String channelName,
          @RequestParam(required = false) String reportSpace,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) String syncPoint) throws ResourceAccessAuthorizationException {
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();
    AccountEntryDownload accountEntryDownloads = dataLogic.getAccountEntries(reportSpace,syncPoint,maxRecords);
    if (accountEntryDownloads == null){
      throw new ResourceNotFoundException("No account entries available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }

    return new DownloadAccountEntryData(accountEntryDownloads.getUpToSyncpoint(), accountEntryDownloads.getAccountEntryList());
  }


  /**
   * Bulk Document download API
   * Requires either a valid Channel Name or Report Space to determine which pool of data to reference (for which regulator etc).
   * @param channelName (Mutually-optional) used to determine the default schema for the reports as well as the Report Space from which to pull the data.  Takes precedence over a provided Report Space.
   * @param reportSpace (Mutually-optional) if the channel name is not provided, then this parameter is used to determine the report pool to access and the default schema in which to provide them.
   * @param maxRecords Max records returned is defaulted to 10 if not otherwise stipulated - recommended to keep this number below 100.
   * @param syncPoint The syncpoint is used to determine the last received record from the download queue, and if provided, only those items in the download queue with a syncpoint greater than that provided will be returned (to the maximum count stipulated/defaulted).  If not provided, the function will return all records from the beginning of the download queue to the count specified.
   * @return a Map of TrnReferences with associated document data and details.
   * @throws ResourceAccessAuthorizationException
   */
  @RequestMapping(value = "/documents", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  DownloadDocumentInfo getDocumentDownloads(
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) String syncPoint) throws ResourceAccessAuthorizationException {
//    SecurityHelper.checkUserAuthority(SecurityHelper.CONSUMER_API, "getDocumentDownloads()");
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();

    DocumentDataDownload documents = dataLogic.getDocuments(reportSpace, syncPoint, maxRecords);

    if (documents == null){
      throw new ResourceNotFoundException("No documents available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }
    return new DownloadDocumentInfo(documents.getUpToSyncpoint(), DownloadDocumentInfo.wrapDocumentDataList(documents.getDocumentDataList(), true));
  }


  /**
   * Bulk Checksum download API
   * Requires either a valid Channel Name or Report Space to determine which pool of data to reference (for which regulator etc).
   * @param channelName (Mutually-optional) used to determine the default schema for the reports as well as the Report Space from which to pull the data.  Takes precedence over a provided Report Space.
   * @param reportSpace (Mutually-optional) if the channel name is not provided, then this parameter is used to determine the report pool to access and the default schema in which to provide them.
   * @param maxRecords Max records returned is defaulted to 10 if not otherwise stipulated - recommended to keep this number below 100.
   * @param syncPoint The syncpoint is used to determine the last received record from the download queue, and if provided, only those items in the download queue with a syncpoint greater than that provided will be returned (to the maximum count stipulated/defaulted).  If not provided, the function will return all records from the beginning of the download queue to the count specified.
   * @return the most recent version of each checksum file in the download queue.
   */
  @RequestMapping(value = "/checksums", method = RequestMethod.GET, produces = "application/json")
  public
  @ResponseBody
  DownloadChecksumInfo getChecksumDownloads(
          @RequestParam(required=false) String channelName,
          @RequestParam(required=false) String reportSpace,
          @RequestParam(required = false) Integer maxRecords,
          @RequestParam(required = false) String syncPoint) {
    Channel channel = null;
    if (channelName != null) {
      channel = systemInformation.getChannel(channelName);
    }
    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
    if (channelName != null && channel == null) {
      throw new PreconditionFailedException("A valid channelName must be specified");
    }
    if (!systemInformation.isValidReportSpace(reportSpace)) {
      throw new PreconditionFailedException("A valid channelName or reportSpace must be specified");
    }
    if (maxRecords == null)
      maxRecords = systemInformation.getDefaultMaxDownloadRecords();

    ChecksumDataDownload checksums = dataLogic.getChecksums(reportSpace, syncPoint, maxRecords);

    if (checksums == null){
      throw new ResourceNotFoundException("No checksums available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
    }
    return new DownloadChecksumInfo(checksums.getUpToSyncpoint(), DownloadChecksumInfo.wrapChecksumDataList(checksums.getChecksumDataList()));
  }

  //DEPRECATED
//  @RequestMapping(value = "/checksumFile", method = RequestMethod.GET)
//  public void getChecksumFile(
//          @RequestParam(required=false) String channelName,
//          @RequestParam(required=false) String reportSpace,
//          @RequestParam(required = false) Integer maxRecords,
//          @RequestParam(required = false) String syncPoint,
//          HttpServletRequest request,
//          HttpServletResponse response
//  ) throws Exception {
//    Channel channel = null;
//    if (channelName != null) {
//      channel = systemInformation.getChannel(channelName);
//    }
//    reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
//    if (channelName != null && channel == null) {
//      throw new PreconditionFailedException("A valid channelName must be specified");
//    }
//    if (!systemInformation.isValidReportSpace(reportSpace)) {
//      throw new PreconditionFailedException("A valid reportSpace must be specified");
//    }
//    if (maxRecords == null)
//      maxRecords = 1;//systemInformation.getDefaultMaxDownloadRecords();
//
//    ChecksumDataDownload checksums = dataLogic.getChecksums(reportSpace, syncPoint, 1);
//
//    if (checksums == null){
//      throw new ResourceNotFoundException("No checksums available for download - provided syncPoint: "+(syncPoint==null?"null":syncPoint));
//    }
//    if (checksums.getChecksumDataList().size() >= 1) {
//      ChecksumData checksumData = checksums.getChecksumDataList().get(0);
//      //return new DownloadChecksumInfo(checksums.getUpToSyncpoint(), DownloadChecksumInfo.wrapChecksumDataList(checksums.getChecksumDataList()));
//      response.setHeader("Content-Disposition", "attachment; filename=" + checksumData.getFileName());
//      response.setHeader("Content-Type", checksumData.getFileType());
//      response.setHeader("ValueDate", checksumData.getValueDate());
//      try {
//        PrintWriter writer = response.getWriter();
//        writer.print(new String(checksumData.getContent()));
//        writer.flush();
//      } catch (Exception error){
//        //throw some error
//        throw new Exception("Unable to obtain PrintWriter for checksum file HTTP Servlet Response");
//      }
//    } else {
//      throw new ResourceNotFoundException("No available checksum files to download beyond the provided sync point ("+syncPoint+").");
//    }
//  }
}
