package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.*;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import java.util.Map;

/**
 * Created by jake on 5/25/17.
 */

@JsonPropertyOrder({
        "ChannelName"
        , "TrnReference"
        , "CustomerOnUs"
        , "CustomerPayment"
        , "CustomerReceipt"
        , "BankPayment"
        , "BankReceipt"
        , "CorrespondentBank"
        , "Card"
})
@MappedSuperclass
public class EvaluationRequest {


  public static AccountHolderStatus getAccountHolderStatus(String accountHolderStatus){
    if (accountHolderStatus != null) {
      for (AccountHolderStatus stat : AccountHolderStatus.values()) {
        if (stat.name().equalsIgnoreCase(accountHolderStatus)) {
          return stat;
        }
      }
    }
    return AccountHolderStatus.Unknown;
  }



  @JsonPropertyOrder({
      "OrderingBIC"
      , "OrderingInstituition"
      , "OrderingCurrency"
      , "OrderingResidenceStatus"
      , "OrderingAccountType"
      , "Field72"
      , "OrderingOptionalParams"
      , "BeneficiaryBIC"
      , "BeneficiaryInstituition"
      , "BeneficiaryCurrency"
      , "BeneficiaryResidenceStatus"
      , "BeneficiaryAccountType"
      , "BeneficiaryOptionalParams"
      , "Additional"
  })
  public static class ConsolidatedEvaluation {
    private String orderingBIC;
    private String orderingCurrency;
    private ResidenceStatus orderingResStatus;
    private BankAccountType orderingAccType;
    private String field72;
    private Map<String, Object> orderingOptionalParams;

    private String beneficiaryBIC;
    private String beneficiaryCurrency;
    private ResidenceStatus beneficiaryResStatus;
    private BankAccountType beneficiaryAccType;
    private Map<String, Object> beneficiaryOptionalParams;

    private Map<String, String> additionalParams;

    public ConsolidatedEvaluation() {
    }

    public ConsolidatedEvaluation(String orderingBIC, String orderingCurrency, ResidenceStatus orderingResStatus, BankAccountType orderingAccType, String field72, Map<String, Object> orderingOptionalParams, String beneficiaryBIC, String beneficiaryCurrency, ResidenceStatus beneficiaryResStatus, BankAccountType beneficiaryAccType, Map<String, Object> beneficiaryOptionalParams, Map<String, String> additionalParams) {
      this.orderingBIC = orderingBIC;
      this.orderingCurrency = orderingCurrency;
      this.orderingResStatus = orderingResStatus;
      this.orderingAccType = orderingAccType;
      this.field72 = field72;
      this.orderingOptionalParams = orderingOptionalParams;
      this.beneficiaryBIC = beneficiaryBIC;
      this.beneficiaryCurrency = beneficiaryCurrency;
      this.beneficiaryResStatus = beneficiaryResStatus;
      this.beneficiaryAccType = beneficiaryAccType;
      this.beneficiaryOptionalParams = beneficiaryOptionalParams;
      this.additionalParams = additionalParams;
    }

    @JsonProperty("OrderingBIC")
    public String getOrderingBIC() {
      return orderingBIC;
    }

    public void setOrderingBIC(String orderingBIC) {
      this.orderingBIC = orderingBIC;
    }

    @JsonProperty("OrderingCurrency")
    public String getOrderingCurrency() {
      return orderingCurrency;
    }

    public void setOrderingCurrency(String orderingCurrency) {
      this.orderingCurrency = orderingCurrency;
    }

    @JsonProperty("OrderingResidenceStatus")
    public ResidenceStatus getOrderingResStatus() {
      return orderingResStatus;
    }

    public void setOrderingResStatus(ResidenceStatus orderingResStatus) {
      this.orderingResStatus = orderingResStatus;
    }

    @JsonProperty("OrderingAccountType")
    public BankAccountType getOrderingAccType() {
      return orderingAccType;
    }

    public void setOrderingAccType(BankAccountType orderingAccType) {
      this.orderingAccType = orderingAccType;
    }

    @JsonProperty("Field72")
    public String getField72() {
      return field72;
    }

    public void setField72(String field72) {
      this.field72 = field72;
    }

    @JsonProperty("OrderingOptionalParameters")
    public Map<String, Object> getOrderingOptionalParams() {
      return orderingOptionalParams;
    }

    public void setOrderingOptionalParams(Map<String, Object> orderingOptionalParams) {
      this.orderingOptionalParams = orderingOptionalParams;
    }

    @JsonProperty("BeneficiaryBIC")
    public String getBeneficiaryBIC() {
      return beneficiaryBIC;
    }

    public void setBeneficiaryBIC(String beneficiaryBIC) {
      this.beneficiaryBIC = beneficiaryBIC;
    }

    @JsonProperty("BeneficiaryCurrency")
    public String getBeneficiaryCurrency() {
      return beneficiaryCurrency;
    }

    public void setBeneficiaryCurrency(String beneficiaryCurrency) {
      this.beneficiaryCurrency = beneficiaryCurrency;
    }

    @JsonProperty("BeneficiaryResidenceStatus")
    public ResidenceStatus getBeneficiaryResStatus() {
      return beneficiaryResStatus;
    }

    public void setBeneficiaryResStatus(ResidenceStatus beneficiaryResStatus) {
      this.beneficiaryResStatus = beneficiaryResStatus;
    }

    @JsonProperty("BeneficiaryAccountType")
    public BankAccountType getBeneficiaryAccType() {
      return beneficiaryAccType;
    }

    public void setBeneficiaryAccType(BankAccountType beneficiaryAccType) {
      this.beneficiaryAccType = beneficiaryAccType;
    }

    @JsonProperty("BeneficiaryOptionalParameters")
    public Map<String, Object> getBeneficiaryOptionalParams() {
      return beneficiaryOptionalParams;
    }

    public void setBeneficiaryOptionalParams(Map<String, Object> beneficiaryOptionalParams) {
      this.beneficiaryOptionalParams = beneficiaryOptionalParams;
    }


    @JsonProperty("Additional")
    public Map<String, String> getAdditionalParams() {
      return additionalParams;
    }

    public void setAdditionalParams(Map<String, String> additionalParams) {
      this.additionalParams = additionalParams;
    }
  }



  @JsonPropertyOrder({
          "DrResStatus"
          , "DrAccType"
          , "CrResStatus"
          , "CrAccType"
          , "Additional"
  })
  public static class EvaluationCustomerOnUs {
    private ResidenceStatus drResStatus;
    private BankAccountType drAccType;
    private ResidenceStatus crResStatus;
    private BankAccountType crAccType;
    @ManyToMany(fetch= FetchType.EAGER)
    private Map<String, String> additional;


    public EvaluationCustomerOnUs() {
    }

    public EvaluationCustomerOnUs(ResidenceStatus drResStatus, BankAccountType drAccType,
                                  ResidenceStatus crResStatus, BankAccountType crAccType) {
      this(drResStatus, drAccType, crResStatus, crAccType, null);
    }

    public EvaluationCustomerOnUs(ResidenceStatus drResStatus, BankAccountType drAccType,
                                  ResidenceStatus crResStatus, BankAccountType crAccType, Map<String, String> additional) {
      this.drResStatus = drResStatus;
      this.drAccType = drAccType;
      this.crResStatus = crResStatus;
      this.crAccType = crAccType;
      this.additional = additional;
    }

    public void setDrResStatus(ResidenceStatus drResStatus) {
      this.drResStatus = drResStatus;
    }

    public void setDrAccType(BankAccountType drAccType) {
      this.drAccType = drAccType;
    }

    public void setCrResStatus(ResidenceStatus crResStatus) {
      this.crResStatus = crResStatus;
    }

    public void setCrAccType(BankAccountType crAccType) {
      this.crAccType = crAccType;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("DrResStatus")
    public ResidenceStatus getDrResStatus() {
      return drResStatus;
    }

    @JsonProperty("DrAccType")
    public BankAccountType getDrAccType() {
      return drAccType;
    }

    @JsonProperty("CrResStatus")
    public ResidenceStatus getCrResStatus() {
      return crResStatus;
    }

    @JsonProperty("CrAccType")
    public BankAccountType getCrAccType() {
      return crAccType;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }

    @JsonIgnore
    public AccountHolderStatus getDrAccountHolderStatus() {
      return getAccountHolderStatus(additional.get("DrAccountHolderStatus"));
    }

    public void setDrAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
      additional.put("DrAccountHolderStatus", accountHolderStatus.name());
    }

    @JsonIgnore
    public AccountHolderStatus getCrAccountHolderStatus() {
      return getAccountHolderStatus(additional.get("CrAccountHolderStatus"));
    }

    public void setCrAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
      additional.put("CrAccountHolderStatus", accountHolderStatus.name());
    }

  }

  @JsonPropertyOrder({
          "DrResStatus"
          , "DrAccType"
          , "BeneficiaryBIC"
          , "BeneficiaryInstituition"
          , "BeneficiaryCurrency"
          , "Additional"
  })
  public static class EvaluationCustomerPayment {
    private ResidenceStatus drResStatus;
    private BankAccountType drAccType;
    private String beneficiaryBIC;
    private CounterpartyInstituition beneficiaryInstituition;
    private String beneficiaryCurrency;
    private Map<String, String> additional;

    public EvaluationCustomerPayment() {}

    public EvaluationCustomerPayment(ResidenceStatus drResStatus, BankAccountType drAccType,
                                     String beneficiaryBIC, CounterpartyInstituition beneficiaryInstituition, String beneficiaryCurrency) {
      this(drResStatus, drAccType, beneficiaryBIC, beneficiaryInstituition, beneficiaryCurrency, null);
    }

    public EvaluationCustomerPayment(ResidenceStatus drResStatus, BankAccountType drAccType,
                                     String beneficiaryBIC, CounterpartyInstituition beneficiaryInstituition, String beneficiaryCurrency,
                                     Map<String, String> additional) {
      this.drResStatus = drResStatus;
      this.drAccType = drAccType;
      this.beneficiaryBIC = beneficiaryBIC;
      this.beneficiaryInstituition = beneficiaryInstituition;
      this.beneficiaryCurrency = beneficiaryCurrency;
      this.additional = additional;
    }

    public void setDrResStatus(ResidenceStatus drResStatus) {
      this.drResStatus = drResStatus;
    }

    public void setDrAccType(BankAccountType drAccType) {
      this.drAccType = drAccType;
    }

    public void setBeneficiaryBIC(String beneficiaryBIC) {
      this.beneficiaryBIC = beneficiaryBIC;
    }

    public void setBeneficiaryInstituition(CounterpartyInstituition beneficiaryInstituition) {
      this.beneficiaryInstituition = beneficiaryInstituition;
    }

    public void setBeneficiaryCurrency(String beneficiaryCurrency) {
      this.beneficiaryCurrency = beneficiaryCurrency;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("DrResStatus")
    public ResidenceStatus getDrResStatus() {
      return drResStatus;
    }

    @JsonProperty("DrAccType")
    public BankAccountType getDrAccType() {
      return drAccType;
    }

    @JsonProperty("BeneficiaryBIC")
    public String getBeneficiaryBIC() {
      return beneficiaryBIC;
    }

    @JsonProperty("BeneficiaryInstituition")
    public CounterpartyInstituition getBeneficiaryInstituition() {
      return beneficiaryInstituition;
    }

    @JsonProperty("BeneficiaryCurrency")
    public String getBeneficiaryCurrency() {
      return beneficiaryCurrency;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }

    @JsonIgnore
    public AccountHolderStatus getDrAccountHolderStatus() {
      return getAccountHolderStatus(additional.get("DrAccountHolderStatus"));
    }

    public void setDrAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
      additional.put("DrAccountHolderStatus", accountHolderStatus.name());
    }

  }

  @JsonPropertyOrder({
          "CrResStatus"
          , "CrAccType"
          , "OrderingBIC"
          , "OrderingInstituition"
          , "OrderingCurrency"
          , "Field72"
          , "Additional"
  })
  public static class EvaluationCustomerReceipt {
    private ResidenceStatus crResStatus;
    private BankAccountType crAccType;
    private String orderingBIC;
    private CounterpartyInstituition orderingInstituition;
    private String orderingCurrency;
    private String field72;
    private Map<String, String> additional;

    public EvaluationCustomerReceipt() {}

    public EvaluationCustomerReceipt(ResidenceStatus crResStatus, BankAccountType crAccType,
                                     String orderingBIC, CounterpartyInstituition orderingInstituition, String orderingCurrency, String field72) {
      this(crResStatus, crAccType, orderingBIC, orderingInstituition, orderingCurrency, field72, null);
    }

    public EvaluationCustomerReceipt(ResidenceStatus crResStatus, BankAccountType crAccType,
                                     String orderingBIC, CounterpartyInstituition orderingInstituition, String orderingCurrency, String field72,
                                     Map<String, String> additional) {
      this.crResStatus = crResStatus;
      this.crAccType = crAccType;
      this.orderingBIC = orderingBIC;
      this.orderingInstituition = orderingInstituition;
      this.orderingCurrency = orderingCurrency;
      this.field72 = field72;
      this.additional = additional;
    }

    public void setCrResStatus(ResidenceStatus crResStatus) {
      this.crResStatus = crResStatus;
    }

    public void setCrAccType(BankAccountType crAccType) {
      this.crAccType = crAccType;
    }

    public void setOrderingBIC(String orderingBIC) {
      this.orderingBIC = orderingBIC;
    }

    public void setOrderingInstituition(CounterpartyInstituition orderingInstituition) {
      this.orderingInstituition = orderingInstituition;
    }

    public void setOrderingCurrency(String orderingCurrency) {
      this.orderingCurrency = orderingCurrency;
    }

    public void setField72(String field72) {
      this.field72 = field72;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("CrResStatus")
    public ResidenceStatus getCrResStatus() {
      return crResStatus;
    }

    @JsonProperty("CrAccType")
    public BankAccountType getCrAccType() {
      return crAccType;
    }

    @JsonProperty("OrderingBIC")
    public String getOrderingBIC() {
      return orderingBIC;
    }

    @JsonProperty("OrderingInstituition")
    public CounterpartyInstituition getOrderingInstituition() {
      return orderingInstituition;
    }

    @JsonProperty("OrderingCurrency")
    public String getOrderingCurrency() {
      return orderingCurrency;
    }

    @JsonProperty("Field72")
    public String getField72() {
      return field72;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }

    @JsonIgnore
    public AccountHolderStatus getCrAccountHolderStatus() {
      return getAccountHolderStatus(additional.get("CrAccountHolderStatus"));
    }

    public void setCrAccountHolderStatus(AccountHolderStatus accountHolderStatus) {
      additional.put("CrAccountHolderStatus", accountHolderStatus.name());
    }

  }

  @JsonPropertyOrder({
          "OrderingBIC"
          , "BeneficiaryBIC"
          , "BeneficiaryCurrency"
          , "Additional"
  })
  public static class EvaluationBankPayment {
    private String orderingBIC;
    private String beneficiaryBIC;
    private String beneficiaryCurrency;
    private Map<String, String> additional;


    public EvaluationBankPayment() {}

    public EvaluationBankPayment(String orderingBIC, String beneficiaryBIC, String beneficiaryCurrency) {
      this(orderingBIC, beneficiaryBIC, beneficiaryCurrency, null);
    }

    public EvaluationBankPayment(String orderingBIC, String beneficiaryBIC, String beneficiaryCurrency, Map<String, String> additional) {
      this.orderingBIC = orderingBIC;
      this.beneficiaryBIC = beneficiaryBIC;
      this.beneficiaryCurrency = beneficiaryCurrency;
      this.additional = additional;
    }

    public void setOrderingBIC(String orderingBIC) {
      this.orderingBIC = orderingBIC;
    }

    public void setBeneficiaryBIC(String beneficiaryBIC) {
      this.beneficiaryBIC = beneficiaryBIC;
    }

    public void setBeneficiaryCurrency(String beneficiaryCurrency) {
      this.beneficiaryCurrency = beneficiaryCurrency;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("OrderingBIC")
    public String getOrderingBIC() {
      return orderingBIC;
    }

    @JsonProperty("BeneficiaryBIC")
    public String getBeneficiaryBIC() {
      return beneficiaryBIC;
    }

    @JsonProperty("BeneficiaryCurrency")
    public String getBeneficiaryCurrency() {
      return beneficiaryCurrency;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }

  }

  @JsonPropertyOrder({
          "OrderingBIC"
          , "BeneficiaryBIC"
          , "BeneficiaryCurrency"
          , "Additional"
  })
  public static class EvaluationBankReceipt {
    private String beneficiaryBIC;
    private String orderingBIC;
    private String orderingCurrency;
    private Map<String, String> additional;


    public EvaluationBankReceipt() {}

    public EvaluationBankReceipt(String beneficiaryBIC, String orderingBIC, String orderingCurrency) {
      this(beneficiaryBIC, orderingBIC, orderingCurrency, null);
    }

    public EvaluationBankReceipt(String beneficiaryBIC, String orderingBIC, String orderingCurrency, Map<String, String> additional) {
      this.beneficiaryBIC = beneficiaryBIC;
      this.orderingBIC = orderingBIC;
      this.orderingCurrency = orderingCurrency;
      this.additional = additional;
    }

    public void setBeneficiaryBIC(String beneficiaryBIC) {
      this.beneficiaryBIC = beneficiaryBIC;
    }

    public void setOrderingBIC(String orderingBIC) {
      this.orderingBIC = orderingBIC;
    }

    public void setOrderingCurrency(String orderingCurrency) {
      this.orderingCurrency = orderingCurrency;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("BeneficiaryBIC")
    public String getBeneficiaryBIC() {
      return beneficiaryBIC;
    }

    @JsonProperty("OrderingBIC")
    public String getOrderingBIC() {
      return orderingBIC;
    }

    @JsonProperty("OrderingCurrency")
    public String getOrderingCurrency() {
      return orderingCurrency;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }
  }

  @JsonPropertyOrder({
          "OrderingInstitutionBIC"
          , "BeneficiaryInstitutionBIC"
          , "OrderingCustomer"
          , "Additional"
  })
  public static class EvaluationCorrespondentBank {
    private String beneficiaryInstitutionBIC;
    private String orderingInstitutionBIC;
    private OrderingCustomerType orderingCustomer;
    private Map<String, String> additional;


    public EvaluationCorrespondentBank() {}

    public EvaluationCorrespondentBank(String beneficiaryInstitutionBIC, String orderingInstitutionBIC, OrderingCustomerType orderingCustomer) {
      this(beneficiaryInstitutionBIC, orderingInstitutionBIC, orderingCustomer, null);
    }

    public EvaluationCorrespondentBank(String beneficiaryInstitutionBIC, String orderingInstitutionBIC, OrderingCustomerType orderingCustomer, Map<String, String> additional) {
      this.beneficiaryInstitutionBIC = beneficiaryInstitutionBIC;
      this.orderingInstitutionBIC = orderingInstitutionBIC;
      this.orderingCustomer = orderingCustomer;
      this.additional = additional;
    }

    public void setBeneficiaryBIC(String beneficiaryInstitutionBIC) {
      this.beneficiaryInstitutionBIC = beneficiaryInstitutionBIC;
    }

    public void setOrderingBIC(String orderingInstitutionBIC) {
      this.orderingInstitutionBIC = orderingInstitutionBIC;
    }

    public void setOrderingCurrency(OrderingCustomerType orderingCustomer) {
      this.orderingCustomer = orderingCustomer;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }

    @JsonProperty("BeneficiaryInstitutionBIC")
    public String getBeneficiaryInstitutionBIC() {
      return beneficiaryInstitutionBIC;
    }

    @JsonProperty("OrderingInstitutionBIC")
    public String getOrderingInstitutionBIC() {
      return orderingInstitutionBIC;
    }

    @JsonProperty("OrderingCustomer")
    public OrderingCustomerType getOrderingCustomer() {
      return orderingCustomer;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }
  }

  @JsonPropertyOrder({
          "IssuingCountry"
          , "UseCountry"
          , "Additional"
  })
  public static class EvaluationCard {
    private String IssuingCountry;
    private String useCountry;
    private Map<String, String> additional;


    public EvaluationCard() {}

    public EvaluationCard(String IssuingCountry, String useCountry) {
      this(IssuingCountry, useCountry, null);
    }

    public EvaluationCard(String IssuingCountry, String useCountry, Map<String, String> additional) {
      this.IssuingCountry = IssuingCountry;
      this.useCountry = useCountry;
    }

    @JsonProperty("IssuingCountry")
    public String getIssuingCountry() {
      return IssuingCountry;
    }

    public void setIssuingCountry(String IssuingCountry) {
      this.IssuingCountry = IssuingCountry;
    }

    @JsonProperty("UseCountry")
    public String getUseCountry() {
      return useCountry;
    }

    public void setUseCountry(String useCountry) {
      this.useCountry = useCountry;
    }

    @JsonProperty("Additional")
    public Map<String, String> getAdditional() {
      return additional;
    }

    public void setAdditional(Map<String, String> additional) {
      this.additional = additional;
    }
  }

  private String channelName;
  private String trnReference;
  private ConsolidatedEvaluation consolidatedEvaluation;
  private EvaluationCustomerOnUs evaluationCustomerOnUs;
  private EvaluationCustomerPayment evaluationCustomerPayment;
  private EvaluationCustomerReceipt evaluationCustomerReceipt;
  private EvaluationBankPayment evaluationBankPayment;
  private EvaluationBankReceipt evaluationBankReceipt;
  private EvaluationCorrespondentBank evaluationCorrespondentBank;
  private EvaluationCard evaluationCard;

  @JsonProperty("ChannelName")
  public String getChannelName() {
    return channelName;
  }

  public void setChannelName(String channelName) {
    this.channelName = channelName;
  }

  @JsonProperty(value = "TrnReference")
  public String getTrnReference() {
    return trnReference;
  }

  public void setTrnReference(String trnReference) {
    this.trnReference = trnReference;
  }


  @JsonProperty("ConsolidatedEvaluation")
  public ConsolidatedEvaluation getConsolidatedEvaluation() {
    return consolidatedEvaluation;
  }

  public void setConsolidatedEvaluation(ConsolidatedEvaluation consolidatedEvaluation) {
    this.consolidatedEvaluation = consolidatedEvaluation;
  }

  @JsonProperty("CustomerOnUs")
  public EvaluationCustomerOnUs getEvaluationCustomerOnUs() {
    return evaluationCustomerOnUs;
  }

  public void setEvaluationCustomerOnUs(EvaluationCustomerOnUs evaluationCustomerOnUs) {
    this.evaluationCustomerOnUs = evaluationCustomerOnUs;
  }

  @JsonProperty("CustomerPayment")
  public EvaluationCustomerPayment getEvaluationCustomerPayment() {
    return evaluationCustomerPayment;
  }

  public void setEvaluationCustomerPayment(EvaluationCustomerPayment evaluationCustomerPayment) {
    this.evaluationCustomerPayment = evaluationCustomerPayment;
  }

  @JsonProperty("CustomerReceipt")
  public EvaluationCustomerReceipt getEvaluationCustomerReceipt() {
    return evaluationCustomerReceipt;
  }

  public void setEvaluationCustomerReceipt(EvaluationCustomerReceipt evaluationCustomerReceipt) {
    this.evaluationCustomerReceipt = evaluationCustomerReceipt;
  }

  @JsonProperty("BankPayment")
  public EvaluationBankPayment getEvaluationBankPayment() {
    return evaluationBankPayment;
  }

  public void setEvaluationBankPayment(EvaluationBankPayment evaluationBankPayment) {
    this.evaluationBankPayment = evaluationBankPayment;
  }

  @JsonProperty("BankReceipt")
  public EvaluationBankReceipt getEvaluationBankReceipt() {
    return evaluationBankReceipt;
  }

  public void setEvaluationBankReceipt(EvaluationBankReceipt evaluationBankReceipt) {
    this.evaluationBankReceipt = evaluationBankReceipt;
  }
  
  @JsonProperty("CorrespondentBank")
  public EvaluationCorrespondentBank getEvaluationCorrespondentBank() {
    return evaluationCorrespondentBank;
  }
  
  public void setEvaluationCorrespondentBank(EvaluationCorrespondentBank evaluationCorrespondentBank) {
    this.evaluationCorrespondentBank = evaluationCorrespondentBank;
  }
  
  @JsonProperty("Card")
  public EvaluationCard getEvaluationCard() {
    return evaluationCard;
  }

  public void setEvaluationCard(EvaluationCard evaluationCard) {
    this.evaluationCard = evaluationCard;
  }
}
