package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.Scope;


@JsonPropertyOrder({
        "Type"
        , "Scope"
        , "Field"
        , "MoneyIndex"
        , "ImportExportIndex"
        , "From"
        , "To"
})
public class ChangeItem {
  private final ChangeType type;
  private final Scope scope;
  private final String field;
  private final Integer moneyInd;
  private final Integer importexportInd;
  private final String from;
  private final String to;

  public ChangeItem(ChangeType type, Scope scope, String field, Integer moneyInd, Integer importexportInd, String from, String to) {
    this.type = type;
    this.scope = scope;
    this.field = field;
    this.moneyInd = moneyInd;
    this.importexportInd = importexportInd;
    this.from = from;
    this.to = to;
  }

  @JsonProperty("Type")
  public ChangeType getType() {
    return type;
  }

  @JsonProperty("Scope")
  public Scope getScope() {
    return scope;
  }

  @JsonProperty("Field")
  public String getField() {
    return field;
  }

  @JsonProperty("MoneyIndex")
  public Integer getMoneyInd() {
    return moneyInd;
  }

  @JsonProperty("ImportExportIndex")
  public Integer getImportexportInd() {
    return importexportInd;
  }

  @JsonProperty("From")
  public String getFrom() {
    return from;
  }

  @JsonProperty("To")
  public String getTo() {
    return to;
  }
}
