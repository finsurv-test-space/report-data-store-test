package za.co.synthesis.regulatory.report.security.jwt;

import com.unboundid.ldap.sdk.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import za.co.synthesis.regulatory.report.businesslogic.UserInformation;
import za.co.synthesis.regulatory.report.persist.UserData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LDAPAuthModule implements IJWTAuthModule {
  public static class LDAPAuthException extends AuthenticationException {
    public LDAPAuthException(String msg) {
      super(msg);
    }

    public LDAPAuthException(String msg, Throwable t) {
      super(msg, t);
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(LDAPAuthModule.class);

  private UserInformation userInformation;
  private LDAPConnectionPool ldapSearchPool;
  private final AuthenticationCache authenticationCache = new AuthenticationCache();
  private Map<String, String> groupUserMap = null;
  private Map<String, String> groupRightMap = null;
  private Map<String, String> groupAccessMap = null;
  private String ldapHost = "ldap.jumpcloud.com";
  private int ldapPort = 389;
  private String ldapDSN = "ou=Users,o=5a1594e52a1ccf880dc36555,dc=jumpcloud,dc=com";
  private String ldapSearchUser = "jamese";
  private String ldapSearchPassword = "tes1@weg";

  @Override
  public boolean isAvailable() {
    return (ldapHost != null && ldapDSN != null);
  }

  @Required
  public void setUserInformation(UserInformation userInformation) {
    this.userInformation = userInformation;
  }

  public void setLdapHost(String ldapHost) {
    this.ldapHost = ldapHost;
  }

  public void setLdapPort(int ldapPort) {
    this.ldapPort = ldapPort;
  }

  public void setLdapDSN(String ldapDSN) {
    this.ldapDSN = ldapDSN;
  }

  public void setLdapSearchUser(String ldapSearchUser) {
    this.ldapSearchUser = ldapSearchUser;
  }

  public void setLdapSearchPassword(String ldapSearchPassword) {
    this.ldapSearchPassword = ldapSearchPassword;
  }

  public void setGroupUserMap(Map<String, String> groupUserMap) {
    this.groupUserMap = groupUserMap;
  }

  public void setGroupRightMap(Map<String, String> groupRightMap) {
    this.groupRightMap = groupRightMap;
  }

  public void setGroupAccessMap(Map<String, String> groupAccessMap) {
    this.groupAccessMap = groupAccessMap;
  }

  public void setMaxCacheSize(int maxCacheSize) {
    authenticationCache.setMaxCacheSize(maxCacheSize);
  }

  public void setCacheNocheckMillis(long nocheckMillis) {
    authenticationCache.setNocheckMillis(nocheckMillis);
  }

  public void setCacheExpiryMillis(long expiryMillis) {
    authenticationCache.setExpiryMillis(expiryMillis);
  }
  public void setCacheExpiryHours(int hours) {
    authenticationCache.setExpiryMillis(3600000 * (long)hours);
  }

  @Override
  public String getName() {
    return "ldap";
  }

  // Note: LDAPConnectionPool seems very slow
  private synchronized LDAPConnectionPool getConnectionPool() throws LDAPException {
    if (ldapSearchPool == null) {
      // Create the connection options
      LDAPConnectionOptions ldapOptions = new LDAPConnectionOptions();
      ldapOptions.setAbandonOnTimeout(true);
      ldapOptions.setBindWithDNRequiresPassword(true);
      ldapOptions.setResponseTimeoutMillis(60000);
      ldapOptions.setUsePooledSchema(true);
      ldapOptions.setPooledSchemaTimeoutMillis(60000);
      ldapOptions.setUseSynchronousMode(true);

      String[] addresses = new String[1];
      addresses[0] = ldapHost;
      int[] ports = new int[1];
      ports[0] = ldapPort;

      // Create the failover server set with the desired addresses and
      // ports.
      FailoverServerSet failoverSet = new FailoverServerSet(addresses,
              ports, ldapOptions);

      // Create a bind request that will be used to authenticate
      // newly-established connections.
      String userDN = "uid=" + ldapSearchUser + "," + ldapDSN;
      SimpleBindRequest bindRequest =
              new SimpleBindRequest(userDN, this.ldapSearchPassword);

      // Create and configure the connection pool.
      ldapSearchPool = new LDAPConnectionPool(failoverSet,
              bindRequest, 1, 10);
      ldapSearchPool.setMaxConnectionAgeMillis(1800);
      ldapSearchPool.setMaxDefunctReplacementConnectionAgeMillis(1800L);
      ldapSearchPool.setMaxWaitTimeMillis(3000L);
      ldapSearchPool.setRetryFailedOperationsDueToInvalidConnections(true);
      ldapSearchPool.setHealthCheckIntervalMillis(1900L);
    }
    return ldapSearchPool;
  }

  private void getGroupsRecursively(final LDAPConnection connection, final String distinguishedName,
                                    final Map<String, List<String>> groupsByDN) throws LDAPSearchException {
    if (!groupsByDN.containsKey(distinguishedName)) {
      Filter groupFilter = Filter.createANDFilter(Filter.createEqualityFilter("objectClass", "groupOfNames"),
              Filter.createEqualityFilter("member", distinguishedName));
      SearchResult result = connection.search(ldapDSN, SearchScope.SUB, groupFilter);
      List<SearchResultEntry> searchResults = result.getSearchEntries();
      for (SearchResultEntry searchResult : searchResults) {
        String groupName = searchResult.getAttributeValue("cn");
        if (groupsByDN.containsKey(distinguishedName)) {
          groupsByDN.get(distinguishedName).add(groupName);
        }
        else {
          List<String> newList = new ArrayList<String>();
          newList.add(groupName);
          groupsByDN.put(distinguishedName, newList);
        }
        getGroupsRecursively(connection, searchResult.getDN(), groupsByDN);
      }
    }
  }

  private boolean authForLDAPUser(String username, String password) {
    long millis = System.currentTimeMillis();

    AuthenticationCache.CachedLogin cachedLogin = authenticationCache.checkCacheWithPassword(millis, username, password);
    if (cachedLogin == AuthenticationCache.CachedLogin.CacheCanBeUsed) {
      return true;
    }
    try {
      LDAPConnection connection = new LDAPConnection();
      connection.connect(ldapHost, ldapPort);

      String userDN = "uid=" + username + "," + ldapDSN;
      BindResult res = connection.bind(userDN, password);
      if (res.getResultCode() != ResultCode.SUCCESS)
        throw new LDAPAuthException("Cannot authenticate '" + username + "' against LDAP");

      connection.close();

      return true;
    } catch (LDAPException e) {
      throw new LDAPAuthException("Cannot authenticate '" + username + "' against LDAP", e);
    }
  }

  private List<String> authAndGetGroupsForLDAPUser(String username, String password) {

    //TODO: Create alternate LDAP auth which ONLY authenticates IF a template user of the same username exists within the system and will inherit the template user access etc/
    //TODO: for this alternate LDAP auth, if the active status is false, then the authentication should fail.

    long millis = System.currentTimeMillis();

    AuthenticationCache.CachedLogin cachedLogin = authenticationCache.checkCacheWithPassword(millis, username, password);
    if (cachedLogin == AuthenticationCache.CachedLogin.CacheCanBeUsed) {
      return authenticationCache.getUserGroups(username);
    }
    try {
      LDAPConnection connection = new LDAPConnection();
      connection.connect(ldapHost, ldapPort);

      String userDN = "uid=" + username + "," + ldapDSN;
      BindResult res = connection.bind(userDN, password);
      if (res.getResultCode() != ResultCode.SUCCESS)
        throw new LDAPAuthException("Cannot authenticate '" + username + "' against LDAP");

      //LDAPConnectionPool pool = getConnectionPool();
      //LDAPConnection searchConnection = pool.getConnection();
      Map<String, List<String>> groupsByDN = new HashMap<String, List<String>>();
      getGroupsRecursively(connection, userDN, groupsByDN);

      List<String> result = new ArrayList<String>();
      for (List<String> groupNames : groupsByDN.values()) {
        for (String groupName : groupNames) {
          if (!result.contains(groupName))
            result.add(groupName);
        }
      }
      //pool.releaseConnection(searchConnection);
      connection.close();

      authenticationCache.onAuthenticate(millis, username, password, result);

      return result;
    } catch (LDAPException e) {
      if (cachedLogin == AuthenticationCache.CachedLogin.DoLogin) {
        logger.error("Cannot authenticate '" + username + "' against LDAP so will use cached credentials: " + e.getDiagnosticMessage());
        return authenticationCache.getUserGroupsAndResetCheckTime(millis, username);
      } else {
        throw new LDAPAuthException("Cannot authenticate '" + username + "' against LDAP", e);
      }
    }
  }

  private List<String> getGroupsForLDAPUser(String username) {
    long millis = System.currentTimeMillis();

    AuthenticationCache.CachedLogin cachedLogin = authenticationCache.checkCacheNoPassword(millis, username);
    if (cachedLogin == AuthenticationCache.CachedLogin.CacheCanBeUsed) {
      return authenticationCache.getUserGroups(username);
    }
    try {
      LDAPConnectionPool pool = getConnectionPool();
      LDAPConnection searchConnection = pool.getConnection();
      Map<String, List<String>> groupsByDN = new HashMap<String, List<String>>();
      String userDN = "uid=" + username + "," + ldapDSN;
      getGroupsRecursively(searchConnection, userDN, groupsByDN);

      List<String> result = new ArrayList<String>();
      for (List<String> groupNames : groupsByDN.values()) {
        for (String groupName : groupNames) {
          if (!result.contains(groupName))
            result.add(groupName);
        }
      }
      pool.releaseConnection(searchConnection);

      authenticationCache.onRefreshGroups(millis, username, result);

      return result;
    } catch (LDAPException e) {
      if (cachedLogin == AuthenticationCache.CachedLogin.DoLogin) {
        logger.error("Search for '" + username + "' failed against LDAP so will use cached credentials: " + e.getDiagnosticMessage());
        return authenticationCache.getUserGroupsAndResetCheckTime(millis, username);
      } else {
        throw new LDAPAuthException("Cannot find '" + username + "' in the LDAP store", e);
      }
    }
  }

  private String getTemplateUser(List<String> groups) {
    if (groupUserMap != null) {
      for (String group : groups) {
        if (groupUserMap.containsKey(group)) {
          return groupUserMap.get(group);
        }
      }
    }
    return null;
  }

  private List<String> getRights(List<String> groups, List<String> existingRights) {
    List<String> result = new ArrayList<String>();
    if (existingRights != null)
      result.addAll(existingRights);
    if (groupRightMap != null) {
      for (String group : groups) {
        if (groupRightMap.containsKey(group)) {
          String rights = groupRightMap.get(group);
          String[] rightsArray = rights.split("\\s*,\\s*");
          for (String right : rightsArray) {
            if (!result.contains(right))
              result.add(right);
          }
        }
      }
    }
    return result;
  }

  private List<String> getAccessSets(List<String> groups, List<String> existingAccess) {
    List<String> result = new ArrayList<String>();
    if (existingAccess != null)
      result.addAll(existingAccess);
    if (groupAccessMap != null) {
      for (String group : groups) {
        if (groupAccessMap.containsKey(group)) {
          String accessSets = groupAccessMap.get(group);
          String[] accessSetArray = accessSets.split("\\s*,\\s*");
          for (String access : accessSetArray) {
            if (!result.contains(access))
              result.add(access);
          }
        }
      }
    }
    return result;
  }

  private Map<String, Object> getLDAPOther(String username){
    Map<String, Object> result = new HashMap<>();
    result.put("LDAP", username);
    return result;
  }

  @Override
  public String authAndComposeJWTIdToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    boolean authed = authForLDAPUser(username, password);
    if (authed) {
      return provider.produceJWTID(username, getName(), getLDAPOther(username));
    }
    throw new LDAPAuthException("The user '" + username + "' is not associated with a group with rights");
  }

  @Override
  public String composeJWTAccessTokenForAuthenticatedUser(JWTProvider provider, String username) throws AuthenticationException {
    List<String> groups = getGroupsForLDAPUser(username);
    String templateUser = getTemplateUser(groups);
    if (templateUser != null) {
      UserData data = userInformation.getUser(templateUser);
      if (data != null) {
        List<String> accessSets = getAccessSets(groups, data.getAccess());
        List<String> rights = getRights(groups, data.getRights());
        return provider.produceJWTAccess(templateUser, accessSets, rights, getLDAPOther(username));
      }
    }
    throw new LDAPAuthException("The user '" + username + "' is not associated with a group with rights");
  }

  @Override
  public String authAndComposeJWTAccessToken(JWTProvider provider, String username, String password) throws AuthenticationException {
    List<String> groups = authAndGetGroupsForLDAPUser(username, password);
    String templateUser = getTemplateUser(groups);
    if (templateUser != null) {
      UserData data = userInformation.getUser(templateUser);
      if (data != null) {
        List<String> accessSets = getAccessSets(groups, data.getAccess());
        List<String> rights = getRights(groups, data.getRights());
        return provider.produceJWTAccess(templateUser, accessSets, rights, getLDAPOther(username));
      }
    }
    throw new LDAPAuthException("The user '" + username + "' is not associated with a group with rights");
  }

  @Override
  public JWTAuthToken authAndGenerateAccessToken(String username, String password) {
    List<String> groups = authAndGetGroupsForLDAPUser(username, password);
    String templateUser = getTemplateUser(groups);
    UserData data = userInformation.getUser(templateUser);
    if (data != null) {
      List<String> accessSets = getAccessSets(groups, data.getAccess());
      List<String> rights = getRights(groups, data.getRights());
      String[] rightsArray = new String[rights.size()];
      rights.toArray(rightsArray);

      JWTAuthToken jwtAuthToken = new JWTAuthToken(templateUser, accessSets, getLDAPOther(username), AuthorityUtils.createAuthorityList(rightsArray), JWTAuthToken.TokenUse.ACCESS);
      jwtAuthToken.setAuthenticated(true);
      return jwtAuthToken;
    }
    else {
      JWTAuthToken jwtAuthToken = new JWTAuthToken(username, null, getLDAPOther(username), null, JWTAuthToken.TokenUse.ACCESS);
      jwtAuthToken.setAuthenticated(false);
      return jwtAuthToken;
    }
  }
}