package za.co.synthesis.regulatory.report.support.properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by jake on 3/17/17.
 */
public class EnvEntryPropertyPlaceholderConfigurer extends
        PropertyPlaceholderConfigurer {

  public static final String CONTAINER_PREFIX = "java:comp/env/";
  public static final String USERPASSWORDHASH = "settingsPassword";
  public static final String PREFIX_USERENCRYPT = "UENC:";

  private SettingsCrypto userCrypto = null;
  private boolean resourceRef = true;
  private Properties loadedProps = null;

  public void setResourceRef(boolean resourceRef) {
    this.resourceRef = resourceRef;
  }

  public boolean isResourceRef() {
    return resourceRef;
  }

  private boolean envEntryOverride = false;

  private JndiTemplate jndiTemplate;

  public void setEnvEntryOverride(boolean contextOverride) {
    this.envEntryOverride = contextOverride;
  }

  public String resolvePlaceholderByLoadedProps(String placeholder) {
    return super.resolvePlaceholder(placeholder, loadedProps);
  }

  protected String resolvePlaceholder(String placeholder, Properties props) {
    String value = null;
    if (jndiTemplate == null)
      jndiTemplate = new JndiTemplate();

    if (this.envEntryOverride) {
      value = resolveEnvEntryPlaceholder(placeholder);
      if (value != null)
        logger.info("Resolved property " + placeholder + " to " + maskPassword(placeholder,value) + " from context. Context entry overrides properties file.");
      else
        logger.debug("Could not resolve property " + placeholder + " from context. Will attempt to resolve it from properties file.");
    }
    if (value == null) {
      value = super.resolvePlaceholder(placeholder, props);
      if (value != null)
        logger.info("Resolved property " + placeholder + " to " + maskPassword(placeholder,value) + " from properties file.");
    }
    if (value == null) {
      value = resolveEnvEntryPlaceholder(placeholder);
      if (value != null)
        logger.info("Resolved property " + placeholder + " to " + maskPassword(placeholder,value) + " from context.");
    }
    return value;
  }

  /**
   * Return ***** instead of the value if the placeholder contains the string 'password' somewhere.
   * @param placeholder to be checked for 'password'
   * @param value to be masked
   * @return Masked password, otherwise the original string.
   */
  private String maskPassword(String placeholder, String value) {
    if ( placeholder == null ) return value;
    else if ( placeholder.toLowerCase().contains( "password" ) ) return "*****";
    else return value;
  }

  protected String resolveEnvEntryPlaceholder(String placeholder) {
    String value = null;
    Object envEntry = null;
    String jndiNameToUse = convertJndiName(placeholder);
    try {
      envEntry = jndiTemplate.lookup(jndiNameToUse);
    } catch (NamingException e) {
      logger.debug("Naming Exception while trying to lookup env entry "
              + placeholder + " (" + jndiNameToUse + "):" + e.getMessage());
    }
    if (envEntry != null) {
      value = envEntry.toString();

      // If an encrypted setting then look up settingsPassword and use it to decrypt
      if (value != null && value.startsWith(PREFIX_USERENCRYPT)) {
        if (userCrypto == null) {
          jndiNameToUse = convertJndiName(USERPASSWORDHASH);
          try {
            envEntry = jndiTemplate.lookup(jndiNameToUse);
          } catch (NamingException e) {
            logger.debug("Naming Exception while trying to lookup env entry "
                    + USERPASSWORDHASH + " (" + jndiNameToUse + "):" + e.getMessage());
          }
          if (envEntry != null) {
            String userPassword = envEntry.toString();
            if (userPassword != null) {
              userPassword = SettingsCrypto.Standard().decrypt(userPassword);
              userCrypto = SettingsCrypto.UserPassword(userPassword);
            }
          }
        }

        if (userCrypto != null)
          value = userCrypto.decrypt(value.substring(PREFIX_USERENCRYPT.length()));
      }
    }
    return value;
  }

  protected String convertJndiName(String jndiName) {
    if (isResourceRef() && !jndiName.startsWith(CONTAINER_PREFIX)
            && jndiName.indexOf(':') == -1) {
      jndiName = (new StringBuffer(CONTAINER_PREFIX).append(jndiName))
              .toString();
    }
    return jndiName;
  }

  @Override
  protected void loadProperties(Properties props) throws IOException {
    super.loadProperties(props);
    loadedProps = props;
  }
}
