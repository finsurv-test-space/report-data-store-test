package za.co.synthesis.regulatory.report.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import za.co.synthesis.regulatory.report.service.StartJetty;
import za.co.synthesis.regulatory.report.support.HttpResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jake on 6/8/17.
 */
public class ReportServicesBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
  @Override
  public void commence(final HttpServletRequest request,
                       final HttpServletResponse response,
                       final AuthenticationException authException) throws IOException, ServletException {
    if (SecurityHelper.getAuthMeta() == null) {
      String myUrl = request.getRequestURI();

//    if (myUrl.contains("internal")) {
//      response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//      response.setHeader("Location", "/report-data-store/internal/login.html");
//    }
//    else {
      //Authentication failed, send error response.
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

      HttpResponseUtil.writeError(response, authException.getClass().getSimpleName(), authException.getMessage());
//    }
    }
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    setRealmName(StartJetty.REALM);
    super.afterPropertiesSet();
  }
}