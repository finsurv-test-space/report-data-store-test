package za.co.synthesis.regulatory.report.support.properties;

/**
 * Created by jake on 3/17/17.
 */
public class PropertyException extends Exception {
  public PropertyException(String message) {
    super(message);
  }

  public PropertyException(String message, java.lang.Throwable throwable) {
    super(message, throwable);
  }
}