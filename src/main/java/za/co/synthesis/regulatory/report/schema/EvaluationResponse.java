package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.rule.core.*;

import javax.persistence.MappedSuperclass;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jake on 4/14/16.
 */
@JsonPropertyOrder({
        "Decision"
        , "ReportingSide"
        , "ReportingQualifier"
        , "Flow"
        , "ResSide"
        , "ResAccountType"
        , "ResException"
        , "NonResSide"
        , "NonResAccountType"
        , "NonResException"
        , "Subject"
        , "LocationCountry"
        , "ManualSection"
        , "DrSideSwift"
        , "Category"
        , "NotCategory"
        , "PossibleDrAccountTypes"
        , "PossibleCrAccountTypes"
})
@MappedSuperclass
public class EvaluationResponse {
  public enum ReportableDecision {
    ReportToRegulator("ReportToRegulator"),
    DoNotReport("DoNotReport"),
    Illegal("Illegal"),
    Unknown("Unknown");

    String text;

    ReportableDecision(String text){
      this.text = text;
    }

    public String toString(){
      return text;
    }

    public boolean equals(String text){
      return this.text.equals(text);
    }

    public static ReportableDecision fromString(String text){
      for (ReportableDecision decision : ReportableDecision.values()){
        if (decision.equals(text)){
          return decision;
        }
      }
      return ReportableDecision.Unknown;
    }
  }

  private ReportableDecision reportableDecision;
  private String reportingQualifier;
  private String flow;
  private DrCr reportingSide;
  private DrCr nonResSide;
  private ReportingAccountType nonResAccountType;
  private String nonResException;
  private DrCr resSide;
  private ReportingAccountType resAccountType;
  private String resException;
  private String subject;
  private String locationCountry;
  private String manualSection;
  private String drSideSwift;
  @JsonIgnore
  private AccountHolderStatus accStatusFilter;
  private List<String> category;
  private List<String> notCategory;
  @JsonIgnore
  private List<BankAccountType> drAccountTypes;
  @JsonIgnore
  private List<BankAccountType> crAccountTypes;

  public EvaluationResponse() {
    category = new ArrayList<String>();
    notCategory = new ArrayList<String>();
    drAccountTypes = new ArrayList<BankAccountType>();
    crAccountTypes = new ArrayList<BankAccountType>();
  }

  public EvaluationResponse(IEvaluationDecision decision) {
    reportableDecision = ReportableDecision.Unknown;
    if (decision.getReportable() == ReportableType.Reportable) {
      reportableDecision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCUS";
    }
    else
    if (decision.getReportable() == ReportableType.ZZ1Reportable) {
      reportableDecision = decision.isZZ1Reportable() ? ReportableDecision.ReportToRegulator : ReportableDecision.DoNotReport;
      reportingQualifier = "NON REPORTABLE";
    }
    else
    if (decision.getReportable() == ReportableType.CARDResReportable) {
      reportableDecision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCARD RESIDENT";
    }
    else
    if (decision.getReportable() == ReportableType.CARDNonResReportable) {
      reportableDecision = ReportableDecision.ReportToRegulator;
      reportingQualifier = "BOPCARD NON RESIDENT";
    }
    else
    if (decision.getReportable() == ReportableType.NotReportable) {
      reportableDecision = ReportableDecision.DoNotReport;
    }
    if (decision.getReportable() == ReportableType.Illegal) {
      reportableDecision = ReportableDecision.Illegal;
    }

    flow = decision.getFlow() != null ? (decision.getFlow() == FlowType.Inflow ? "IN" : "OUT") : null;
    reportingSide = decision.getReportingSide();
    nonResSide = decision.getNonResSide();
    nonResAccountType = decision.getNonResAccountType();
    nonResException = decision.getNonResException();
    resSide = decision.getResSide();
    resAccountType = decision.getResAccountType();
    resException = decision.getResException();
    subject = decision.getSubject();
    locationCountry = decision.getLocationCountry();
    manualSection = decision.getManualSection();
    drSideSwift = decision.getDrSideSwift();
    accStatusFilter = decision.getAccStatusFilter();
    category = decision.getCategory();
    notCategory = decision.getNotCategory();
    drAccountTypes = decision.getPossibleDrAccountTypes();
    crAccountTypes = decision.getPossibleCrAccountTypes();
  }

  public void setReportableDecision(ReportableDecision reportableDecision) {
    this.reportableDecision = reportableDecision;
  }

  public void setReportingQualifier(String reportingQualifier) {
    this.reportingQualifier = reportingQualifier;
  }

  public void setFlowType(FlowType flow) {
    this.flow = flow != null ? (flow == FlowType.Inflow ? "IN" : "OUT") : null;
  }
  public void setFlow(String flow) {
    this.flow = flow;
  }

  public void setReportingSide(DrCr reportingSide) {
    this.reportingSide = reportingSide;
  }

  public void setNonResSide(DrCr nonResSide) {
    this.nonResSide = nonResSide;
  }

  public void setNonResAccountType(ReportingAccountType nonResAccountType) {
    this.nonResAccountType = nonResAccountType;
  }

  public void setNonResException(String nonResException) {
    this.nonResException = nonResException;
  }

  public void setResSide(DrCr resSide) {
    this.resSide = resSide;
  }

  public void setResAccountType(ReportingAccountType resAccountType) {
    this.resAccountType = resAccountType;
  }

  public void setResException(String resException) {
    this.resException = resException;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public void setLocationCountry(String locationCountry) {
    this.locationCountry = locationCountry;
  }

  public void setManualSection(String manualSection) {
    this.manualSection = manualSection;
  }

  public void setDrSideSwift(String drSideSwift) {
    this.drSideSwift = drSideSwift;
  }

  public void setCategory(List<String> category) {
    this.category = category;
  }

  public void setNotCategory(List<String> notCategory) {
    this.notCategory = notCategory;
  }

  public void setAccStatusFilter(AccountHolderStatus accStatusFilter) {
    this.accStatusFilter = accStatusFilter;
  }

  public void setDrAccountTypes(List<BankAccountType> drAccountTypes) {
    this.drAccountTypes = drAccountTypes;
  }

  public void setCrAccountTypes(List<BankAccountType> crAccountTypes) {
    this.crAccountTypes = crAccountTypes;
  }

  @JsonProperty("PossibleDrAccountTypes")
  public List<BankAccountType> getPossibleDrAccountTypes() {
    return drAccountTypes;
  }

  @JsonProperty("PossibleCrAccountTypes")
  public List<BankAccountType> getPossibleCrAccountTypes() {
    return crAccountTypes;
  }

  @JsonProperty("Decision")
  public ReportableDecision getReportableDecision() {
    return reportableDecision;
  }

  @JsonProperty("ReportingQualifier")
  public String getReportingQualifier() {
    return reportingQualifier;
  }

  @JsonProperty("ReportingSide")
  public DrCr getReportingSide() {
    return reportingSide;
  }

  @JsonProperty("Flow")
  public String getFlow() {
    return flow;
  }

  @JsonProperty("NonResSide")
  public DrCr getNonResSide() {
    return nonResSide;
  }

  @JsonProperty("NonResAccountType")
  public ReportingAccountType getNonResAccountType() {
    return nonResAccountType;
  }

  @JsonProperty("NonResException")
  public String getNonResException() {
    return nonResException;
  }

  @JsonProperty("ResSide")
  public DrCr getResSide() {
    return resSide;
  }

  @JsonProperty("ResAccountType")
  public ReportingAccountType getResAccountType() {
    return resAccountType;
  }

  @JsonProperty("ResException")
  public String getResException() {
    return resException;
  }

  @JsonProperty("Subject")
  public String getSubject() {
    return subject;
  }

  @JsonProperty("LocationCountry")
  public String getLocationCountry() {
    return locationCountry;
  }

  @JsonProperty("ManualSection")
  public String getManualSection() {
    return manualSection;
  }

  @JsonProperty("DrSideSwift")
  public String getDrSideSwift() {
    return drSideSwift;
  }

  @JsonProperty("Category")
  public List<String> getCategory() {
    return category;
  }

  @JsonProperty("NotCategory")
  public List<String> getNotCategory() {
    return notCategory;
  }

  public static List<EvaluationResponse> getEvaluationResponseArray(List<IEvaluationDecision> decisions){
    List<EvaluationResponse> result = new ArrayList<EvaluationResponse>();
    if (decisions != null){
      for (IEvaluationDecision decision : decisions){
        if (decision != null){
          result.add(new EvaluationResponse(decision));
        }
      }
    }
    return result;
  }
}
