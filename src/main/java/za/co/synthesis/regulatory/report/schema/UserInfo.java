package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.regulatory.report.persist.UserData;

import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/06/21.
 */
@JsonPropertyOrder({
        "Auth"
        , "Channels"
        , "ReportLists"
        , "UserEvents"
        , "Access"
})
public class UserInfo {
  @JsonPropertyOrder({
          "Username"
          , "Password"
          , "Rights"
          , "Access"
          , "Active"
          , "Details"

  })
  public static class Auth {
    private String username;
    private String password;
    private List<String> rights;
    private List<String> access;
    private Boolean active;

    public Auth() {
    }

    public Auth(UserData user) {
      this.username = user.getUsername();
      this.password = user.getPassword();
      this.rights = user.getRights();
      this.access = user.getAccess();
      this.active = user.getActive();
    }

    @JsonProperty("Username")
    public String getUsername() {
      return username;
    }

      /**
       * This password is not necessarily the plaintext password.
       * Presently Report Services (Report Data Store) makes use of BCrypt hashes for storing and comparing passwords.
       * @return
       */
    @JsonProperty("Password")
    public String getPassword() {
      return password;
    }

    @JsonProperty("Rights")
    public List<String> getRights() {
      return rights;
    }

    @JsonProperty("Access")
    public List<String> getAccess() {
        return access;
    }

    @JsonProperty("Active")
    public Boolean getActive() { return active; }

    public void setUsername(String username) {
      this.username = username;
    }

    public void setRights(List<String> rights) {
      this.rights = rights;
    }

    public void setAccess(List<String> access) {
      this.access = access;
    }

    public void setActive(Boolean active) { this.active = active; }
  }

  private Auth auth;
  private List<Channel> channels;
  private List<String> reportLists;
  private List<String> userEvents;
  private Map<String, Object> details;

  public UserInfo() {
  }

  public UserInfo(UserData auth, List<Channel> channels, List<String> reportLists, List<String> userEvents, Map<String, Object> details) {
    this.auth = new Auth(auth);
    this.channels = channels;
    this.reportLists = reportLists;
    this.userEvents = userEvents;
    this.details = details;
  }

  @JsonProperty("Auth")
  public Auth getAuth() {
    return auth;
  }

  @JsonProperty("Channels")
  public List<Channel> getChannels() {
    return channels;
  }

  @JsonProperty("ReportLists")
  public List<String> getReportLists() {
    return reportLists;
  }

  @JsonProperty("Details")
  public Map<String, Object> getDetails() { return details; }

  @JsonProperty("UserEvents")
  public List<String> getUserEvents() {
    return userEvents;
  }

  public void setAuth(Auth auth) {
    this.auth = auth;
  }

  public void setChannels(List<Channel> channels) {
    this.channels = channels;
  }

  public void setDetails(Map<String, Object> details) {
    this.details = details;
  }

  public void setReportLists(List<String> reportLists) {
    this.reportLists = reportLists;
  }

  public void setUserEvents(List<String> userEvents) {
    this.userEvents = userEvents;
  }
}
