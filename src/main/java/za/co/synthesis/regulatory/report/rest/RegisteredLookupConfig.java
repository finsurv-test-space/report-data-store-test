package za.co.synthesis.regulatory.report.rest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by james on 2017/05/31.
 */
public class RegisteredLookupConfig {
    public enum LookupSourceType{
        STATIC,
        FILE,
        DATABASE,
        REST
    }

    public enum LookupSourceFormat{ //relevant for sources: FILE, REST
        JSON,
        XML,
        CSV
    }


    String name;
    LookupSourceType sourceType = LookupSourceType.STATIC;
    LookupSourceFormat sourceFormat = LookupSourceFormat.JSON;
    Map<String, String> sourceParams = new HashMap<>();
    //TODO: ...add functionality and params for Caching/refresh settings etc.
    //TODO: ...add model schema & example / POJO source file for runtime compile?
    //TODO: ...add description/documentation?

    public RegisteredLookupConfig(String name){
        this.name = name;
    }

    public RegisteredLookupConfig(String name, Map<String, String> sourceParams){
        this.name = name;
        this.sourceParams = sourceParams;
    }

    public RegisteredLookupConfig param(String paramName, String paramValue){
        this.sourceParams.put(paramName, paramValue);
        return this;
    }

    public RegisteredLookupConfig staticLookup(){
        //defined in the finsurv-rule lookups.js files as needed.
        this.sourceType = LookupSourceType.STATIC;
        return this;
    }

    public RegisteredLookupConfig restLookup(String url, Map<String, String> params){
        //TODO: add rest lookup functionality
        this.sourceType = LookupSourceType.REST;
        return this;
    }

    public RegisteredLookupConfig dbLookup(/*db params*/){
        //TODO: add db lookup functionality
        this.sourceType = LookupSourceType.DATABASE;
        return this;
    }

    public RegisteredLookupConfig fileLookup(String filePath, LookupSourceFormat sourceFormat){
        //TODO: add file lookup functionality
        this.sourceType = LookupSourceType.DATABASE;
        return this;
    }



}
