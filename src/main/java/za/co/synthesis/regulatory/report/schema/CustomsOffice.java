package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by james on 2017/05/30.
 */
@JsonPropertyOrder({
        "Code"
        , "Name"
})
public class CustomsOffice {
    private String code;
    private String name;

    public CustomsOffice(Object code, Object name) {
        this.code = (String)code;
        this.name = (String)name;
    }

    @JsonProperty("Code")
    public String getCode() {
        return code;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }
}
