package za.co.synthesis.regulatory.report.businesslogic;

import org.springframework.beans.factory.annotation.Required;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;
import za.co.synthesis.regulatory.report.support.properties.CompoundResource;
import za.co.synthesis.regulatory.report.swagger.SwaggerUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by jake on 7/27/17.
 */
public class TypeLogic {
  private IConfigStore configStore;
  private CompoundResource environmentResources;

  @Required
  public void setConfigStore(IConfigStore configStore) {
    this.configStore = configStore;
  }

  public void setEnvironmentResources(CompoundResource environmentResources) {
    this.environmentResources = environmentResources;
  }

  @Required

  public JSObject getType(String typeName, String schema) throws Exception {
    if (typeName.equals("report")) {
      String meta_path = configStore.getDefaultTypeLocation()+"meta/";
      String report_path = configStore.getDefaultTypeLocation() + schema + "/";
      String compoundReport_path = configStore.getDefaultTypeLocation()+"report/";

      JSObject metaType = configStore.getTypeJSObject(meta_path + "type");
      JSObject customMetaType = configStore.getTypeJSObject(meta_path + "custom");

      SwaggerUtils.mergeTypes(metaType, customMetaType);

      JSObject reportType = configStore.getTypeJSObject(report_path + "type");
      JSObject customReportType = configStore.getTypeJSObject(report_path + "custom");

      SwaggerUtils.mergeTypes(reportType, customReportType);

      JSObject compoundType = configStore.getTypeJSObject(compoundReport_path + "type");

      SwaggerUtils.mergeTypes(compoundType, metaType);
      SwaggerUtils.mergeTypes(compoundType, reportType);

      return compoundType;
    }
    else
    if (typeName.equals("report_core")) {
      String resource_path = configStore.getDefaultTypeLocation() + schema + "/";

      JSObject coreType = configStore.getTypeJSObject(resource_path + "type");
      JSObject customType = configStore.getTypeJSObject(resource_path + "custom");

      SwaggerUtils.mergeTypes(coreType, customType);
      return coreType;
    }
    else
    if (typeName.equals("report_meta")) {
      String resource_path = configStore.getDefaultTypeLocation() + schema + "/";

      JSObject coreType = configStore.getTypeJSObject(resource_path + "type");
      JSObject customType = configStore.getTypeJSObject(resource_path + "custom");

      SwaggerUtils.mergeTypes(coreType, customType);
      return coreType;
    }
    else
    if (typeName.equals("error")) {
      String resource_path = configStore.getDefaultTypeLocation() + typeName + "/";

      JSObject coreType = configStore.getTypeJSObject(resource_path + "type");

      return coreType;
    }
    return null;
  }
}
