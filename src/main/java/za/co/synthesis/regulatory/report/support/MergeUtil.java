package za.co.synthesis.regulatory.report.support;

import za.co.synthesis.javascript.*;
import za.co.synthesis.regulatory.report.dao.IExternalLookup;
import za.co.synthesis.regulatory.report.persist.IConfigStore;
import za.co.synthesis.regulatory.report.persist.LookupData;

import java.util.List;
import java.util.Map;

public class MergeUtil {
  private static void updateLookups(JSObject lookups, IExternalLookup lookup, String name) throws Exception {
    JSArray result = lookup.getFullLookupList(name);
    if (result != null) {
      lookups.remove(name);
      lookups.put(name, result);
    }
  }

  public static JSObject mergeDBIntoLookups(JdbcConnectionManager jdbcConnectionManager, IConfigStore configStore, JSObject initialLookups, List<LookupData> lookups) throws Exception {
    for (LookupData lookupData : lookups) {
      if (lookupData.getSql() != null) {
        DBExternalLookup dbQuery = new DBExternalLookup(jdbcConnectionManager, lookupData);
        updateLookups(initialLookups, dbQuery, lookupData.getName());
      }
      if (lookupData.getRest() != null) {
        RestCallExternalLookup restQuery = new RestCallExternalLookup(configStore, lookupData);
        updateLookups(initialLookups, restQuery, lookupData.getName());
      }
    }
    return initialLookups;
  }

  public static String mergeDBIntoLookups(JdbcConnectionManager jdbcConnectionManager, IConfigStore configStore, String initialLookups, List<LookupData> lookups) throws Exception {
    JSStructureParser parser = new JSStructureParser(initialLookups);
    Object data = parser.parse();

    JSObject jsLookups = JSUtil.findObjectWith(data, "categories");
    mergeDBIntoLookups(jdbcConnectionManager, configStore, jsLookups, lookups);

    JSWriter writer = new JSWriter();
    writer.setNewline("\n");
    writer.setIndent(" ");

    if (data instanceof JSObject) {
      JSObject jsData = (JSObject)data;
      for (Map.Entry<String, Object> entry : jsData.entrySet()) {
        ((JSFunctionCall)entry.getValue()).compose(writer);
        break;
      }
    }
    else
    if (data instanceof JSArray) {
      ((JSArray)data).compose(writer);
    }
    return writer.toString();
  }
}
