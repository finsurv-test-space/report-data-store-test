package za.co.synthesis.regulatory.report.persist.types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;

/**
 * Created by james on 2017/09/05.
 */
@Entity
public class DocumentDownloadObject extends PersistenceObject {
  private static final Logger logger = LoggerFactory.getLogger(DocumentDownloadObject.class);

  private String reportSpace;
  private String reportKey;
  private String documentHandle;
  private String documentGuid;

  public DocumentDownloadObject(String uuid) {
    super(uuid);
  }

  public DocumentDownloadObject(String uuid, String reportSpace, String reportKey, String documentHandle, String documentGuid) {
    super(uuid);
    this.reportSpace = reportSpace;
    this.reportKey = reportKey;
    this.documentHandle = documentHandle;
    this.documentGuid = documentGuid;
  }

  public DocumentDownloadObject(String uuid, DocumentObject documentObject) {
    super(uuid);
    this.reportSpace = documentObject.getReportSpace();
    this.reportKey = documentObject.getReportKey();
    this.documentHandle = documentObject.getDocumentHandle();
    this.documentGuid = documentObject.getGUID();
  }

  public String getReportSpace() {
    return reportSpace;
  }

  public void setReportSpace(String reportSpace) {
    this.reportSpace = reportSpace;
  }

  public String getReportKey() {
    return reportKey;
  }

  public void setReportKey(String reportKey) {
    this.reportKey = reportKey;
  }

  public String getDocumentHandle() {
    return documentHandle;
  }

  public void setDocumentHandle(String documentHandle) {
    this.documentHandle = documentHandle;
  }

  public String getDocumentGuid() {
    return documentGuid;
  }

  public void setDocumentGuid(String documentGuid) {
    this.documentGuid = documentGuid;
  }
}
