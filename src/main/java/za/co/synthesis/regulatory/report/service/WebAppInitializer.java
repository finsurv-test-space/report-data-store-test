package za.co.synthesis.regulatory.report.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import za.co.synthesis.regulatory.report.support.properties.CompoundProperties;
import za.co.synthesis.regulatory.report.swagger.SwaggerDocFilter;

import javax.servlet.*;
import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumSet;

/**
 * Created by kudzimutamba on 2017/09/28.
 */
public class WebAppInitializer implements WebApplicationInitializer {
  static final Logger logger = LoggerFactory.getLogger(WebAppInitializer.class);

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {

    String contextPath = servletContext.getContextPath();
    logger.debug("...Servlet context path: \""+contextPath+"\".");
    //register the Spring Root Context configuration.
    String externalXMLpath = System.getProperty("spring-external-root-path");
    if (externalXMLpath != null){
      externalXMLpath = externalXMLpath.trim();
      logger.debug("...External xml root file path provided by system property (spring-external-root-path): \""+externalXMLpath+"\".");
      if (Files.isRegularFile(Paths.get(externalXMLpath))) {
        String filePath = Paths.get(externalXMLpath).toUri().toString().trim();
        logger.debug("...Loading external xml root file from: \""+filePath+"\".");
        servletContext.setInitParameter("contextConfigLocation", filePath);
      }
    } else {
      logger.debug("...External xml root file path NOT PROVIDED by system property (spring-external-root-path) - attempting to resolve...");
      //set default path
      String pathToLoad = "conf/report-service-external-root.xml";
      String externalXmlResourcePath = "file:"+pathToLoad;
      File file = new File(pathToLoad);
      //URL u = String.class.getResource(externalXmlResourcePath);
//      if (file.exists() || u != null) {
      if (file.exists() && file.isFile()) {
        logger.debug("...External xml root file path: \"" + file.getAbsolutePath() + "\".");
        logger.debug("...External xml root file seems to exist at: \""+externalXmlResourcePath+"\".");
        servletContext.setInitParameter("contextConfigLocation", externalXmlResourcePath);
      } else {
        pathToLoad = "../conf/report-service-external-root.xml";
        externalXmlResourcePath = "file:"+pathToLoad;
        file = new File(pathToLoad);
//        u = String.class.getResource(externalXmlResourcePath);
//        if (file.exists() || u != null) {
        if (file.exists() && file.isFile()) {
          logger.debug("...External xml root file path: \"" + file.getAbsolutePath() + "\".");
          logger.debug("...External xml root file seems to exist at: \"" + externalXmlResourcePath + "\".");
        } else {
            logger.debug("...External xml root file defaulted to: \"" + externalXmlResourcePath + "\".");
        }
        servletContext.setInitParameter("contextConfigLocation", externalXmlResourcePath);
      }
//      servletContext.setInitParameter("contextConfigLocation", "file:conf/report-service-external-root.xml");
    }
    servletContext.addListener(new ContextLoaderListener());


    //Add Swagger Docs filter
    FilterRegistration.Dynamic swaggerDocsFilter = servletContext.addFilter("SwaggerDocs",SwaggerDocFilter.class);
    swaggerDocsFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),false,"/v2/api-docs");

    //Add the security filters
    DelegatingFilterProxy targetDelegateFilter = new DelegatingFilterProxy("reportServiceSecurityFilterChain");
    FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter("springSecurityFilterChain",targetDelegateFilter);
    springSecurityFilterChain.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.ERROR, DispatcherType.ASYNC),false,"/*");

    //Register the WebApp Dispatcher servlet.
    AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
    ctx.register(WebAppConfig.class);
    ctx.setServletContext(servletContext);

    
    //***  NOTE: For reference on upload limits see 3. With Servlet 3.0: http://www.baeldung.com/spring-file-upload  ***//
    File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
    logger.debug("...Upload temp dir (java.io.tmpdir) set to: \"" + uploadDirectory + "\".");

    MultipartConfigElement multipartConfigElement = StartJetty.GetMultipartLimitConfigs(logger, uploadDirectory);
  
    ServletRegistration.Dynamic servlet = servletContext.addServlet("/", new DispatcherServlet(ctx));
    servlet.setMultipartConfig(multipartConfigElement);
    servlet.setLoadOnStartup(1);
    servlet.addMapping("/*");
  }
}
