package za.co.synthesis.regulatory.report.dao;

/**
 * Created by jake on 3/22/17.
 */
public class JdbcEntry {
  private String transactionValue;

  JdbcEntry(String transactionValue) {
    this.transactionValue = transactionValue;
  }

  String getTransactionValue() {
    return transactionValue;
  }
}
