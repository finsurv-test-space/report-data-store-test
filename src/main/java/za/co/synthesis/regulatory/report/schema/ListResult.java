package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 6/27/17.
 */
@JsonPropertyOrder({
        "Headers"
        , "Rows"
})
public class ListResult {
  private final List<String> headers = new ArrayList<String>();
  private final List<Map<String, String>> rows = new ArrayList<Map<String, String>>();

  @JsonProperty("Headers")
  public List<String> getHeaders() {
    return headers;
  }

  @JsonProperty("Rows")
  public List<Map<String, String>> getRows() {
    return rows;
  }
}
