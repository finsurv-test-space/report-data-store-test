package za.co.synthesis.regulatory.report.persist;

import java.util.List;

/**
 * Created by james on 2017/09/05.
 */

public class ReportInfoDownload {
  private final String upToSyncpoint;
  private final List<ReportInfo> reportInfoList;

  public ReportInfoDownload(String upToSyncpoint, List<ReportInfo> reportInfoList) {
    this.upToSyncpoint = upToSyncpoint;
    this.reportInfoList = reportInfoList;
  }

  public String getUpToSyncpoint() {
    return upToSyncpoint;
  }

  public List<ReportInfo> getReportInfoList() {
    return reportInfoList;
  }
}