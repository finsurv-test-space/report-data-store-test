package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import za.co.synthesis.javascript.JSObject;

@JsonPropertyOrder({
    "PublicKey",
    "Id",
    "Algorithm",
    "Source",
    "Expiry"
})
public class PublicKeyRef {
  private String id;
  private String publicKey;
  private String algorithm;
  private String source;
  private String expiry;
  
  @JsonProperty("PublicKey")
  public String getPublicKey() {
    return publicKey;
  }
  
  public void setPublicKey(String publicKey) {
    this.publicKey = publicKey;
  }
  
  @JsonProperty("Id")
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  @JsonProperty("Algorithm")
  public String getAlgorithm() {
    return this.algorithm;
  }
  
  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }
  
  @JsonProperty("Source")
  public String getSource() {
    return this.source;
  }
  
  public void setSource(String source) {
    this.source = source;
  }
  
  @JsonProperty("Expiry")
  public String getExpiry() {
    return this.expiry;
  }
  
  public void setExpiry(String expiry) {
    this.expiry = expiry;
  }
  
}