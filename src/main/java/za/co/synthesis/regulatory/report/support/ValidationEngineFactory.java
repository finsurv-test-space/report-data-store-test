package za.co.synthesis.regulatory.report.support;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.regulatory.report.validate.ValidateFactory;
import za.co.synthesis.regulatory.report.validate.ValidationRunner;
import za.co.synthesis.rule.core.ValidationEngine;
import za.co.synthesis.rule.core.impl.DefaultSupporting;
import za.co.synthesis.rule.support.Util;

import java.io.StringReader;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 3/31/16.
 */
public class ValidationEngineFactory {
  private static class EngineEntry {
    private final ValidationEngine engine;
    private PackageCache.Entry lookupEntry;
    private PackageCache.Entry validationEntry;
    private final DefaultSupporting supporting;

    public EngineEntry(ValidationEngine engine, DefaultSupporting supporting) {
      this.engine = engine;
      this.supporting = supporting;
    }

    public EngineEntry(ValidationEngine engine, DefaultSupporting supporting, PackageCache.Entry lookupEntry, PackageCache.Entry validationEntry) {
      this.engine = engine;
      this.supporting = supporting;
      this.lookupEntry = lookupEntry;
      this.validationEntry = validationEntry;
    }

    public ValidationEngine getEngine() {
      return engine;
    }

    public DefaultSupporting getSupporting() {
      return supporting;
    }

    public PackageCache.Entry getLookupEntry() {
      return lookupEntry;
    }

    public void setLookupEntry(PackageCache.Entry lookupEntry) {
      this.lookupEntry = lookupEntry;
    }

    public PackageCache.Entry getValidationEntry() {
      return validationEntry;
    }

    public void setValidationEntry(PackageCache.Entry validationEntry) {
      this.validationEntry = validationEntry;
    }
  }

  private final Map<String, EngineEntry> validationEngineMap = new HashMap<String, EngineEntry>();
  private final Map<String, EngineEntry> documentEngineMap = new HashMap<String, EngineEntry>();
  private RulesCache rulesCache;
  private ValidationRunner validationRunner;

  public ValidationEngineFactory() {
  }

  @Required
  public void setRulesCache(RulesCache rulesCache) {
    this.rulesCache = rulesCache;
  }

  @Required
  public void setValidationRunner(ValidationRunner validationRunner) {
    this.validationRunner = validationRunner;
  }

  private EngineEntry getValidationEngine(String packageName) throws Exception {
    EngineEntry engineEntry = validationEngineMap.get(packageName);
    PackageCache.Entry lookupEntry = engineEntry != null ? engineEntry.getLookupEntry() : null;
    PackageCache.Entry validationEntry;

    if (lookupEntry != null) {
      if (rulesCache.isEntryOld(packageName, lookupEntry)) {
        lookupEntry = null;
      }
    }

    if (lookupEntry == null) {
      lookupEntry = rulesCache.getLookups(packageName, PackageCache.CacheStrategy.GetFromCache);
      validationEntry = rulesCache.getValidationRules(packageName, PackageCache.CacheStrategy.GetFromCache);

      if (lookupEntry != null && validationEntry != null) {
        ValidationEngine engine = new ValidationEngine();

        engine.setupLoadLookup(lookupEntry.getName(), new StringReader(lookupEntry.getData()));

        engine.setupLoadRule(validationEntry.getName(), new StringReader(validationEntry.getData()));

        DefaultSupporting supporting = new DefaultSupporting();
        supporting.setGoLiveDate(Util.date("2013-08-19"));
        supporting.setCurrentDate(LocalDate.now());
        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);
        engine.setupCustomValidateFactory(new ValidateFactory(rulesCache, packageName, validationRunner));

        if (validationEngineMap.containsKey(packageName)) {
          validationEngineMap.remove(packageName);
        }
        engineEntry = new EngineEntry(engine, supporting, lookupEntry, validationEntry);
        validationEngineMap.put(packageName, engineEntry);
      }
    }
    return engineEntry;
  }

  private EngineEntry getDocumentEngine(String packageName) throws Exception {
    EngineEntry engineEntry = documentEngineMap.get(packageName);
    PackageCache.Entry lookupEntry = engineEntry != null ? engineEntry.getLookupEntry() : null;
    PackageCache.Entry validationEntry;

    if (lookupEntry != null) {
      if (rulesCache.isEntryOld(packageName, lookupEntry)) {
        lookupEntry = null;
      }
    }

    if (lookupEntry == null) {
      lookupEntry = rulesCache.getLookups(packageName, PackageCache.CacheStrategy.GetFromCache);
      validationEntry = rulesCache.getDocumentRules(packageName, PackageCache.CacheStrategy.GetFromCache);

      if (lookupEntry != null && validationEntry != null) {
        ValidationEngine engine = new ValidationEngine();

        engine.setupLoadLookup(lookupEntry.getName(), new StringReader(lookupEntry.getData()));

        engine.setupLoadRule(validationEntry.getName(), new StringReader(validationEntry.getData()));

        DefaultSupporting supporting = new DefaultSupporting();
        supporting.setGoLiveDate(Util.date("2013-08-19"));
        supporting.setCurrentDate(LocalDate.now());
        engine.setupSupporting(supporting);
        engine.setIgnoreMissingCustomValidate(true);

        if (documentEngineMap.containsKey(packageName)) {
          documentEngineMap.remove(packageName);
        }
        engineEntry = new EngineEntry(engine, supporting, lookupEntry, validationEntry);
        documentEngineMap.put(packageName, engineEntry);
      }
    }
    return engineEntry;
  }

  public synchronized ValidationEngine getEngineForCurrentDate(String packageName) throws Exception {
    EngineEntry engineEntry = getValidationEngine(packageName);
    
    if (engineEntry.getSupporting() != null) {
      engineEntry.getSupporting().setCurrentDate(LocalDate.now());
    }
    return engineEntry.getEngine();
  }

  public synchronized ValidationEngine getDocumentEngineForCurrentDate(String packageName) throws Exception {
    EngineEntry engineEntry = getDocumentEngine(packageName);
  
    if (engineEntry.getSupporting() != null) {
      engineEntry.getSupporting().setCurrentDate(LocalDate.now());
    }
    return engineEntry.getEngine();
  }
}
