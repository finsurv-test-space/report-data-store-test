package za.co.synthesis.regulatory.report.persist.index;

import za.co.synthesis.regulatory.report.persist.CriterionType;

/**
 * Field to be indexed:  A field is either searchable of just a stored field
 */
public class IndexField {
  public static enum Usage {
    Search("Search"),
    Store("Store");

    private String text;

    Usage(String text){
      this.text = text;
    }

    public String toString(){
      return this.text;
    }
  }
  private final CriterionType type;
  private final Usage usage;
  private final String name;

  public IndexField(CriterionType type, Usage usage, String name) {
    this.type = type;
    this.usage = usage;
    this.name = name;
  }

  public CriterionType getType() {
    return type;
  }

  public Usage getUsage() {
    return usage;
  }

  public String getName() {
    return name;
  }
}
