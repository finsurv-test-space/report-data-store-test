package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by jake on 5/24/17.
 */
/*
ihqCompanies : [
    {
      ihqCode : "IHQ123",
      registrationNumber : "2013/1234567/07",
      companyName : "company1"
    }
 */
@JsonPropertyOrder({
        "IhqCode"
        , "RegistrationNumber"
        , "CompanyName"
})
public class IhqCompany {
  private String ihqCode;
  private String registrationNumber;
  private String companyName;

  public IhqCompany(Object ihqCode, Object registrationNumber, Object companyName) {
    this.ihqCode = (String)ihqCode;
    this.registrationNumber = (String)registrationNumber;
    this.companyName = (String)companyName;
  }

  @JsonProperty("IhqCode")
  public String getIhqCode() {
    return ihqCode;
  }

  public void setIhqCode(String ihqCode) {
    this.ihqCode = ihqCode;
  }

  @JsonProperty("RegistrationNumber")
  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  @JsonProperty("CompanyName")
  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
}
