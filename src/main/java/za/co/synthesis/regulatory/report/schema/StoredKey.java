package za.co.synthesis.regulatory.report.schema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;

import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDate;
import java.util.Base64;

public class StoredKey {
  private static final Logger logger = LoggerFactory.getLogger(StoredKey.class);

  private String keyId;
  private Key key;
  private String algorithm;
  private boolean isPrivateKey;
  private String expiry;

  //TODO: implement the following fields and all related logic
//  private String comment;
//  private String updateUrl;
//  private LocalDate expiryDate;

  public StoredKey(String keyId, Key key, String algorithm, boolean isPrivateKey){
    this.keyId = keyId;
    this.key = key;
    this.algorithm = algorithm;
    this.isPrivateKey = isPrivateKey;
  }

  public String getKeyId() {
    return keyId;
  }

  public void setKeyId(String keyId) {
    this.keyId = keyId;
  }

  public boolean isPrivateKey() {
    return isPrivateKey;
  }

  public void setPrivateKey(boolean privateKey) {
    isPrivateKey = privateKey;
  }

  public Key getKey() {
    return key;
  }

  public void setKey(Key key) {
    this.key = key;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(String algorithm) {
    this.algorithm = algorithm;
  }

  public String getEncodedKey(){
    return getBase64Key(key);
  }

  public static String getBase64Key(Key key){
    return Base64.getEncoder().encodeToString(key.getEncoded());
  }

  public static byte[] base64DecodeKey(String base64KeyString){
    return Base64.getDecoder().decode(base64KeyString);
  }

  public JSObject toJSObject() {
    JSObject jso = new JSObject();
    jso.put("id", keyId);
    jso.put("algorithm", algorithm);
    jso.put("key", getEncodedKey());
    jso.put("private", isPrivateKey);
    jso.put("expiry", expiry);
    return jso;
  }

  public String toString(){
    JSObject jso = toJSObject();

    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent(" ");
    writer.setNewline("\n");

    jso.compose(writer);

    return writer.toString();
  }

  public static StoredKey fromJSObject(JSObject jso) {
    String keyId = (String)jso.get("id");
    String algorithm = (String)jso.get("algorithm");
    Boolean isPrivateKey = (Boolean)jso.get("private");
    String encodedKeyContent = (String)jso.get("key");
    String expiry = (String)jso.get("expiry");
    Key key = getKeyfromBytes(base64DecodeKey(encodedKeyContent), algorithm, isPrivateKey);
    return new StoredKey(keyId, key, algorithm, isPrivateKey);
  }

  public static Key getKeyfromBytes(byte[] key, String algorithm, boolean isPrivateKey){
    try {
    KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
      if (isPrivateKey) {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(key);
        return keyFactory.generatePrivate(keySpec);
      } else {
        try {
          X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
          return keyFactory.generatePublic(keySpec);
        } catch(Exception e){
          logger.error("X509EncodedKeySpec appears to be incompatible - attempting another spec...",e);
//          X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
//          return keyFactory.generatePublic(keySpec);
        }
      }
    } catch(Exception e){
      logger.error("Unable to deserialise key from bytes",e);
    }
    return null;
  };

}
