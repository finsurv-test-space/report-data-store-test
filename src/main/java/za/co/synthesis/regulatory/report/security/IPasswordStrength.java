package za.co.synthesis.regulatory.report.security;

import java.util.List;

public interface IPasswordStrength {
    int getMinimumPasswordLength();

    boolean getPasswordStrength(String password);

    List<String> getRegexValidations();

    int getRegexValidationCount();

    void setRegexValidations(List<String> validations);

    void setRegexValidationCount(int regexValidationCount);

    void setMinimumPasswordLength(int passwordLength);

    String getPasswordStrengthRequirements();

    void setPasswordStrengthRequirements(String newRequirement);
}
