package za.co.synthesis.regulatory.report.rest;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.ReportInfo;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.support.FileChannelCache;
import za.co.synthesis.regulatory.report.support.IChannelCache;
import za.co.synthesis.regulatory.report.support.PreconditionFailedException;
import za.co.synthesis.regulatory.report.support.ResourceNotFoundException;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("producer/api/form")
public class Form extends ControllerBase {
  private static final Logger logger = LoggerFactory.getLogger(Form.class);
  
  
  @Autowired
  private SystemInformation systemInformation;
  
  @Autowired
  private DataLogic dataLogic;

  @Autowired
  ApplicationContext ctx;

  @Autowired
  private IChannelCache channelCache;


  private static String authorisationHeader(Authentication authentication) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!(authentication instanceof AnonymousAuthenticationToken) && (authentication != null)) {
      String currentUserName = authentication.getName();
      Object credentials = authentication.getCredentials();
      if (credentials != null) {
        return "Basic " + Base64.encode((currentUserName + ":" + credentials.toString()).getBytes());
      }
    }
    return "";
  }




//-----------------------------------------------------------------------------------------------
//  Externally Registered Form API
  
  /**
   * This end-point returns a stand-alone bop form with partials, rules and styling as per the channel package stipulated, pre-loaded with the specified report data (as per the trnReference provided)<br/>
   * <br/>
   * <i><b>Note: doSubmit</b>If you wish for the form to display with a submit button populated and enabled such that it submits back to the url the form was loaded from, you need to add an additional parameter onto url - "<b>doSubmit</b>"</i>
   * <br/><code>doSubmit=true</code>
   * <br/>For example:
   * <code>http://localhost:8080/report-data-store/internal/producer/api/form?channelName=coreSARB&trnReference=5535552430&doSubmit=true</code>
   * <br/>
   * <i><b>Note: confirmExit</b>If you wish for the form warn of possibly losing data or changes thereto when exiting or navigating away, a pop-up warning can be enabled by adding an additional parameter to the url - "<b>confirmExit</b>"</i>
   * <br/><code>confirmExit=true</code>
   * <br/>For example:
   * <code>http://localhost:8080/report-data-store/internal/producer/api/form?channelName=coreSARB&trnReference=5535552430&confirmExit=true</code>
   * @param channelName The channel package name containing the relevant validation rule set, form partials and styling artefacts
   * @param trnReference The report reference for report data currently stored in the system, which is to be injected into the html to be displayed
   * @param request
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getExternalForm(@RequestParam(required = true) String channelName,
                       @RequestParam(required = false) String trnReference,
                       @RequestParam(required = false) String proxyPath,
                       @RequestParam(required = false) Boolean packedRules,
                       @RequestParam(required = false) Boolean allowNew,
                       HttpServletRequest request,
                       HttpServletResponse response) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getExternalForm()");
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
  
    String resultHtml = getForm(channelName, trnReference, systemInformation, dataLogic, channelCache, false, request.getParameterMap(), proxyPath, packedRules, allowNew);
    response.getWriter().print(resultHtml);
  }

  public static String getForm(String channelName,
                               String trnReference,
                               SystemInformation channelInformation,
                               DataLogic dataLogic,
                               IChannelCache channelCache,
                               Boolean internal,
                               Map<String, String[]> parameterMap,
                               String proxyPath,
                               Boolean packedRules,
                               Boolean allowNew) throws Exception {
    allowNew = (allowNew == null)?false:allowNew;
    packedRules = (packedRules == null)?false:packedRules;
    Channel channel = channelInformation.getChannel(channelName);
    String reportSpace = channel != null ? channel.getReportSpace() : null;
    if (reportSpace == null) {
      throw new PreconditionFailedException("A valid channelName must be provided.");
    }
    JSObject jsReportData = new JSObject();
    //JSObject jsCredentials = new JSObject();
    //jsCredentials.put("Authorization", authorisationHeader(authentication));
    //jsReportData.put("Credentials", jsCredentials);

    if (trnReference != null) {
      ReportInfo data = dataLogic.getReport(reportSpace, trnReference);
      if(data!=null){
        JSObject jsMeta = JsonSchemaTransform.mapToJson(data.getReportData().getMeta());
        JSObject jsReport = JsonSchemaTransform.mapToJson(data.getReportData().getReport());
        jsReportData.put("Meta", jsMeta);
        jsReportData.put("Report", jsReport);
      } else if(allowNew) {
        jsReportData.put("Meta", "{}");
        jsReportData.put("Report", "{}");
      } else {
        throw new ResourceNotFoundException("Report with trnReference '" + trnReference + "' was not found");
      }
    } else {
      jsReportData.put("Meta", "{}");
      jsReportData.put("Report", "{}");
    }
    JSWriter jsWriter = new JSWriter();
    jsWriter.setQuoteAttributes(true);
    jsReportData.compose(jsWriter);

    String resultHtml = channelCache.getForm(channelName, jsWriter.toString(),  internal, parameterMap, proxyPath, packedRules, allowNew);
    //if trnReference is set, then we need to inject some data etc into this form...
    
    return resultHtml;

  }
  
  
  /**
   * This end point returns the stand-alone document upload form (HTML).<br/>
   * Typical use would be to provide an url to an iFrame (or clickable link) with the necessary parameters to render the form for a specific BOP transaction which requires document uploads.
   * @param channelName The channel package name containing the relevant rule set and associated with the required report space
   * @param trnReference The report reference (for a report currently stored in the system) for which the required documents list should be generated
   * @param request
   * @param response
   * @throws Exception
   */
  @RequestMapping(value = "/document", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
  @ResponseStatus(HttpStatus.OK)
  public
  @ResponseBody
  void getDocumentDownloadFormExternal(@RequestParam String channelName,
                                       @RequestParam(required = true) String trnReference,
                                       @RequestParam(required = false) String proxyPath,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws Exception {
    //SecurityHelper.checkUserAuthority(SecurityHelper.PRODUCER_API, "getDocumentDownloadFormExternal()");
    getDocumentDownloadFormGeneric(channelName, trnReference, request, response, systemInformation, channelCache, proxyPath);
  }


  public static void getDocumentDownloadFormGeneric(String channelName,
                                                    String trnReference,
                                                    HttpServletRequest request,
                                                    HttpServletResponse response,
                                                    SystemInformation systemInformation,
                                                    IChannelCache channelCache,
                                                    String proxyPath) throws Exception {
    response.setHeader("Content-Type","text/html; charset=UTF-8");
    URL url = Resources.getResource("pages/index.html");
    String _proxyPath = (proxyPath != null) ? proxyPath : ((FileChannelCache) channelCache).getUiContextAndProxyPath();
    String htmlResponseString = Resources.toString(url, Charsets.UTF_8)
        .replace("#{trnReference}", URLEncoder.encode(trnReference, Charsets.UTF_8.toString()))
        .replace("#{channel}", channelName)
        .replace("#{reportSpace}", systemInformation.getChannel(channelName).getReportSpace())
        .replace("#{rdsProxyPath}", _proxyPath
        );
    
    //Check for "access_token" param on request url, inject if present...
    Map<String, String[]> requestParams = request.getParameterMap();
    String accessToken = "";
    if (requestParams != null && requestParams.containsKey("access_token")) {
      //get the first non-null token and attach that.
      for (String token : requestParams.get("access_token")){
        accessToken = token;
        if (accessToken != null && !accessToken.isEmpty())
          break;
      }
      if (accessToken != null && !accessToken.isEmpty()) {
        htmlResponseString = htmlResponseString.replaceAll("#\\{access_token_url}", "&access_token=" + accessToken)
            .replaceAll("#\\{access_token_property}", "access_token: '" + accessToken + "',")
            .replaceAll("#\\{access_token_global}", "window.access_token = '" + accessToken + "';");
      }
    }
    //remove any lingering placeholders...
    htmlResponseString = htmlResponseString.replaceAll("#\\{access_token_url}", "")
        .replaceAll("#\\{access_token_property}", "")
        .replaceAll("#\\{access_token_global}", "");
    
    
    htmlResponseString = ((FileChannelCache)channelCache).getUiContextAndProxyPath().endsWith("/")?
        htmlResponseString.replace(((FileChannelCache)channelCache).getUiContextAndProxyPath()+"/",((FileChannelCache)channelCache).getUiContextAndProxyPath()):
        htmlResponseString
    ;
    
    response.getWriter().print(htmlResponseString);
  }
  

}
