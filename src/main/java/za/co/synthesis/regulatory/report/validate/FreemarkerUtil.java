package za.co.synthesis.regulatory.report.validate;

import freemarker.ext.dom.NodeModel;
import org.apache.commons.text.StringEscapeUtils;
import org.xml.sax.InputSource;
import za.co.synthesis.javascript.JSStructureParser;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.net.URLEncoder;
import java.util.Base64;

/**
 *
 * Created by jake on 9/15/17.
 */
public class FreemarkerUtil {
  public static String encodeUrl(String str) {
    try {
      return URLEncoder.encode(str, "UTF-8");
    } catch (UnsupportedEncodingException e) {
    }
    return str;
  }

  public static String decodeUrl(String encoded) {
    try {
      return URLDecoder.decode(encoded, "UTF-8");
    } catch (UnsupportedEncodingException e) {
    }
    return encoded;
  }

  public static String encodeBase64(String str) {
    byte[] encodedBytes = Base64.getEncoder().encode(str.getBytes());
    return new String(encodedBytes);
  }

  public static String escapeJava(String str) {
    return StringEscapeUtils.escapeJava(str);
  }


  public static String escapeSql(String str) {
    //return StringEscapeUtils.escapeSql(str);  //This was deprecated (in commons-lang3 already) and subsequently removed from commons-text.
    return str!=null?escapeJavaScript(str):"";
  }

  public static String escapeSql(Number num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Integer num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Float num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeSql(Long num) {
    return num!=null?escapeSql(""+num):"";
  }

  public static String escapeJavaScript(String str) {
    return StringEscapeUtils.escapeEcmaScript(str);
  }

  public static String escapeCsv(String str) {
    return StringEscapeUtils.escapeCsv(str);
  }

  public static String escapeXml1_0(String str) {
    return StringEscapeUtils.escapeXml10(str);
  }

  public static String escapeXml1_1(String str) {
    return StringEscapeUtils.escapeXml11(str);
  }

  public static String unescapeJava(String str) {
    return StringEscapeUtils.unescapeJava(str);
  }

  public static String unescapeJavaScript(String str) {
    return StringEscapeUtils.unescapeEcmaScript(str);
  }

  public static String unescapeCsv(String str) {
    return StringEscapeUtils.unescapeCsv(str);
  }

  public static String unescapeXml(String str) {
    return StringEscapeUtils.unescapeXml(str);
  }

  public static Object jsonDeserialize(final String str) throws Exception {
    if (str != null) {
      JSStructureParser parser = new JSStructureParser(str);
      return parser.parse();
    }
    return null;
  }

  public static List<Map<String, Object>> sqlDeserialize(final List<Map<String, Object>> sqlResponse) throws Exception {
    return sqlResponse;
  }

  public static Map<String, Object> sqlFirstRowDeserialize(final List<Map<String, Object>> sqlResponse) throws Exception {
    if (sqlResponse != null && sqlResponse.size() > 0)
      return sqlResponse.get(0);
    return null;
  }

  public static NodeModel xmlDeserialize(final String str) throws Exception {
    if (str != null) {
      InputSource inputSource = new InputSource(new StringReader( str ));
      return freemarker.ext.dom.NodeModel.parse(inputSource, true, true);
    }
    return null;
  }

  public static Boolean strGreaterThan(final String str1, final String str2) throws Exception {
    return (str1 != null && str2 != null && str1.compareTo(str2) > 0);
  }
  public static Boolean strGreaterThanEquals(final String str1, final String str2) throws Exception {
    return (str1 != null && str2 != null && str1.compareTo(str2) >= 0);
  }
  public static Boolean strLessThan(final String str1, final String str2) throws Exception {
    return (str1 != null && str2 != null && str1.compareTo(str2) < 0);
  }
  public static Boolean strLessThanEquals(final String str1, final String str2) throws Exception {
    return (str1 != null && str2 != null && str1.compareTo(str2) <= 0);
  }
}
