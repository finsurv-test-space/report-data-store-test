package za.co.synthesis.regulatory.report.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import za.co.synthesis.mapping.configuration.*;
import za.co.synthesis.mapping.source.ISourceData;
import za.co.synthesis.mapping.source.ISourceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 3/22/17.
 */
public class JdbcSourceManager implements ISourceManager {
  private static final Logger logger = LoggerFactory.getLogger(JdbcSourceManager.class);

  private JdbcTemplate jdbcTemplate;
  private PullReference pullReference = null;
  private Map<String, JdbcDataSet> dataSetMap = new HashMap<String, JdbcDataSet>();
  private List<String> pulledReferences = null;
  private int pulledReferencesCursor = 0;
  private JdbcDataSet mainDataSet = null;
  private int readTransactions = 0;
  private int bufferSize = 10;

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public JdbcSourceManager(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public JdbcSourceManager() {

  }

  public int getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(int bufferSize) {
    this.bufferSize = bufferSize;
  }

  public void open(PullReference pullReference) throws Exception {
    readTransactions = 0;
    this.pullReference = pullReference;
  }

  @Override
  public void setup(MappingConfig mappingConfig) {
    dataSetMap.clear();
    pulledReferencesCursor = 0;
    for (Source source : mappingConfig.getSources()) {
      Object description = source.configMap().get("description");
      Object master_src = source.configMap().get("master_src");
      Object select = source.configMap().get("select");
      Object transaction_field = source.configMap().get("transaction_field");
      Object money_field = source.configMap().get("money_field");
      Object master_transaction_field = source.configMap().get("master_transaction_field");
      Object master_money_field = source.configMap().get("master_money_field");
      Object order_by = source.configMap().get("order_by");
      Object unique_key = source.configMap().get("unique_key");
      Object enforce_unique_key = source.configMap().get("enforce_unique_key");

      String sDescription = description != null ? description.toString() : null;
      String sMaster_src = master_src != null ? master_src.toString() : null;
      String sSelect = select != null ? select.toString() : null;
      String sTransaction_field = transaction_field != null ? transaction_field.toString() : null;
      String sMoney_field = money_field != null ? money_field.toString() : null;
      String sMaster_Transaction_field = master_transaction_field != null ? master_transaction_field.toString() : null;
      String sMaster_Money_field = master_money_field != null ? master_money_field.toString() : null;
      String sOrder_by = order_by != null ? order_by.toString() : null;
      String sUnique_key = unique_key != null ? unique_key.toString() : null;
      boolean bEnforceKey = enforce_unique_key != null ? (Boolean)enforce_unique_key : false;

      JdbcDataSet dataSet = new JdbcDataSet(source.getCode(), sMaster_src, sSelect, sTransaction_field, sMoney_field,
              sMaster_Transaction_field, sMaster_Money_field, sOrder_by,
              sUnique_key, bEnforceKey);
      dataSetMap.put(source.getCode(), dataSet);

      if ((sMaster_src == null) && (master_src == null || master_src.toString().length() == 0)) {
        mainDataSet = dataSet;
      }
    }

    for (Section section : mappingConfig.getSections()) {
      for (Field field : section.getFields()) {
        boolean isDate = false;
        if (field.getDataType() != null && (field.getDataType().equals("date") || field.getDataType().equals("datetime"))) {
          isDate = true;
        }
        field.setExternalConfig(new JdbcExternalConfig(field.getSourceColName(), isDate));
      }
    }
  }

  public static boolean numericTest (Object obj){
    boolean isNumeric = obj.getClass().isPrimitive();
//    try{
//      //Long x = Long((long) obj.getClass().isPrimitive());
//      isNumeric = obj.getClass().isPrimitive();
//    } catch (Exception error){}
    return isNumeric;
  }

  private static List<String> composeInClause(List<String> list, boolean isString, int splitSize) {
    List<String> result = new ArrayList<String>();
    int i = 0;
    StringBuilder inClause = new StringBuilder();
    for (String ref : list) {
      if (inClause.length() > 0) {
        inClause.append(",");
      }
      if (isString) {
        inClause.append("'").append(ref).append("'");
      } else {
        inClause.append(ref);
      }
      if (++i >= splitSize) {
        result.add(inClause.toString());
        inClause = new StringBuilder();
        i = 0;
      }
    }
    if (inClause.length() > 0)
      result.add(inClause.toString());

    return result;
  }

  private static void bufferToDataSet(JdbcTemplate jdbcTemplate, JdbcDataSet dataSet, String selectClause) {
    SqlRowSet rs;
    try {
      rs = jdbcTemplate.queryForRowSet(selectClause);
      long startLoadMillis = System.currentTimeMillis();
      dataSet.bufferResults(rs);
      long endLoadMillis = System.currentTimeMillis();
      logger.info("*** Dataset collation and buffering time: {} millseconds ***", endLoadMillis - startLoadMillis);
    } catch (Exception e) {
      logger.error("Query with the following select clause failed: {}", selectClause, e);
    }
  }

  private static void loadDataSet(JdbcTemplate jdbcTemplate, JdbcDataSet dataSet,
                                  PullReference pullReference, List<String> references,
                                  Map<String, JdbcDataSet> dataSetMap) {
    dataSet.clear();

    //do this for selects without "special injection" placeholders

    boolean referencesAreStrings = false;
    boolean insertReferences = false;
    boolean insertMasterReferences = false;
    String replaceStr = null;
    if (dataSet.getSelect().contains("{References}")) {
      referencesAreStrings = false;
      insertReferences = true;
      replaceStr = "\\{References}";
    }
    else
    if (dataSet.getSelect().contains("{'References'}")) {
      referencesAreStrings = true;
      insertReferences = true;
      replaceStr = "\\{'References'}";
    }
    else
    if (dataSet.getSelect().contains("{MasterReferences}")) {
      referencesAreStrings = false;
      insertMasterReferences = true;
      replaceStr = "\\{MasterReferences}";
    }
    else
    if (dataSet.getSelect().contains("{'MasterReferences'}")) {
      referencesAreStrings = true;
      insertMasterReferences = true;
      replaceStr = "\\{'MasterReferences'}";
    }

    if (insertReferences) {
      List<String> inClauseList = composeInClause(references, referencesAreStrings, 1000);

      try {
        for (String inClause : inClauseList) {
          StringBuilder selectClause = new StringBuilder();
          selectClause.append(dataSet.getSelect().
                  replaceFirst(replaceStr, inClause)
          );
          if (!dataSet.getSelect().replaceAll("\\n"," ").contains(" ORDER BY ")) {
            selectClause.append(" ORDER BY ").append(dataSet.getOrder_by());
          }
          bufferToDataSet(jdbcTemplate, dataSet, selectClause.toString());
        }
      } catch (Exception e) {
        logger.error("Cannot pull dataset '" + dataSet.getCode() + "'", e);
      }
    }
    else
    if (insertMasterReferences) {
      JdbcDataSet masterDataSet = dataSetMap.get(dataSet.getMasterSrcCode());
      List<String> masterReferences = new ArrayList<String>();
      if (masterDataSet.isDataBufferLoaded()) {
        for (int i=0; i < masterDataSet.getResultCount(); i++) {
          Map<String, String> row = masterDataSet.getResult(i);
          String ref = row.get(dataSet.getMaster_transaction_field());
          masterReferences.add(ref);
        }
      }
      List<String> inClauseList = composeInClause(masterReferences, referencesAreStrings, 1000);
      try {
        for (String inClause : inClauseList) {
          StringBuilder selectClause = new StringBuilder();
          selectClause.append(dataSet.getSelect().
                  replaceFirst(replaceStr, inClause)
          );
          if (!dataSet.getSelect().replaceAll("\\n"," ").contains(" ORDER BY ")) {
            selectClause.append(" ORDER BY ").append(dataSet.getOrder_by());
          }
          bufferToDataSet(jdbcTemplate, dataSet, selectClause.toString());
        }
      } catch (Exception e) {
        logger.error("Cannot pull dataset '" + dataSet.getCode() + "'", e);
      }
    }
    else {
      List<String> inClauseList = composeInClause(references, referencesAreStrings, 1000);
      for (String inClause : inClauseList) {
        StringBuilder selectClause = new StringBuilder();
        selectClause.append(dataSet.getSelect()).append(" WHERE ").append(dataSet.getTransaction_link());
        selectClause.append(" IN (").append(inClause).append(")");
        if (!dataSet.getSelect().replaceAll("\\n"," ").contains(" ORDER BY ")) {
          selectClause.append(" ORDER BY ").append(dataSet.getOrder_by());
        }
        bufferToDataSet(jdbcTemplate, dataSet, selectClause.toString());
      }
    }
  }

  private void loadEntries() {
    pulledReferencesCursor = 0;
    pulledReferences = new ArrayList<String>();

    for (int i=0; i<bufferSize; i++) {
      String ref = pullReference.getNextReference();
      if (ref == null)
        break;
      if (!pulledReferences.contains(ref)) {
        //pulledReferences.add(ref);

        if (pulledReferences.size() == 0 || (pulledReferences.get(pulledReferences.size()-1).compareTo(ref) < 0) ) {
          pulledReferences.add(ref);
//        } else {
//          logger.info("skipping pulled ref " + ref + ".");
        }
      }
    }

    long startLoadMillis = System.currentTimeMillis();
    if (pulledReferences.size() > 0) {
      // master sets first
      for (JdbcDataSet dataSet : dataSetMap.values()) {
        if (dataSet.getMasterSrcCode() == null) {
          loadDataSet(jdbcTemplate, dataSet, pullReference, pulledReferences, dataSetMap);
          logger.info("read " + dataSet.getResultCount() + " from " + dataSet.getCode());
        }
      }

      // sets with a master second
      for (JdbcDataSet dataSet : dataSetMap.values()) {
        if (dataSet.getMasterSrcCode() != null) {

          // Sort out our records for the child data set
          JdbcDataSet masterDataset = dataSetMap.get(dataSet.getMasterSrcCode());
          ArrayList<String> newPulledReferences = new ArrayList<String>();
          for (int i = 0; i < masterDataset.getResultCount(); i++) {
            String ref = masterDataset.getResult(i).get(dataSet.getMaster_transaction_field());
            if (ref != null) {
              if (!newPulledReferences.contains(ref)) {
                if (newPulledReferences.size() == 0 || (newPulledReferences.get(newPulledReferences.size()-1).compareTo(ref) < 0) ) {
                  newPulledReferences.add(ref);
//                } else {
//                  logger.info("skipping ref " + ref + " from " + dataSet.getCode());
                }
              }
            }
          }

          loadDataSet(jdbcTemplate, dataSet, pullReference, newPulledReferences, dataSetMap);
          logger.info("read " + dataSet.getResultCount() + " from " + dataSet.getCode());
        }
      }
    }
    long endLoadMillis = System.currentTimeMillis();
    logger.info("*** Load initial db dataset time: " + (endLoadMillis - startLoadMillis) + " millseconds ***");
    startLoadMillis = System.currentTimeMillis();

    // We need revise the list of pulled references based on the master record (transaction field)
    if (pulledReferences.size() > 0) {
      for (JdbcDataSet dataSet : dataSetMap.values()) {
        if (dataSet.getMasterSrcCode() == null) {
          pulledReferences.clear();

          for (int i = 0; i < dataSet.getResultCount(); i++) {
            String ref = dataSet.getResult(i).get(dataSet.getTransaction_field());
            if (ref != null) {
              if (!pulledReferences.contains(ref)) {
                pulledReferences.add(ref);
              }
            }
          }
          break;
        }
      }
    }
    readTransactions += pulledReferences.size();
    logger.info("read " + pulledReferences.size() + " unique transactions");
    endLoadMillis = System.currentTimeMillis();
    logger.info("*** Revise initial db dataset time: " + (endLoadMillis - startLoadMillis) + " millseconds ***");
  }

  @Override
  public Object getNextEntry() {
    if (pulledReferences == null ||
            (
                    (
                            pulledReferencesCursor >= pulledReferences.size() ||
                                    pulledReferences.size() == 0
                    ) &&
                            pullReference.isMore()
            )
            ){
      long startLoadMillis = System.currentTimeMillis();
      loadEntries();
      long endLoadMillis = System.currentTimeMillis();
      logger.info("*** Entryset load time: " + (endLoadMillis - startLoadMillis) + " millseconds ***");
    }

    JdbcEntry nextLine = null;
    if (pulledReferencesCursor < pulledReferences.size()) {
      nextLine = new JdbcEntry(pulledReferences.get(pulledReferencesCursor++));
    }
    return nextLine;
  }

  @Override
  public ISourceData getSourceData(Object entry, String source, MappingScope mappingScope) {
    JdbcDataSet dataSet = dataSetMap.get(source);
    JdbcEntry jdbcEntry = (JdbcEntry)entry;

    if (mappingScope == MappingScope.Transaction || mappingScope == MappingScope.Results) {
      return new JdbcSourceData(0, dataSet.getFocusedLevel1Results(jdbcEntry.getTransactionValue()), null);
    }
    if (mappingScope == MappingScope.Money) {
      return new JdbcSourceData(1, dataSet.getFocusedLevel1Results(jdbcEntry.getTransactionValue()), null);
    }
    if (mappingScope == MappingScope.ImportExport) {
      JdbcDataSet masterDataSet = dataSetMap.get(dataSet.getMasterSrcCode());

      return new JdbcSourceData(2, null, dataSet.getFocusedLevel2Results(masterDataSet, jdbcEntry.getTransactionValue()));
    }
    return null;
  }
}
