package za.co.synthesis.regulatory.report.schema;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "alg"
        , "kid"
        , "kty"
        , "n"
        , "use"
})
public class PublicKey {
  private String algorithm;
  private String kid;
  private String keyType;
  private String content;
  private String usage;

  public PublicKey(String algorithm, String kid, String keyType, String content, String usage) {
    this.algorithm = algorithm;
    this.kid = kid;
    this.keyType = keyType;
    this.content = content;
    this.usage = usage;
  }

  @JsonProperty("alg")
  public String getAlgorithm() {
    return algorithm;
  }

  @JsonProperty("kid")
  public String getKid() {
    return kid;
  }

  @JsonProperty("kty")
  public String getKeyType() {
    return keyType;
  }

  @JsonProperty("n")
  public String getContent() {
    return content;
  }

  @JsonProperty("use")
  public String getUsage() {
    return usage;
  }
}
