package za.co.synthesis.regulatory.report.persist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/10/10.
 */
public class RequestParamData {
  private String name;
  private String source;
  private boolean isList = true;
  private String autoListSeparator = ",";
  private RequestParamDuplicateHandlingMethod duplicates = RequestParamDuplicateHandlingMethod.REPLACE;
  private final List<String> values = new ArrayList<>();
  private final Map<String, Object> meta = new HashMap<>();

  public void configure(Map<String,Object> configuration) {
    if (configuration!=null){
      autoListSeparator = null;
      for (Map.Entry<String, Object> entry : configuration.entrySet()) {
        //List/single-item
        if ("List".equalsIgnoreCase(entry.getKey())) {
          try {
            isList = (Boolean) entry.getValue();
          } catch (Exception e) {

          }
        } else
          //autoListSeparator
          if ("ListSeparator".equalsIgnoreCase(entry.getKey())) {
            try {
              autoListSeparator = (String) entry.getValue();
            } catch (Exception e) {

            }
          } else
        //Duplicate handling
        if ("DuplicateHandling".equalsIgnoreCase(entry.getKey())) {
          try {
            duplicates = RequestParamDuplicateHandlingMethod.valueOf((String) entry.getValue());
          } catch (Exception e) {

          }
        } else {
          //We have additional, non-functional properties - add them to meta
          meta.put(entry.getKey(), entry.getValue());
        }

      }
    }
  }

  public enum RequestParamDuplicateHandlingMethod{
    APPEND("Append"),
    FIRST("First"),
    REPLACE("Replace");

    private String text;
    RequestParamDuplicateHandlingMethod(String text){
      this.text = text;
    }

    public String toString(){
      return this.text;
    }
  }

  public RequestParamData(String name) {
    this(name, "REQUEST", true, RequestParamDuplicateHandlingMethod.REPLACE);
  }

  public RequestParamData(String name, String value) {
    this(name, "REQUEST", true, RequestParamDuplicateHandlingMethod.REPLACE);
    setValue(value);
  }

  public RequestParamData(String name, List<String> values) {
    this(name, "REQUEST", true, RequestParamDuplicateHandlingMethod.REPLACE);
  }

  public RequestParamData(String name, String source, String value) {
    this(name, source, true, RequestParamDuplicateHandlingMethod.REPLACE);
    setValue(value);
  }

  public RequestParamData(String name, String source, List<String> values) {
    this(name, source, true, RequestParamDuplicateHandlingMethod.REPLACE);
    setValue(values);
  }

//  public RequestParamData(String name, String source, boolean isList, String duplicates) {
//    this(name, source, isList, RequestParamDuplicateHandlingMethod.valueOf(duplicates));
//  }

  public RequestParamData(String name, String source, boolean isList, RequestParamDuplicateHandlingMethod duplicates) {
    this.name = name;
    this.source = source;
    this.isList = isList;
    this.duplicates = duplicates;
  }

  public RequestParamData(String name, String source, boolean isList, RequestParamDuplicateHandlingMethod duplicates, String
    value) {
    this(name, source, isList, duplicates);
    this.setValue(value);
  }

  public RequestParamData(String name, String source, boolean isList, RequestParamDuplicateHandlingMethod duplicates, List<String> values) {
    this(name, source, isList, duplicates);
    this.setValue(values);
  }


  public void parseAndSetValue(String value){
    if (value != null) {
      if (this.isList) {
        if (this.duplicates.equals(RequestParamDuplicateHandlingMethod.APPEND)) {
          this.values.add(value);
        } else if (this.duplicates.equals(RequestParamDuplicateHandlingMethod.FIRST) &&
                this.values.size() == 0) {
          this.values.add(value);
        } else if (this.duplicates.equals(RequestParamDuplicateHandlingMethod.REPLACE)) {
          if (!this.values.contains(value)) {
            this.values.add(value);
          }
        }
      } else {
        if ((
                this.duplicates.equals(RequestParamDuplicateHandlingMethod.APPEND) ||
                        this.duplicates.equals(RequestParamDuplicateHandlingMethod.FIRST)) && this.values.size() == 0) {
          this.values.add(value);
        } else if (this.duplicates.equals(RequestParamDuplicateHandlingMethod.REPLACE)) {
          this.values.clear();
          this.values.add(value);
        }
      }
    }
  }

  public void clear(){
    this.values.clear();
  }

  public void setValue(String value){
    if (value != null) {
      if (this.isList && autoListSeparator != null && !autoListSeparator.isEmpty()) {
        setValue(value.split(autoListSeparator));
      } else {
        parseAndSetValue(value);
      }
    }
  }

  public void setValue(List<String> values){
    for (String value : values){
      parseAndSetValue(value);
    }
  }

  public void setValue(String[] values){
    for (String value : values){
      parseAndSetValue(value);
    }
  }

  public List<String> getValues(){
    return this.values;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isList() {
    return isList;
  }

  public void setIsList(boolean isList) {
    this.isList = isList;
    if (!isList && values.size()>1){
      String value=null;
      if (duplicates.equals(RequestParamDuplicateHandlingMethod.REPLACE)){
        value = values.get(values.size()-1);
      } else {
        value = values.get(0);
      }
      values.clear();
      if (value!=null) {
        values.add(value);
      }
    }
  }

  public String getAutoListSeparator() {
    return autoListSeparator;
  }

  /**
   * Setting this to a value - such as "," will enable the class to automatically convert comma-separated strings into lists and handle them accordingly.
   * Similarly, any other delimiter may be specified - so long as it conforms to the String.split function regex.
   * @param autoListSeparator
   */
  public void setAutoListSeparator(String autoListSeparator) {
    this.autoListSeparator = autoListSeparator;
  }

  public RequestParamDuplicateHandlingMethod getDuplicates() {
    return duplicates;
  }

  public void setDuplicates(RequestParamDuplicateHandlingMethod duplicates) {
    this.duplicates = duplicates;
  }



  public String csvValues(boolean quotedStrings) {
    String description = "";
    if (isList) {
      for (String value : values) {
        description += (description.length() > 0 ? ", " : "") + (quotedStrings?"\"":"") + value + (quotedStrings?"\"":"");
      }
    } else {
      if (values.size() > 0) {
        description = (quotedStrings?"\"":"") + values.get(0) + (quotedStrings?"\"":"");
//      } else {
//        description = "";
      }
    }
    return description;
  }

  public String toString(){
    String description = csvValues(true);
    description = this.name+"="+(isList?"[":"")+description+(isList?"]":"");
    return description;
  }


  public String getSqlComparatorPreparedQuery(String fieldName){
    String sql = "";
    if (values.size() > 0) {
      if (isList) {
        for (String value : values) {
          sql += (sql.length() > 0 ? ", " : "") + "?";
        }
        sql = (fieldName==null?"":fieldName+" ")+"IN (" + sql + ") ";
      } else {
        sql = (fieldName==null?"":fieldName+" ")+"= ? ";
      }
    }
    return sql;
  }

  public String getSqlComparatorPreparedQuery(){
    return getSqlComparatorPreparedQuery(null);
  }

  public List<String> getSqlComparatorPreparedParams(){
    if (values.size() > 0) {
      if (isList) {
        return values;
      } else {
        if (values.size() == 1) {
          return values;
        } else {
          List<String> result = new ArrayList<String>();
          if (duplicates.equals(RequestParamDuplicateHandlingMethod.REPLACE)){
            result.add(values.get(values.size()-1));
          } else {
            result.add(values.get(0));
          }
          return result;
        }
      }
    }
    return values;
  }

  public Map<String, Object> getMeta(){
    return this.meta;
  }
}
