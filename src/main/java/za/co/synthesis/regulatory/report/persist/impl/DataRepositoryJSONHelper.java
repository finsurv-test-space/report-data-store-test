package za.co.synthesis.regulatory.report.persist.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.persist.*;
import za.co.synthesis.rule.core.StatusType;

import java.util.*;

/**
 * Created by jake on 7/27/17.
 */
public class DataRepositoryJSONHelper {
  private static final Logger logger = LoggerFactory.getLogger(DataRepositoryJSONHelper.class);
  private static String paramReplace(final Properties properties, String str) {
    if (str != null && str.contains("$")) {
      for (String propName : properties.stringPropertyNames()) {
        String find = "${" + propName + "}";
        str = str.replace(find, properties.getProperty(propName));
      }
    }
    return str;
  }

  private static String paramReplace(final Properties properties, Object value) {
    if (value instanceof String) {
      return paramReplace(properties, (String)value);
    }
    return null;
  }

  public static DataRepositoryDefinition parseDataRepository(Properties properties, String repoName, Map repoJS) {
    DataRepositoryDefinition repoDefinition = null;
    repoDefinition = new DataRepositoryDefinition(repoName);
    if (repoJS.containsKey("datasource")) {
      Map dsMap = (Map)repoJS.get("datasource");
      Object objDriverClassName = dsMap.get("jdbc.driverClassName");
      Object objUrl = dsMap.get("jdbc.url");
      Object objUsername = dsMap.get("jdbc.username");
      Object objPassword = dsMap.get("jdbc.password");

      repoDefinition.setDataSource(new DataSourceData(
              paramReplace(properties, objDriverClassName),
              paramReplace(properties, objUrl),
              paramReplace(properties, objUsername),
              paramReplace(properties, objPassword)));
    }
    if (repoJS.containsKey("lookups")) {
      for (Map.Entry<String, Object> lookup : ((JSObject)repoJS.get("lookups")).entrySet()) {
        JSObject jsLookup = (JSObject) lookup.getValue();
        String sql = paramReplace(properties, getSqlStringFromJSDef(jsLookup));
        String compose = getComposeStringFromJSDef(jsLookup);
        if (sql != null) {
          LookupData luDef = new LookupData(repoDefinition.getDataSource(), lookup.getKey(), sql, compose);
          repoDefinition.addLookupDefinition(lookup.getKey(), luDef);
        }
        else {
          RestData rest = getRestFromJSDef(properties, jsLookup);
          if (rest != null) {
            LookupData luDef = new LookupData(lookup.getKey(), rest, compose);
            repoDefinition.addLookupDefinition(lookup.getKey(), luDef);
          }
        }
      }
    }
    if (repoJS.containsKey("validations")) {
      for (Map.Entry<String, Object> validation : ((JSObject)repoJS.get("validations")).entrySet()) {
        JSObject jsValidation = (JSObject) validation.getValue();
        String endpoint = paramReplace(properties, jsValidation.get("endpoint"));
        List<ParameterData> parameters = getParametersFromJSDef(jsValidation);
        //if this is a SQL validation...
        String sql = getSqlStringFromJSDef(jsValidation);
        String compose = getComposeStringFromJSDef(jsValidation);
        int timeout = 10;
        String failOnError = null;
        String failOnBusy = null;
        if(jsValidation.containsKey("failOnError")){
          try{
            if(jsValidation.get("failOnError") instanceof String){
              failOnError = (String)jsValidation.get("failOnError");
            }
          }catch (Exception err){

          }
        }
        if(jsValidation.containsKey("failOnBusy")){
          try{
            if(jsValidation.get("failOnBusy") instanceof String){
              failOnBusy = (String)jsValidation.get("failOnBusy");
            }
          }catch (Exception err){

          }
        }
        if (jsValidation.containsKey("timeout")){
          try{
            if (jsValidation.get("timeout") instanceof String){
              timeout = Integer.parseInt((String)jsValidation.get("timeout"));
            } else if (jsValidation.get("timeout") instanceof Number){
              timeout = Integer.parseInt(""+((Number)jsValidation.get("timeout")));
            }
          } catch (Exception err){
            //meh
          }
        }

        ValidationData vDef = null;
        if (sql != null) {
          vDef = new ValidationData(repoDefinition.getDataSource(), validation.getKey(), endpoint, sql, compose, timeout,failOnBusy, failOnError);
          vDef.getParameters().addAll(parameters);
        }
        //if this is a REST validation...
        RestData rest = getRestFromJSDef(properties, jsValidation);
        if (rest != null) {
          vDef = new ValidationData(validation.getKey(), endpoint, rest, compose, timeout,failOnBusy, failOnError);
          vDef.getParameters().addAll(parameters);
        }
        if (vDef != null){
          repoDefinition.addValidationDefinition(validation.getKey(), vDef);
        }
      }
    }
    return repoDefinition;
  }

  public static Map<String, ProxyCallData> parseProxyCallData(Properties properties, Map repoJS) {
    Map<String, ProxyCallData> result = new HashMap<String, ProxyCallData>();

    for (Object key : repoJS.keySet()) {
      JSObject jsCall = (JSObject) repoJS.get(key);
      List<ParameterData> parameters = getParametersFromJSDef(jsCall);
      //if this is a SQL validation...
      String compose = getComposeStringFromJSDef(jsCall);
      ProxyCallData vDef = null;
      //if this is a REST validation...
      RestData rest = getRestFromJSDef(properties, jsCall);
      if (rest != null) {
        vDef = new ProxyCallData(key.toString(), parameters, rest, compose);
        logger.info("Loaded proxy call details: {}, {} {}", key.toString(), rest.getVerb(), rest.getUrl());
      }
      if (vDef != null){
        result.put(key.toString(), vDef);
      }
    }
    return result;
  }

  private static String getSqlStringFromJSDef(JSObject jsDef){
    String sql = null;
    if (jsDef != null &&
            jsDef.containsKey("sql")){
      Object sqlDef = jsDef.get("sql");
      sql = getString(sqlDef);
    }
    return sql;
  }

  private static String getComposeStringFromJSDef(JSObject jsDef){
    String compose = null;
    if (jsDef != null &&
            jsDef.containsKey("compose")){
      Object composeDef = jsDef.get("compose");
      compose = getString(composeDef);
    }
    return compose;
  }

  private static String getString(Object jsDef) {
    String result = null;
    if (jsDef instanceof List){ //assume *String* array to be re-assembled into multi-line string
      for (Object line : (List) jsDef){
        result = (result != null ? result + "\n" : "") + line.toString();
      }
    } else if (jsDef instanceof String){ //some form of string, already assembled - just return.
      result = (String) jsDef;
    }
    return result;
  }

  private static ParameterData getParameterFromJSParam(Map jsParam){
    if (jsParam != null){
      Object paramName = jsParam.get("name");
      Object paramPath = jsParam.get("path");
      if ((paramName instanceof String && ((String)paramName).length()>0) &&
              (paramPath instanceof String && ((String)paramPath).length()>0)){
        return new ParameterData((String)paramName, (String)paramPath);
      }
    }
    return null;
  }

  private static List<ParameterData> getParametersFromJSDef(JSObject jsDef){
    List<ParameterData> parameters = null;
    if (jsDef != null &&
            jsDef.containsKey("parameters")){
      Object paramSet = jsDef.get("parameters");
      if (paramSet instanceof List){ //assume *String* array to be re-assembled into multi-line string
        for (Object paramMap : (List)paramSet){
          if (paramMap instanceof Map){
            ParameterData param = getParameterFromJSParam((Map) paramMap);
            if (param != null) {
              if (parameters==null){
                parameters = new ArrayList<>();
              }
              parameters.add(param);
            }
          }
        }
      }
    }
    return parameters;
  }

  public static List<ValidationData.Result> getResultFromJSDef(JSObject jsDef){
    List<ValidationData.Result> results = null;
    if (jsDef!=null && jsDef.containsKey("result")){
      Object resultObj = jsDef.get("result");
      if (resultObj instanceof List) {
        for (JSObject result : (List<JSObject>)resultObj){
          Object match = result.get("match");
          Object status = result.get("status");
          Object code = result.get("code");
          Object message = result.get("message");
          if (status!=null){
            if (results == null){
              results = new ArrayList<>();
            }
            StatusType statusType = (status.toString().equalsIgnoreCase("pass")?StatusType.Pass:(status.toString().equalsIgnoreCase("fail")?StatusType.Fail:StatusType.Error));
            ValidationData.Result res = new ValidationData.Result((String)match, statusType, code==null?null:code.toString(), message==null?null:message.toString());
            results.add(res);
          }

        }
      }
    }
    return results;
  }

  private static HeaderData getHeaderFromJSHeader(Map jsHeader){
    if (jsHeader != null){
      Object headerName = jsHeader.get("name");
      Object headerValue = jsHeader.get("value");
      if ((headerName instanceof String && ((String)headerName).length()>0) &&
              (headerValue instanceof String && ((String)headerValue).length()>0)){
        return new HeaderData((String)headerName, (String)headerValue);
      }
    }
    return null;
  }

  private static List<HeaderData> getHeadersFromJSDef(Map jsDef) {
    List<HeaderData> headers = null;
    if (jsDef != null &&
            jsDef.containsKey("headers")){
      Object headerSet = jsDef.get("headers");
      if (headerSet instanceof List){ //assume *String* array to be re-assembled into multi-line string
        for (Object headerMap : (List)headerSet){
          if (headerMap instanceof Map){
            HeaderData header = getHeaderFromJSHeader((Map) headerMap);
            if (header != null) {
              if (headers==null){
                headers = new ArrayList<>();
              }
              headers.add(header);
            }
          }
        }
      }
    }
    return headers;
  }

  private static RestData getRestFromJSDef(Properties properties, JSObject jsDef){
    RestData result = null;
    if (jsDef!=null && jsDef.containsKey("rest")){
      Object resultObj = jsDef.get("rest");
      if (resultObj instanceof Map) {
        Map restMap = (Map)resultObj;
        Object verb = restMap.get("verb");
        String url = paramReplace(properties, restMap.get("url"));
        Object body = restMap.get("body");
        List<HeaderData> headers = getHeadersFromJSDef(restMap);
        if (verb!=null && verb instanceof String && url!=null) {
          result = new RestData();
          result.setVerb((String)verb);
          result.setUrl(url);
          if (body!=null) {
            result.setBody(getString(body));
          }
          if (headers != null) {
            result.setHeaders(headers);
          }
          List<Object> allowedHttpCodes = null;
          try {
            allowedHttpCodes = (List<Object>) restMap.get("allowedHttpCodes");
            result.setAcceptHttpCodes(allowedHttpCodes);
          }catch (Exception err){
            //ignore.
          }
        }
      }
    }
    return result;
  }
}
