package za.co.synthesis.regulatory.report.persist;

/**
 * Created by james on 2017/07/04.
 */
public class LookupData {
  private final DataSourceData dataSource;
  private final String name;
  private final String sql;
  private final RestData rest;
  private final String compose;

  public LookupData(DataSourceData dataSource, String name, String sql, String compose) {
    this.dataSource = dataSource;
    this.name = name;
    this.sql = sql;
    this.rest = null;
    this.compose = compose;
  }

  public LookupData(String name, RestData rest, String compose) {
    this.dataSource = null;
    this.name = name;
    this.sql = null;
    this.rest = rest;
    this.compose = compose;
  }

  public LookupData(LookupData obj) {
    this.dataSource = obj.dataSource;
    this.name = obj.name;
    this.sql = obj.sql;
    this.rest = obj.rest;
    this.compose = obj.compose;
  }

  public DataSourceData getDataSource() {
    return dataSource;
  }

  public String getName() {
    return name;
  }

  public String getSql() {
    return sql;
  }

  public RestData getRest() {
    return rest;
  }

  public String getCompose() {
    return compose;
  }
}
