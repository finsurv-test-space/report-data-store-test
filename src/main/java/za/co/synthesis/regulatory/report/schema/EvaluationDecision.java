package za.co.synthesis.regulatory.report.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.MappedSuperclass;

/**
 * Created by jake on 5/25/17.
 */
@JsonPropertyOrder({
        "Parameters"
        , "Evaluation"
})
@MappedSuperclass
public class EvaluationDecision {
  private EvaluationRequest parameters;
  private EvaluationResponse evaluation;

  public EvaluationDecision() {
  }

  public EvaluationDecision(EvaluationRequest parameters, EvaluationResponse evaluation) {
    this.parameters = parameters;
    this.evaluation = evaluation;
  }

  public void setParameters(EvaluationRequest parameters) {
    this.parameters = parameters;
  }

  public void setEvaluation(EvaluationResponse evaluation) {
    this.evaluation = evaluation;
  }

  @JsonProperty("Parameters")
  public EvaluationRequest getParameters() {
    return parameters;
  }

  @JsonProperty("Evaluation")
  public EvaluationResponse getEvaluation() {
    return evaluation;
  }
}
