package za.co.synthesis.regulatory.report.persist;

import org.springframework.web.bind.annotation.RequestMethod;
import za.co.synthesis.rule.core.StatusType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 2017/07/04.
 */
public class ValidationData {
  public static class Result {
    private String match;
    private StatusType statusType;
    private String code;
    private String message;

    public Result(String match, StatusType statusType, String code, String message) {
      this.match = match;
      this.statusType = statusType;
      this.code = code;
      this.message = message;
    }

    public String getMatch() {
      return match;
    }

    public StatusType getStatusType() {
      return statusType;
    }

    public String getCode() {
      return code;
    }

    public String getMessage() {
      return message;
    }
  }

  private final DataSourceData dataSource;
  private final String name;
  private final String endpoint;
  private final int timeout;
  private final List<ParameterData> parameters;
  private final String sql;
  private final RestData rest;
  private final String compose;
  private final String failOnBusy;
  private final String failOnError;

  public ValidationData(ValidationData obj) {
    this.dataSource = obj.dataSource;
    this.name = obj.name;
    this.endpoint = obj.endpoint;
    this.sql = obj.sql;
    this.rest = obj.rest;
    this.parameters = obj.parameters;
    this.compose = obj.compose;
    this.timeout = obj.timeout;
    this.failOnBusy = obj.failOnBusy;
    this.failOnError = obj.failOnError;
  }

  public ValidationData(DataSourceData dataSource, String name, String endpoint, String sql, String compose, int timeout, String failOnBusy, String failOnError) {
    this.dataSource = dataSource;
    this.name = name;
    this.endpoint = endpoint;
    this.sql = sql;
    this.rest = null;
    this.parameters = new ArrayList<ParameterData>();
    this.compose = compose;
    this.timeout = timeout;
    this.failOnError = failOnError;
    this.failOnBusy = failOnBusy;
  }

  public ValidationData(DataSourceData dataSource, String name, String endpoint, String sql, String compose, int timeout) {
    this.dataSource = dataSource;
    this.name = name;
    this.endpoint = endpoint;
    this.sql = sql;
    this.rest = null;
    this.parameters = new ArrayList<ParameterData>();
    this.compose = compose;
    this.timeout = timeout;
    this.failOnError = null;
    this.failOnBusy = null;
  }
  public ValidationData(String name, String endpoint, RestData rest, String compose, int timeout,String failOnBusy, String failOnError) {
    this.dataSource = null;
    this.name = name;
    this.endpoint = endpoint;
    this.sql = null;
    this.rest = rest;
    this.parameters = new ArrayList<ParameterData>();
    this.compose = compose;
    this.timeout = timeout;
    this.failOnError = failOnBusy;
    this.failOnBusy = failOnError;
  }
  public ValidationData(String name, String endpoint, RestData rest, String compose, int timeout) {
    this.dataSource = null;
    this.name = name;
    this.endpoint = endpoint;
    this.sql = null;
    this.rest = rest;
    this.parameters = new ArrayList<ParameterData>();
    this.compose = compose;
    this.timeout = timeout;
    this.failOnError = null;
    this.failOnBusy = null;
  }

  public DataSourceData getDataSource() {
    return dataSource;
  }

  public String getName() {
    return name;
  }

  public String getEndpoint() {
    return endpoint;
  }

  public List<ParameterData> getParameters() {
    return parameters;
  }

  public String getSql() {
    return sql;
  }

  public RestData getRest() {
    return rest;
  }

  public String getCompose() {
    return compose;
  }

  public String getFailOnBusy() { return failOnBusy; }

  public String getFailOnError() { return failOnError; }

  public int getTimeout() {
    return timeout;
  }
}
