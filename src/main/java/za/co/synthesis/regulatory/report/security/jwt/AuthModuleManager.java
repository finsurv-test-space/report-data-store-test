package za.co.synthesis.regulatory.report.security.jwt;

import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class AuthModuleManager {
  private List<IJWTAuthModule> authModules;

  @Required
  public void setAuthModules(List<IJWTAuthModule> authModules) {
    this.authModules = authModules;
  }

  public List<IJWTAuthModule> getAuthModules() {
    return authModules;
  }

  public IJWTAuthModule getAuthModuleByName(String name) {
    for (IJWTAuthModule authModule : authModules) {
      if (authModule.getName().equals(name)) {
        return authModule;
      }
    }
    return null;
  }
}
