package za.co.synthesis.regulatory.report.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import za.co.synthesis.regulatory.report.businesslogic.DataLogic;
import za.co.synthesis.regulatory.report.businesslogic.SystemInformation;
import za.co.synthesis.regulatory.report.persist.FileData;
import za.co.synthesis.regulatory.report.schema.Channel;
import za.co.synthesis.regulatory.report.schema.DocumentInfo;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.security.SecurityHelper;
import za.co.synthesis.regulatory.report.support.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("internal/producer/api")
public class InternalProducerDocumentApi extends ControllerBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalProducerDocumentApi.class);

    @Autowired
    private SystemInformation systemInformation;

    @Autowired
    private DataLogic dataLogic;

    @Autowired
    ApplicationContext ctx;

  @Autowired
  ReportServiceExecutorService reportServiceExecutorService;


//-----------------------------------------------------------------------------------------------
//  Document APIs
    /**
     * expose request for "producer/api/document/report"  [GET]
     * Exposed as "internal/producer/api/document/report"  [GET]
     */
    @RequestMapping(value = "/document/report", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<DocumentInfo> getInternalDocumentInfoListForReportRef(
            @RequestParam String channelName,
            @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDocumentInfoListForReportRef()");
        return dataLogic.getDocumentInfoForReportOrTrnReference(channelName,
                trnReference,
                null);
    }



    /**
     * expose request for "producer/api/document/report"  [POST]
     * Exposed as "internal/producer/api/document/report"  [POST]
     */
    @RequestMapping(value = "/document/report", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    List<DocumentInfo> getInternalDocumentInfoListForReport(
            @RequestParam String channelName,
            @RequestBody ReportData report) throws ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDocumentInfoListForReport()");
        return dataLogic.getDocumentInfoForReportOrTrnReference(channelName,
                null,
                report);
    }



    /**
     * expose request for "producer/api/report/documents"  [GET]
     * Exposed as "internal/producer/api/report/documents"  [GET]
     */
    @RequestMapping(value = "/report/documents", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocumentInfo> getInternalReportDocuments(@RequestParam String channelName,
                                          @RequestParam String trnReference) throws ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalReportDocuments()");
      Channel channel = null;
      if (channelName != null) {
        channel = systemInformation.getChannel(channelName);
      }
      String reportSpace = (channel != null ? channel.getReportSpace() : null);
      if (reportSpace == null)
        throw new PreconditionFailedException("A valid channelName must be specified");

      List<DocumentInfo> documentList = dataLogic.getDocumentsForReport(channel, trnReference);
      if (documentList == null)
          throw new ResourceNotFoundException("No Report Documents for '" + trnReference + "@" + reportSpace + "' found");

      return documentList;
    }



    /**
     * expose request for "producer/api/document/acknowledge"  [POST]
     * Exposed as "internal/producer/api/document/acknowledge"  [POST]
     */
    @RequestMapping(value = "/document/acknowledge", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    DocumentInfo setInternalDocumentAcknowledgement(
            @RequestParam(required=false) String channelName,
            @RequestParam(required=false) String reportSpace,
            @RequestParam String documentHandle,
            @RequestParam boolean acknowledgement,
            @RequestParam (required = false)String acknowledgementComment) throws IOException, ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setInternalDocumentAcknowledgement()");
      Channel channel = null;
      if (channelName != null) {
        channel = systemInformation.getChannel(channelName);
      }
      reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
      if (channelName != null && channel == null) {
        throw new PreconditionFailedException("A valid channelName must be specified");
      }
      if (!systemInformation.isValidReportSpace(reportSpace)) {
        throw new PreconditionFailedException("A valid reportSpace must be specified");
      }
      return Document.setDocumentAcknowledgement(reportSpace, documentHandle, acknowledgement, acknowledgementComment,
              dataLogic, systemInformation);
    }




    /**
     * expose request for "producer/api/document/acknowledge"  [GET]
     * Exposed as "internal/producer/api/document/acknowledge"  [GET]
     */
    @RequestMapping(value = "/document/acknowledge", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    DocumentInfo getInternalDocumentAcknowledgement(
            @RequestParam(required=false) String channelName,
            @RequestParam(required=false) String reportSpace,
            @RequestParam String documentHandle) throws IOException, ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDocumentAcknowledgement()");
      Channel channel = null;
      if (channelName != null) {
        channel = systemInformation.getChannel(channelName);
      }
      reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
      if (channelName != null && channel == null) {
        throw new PreconditionFailedException("A valid channelName must be specified");
      }
      if (!systemInformation.isValidReportSpace(reportSpace)) {
        throw new PreconditionFailedException("A valid reportSpace must be specified");
      }

      DocumentInfo doc = dataLogic.getDocument(reportSpace, documentHandle);
      if (doc != null)
        return doc;
      throw new ResourceNotFoundException("Unable to locate the document specified.");
    }





    /**
     * expose request for "producer/api/document"  [POST]
     * Exposed as "internal/producer/api/document"  [POST]
     */
    @RequestMapping(value = "/document", method = RequestMethod.POST, produces = "application/json", headers = "content-type=multipart/form-data")
    public
    @ResponseBody
    DocumentInfo setInternalDocumentMultiPart(
            @RequestParam(required=false) String channelName,
            @RequestParam(required=false) String reportSpace,
            @RequestParam String documentHandle,
            @RequestParam MultipartFile file) throws IOException, ResourceAccessAuthorizationException {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "setInternalDocumentMultiPart()");
      Channel channel = null;
      if (channelName != null) {
        channel = systemInformation.getChannel(channelName);
      }
      reportSpace = (channel != null ? channel.getReportSpace() : reportSpace);
      if (channelName != null && channel == null) {
        throw new PreconditionFailedException("A valid channelName must be specified");
      }
      if (!systemInformation.isValidReportSpace(reportSpace)) {
        throw new PreconditionFailedException("A valid reportSpace must be specified");
      }
  
      if (FileUploadUtil.isMimeTypeWhiteListed(file)) {
        String fileName = file.getOriginalFilename();
        if (!FileUploadUtil.isNameSafe(file.getOriginalFilename())) {
          try {
            fileName = FileUploadUtil.makeNameSafe(file.getOriginalFilename());
          } catch (Exception e) {
            throw new PreconditionFailedException(e.getMessage());
          }
        }
        FileData data = new FileData(file.getContentType(), fileName, file.getBytes());
        return dataLogic.setDocumentContent(reportSpace, documentHandle, data);
      }
      throw new PreconditionFailedException("Invalid file type (or derived mime type) provided");
    }


//  @RequestMapping(value = "/document", method = RequestMethod.POST, produces = "application/json", headers = "content-type!=multipart/form-data")
//  public
//  @ResponseBody
//  DocumentInfo setInternalDocumentChunked(
//          @RequestParam String reportSpace,
//          @RequestParam String documentHandle,
//          final HttpServletRequest request,
//          final HttpServletResponse response) throws IOException {
//    if (reportSpace == null)
//      throw new PreconditionFailedException("A valid reportSpace must be specified");
//
//    request.getHeader("content-range");//Content-Range:bytes 737280-819199/845769
//    request.getHeader("content-length"); //845769
//    request.getHeader("content-disposition"); // Content-Disposition:attachment; filename="Screenshot%20from%202012-12-19%2017:28:01.png"
//    request.getInputStream(); //actual content.
//
//
//    byte[] bytes = new byte[0];
//    request.getInputStream().read(bytes);
//
//    FileData data = new FileData("", "", bytes);
//    DocumentInfo info = dataLogic.setDocumentContent(reportSpace, documentHandle, data);
////    DocumentInfo info = systemInformation.getDocumentInfo(documentHandle);
//    return dataLogic.setDocument(info, data);
//  }



    /**
     * expose request for "producer/api/document"  [GET]
     * Exposed as "internal/producer/api/document"  [GET]
     */
    @RequestMapping(value = "/document", method = RequestMethod.GET)
    public
    @ResponseBody
    void getInternalDocument(
            @RequestParam String channelName,
            @RequestParam String documentHandle,
            HttpServletResponse response) throws Exception {
      SecurityHelper.checkUserAuthority(SecurityHelper.INTERNAL_API, "getInternalDocument()");
        Document.getDocumentContent(systemInformation, dataLogic, channelName, documentHandle, response);
    }

}
