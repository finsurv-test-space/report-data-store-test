package za.co.synthesis.regulatory.report.dao;

import za.co.synthesis.javascript.JSArray;

import java.util.List;
import java.util.Map;

/**
 * Provide an interface for all DB connectivity so that multiple
 */
public interface IExternalLookup {
  JSArray getFullLookupList(String listName);
}
