var express = require('express');
var proxy = require('proxy-middleware');
var url = require('url');

var proxyUrl = 'http://localhost:8080/report-data-store';
var proxyRoute = "/report-data-store";


var app = express();
var port = 3004;

var proxyOptions = url.parse(proxyUrl);
proxyOptions.auth = "superuser:abc"

app.use(proxyRoute, proxy(proxyOptions));

app.get('/channel', function (req, res) {
  res.sendFile(__dirname+ '/channel.html');
});

app.get('/upload', function (req, res) {
  res.sendFile(__dirname+ '/upload.html');
});



app.listen(port);

exports = module.exports = app;
