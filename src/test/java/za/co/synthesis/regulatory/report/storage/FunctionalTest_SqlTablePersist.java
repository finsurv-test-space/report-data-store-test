package za.co.synthesis.regulatory.report.storage;

import org.springframework.jdbc.core.JdbcTemplate;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.persist.CriterionData;
import za.co.synthesis.regulatory.report.persist.PersistException;
import za.co.synthesis.regulatory.report.persist.ITablePersist;
import za.co.synthesis.regulatory.report.persist.common.TableDefinition;
import za.co.synthesis.regulatory.report.schema.SortOrderField;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 8/21/17.
 */
public class FunctionalTest_SqlTablePersist implements ITablePersist<TEST_HATable> {
  private TableDefinition tableDefinition;
  private JdbcTemplate jdbcTemplate;

  public FunctionalTest_SqlTablePersist(TableDefinition tableDefinition, JdbcTemplate jdbcTemplate) {
    this.tableDefinition = tableDefinition;
    this.jdbcTemplate = jdbcTemplate;
  }

  private String mapToJSON(Map<String, Object> map) {
    JSObject jsObject = JsonSchemaTransform.mapToJson(map);
    JSWriter writer = new JSWriter();
    writer.setQuoteAttributes(true);
    writer.setIndent("  ");
    writer.setNewline("\n");
    jsObject.compose(writer);
    return writer.toString();
  }

  private Map<String, Object> jsonToMap(String str) throws Exception {
    JSStructureParser parser = new JSStructureParser(str);
    return (Map<String, Object>)parser.parse();
  }

  @Override
  public void write(TEST_HATable obj) throws PersistException {
    String sql = "IF NOT EXISTS (SELECT * FROM " + tableDefinition.getTablename() + " WHERE [UUID] = ?)" +
            "BEGIN " +
            "  INSERT " + tableDefinition.getTablename() + "([UUID], [Reference], [When], [Content])" +
            "  VALUES(?, ?, ?, ?)" +
            "END";

    jdbcTemplate.update(sql, obj.getUuid(), obj.getUuid(), obj.getReference(), obj.getWhen(), mapToJSON(obj.getContent()));
  }

  @Override
  public void write(List<TEST_HATable> objects) throws PersistException {

  }

  @Override
  public PersistenceObjectList<TEST_HATable> readFromSyncpoint(String syncpoint, int maxRecords) {
    String sql = "SELECT TOP " + maxRecords + " [UUID], master.sys.fn_varbintohexstr(CONVERT(VARBINARY(8), [TStamp])) as TStamp, [Reference], [When], [Content] FROM TEST_HATable";
    if (syncpoint != null) {
      sql += " WHERE [TStamp] > CONVERT(VARBINARY(8), ?, 1)";
    }
    sql += " ORDER BY [TStamp]";

    List<Map<String, Object>> list;
    if (syncpoint != null)
      list = jdbcTemplate.queryForList(sql, syncpoint);
    else
      list = jdbcTemplate.queryForList(sql);

    String newSyncpoint = syncpoint;
    List<TEST_HATable> objList = new ArrayList<>();
    for (Map<String, Object> row : list) {
      TEST_HATable obj = new TEST_HATable((String)row.get("UUID"));
      obj.setWhen((Timestamp) row.get("When"));
      obj.setReference((String)row.get("Reference"));
      try {
        obj.setContent(jsonToMap((String)row.get("Content")));
      } catch (Exception e) {
        e.printStackTrace();
      }
      newSyncpoint = (String)row.get("TStamp");
      objList.add(obj);
    }

    return new PersistenceObjectList<TEST_HATable>(newSyncpoint != null ? newSyncpoint : null, objList);
  }

  @Override
  public PersistenceObjectList<TEST_HATable> readFromSyncpoint(String syncpoint, String reportSpace, int maxRecords) {
    return null;
  }

  @Override
  public List<TEST_HATable> queryByMapList(Map<String, List<String>> queryMapList, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    return null;
  }

  @Override
  public List<TEST_HATable> searchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    return null;
  }

  @Override
  public Class getPersistClass() {
    return TEST_HATable.class;
  }

  @Override
  public List<TEST_HATable> readCurrent() {
    return null;
  }

  @Override
  public List<TEST_HATable> readBySelector(int maxRecords) {
    return new ArrayList<>();
  }

  @Override
  public TableDefinition getTableDefinition() {
    return null;
  }

  @Override
  public List<TEST_HATable> fullTextSearchRecords(List<CriterionData> criteria, int maxResults, int page, boolean currentItemsOnly, List<SortOrderField> sortBy) {
    return null;
  }

  @Override
  public boolean fullTextSearchSupported() {
    return false;
  }

  @Override
  public void setFullTextSearchSupported(boolean fullTextSearchSupported) {
  }
  
  @Override
  public int updateCriteria(List<CriterionData> criteria, List<CriterionData> newCriteria) {
    return 0;
  }
}
