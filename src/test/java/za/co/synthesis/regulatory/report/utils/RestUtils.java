package za.co.synthesis.regulatory.report.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;

import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/07/28.
 */
public class RestUtils {

    public interface ServerlessTests { /* category marker */ }
    public interface FastTests { /* category marker */ }
    public interface SlowTests { /* category marker */ }



    public static String getJsonStringFromResource(ApplicationContext ctx, String src_resource_path, boolean isJSObject) throws Exception{
        Object jsReportData =  isJSObject?
                JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path):
                JSReaderUtil.loadResourceAsJSArray(ctx, src_resource_path);
        JSWriter jsWriter = new JSWriter();
        jsWriter.setQuoteAttributes(true);
        if (isJSObject){
            ((JSObject)jsReportData).compose(jsWriter);
        } else {
            ((JSArray)jsReportData).compose(jsWriter);
        }
        return jsWriter.toString();
    }

    public static HttpHeaders getHeadersForJsonWithBasicAuth(String basicAuthCredentials){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        if (basicAuthCredentials!=null) {
            String base64Creds = Base64.getMimeEncoder().encodeToString(basicAuthCredentials.getBytes());
            headers.add("Authorization", "Basic " + base64Creds);
        }
        return headers;
    }

    public static HttpEntity<String> getHttpEntityRequest(ApplicationContext ctx, Map<String, List<String>> customHeaders) throws Exception{
        return getHttpEntityRequest(ctx, null, null, null, false, customHeaders);
    }

    public static HttpEntity<String> getHttpEntityRequest(ApplicationContext ctx, String credentials, String cookies) throws Exception{
        return getHttpEntityRequest(ctx, null, credentials, cookies, false, null);
    }

    public static HttpEntity<String> getHttpEntityRequest(ApplicationContext ctx, String resourcePath, String credentials, String cookies) throws Exception {
        return getHttpEntityRequest(ctx, resourcePath, credentials, cookies, false, null);
    }

    public static HttpEntity<String> getHttpEntityRequest(ApplicationContext ctx, String resourcePath, String credentials, String cookies, boolean arrayObjectExpected) throws Exception {
        return getHttpEntityRequest(ctx, resourcePath, credentials, cookies, arrayObjectExpected, null);
    }

    public static HttpEntity<String> getHttpEntityRequest(ApplicationContext ctx, String payload, String credentials, String cookies, boolean arrayObjectExpected, Map<String, List<String>> customHeaders) throws Exception{
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        if (payload == null || payload.isEmpty()) {
            if (credentials != null &&
                    credentials.indexOf(":") > 1 &&
                    !credentials.startsWith("{") &&
                    !credentials.endsWith("}")
                ) {
            String username = credentials.split(":")[0];
            String password = credentials.split(":")[1];
            payload = "{\r\n" +
                    "\"Username\":\"" + username + "\",\r\n" +
                    "\"SetCookie\":true,\r\n" +
                    "\"Password\":\"" + password + "\"\r\n" +
                    "}";
            }
        } else {
            headers = getHeadersForJsonWithBasicAuth(credentials);
        }

        if (customHeaders != null && customHeaders.size() > 0){
            headers.putAll(customHeaders);
        }

        if (cookies != null && !cookies.isEmpty()){
            headers.set("Cookie", cookies);
        }

        HttpEntity<String> request = new HttpEntity<String>(
                (
                    (
                        payload != null &&
                        !payload.trim().startsWith("{") &&
                        payload.indexOf("/") >= 0
                    ) ?
                        getJsonStringFromResource(ctx, payload, !arrayObjectExpected):
                        payload
                ),
                headers);
        return request;
    }
}
