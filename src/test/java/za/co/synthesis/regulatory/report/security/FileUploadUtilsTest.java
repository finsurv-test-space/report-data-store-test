package za.co.synthesis.regulatory.report.security;

import org.junit.Test;
import za.co.synthesis.regulatory.report.support.FileUploadUtil;

import java.util.ArrayList;
import java.util.List;

public class FileUploadUtilsTest {
  
  @Test
  public void testFileNameUtilities(){
    try {
      FileUploadUtil.makeNameSafe("some_name_that_is.safe");
      FileUploadUtil.makeNameSafe("some_name_that_is.not.safe");

      String nameX = "";
      for (int i =0; i< 255; i++){
        nameX += "x";
      }
      nameX += ".ext";
      assert(!FileUploadUtil.isNameSafe(nameX));
      assert(FileUploadUtil.isNameSafe(FileUploadUtil.makeNameSafe(nameX)));
      
      nameX = "prefix.";
      for (int i =0; i< 255; i++){
        nameX += "x";
      }
      assert(!FileUploadUtil.isNameSafe(nameX));
      assert(FileUploadUtil.isNameSafe(FileUploadUtil.makeNameSafe(nameX)));
    } catch(Exception e){}
  }
  
  @Test
  public void testFileExtensionWhitelistingUtilities(){
    try {
      List<String> extensionWhiteList = new ArrayList<>();
      FileUploadUtil.setExtensionWhiteList(extensionWhiteList);
      String unsafeName = "some_name_that_is.not.s$^afe<>##@";
      assert(FileUploadUtil.isExtensionWhiteListed(unsafeName));

      String currentName = "some_name_that_is.not.safe";
      String newName = FileUploadUtil.makeNameSafe(currentName);
  
      extensionWhiteList = new ArrayList<>();
      extensionWhiteList.add("safe");
      extensionWhiteList.add("is.safe");
      extensionWhiteList.add("isVerySafe");
      FileUploadUtil.setSafeExtensionWhiteList(extensionWhiteList);
      assert(!FileUploadUtil.extensionWhiteList.contains("is.safe"));
      assert(FileUploadUtil.extensionWhiteList.contains("is_safe"));
      assert(FileUploadUtil.isExtensionWhiteListed(newName));
      assert(FileUploadUtil.isExtensionWhiteListed("is.safe"));
  
      FileUploadUtil.setExtensionWhiteList(extensionWhiteList);
      assert(FileUploadUtil.extensionWhiteList.contains("is.safe"));
      assert(!FileUploadUtil.extensionWhiteList.contains("is_safe"));
      assert(FileUploadUtil.isExtensionWhiteListed(newName));
      assert(FileUploadUtil.isExtensionWhiteListed("is.safe"));   //extension extracted as "safe" => which exists in the whitelist
    } catch(Exception e){}
  }
  
  
}
