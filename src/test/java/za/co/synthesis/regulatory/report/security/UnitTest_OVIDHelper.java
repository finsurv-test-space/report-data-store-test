package za.co.synthesis.regulatory.report.security;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.regulatory.report.security.investec.OVIDHelper;
import za.co.synthesis.regulatory.report.utils.RestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

/**
 * Created by Marais Neethling on 2017/10/13.
 */
public class UnitTest_OVIDHelper {

  OVIDHelper ovidHelper = new OVIDHelper();
  String src_resource_path = "/samples/json/GetRolesByGCNSystemNameResult.json";


  @Category({RestUtils.FastTests.class, RestUtils.ServerlessTests.class})
  @Test
  public void testGetOVIDSformJSON() throws Exception {
    JSObject o = loadResourceAsJSObject(src_resource_path);
    List<String> ovids = ovidHelper.getOVIDSfromJSON(o);
    System.out.println(ovids);
  }

  private JSObject loadResourceAsJSObject(String path) throws IOException, Exception {
    Reader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(path)));
    if (reader != null) {
      JSStructureParser parser = new JSStructureParser(reader);
      Object obj = parser.parse();
      if (obj instanceof JSObject)
        return (JSObject) obj;
    }

    return null;
  }

}
