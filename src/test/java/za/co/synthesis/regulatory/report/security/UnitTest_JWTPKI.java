package za.co.synthesis.regulatory.report.security;

import io.jsonwebtoken.*;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import za.co.synthesis.regulatory.report.security.jwt.JWTConsumer;
import za.co.synthesis.regulatory.report.security.jwt.JWTProvider;
import za.co.synthesis.regulatory.report.utils.RestUtils;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

public class UnitTest_JWTPKI {

  @Category({RestUtils.FastTests.class, RestUtils.ServerlessTests.class})
  @Test
  public void signAndCheck() throws Exception {
    KeyPair keyPair = SecurityHelper.generatePPK();

    Map<String, Object> claims = new HashMap<String, Object>();
    Map<String, Object> headers = new HashMap<String, Object>();
    claims.put("user", "me");

    JwtBuilder jwtBuilder = Jwts.builder()
            .setSubject("Test")
            .addClaims(claims)
            .compressWith(CompressionCodecs.DEFLATE)
            .signWith(SignatureAlgorithm.RS512, keyPair.getPrivate());

    String jwtToken = jwtBuilder.compact();

    JwtParser jwtParser = Jwts.parser();
    boolean isSigned = jwtParser.isSigned(jwtToken) /* && sigKey != null*/;
    if (!isSigned){
      throw new JWTConsumer.JwtTokenException("Token supplied is either unsigned or signed using a key which has not been published to this service.");
    }

    Jwt token = jwtParser
            .requireSubject("Test")
            .setSigningKey(keyPair.getPublic())
            .parse(jwtToken);

    System.out.println(token.getBody().toString());
  }

}
