package za.co.synthesis.regulatory.report.storage;

import za.co.synthesis.regulatory.report.persist.types.PersistenceObject;

import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by jake on 8/21/17.
 * @deprecated
 */
@Entity
public class TEST_HATable extends PersistenceObject {
  private String reference;
  private java.sql.Timestamp when;
  private Map<String, Object> content;

  public TEST_HATable(String uuid) {
    super(uuid);
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public java.sql.Timestamp getWhen() {
    return when;
  }

  public void setWhen(java.sql.Timestamp when) {
    this.when = when;
  }

  public Map<String, Object> getContent() {
    return content;
  }

  public void setContent(Map<String, Object> content) {
    this.content = content;
  }
}
