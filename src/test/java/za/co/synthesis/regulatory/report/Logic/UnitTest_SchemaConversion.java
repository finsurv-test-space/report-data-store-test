package za.co.synthesis.regulatory.report.Logic;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.regulatory.report.history.HistoryUtils;
import za.co.synthesis.regulatory.report.schema.ReportData;
import za.co.synthesis.regulatory.report.support.JSReaderUtil;
import za.co.synthesis.regulatory.report.transform.JsonMapper;
import za.co.synthesis.regulatory.report.transform.JsonSchemaTransform;
import za.co.synthesis.regulatory.report.utils.RestUtils;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 6/3/17.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:report-service-external-root-tests.xml"})
public class UnitTest_SchemaConversion implements ApplicationContextAware{


  @Autowired
  public ApplicationContext ctx;

  @Autowired
  public JsonSchemaTransform transformer;


  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.ctx = applicationContext;
  }

    //unfortunately, this test case requires the application context in it's current form and is breaking the CI build.
  @Category({RestUtils.FastTests.class, RestUtils.ServerlessTests.class})
  @Test
  public void convertJSObjectSarbToSadc() throws Exception {
    String reportNodeName = "Report";
    String metaNodeName = "Meta";
    String srcFormat = "sarb";
    String destFormat = "sadc";
    String src_resource_path = "classpath:/samples/json/" + srcFormat + "_transaction.json";
//    String src_resource_path = "/samples/json/" + srcFormat + "_transaction.json";

    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//      Path path = Paths.get(getClass().getClassLoader()
//              .getResource(src_resource_path).toURI());
//    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(path);
    ReportData originalData = new ReportData((Map<String,Object>)originalObj.get(metaNodeName), (Map<String,Object>)originalObj.get(reportNodeName));

    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(path);

    System.err.flush();
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Original Object:\r\n----------------\r\n");
    JsonMapper jm = new JsonMapper();
    Map<String, Object> srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);
    System.out.flush();

    JSObject report = (JSObject)srcObj.get(reportNodeName);
    //check that the mapToJson is working...
    assert(JsonSchemaTransform.mapToJson((Map<String, Object>) report)!=null);
    assert(report != null);
    //Check that DomesticCurrency is not present to begin with
    Object dc = report.get("DomesticCurrency");
    assert(dc == null);
    JSArray ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    JSObject ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the RandValue is present to begin with
    String randValue = (String)ma_one.get("RandValue");
    assert(randValue!=null && !randValue.isEmpty());
    //Check that the DomesticValue is not present to begin with
    String domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue==null);

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, srcFormat, destFormat);
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converted Object:\r\n-----------------\r\n");
    jm = new JsonMapper();
    srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);

    ReportData newData = new ReportData((Map<String,Object>)srcObj.get(metaNodeName), (Map<String,Object>)srcObj.get(reportNodeName));
    System.out.println(HistoryUtils.getJsonDiffText(originalData, newData));
    System.out.flush();

    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the defaulted DomesticCurrency is present
//    String domesticCurrency = (String)report.get("DomesticCurrency");
//    assert(domesticCurrency.equalsIgnoreCase("ZAR"));
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //Check that the RandValue is no longer present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);
    //Check that the Domestic value is now present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());


    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Reverting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, destFormat, srcFormat);
    System.out.flush();


    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the untouched, defaulted value is removed as part of the conversion back
    String domesticCurrency = (String)report.get("DomesticCurrency");
    assert(domesticCurrency==null);
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the RandValue is present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue!=null && !randValue.isEmpty());
    //Check that the DomesticValue is no longer present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue==null);
  }

  //unfortunately, this test case requires the application context in it's current form and is breaking the CI build.
  @Category({RestUtils.FastTests.class, RestUtils.ServerlessTests.class})
  @Test
  public void convertJSObjectSadcToSarb() throws Exception {
    String reportNodeName = "Report";
    String metaNodeName = "Meta";
    String srcFormat = "sadc";
    String destFormat = "sarb";
    String src_resource_path = "classpath:/samples/json/" + srcFormat + "_transaction.json";
//    String src_resource_path = "/samples/json/" + srcFormat + "_transaction.json";

    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//      Path path = Paths.get(getClass().getClassLoader()
//              .getResource(src_resource_path).toURI());
//    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(path);
    ReportData originalData = new ReportData((Map<String,Object>)originalObj.get(metaNodeName), (Map<String,Object>)originalObj.get(reportNodeName));

    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(path);

    System.err.flush();
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Original Object:\r\n----------------\r\n");
    JsonMapper jm = new JsonMapper();
    Map<String, Object> srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);
    System.out.flush();

    JSObject report = (JSObject)srcObj.get(reportNodeName);
    //check that the mapToJson is working...
    assert(JsonSchemaTransform.mapToJson((Map<String, Object>) report)!=null);
    assert(report != null);
    //Check that DomesticCurrency is not present to begin with
    Object dc = report.get("DomesticCurrency");
    assert(dc != null);
    JSArray ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    JSObject ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the DomesticValue is present to begin with
    String domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());
    //Check that the RandValue is not present to begin with
    String randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, srcFormat, destFormat);
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converted Object:\r\n-----------------\r\n");
    jm = new JsonMapper();
    srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);

    ReportData newData = new ReportData((Map<String,Object>)srcObj.get(metaNodeName), (Map<String,Object>)srcObj.get(reportNodeName));
    System.out.println(HistoryUtils.getJsonDiffText(originalData, newData));
    System.out.flush();

    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the defaulted DomesticCurrency is not present
//    String domesticCurrency = (String)report.get("DomesticCurrency");
//    assert(domesticCurrency==null);
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //Check that the RandValue is now present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue!=null && !randValue.isEmpty());
    //Check that the Domestic value is no longer present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue==null);


    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Reverting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, destFormat, srcFormat);
    System.out.flush();


    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the untouched, defaulted value is now present as part of the conversion back
    String domesticCurrency = (String)report.get("DomesticCurrency");
    assert(domesticCurrency!=null && !domesticCurrency.isEmpty());
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the RandValue is not present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);
    //Check that the DomesticValue is present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());
  }

  //unfortunately, this test case requires the application context in it's current form and is breaking the CI build.
  @Category({RestUtils.FastTests.class, RestUtils.ServerlessTests.class})
  @Test
  public void convertJSObjectSadcToRbm() throws Exception {
    String reportNodeName = "Report";
    String metaNodeName = "Meta";
    String srcFormat = "sadc";
    String destFormat = "rbm";
    String src_resource_path = "classpath:/samples/json/" + srcFormat + "_transaction.json";
//    String src_resource_path = "/samples/json/" + srcFormat + "_transaction.json";

    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//      Path path = Paths.get(getClass().getClassLoader()
//              .getResource(src_resource_path).toURI());
//    JSObject originalObj =  JSReaderUtil.loadResourceAsJSObject(path);
    ReportData originalData = new ReportData((Map<String,Object>)originalObj.get(metaNodeName), (Map<String,Object>)originalObj.get(reportNodeName));

    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(ctx, src_resource_path);
//    JSObject srcObj =  JSReaderUtil.loadResourceAsJSObject(path);

    System.err.flush();
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Original Object:\r\n----------------\r\n");
    JsonMapper jm = new JsonMapper();
    Map<String, Object> srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);
    System.out.flush();

    JSObject report = (JSObject)srcObj.get(reportNodeName);
    //check that the mapToJson is working...
    assert(JsonSchemaTransform.mapToJson((Map<String, Object>) report)!=null);
    assert(report != null);
    //Check that DomesticCurrency is present
    Object dc = report.get("DomesticCurrency");
    assert(dc != null);
    JSArray ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    JSObject ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the DomesticValue is present to begin with
    String domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());
    //Check that the RandValue is not present to begin with
    String randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, srcFormat, destFormat);
    System.out.flush();

    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Converted Object:\r\n-----------------\r\n");
    jm = new JsonMapper();
    srcMap = new HashMap<>();
    jm.mapJsonObject(srcObj.get(reportNodeName), "", srcMap);

    ReportData newData = new ReportData((Map<String,Object>)srcObj.get(metaNodeName), (Map<String,Object>)srcObj.get(reportNodeName));
    System.out.println(HistoryUtils.getJsonDiffText(originalData, newData));
    System.out.flush();

    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the defaulted DomesticCurrency is present
    String domesticCurrency = (String)report.get("DomesticCurrency");
    assert(domesticCurrency!=null && !domesticCurrency.isEmpty());
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //Check that the RandValue is no longer present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);
    //Check that the Domestic value is now present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());


    System.out.println("\r\n\r\n================================================================================\r\n");
    System.out.println("Reverting Object:\r\n------------------\r\n");
    transformer.convert(srcObj, destFormat, srcFormat);
    System.out.flush();


    report = (JSObject)srcObj.get(reportNodeName);
    assert(report != null);
    //Check that the untouched, defaulted value is now present as part of the conversion back
    domesticCurrency = (String)report.get("DomesticCurrency");
    assert(domesticCurrency!=null && !domesticCurrency.isEmpty());
    ma = (JSArray)report.get("MonetaryAmount");
    assert(ma!=null);
    ma_one = (JSObject)ma.get(0);
    assert(ma_one!=null);
    //check the RandValue is not present
    randValue = (String)ma_one.get("RandValue");
    assert(randValue==null);
    //Check that the DomesticValue is present
    domesticValue = (String)ma_one.get("DomesticValue");
    assert(domesticValue!=null && !domesticValue.isEmpty());
  }


}
