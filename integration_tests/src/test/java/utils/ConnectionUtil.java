package utils;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.synthesis.rule.core.CustomValidateResult;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...
public class ConnectionUtil {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);

    public static final String urlTemplateKey = "urlTemplate";
    public static final String httpRequestMethodKey = "httpRequestMethod";
    public static final String headerTemplatesKey = "headerTemplates";
    public static final String payloadTemplateKey = "payloadTemplate";

    public static final String sslContextKey = "sslContext";

    public static final String keyStoreTemplateKey = "keyStoreTemplate";
    public static final String keyStoreTypeKey = "keyStoreType";
    public static final String keyStorePathKey = "keyStorePath";
    public static final String keyStorePassKey = "keyStorePass";

    public static final String trustStoreTemplateKey = "trustStoreTemplate";
    public static final String trustStoreTypeKey = "trustStoreType";
    public static final String trustStorePathKey = "trustStorePath";
    public static final String trustStorePassKey = "trustStorePass";

    public static final String defaultSSLContext = "TLS";
    public static final String allowedHttpRequestMethodsRegex = "^(?i)(:?(:?GET)|(:?PUT)|(:?POST)|(:?DELETE)|(:?OPTIONS))$";



    public static String getHttpConnectionBody(HttpURLConnection connection) throws Exception{
        if (connection instanceof  HttpURLConnection) {
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine+"\n");
                }
                in.close();

                // print result
                String responseStr = response.toString();
//                System.out.println(responseStr);
                return responseStr;
            } else {
                throw new Exception(connection.getRequestMethod()+" request failed (HTTP Response Code: " + responseCode + ")");
            }
        }
        return null;
    }

    public static String getHttpsConnectionBody(HttpsURLConnection connection) throws Exception{
        if (connection instanceof  HttpURLConnection) {
            int responseCode = connection.getResponseCode();
//            System.out.println("GET Response Code :: " + responseCode);
            CustomValidateResult result;
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                String responseStr = response.toString();
//                System.out.println(responseStr);
                return responseStr;
            } else {
                throw new Exception(connection.getRequestMethod()+" request failed (HTTP Response Code: " + responseCode + ")");
            }
        }
        return null;
    }


    public static HttpURLConnection getHttpConnection(String url, String requestMethod) throws Exception{
        HttpURLConnection connection = null;
        try {
            URL reportDataStoreURL = new URL(url);
//            System.out.println("**Calling URL: "+reportDataStoreURL+"\n\n");
            connection = (HttpURLConnection) reportDataStoreURL.openConnection();
            connection.setRequestMethod(requestMethod);
            return connection;
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }
        return null;
    }


    public static ConnectionResponse getHttpConnectionResponse(String channelName, HttpURLConnection connection) throws Exception{
        ConnectionResponse response = new ConnectionResponse(channelName);
        response.setResponseCode(connection.getResponseCode());
        response.setResponseHeaders(connection.getHeaderFields());
        String httpContent = ConnectionUtil.getHttpConnectionBody(connection);
        response.setResponseBody(httpContent);
        return response;
    }

    public static ConnectionResponse getHttpsConnectionResponse(String channelName, HttpsURLConnection connection) throws Exception{
        ConnectionResponse response = new ConnectionResponse(channelName);
        response.setResponseCode(connection.getResponseCode());
        response.setResponseHeaders(connection.getHeaderFields());
        String httpContent = ConnectionUtil.getHttpsConnectionBody(connection);
        response.setResponseBody(httpContent);
        return response;
    }
}
