package utils;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Properties;

public class RDSLogin {

    private String loginUrl;
    private ResponseEntity<String> responseEntity = null;
    private String cookies = "";

    public RDSLogin() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        loginUrl = properties.getProperty("uriBase") + "login/api/login";
    }

    public RDSLogin login() throws Exception {
        Properties properties = IntegrationTestProperties.getInstance();
        return login(properties.getProperty("username"), properties.getProperty("password"));
    }

    public RDSLogin login(String username, String password) {
        String requestBody = "{\r\n" +
                "\"Username\":\"" + username + "\",\r\n" +
                "\"SetCookie\":true,\r\n" +
                "\"Password\":\"" + password + "\"\r\n" +
                "}";

        HttpEntity<String> request = new HttpEntityBuilder()
            .setContentType(MediaType.APPLICATION_JSON_VALUE)
            .setPayload(requestBody)
            .build();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(loginUrl);

        RestTemplate testyResty = new RestTemplate();

        responseEntity = testyResty.exchange(
                builder.build().toUri().toString(),
                HttpMethod.POST,
                request,
                String.class);

        cookies = getCookies(responseEntity.getHeaders());

        return this;
    }

    private String getCookies(HttpHeaders httpHeaders) {
        String cookies = "";
        for (String cookie : httpHeaders.get("Set-Cookie")){
            cookies += (cookies.length() > 0 ? "; " : "") + cookie;
        }
        return cookies;
    }

    public String getCookies() {
        return this.cookies;
    }

    public ResponseEntity<String> getResponseEntity() {
        return this.responseEntity;
    }
}
