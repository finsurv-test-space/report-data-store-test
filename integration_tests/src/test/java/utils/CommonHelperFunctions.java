package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

//TODO: Complete (client-friendly) java doc for the code below...

/**
 * A collection of helper functions specifically crafted for working with JSON data, Finsurv context and the FinsurvLocal and Report Data Store code bases.
 */
public class CommonHelperFunctions {
    private static final Logger logger = LoggerFactory.getLogger(CommonHelperFunctions.class);


    public static void processArguments(String[] args, String channelName, String jsonFile) {
        boolean unknownArguments = false;
        for (String arg: args){
            String argument = arg.replaceAll("^[-/]+", "");
            int colonIndex = argument.indexOf(":");
            if ("channel".equalsIgnoreCase(argument.substring(0,colonIndex))){
                channelName = argument.substring(colonIndex+1);
                logger.info("...channel name provided: "+channelName);
            } else if ("json".equalsIgnoreCase(argument.substring(0,colonIndex))){
                jsonFile = argument.substring(colonIndex+1);
                logger.info("...json file name provided: "+jsonFile);
            } else {
                unknownArguments = true;
            }
        }
        if (unknownArguments){
            logger.info("Expected arguments and usage:\n\n" +
                    "\t-channel:[channelName] \tThe channel package to use. If not provided, 'sbZA' will be used\n" +
                    //"\t-evaluate:[filename] \tRun the bulk evaluation against the given file.\n" +
                    "\t-validate:[filename]   \tRun the bulk validation against the given file.\n" +
                    "\t-json:[filename]       \tFile containing the BOP JSON to use.\n" +
                    "");
        }
    }

    /**
     * Takes in a path and reads in the file content
     * @param path
     * @return
     */
    public static String getFileContent(String path) {
        String content = null;
        //JAVA 8 COMPATIBLE CODE:    (:MEH:)
        //-----------------------
        Charset encoding = StandardCharsets.UTF_8;
        try
        {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            content = new String(encoded, encoding);
        }
        catch (IOException e) {
            logger.error("Cannot load from file name '" + path + "'");
            return null;
        }
        //-----------------------

    /*
    //JAVA 6 COMPATIBLE CODE:   (:SADFACE:)
    //-----------------------
    File file = new File(path); // For example, foo.txt
    FileReader reader = null;
    try {
      reader = new FileReader(file);
      char[] chars = new char[(int) file.length()];
      reader.read(chars);
      content = new String(chars);
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if(reader != null){
        try {
          reader.close();
        } catch(Exception e){
          //unable to close the reader.
        }
      }
    }
    */
        return content;
    }


    public static String addParamsToHttpConnection(String url, Map<String, String> params) {

        if(params.size() == 0)
            return url;

        url = url + "?";
        for(String paramName : params.keySet()) {
            url = url + paramName + "=" + params.get(paramName) + "&";
        }

        if(url.lastIndexOf('&') == url.length() - 1)
            url = url.substring(0, url.length() - 1);

        return url;
    }

    public static HttpURLConnection addHeadersToHttpConnection(HttpURLConnection connection, Map<String, String> headers) {

        if(headers.size() == 0)
            return connection;

        for(String headerName : headers.keySet()) {
            connection.addRequestProperty(headerName, headers.get(headerName));
        }

        return connection;
    }

    public static HttpsURLConnection addHeadersToHttpConnection(HttpsURLConnection connection, Map<String, String> headers) {

        if(headers.size() == 0)
            return connection;

        for(String headerName : headers.keySet()) {
            connection.addRequestProperty(headerName, headers.get(headerName));
        }

        return connection;
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------->

}
