package utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class HttpEntityBuilder {

    private String payload;
    private String resourcePath;
    private String credentials;
    private String username;
    private String password;
    private String cookies;
    private String contentType;
    private String accept;
    private Map<String, List<String>> customHeaders;
    private HttpHeaders headers;

    public String getPayload() {
        return payload;
    }

    public HttpEntityBuilder setPayload(String payload) {
        this.payload = payload;
        return this;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public HttpEntityBuilder setResourcePath(String resourcePath) throws IOException {
        this.resourcePath = resourcePath;

        Path path = Paths.get(resourcePath);
        if (Files.exists(path))
            payload = readFileContentsIntoString(path);
        else
            throw new FileNotFoundException("Could not find resource file " + resourcePath);

        return this;
    }

    private String readFileContentsIntoString(Path path) throws IOException
    {
        StringBuilder contentBuilder = new StringBuilder();
        Files.lines(path, StandardCharsets.UTF_8)
            .forEach(line -> contentBuilder.append(line).append("\n"));
        return contentBuilder.toString();
    }

    public String getCredentials() {
        return credentials;
    }

    public HttpEntityBuilder setCredentials(String credentials) {
        this.credentials = credentials;
        if (credentials != null && credentials.indexOf(":") > 1) {
            this.username = credentials.split(":")[0];
            this.password = credentials.split(":")[1];
        }
        return this;
    }

    public String getUsername() {
        return username;
    }

    public HttpEntityBuilder setUsername(String username) {
        this.username = username;
        this.credentials = username + ":" + (isPopulated(password) ? password : "");
        return this;
    }

    public String getPassword() {
        return password;
    }

    public HttpEntityBuilder setPassword(String password) {
        this.password = password;
        this.credentials = (isPopulated(username) ? username : "") + ":" + password;
        return this;
    }

    public String getCookies() {
        return cookies;
    }

    public HttpEntityBuilder setCookies(String cookies) {
        this.cookies = cookies;
        return this;
    }

    public Map<String, List<String>> getCustomHeaders() {
        return customHeaders;
    }

    public HttpEntityBuilder setCustomHeaders(Map<String, List<String>> customHeaders) {
        this.customHeaders = customHeaders;
        return this;
    }

    public HttpEntityBuilder setHeaders(HttpHeaders headers) {
        this.headers = headers;
        return this;
    }

    public HttpHeaders getHeaders() {
        return this.headers;
    }

    public HttpEntity<String> build() {
        headers = new HttpHeaders();
        headers.add("Accept", this.accept);
        if(contentType != null)
        {
            headers.add("Content-Type", this.contentType);
        }

        if (isPopulated(credentials)) {
            headers.add("Authorization", "Basic " + base64Encode(credentials));
        }

        if (isPopulated(customHeaders)) {
            headers.putAll(customHeaders);
        }

        if (isPopulated(cookies)){
            headers.set("Cookie", cookies);
        }

        return new HttpEntity<>(payload, headers);
    }

    private boolean isPopulated(String value) {
        return !isNotPopulated(value);
    }

    private boolean isNotPopulated(String value) {
        return value == null || value.isEmpty();
    }

    private String usernameAndPasswordJsonPayload() {
        return
                "{\r\n" +
                    "\"Username\":\"" + username + "\",\r\n" +
                    "\"SetCookie\":true,\r\n" +
                    "\"Password\":\"" + password + "\"\r\n" +
                "}";
    }

    public HttpHeaders basicAuthHeaders(){
        headers.add("Accept", this.accept);
        headers.add("Content-Type", this.contentType);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", this.accept);
        if(contentType != null)
        {
            headers.add("Content-Type", this.contentType);
        }
        headers.add("Authorization", "Basic " + base64Encode(credentials));
        return headers;
    }

    public String getContentType(){
        return contentType;
    }

    public HttpEntityBuilder setContentType(String contentType){
        this.contentType = contentType;
        return this;
    }

    public HttpEntityBuilder setAccept(String accept){
        this.accept = accept;
        return this;
    }

    public String getAccept(){
        return accept;
    }

    private boolean isPopulated(Map<String, List<String>> mappedList) {
        return mappedList != null && mappedList.size() > 0;
    }

    private String base64Encode(String value) {
        return Base64.getMimeEncoder().encodeToString(value.getBytes());
    }
}
