package webservices.upload;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.*;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class Producer_Api_Upload_Reports_Sync_Data {

    private final String testUrl;

    public Producer_Api_Upload_Reports_Sync_Data() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "producer/api/upload/reports/sync/data";
    }

    public HttpEntity<String> uploadBulkReportsSynchSchema(String cookies, String reportPath, String schema, String channel) throws Exception {
        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(cookies)
                .setResourcePath(reportPath)
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl + "/" + schema)
                .queryParam("channelName", channel)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();
        return testyResty.exchange(
                uriString,
                HttpMethod.POST,
                request,
                String.class
        );
    }

    @Test
    public void when_schema_and_channel_are_valid_expect_successful_upload() throws Exception{
        String resourcePath = "src/test/resources/samples/json/multiple_sarb_reports.json";
        String schema = "genv3";
        String channel = "sbZA";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        HttpEntity<String> response = uploadBulkReportsSynchSchema(rdsLogin.getCookies(), resourcePath, schema, channel);

        Assert.assertNotNull(response);

        JSArray responseObj = JSReaderUtil.loadReaderAsJSArray(new StringReader(response.getBody()));
        Assert.assertNotNull(responseObj);
        Assert.assertTrue(responseObj.size() > 0);

        for (Object valRep : responseObj) {
            JSObject validationResponse = (JSObject)valRep;
            Assert.assertTrue(validationResponse.containsKey("ResultType"));
            Assert.assertTrue(validationResponse.containsKey("TrnReference"));

            String trnRef = validationResponse.get("TrnReference").toString();
            Assert.assertNotNull(trnRef);
            Assert.assertFalse(trnRef.isEmpty());

            if (validationResponse.containsKey("Validations")){
                JSArray validationResults = (JSArray)validationResponse.get("Validations");
                if (validationResults!= null && validationResults.size() > 0){
                    for (Object valRes : validationResults){
                        JSObject valResObj = ((JSObject)valRes);
                        Assert.assertTrue(valResObj.containsKey("Type"));
                        Assert.assertNotNull(valResObj.get("Type"));

                        String valType = valResObj.get("Type").toString();
                        Assert.assertNotNull(valType);
                    }
                }
            }
        }
    }
}
