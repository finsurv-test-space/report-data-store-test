package webservices.lookup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.HttpEntityBuilder;
import utils.IntegrationTestProperties;
import utils.RDSLogin;

import java.io.IOException;
import java.util.Properties;

public class Producer_Api_Lookup_Countries {
    private String testUrl;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "producer/api/lookup/countries";
    }

    @Test
    public void when_channel_is_valid_get_countries() throws Exception {
        String channelName = "sbZA";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl)
                .queryParam("channelName", channelName)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();
        ResponseEntity<String> response = testyResty.exchange(
                uriString,
                HttpMethod.GET,
                request,
                String.class
        );

        Assert.assertNotNull(response);

        HttpStatus responseStatus = response.getStatusCode();
        Assert.assertNotNull(responseStatus);
        Assert.assertEquals(200, responseStatus.value());
    }
}