package webservices.download;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.HttpEntityBuilder;
import utils.IntegrationTestProperties;
import utils.JSReaderUtil;
import utils.RDSLogin;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class Consumer_Api_Download_Decisions {

    private String testUrl;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "consumer/api/download/decisions";
    }

    @Test
    public void when_channel_is_valid_get_decisions_successfully() throws Exception {
        String channelName = "sbZA";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login("superuser", "abc");

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl)
                .queryParam("channelName", channelName)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();

        HttpEntity<String> response = testyResty.exchange(
                uriString,
                HttpMethod.GET,
                request,
                String.class
        );

        Assert.assertNotNull(response);

        JSObject responseObj = JSReaderUtil.loadReaderAsJSObject(new StringReader(response.getBody()));
        Assert.assertNotNull(responseObj);
        Assert.assertTrue(responseObj.containsKey("Decisions"));

        JSArray reports = ((JSArray) responseObj.get("Decisions"));
        Assert.assertNotNull(reports);
        Assert.assertTrue(reports.size() > 0);
    }
}