package webservices.login_api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.HttpEntityBuilder;
import utils.IntegrationTestProperties;
import utils.RDSLogin;

import java.io.IOException;
import java.util.Properties;

public class Login_Api_Whoami {

    private String username;
    private String testUrl;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        username = properties.getProperty("username");
        testUrl = uriBase + "login/api/whoami";
    }

    @Test
    public void when_is_logged_in_then_return_user_details() throws Exception{
        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        String url = UriComponentsBuilder
                .fromHttpUrl(testUrl)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();
        ResponseEntity<String> response = testyResty.exchange(
                url,
                HttpMethod.GET,
                request,
                String.class
        );

        Assert.assertNotNull("A null response was received",response);
        Assert.assertEquals("The response code received was incorrect",200, response.getStatusCode().value());

        Assert.assertTrue(response.getBody().contains("Auth"));
        Assert.assertTrue(response.getBody().contains("Username"));
        Assert.assertTrue(response.getBody().contains(username));
        Assert.assertTrue(response.getBody().contains("Password"));
        Assert.assertTrue(response.getBody().contains("Rights"));
        Assert.assertTrue(response.getBody().contains("Access"));
        Assert.assertTrue(response.getBody().contains("Channels"));
        Assert.assertTrue(response.getBody().contains("ReportLists"));
        Assert.assertTrue(response.getBody().contains("UserEvents"));
    }
}
