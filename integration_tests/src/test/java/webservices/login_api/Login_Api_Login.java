package webservices.login_api;

import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.HttpServerErrorException;
import utils.IntegrationTestProperties;
import utils.RDSLogin;

import java.io.IOException;
import java.util.Properties;

public class Login_Api_Login {

    private String username;
    private String password;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    private boolean hasCookie(ResponseEntity<String> response, String cookieName) {
        return getCookie(response, cookieName) != null;
    }

    private String getCookie(ResponseEntity<String> response, String cookieName) {
        for (String cookie : response.getHeaders().get("Set-Cookie")) {
            if (cookie.startsWith(cookieName)) return cookie;
        }
        return null;
    }

    private String getCookieValue(ResponseEntity<String> response, String cookieName) {
        if (hasCookie(response, cookieName)) {
            String cookie = getCookie(response, cookieName);
            Assert.assertNotNull(cookie);
            return cookie.substring(cookie.indexOf("=") + 1);
        }
        return "";
    }

    @Test
    public void when_valid_username_and_password_then_successful_login() throws Exception {
        String cookieName = "access_token";

        RDSLogin login = new RDSLogin();
        ResponseEntity<String> response = login.login(username, password).getResponseEntity();

        Assert.assertNotNull("A null response was received", response);
        Assert.assertEquals("Incorrect response code",200, response.getStatusCode().value());
        Assert.assertNotEquals("Internal server error",500, response.getStatusCode().value());
        Assert.assertNotEquals("Temporary redirect",307, response.getStatusCode().value());

        Assert.assertNotNull("The response body is null", response.getBody());
        Assert.assertNotEquals("The response body is empty", 0, response.getBody().length());

        Assert.assertNotNull("The response header is null", response.getBody());
        Assert.assertNotEquals("The response header is empty", 0, response.getBody().length());

        Assert.assertTrue("The response header does not contain any cookies", response.getHeaders().containsKey("Set-Cookie"));

        Assert.assertTrue("The access_token cookie is not present", hasCookie(response, cookieName));
        Assert.assertTrue("The access_token cookie has not been set", getCookieValue(response, cookieName).length() > 0);
    }

    @Test
    public void when_valid_username_and_invalid_password_then_unsuccessful_login() throws Exception {
        RDSLogin login = new RDSLogin();
        try {
            login.login(username, "INVALIDPASSWORD");
            Assert.fail("Expected exception was not thrown");
        } catch (HttpServerErrorException ex) {
            Assert.assertEquals("Server response code is not 500", HttpStatus.INTERNAL_SERVER_ERROR, ex.getStatusCode());
        }
    }

    @Test
    public void when_invalid_username_and_valid_password_then_unsuccessful_login() throws Exception {
        RDSLogin login = new RDSLogin();
        try {
            login.login("INVALIDUSERNAME", password);
            Assert.fail("Expected exception was not thrown");
        } catch (HttpServerErrorException ex) {
            Assert.assertEquals("Server response code is not 500", HttpStatus.INTERNAL_SERVER_ERROR, ex.getStatusCode());
        }
    }
}
