package webservices.document;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Producer_Api_Document {
    private String testUrl;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "producer/api/document/adhoc";
    }

    @Test
    public void when_channel_and_trnreference_are_valid_expect_successful_upload() throws Exception {
        String resourcePath = "src/test/resources/samples/json/sample_document.txt";
        String channelName = "sbZA";
        String trnReference = "SINGLEUPLOAD";
        String documentId = "TESTDOC001";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login("superuser", "abc");

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .setResourcePath(resourcePath)
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl)
                .queryParam("channelName", channelName)
                .queryParam("trnReference", trnReference)
                .queryParam("documentId", documentId)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();
        ResponseEntity<String> response = testyResty.exchange(
                uriString,
                HttpMethod.POST,
                request,
                String.class
        );

        Assert.assertNotNull(response);

        HttpStatus responseStatus = response.getStatusCode();
        Assert.assertNotNull(responseStatus);
        Assert.assertEquals(200, responseStatus.value());
    }

    @Test
    public void when_channel_and_trnreference_are_valid_expect_successful_upload_with_httpURLConnection() {
        HttpURLConnection connection = null;
        String url = testUrl;
        String data = CommonHelperFunctions.getFileContent("src/test/resources/samples/json/sample_document.txt");
        String channelName = "sbZA";
        String trnReference = "SINGLEUPLOAD";
        String documentId = "TESTDOC001";

        String attachmentName = "file";
        String attachmentFileName = "sample_document.txt";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";

        try {
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String> headers = new HashMap<String, String>();

            params.put("channelName", channelName);
            params.put("trnReference", trnReference);
            params.put("documentId", documentId);

            String encoded = Base64.getEncoder().encodeToString(("superuser:abc").getBytes(StandardCharsets.UTF_8));  //Java 8

            headers.put("Authorization", "Basic "+encoded);

            url = CommonHelperFunctions.addParamsToHttpConnection(url, params);

            connection = ConnectionUtil.getHttpConnection(url,"POST");
            connection = CommonHelperFunctions.addHeadersToHttpConnection(connection, headers);

            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            DataOutputStream request = new DataOutputStream(
                    connection.getOutputStream());

            request.writeBytes(twoHyphens + boundary + crlf);
            request.writeBytes("Content-Disposition: form-data; name=\"" +
                    attachmentName + "\";filename=\"" +
                    attachmentFileName + "\"" + crlf);
            request.writeBytes(crlf);

            request.write(data.getBytes(StandardCharsets.UTF_8));

            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary +
                    twoHyphens + crlf);

            request.flush();
            request.close();

            //READ RESPONSE
            InputStream responseStream = new
                    BufferedInputStream(connection.getInputStream());

            BufferedReader responseStreamReader =
                    new BufferedReader(new InputStreamReader(responseStream));

            String line = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = responseStreamReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();

            responseStream.close();
            connection.disconnect();
            System.out.println("\n\nResponse: " + response);
        } catch (Exception e) {
            System.err.println("Can not connect to " + url);
            e.printStackTrace();
        }
    }
}