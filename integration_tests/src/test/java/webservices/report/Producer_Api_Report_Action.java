package webservices.report;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.HttpEntityBuilder;
import utils.IntegrationTestProperties;
import utils.JSReaderUtil;
import utils.RDSLogin;
import webservices.upload.Producer_Api_Upload_Reports_Sync_Data;
import za.co.synthesis.javascript.JSObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class Producer_Api_Report_Action {

    private String testUrl;

    @Before
    public void initializeProperties() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "producer/api/report/action";
    }

    @Test
    public void when_channel_reportingspace_and_trnreference_are_valid_and_action_is_Replace_expect_successful_response() throws Exception{
        String resourcePath = "src/test/resources/samples/json/single_sarb_report.json";
        String schema = "genv3";
        String channelName = "sbZA";
        String trnReference = "SINGLEUPLOAD";
        String action = "Replace";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        Producer_Api_Upload_Reports_Sync_Data report = new Producer_Api_Upload_Reports_Sync_Data();
        report.uploadBulkReportsSynchSchema(rdsLogin.getCookies(), resourcePath, schema, channelName);

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl)
                .queryParam("channelName", channelName)
                .queryParam("trnReference", trnReference)
                .queryParam("action", action)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();

        HttpEntity<String> response = testyResty.exchange(
                uriString,
                HttpMethod.POST,
                request,
                String.class
        );

        assertNewState("Replace", response);
    }

    private void assertNewState(String expected, HttpEntity<String> actual) throws Exception {
        Assert.assertNotNull(actual);
        JSObject responseObj = JSReaderUtil.loadReaderAsJSObject(new StringReader(actual.getBody()));

        Assert.assertNotNull(responseObj);
        Assert.assertTrue(responseObj.containsKey("State"));
        Assert.assertEquals(expected, responseObj.get("State"));
    }

    @Test
    public void when_channel_reportingspace_and_trnreference_are_valid_and_action_is_Cancel_expect_successful_response() throws Exception{
        String resourcePath = "src/test/resources/samples/json/single_sarb_report.json";
        String schema = "genv3";
        String channelName = "sbZA";
        String trnReference = "SINGLEUPLOAD";
        String action = "Cancel";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        Producer_Api_Upload_Reports_Sync_Data report = new Producer_Api_Upload_Reports_Sync_Data();
        report.uploadBulkReportsSynchSchema(rdsLogin.getCookies(), resourcePath, schema, channelName);

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        String uriString = UriComponentsBuilder.fromHttpUrl(testUrl)
                .queryParam("channelName", channelName)
                .queryParam("trnReference", trnReference)
                .queryParam("action", action)
                .build()
                .encode()
                .toUri()
                .toString();

        RestTemplate testyResty = new RestTemplate();

        HttpEntity<String> response = testyResty.exchange(
                uriString,
                HttpMethod.POST,
                request,
                String.class
        );

        assertNewState("Cancelled", response);
    }
}
