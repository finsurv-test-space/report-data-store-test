package webservices.report;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import utils.HttpEntityBuilder;
import utils.IntegrationTestProperties;
import utils.JSReaderUtil;
import utils.RDSLogin;
import webservices.upload.Producer_Api_Upload_Reports_Sync_Data;
import za.co.synthesis.javascript.JSObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class Producer_Api_Report_Data {

    private final String testUrl;

    public Producer_Api_Report_Data() throws IOException {
        Properties properties = IntegrationTestProperties.getInstance();
        String uriBase = properties.getProperty("uriBase");
        testUrl = uriBase + "producer/api/report/data";
    }

    @Test
    public void when_schema_channel_and_trnreference_are_valid_expect_successful_response() throws Exception {
        String resourcePath = "src/test/resources/samples/json/single_sarb_report.json";
        String schema = "genv3";
        String channelName = "sbZA";
        String trnReference = "SINGLEUPLOAD";

        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        Producer_Api_Upload_Reports_Sync_Data report = new Producer_Api_Upload_Reports_Sync_Data();
        report.uploadBulkReportsSynchSchema(rdsLogin.getCookies(), resourcePath, schema, channelName);

        HttpEntity<String> response = getReportDefault(channelName, schema, trnReference);

        Assert.assertNotNull(response);

        JSObject responseObj = JSReaderUtil.loadReaderAsJSObject(new StringReader(response.getBody()));
        Assert.assertNotNull(responseObj);
        Assert.assertTrue(responseObj.containsKey("Report"));

        JSObject reportObj = (JSObject)responseObj.get("Report");
        Assert.assertNotNull(reportObj);
        Assert.assertTrue(reportObj.containsKey("TrnReference"));
        Assert.assertEquals(trnReference, reportObj.get("TrnReference").toString());
    }

    public ResponseEntity<String> getReportDefault(String channelName, String schema, String trnReference) throws Exception {
        return getReportDefault(channelName, null, schema, trnReference);
    }

    public ResponseEntity<String> getReportDefault(String channelName, String reportSpace, String schema, String trnReference) throws Exception {
        RDSLogin rdsLogin = new RDSLogin();
        rdsLogin.login();

        HttpEntity<String> request = new HttpEntityBuilder()
                .setCookies(rdsLogin.getCookies())
                .build();

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(testUrl)
            .queryParam("channelName", channelName)
            .queryParam("schema", schema)
            .queryParam("trnReference", trnReference);

        if (reportSpace != null)
            uriBuilder.queryParam("reportSpace", reportSpace);

        String uriString = uriBuilder.build().toUri().toString();

        RestTemplate testyResty = new RestTemplate();
        return testyResty.exchange(
                uriString,
                HttpMethod.GET,
                request,
                String.class);
    }
}